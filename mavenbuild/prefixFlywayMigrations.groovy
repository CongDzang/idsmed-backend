import static groovy.io.FileType.*

def incrementalScriptsDir = new File("src/main/resources/db/migration");

incrementalScriptsDir.eachDirRecurse {
    println "Looping in :" + it.toString()
    def newDif =  new File(it.toString());
    newDif.traverse(type: FILES, excludeNameFilter: ~/.+\d+__.*/) { file ->
        def timestamp = new Date().format('yyyyMMddHHmmssSSS', TimeZone.getTimeZone('GMT+8'))
        println "Renaming $file.name to ${timestamp}__$file.name"
        file.renameTo("$file.parentFile.absolutePath$file.separator${timestamp}__$file.name")
        sleep(100)
    }
}
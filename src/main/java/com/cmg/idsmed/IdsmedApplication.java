package com.cmg.idsmed;

import com.cmg.idsmed.config.HttpConfig;
import com.cmg.idsmed.config.rabbitmq.IdsmedQueueConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;

import com.cmg.idsmed.config.dbconfig.PostgreSqlConfig;
import com.cmg.idsmed.config.security.SecurityConfig;

import org.springframework.scheduling.annotation.EnableScheduling;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@Import({PostgreSqlConfig.class, SecurityConfig.class, IdsmedQueueConfig.class, HttpConfig.class})
@EnableSwagger2
//@EnableScheduling
public class IdsmedApplication extends SpringBootServletInitializer{
	//Test CD on training 1
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(IdsmedApplication.class);
	}

	public static void main(String[] args) {
		SpringApplication.run(IdsmedApplication.class, args);
	}

	/**
	 * Message source.
	 *
	 * @return the reloadable resource bundle message source
	 */
	@Bean
	public MessageSource messageSource() {
		ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
		messageSource.setBasename("classpath:i18n/messages");
		messageSource.setCacheSeconds(1800);
		return messageSource;
	}

}

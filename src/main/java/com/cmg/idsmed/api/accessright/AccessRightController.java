package com.cmg.idsmed.api.accessright;

import com.cmg.idsmed.common.utils.LangHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.cmg.idsmed.common.Constant.IdsmedMessageKeyConst;
import com.cmg.idsmed.common.enums.ResponseStatusEnum;
import com.cmg.idsmed.common.exception.IdsmedBaseException;
import com.cmg.idsmed.dto.ResponseObject;
import com.cmg.idsmed.dto.accessright.AccessRightListResponse;
import com.cmg.idsmed.dto.auth.IdsmedRoleRequest;
import com.cmg.idsmed.service.accessright.AccessRightService;
import com.cmg.idsmed.service.share.MessageResourceService;

@RestController
@RequestMapping(value = "api/accessright")
public class AccessRightController {
	@Autowired
	private AccessRightService accessRightService;

	@Autowired
	private MessageResourceService messageResourceService;

	@RequestMapping(method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Object> getAccessRightList(@RequestParam(required = false) String name,
			@RequestParam(required = false) Integer status, @RequestParam(required = false) Integer roleType,
													 @RequestParam(required = false) Integer pageIndex,
			@RequestParam(required = false) Integer pageSize, @RequestParam(required = true) String langCode) {
		ResponseObject<Object> response = new ResponseObject<>();
		try {
			AccessRightListResponse accessRightListResponse = accessRightService.getAccessRightList(name, status, roleType, pageIndex, pageSize);
			response.setResponseData(accessRightListResponse);
			if(CollectionUtils.isEmpty(accessRightListResponse.getAccessRightList())){
				response.setMessage(messageResourceService.getMessage(IdsmedMessageKeyConst.ACCESS_RIGHT_NOT_FOUND, langCode));
			}
		} catch (IdsmedBaseException e) {
			response.setCode(ResponseStatusEnum.ERROR.getCode());
			response.setError(e.getError());
		}

		return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Object> getAccessRightById(@PathVariable("id") Long id) {
		ResponseObject<Object> response = new ResponseObject<>();
		try {
			response.setResponseData(accessRightService.getAccessRightById(id));
		} catch (IdsmedBaseException e) {
			response.setCode(ResponseStatusEnum.ERROR.getCode());
			response.setError(e.getError());
		}

		return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
	}


	@RequestMapping(value = "/create", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<Object> create(@RequestBody IdsmedRoleRequest request) {
		ResponseObject<Object> response = new ResponseObject<>();
		try {
			response.setResponseData(accessRightService.create(request));
			response.setMessage(messageResourceService.getMessage(IdsmedMessageKeyConst.CREATE_ACCESS_RIGHT_SUCCESS_MESSAGE,
					LangHelper.getLangCode(request.getLangCode())));
		} catch (IdsmedBaseException e) {
			response.setCode(ResponseStatusEnum.ERROR.getCode());
			response.setError(e.getError());
		}
		return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<Object> edit(@RequestBody IdsmedRoleRequest request) {
		ResponseObject<Object> response = new ResponseObject<>();
		try {
			response.setResponseData(accessRightService.edit(request));
			response.setMessage(messageResourceService.getMessage(IdsmedMessageKeyConst.EDIT_ACCESS_RIGHT_SUCCESS_MESSAGE,
					LangHelper.getLangCode(request.getLangCode())));
		} catch (IdsmedBaseException e) {
			response.setCode(ResponseStatusEnum.ERROR.getCode());
			response.setError(e.getError());
		}
		return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/approve", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<Object> approve(@RequestBody IdsmedRoleRequest request) {
		ResponseObject<Object> response = new ResponseObject<>();
		try {
			response.setResponseData(accessRightService.approve(request));
			response.setMessage(messageResourceService.getMessage(IdsmedMessageKeyConst.APPROVE_ACCESS_RIGHT_SUCCESS_MESSAGE,
					LangHelper.getLangCode(request.getLangCode())));
		} catch (IdsmedBaseException e) {
			response.setCode(ResponseStatusEnum.ERROR.getCode());
			response.setError(e.getError());
		}
		return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/get-init-info", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Object> getFunctionName() {
		ResponseObject<Object> response = new ResponseObject<>();
		try {
			response.setResponseData(accessRightService.getFunctionName());
		} catch (IdsmedBaseException e) {
			response.setCode(ResponseStatusEnum.ERROR.getCode());
			response.setError(e.getError());
		}
		return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
	}

	@RequestMapping(value = "/status/inactive", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseEntity<Object> inactiveRole(@RequestBody IdsmedRoleRequest request) {
		ResponseObject<Object> response = new ResponseObject<>();
		try {
			response.setResponseData(accessRightService.inactiveRole(request));
			response.setMessage(messageResourceService.getMessage(
					IdsmedMessageKeyConst.INACTIVE_ROLE_SUCCESS, LangHelper.getLangCode(request.getLangCode())));
		} catch (IdsmedBaseException e) {
			response.setCode(ResponseStatusEnum.ERROR.getCode());
			response.setError(e.getError());
		}

		return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
	}

	@RequestMapping(value = "/status/resume", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseEntity<Object> resumeInactiveRole(@RequestBody IdsmedRoleRequest request) {
		ResponseObject<Object> response = new ResponseObject<>();
		try {
			response.setResponseData(accessRightService.resumeInactiveRole(request));
			response.setMessage(messageResourceService.getMessage(
					IdsmedMessageKeyConst.INACTIVE_ROLE_SUCCESS, LangHelper.getLangCode(request.getLangCode())));
		} catch (IdsmedBaseException e) {
			response.setCode(ResponseStatusEnum.ERROR.getCode());
			response.setError(e.getError());
		}

		return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
	}
}

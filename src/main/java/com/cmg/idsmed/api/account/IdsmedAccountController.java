package com.cmg.idsmed.api.account;

import com.cmg.idsmed.common.utils.LangHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.cmg.idsmed.common.Constant.IdsmedMessageKeyConst;
import com.cmg.idsmed.common.enums.ResponseStatusEnum;
import com.cmg.idsmed.common.exception.IdsmedBaseException;
import com.cmg.idsmed.dto.ResponseObject;
import com.cmg.idsmed.dto.account.IdsmedAccountSettingsRequest;
import com.cmg.idsmed.service.share.MessageResourceService;
import com.cmg.idsmed.service.user.IdsmedUserService;


@RestController
@RequestMapping(value = "api/account")
public class IdsmedAccountController {

    @Autowired
    private IdsmedUserService idsmedUserService;

    @Autowired
    private MessageResourceService messageResourceService;

	

    @RequestMapping(value="/settings", method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<Object> edit(@RequestBody IdsmedAccountSettingsRequest request) {
        ResponseObject<Object> response = new ResponseObject<>();
        try {
            response.setResponseData(idsmedUserService.editAccountSettings(request));
            response.setMessage(messageResourceService.getMessage(IdsmedMessageKeyConst.EDIT_ACCOUNT_SETTING_SUCCESS_MESSAGE, LangHelper.getLangCode(request.getLangCode())));
        } catch (IdsmedBaseException e) {
            response.setCode(ResponseStatusEnum.ERROR.getCode());
            response.setError(e.getError());
        }
        return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
    }
}

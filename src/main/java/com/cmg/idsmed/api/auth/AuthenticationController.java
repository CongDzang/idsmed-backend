package com.cmg.idsmed.api.auth;

import com.cmg.idsmed.common.Constant.IdsmedMessageKeyConst;
import com.cmg.idsmed.common.enums.ResponseStatusEnum;
import com.cmg.idsmed.common.exception.ErrorInfo;
import com.cmg.idsmed.common.exception.IdsmedBaseException;
import com.cmg.idsmed.dto.ResponseObject;
import com.cmg.idsmed.dto.auth.*;
import com.cmg.idsmed.model.entity.log.LogUserLoginHistory;
import com.cmg.idsmed.service.auth.AuthenticationService;
import com.cmg.idsmed.service.share.MessageResourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(value = "api/auth")
public class AuthenticationController {

    @Autowired
    private AuthenticationService authenticationService;

    @Autowired
    private MessageResourceService messageResourceService;

    @Autowired
    private HttpServletRequest httpServletRequest;

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<Object> login(@RequestBody LoginRequest request) {
        ResponseObject<Object> response = new ResponseObject<>();
        try {
            LoginResponse loginResponse = authenticationService.login(request);
            response.setResponseData(loginResponse);
            response.setMessage(messageResourceService.getMessage(IdsmedMessageKeyConst.LOGIN_SUCCESS_MESSAGE, request.getLangCode()));
            authenticationService.logUserLoginHistory(request.getLoginId(), httpServletRequest.getRemoteAddr(), LogUserLoginHistory.LoginStatusEnum.SUCCESS.getCode());
        } catch (IdsmedBaseException e) {
            authenticationService.logUserLoginHistory(request.getLoginId(), httpServletRequest.getRemoteAddr(), LogUserLoginHistory.LoginStatusEnum.FAIL.getCode());
            response.setCode(ResponseStatusEnum.ERROR.getCode());
            response.setError(e.getError());
        }
        return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
    }

    @RequestMapping(value="/api-auth", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<Object> apiAuthentication(@RequestBody ApiAuthRequest request) {
        String clientIp = httpServletRequest.getRemoteAddr();
        ResponseObject<Object> response = new ResponseObject<>();

        try {
            List<String> invalidParameters = validateRequest(request);
            if (!CollectionUtils.isEmpty(invalidParameters)) {
                response.setCode(ResponseStatusEnum.ERROR.getCode());
                response.setMessage(String.format(messageResourceService.getErrorInfo(ErrorInfo.AUTH_PARAM_MISSING_ERROR, request.getLangCode()).getMessage(), invalidParameters.stream().collect(Collectors.joining(","))));
                return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
            }
            
            ApiAuthResponse apiAuthResponse = authenticationService.apiGrantAuth(request, clientIp);
            response.setResponseData(apiAuthResponse);
        } catch (IdsmedBaseException e) {
            response.setCode(ResponseStatusEnum.ERROR.getCode());
            response.setError(e.getError());
        }
        return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
    }

    @RequestMapping(value="/password", method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<Object> changePassword(@RequestBody ChangePasswordRequest request) {
        ResponseObject<Object> response = new ResponseObject<>();
        try {
            response.setResponseData(authenticationService.changePassword(request));
            response.setMessage(messageResourceService.getMessage(IdsmedMessageKeyConst.CHANGE_PASSWORD_SUCCESS_MESSAGE, request.getLangCode()));
        } catch (IdsmedBaseException e) {
            response.setCode(ResponseStatusEnum.ERROR.getCode());
            response.setError(e.getError());
        }
        return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
    }

    @RequestMapping(value="/password/resetting", method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<Object> resetPassword(@RequestParam() String loginId, @RequestParam() String email, @RequestParam() String langCode) {
        ResponseObject<Object> response = new ResponseObject<>();
        try {
            response.setResponseData(authenticationService.resetPassword(email, loginId, langCode));
            response.setMessage(messageResourceService.getMessage(IdsmedMessageKeyConst.RESET_PASSWORD_SUCCESS_MESSAGE, langCode));
        } catch (IdsmedBaseException e) {
            response.setCode(ResponseStatusEnum.ERROR.getCode());
            response.setError(e.getError());
        }
        return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
    }


    private List<String> validateRequest(ApiAuthRequest request) {
        List<String> invalidParameters = new ArrayList<String> ();
        if (StringUtils.isEmpty(request.getCountryCode())) {
            invalidParameters.add(ApiAuthRequest.COUNTRY_CODE_PARAMETER_NAME);
        }
        if (StringUtils.isEmpty(request.getClientId())) {
            invalidParameters.add(ApiAuthRequest.CLIENT_ID_PARAMETER_NAME);
        }
        if (StringUtils.isEmpty(request.getPassword())) {
            invalidParameters.add(ApiAuthRequest.PASSWORD_PARAMETER_NAME);
        }
        if (StringUtils.isEmpty(request.getAppKey())) {
            invalidParameters.add(ApiAuthRequest.APPKEY_PARAMETER_NAME);
        }
        if (StringUtils.isEmpty(request.getLangCode())) {
            invalidParameters.add(ApiAuthRequest.LANGCODE_PARAMETER_NAME);
        }
        return invalidParameters;
    }
}

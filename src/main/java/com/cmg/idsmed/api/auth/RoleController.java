package com.cmg.idsmed.api.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.cmg.idsmed.common.enums.ResponseStatusEnum;
import com.cmg.idsmed.common.exception.IdsmedBaseException;
import com.cmg.idsmed.dto.ResponseObject;
import com.cmg.idsmed.service.auth.IdsmedRoleService;

@RestController
@RequestMapping(value = "api/role")
public class RoleController {

    @Autowired
    private IdsmedRoleService roleService;

    @RequestMapping(method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Object> getRoleList(@RequestParam("loginId") String loginId) {
		ResponseObject<Object> response = new ResponseObject<>();
		
		try {
			response.setResponseData(roleService.getRoleList(loginId));
		} catch (IdsmedBaseException e) {
			response.setCode(ResponseStatusEnum.ERROR.getCode());
			response.setError(e.getError());
		}

		return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
	}
}

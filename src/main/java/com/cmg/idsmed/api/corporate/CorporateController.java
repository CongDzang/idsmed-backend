package com.cmg.idsmed.api.corporate;

import com.cmg.idsmed.common.Constant.IdsmedMessageKeyConst;
import com.cmg.idsmed.common.enums.ResponseStatusEnum;
import com.cmg.idsmed.common.exception.IdsmedBaseException;
import com.cmg.idsmed.common.utils.LangHelper;
import com.cmg.idsmed.dto.ResponseObject;
import com.cmg.idsmed.dto.corporate.CorporateRegistrationRequest;
import com.cmg.idsmed.service.corporate.CorporateService;
import com.cmg.idsmed.service.share.MessageResourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.Optional;

@RestController
@RequestMapping(value = "api/corporate")
public class CorporateController {
	@Autowired
	private CorporateService corporateService;

	@Autowired
	private MessageResourceService messageResourceService;

	@RequestMapping(value="/user", method = RequestMethod.POST, consumes = "multipart/form-data")
	@ResponseBody
	public ResponseEntity<Object> registration(@RequestPart("corporateRegistrationRequest") CorporateRegistrationRequest request, @RequestPart("profileImgUpload") Optional<MultipartFile> profileImage, @RequestPart("frontIdImage") Optional<MultipartFile> frontIdImage, @RequestPart("backIdImage") Optional<MultipartFile> backIdImage) {
		ResponseObject<Object> response = new ResponseObject<>();
		try {
			response.setResponseData(corporateService.corporateUserRegister(request, profileImage.orElse(null), frontIdImage.orElse(null), backIdImage.orElse(null)));
			response.setMessage(messageResourceService.getMessage(IdsmedMessageKeyConst.USER_REGISTRATION_SUCCESS_MESSAGE, LangHelper.getLangCode(request.getLangCode())));
		} catch (IdsmedBaseException e) {
			response.setCode(ResponseStatusEnum.ERROR.getCode());
			response.setError(e.getError());
		}
		return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
	}

	@RequestMapping(value="/user", method = RequestMethod.PUT, consumes = "multipart/form-data")
	@ResponseBody
	public ResponseEntity<Object> editCorporateUser(@RequestPart("corporateRegistrationRequest") CorporateRegistrationRequest request, @RequestPart("profileImgUpload") Optional<MultipartFile> profileImage, @RequestPart("frontIdImage") Optional<MultipartFile> frontIdImage, @RequestPart("backIdImage") Optional<MultipartFile> backIdImage) {
		ResponseObject<Object> response = new ResponseObject<>();
		try {
			response.setResponseData(corporateService.editCorporateUser(request, profileImage.orElse(null), frontIdImage.orElse(null), backIdImage.orElse(null)));
			response.setMessage(messageResourceService.getMessage(IdsmedMessageKeyConst.USER_UPDATE_SUCCESS_MESSAGE, LangHelper.getLangCode(request.getLangCode())));
		} catch (IdsmedBaseException e) {
			response.setCode(ResponseStatusEnum.ERROR.getCode());
			response.setError(e.getError());
		}
		return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
	}
}

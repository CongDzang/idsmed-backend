package com.cmg.idsmed.api.dashboard;

import com.cmg.idsmed.common.enums.ResponseStatusEnum;
import com.cmg.idsmed.common.exception.IdsmedBaseException;
import com.cmg.idsmed.dto.ResponseObject;
import com.cmg.idsmed.service.dashboard.DashboardService;
import com.cmg.idsmed.service.task.IdsmedTaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.ZonedDateTime;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping(value = "api/dashboard")
public class DashboardController {

	@Autowired
	private DashboardService dashboardService;

	@Autowired
	private IdsmedTaskService idsmedTaskService;

	@RequestMapping(method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Object> getDashboardInfo(@RequestParam(required = false) List<Long> vendorIds,
												   @RequestParam(required = false) ZonedDateTime date) {
		ResponseObject<Object> response = new ResponseObject<>();
		try {
			response.setResponseData(dashboardService.getDashboardInfo(vendorIds, date));
		} catch (IdsmedBaseException e) {
			response.setCode(ResponseStatusEnum.ERROR.getCode());
			response.setError(e.getError());
		}

		return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/map")
	@ResponseBody
	public ResponseEntity<Object> getMapInfoByProvinceName() {
		ResponseObject<Object> response = new ResponseObject<>();
		try {
			response.setResponseData(dashboardService.getMapInfoByProvinceName());
		} catch (IdsmedBaseException e) {
			response.setCode(ResponseStatusEnum.ERROR.getCode());
			response.setError(e.getError());
		}

		return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
	}

	@RequestMapping(value = "/task", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Object> getTasksByLoginId(@RequestParam() String loginId
			, @RequestParam(required = false) Integer pageIndex
			, @RequestParam(required = false) Integer pageSize
			, @RequestParam() String langCode
													) {
		ResponseObject<Object> response = new ResponseObject<>();
		try {
			response.setResponseData(idsmedTaskService.getTasksByLoginId(loginId, pageIndex, pageSize, langCode));
		} catch (IdsmedBaseException e) {
			response.setCode(ResponseStatusEnum.ERROR.getCode());
			response.setError(e.getError());
		}

		return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
	}

}

package com.cmg.idsmed.api.email;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.cmg.idsmed.common.Constant.IdsmedMessageKeyConst;
import com.cmg.idsmed.common.enums.ResponseStatusEnum;
import com.cmg.idsmed.common.exception.IdsmedBaseException;
import com.cmg.idsmed.common.utils.LangHelper;
import com.cmg.idsmed.dto.ResponseObject;
import com.cmg.idsmed.dto.email.EmailNotificationConfigByUserRequest;
import com.cmg.idsmed.service.email.EmailNotificationConfigService;
import com.cmg.idsmed.service.share.MessageResourceService;

@RestController
@RequestMapping(value = "api/email-notification-config")
public class EmailNotificationConfigController {

	@Autowired
	EmailNotificationConfigService emailNotificationConfigService;

	@Autowired
	MessageResourceService messageResourceService;

	@RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Object> getEmailNotificationConfigs(@RequestParam String langCode) {
		ResponseObject<Object> response = new ResponseObject<>();
		try {
			response.setResponseData(emailNotificationConfigService.getEmailNotificationConfigs(langCode));
			response.setMessage(messageResourceService.getMessage(
					IdsmedMessageKeyConst.GET_EMAIL_NOTIFICATION_CONFIGS_SUCCESS_MESSAGE,
					LangHelper.getLangCode(langCode)));
			
		} catch (IdsmedBaseException e) {
			response.setCode(ResponseStatusEnum.ERROR.getCode());
			response.setError(e.getError());
		}

		return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<Object> create(@RequestBody EmailNotificationConfigByUserRequest request) {
		ResponseObject<Object> response = new ResponseObject<>();
		try {
			response.setResponseData(emailNotificationConfigService.createEmailNotificationConfigByUser(request));
			response.setMessage(messageResourceService.getMessage(
					IdsmedMessageKeyConst.CREATE_EMAIL_NOTIFICATION_CONFIG_SUCCESS_MESSAGE,
					LangHelper.getLangCode(request.getLangCode())));

		} catch (IdsmedBaseException e) {
			response.setCode(ResponseStatusEnum.ERROR.getCode());
			response.setError(e.getError());
		}

		return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.PUT)
	@ResponseBody
	public ResponseEntity<Object> edit(@RequestBody EmailNotificationConfigByUserRequest request) {
		ResponseObject<Object> response = new ResponseObject<>();
		try {
			response.setResponseData(emailNotificationConfigService.updateEmailNotificationConfigByUser(request));
			response.setMessage(messageResourceService.getMessage(
					IdsmedMessageKeyConst.EDIT_EMAIL_NOTIFICATION_CONFIG_SUCCESS_MESSAGE,
					LangHelper.getLangCode(request.getLangCode())));

		} catch (IdsmedBaseException e) {
			response.setCode(ResponseStatusEnum.ERROR.getCode());
			response.setError(e.getError());
		}

		return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
	}

	/*
	 * NOTE: Currently Email Notification Configuration will use this API call ONLY
	 * due to scenario cases, all configuration (create and update) will take place
	 * in the service implementation of this API
	 */
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<Object> updateEmailNotifications(@RequestBody EmailNotificationConfigByUserRequest request) {
		ResponseObject<Object> response = new ResponseObject<>();
		try {
			response.setResponseData(emailNotificationConfigService.updateEmailNotificationConfigs(request));
			response.setMessage(messageResourceService.getMessage(
					IdsmedMessageKeyConst.EDIT_EMAIL_NOTIFICATION_CONFIG_SUCCESS_MESSAGE,
					LangHelper.getLangCode(request.getLangCode())));

		} catch (IdsmedBaseException e) {
			response.setCode(ResponseStatusEnum.ERROR.getCode());
			response.setError(e.getError());
		}

		return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
	}
}

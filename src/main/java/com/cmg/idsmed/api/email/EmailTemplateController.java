package com.cmg.idsmed.api.email;

import com.cmg.idsmed.common.Constant.IdsmedMessageKeyConst;
import com.cmg.idsmed.common.enums.ResponseStatusEnum;
import com.cmg.idsmed.common.exception.IdsmedBaseException;
import com.cmg.idsmed.common.utils.LangHelper;
import com.cmg.idsmed.dto.ResponseObject;
import com.cmg.idsmed.dto.email.EmailTemplateListResponse;
import com.cmg.idsmed.dto.email.EmailTemplateRequest;
import com.cmg.idsmed.service.email.EmailTemplateService;
import com.cmg.idsmed.service.email.EmailWildcardService;
import com.cmg.idsmed.service.share.MessageResourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "api/email-template")
public class EmailTemplateController {

    @Autowired
    EmailTemplateService emailTemplateService;

    @Autowired
    EmailWildcardService emailWildcardService;

    @Autowired
    MessageResourceService messageResourceService;

    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<Object> create(@RequestBody EmailTemplateRequest request) {
        ResponseObject<Object> response = new ResponseObject<>();
        try {
            response.setResponseData(emailTemplateService.createEmailTemplate(request));
            response.setMessage(messageResourceService.getMessage(IdsmedMessageKeyConst
                    .CREATE_EMAIL_TEMPLATE_SUCCESS_MESSAGE, LangHelper.getLangCode(request.getLangCode())));

        } catch (IdsmedBaseException e) {
            response.setCode(ResponseStatusEnum.ERROR.getCode());
            response.setError(e.getError());
        }

        return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Object> getTemplateById(@PathVariable("id") Long id, @RequestParam() String langCode) {
        ResponseObject<Object> response = new ResponseObject<>();
        try {
            response.setResponseData(emailTemplateService.getTemplateById(id, langCode));
        } catch (IdsmedBaseException e) {
            response.setCode(ResponseStatusEnum.ERROR.getCode());
            response.setError(e.getError());
        }

        return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Object> getTemplateList(
            @RequestParam(required = false) String templateName,
            @RequestParam(required = false) String subject,
            @RequestParam(required = false) Integer status,
            @RequestParam(required = false) Integer pageIndex,
            @RequestParam(required = false) Integer pageSize,
            @RequestParam(required = true) String langCode) {
        ResponseObject<Object> response = new ResponseObject<>();
        try {
            EmailTemplateListResponse emailTemplateListResponse = emailTemplateService.getTemplateList(templateName, subject, status, pageIndex, pageSize, langCode);
            response.setResponseData(emailTemplateListResponse);

            if (CollectionUtils.isEmpty(emailTemplateListResponse.getEmailTemplates())) {
                response.setMessage(messageResourceService.getMessage(IdsmedMessageKeyConst.EMAIL_TEMPLATES_NOT_FOUND,
                        LangHelper.getLangCode(langCode)));
            }
        } catch (IdsmedBaseException e) {
            response.setCode(ResponseStatusEnum.ERROR.getCode());
            response.setError(e.getError());
        }

        return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
    }

    // To get email template list for dropdown option list in email notification config
    @RequestMapping(value="/list", method = RequestMethod.GET)
    @ResponseBody
    public  ResponseEntity<Object> getTemplateList(@RequestParam(required = true) String langCode) {
        ResponseObject<Object> response = new ResponseObject<>();
        try {
            EmailTemplateListResponse emailTemplateListResponse = emailTemplateService.getEmailTemplates();
            response.setResponseData(emailTemplateListResponse);
            if (emailTemplateListResponse.getEmailTemplates().size() == 0) {
                response.setMessage(messageResourceService.getMessage(IdsmedMessageKeyConst.EMAIL_TEMPLATES_NOT_FOUND,
                        LangHelper.getLangCode(langCode)));
            }
        } catch (IdsmedBaseException e) {
            response.setCode(ResponseStatusEnum.ERROR.getCode());
            response.setError(e.getError());
        }

        return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<Object> edit(@RequestBody EmailTemplateRequest request) {
        ResponseObject<Object> response = new ResponseObject<>();
        try {
            response.setResponseData(emailTemplateService.editEmailTemplate(request));
            response.setMessage(messageResourceService.getMessage(IdsmedMessageKeyConst
                    .EDIT_EMAIL_TEMPLATE_SUCCESS_MESSAGE, LangHelper.getLangCode(request.getLangCode())));

        } catch (IdsmedBaseException e) {
            response.setCode(ResponseStatusEnum.ERROR.getCode());
            response.setError(e.getError());
        }

        return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
    }

    @RequestMapping(value = "/inactive-email-template", method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<Object> inactiveEmailTemplate(@RequestBody EmailTemplateRequest request) {
        ResponseObject<Object> response = new ResponseObject<>();

        try {
            response.setResponseData(emailTemplateService.inactiveEmailTemplate(request));
            response.setMessage(messageResourceService.getMessage(IdsmedMessageKeyConst
                    .INACTIVE_EMAIL_TEMPLATE_SUCCESS_MESSAGE, LangHelper.getLangCode(request.getLangCode())));
        } catch (IdsmedBaseException e) {
            response.setCode(ResponseStatusEnum.ERROR.getCode());
            response.setError(e.getError());
        }

        return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
    }

    @RequestMapping(value = "/wildcard", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Object> getWildcardList(@RequestParam String langCode) {
        ResponseObject<Object> response = new ResponseObject<>();

        try {
            response.setResponseData(emailWildcardService.getEmailWildcardList(langCode));
            response.setMessage(messageResourceService.getMessage(IdsmedMessageKeyConst
                    .GET_EMAIL_WILDCARD_SUCCESS_MESSAGE, LangHelper.getLangCode(langCode)));
        } catch (IdsmedBaseException e) {
            response.setCode(ResponseStatusEnum.ERROR.getCode());
            response.setError(e.getError());
        }

        return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
    }
}

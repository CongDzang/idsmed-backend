package com.cmg.idsmed.api.error;

import com.cmg.idsmed.common.enums.ResponseStatusEnum;
import com.cmg.idsmed.common.exception.IdsmedBaseException;
import com.cmg.idsmed.dto.ResponseObject;
import com.cmg.idsmed.dto.error.LogErrorApiRequest;
import com.cmg.idsmed.service.error.LogErrorApiService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(value = "api/log")
public class LogErrorApiController
{
    private static final Logger logger = LoggerFactory.getLogger(LogErrorApiController.class);

    @Autowired
    private LogErrorApiService logErrorApiService;

    @RequestMapping(value="/save", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<Object> saveApiError(@RequestBody LogErrorApiRequest request, HttpServletRequest req)
    {
        logger.info("LogErrorApiController.saveApiError");
        logger.debug("LogErrorApiController.saveApiError request = " + request.toString());
        ResponseObject<Object> response = new ResponseObject<>();

        try
        {
            logErrorApiService.saveLogError(request.getLogin(), request.getErrorCode(), request.getDescription(),
                                            request.getUrl(), req.getRemoteAddr(), request.getBrowser(), request.getOs());
            response.setCode(ResponseStatusEnum.SUCCESS.getCode());
        }
        catch (IdsmedBaseException e)
        {
            response.setCode(ResponseStatusEnum.ERROR.getCode());
            response.setError(e.getError());
        }
        return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
    }
}
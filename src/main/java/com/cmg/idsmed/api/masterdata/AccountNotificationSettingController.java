package com.cmg.idsmed.api.masterdata;

import com.cmg.idsmed.common.enums.ResponseStatusEnum;
import com.cmg.idsmed.common.exception.IdsmedBaseException;
import com.cmg.idsmed.dto.ResponseObject;
import com.cmg.idsmed.service.masterdata.AccountNotificationSettingService;
import com.cmg.idsmed.service.masterdata.ProductBrandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping(value = "api/masterdata/account-notification-setting")
public class AccountNotificationSettingController {
	@Autowired
	AccountNotificationSettingService accountNotificationSettingService;

    @RequestMapping(method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Object> getAccountNotificationSettingConstantMap(@RequestParam(required = false) Integer type, @RequestParam(required = false) String loginId, @RequestParam String langCode) {
		ResponseObject<Object> response = new ResponseObject<>();
		try {
			response.setResponseData(accountNotificationSettingService.getAccountNotificationSettingConstantMap(type, loginId, langCode));
		} catch (IdsmedBaseException e) {
			response.setCode(ResponseStatusEnum.ERROR.getCode());
			response.setError(e.getError());
		}

		return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
	}
}

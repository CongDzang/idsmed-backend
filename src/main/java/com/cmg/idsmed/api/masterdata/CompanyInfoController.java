package com.cmg.idsmed.api.masterdata;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.cmg.idsmed.common.enums.ResponseStatusEnum;
import com.cmg.idsmed.common.exception.IdsmedBaseException;
import com.cmg.idsmed.dto.ResponseObject;
import com.cmg.idsmed.service.vendor.VendorService;


@RestController
@RequestMapping(value = "api/company")
public class CompanyInfoController {

    @Autowired
    private VendorService vendorService;

	@RequestMapping(value = "/{companyCode}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Object> getCompanyBackgroundInfo(@PathVariable("companyCode") String companyCode) {
		ResponseObject<Object> response = new ResponseObject<>();
		try {
			response.setResponseData(vendorService.getComapanyBackgroundInfo(companyCode));
		} catch (IdsmedBaseException e) {
			response.setCode(ResponseStatusEnum.ERROR.getCode());
			response.setError(e.getError());
		}

		return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
	}
}

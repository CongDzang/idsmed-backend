package com.cmg.idsmed.api.masterdata;

import com.cmg.idsmed.common.Constant.IdsmedMessageKeyConst;
import com.cmg.idsmed.common.enums.ResponseStatusEnum;
import com.cmg.idsmed.common.exception.IdsmedBaseException;
import com.cmg.idsmed.common.utils.LangHelper;
import com.cmg.idsmed.dto.ResponseObject;
import com.cmg.idsmed.dto.masterdata.EmailProviderRequest;
import com.cmg.idsmed.service.masterdata.*;
import com.cmg.idsmed.service.share.MessageResourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "api/masterdata")
public class MasterDataController {
	@Autowired
	private MasterDataService masterDataService;

	@Autowired
	private ProductBrandService productBrandService;

	@Autowired
	private CareAreaService careAreaService;

	@Autowired
	private ProductCategoryService productCategoryService;

	@Autowired
	private ProductSecondCategoryService productSecondCategoryService;

	@Autowired
	private MessageResourceService messageResourceService;

	private static final Integer PAGE_INDEX_DEFAULT = 0;
	private static final Integer PAGE_SIZE_DEFAULT = 100;

	@RequestMapping(value = "/verifying-work-flow-setting", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Object> getVerifyingWorkFlowSetting(@RequestParam() String countryCode,
															  @RequestParam(required = false) String langCode) {
		ResponseObject<Object> response = new ResponseObject<>();
		try {
			response.setResponseData(masterDataService.getVerifyingWorkFlowSettingByCountryCode(countryCode, langCode));
		} catch (IdsmedBaseException e) {
			response.setCode(ResponseStatusEnum.ERROR.getCode());
			response.setError(e.getError());
		}

		return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
	}

	@RequestMapping(value = "/currency-setting", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Object> getCurrencySettingByCountryCode(@RequestParam() String countryCode,
															  @RequestParam(required = false) String langCode) {
		ResponseObject<Object> response = new ResponseObject<>();
		try {
			response.setResponseData(masterDataService.getCurrencySettingByCountryCode(countryCode, langCode));
		} catch (IdsmedBaseException e) {
			response.setCode(ResponseStatusEnum.ERROR.getCode());
			response.setError(e.getError());
		}

		return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
	}


	@RequestMapping(value = "/productbrand", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Object> getProductBrandList(@RequestParam(required = false) Integer pageIndex, @RequestParam(required = false) Integer pageSize) {

		pageIndex = pageIndex == null ? PAGE_INDEX_DEFAULT : (pageIndex - 1 < 0 ? 0 : pageIndex - 1);
		pageSize = pageSize == null ? PAGE_SIZE_DEFAULT : pageSize;


		ResponseObject<Object> response = new ResponseObject<>();
		try {
			response.setResponseData(productBrandService.getProductBrandList(pageIndex, pageSize));
		} catch (IdsmedBaseException e) {
			response.setCode(ResponseStatusEnum.ERROR.getCode());
			response.setError(e.getError());
		}

		return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
	}

	@RequestMapping(value = "/productbrand/favourite", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Object> getFavouriteBrandList(@RequestParam(required = true) Long userId) {
		ResponseObject<Object> response = new ResponseObject<>();
		try {
			response.setResponseData(productBrandService.getFavouriteProductBrandList(userId));
		} catch (IdsmedBaseException e) {
			response.setCode(ResponseStatusEnum.ERROR.getCode());
			response.setError(e.getError());
		}

		return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
	}

	@RequestMapping(value = "/productbrand/product-favourite-brand", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Object> getFavouriteBrand(@RequestParam(required = true) String userId) {
		ResponseObject<Object> response = new ResponseObject<>();
		try {
			response.setResponseData(productBrandService.getFavouriteBrand(userId));
		} catch (IdsmedBaseException e) {
			response.setCode(ResponseStatusEnum.ERROR.getCode());
			response.setError(e.getError());
		}

		return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
	}


	@RequestMapping(value = "/carearea", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Object> getCareAreaList(@RequestParam(required = false) Integer pageIndex,
												  @RequestParam(required = false) Integer pageSize,
												  @RequestParam(required = false) String langCode) {
		ResponseObject<Object> response = new ResponseObject<>();
		try {
			response.setResponseData(careAreaService.getCareAreaList(pageIndex, pageSize, langCode));
		} catch (IdsmedBaseException e) {
			response.setCode(ResponseStatusEnum.ERROR.getCode());
			response.setError(e.getError());
		}

		return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
	}

	@RequestMapping(value = "/carearea/favourite", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Object> getFavouriteCareAreaList(@RequestParam(required = true) Long userId,
														   @RequestParam(required = false) String langCode) {
		ResponseObject<Object> response = new ResponseObject<>();
		try {
			response.setResponseData(careAreaService.getFavouriteCareAreaList(userId, langCode));
		} catch (IdsmedBaseException e) {
			response.setCode(ResponseStatusEnum.ERROR.getCode());
			response.setError(e.getError());
		}

		return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/carearea/list")
	@ResponseBody
	public ResponseEntity<Object> getCareAreaList(@RequestParam() String langCode) {
		ResponseObject<Object> response = new ResponseObject<>();
		try {
			response.setResponseData(careAreaService.getCareAreaList(langCode));
		} catch (IdsmedBaseException e) {
			response.setCode(ResponseStatusEnum.ERROR.getCode());
			response.setError(e.getError());
		}

		return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
	}

	@RequestMapping(value = "/productcategory", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Object> getProductCategoryList(@RequestParam(required = false) Integer pageIndex, @RequestParam(required = false) Integer pageSize) {

		pageIndex = pageIndex == null ? PAGE_INDEX_DEFAULT : (pageIndex - 1 < 0 ? 0 : pageIndex - 1);
		pageSize = pageSize == null ? PAGE_SIZE_DEFAULT : pageSize;


		ResponseObject<Object> response = new ResponseObject<>();
		try {
			response.setResponseData(productCategoryService.getProductCategoryList(pageIndex, pageSize));
		} catch (IdsmedBaseException e) {
			response.setCode(ResponseStatusEnum.ERROR.getCode());
			response.setError(e.getError());
		}

		return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
	}

	@RequestMapping(value = "/productsecondcategory", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Object> getProductSecondCategoryList(@RequestParam(required = false) Integer pageIndex, @RequestParam(required = false) Integer pageSize) {

		pageIndex = pageIndex == null ? PAGE_INDEX_DEFAULT : (pageIndex - 1 < 0 ? 0 : pageIndex - 1);
		pageSize = pageSize == null ? PAGE_SIZE_DEFAULT : pageSize;

		ResponseObject<Object> response = new ResponseObject<>();
		try {
			response.setResponseData(productSecondCategoryService.getProductSecondCategoryList(pageIndex, pageSize));
		} catch (IdsmedBaseException e) {
			response.setCode(ResponseStatusEnum.ERROR.getCode());
			response.setError(e.getError());
		}

		return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
	}

	@RequestMapping(value = "/email-provider", method = RequestMethod.POST)
	@ResponseBody
	@PreAuthorize("@PermissionService.isMethodPermited(authentication, {'SCF0000001'})")
	public ResponseEntity<Object> changeEmailProvider(@RequestBody() EmailProviderRequest request) {
		ResponseObject<Object> response = new ResponseObject<>();
		try {
			response.setResponseData(masterDataService.changeEmailProvider(request));
			response.setMessage(messageResourceService.getMessage(IdsmedMessageKeyConst.CHANGE_EMAIL_PROVIDER_SUCCESS_MESSAGE, LangHelper.getLangCode(request.getLangCode())));
		} catch (IdsmedBaseException e) {
			response.setCode(ResponseStatusEnum.ERROR.getCode());
			response.setError(e.getError());
		}

		return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
	}

	@RequestMapping(value = "/email-provider", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Object> getCurrentEmailProvider( @RequestParam() String langCode) {
		ResponseObject<Object> response = new ResponseObject<>();
		try {
			response.setResponseData(masterDataService.getEmailProvider(langCode));
		} catch (IdsmedBaseException e) {
			response.setCode(ResponseStatusEnum.ERROR.getCode());
			response.setError(e.getError());
		}

		return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
	}
}

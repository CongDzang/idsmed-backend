package com.cmg.idsmed.api.notification;

import com.cmg.idsmed.common.enums.ResponseStatusEnum;
import com.cmg.idsmed.common.exception.IdsmedBaseException;
import com.cmg.idsmed.dto.ResponseObject;
import com.cmg.idsmed.dto.notification.NotificationsUpdateStatusRequest;
import com.cmg.idsmed.service.share.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "api/notification")
public class  NotificationController {

	@Autowired
	private NotificationService notificationService;

	@RequestMapping(method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Object> getAllNotificationByAccount(@RequestParam() String loginId,
															  @RequestParam() String langCode) {
		ResponseObject<Object> response = new ResponseObject<>();
		try {
			response.setResponseData(notificationService.getAllNotArchivedNotificationByAccount(loginId, langCode));
		} catch(IdsmedBaseException e) {
			response.setCode(ResponseStatusEnum.ERROR.getCode());
			response.setError(e.getError());
		}

		return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
	}

	/**
	 *
	 * @param notificationIds
	 * @param loginId
	 * @param status    CREATE(0, "created"),
	 * 					SENT(1, "sent"),
				 * 		ERROR(2, "error"),
				 * 		DELIVERED(3, "delivered"),
	 * 					READ(4, "read"),
	 * 					ARCHIVED(5, "archived");
	 * 					Go to NotificationRecipient to see in details
	 * @return
	 */
	@RequestMapping(value = "/notification-recipient", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseEntity<Object> updateNotificationRecipientsStatus(@RequestBody() NotificationsUpdateStatusRequest request) {

		ResponseObject<Object> response = new ResponseObject<>();
		try {
			response.setResponseData(notificationService.updateNotificationRecipientsStatus(request));
		} catch(IdsmedBaseException e) {
			response.setCode(ResponseStatusEnum.ERROR.getCode());
			response.setError(e.getError());
		}

		return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);

	}
}


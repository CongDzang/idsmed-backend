package com.cmg.idsmed.api.procurement;

import com.cmg.idsmed.common.enums.ResponseStatusEnum;
import com.cmg.idsmed.common.exception.IdsmedBaseException;
import com.cmg.idsmed.dto.ResponseObject;
import com.cmg.idsmed.service.procurement.ProcurementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "api/procurement")
public class ProcurementController {

    @Autowired
    ProcurementService procurementService;

    @RequestMapping(value = "/small-order", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Object> getSmallOrderList(@RequestParam(required = false) Integer pageIndex,
                                                    @RequestParam(required = false) Integer pageSize,
                                                    @RequestParam String loginId,
                                                    @RequestParam String langCode) {
        ResponseObject<Object> response = new ResponseObject<>();

        try {
            response.setResponseData(procurementService.getSmallOrderList(pageIndex, pageSize, loginId, langCode));
        } catch (IdsmedBaseException e) {
            response.setCode(ResponseStatusEnum.ERROR.getCode());
            response.setError(e.getError());
        }

        return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
    }

    @RequestMapping(value = "/small-order/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Object> getSmallOrderById(@PathVariable("id") Long id,
                                                    @RequestParam String langCode) {
        ResponseObject<Object> response = new ResponseObject<>();

        try {
            response.setResponseData(procurementService.getSmallOrderById(id, langCode));
        } catch (IdsmedBaseException e) {
            response.setCode(ResponseStatusEnum.ERROR.getCode());
            response.setError(e.getError());
        }

        return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
    }

}

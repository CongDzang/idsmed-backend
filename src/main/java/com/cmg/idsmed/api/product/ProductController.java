package com.cmg.idsmed.api.product;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import com.cmg.idsmed.common.enums.LangEnum;
import com.cmg.idsmed.common.utils.LangHelper;
import com.cmg.idsmed.dto.masterdata.CountryProvincePairRequest;
import com.cmg.idsmed.dto.product.*;
import com.cmg.idsmed.dto.product.comparison.ProductComparisonResponse;
import com.cmg.idsmed.dto.product.config.ProductInfoConfigurationListResponse;
import com.cmg.idsmed.dto.product.config.ProductInfoConfigurationRequest;
import com.cmg.idsmed.dto.product.config.ProductInfoConfigurationResponse;
import com.cmg.idsmed.dto.product.config.ProductLabelConfigurationRequest;
import com.cmg.idsmed.service.product.ProductCSVService;
import com.cmg.idsmed.service.product.config.ProductInfoConfigurationService;
import org.apache.poi.util.SystemOutLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.cmg.idsmed.common.Constant.IdsmedMessageKeyConst;
import com.cmg.idsmed.common.enums.CountryCodeEnum;
import com.cmg.idsmed.common.enums.ResponseStatusEnum;
import com.cmg.idsmed.common.exception.IdsmedBaseException;
import com.cmg.idsmed.dto.ResponseObject;
import com.cmg.idsmed.service.masterdata.ProductBrandService;
import com.cmg.idsmed.service.product.ProductExcelService;
import com.cmg.idsmed.service.product.ProductService;
import com.cmg.idsmed.service.share.MessageResourceService;

@RestController
@RequestMapping(value = "api/product")
public class ProductController {
    @Autowired
    private ProductService productService;

    @Autowired
    private ProductExcelService productExcelService;

    @Autowired
    private ProductBrandService productBrandService;

    @Autowired
    private MessageResourceService messageResourceService;

    @Autowired
    private ProductInfoConfigurationService productInfoConfigurationService;

    @Autowired
    private ProductCSVService productCSVService;


//    @RequestMapping(method = RequestMethod.GET)
//    @ResponseBody
//    public ResponseEntity<Object> getProductList(@RequestParam(required = false) Integer pageIndex,
//                                                 @RequestParam(required = false) Integer pageSize,
//                                                 @RequestParam(required = true) String langCode) {
//        ResponseObject<Object> response = new ResponseObject<>();
//        try {
//            ProductListResponse productListResponse = productService.getProductList(pageIndex, pageSize, langCode);
//            response.setResponseData(productListResponse);
//            if (CollectionUtils.isEmpty(productListResponse.getProductList()))
//                response.setMessage(messageResourceService.getMessage(IdsmedMessageKeyConst.PRODUCT_NOT_FOUND, LangHelper.getLangCode(langCode)));
//        } catch (IdsmedBaseException e) {
//            response.setCode(ResponseStatusEnum.ERROR.getCode());
//            response.setError(e.getError());
//        }
//
//        return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
//    }

    @RequestMapping(value = "/newRecommend", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Object> getProductListOfNewRecommend(@RequestParam(required = true) Integer duration,
                                                               @RequestParam(required = false) Integer pageIndex,
                                                               @RequestParam(required = false) Integer pageSize,
                                                               @RequestParam(required = true) String langCode) {
        ResponseObject<Object> response = new ResponseObject<>();
        try {
            ProductListResponse productListResponse = productService.getProductListOfNewRecommend(duration, pageIndex, pageSize, langCode);
            response.setResponseData(productListResponse);
            if (CollectionUtils.isEmpty(productListResponse.getProductList()))
                response.setMessage(messageResourceService.getMessage(IdsmedMessageKeyConst.PRODUCT_NOT_FOUND, LangHelper.getLangCode(langCode)));
        } catch (IdsmedBaseException e) {
            response.setCode(ResponseStatusEnum.ERROR.getCode());
            response.setError(e.getError());
        }

        return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Object> getProductListForVendor(@RequestParam(required = false) Long vendorId,
                                                          @RequestParam(required = false) String productNamePrimary,
                                                          @RequestParam(required = false) Integer status,
                                                          @RequestParam(required = false) String productCode,
                                                          @RequestParam(required = false) String productModel,
                                                          @RequestParam(required = false) String companyNamePrimary,
                                                          @RequestParam(required = false) Integer vendorStatus,
                                                          @RequestParam(required = false) Long vendorCoordinatorId,
                                                          @RequestParam(required = false) Integer pageIndex,
                                                          @RequestParam(required = false) Integer pageSize,
                                                          @RequestParam(required = true) String langCode) {
        ResponseObject<Object> response = new ResponseObject<>();
        try {
            ProductListResponse productListResponse = productService.getProductListForVendor(vendorId, productNamePrimary, status, productCode, productModel, companyNamePrimary, vendorStatus, vendorCoordinatorId, pageIndex, pageSize);
            response.setResponseData(productListResponse);
            if (CollectionUtils.isEmpty(productListResponse.getProductList())) {
                response.setMessage(messageResourceService.getMessage(IdsmedMessageKeyConst.PRODUCT_NOT_FOUND, LangHelper.getLangCode(langCode)));
            }
        } catch (IdsmedBaseException e) {
            response.setCode(ResponseStatusEnum.ERROR.getCode());
            response.setError(e.getError());
        }

        return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
    }


    @RequestMapping(method = RequestMethod.POST, consumes = "multipart/form-data")
    @ResponseBody
    public ResponseEntity<Object> create(@RequestPart("productRequest") ProductRequest request,
                                         @RequestPart("videoUpload") Optional<MultipartFile> videoFile,
                                         @RequestPart("brochureUpload") Optional<MultipartFile> brochureFile,
                                         @RequestPart("imagesUpload") List<MultipartFile> imageFiles,
                                         @RequestPart("documentAttachmentUpload") List<MultipartFile> productDocumentAttachments) {
        ResponseObject<Object> response = new ResponseObject<>();
        try {
            response.setResponseData(productService.create(request, brochureFile.orElse(null), videoFile.orElse(null), imageFiles, productDocumentAttachments));
            response.setMessage(messageResourceService.getMessage(IdsmedMessageKeyConst.CREATE_PRODUCT_SUCCESS_MESSAGE, LangHelper.getLangCode(request.getLangCode())));
        } catch (IdsmedBaseException e) {
            response.setCode(ResponseStatusEnum.ERROR.getCode());
            response.setError(e.getError());
        }
        return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Object> getProductById(@PathVariable("id") Long id, @RequestParam(required = false, value = "loginId") String loginId,  @RequestParam String langCode) {
        ResponseObject<Object> response = new ResponseObject<>();
        try {
            ProductResponse res = null;
            if(!StringUtils.isEmpty(loginId)) {
                res = productService.getProductById(id, loginId, langCode);
            }
            response.setResponseData(res);
            response.setMessage(messageResourceService.getMessage(IdsmedMessageKeyConst.GET_PRODUCT_DETAILS_SUCCESS,
                    LangHelper.getLangCode(langCode)));
        } catch (IdsmedBaseException e) {
            response.setCode(ResponseStatusEnum.ERROR.getCode());
            response.setError(e.getError());
            return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
    }

    /**
     * @param keyword
     * @param careAreaIds   : list of id of ProductCareArea
     * @param categoryIds   : list of id of ProductCategory
     * @param brandIds      : list of id of ProductBrand
     * @param type      : defined by BEST_SELLER_SEARCH_TYPE = hot
     *                      but now this feature hasn't been implemented
     * @param pageIndex
     * @param pageSize
     * @param langCode
     * @param sortFieldCode : Defined by this ProductCustomRepositoryImpl.FuzzySearchProductSortFieldEnum
     * @param sortOrder:    pass string "asc" or "desc"
     * @return
     */
    @RequestMapping(value = "/search", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Object> getProductSearchByKey(@RequestParam(required = false) String keyword,
                                                        @RequestParam(required = false) List<Long> careAreaIds,
                                                        @RequestParam(required = false) List<Long> categoryIds,
                                                        @RequestParam(required = false) List<Long> brandIds,
                                                        @RequestParam(required = false) String type,
                                                        @RequestParam(required = false) Integer pageIndex,
                                                        @RequestParam(required = false) Integer pageSize,
                                                        @RequestParam(required = false) String langCode,
                                                        @RequestParam(required = false) Integer sortFieldCode,
                                                        @RequestParam(required = false) String sortOrder,
                                                        @RequestParam(required = false) String loginId,
                                                        @RequestParam(required = false) BigDecimal minPrice,
                                                        @RequestParam(required = false) BigDecimal maxPrice,
                                                        @RequestParam(required = false) Integer productType,
                                                        @RequestParam(required = false) String manufacturerCountryCode,
                                                        @RequestParam(required = false) String manufacturerProvinceCode,
                                                        @RequestParam(required = false) List<Long> secondCategoryIds) {
        ResponseObject<Object> response = new ResponseObject<>();
        try {
            ProductListResponse productListResponse = productService.searchProductByKeyword(keyword, careAreaIds, categoryIds
                        , brandIds, type, pageIndex, pageSize
                        , sortFieldCode, sortOrder, loginId, minPrice, maxPrice, productType, manufacturerCountryCode, manufacturerProvinceCode, secondCategoryIds, langCode);
            response.setResponseData(productListResponse);
            if (CollectionUtils.isEmpty(productListResponse.getProductList())) {
                response.setMessage(messageResourceService.getMessage(IdsmedMessageKeyConst.PRODUCT_NOT_FOUND, LangHelper.getLangCode(langCode)));
            }
        } catch (IdsmedBaseException e) {
            response.setCode(ResponseStatusEnum.ERROR.getCode());
            response.setError(e.getError());
        }
        return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
    }

    @RequestMapping(value = "/edit", method = RequestMethod.POST, consumes = "multipart/form-data")
    @ResponseBody
    public ResponseEntity<Object> edit(@RequestPart("productRequest") ProductRequest request,
                                       @RequestPart("videoUpload") Optional<MultipartFile> videoFile,
                                       @RequestPart("brochureUpload") Optional<MultipartFile> brochureFile,
                                       @RequestPart("imagesUpload") List<MultipartFile> imageFiles,
                                       @RequestPart("documentAttachmentUpload") List<MultipartFile> productDocumentAttachments) {
        ResponseObject<Object> response = new ResponseObject<>();
        try {
            response.setResponseData(productService.updateDraftOfPendingStatus(request, brochureFile.orElse(null), videoFile.orElse(null), imageFiles, productDocumentAttachments));
            response.setMessage(messageResourceService.getMessage(IdsmedMessageKeyConst.EDIT_PRODUCT_SUCCESS_MESSAGE, LangHelper.getLangCode(request.getLangCode())));
        } catch (IdsmedBaseException e) {
            response.setCode(ResponseStatusEnum.ERROR.getCode());
            response.setError(e.getError());
        }
        return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
    }

    @RequestMapping(value = "/review", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<Object> updatePendingApproveStatus(@RequestBody ProductRequest request) {
        ResponseObject<Object> response = new ResponseObject<>();
        try {
            response.setResponseData(productService.updatePendingApproveStatus(request));
            response.setMessage(messageResourceService.getMessage(IdsmedMessageKeyConst.REVIEW_PRODUCT_SUCCESS_MESSAGE, LangHelper.getLangCode(request.getLangCode())));
        } catch (IdsmedBaseException e) {
            response.setCode(ResponseStatusEnum.ERROR.getCode());
            response.setError(e.getError());
        }
        return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
    }

    @RequestMapping(value = "/approve", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<Object> updateApproveStatus(@RequestBody ProductRequest request) {
        ResponseObject<Object> response = new ResponseObject<>();
        try {
            response.setResponseData(productService.updateApproveStatus(request));
            response.setMessage(messageResourceService.getMessage(IdsmedMessageKeyConst.APPROVE_PRODUCT_SUCCESS_MESSAGE, LangHelper.getLangCode(request.getLangCode())));
        } catch (IdsmedBaseException e) {
            response.setCode(ResponseStatusEnum.ERROR.getCode());
            response.setError(e.getError());
        }
        return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
    }

    @RequestMapping(value = "/reject", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<Object> updateRejectStatus(@RequestBody ProductRequest request) {
        ResponseObject<Object> response = new ResponseObject<>();
        try {
            response.setResponseData(productService.updateRejectStatus(request));
            response.setMessage(messageResourceService.getMessage(IdsmedMessageKeyConst.REJECT_PRODUCT_SUCCESS_MESSAGE, LangHelper.getLangCode(request.getLangCode())));
        } catch (IdsmedBaseException e) {
            response.setCode(ResponseStatusEnum.ERROR.getCode());
            response.setError(e.getError());
        }
        return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
    }

    @RequestMapping(value = "/add-product-info", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Object> getProductAddInfoDetail(@RequestParam String langCode) {
        ResponseObject<Object> response = new ResponseObject<>();
        try {
            response.setResponseData(productService.getProductAddInfoDetail(langCode));
        } catch (IdsmedBaseException e) {
            response.setCode(ResponseStatusEnum.ERROR.getCode());
            response.setError(e.getError());
        }
        return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
    }

    @RequestMapping(value = "/product-csv", method = RequestMethod.POST, consumes = "multipart/form-data")
    @ResponseBody
    public ResponseEntity<Object> uploadProductCSV(@RequestPart("productCSVUploadRequest") ProductCSVUploadRequest request
            , @RequestPart("uploadFile") MultipartFile file) {
        ResponseObject<Object> response = new ResponseObject<>();
        try {
            response.setResponseData(productCSVService.uploadProductCSVIntoOss(request, file));
            response.setMessage(messageResourceService.getMessage(IdsmedMessageKeyConst.UPLOAD_PRODUCT_CSV_FILE_SUCCESS_MESSAGE, LangHelper.getLangCode(request.getLangCode())));
        } catch (IdsmedBaseException e) {
            response.setCode(ResponseStatusEnum.ERROR.getCode());
            response.setError(e.getError());
        }
        return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
    }

    @RequestMapping(value = "/product-excel", method = RequestMethod.POST, consumes = "multipart/form-data")
    @ResponseBody
    public ResponseEntity<Object> uploadProductExcel(@RequestPart("productExcelUploadRequest") ProductExcelUploadRequest request
            , @RequestPart("uploadFile") MultipartFile file) {
        ResponseObject<Object> response = new ResponseObject<>();
        try {
            response.setResponseData(productExcelService.uploadProductExcelIntoOss(request, file));
            response.setMessage(messageResourceService.getMessage(IdsmedMessageKeyConst.UPLOAD_PRODUCT_CSV_FILE_SUCCESS_MESSAGE, LangHelper.getLangCode(request.getLangCode())));
        } catch (IdsmedBaseException e) {
            response.setCode(ResponseStatusEnum.ERROR.getCode());
            response.setError(e.getError());
        }
        return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
    }

    @RequestMapping(value = "/search-fields-info", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Object> getProductSearchFieldsInfo(@RequestParam(required = false) List<Long> vendorIds) {
        ResponseObject<Object> response = new ResponseObject<>();
        try {
            response.setResponseData(productService.getProductListSearchFields(vendorIds));
        } catch (IdsmedBaseException e) {
            response.setCode(ResponseStatusEnum.ERROR.getCode());
            response.setError(e.getError());
        }

        return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
    }


    @RequestMapping(value = "/product-favourite-brand", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Object> getFavouriteBrandList(@RequestParam(required = true) String userId) {
        ResponseObject<Object> response = new ResponseObject<>();
        try {
            response.setResponseData(productBrandService.getFavouriteBrand(userId));
        } catch (IdsmedBaseException e) {
            response.setCode(ResponseStatusEnum.ERROR.getCode());
            response.setError(e.getError());
        }

        return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
    }

    /**
     * @param countryId  pass null when you want to get all list
     * @param provinceId pass null when you want to get all list or you want to get only config for country
     * @param status     pass null if you want to get both status
     *                   ACTIVE(1, "active"),
     *                   INACTIVE(10, "inactive");
     * @param pageIndex
     * @param pageSize
     * @param langCode
     * @return
     */
    @RequestMapping(value = "/configuration", method = RequestMethod.GET)
    @ResponseBody
//    @PreAuthorize("@PermissionService.isMethodPermited(authentication, {'PC00000001'})")
    public ResponseEntity<Object> getProductInfoConfigurationList(@RequestParam(required = false) Long countryId,
                                                                      @RequestParam(required = false) Long provinceId,
                                                                      @RequestParam(required = false) Integer status,
                                                                      @RequestParam(required = false) Integer pageIndex,
                                                                      @RequestParam(required = false) Integer pageSize,
                                                                      @RequestParam(required = false) String langCode) {
        ResponseObject<Object> response = new ResponseObject<>();
        try {
            ProductInfoConfigurationListResponse productInfoConfigurationListResponse = productInfoConfigurationService.getAllProductInfoConfiguration(countryId, provinceId, status, pageIndex, pageSize, langCode);

            if (CollectionUtils.isEmpty(productInfoConfigurationListResponse.getProductInfoConfigs())) {
                response.setMessage(messageResourceService.getMessage(IdsmedMessageKeyConst.PRODUCT_CONFIGURATION_NOT_FOUND, LangHelper.getLangCode(langCode)));
            }
            response.setResponseData(productInfoConfigurationListResponse);
        } catch (IdsmedBaseException e) {
            response.setCode(ResponseStatusEnum.ERROR.getCode());
            response.setError(e.getError());
        }

        return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
    }

    @RequestMapping(value = "/configuration/{id}", method = RequestMethod.GET)
    @ResponseBody
//    @PreAuthorize("@PermissionService.isMethodPermited(authentication, {'API0000001', 'API0000002', 'API0000002'})")
    public ResponseEntity<Object> getProductInfoConfigurationById(@PathVariable("id") Long id,
                                                              @RequestParam(required = false) String langCode) {
        ResponseObject<Object> response = new ResponseObject<>();
        try {
            ProductInfoConfigurationResponse productInfoConfigurationResponse = productInfoConfigurationService.getProductInfoConfigurationById(id);
            if (productInfoConfigurationResponse == null) {
                response.setMessage(messageResourceService.getMessage(IdsmedMessageKeyConst.PRODUCT_CONFIGURATION_NOT_FOUND, LangHelper.getLangCode(langCode)));
            }
            response.setResponseData(productInfoConfigurationResponse);
        } catch (IdsmedBaseException e) {
            response.setCode(ResponseStatusEnum.ERROR.getCode());
            response.setError(e.getError());
        }

        return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
    }

    @RequestMapping(value = "/configuration", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<Object> createProductInfoConfig(@RequestBody ProductInfoConfigurationRequest productInfoConfigurationRequest) {
        ResponseObject<Object> response = new ResponseObject<>();
        try {
            ProductInfoConfigurationResponse productInfoConfigurationResponse = productInfoConfigurationService.createProductConfiguration(productInfoConfigurationRequest);
            if (productInfoConfigurationResponse == null) {
                response.setMessage(messageResourceService.getMessage(IdsmedMessageKeyConst.PRODUCT_CONFIGURATION_NOT_FOUND, LangHelper.getLangCode(productInfoConfigurationRequest.getLangCode())));
            }
            response.setResponseData(productInfoConfigurationResponse);
        } catch (IdsmedBaseException e) {
            response.setCode(ResponseStatusEnum.ERROR.getCode());
            response.setError(e.getError());
        }

        return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
    }

    @RequestMapping(value = "/configuration", method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<Object> editProductInfoConfig(@RequestBody ProductInfoConfigurationRequest productInfoConfigurationRequest) {
        ResponseObject<Object> response = new ResponseObject<>();
        try {
            ProductInfoConfigurationResponse productInfoConfigurationResponse = productInfoConfigurationService.editProductConfiguration(productInfoConfigurationRequest);
            if (productInfoConfigurationResponse == null) {
                response.setMessage(messageResourceService.getMessage(IdsmedMessageKeyConst.PRODUCT_CONFIGURATION_NOT_FOUND, LangHelper.getLangCode(productInfoConfigurationRequest.getLangCode())));
            }
            response.setResponseData(productInfoConfigurationResponse);
        } catch (IdsmedBaseException e) {
            response.setCode(ResponseStatusEnum.ERROR.getCode());
            response.setError(e.getError());
        }

        return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
    }

    @RequestMapping(value = "/configuration/label-config", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Object> getProductLabelConfigs(@RequestParam(required = false) Long vendorId
            , @RequestParam(required = false) Integer status
            , @RequestParam(required = false) String langCode) {

        ResponseObject<Object> response = new ResponseObject<>();
        try {
            response.setResponseData(productInfoConfigurationService.getProductLabelConfigs(vendorId, status, langCode));
        } catch (IdsmedBaseException e) {
            response.setCode(ResponseStatusEnum.ERROR.getCode());
            response.setError(e.getError());
        }

        return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.PUT, consumes = "multipart/form-data")
    @ResponseBody
    public ResponseEntity<Object> editProductDocumentAttachment(@RequestPart("ProductDocumentAttachmentEditRequest") ProductDocumentAttachmentEditRequest productDocumentAttachmentEditRequest
            , @RequestPart("documentAttachmentUpload") List<MultipartFile> productDocumentAttachments) {
        ResponseObject<Object> response = new ResponseObject<>();
        try {
            response.setResponseData(productService.editProductDocumentAttachment(productDocumentAttachmentEditRequest, productDocumentAttachments));
            response.setMessage(messageResourceService.getMessage(IdsmedMessageKeyConst.EDIT_PRODUCT_DOCUMENT_ATTACHMENT_SUCCESS_MESSAGE, LangHelper.getLangCode(productDocumentAttachmentEditRequest.getLangCode())));
        } catch (IdsmedBaseException e) {
            response.setCode(ResponseStatusEnum.ERROR.getCode());
            response.setError(e.getError());
        }

        return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
    }

    @RequestMapping(value = "/product-rating", method = RequestMethod.POST, consumes = "application/json")
    @ResponseBody
    public ResponseEntity<Object> productRating(@RequestBody ProductRatingRequest productRatingRequest) {
        ResponseObject<Object> response = new ResponseObject<>();
        try {
            response.setResponseData(productService.productRating(productRatingRequest));
            response.setMessage(messageResourceService.getMessage(IdsmedMessageKeyConst.PRODUCT_RATING_SUCCESS_MESSAGE, LangHelper.getLangCode(productRatingRequest.getLangCode())));
        } catch (IdsmedBaseException e) {
            response.setCode(ResponseStatusEnum.ERROR.getCode());
            response.setError(e.getError());
        }

        return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
    }

    @RequestMapping(value = "/product-rating-comment", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Object> getProductRatingCommentList(@RequestParam(required = false) Long productId, @RequestParam(required = false) Double score, @RequestParam(required = false) String langCode
    , @RequestParam(required = false) Integer pageIndex, @RequestParam(required = false) Integer pageSize) {
        ResponseObject<Object> response = new ResponseObject<>();
        try {
            ProductRatingDetailListResponse productRatingDetailListResponse = productService.getProductRatingCommentList(productId, score, pageIndex, pageSize, langCode);
            response.setResponseData(productRatingDetailListResponse);
            if (CollectionUtils.isEmpty(productRatingDetailListResponse.getProductRatingDetailList())) {
                response.setMessage(messageResourceService.getMessage(IdsmedMessageKeyConst.PRODUCT_RATING_DETAIL_NOT_FOUND, LangHelper.getLangCode(langCode)));
            }
        } catch (IdsmedBaseException e) {
            response.setCode(ResponseStatusEnum.ERROR.getCode());
            response.setError(e.getError());
        }

        return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
    }

    @RequestMapping(value = "/wishlist", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<Object> updateProductInWishlist(@RequestBody ProductWishlistRequest request) {
        ResponseObject<Object> response = new ResponseObject<>();

        try {
            response.setResponseData(productService.updateWishlistInProductDetail(request));
            response.setMessage(messageResourceService.getMessage(IdsmedMessageKeyConst.ADD_TO_WISHLIST_SUCCESS_MESSAGE,
                    LangHelper.getLangCode(request.getLangCode())));

        } catch (IdsmedBaseException e) {
            response.setCode(ResponseStatusEnum.ERROR.getCode());
            response.setError(e.getError());
        }

        return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
    }

    @RequestMapping(value = "/wishlist/product", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Object> findFavouriteProductById(@RequestParam("productId") Long id,
                                                           @RequestParam("loginId") String loginId,
                                                           @RequestParam() String langCode) {
        ResponseObject<Object> response = new ResponseObject<>();

        try {
            response.setResponseData(productService.getFavouriteProductById(id, loginId, langCode));

        } catch (IdsmedBaseException e) {
            response.setCode(ResponseStatusEnum.ERROR.getCode());
            response.setError(e.getError());
        }

        return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);

    }

    @RequestMapping(value = "/inactive-product", method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<Object> inactiveProduct(@RequestBody ProductInactiveRequest request) {
        ResponseObject<Object> response = new ResponseObject<>();

        try {
            response.setResponseData(productService.inactiveProduct(request));
            response.setMessage(messageResourceService.getMessage(IdsmedMessageKeyConst.INACTIVE_PRODUCT_SUCCESS_MESSAGE,
                    LangHelper.getLangCode(request.getLangCode())));
        } catch (IdsmedBaseException e) {
            // TODO: handle exception
            response.setCode(ResponseStatusEnum.ERROR.getCode());
            response.setError(e.getError());
        }

        return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
    }

    @RequestMapping(value = "/product-label", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Object> getListOfProductTechnicalAndFeature(@RequestParam String langCode) {
        ResponseObject<Object> response = new ResponseObject<>();

        try {
            response.setResponseData(productService.getListOfProductTechnicalAndFeature(langCode));
        } catch (IdsmedBaseException e) {
            response.setCode(ResponseStatusEnum.ERROR.getCode());
            response.setError(e.getError());
        }

        return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
    }

    @RequestMapping(value = "/search/keyword", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Object> getKeywordHistories(@RequestParam(required = true) Long userId, @RequestParam(required = false) Integer number) {
        ResponseObject<Object> response = new ResponseObject<>();
        try {
            response.setResponseData(productService.getLastProductSearchKeyword(userId, number));
        } catch (IdsmedBaseException e) {
            response.setCode(ResponseStatusEnum.ERROR.getCode());
            response.setError(e.getError());
        }
        return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
    }

    @RequestMapping(value = "/comparison", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Object> comparisionProducts(@RequestParam() List<Long> productIds
            , @RequestParam() String langCode) {
        ResponseObject<Object> response = new ResponseObject<>();
        try {
            List<ProductComparisonResponse> responseList= productService.compareProductsV2(productIds, langCode);
            if (CollectionUtils.isEmpty(responseList.get(0).getComparedData())) {
                response.setMessage(messageResourceService.getMessage(IdsmedMessageKeyConst.PRODUCT_COMPARISON_NO_DATA,
                        LangHelper.getLangCode(langCode)));
            }
            response.setResponseData(responseList);
        } catch (IdsmedBaseException e) {
            response.setCode(ResponseStatusEnum.ERROR.getCode());
            response.setError(e.getError());
        }
        return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
    }

    @RequestMapping(value = "/medical-consumables-label", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Object> getListOfMedicalConsumablesLabel(@RequestParam String langCode) {
        ResponseObject<Object> response = new ResponseObject<>();

        try {
            response.setResponseData(productService.getListOfMedicalConsumablesLabel(langCode));
        } catch (IdsmedBaseException e) {
            response.setCode(ResponseStatusEnum.ERROR.getCode());
            response.setError(e.getError());
        }

        return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
    }

    @RequestMapping(value = "/product-hierarchy-list", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Object> getProductHierarchyList() {
        ResponseObject<Object> response = new ResponseObject<>();

        try {
            response.setResponseData(productService.getProductHierarchyList());
        } catch (IdsmedBaseException e) {
            response.setCode(ResponseStatusEnum.ERROR.getCode());
            response.setError(e.getError());
        }

        return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
    }

    @RequestMapping(value = "/resume-product", method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<Object> resumeInactiveProduct(@RequestBody ProductInactiveRequest request) {
        ResponseObject<Object> response = new ResponseObject<>();

        try {
            response.setResponseData(productService.resumeInactiveProduct(request));
            response.setMessage(messageResourceService.getMessage(IdsmedMessageKeyConst.REACTIVATE_PRODUCT_SUCCESS_MESSAGE,
                    LangHelper.getLangCode(request.getLangCode())));
        } catch (IdsmedBaseException e) {
            response.setCode(ResponseStatusEnum.ERROR.getCode());
            response.setError(e.getError());
        }

        return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
    }

    @RequestMapping(value = "/min-max-price", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Object> getMinMaxPrice(@RequestParam() String langCode) {
        ResponseObject<Object> response = new ResponseObject<>();
        try {
            response.setResponseData(productService.getMinAndMaxProductPrice(langCode));
        } catch (IdsmedBaseException e) {
            response.setCode(ResponseStatusEnum.ERROR.getCode());
            response.setError(e.getError());
        }
        return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
    }

    @RequestMapping(value = "/top-rating-latest-approved", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Object> getTopRatingAndLatestApprovedProduct(@RequestParam() String langCode, @RequestParam(required = false) String loginId) {
        ResponseObject<Object> response = new ResponseObject<>();
        try {
            response.setResponseData(productService.getTopRatingAndLatestApprovedProduct(langCode, loginId));
        } catch (IdsmedBaseException e) {
            response.setCode(ResponseStatusEnum.ERROR.getCode());
            response.setError(e.getError());
        }
        return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
    }

    @RequestMapping(value = "/wishlist", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Object> getWishList(@RequestParam() String langCode, @RequestParam() String loginId) {
        ResponseObject<Object> response = new ResponseObject<>();
        try {
            response.setResponseData(productService.getAllApprovedProductHaveProductWishlist(langCode, loginId));
        } catch (IdsmedBaseException e) {
            response.setCode(ResponseStatusEnum.ERROR.getCode());
            response.setError(e.getError());
        }
        return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
    }

    @RequestMapping(value = "/anonymous", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Object> getProductByIdForAnonymous(@RequestParam() Long productId, @RequestParam String langCode) {
        ResponseObject<Object> response = new ResponseObject<>();
        try {
            ProductResponse res = productService.getProductByIdForAnonymous(productId, langCode);
            response.setResponseData(res);
            response.setMessage(messageResourceService.getMessage(IdsmedMessageKeyConst.GET_PRODUCT_DETAILS_SUCCESS,
                    LangHelper.getLangCode(langCode)));
        } catch (IdsmedBaseException e) {
            response.setCode(ResponseStatusEnum.ERROR.getCode());
            response.setError(e.getError());
            return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
    }

    /**
     * @param keyword
     * @param careAreaIds   : list of id of ProductCareArea
     * @param categoryIds   : list of id of ProductCategory
     * @param brandIds      : list of id of ProductBrand
     * @param type      : defined by BEST_SELLER_SEARCH_TYPE = hot
     *                      but now this feature hasn't been implemented
     * @param pageIndex
     * @param pageSize
     * @param langCode
     * @param sortFieldCode : Defined by this ProductCustomRepositoryImpl.FuzzySearchProductSortFieldEnum
     * @param sortOrder:    pass string "asc" or "desc"
     * @return
     */
    @RequestMapping(value = "/anonymous/search", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Object> getProductSearchByKeyForAnonymous(@RequestParam(required = false) String keyword,
                                                        @RequestParam(required = false) List<Long> careAreaIds,
                                                        @RequestParam(required = false) List<Long> categoryIds,
                                                        @RequestParam(required = false) List<Long> brandIds,
                                                        @RequestParam(required = false) String type,
                                                        @RequestParam(required = false) Integer pageIndex,
                                                        @RequestParam(required = false) Integer pageSize,
                                                        @RequestParam(required = false) String langCode,
                                                        @RequestParam(required = false) Integer sortFieldCode,
                                                        @RequestParam(required = false) String sortOrder,
                                                        @RequestParam(required = false) BigDecimal minPrice,
                                                        @RequestParam(required = false) BigDecimal maxPrice,
                                                        @RequestParam(required = false) Integer productType,
                                                        @RequestParam(required = false) String manufacturerCountryCode,
                                                        @RequestParam(required = false) String manufacturerProvinceCode,
                                                        @RequestParam(required = false) List<Long> secondCategoryIds) {
        ResponseObject<Object> response = new ResponseObject<>();
        try {
            ProductListResponse productListResponse = productService.searchProductByKeywordForAnonymous(keyword, careAreaIds, categoryIds
                        , brandIds, type, pageIndex, pageSize
                        , sortFieldCode, sortOrder, minPrice, maxPrice, productType, manufacturerCountryCode, manufacturerProvinceCode, secondCategoryIds, langCode);
            response.setResponseData(productListResponse);
            if (CollectionUtils.isEmpty(productListResponse.getProductList())) {
                response.setMessage(messageResourceService.getMessage(IdsmedMessageKeyConst.PRODUCT_NOT_FOUND, LangHelper.getLangCode(langCode)));
            }
        } catch (IdsmedBaseException e) {
            response.setCode(ResponseStatusEnum.ERROR.getCode());
            response.setError(e.getError());
        }
        return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
    }

    @RequestMapping(value = "/third-level-product-hierarchy-by-code", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Object> getThirdLevelProductHierarchyByCode(@RequestParam() String productHierarchyLevelFourCode, @RequestParam() String langCode) {
        ResponseObject<Object> response = new ResponseObject<>();

        try {
            response.setResponseData(productService.getThirdLevelProductHierarchy(productHierarchyLevelFourCode, langCode));
        } catch (IdsmedBaseException e) {
            response.setCode(ResponseStatusEnum.ERROR.getCode());
            response.setError(e.getError());
        }

        return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
    }

    @RequestMapping(value = "/popular-search-keyword", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Object> getPopularSearchKeyword(@RequestParam() Long numberOfKeyword) {
        ResponseObject<Object> response = new ResponseObject<>();

        try {
            response.setResponseData(productService.getPopularSearchKeyword(numberOfKeyword));
        } catch (IdsmedBaseException e) {
            response.setCode(ResponseStatusEnum.ERROR.getCode());
            response.setError(e.getError());
        }

        return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
    }
}

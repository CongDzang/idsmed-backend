package com.cmg.idsmed.api.user;

import java.util.Optional;

import com.cmg.idsmed.common.enums.UserTypeEnum;
import com.cmg.idsmed.common.utils.LangHelper;
import com.cmg.idsmed.dto.user.*;
import com.cmg.idsmed.service.corporate.CorporateService;
import com.cmg.idsmed.service.hospital.HospitalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.cmg.idsmed.common.Constant.IdsmedMessageKeyConst;
import com.cmg.idsmed.common.enums.ResponseStatusEnum;
import com.cmg.idsmed.common.exception.IdsmedBaseException;
import com.cmg.idsmed.dto.ResponseObject;
import com.cmg.idsmed.service.share.MessageResourceService;
import com.cmg.idsmed.service.user.IdsmedUserService;


@RestController
@RequestMapping(value = "api/user")
public class IdsmedUserController {

    @Autowired
    private IdsmedUserService idsmedUserService;

    @Autowired
    private MessageResourceService messageResourceService;

    @Autowired
	private HospitalService hospitalService;

    @Autowired
	private CorporateService corporateService;

	@RequestMapping(method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Object> getUserList(@RequestParam(value="loginId") String loginId,
			@RequestParam(value="loginIdKeyword", required = false) String loginIdKeyword,
			@RequestParam(value="firstname", required = false) String firstname,
			@RequestParam(value="lastname", required = false) String lastname,
			@RequestParam(value="status", required = false) Integer status,
			@RequestParam(required = false) Integer pageIndex,
			@RequestParam(required = false) Integer pageSize,
			@RequestParam(required = true) String langCode) {
		ResponseObject<Object> response = new ResponseObject<>();
		try {
			IdsmedUserListResponse userListResponse = idsmedUserService.getUserList(loginId, loginIdKeyword, firstname, lastname, status, pageIndex, pageSize, langCode);
			response.setResponseData(userListResponse);
			if (CollectionUtils.isEmpty(userListResponse.getUserList())) {
				response.setMessage(messageResourceService.getMessage(IdsmedMessageKeyConst.USER_NOT_FOUND, langCode));
			}
		} catch (IdsmedBaseException e) {
			response.setCode(ResponseStatusEnum.ERROR.getCode());
			response.setError(e.getError());
		}

		return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
	}

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Object> getUserById(@PathVariable("id") Long id) {
        ResponseObject<Object> response = new ResponseObject<>();
        try {
            response.setResponseData(idsmedUserService.getUserById(id));
        } catch (IdsmedBaseException e) {
			response.setCode(ResponseStatusEnum.ERROR.getCode());
            response.setError(e.getError());
        }

        return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
    }

	@RequestMapping(value = "/hospital/{id}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Object> geHospitalUserById(@PathVariable("id") Long id, @RequestParam() String langCode) {
		ResponseObject<Object> response = new ResponseObject<>();
		try {
			response.setResponseData(idsmedUserService.getHospitalUserById(id, langCode));
		} catch (IdsmedBaseException e) {
			response.setCode(ResponseStatusEnum.ERROR.getCode());
			response.setError(e.getError());
		}

		return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
	}

	@RequestMapping(value = "/corporate/{id}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Object> getCorporateUserById(@PathVariable("id") Long id, @RequestParam() String langCode) {
		ResponseObject<Object> response = new ResponseObject<>();
		try {
			response.setResponseData(idsmedUserService.getCorporateUserById(id, langCode));
		} catch (IdsmedBaseException e) {
			response.setCode(ResponseStatusEnum.ERROR.getCode());
			response.setError(e.getError());
		}

		return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
	}

	@RequestMapping(value = "/individual/{id}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Object> getIndividualUserById(@PathVariable("id") Long id, @RequestParam() String langCode) {
		ResponseObject<Object> response = new ResponseObject<>();
		try {
			response.setResponseData(idsmedUserService.getIndividualUserById(id, langCode));
		} catch (IdsmedBaseException e) {
			response.setCode(ResponseStatusEnum.ERROR.getCode());
			response.setError(e.getError());
		}

		return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
	}

    
    @RequestMapping(value="/create", method = RequestMethod.POST, consumes = "multipart/form-data")
    @ResponseBody
    public ResponseEntity<Object> create(@RequestPart("createUserRequest") IdsmedUserRequest request, @RequestPart("profileImgUpload") Optional<MultipartFile> profileImage) {
        ResponseObject<Object> response = new ResponseObject<>();
        try {
            response.setResponseData(idsmedUserService.create(request, profileImage.orElse(null)));
            response.setMessage(messageResourceService.getMessage(IdsmedMessageKeyConst.CREATE_USER_SUCCESS_MESSAGE, LangHelper.getLangCode(request.getLangCode())));
        } catch (IdsmedBaseException e) {
            response.setCode(ResponseStatusEnum.ERROR.getCode());
            response.setError(e.getError());
        }
        return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
    }

    @RequestMapping(value="/edit", method = RequestMethod.PUT, consumes = "multipart/form-data")
    @ResponseBody
    public ResponseEntity<Object> edit(@RequestPart("editUserRequest") IdsmedUserRequest request, @RequestPart("profileImgUpload") Optional<MultipartFile> profileImage) {
        ResponseObject<Object> response = new ResponseObject<>();
        try {
            response.setResponseData(idsmedUserService.edit(request, profileImage.orElse(null)));
            response.setMessage(messageResourceService.getMessage(IdsmedMessageKeyConst.EDIT_USER_SUCCESS_MESSAGE, LangHelper.getLangCode(request.getLangCode())));
        } catch (IdsmedBaseException e) {
            response.setCode(ResponseStatusEnum.ERROR.getCode());
            response.setError(e.getError());
        }
        return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
    }
    
    @RequestMapping(value="/approve", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<Object> approve(@RequestBody IdsmedUserRequest request) {
        ResponseObject<Object> response = new ResponseObject<>();
        try {
            response.setResponseData(idsmedUserService.approve(request));
            response.setMessage(messageResourceService.getMessage(IdsmedMessageKeyConst.APPROVE_USER_SUCCESS_MESSAGE, LangHelper.getLangCode(request.getLangCode())));
        } catch (IdsmedBaseException e) {
            response.setCode(ResponseStatusEnum.ERROR.getCode());
            response.setError(e.getError());
        }
        return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
    }
    
    @RequestMapping(value="/reject", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<Object> reject(@RequestBody IdsmedUserRequest request) {
        ResponseObject<Object> response = new ResponseObject<>();
        try {
            response.setResponseData(idsmedUserService.reject(request));
            response.setMessage(messageResourceService.getMessage(IdsmedMessageKeyConst.REJECT_USER_SUCCESS_MESSAGE, LangHelper.getLangCode(request.getLangCode())));
        } catch (IdsmedBaseException e) {
            response.setCode(ResponseStatusEnum.ERROR.getCode());
            response.setError(e.getError());
        }
        return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
    }

    @RequestMapping(value = "/role", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseEntity<Object> updateUserRole(@RequestBody() UserUpdateRoleSimpleRequest request)  {
		ResponseObject<Object> response = new ResponseObject<>();
		try {
			response.setResponseData(idsmedUserService.updateUserRole(request));
			response.setMessage(messageResourceService.getMessage(IdsmedMessageKeyConst.EDIT_USER_SUCCESS_MESSAGE, LangHelper.getLangCode(request.getLangCode())));
		} catch (IdsmedBaseException e) {
			response.setCode(ResponseStatusEnum.ERROR.getCode());
			response.setError(e.getError());
		}
		return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
	}

    @RequestMapping(value = "/list-vendor-coordinator", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Object> getVendorCoordinatorList()  {
        ResponseObject<Object> response = new ResponseObject<>();
        try {
            response.setResponseData(idsmedUserService.getVendorCoordinatorList());
        } catch (IdsmedBaseException e) {
            response.setCode(ResponseStatusEnum.ERROR.getCode());
            response.setError(e.getError());
        }
        return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
    }

    @RequestMapping(value = "/status/inactive", method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<Object> inactiveUser(@RequestBody UserStatusRequest request) {
	    ResponseObject<Object> response = new ResponseObject<>();
	    try {
	        response.setResponseData(idsmedUserService.inactiveUserStatus(request));
            response.setMessage(messageResourceService.getMessage(
                    IdsmedMessageKeyConst.INACTIVE_USER_SUCCESS, LangHelper.getLangCode(request.getLangCode())));
        } catch (IdsmedBaseException e) {
	        response.setCode(ResponseStatusEnum.ERROR.getCode());
            response.setError(e.getError());
        }

	    return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
	}

	@RequestMapping(value = "/status/resume", method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<Object> resumeInactiveUser(@RequestBody UserStatusRequest request) {
        ResponseObject<Object> response = new ResponseObject<>();
        try {
            response.setResponseData(idsmedUserService.resumeInactiveUserStatus(request));
            response.setMessage(messageResourceService.getMessage(
                    IdsmedMessageKeyConst.REACTIVE_USER_SUCCESS, LangHelper.getLangCode(request.getLangCode())));
        } catch (IdsmedBaseException e) {
            response.setCode(ResponseStatusEnum.ERROR.getCode());
            response.setError(e.getError());
        }

        return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
    }

    @RequestMapping(value = "/re-registration/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Object> reRegisterUserById(@PathVariable("id") Long id,
                                                     @RequestParam String langCode) {
        ResponseObject<Object> response = new ResponseObject<>();
        try {
            response.setResponseData(idsmedUserService.reRegisterUserById(id, langCode));
        } catch (IdsmedBaseException e) {
            response.setCode(ResponseStatusEnum.ERROR.getCode());
            response.setError(e.getError());
        }

        return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
    }

	@RequestMapping(value = "/activation", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Object> activatelUser(@RequestParam() String uuid, @RequestParam() String langCode, @RequestParam() Integer userType) {
		ResponseObject<Object> response = new ResponseObject<>();
		try {
			if (userType.equals(UserTypeEnum.HOSPITAL_USER.getCode())) {
				response.setResponseData(hospitalService.activateHospitalUser(uuid, langCode));
			}

			if (userType.equals(UserTypeEnum.CORPORATE_USER.getCode())) {
				response.setResponseData(corporateService.activateCorporatelUser(uuid, langCode));
			}

			if (userType.equals(UserTypeEnum.INDIVIDUAL_USER.getCode())) {
				response.setResponseData(idsmedUserService.activateIndividualUser(uuid, langCode));
			}

			response.setMessage(messageResourceService.getMessage(IdsmedMessageKeyConst.USER_ACTIVATE_SUCCESS_MESSAGE, LangHelper.getLangCode(langCode)));
		} catch (IdsmedBaseException e) {
			response.setCode(ResponseStatusEnum.ERROR.getCode());
			response.setError(e.getError());
		}
		return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
	}

	@RequestMapping(value="/individual-user", method = RequestMethod.POST, consumes = "multipart/form-data")
	@ResponseBody
	public ResponseEntity<Object> IndividualUserRegister(@RequestPart("idsmedIndividualUserRequest") IdsmedIndividualUserRequest request, @RequestPart("profileImgUpload") Optional<MultipartFile> profileImage, @RequestPart("frontIdImage") Optional<MultipartFile> frontIdImage, @RequestPart("backIdImage") Optional<MultipartFile> backIdImage) {
		ResponseObject<Object> response = new ResponseObject<>();
		try {
			response.setResponseData(idsmedUserService.individualUserRegister(request, profileImage.orElse(null), frontIdImage.orElse(null), backIdImage.orElse(null)));
			response.setMessage(messageResourceService.getMessage(IdsmedMessageKeyConst.USER_REGISTRATION_SUCCESS_MESSAGE, LangHelper.getLangCode(request.getLangCode())));
		} catch (IdsmedBaseException e) {
			response.setCode(ResponseStatusEnum.ERROR.getCode());
			response.setError(e.getError());
		}
		return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
	}

	@RequestMapping(value="/individual-user", method = RequestMethod.PUT, consumes = "multipart/form-data")
	@ResponseBody
	public ResponseEntity<Object> editIndividualUser(@RequestPart("idsmedIndividualUserRequest") IdsmedIndividualUserRequest request, @RequestPart("profileImgUpload") Optional<MultipartFile> profileImage, @RequestPart("frontIdImage") Optional<MultipartFile> frontIdImage, @RequestPart("backIdImage") Optional<MultipartFile> backIdImage) {
		ResponseObject<Object> response = new ResponseObject<>();
		try {
			response.setResponseData(idsmedUserService.editIndividuallUser(request, profileImage.orElse(null), frontIdImage.orElse(null), backIdImage.orElse(null)));
			response.setMessage(messageResourceService.getMessage(IdsmedMessageKeyConst.USER_UPDATE_SUCCESS_MESSAGE, LangHelper.getLangCode(request.getLangCode())));
		} catch (IdsmedBaseException e) {
			response.setCode(ResponseStatusEnum.ERROR.getCode());
			response.setError(e.getError());
		}
		return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
	}
}

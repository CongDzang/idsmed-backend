package com.cmg.idsmed.api.vendor;

import java.util.List;
import java.util.Optional;

import com.cmg.idsmed.common.enums.VendorActionEnum;
import com.cmg.idsmed.common.utils.LangHelper;
import com.cmg.idsmed.dto.masterdata.CountryProvincePairRequest;
import com.cmg.idsmed.dto.vendor.*;
import com.cmg.idsmed.model.entity.vendor.config.VendorInfoConfig;
import com.cmg.idsmed.service.vendor.VendorInfoConfigService;
import org.apache.commons.codec.language.bm.Lang;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.cmg.idsmed.common.Constant.IdsmedMessageKeyConst;
import com.cmg.idsmed.common.enums.ResponseStatusEnum;
import com.cmg.idsmed.common.exception.IdsmedBaseException;
import com.cmg.idsmed.dto.ResponseObject;
import com.cmg.idsmed.service.share.MessageResourceService;
import com.cmg.idsmed.service.vendor.VendorService;

@RestController
@RequestMapping(value = "api/vendor")
public class VendorController {
	@Autowired
	private VendorService vendorService;
	
	@Autowired
	private MessageResourceService messageResourceService;

	@Autowired
	private VendorInfoConfigService vendorInfoConfigService;

	@RequestMapping(method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Object> getVendorList(@RequestParam(required = false) String companyNamePrimary,
												@RequestParam(required = false) Integer status,
												@RequestParam(required = false) Integer vendorCoordinator,
												@RequestParam(required = false) Integer pageIndex,
												@RequestParam(required = false) Integer pageSize,
												@RequestParam() String langCode) {
		ResponseObject<Object> response = new ResponseObject<>();
		try {
			VendorListResponse vendorListResponse = vendorService.getVendorList(companyNamePrimary, status, vendorCoordinator, pageIndex, pageSize);
			response.setResponseData(vendorListResponse);
			if (CollectionUtils.isEmpty(vendorListResponse.getVendorList())) {
				response.setMessage(messageResourceService.getMessage(IdsmedMessageKeyConst.VENDOR_NOT_FOUND, langCode));
			}
		} catch (IdsmedBaseException e) {
			response.setCode(ResponseStatusEnum.ERROR.getCode());
			response.setError(e.getError());
		}

		return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
	}
	
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Object> getVendorById(@PathVariable("id") Long id) {
        ResponseObject<Object> response = new ResponseObject<>();
        try {
            response.setResponseData(vendorService.getVendorById(id));
        } catch (IdsmedBaseException e) {
			response.setCode(ResponseStatusEnum.ERROR.getCode());
            response.setError(e.getError());
        }

        return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
    }

    @RequestMapping(value = "/re-registration/{id}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Object> getReRegisterVendorById(@PathVariable("id") Long id,
			@RequestParam() String langCode) {
		ResponseObject<Object> response = new ResponseObject<>();
		try {
			response.setResponseData(vendorService.getReRegisterVendorById(id, langCode));
		} catch (IdsmedBaseException e) {
			response.setCode(ResponseStatusEnum.ERROR.getCode());
			response.setError(e.getError());
		}

		return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
	}

    @RequestMapping(value = "/review", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<Object> updatePendingApproveStatus(@RequestBody VendorRequest request) {
        ResponseObject<Object> response = new ResponseObject<>();
        try {
            response.setResponseData(vendorService.updatePendingApproveStatus(request));
            response.setResponseData(messageResourceService.getMessage(IdsmedMessageKeyConst.REVIEW_VENDOR_SUCCESS_MESSAGE, LangHelper.getLangCode(request.getLangCode())));
        } catch (IdsmedBaseException e) {
            response.setCode(ResponseStatusEnum.ERROR.getCode());
            response.setError(e.getError());
        }
        return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
    }
    
    @RequestMapping(value = "/approve", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<Object> updateApproveStatus(@RequestBody VendorRequest request) {
        ResponseObject<Object> response = new ResponseObject<>();
        try {
            response.setResponseData(vendorService.updateApproveStatus(request));
            response.setMessage(messageResourceService.getMessage(IdsmedMessageKeyConst.APPROVE_VENDOR_SUCCESS_MESSAGE, LangHelper.getLangCode(request.getLangCode())));
        } catch (IdsmedBaseException e) {
            response.setCode(ResponseStatusEnum.ERROR.getCode());
            response.setError(e.getError());
        }
        return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
    }
    
    @RequestMapping(value = "/reject", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<Object> updateRejectStatus(@RequestBody VendorRequest request) {
        ResponseObject<Object> response = new ResponseObject<>();
        try {
            response.setResponseData(vendorService.updateRejectStatus(request));
            response.setMessage(messageResourceService.getMessage(IdsmedMessageKeyConst.REJECT_VENDOR_SUCCESS_MESSAGE, LangHelper.getLangCode(request.getLangCode())));
        } catch (IdsmedBaseException e) {
            response.setCode(ResponseStatusEnum.ERROR.getCode());
            response.setError(e.getError());
        }
        return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
    }

    @RequestMapping(value = "/create", method=RequestMethod.POST, consumes = "multipart/form-data")
	@ResponseBody
	public ResponseEntity<Object> create(@RequestPart("createVendorRequest") VendorRequest request,
										 @RequestPart(value = "logoUpload", required = false) MultipartFile companyLogo,
										 @RequestPart("documentsUpload") List<MultipartFile> companyDocuments) {
		ResponseObject<Object> response = new ResponseObject<>();
		try {
			response.setResponseData(vendorService.create(request, companyLogo, companyDocuments));
			response.setMessage(messageResourceService.getMessage(IdsmedMessageKeyConst.CREATE_VENDOR_SUCCESS_MESSAGE, LangHelper.getLangCode(request.getLangCode())));
			} catch (IdsmedBaseException e) {
			response.setCode(ResponseStatusEnum.ERROR.getCode());
			response.setError(e.getError());
		}
		return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
	}
    @RequestMapping(value = "/edit", method=RequestMethod.PUT, consumes = "multipart/form-data")
	@ResponseBody
	public ResponseEntity<Object> edit(@RequestPart("editVendorRequest") VendorRequest request, @RequestPart("logoUpload") Optional<MultipartFile> companyLogo, @RequestPart("documentsUpload") Optional<List<MultipartFile>> companyDocuments) {
		ResponseObject<Object> response = new ResponseObject<>();
		try {
			response.setResponseData(vendorService.edit(request, companyLogo.orElse(null), companyDocuments.orElse(null)));
			response.setMessage(messageResourceService.getMessage(IdsmedMessageKeyConst.EDIT_VENDOR_SUCCESS_MESSAGE, LangHelper.getLangCode(request.getLangCode())));
			} catch (IdsmedBaseException e) {
			response.setCode(ResponseStatusEnum.ERROR.getCode());
			response.setError(e.getError());
		}
		return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
	}

	/**
	 *
	 * @param countryId pass null when you want to get all list
	 * @param provinceId pass null when you want to get all list or you want to get only config for country
	 * @param status	pass null if you want to get both status
	 * 			ACTIVE(1, "active"),
	 * 			INACTIVE(10, "inactive");
	 * @param pageIndex
	 * @param pageSize
	 * @param langCode
	 * @return
	 */
	@RequestMapping(value = "/configuration", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Object> getVendorInfoConfiguration(@RequestParam(required = false) Long countryId
			, @RequestParam(required = false) Long provinceId
			, @RequestParam(required = false) Integer status
			//, @RequestParam(required = false) Integer location
			, @RequestParam(required = false) Integer vendorType
			, @RequestParam(required = false) Integer pageIndex
			, @RequestParam(required = false) Integer pageSize
			, @RequestParam(required = false) String langCode) {
		ResponseObject<Object> response = new ResponseObject<>();
		try {
			VendorInfoConfigListResponse configListResponse = vendorInfoConfigService.getAllVendorConfig(countryId, provinceId /*location*/, vendorType, status, pageIndex, pageSize);
			response.setResponseData(configListResponse);
			if (configListResponse.getTotalCount().equals(0)) {
				response.setMessage(messageResourceService.getMessage(IdsmedMessageKeyConst.VENDOR_INFO_CONFIGURATION_NOT_FOUND, LangHelper.getLangCode(langCode)));
			}
		} catch (IdsmedBaseException e) {
			response.setCode(ResponseStatusEnum.ERROR.getCode());
			response.setError(e.getError());
		}
		return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
	}

	@RequestMapping(value = "/configuration", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<Object> createVendorInfoConfig(@RequestBody() VendorInfoConfigRequest request) {
		ResponseObject<Object> response = new ResponseObject<>();
		try {
			response.setResponseData(vendorInfoConfigService.create(request));
		} catch (IdsmedBaseException e) {
			response.setCode(ResponseStatusEnum.ERROR.getCode());
			response.setError(e.getError());
		}

		return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
	}

	@RequestMapping(value = "/configuration/{id}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Object> getVendorInfoConfigById(@PathVariable(value = "id") Long id, @RequestParam() String langCode) {
		ResponseObject<Object> response = new ResponseObject<>();
		try {
			response.setResponseData(vendorInfoConfigService.getVendorInfoConfigById(id, langCode));
		} catch (IdsmedBaseException e) {
			response.setCode(ResponseStatusEnum.ERROR.getCode());
			response.setError(e.getError());
		}

		return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
	}

	@RequestMapping(value = "/configuration", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseEntity<Object> updateVendorInfoConfig(@RequestBody() VendorInfoConfigRequest request) {
		ResponseObject<Object> response = new ResponseObject<>();
		try {
			response.setResponseData(vendorInfoConfigService.edit(request));
		} catch (IdsmedBaseException e) {
			response.setCode(ResponseStatusEnum.ERROR.getCode());
			response.setError(e.getError());
		}

		return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
	}

	@RequestMapping(value = "/configuration/label-config", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Object> getVendorLabelConfigs(@RequestParam() Long countryId
			, @RequestParam(required = false) Long provinceId
			, @RequestParam(required = false) Integer location
			, @RequestParam(required = false) Integer vendorType
			, @RequestParam(required = false) Integer status
			, @RequestParam(required = false) String langCode) {

		ResponseObject<Object> response = new ResponseObject<>();
		try {
			response.setResponseData(vendorInfoConfigService.getVendorLabelConfigs(countryId, provinceId /*location*/, vendorType, status, langCode));
		} catch (IdsmedBaseException e) {
			response.setCode(ResponseStatusEnum.ERROR.getCode());
			response.setError(e.getError());
		}

		return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
	}

	@RequestMapping(value = "/file-attachment", method = RequestMethod.PUT, consumes = "multipart/form-data")
	@ResponseBody
	public ResponseEntity<Object> editVendorFileAttachment(@RequestPart("request") VendorFileAttachmentsEditRequest request, @RequestPart("uploadFiles") List<MultipartFile> uploadFiles) {
		ResponseObject<Object> response = new ResponseObject<>();
		try {
			response.setResponseData(vendorService.editVendorFileAttachment(request, uploadFiles));
			response.setMessage(messageResourceService.getMessage(IdsmedMessageKeyConst.EDIT_VENDOR_FILE_ATTACHMENT_SUCCESS_MESSAGE, LangHelper.getLangCode(request.getLangCode())));
		} catch (IdsmedBaseException e) {
			response.setCode(ResponseStatusEnum.ERROR.getCode());
			response.setError(e.getError());
		}

		return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
	}

	@RequestMapping(value = "/status", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseEntity<Object> setVendorStatus(@RequestBody() VendorSetStatusRequest request) {
		ResponseObject<Object> response = new ResponseObject<>();
		try {
			response.setResponseData(vendorService.setVendorStatus(request));
			if (request.getActionCode().equals(VendorActionEnum.SUSPEND.getCode())) {
				response.setMessage(messageResourceService
						.getMessage(IdsmedMessageKeyConst.VENDOR_SUSPEND_SUCCESS_MESSAGE
								, LangHelper.getLangCode(request.getLangCode())));
			}

			if (request.getActionCode().equals(VendorActionEnum.TERMINATE.getCode())) {
				response.setMessage(messageResourceService
						.getMessage(IdsmedMessageKeyConst.VENDOR_TERMINATE_SUCCESS_MESSAGE
								, LangHelper.getLangCode(request.getLangCode())));
			}

		} catch (IdsmedBaseException e) {
			response.setCode(ResponseStatusEnum.ERROR.getCode());
			response.setError(e.getError());
		}

		return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
	}

	@RequestMapping(value = "/status/resume", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseEntity<Object> resumeDeactiveVendor(@RequestBody() VendorSetStatusRequest request) {
		ResponseObject<Object> response = new ResponseObject<>();
		try {
			response.setResponseData(vendorService.resumeDeactiveVendor(request));
			response.setMessage(messageResourceService
					.getMessage(IdsmedMessageKeyConst.VENDOR_RESUME_SUCCESS_MESSAGE
							, LangHelper.getLangCode(request.getLangCode())));
		} catch (IdsmedBaseException e) {
			response.setCode(ResponseStatusEnum.ERROR.getCode());
			response.setError(e.getError());
		}
		return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
	}

	@RequestMapping(value = "/vendor-coordinator-by-company-code", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Object> getVendorCoordinatorByCompanyCode(@RequestParam() String companyCode, @RequestParam(required = false) String langCode) {
		ResponseObject<Object> response = new ResponseObject<>();
		try {
			response.setResponseData(vendorService.getVendorCoordinatorByCompanyCode(companyCode, langCode));
		} catch (IdsmedBaseException e) {
			response.setCode(ResponseStatusEnum.ERROR.getCode());
			response.setError(e.getError());
		}
		return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
	}


}
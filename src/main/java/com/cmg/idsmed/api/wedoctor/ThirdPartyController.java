package com.cmg.idsmed.api.wedoctor;

import com.cmg.idsmed.common.Constant.IdsmedMessageKeyConst;
import com.cmg.idsmed.common.enums.ResponseStatusEnum;
import com.cmg.idsmed.common.exception.IdsmedBaseException;
import com.cmg.idsmed.common.utils.LangHelper;
import com.cmg.idsmed.dto.ResponseObject;

import com.cmg.idsmed.dto.procurement.CancelOrderRequest;
import com.cmg.idsmed.dto.procurement.OrderStatusRequest;
import com.cmg.idsmed.dto.procurement.PurchaseOrderRequest;
import com.cmg.idsmed.dto.procurement.SyncLogisticsRequest;
import com.cmg.idsmed.dto.product.ProductResponse;
import com.cmg.idsmed.dto.product.SimpleProductListResponse;
import com.cmg.idsmed.dto.product.SimpleProductResponse;
import com.cmg.idsmed.dto.product.wedoctor.ProductListWedoctorResponse;
import com.cmg.idsmed.dto.product.wedoctor.ProductWedoctorResponse;
import com.cmg.idsmed.dto.thirdparty.CustomerRequest;
import com.cmg.idsmed.dto.thirdparty.CustomerResponse;

import com.cmg.idsmed.dto.product.*;

import com.cmg.idsmed.service.masterdata.CareAreaService;
import com.cmg.idsmed.service.masterdata.ProductBrandService;
import com.cmg.idsmed.service.masterdata.ProductCategoryService;
import com.cmg.idsmed.service.procurement.PurchaseOrderService;
import com.cmg.idsmed.service.product.KeywordHistoryService;
import com.cmg.idsmed.service.product.ProductService;
import com.cmg.idsmed.service.share.MessageResourceService;
import com.cmg.idsmed.service.thirdparty.ThirdPartyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

@RestController
@RequestMapping(value = "api/third-party")
public class ThirdPartyController {

	@Autowired
	private ProductService productService;

	@Autowired
	private KeywordHistoryService keywordHistoryService;

	@Autowired
	private MessageResourceService messageResourceService;

	@Autowired
	private ProductBrandService productBrandService;

	@Autowired
	private CareAreaService careAreaService;

	@Autowired
	private ProductCategoryService productCategoryService;

	@Autowired
	private ThirdPartyService thirdPartyService;

	@Autowired
	private PurchaseOrderService purchaseOrderService;

	private static final Integer PAGE_INDEX_DEFAULT = 0;
	private static final Integer PAGE_SIZE_DEFAULT = 100;

    /**
     *
     * @param id - Product's ID
     * @param weDoctorCustomerId - WeDoctor Customer's ID, not WeDoctor staff (Request param is userId due to in WeDoctor perspective)
     * @param langCode
     * @return
     */
    @RequestMapping(value = "/product/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Object> getProductById(@PathVariable("id") Long id,
                                                 @RequestParam(required = false, value = "userId") Long weDoctorCustomerId,
                                                 @RequestParam String langCode) {
        ResponseObject<Object> response = new ResponseObject<>();
        try {
            ProductWedoctorResponse res = productService.getProductByIdForWedoctor(id, weDoctorCustomerId, langCode);
            response.setResponseData(res);
            response.setMessage(messageResourceService.getMessage(IdsmedMessageKeyConst.GET_PRODUCT_DETAILS_SUCCESS,
                    LangHelper.getLangCode(langCode)));
        } catch (IdsmedBaseException e) {
            response.setCode(ResponseStatusEnum.ERROR.getCode());
            response.setError(e.getError());
            return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
    }

	/**
	 *
	 * @param id
	 * @param customerId: request value is userId, it is customer id provided by third party, wedoctor
	 * @param langCode
	 * @return
	 */
	@RequestMapping(value = "/product/simple/{id}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Object> getSimpleProductDetail(@PathVariable("id") Long id,
														 @RequestParam(required = false, value = "userId") Long customerId,
														 @RequestParam String langCode) {
		ResponseObject<Object> response = new ResponseObject<>();
		try {
			SimpleProductResponse res = productService.getSimpleProductDetailById(id, customerId, langCode);
			response.setResponseData(res);
			response.setMessage(messageResourceService.getMessage(IdsmedMessageKeyConst.GET_PRODUCT_DETAILS_SUCCESS,
					LangHelper.getLangCode(langCode)));
		} catch (IdsmedBaseException e) {
			response.setCode(ResponseStatusEnum.ERROR.getCode());
			response.setError(e.getError());
			return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
	}


	@RequestMapping(value = "/product/keyword-history", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Object> getHotWords() {
		ResponseObject<Object> response = new ResponseObject<>();
		try {
			response.setResponseData(keywordHistoryService.getAllKeywordOrderByCount());
		} catch (IdsmedBaseException e) {
			response.setCode(ResponseStatusEnum.ERROR.getCode());
			response.setError(e.getError());
		}

		return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
	}

    /**
     *
     * @param keyword - keyword wish to search
     * @param wedoctorCustomerId - WeDoctor Customer's ID, not WeDoctor staff (Request param is userId due to in WeDoctor perspective)
     * @param careArea
     * @param category
     * @param brand
     * @param pageIndex
     * @param pageSize
     * @param langCode
     * @return
     */
	@RequestMapping(value = "/product/searchsimple", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Object> getSimpleProductAdvSearchByKey(@RequestParam(required = false) String keyword,
																 @RequestParam(required = false, value = "userId") Long wedoctorCustomerId,
																 @RequestParam(required = false) Long careArea,
																 @RequestParam(required = false) Long category,
																 @RequestParam(required = false) Long brand,
																 @RequestParam(required = false) Integer pageIndex,
																 @RequestParam(required = false) Integer pageSize,
																 @RequestParam String langCode) {
		ResponseObject<Object> response = new ResponseObject<>();
		try {
			pageIndex = pageIndex == null ? PAGE_INDEX_DEFAULT : (pageIndex - 1 < 0 ? 0 : pageIndex - 1);
			pageSize = pageSize == null ? PAGE_SIZE_DEFAULT : pageSize;

			SimpleProductListResponse productListResponse = productService.searchSimpleProductByKey(keyword, wedoctorCustomerId, careArea, category, brand, pageIndex, pageSize);
			response.setResponseData(productListResponse);
			if (CollectionUtils.isEmpty(productListResponse.getProductList())) {
				response.setMessage(messageResourceService.getMessage(IdsmedMessageKeyConst.PRODUCT_NOT_FOUND, LangHelper.getLangCode(langCode)));
			}
		} catch (IdsmedBaseException e) {
			response.setCode(ResponseStatusEnum.ERROR.getCode());
			response.setError(e.getError());
		}
		return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
	}


	@RequestMapping(value = "/product/wishlist", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<Object> updateProductInWishlist(@RequestBody ProductWishlistForWedoctorRequest request) {
		ResponseObject<Object> response = new ResponseObject<>();

		try {
			response.setResponseData(productService.updateWishlistInProductDetailForWedoctor(request));
			response.setMessage(messageResourceService.getMessage(IdsmedMessageKeyConst.ADD_TO_WISHLIST_SUCCESS_MESSAGE,
					LangHelper.getLangCode(request.getLangCode())));

		} catch (IdsmedBaseException e) {
			response.setCode(ResponseStatusEnum.ERROR.getCode());
			response.setError(e.getError());
		}
		return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
	}

    /**
     *
     * @param clientId - WeDoctor ID, not WeDoctor Customer ID
     * @param userId - SCP Staff ID
     * @param type - Option: new / history / best
     * @param pageIndex
     * @param pageSize
     * @param langCode
     * @return
     */
	@RequestMapping(value = "/product/product-list-filter", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Object> getProductListFilter(@RequestParam(required = false) Long clientId,
													   @RequestParam(required = false) String userId,
													   @RequestParam String type,
													   @RequestParam(required = false) Integer pageIndex,
													   @RequestParam(required = false) Integer pageSize,
                                                       @RequestParam String langCode) {
		ResponseObject<Object> response = new ResponseObject<>();
		try {
			response.setResponseData(productService.getProductListFilter(clientId, userId, type, pageIndex, pageSize));
		} catch (IdsmedBaseException e) {
			response.setCode(ResponseStatusEnum.ERROR.getCode());
			response.setError(e.getError());
		}

		return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
	}

    /**
     *
     * @param wedoctorCustomerId - WeDoctor Customer's ID, not WeDoctor staff (Request param is userId due to in WeDoctor perspective)
     * @param keyword - keyword wish to search
     * @param brand - brand name wish to search
     * @param pageIndex
     * @param pageSize
     * @param langCode
     * @return
     */
	@RequestMapping(value = "/product/product-search-by-str", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Object> getProductSearchByStr(@RequestParam(required = false, value = "userId") Long wedoctorCustomerId,
														@RequestParam() String keyword,
														@RequestParam(required = false) String brand,
														@RequestParam(required = false) Integer pageIndex,
														@RequestParam(required = false) Integer pageSize,
														@RequestParam String langCode) {
		ResponseObject<Object> response = new ResponseObject<>();
		try {
			response.setResponseData(productService.getProductSearchByStr(wedoctorCustomerId, keyword, brand, pageIndex, pageSize, langCode));
		} catch (IdsmedBaseException e) {
			response.setCode(ResponseStatusEnum.ERROR.getCode());
			response.setError(e.getError());
		}

		return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
	}

    /**
     *
     * @param keyword - keyword wish to search
     * @param wedoctorCustomerId - WeDoctor Customer's ID, not WeDoctor staff (Request param is userId due to in WeDoctor perspective)
     * @param pageIndex
     * @param pageSize
     * @param langCode
     * @return
     */
	@RequestMapping(value = "/product/search/favourite", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Object> getProductSearchByKey(@RequestParam(required = false) String keyword,
														@RequestParam(required = false, value = "userId") Long wedoctorCustomerId,
														@RequestParam(required = false) Integer pageIndex,
														@RequestParam(required = false) Integer pageSize,
                                                        @RequestParam String langCode) {
		ResponseObject<Object> response = new ResponseObject<>();
		try {
			response.setResponseData(productService.searchProductByUserFavourite(keyword, wedoctorCustomerId, pageIndex, pageSize));
		} catch (IdsmedBaseException e) {
			response.setCode(ResponseStatusEnum.ERROR.getCode());
			response.setError(e.getError());
		}
		return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
	}

    /**
     *
     * @param wedoctorCustomerId - WeDoctor Customer's ID, not WeDoctor staff (Request param is userId due to in WeDoctor perspective)
     * @param pageIndex
     * @param pageSize
     * @param langCode
     * @return
     */
	@RequestMapping(value = "/product/suggested-products", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Object> getSuggestedProducts(@RequestParam(value = "userId") Long wedoctorCustomerId,
													   @RequestParam(required = false) Integer pageIndex,
													   @RequestParam(required = false) Integer pageSize,
													   @RequestParam String langCode) {

		ResponseObject<Object> response = new ResponseObject<>();
		try {
			response.setResponseData(productService.getSuggestedProducts(wedoctorCustomerId, pageIndex, pageSize, langCode));
			response.setMessage(messageResourceService.getMessage(IdsmedMessageKeyConst.GET_SUGGESTED_PRODUCT_SUCCESS,
					LangHelper.getLangCode(langCode)));
		} catch (IdsmedBaseException e) {
			response.setCode(ResponseStatusEnum.ERROR.getCode());
			response.setError(e.getError());
		}

		return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
	}

    /**
     *
     * @param wedoctorCustomerId - WeDoctor Customer's ID, not WeDoctor staff (Request param is userId due to in WeDoctor perspective)
     * @param pageIndex
     * @param pageSize
     * @param langCode
     * @return
     */
	@RequestMapping(value = "/product/recently-search-product", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Object> getRecentlySearchProduct(@RequestParam(value = "userId")  Long wedoctorCustomerId,
														   @RequestParam(required = false) Integer pageIndex,
														   @RequestParam(required = false) Integer pageSize,
                                                           @RequestParam String langCode) {
		ResponseObject<Object> response = new ResponseObject<>();
		try {
			response.setResponseData(productService.getRecentlySearchProduct(langCode, wedoctorCustomerId, pageIndex, pageSize));
		} catch (IdsmedBaseException e) {
			response.setCode(ResponseStatusEnum.ERROR.getCode());
			response.setError(e.getError());
		}

		return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
	}

	/**
	 * @param keyword
	 * @param careAreaIds   : list of id of ProductCareArea
	 * @param categoryIds   : list of id of ProductCategory
	 * @param brandIds      : list of id of ProductBrand
	 * @param typeCode      : defined by BEST_SELLER_SEARCH_TYPE = 1 search best seller
	 *                      but now this feature hasn't been implemented
	 * @param pageIndex
	 * @param pageSize
	 * @param langCode
	 * @param sortFieldCode : Defined by this ProductCustomRepositoryImpl.FuzzySearchProductSortFieldEnum
	 * @param sortOrder:    pass string "asc" or "desc"
	 * @return
	 */
	@RequestMapping(value = "/product/search", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Object> getProductSearchByKey(@RequestParam(required = false) String keyword,
														@RequestParam(required = false) List<Long> careAreaIds,
														@RequestParam(required = false) List<Long> categoryIds,
														@RequestParam(required = false) List<Long> brandIds,
														@RequestParam(required = false) String type,
														@RequestParam(required = false) Integer pageIndex,
														@RequestParam(required = false) Integer pageSize,
														@RequestParam(required = false) Integer sortFieldCode,
														@RequestParam(required = false) String sortOrder,
														@RequestParam(required = false, value = "userId") Long wedoctorCustomerId,
														@RequestParam(required = false) BigDecimal minPrice,
														@RequestParam(required = false) BigDecimal maxPrice,
                                                        @RequestParam String langCode) {
		ResponseObject<Object> response = new ResponseObject<>();
		try {
			ProductListResponse productListResponse = productService.searchProductByKeywordForWedoctor(keyword, careAreaIds, categoryIds
					, brandIds, type, pageIndex, pageSize
					, sortFieldCode, sortOrder, wedoctorCustomerId, minPrice, maxPrice);
			response.setResponseData(productListResponse);
			if (CollectionUtils.isEmpty(productListResponse.getProductList())) {
				response.setMessage(messageResourceService.getMessage(IdsmedMessageKeyConst.PRODUCT_NOT_FOUND, LangHelper.getLangCode(langCode)));
			}
		} catch (IdsmedBaseException e) {
			response.setCode(ResponseStatusEnum.ERROR.getCode());
			response.setError(e.getError());
		}
		return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
	}

    /**
     *
     * @param wedoctorCustomerId - WeDoctor Customer's ID, not WeDoctor staff (Request param is userId due to in WeDoctor perspective)
     * @param langCode
     * @return
     */
	@RequestMapping(value = "/product/product-favourite-brand", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Object> getFavouriteBrand(@RequestParam(value = "userId") Long wedoctorCustomerId,
													@RequestParam String langCode) {
		ResponseObject<Object> response = new ResponseObject<>();
		try {
			response.setResponseData(productBrandService.getThirdPartyFavouriteBrand(wedoctorCustomerId));
		} catch (IdsmedBaseException e) {
			response.setCode(ResponseStatusEnum.ERROR.getCode());
			response.setError(e.getError());
		}

		return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
	}

    /**
     *
     * @param pageIndex
     * @param pageSize
     * @param langCode
     * @return
     */
	@RequestMapping(value = "/product", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Object> getProductList(@RequestParam(required = false) Integer pageIndex,
												 @RequestParam(required = false) Integer pageSize,
												 @RequestParam String langCode) {
		ResponseObject<Object> response = new ResponseObject<>();
		try {
			ProductListWedoctorResponse productListResponse = productService.getProductListForWedoctor(pageIndex, pageSize, langCode);
			response.setResponseData(productListResponse);
			if (CollectionUtils.isEmpty(productListResponse.getProductList()))
				response.setMessage(messageResourceService.getMessage(IdsmedMessageKeyConst.PRODUCT_NOT_FOUND, LangHelper.getLangCode(langCode)));
		} catch (IdsmedBaseException e) {
			response.setCode(ResponseStatusEnum.ERROR.getCode());
			response.setError(e.getError());
		}

		return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
	}

    /**
     *
     * @param wedoctorCustomerId - WeDoctor Customer's ID, not WeDoctor staff (Request param is userId due to in WeDoctor perspective)
     * @param pageIndex
     * @param pageSize
     * @param langCode
     * @return
     */
	@RequestMapping(value = "/masterdata/carearea", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Object> getCareAreaList(@RequestParam(required = false, value = "userId") Long wedoctorCustomerId,
												  @RequestParam(required = false) Integer pageIndex,
												  @RequestParam(required = false) Integer pageSize,
												  @RequestParam String langCode) {
		ResponseObject<Object> response = new ResponseObject<>();
		try {
			response.setResponseData(careAreaService.getCareAreaList(pageIndex, pageSize, langCode));
		} catch (IdsmedBaseException e) {
			response.setCode(ResponseStatusEnum.ERROR.getCode());
			response.setError(e.getError());
		}

		return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
	}

    /**
     *
     * @param wedoctorCustomerId - WeDoctor Customer's ID, not WeDoctor staff (Request param is userId due to in WeDoctor perspective)
     * @param pageIndex
     * @param pageSize
     * @param langCode
     * @return
     */
	@RequestMapping(value = "/masterdata/productcategory", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Object> getProductCategoryList(@RequestParam(required = false, value = "userId") Long wedoctorCustomerId,
														 @RequestParam(required = false) Integer pageIndex,
														 @RequestParam(required = false) Integer pageSize,
                                                         @RequestParam String langCode) {

		pageIndex = pageIndex == null ? PAGE_INDEX_DEFAULT : (pageIndex - 1 < 0 ? 0 : pageIndex - 1);
		pageSize = pageSize == null ? PAGE_SIZE_DEFAULT : pageSize;


		ResponseObject<Object> response = new ResponseObject<>();
		try {
			response.setResponseData(productCategoryService.getProductCategoryList(pageIndex, pageSize));
		} catch (IdsmedBaseException e) {
			response.setCode(ResponseStatusEnum.ERROR.getCode());
			response.setError(e.getError());
		}

		return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
	}

    /**
     *
     * @param wedoctorCustomerId - WeDoctor Customer's ID, not WeDoctor staff (Request param is userId due to in WeDoctor perspective)
     * @param pageIndex
     * @param pageSize
     * @param langCode
     * @return
     */
	@RequestMapping(value = "/masterdata/productbrand", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Object> getProductBrandList(@RequestParam(required = false, value = "userId") Long wedoctorCustomerId,
													  @RequestParam(required = false) Integer pageIndex,
													  @RequestParam(required = false) Integer pageSize,
													  @RequestParam String langCode) {

		pageIndex = pageIndex == null ? PAGE_INDEX_DEFAULT : (pageIndex - 1 < 0 ? 0 : pageIndex - 1);
		pageSize = pageSize == null ? PAGE_SIZE_DEFAULT : pageSize;


		ResponseObject<Object> response = new ResponseObject<>();
		try {
			response.setResponseData(productBrandService.getProductBrandList(pageIndex, pageSize));
		} catch (IdsmedBaseException e) {
			response.setCode(ResponseStatusEnum.ERROR.getCode());
			response.setError(e.getError());
		}

		return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
	}

    /**
     *
     * @param customerRequest
     * @return
     */
    @RequestMapping(value = "/customer", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<Object> createCustomer(@RequestBody CustomerRequest customerRequest) {
        ResponseObject<Object> response = new ResponseObject<>();
        try {
            CustomerResponse res = thirdPartyService.saveIntoCustomerTable(customerRequest);
            response.setResponseData(res);
        } catch (IdsmedBaseException e) {
            response.setCode(ResponseStatusEnum.ERROR.getCode());
            response.setError(e.getError());
            return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
    }

    /**
     *
     * @param customerRequest
     * @return
     */
    @RequestMapping(value = "/customer", method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<Object> updateCustomer(@RequestBody CustomerRequest customerRequest) {
        ResponseObject<Object> response = new ResponseObject<>();
        try {
            CustomerResponse res = thirdPartyService.updateCustomer(customerRequest);
            response.setResponseData(res);
        } catch (IdsmedBaseException e) {
            response.setCode(ResponseStatusEnum.ERROR.getCode());
            response.setError(e.getError());
            return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
    }

	@RequestMapping(value = "/procurement/order", method = RequestMethod.POST)
	@ResponseBody
    public ResponseEntity<Object> createOrder(@RequestBody PurchaseOrderRequest request) {
		ResponseObject<Object> response = new ResponseObject<>();
		try {
			response.setResponseData(purchaseOrderService.createPurchaseOrder(request));
		} catch (IdsmedBaseException e) {
			response.setCode(ResponseStatusEnum.ERROR.getCode());
			response.setError(e.getError());
			return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.NO_CONTENT);
		}

		return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
	}

	@RequestMapping(value = "/procurement/update-order-status", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseEntity<Object> updateOrderStatus(@RequestBody OrderStatusRequest request) {
    	ResponseObject<Object> response = new ResponseObject<>();
    	try {
    		response.setResponseData(purchaseOrderService.updateOrderStatus(request));
		} catch (IdsmedBaseException e) {
    		response.setCode(ResponseStatusEnum.ERROR.getCode());
    		response.setError(e.getError());
		}

    	return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
	}

	@RequestMapping(value = "/procurement/cancel-order", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseEntity<Object> cancelOrderByProductId(@RequestBody CancelOrderRequest request) {
    	ResponseObject<Object> response = new ResponseObject<>();
    	try {
    		response.setResponseData(purchaseOrderService.cancelOrderByProductId(request));
		} catch (IdsmedBaseException e) {
    		response.setCode(ResponseStatusEnum.ERROR.getCode());
    		response.setError(e.getError());
		}

    	return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);

	}

	@RequestMapping(value = "/procurement/sync-logistic", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseEntity<Object> syncLogisticOrderInfo(@RequestBody SyncLogisticsRequest request) {
    	ResponseObject<Object> response = new ResponseObject<>();
    	try {
    		response.setResponseData(purchaseOrderService.syncOrderLogisticsInfo(request));
		} catch (IdsmedBaseException e) {
    		response.setCode(ResponseStatusEnum.ERROR.getCode());
    		response.setError(e.getError());
		}

    	return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
	}

	@RequestMapping(value = "/product/get-detail-list", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Object> getProductDetailList(@RequestParam() List<Long> productIds, @RequestParam() String langCode) {
    	ResponseObject<Object> response = new ResponseObject<>();
		try {
			response.setResponseData(productService.getProductDetailList(productIds, langCode));
		} catch (IdsmedBaseException e) {
			response.setCode(ResponseStatusEnum.ERROR.getCode());
			response.setError(e.getError());
		}

		return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
	}


}

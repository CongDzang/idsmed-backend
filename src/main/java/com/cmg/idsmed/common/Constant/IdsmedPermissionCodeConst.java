package com.cmg.idsmed.common.Constant;

public interface IdsmedPermissionCodeConst {
	String VENDOR_REVIEW = "VP0000004";
	String VENDOR_APPROVE = "VP0000005";
	String PRODUCT_REVIEW = "PP0000004";
	String PRODUCT_APPROVE = "PP0000005";
	String PRODUCT_REJECT = "PP0000007";
	String USER_APPROVE = "UA0000004";
	String USER_REJECT = "UA0000006";


}

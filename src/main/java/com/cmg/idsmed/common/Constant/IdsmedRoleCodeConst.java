package com.cmg.idsmed.common.Constant;

public interface IdsmedRoleCodeConst {
	String SUPER_ADMIN = "001";
	String VENDOR = "002";
	String VENDOR_REVIEWER = "003";
	String VENDOR_APPROVER = "004";
	String USER_APPROVER = "005";
	String PRODUCT_REVIEWER = "006";
	String PRODUCT_APPROVER = "007";
	String VENDOR_SENIOR = "Ven002";
	String SCP_ADMIN = "017";
	String HOSPITAL_USER = "Hos001";
	String CORPORATE_USER = "Corp001";
	String INDIVIDUAL_USER = "Indiv001";
}

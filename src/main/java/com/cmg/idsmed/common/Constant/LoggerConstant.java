package com.cmg.idsmed.common.Constant;

public interface LoggerConstant {
    public static final String LOGGER_START_METHOD_DESCRIPTION = "logger.start.method.description";
    public static final String LOGGER_END_METHOD_DESCRIPTION ="logger.end.method.description";
}

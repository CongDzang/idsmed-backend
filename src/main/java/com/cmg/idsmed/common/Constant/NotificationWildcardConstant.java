package com.cmg.idsmed.common.Constant;

public interface NotificationWildcardConstant {
	static final String USER_NAME = "[#USER_NAME]";
	static final String LOGIN_ID = "[#USER_ID]";
	static final String USER_STATUS = "[#USER_APP_STATUS]";
	static final String USER_REJECT_REASON = "[#USER_REJECT_REASON]";
	static final String VENDOR_NAME = "[#VENDOR_NAME]";
	static final String COMPANY_CODE = "[#COMPANY_CODE]";
	static final String VENDOR_STATUS = "[#VENDOR_APP_STATUS]";
	static final String VENDOR_REJECT_REASON = "[#VENDOR_REJECT_REASON]";
	static final String VENDOR_EXPIRY_DATE = "[#VENDOR_EXP_DATE]";
	static final String PRODUCT_NAME = "[#PRODUCT_NAME]";
	static final String PRODUCT_MODEL = "[#PRODUCT_MODEL]";
	static final String PRODUCT_CODE = "[#PRODUCT_CODE]";
	static final String PRODUCT_STATUS = "[#PRODUCT_APP_STATUS]";
	static final String PRODUCT_REJECT_REASON = "[#PRODUCT_REJECT_REASON]";
	static final String PRODUCT_EXP_DATE = "[#PRODUCT_EXP_DATE]";
	static final String PRODUCT_BATCH_UPLOAD_FILENAME = "[#PRODUCT_BATCH_UPLOAD_FILENAME]";
	static final String PRODUCT_BATCH_UPLOAD_RESULT = "[#PRODUCT_BATCH_UPLOAD_RESULT]";
	static final String SCP_URL = "[#SCP_URL]";
	static final String VENDOR_RESUBMIT_PROFILE_URL = "[#VENDOR_RESUBMIT_PROFILE_URL]";
	static final String VENDOR_DOCUMENT_NAME = "[#VENDOR_DOCUMENT_NAME]";
	static final String VENDOR_DOCUMENT_EXP_DATE = "[#VENDOR_DOCUMENT_EXP_DATE]";
	static final String VENDOR_DOCUMENT_APP_STATUS = "[#VENDOR_DOCUMENT_APP_STATUS]";
	static final String PRODUCT_DOCUMENT_NAME = "[#PRODUCT_DOCUMENT_NAME]";
	static final String PRODUCT_DOCUMENT_EXP_DATE = "[#PRODUCT_DOCUMENT_EXP_DATE]";
	static final String PRODUCT_DOCUMENT_APP_STATUS = "[#PRODUCT_DOCUMENT_APP_STATUS]";
	static final String RECIPIENT_NAME = "[#RECIPIENT_NAME]";
	static final String SENDER_NAME = "[#SENDER_NAME]";
	static final String USER_DEFAULT_PASSWORD = "[#USER_DEFAULT_PASSWORD]";
	static final String USER_REGISTRATION_RESUBMIT_URL = "[#USER_REGISTRATION_RESUBMIT_URL]";
	static final String USER_ACTIVATION_URL = "[#USER_ACTIVATION_LINK]";
	static final String INACTIVE_SUSPENDED_REASON = " [#INACTIVE_SUSPEND_REASON]";
	static final String TERMINATION_REASON = "[#TERMINATION_REASON]";


}

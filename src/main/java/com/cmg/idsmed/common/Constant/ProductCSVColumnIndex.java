package com.cmg.idsmed.common.Constant;

public interface ProductCSVColumnIndex {

	public final static Integer CARE_AREA_COLUMN_INDEX = 0;
	public final static Integer CATEGORY_COLUMN_INDEX = 1;
	public final static Integer BRAND_COLUMN_INDEX = 2;
	public final static Integer PRODUCT_NAME_PRIMARY_COLUMN_INDEX = 3;
	public final static Integer PRODUCT_NAME_SECONDARY_COLUMN_INDEX = 4;
	public final static Integer PRODUCT_CODE_COLUMN_INDEX = 5;
	public final static Integer PRODUCT_MODEL_COLUMN_INDEX = 6;
	public final static Integer PRODUCT_REG_NO_COLUMN_INDEX = 7;
	public final static Integer PRODUCT_DESCRIPTION_COLUMN_INDEX = 8;
	public final static Integer PRODUCT_HASHTAG_COLUMN_INDEX = 9;
	public final static Integer PRODUCT_UNIT_COLUMN_INDEX = 10;
	public final static Integer PRODUCT_PRICE_COLUMN_INDEX = 11;
	public final static Integer PRODUCT_PACKAGING_COLUMN_INDEX = 12;
	public final static Integer PRODUCT_MIN_OF_ORDER_COLUMN_INDEX = 13;
	public final static Integer PRODUCT_BROCHURE_ATTACHMENT_URL_COLUMN_INDEX = 14;
	public final static Integer PRODUCT_VIDEO_ATTACHMENT_URL_COLUMN_INDEX = 15;

	public final static Integer PRODUCT_IMAGE_1_COLUMN_INDEX = 16;
	public final static Integer PRODUCT_IMAGE_2_COLUMN_INDEX = 17;
	public final static Integer PRODUCT_IMAGE_3_COLUMN_INDEX = 18;
	public final static Integer PRODUCT_IMAGE_4_COLUMN_INDEX = 19;
	public final static Integer PRODUCT_IMAGE_5_COLUMN_INDEX = 20;
	public final static Integer PRODUCT_IMAGE_6_COLUMN_INDEX = 21;

	public final static Integer PRODUCT_TECH_LABEL_1_COLUMN_INDEX = 22;
	public final static Integer PRODUCT_TECH_VALUE_1_COLUMN_INDEX = 23;
	public final static Integer PRODUCT_TECH_LABEL_2_COLUMN_INDEX = 24;
	public final static Integer PRODUCT_TECH_VALUE_2_COLUMN_INDEX = 25;
	public final static Integer PRODUCT_TECH_LABEL_3_COLUMN_INDEX = 26;
	public final static Integer PRODUCT_TECH_VALUE_3_COLUMN_INDEX = 27;
	public final static Integer PRODUCT_TECH_LABEL_4_COLUMN_INDEX = 28;
	public final static Integer PRODUCT_TECH_VALUE_4_COLUMN_INDEX = 29;
	public final static Integer PRODUCT_TECH_LABEL_5_COLUMN_INDEX = 30;
	public final static Integer PRODUCT_TECH_VALUE_5_COLUMN_INDEX = 31;
	public final static Integer PRODUCT_TECH_LABEL_6_COLUMN_INDEX = 32;
	public final static Integer PRODUCT_TECH_VALUE_6_COLUMN_INDEX = 33;

	public final static Integer PRODUCT_FEATURE_LABEL_1_COLUMN_INDEX = 34;
	public final static Integer PRODUCT_FEATURE_VALUE_1_COLUMN_INDEX = 35;
	public final static Integer PRODUCT_FEATURE_LABEL_2_COLUMN_INDEX = 36;
	public final static Integer PRODUCT_FEATURE_VALUE_2_COLUMN_INDEX = 37;
	public final static Integer PRODUCT_FEATURE_LABEL_3_COLUMN_INDEX = 38;
	public final static Integer PRODUCT_FEATURE_VALUE_3_COLUMN_INDEX = 39;
	public final static Integer PRODUCT_FEATURE_LABEL_4_COLUMN_INDEX = 40;
	public final static Integer PRODUCT_FEATURE_VALUE_4_COLUMN_INDEX = 41;
	public final static Integer PRODUCT_FEATURE_LABEL_5_COLUMN_INDEX = 42;
	public final static Integer PRODUCT_FEATURE_VALUE_5_COLUMN_INDEX = 43;
	public final static Integer PRODUCT_FEATURE_LABEL_6_COLUMN_INDEX = 44;
	public final static Integer PRODUCT_FEATURE_VALUE_6_COLUMN_INDEX = 45;
	public final static Integer PRODUCT_CAN_PROCURE_COLUMN_INDEX = 46;
	public final static Integer PRODUCT_DEVICE_CATEGORY_COLUMN_INDEX = 47;

}

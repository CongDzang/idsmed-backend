package com.cmg.idsmed.common.Constant;

public interface ProductExcelFieldLength {

	public final static Integer PRODUCT_BRAND_NAME_LENGTH = 100;
	public final static Integer PRODUCT_CARE_AREA_NAME_LNEGTH = 300;
	public final static Integer PRODUCT_CATEGORY_NAME_LENGTH = 300;
	public final static Integer PRODUCT_NAME_PRIMARY_LENGTH = 1000;
	public final static Integer PRODUCT_NAME_SECONDARY_LENGTH = 1000;
	public final static Integer PRODUCT_CODE_LENGTH = 100;
	public final static Integer PRODUCT_MODEL_LENGTH = 100;
	public final static Integer PRODUCT_REG_NO_LENGTH = 500;
	public final static Integer PRODUCT_DESCRIPTION_LENGTH = 10000;
	public final static Integer PRODUCT_HASHTAG_LENGTH = 5000;
	public final static Integer PRODUCT_UNIT_LENGTH = 100;
	public final static Integer PRODUCT_PRICE_PRECISION_LENGTH = 7;
	public final static Integer PRODUCT_PACKAGING_LENGTH = 200;
	public final static Integer PRODUCT_VENDOR_NAME_LENGTH = 200;
	public final static Integer PRODUCT_BROCHURE_ATTACHMENT_URL_LENGTH = 10000;
	public final static Integer PRODUCT_VIDEO_ATTACHMENT_URL_LENGTH = 10000;
	public final static Integer PRODUCT_CERTIFICATE_ATTACHMENT_URL_LENGTH = 10000;
	public final static Integer PRODUCT_CSV_ORIGINAL_BASE_FILE_NAME_LENGTH = 53;

	public final static Integer PRODUCT_IMAGE_URL_LENGTH = 10000;

	public final static Integer PRODUCT_TECH_LABEL_LENGTH = 100;
	public final static Integer PRODUCT_TECH_VALUE_LENGTH = 1000;

	public final static Integer PRODUCT_FEATURE_LABEL_LENGTH = 100;
	public final static Integer PRODUCT_FEATURE_VALUE_LENGTH = 1000;


}

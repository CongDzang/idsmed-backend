package com.cmg.idsmed.common.Constant;

public class ProductParameterConstant {
	public static final String PAGE_INDEX = "Page Index = ";
	public static final String PAGE_SIZE = "Page Size = ";
	public static final String LANG_CODE = "Language Code = ";
	public static final String USER_ID = "User Id = ";
	public static final String ID = "ID = ";
	public static final String KEYWORD = "Keyword = ";
	public static final String CARE_AREA = "Care Area = ";
	public static final String CATEGORY = "Category = ";
	public static final String BRAND = "Brand = ";
	public static final String TYPE = "Type = ";
	public static final String LAST_N_PRODUCT = "Last N Product = "; 
	public static final String DURATION = "Duration = ";
}

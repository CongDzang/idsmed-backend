package com.cmg.idsmed.common.Constant;

public interface PropertiesLabelConst {
//	security.properties file
	//	url.frontend.domain=http://localhost:4200
	String URL_FRONTEND_DOMAIN = "url.frontend.domain";

	//url.sub.vendor.review=/layout/vendor/vendor-review
	String URL_SUB_URL_VENDOR_REVIEW = "url.sub.vendor.review";

	//url.sub.vendor.approve=/layout/vendor/vendor-approve
	String URL_SUB_VENDOR_APPROVE = "url.sub.vendor.approve";

	//url.sub.product.review=/layout/product/product-review
	String URL_SUB_URL_PRODUCT_REVIEW = "url.sub.product.review";

	//url.sub.product.approve=/layout/product/product-approve
	String URL_SUB_PRODUCT_APPROVE = "url.sub.product.approve";

	//url.sub.user.approve=/layout/user/user-approve
	String URL_SUB_USER_APPROVE = "url.sub.user.approve";

	//smtp.support.mail=support@idsmed.com.my
	String SMTP_SUPPORT_MAIL_LABEL = "smtp.support.mail";

	//idsmed.web.session.timeout=120
	String WEB_SESSION_TIMEOUT_LABEL= "idsmed.web.session.timeout";

	//idsmed.api.session.timeout=10080
	String API_SESSION_TIMEOUT_LABEL= "idsmed.api.session.timeout";

//	Mail Properties File
	//smtp.mail.host=mailserver.cmg.com.my
	String SMTP_MAIL_HOST_LABEL = "smtp.mail.host";

	//smtp.mail.port=465
	String SMTP_MAIL_PORT_LABEL = "smtp.mail.port";

	//smtp.mail.username=chong.sian@cmg.com.my
	String SMTP_MAIL_USERNAME_LABEL = "smtp.mail.username";

	//smtp.mail.password=Chong880722!
	String SMTP_MAIL_PASSWORD_LABEL = "smtp.mail.password";

	//email.provider.config.file=../idsmedschedule/config_files/email_provider_config
	String EMAIL_PROVIDER_CONFIG_FILE_LABEL = "email.provider.config.file";

//	mail.hospital.user.registration.activation.link=https://dev.smartscp.com/frontend/hospital-user-activate
	String EMAIL_USER_REGISTRATION_ACTIVATION_LINK = "mail.user.registration.activation.link";

//	oss.bucket.domain=https://idsmeddev.oss-cn-hongkong.aliyuncs.com/
	String OSS_BUCKET_DOMAIN = "oss.bucket.domain";

}

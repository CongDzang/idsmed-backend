package com.cmg.idsmed.common.Constant;

public interface ScheduleTaskCodeConst {
	String VENDOR_ATTACHMENT_EXPIRY_REMINDER = "CRJ0001";
	String PRODUCT_DOCUMENT_ATTACHMENT_EXPIRY_REMINDER = "CRJ0002";
}

package com.cmg.idsmed.common.enums;

public enum CellTypeEnum {


    NUMERIC_CELL_TYPE(1, "Numeric Cell Type"),
    STRING_CELL_TYPE(2, "String Cell Type");

    private Integer code;
    private String name;

    CellTypeEnum(Integer code, String name) {
        this.code = code;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public Integer getCode() { return code; }

    public void setCode(Integer code) { this.code = code; }
}

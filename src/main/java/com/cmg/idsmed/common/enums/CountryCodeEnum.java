package com.cmg.idsmed.common.enums;


public enum CountryCodeEnum {
	MALAY("malay"),
	CHINA("cn");

	private String code;

	CountryCodeEnum(String code) {
		this.code = code;

	}

	public String getCode() {
		return code;
	}
}

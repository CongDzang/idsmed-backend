package com.cmg.idsmed.common.enums;

import java.util.Arrays;
import java.util.Optional;

public enum CustomerStatusEnum {

	ACTIVE(1, "Active"),
	INACTIVE(2, "Inactive");

	private Integer code;
	private String name;

	private CustomerStatusEnum(Integer code, String name) {
		this.code = code;
		this.name = name;
	}

	public Integer getCode() {
		return code;
	}

	public String getName() {
		return name;
	}
	
	public static String getStatusNameByCode(Integer code) {
		Optional<CustomerStatusEnum> status = Arrays.asList(CustomerStatusEnum.values()).stream().filter(s -> s.getCode() == code).findFirst();
		return status.isPresent() ? status.get().getName() : "";
	}
	
}
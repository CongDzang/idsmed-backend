package com.cmg.idsmed.common.enums;

public enum DefaultPasswordEnum {

    DEFAULT_PASSWORD("idsm@d123");

    private String password;

    DefaultPasswordEnum(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }
}

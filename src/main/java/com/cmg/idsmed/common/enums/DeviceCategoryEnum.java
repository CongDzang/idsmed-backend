package com.cmg.idsmed.common.enums;

public enum DeviceCategoryEnum {

    ONE(1.0, "1"),
    TWO(2.0, "2"),
    THREE(3.0, "3");
    private Double code;
    private String name;

    DeviceCategoryEnum(Double code, String name) {
        this.code = code;
        this.name = name;
    }

    public Double getCode() {
        return code;
    }

    public String getName() {
        return name;
    }
}

package com.cmg.idsmed.common.enums;

public enum EmailErrorEnum {

	UNKNOWN_ERROR(0, "Unknown error"),
	WRONG_RECIPIENT_EMAIL_ADDRESS(1, "Wrong recipient email address");

	private Integer code;
	private String name;

	EmailErrorEnum(Integer code, String name) {
		this.code = code;
		this.name = name;
	}

	public Integer getCode() {
		return code;
	}

	public String getName() {
		return name;
	}
}
package com.cmg.idsmed.common.enums;

public enum EmailProviderEnum {
	SMTP("smtp", "cmg email server"),
	SENDGRID("sendgrid", "sendgrid email service");

	private String code;
	private String description;

	EmailProviderEnum(String code, String description) {
		this.code = code;
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}
}

package com.cmg.idsmed.common.enums;

public enum EmailStatusEnum {

	SENT(0, "Sent"),
	SEND_FAILD(1, "Send fail"),
	RESENT(2, "Resent"),
	RESEND_FAILD(3, "Resend faild");

	private Integer code;
	private String name;

	EmailStatusEnum(Integer code, String name) {
		this.code = code;
		this.name = name;
	}

	public Integer getCode() {
		return code;
	}

	public String getName() {
		return name;
	}
}
package com.cmg.idsmed.common.enums;

public enum EmailTypeEnum {

	/**
	 * NOTE: BE REMIND EMAIL TYPE ENUM NEEDS TO BE TALLY WITH FRONT-END EMAIL TYPE ENUM
	 * AT LEAST FOR THOSE WILL USE ON FRONT-END EMAIL NOTIFICATION CONFIGURATION
	 */
	PRODUCT_APPROVED_NOTIFICATION(0, "Product Approved Notification"),
	PRODUCT_APPROVED_NOTIFICATION_FOR_SCP_ADMIN(1, "Product Approved Notification For SCP Admin"),
	PRODUCT_BATCH_UPLOAD_RESULT_NOTIFICATION(2, "Product Batch Upload Result Notification"),
	PRODUCT_CERTIFICATE_APPROVAL_NOTIFICATION(3, "Product Certificate Approval Notification"),
	PRODUCT_CERTIFICATE_EXPIRY_NOTIFICATION(4, "Product Certificate Expiry Notification"),
	PRODUCT_REJECTED_NOTIFICATION(5, "Product Rejected Notification"),
	REQUEST_TO_APPROVE_VENDOR_REGISTRATION_NOTIFICATION(6, "Request to Approve Vendor Registration Notification"),
	REQUEST_TO_REVIEW_VENDOR_REGISTRATION_NOTIFICATION(7, "Request to Review Vendor Registration Notification"),
	VENDOR_USER_REGISTRATION_APPROVED_NOTIFICATION(8, "Vendor User Registration Approved Notification"),
	USER_REGISTRATION_APPROVED_NOTIFICATION_FOR_SCP_ADMIN(9, "User Registration Approved Notification For SCP Admin"),
	USER_REGISTRATION_REJECTED_NOTIFICATION(10, "User Registration Rejected Notification"),
	VENDOR_PROFILE_CERTIFICATE_APPROVAL_NOTIFICATION(11, "Vendor Profile Certificate Approval Notification"),
	VENDOR_PROFILE_CERTIFICATE_EXPIRY_NOTIFICATION(12, "Vendor Profile Certificate Expiry Notification"),
	VENDOR_REGISTRATION_APPROVED_NOTIFICATION(13, "Vendor Registration Approved Notification"),
	VENDOR_REGISTRATION_NOTIFICATION(14, "Vendor Registration Notification"),
	VENDOR_REGISTRATION_REJECTED_NOTIFICATION(15, "Vendor Registration Rejected Notification"),
	VENDOR_USER_REGISTRATION_NOTIFICATION(16, "Vendor User Registration Notification"),
	NEW_VENDOR_STAFF_REGISTRATION_NOTIFICATION_FOR_VENDOR_ADMIN(17, "New Vendor Staff Registration Notification for Vendor Admin"),

	// PLEASE NOTE: Requested to remain by Cong for 18, 19, 20, might still using
	VENDOR_CREATED_NOTIFICATION(18, "Vendor Created Notification"),
	PRODUCT_CSV_PROCESS_REPORT(19, "Product CSV Process Report"),
	PRODUCT_EXCEL_PROCESS_REPORT(20, "Product Excel Process Report"),

	USER_RESET_PASSWORD_EMAIL(21, "User Reset Password Notification"),
	SCP_USER_REGISTRATION_APPROVED_NOTIFICATION(22, "SCP User Registration Approved Notification"),
	REQUEST_TO_APPROVE_PRODUCT_NOTIFICATION(23, "Request to Approve Product Notification"),
	REQUEST_TO_APPROVE_PRODUCT_CERTIFICATE_NOTIFICATION(24, "Request to Approve Product Certificate Notification"),
	REQUEST_TO_APPROVE_VENDOR_CERTIFICATE_NOTIFICATION(25, "Request to Approve Vendor Certificate Notification"),
	HOSPITAL_USER_REGISTRATION_ACTIVATION_LINK(26, "Hospital User Registration Activation Link"),
	HOSPITAL_USER_REGISTRATION_SUCCESS(27, "Hospital User Registration Success"),
	INACTIVE_SUSPENDED_VENDOR_PROFILE(28, "Inactive / Suspended Vendor Profile"),
	TERMINATE_VENDOR_PROFILE(29, "Terminate Vendor Profile"),
	INACTIVE_SUSPENDED_PRODUCT(30, "Inactive / Suspended Product"),

	CORPORATE_USER_REGISTRATION_ACTIVATION_LINK(31, "Corporate User Registration Activation Link"),
	CORPORATE_USER_REGISTRATION_SUCCESS(32, "Corporate User Registration Success"),
	INDIVIDUAL_USER_REGISTRATION_ACTIVATION_LINK(33, "Individual User Registration Activation Link"),
	INDIVIDUAL_USER_REGISTRATION_SUCCESS(34, "Individual User Registration Success");


	private Integer code;
	private String name;

	EmailTypeEnum(Integer code, String name) {
		this.code = code;
		this.name = name;
	}

	public Integer getCode() {
		return code;
	}

	public String getName() {
		return name;
	}

}
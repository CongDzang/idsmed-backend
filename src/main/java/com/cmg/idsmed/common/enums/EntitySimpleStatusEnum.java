package com.cmg.idsmed.common.enums;

public enum EntitySimpleStatusEnum {
	ACTIVE(1, "active"),
	INACTIVE(2, "inactive");
	private Integer code;
	private String name;
	EntitySimpleStatusEnum(Integer code, String name) {
		this.code = code;
		this.name = name;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}

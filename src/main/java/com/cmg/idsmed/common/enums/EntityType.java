package com.cmg.idsmed.common.enums;

public enum EntityType {
	
    PRODUCT("product"),
    VENDOR("vendor"),
    USER("user");

    private String name;

    EntityType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}

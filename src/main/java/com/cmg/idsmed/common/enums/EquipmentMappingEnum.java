package com.cmg.idsmed.common.enums;

public enum EquipmentMappingEnum {

	/**
	 * Equipment mapping enum
	 * this is for batch upload when user put Y in excel file, then it is mapping to 1
	 * When user put N in excel file, then it is mapping to 2
	 * for detail of equipment type 1 and 2 please refer to EquipmentTypeEnum
	 */
	MEDICAL_EQUIPMENT(1, "Y"),
	NON_MEDICAL_EQUIPMENT(2, "N");

	private Integer code;
	private String name;

	EquipmentMappingEnum(Integer code, String name) {
		this.code = code;
		this.name = name;
	}

	public Integer getCode() {
		return code;
	}

	public String getName() {
		return name;
	}

}
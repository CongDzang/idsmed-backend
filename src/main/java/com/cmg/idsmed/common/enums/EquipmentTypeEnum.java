package com.cmg.idsmed.common.enums;

public enum EquipmentTypeEnum {

	/**
	 * Equipment type enum
	 *
	 */
	MEDICAL_EQUIPMENT(1, "Medical Equipment"),
	NON_MEDICAL_EQUIPMENT(2, "Non Medical Equipment");

	private Integer code;
	private String name;

	EquipmentTypeEnum(Integer code, String name) {
		this.code = code;
		this.name = name;
	}

	public Integer getCode() {
		return code;
	}

	public String getName() {
		return name;
	}

}
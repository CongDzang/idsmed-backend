package com.cmg.idsmed.common.enums;

public enum FileAttachmentStatusEnum {
	PENDING_FOR_REVIEW(0, "Pending For Review"),
	PENDING_FOR_APPROVAL(1, "Pending For Approval"),
	APPROVED(2, "Approved"),
	REJECT(3, "Reject");

	private Integer code;
	private String name;
	FileAttachmentStatusEnum(Integer code, String name) {
		this.code = code;
		this.name = name;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}

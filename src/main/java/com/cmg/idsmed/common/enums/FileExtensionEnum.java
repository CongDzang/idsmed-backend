package com.cmg.idsmed.common.enums;

public enum FileExtensionEnum {

	CSV(0, "csv"),
	XLS(1, "xls"),
	XLSX(2, "xlsx");

	private Integer code;
	private String name;

	FileExtensionEnum(Integer code, String name) {
		this.code = code;
		this.name = name;
	}

	public Integer getCode() {
		return code;
	}

	public String getName() {
		return name;
	}
}

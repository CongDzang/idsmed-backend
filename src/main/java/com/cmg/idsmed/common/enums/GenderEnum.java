package com.cmg.idsmed.common.enums;

public enum GenderEnum {
	MALE(1, "male"),
	FEMALE(2, "female");

	private Integer code;
	private String name;

	GenderEnum(Integer code, String name) {
		this.code = code;
		this.name = name;
	}

	public Integer getCode() {
		return code;
	}

	public String getName() {
		return name;
	}
}

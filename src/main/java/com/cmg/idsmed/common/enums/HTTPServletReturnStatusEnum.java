package com.cmg.idsmed.common.enums;

import java.util.Arrays;
import java.util.Optional;

public enum HTTPServletReturnStatusEnum {

	AUTHORIZED(0, "Authorized"),
	UNAUTHORIZED(1, "Unauthorized");

	private Integer code;
	private String name;

	private HTTPServletReturnStatusEnum(Integer code, String name) {
		this.code = code;
		this.name = name;
	}

	public Integer getCode() {
		return code;
	}

	public String getName() {
		return name;
	}
	
	public static String getStatusNameByCode(Integer code) {
		Optional<HTTPServletReturnStatusEnum> status = Arrays.asList(HTTPServletReturnStatusEnum.values()).stream().filter(s -> s.getCode() == code).findFirst();
		return status.isPresent() ? status.get().getName() : "";
	}
	
}
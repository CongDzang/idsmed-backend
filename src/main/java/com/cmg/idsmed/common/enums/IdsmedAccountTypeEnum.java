package com.cmg.idsmed.common.enums;

public enum IdsmedAccountTypeEnum {
		IDSMED_USER(0, "idsmed user account"),
		SUBSCRIBER(1, "subscriber"),
		VENDOR_USER(2, "vendor user account"),
		HOSPITAL_USER(3, "hospital user account"),
		CORPORATE_USER(4, "Corporate user account"),
		INDIVIDUAL_USER(5, "Individual user account");
		private Integer code;
		private String name;

		IdsmedAccountTypeEnum(Integer code, String name) {
			this.code = code;
			this.name = name;
		}

		public Integer getCode() {
			return code;
		}

		public String getName() {
			return name;
		}

}

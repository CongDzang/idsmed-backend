package com.cmg.idsmed.common.enums;

public enum ImageFileTypeEnum {
//TYPE_CUSTOM = 0;
//TYPE_INT_RGB = 1;
//TYPE_INT_ARGB = 2;
//TYPE_INT_ARGB_PRE = 3;
//TYPE_INT_BGR = 4;
//TYPE_3BYTE_BGR = 5;
//TYPE_4BYTE_ABGR = 6;
//TYPE_4BYTE_ABGR_PRE = 7;
//TYPE_USHORT_565_RGB = 8;
//TYPE_USHORT_555_RGB = 9;
//TYPE_BYTE_GRAY = 10;
//TYPE_USHORT_GRAY = 11;
//TYPE_BYTE_BINARY = 12;
//TYPE_BYTE_INDEXED = 13;

	JPG(0, "jpg"),
	PNG(6, "png"),
	GIF(2, "gif");

	private Integer code;
	private String name;

	ImageFileTypeEnum(Integer code, String name) {
		this.code = code;
		this.name = name;
	}

	public Integer getCode() {
		return code;
	}

	public String getName() {
		return name;
	}
}
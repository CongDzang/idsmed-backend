package com.cmg.idsmed.common.enums;

public enum ImageVersionEnum {
	VERSION1(1, 800, 600),
	VERSION2(2, 500, 500),// for product listing
	VERSION3(3, 200, 200),// for product detail
	VERSION4(4, 100, 100);
	
	private Integer width;
	private Integer height;
	private Integer version;
	
	ImageVersionEnum(Integer version, Integer width, Integer height) {
		this.version = version;
		this.width = width;
		this.height = height;
	}

	public Integer getWidth() {
		return width;
	}

	public Integer getHeight() {
		return height;
	}

	public Integer getVersion() {
		return version;
	}

}
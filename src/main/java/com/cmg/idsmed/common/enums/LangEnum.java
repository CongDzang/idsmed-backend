package com.cmg.idsmed.common.enums;


import java.util.Locale;

public enum LangEnum {
    ENGLISH("en_GB", "English", Locale.US),
    CHINA("zh_CN", "Chinese", Locale.CHINA),
    CHINA_TAIWAN("zh_TW", "Taiwan chinese", Locale.TAIWAN),
    THAI("th_TH", "Thai", Locale.US),
    VIETNAMESE("vi_VN", "Vietnamese", Locale.US),
    INDONESIAN("in_ID", "Indonesian", Locale.US);

    private String code;
    private String name;
    private Locale locale;

    LangEnum(String code, String name, Locale locale) {
        this.code = code;
        this.name = name;
        this.locale = locale;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public Locale getLocale() {
        return locale;
    }

}

package com.cmg.idsmed.common.enums;

public enum LocationTypeEnum {
	/**
	 * 1 - local
	 * 2 - oversea
	 */
	LOCAL(1, "Local"),
	OVERSEA(2, "Oversea");
	private Integer code;
	private String name;
	LocationTypeEnum(Integer code, String name) {
		this.code = code;
		this.name = name;
	}

	public Integer getCode() {
		return code;
	}

	public String getName() {
		return name;
	}
}

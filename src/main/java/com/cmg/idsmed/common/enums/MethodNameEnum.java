package com.cmg.idsmed.common.enums;

public enum MethodNameEnum {
	GETPRODUCTLIST("getProductList"),
	GETPRODUCTDETAIL("getProductDetail"),
	GETPRODUCTSEARCHBYKEY("getProductSearchByKey"),
	GETCAREAREALISTAPI("getCareAreaListApi"),
	GETPRODUCTLISTOFNEWRECOMMEND("getProductListOfNewRecommend"),
	GETBRANDLISTAPI("getBrandListAPI"),
	GETCATEGORYLISTAPI("getCategoryListAPI"),
	GETPRODUCTLISTFILTER("getProductListFilter"),
	GETPRODUCTSEARCHBYSTR("geProductSearchByStr"),
	GETFAVOURITEBRAND("getFavouriteBrand"),
	GETSIMPLEPRODUCTDETAIL("getSimpleProductDetail"),
	GETSUGGESTEDPRODUCTS("getSuggestedProduct"),
	GETRECENTLYSEARCHPRODUCT("getRecentlySearchProduct"),
	CREATECUSTOMER("createCustomer"),
	UPDATECUSTOMER("updateCustomer");
	
	private String name;

	private MethodNameEnum(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
}
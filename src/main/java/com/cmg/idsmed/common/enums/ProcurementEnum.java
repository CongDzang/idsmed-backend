package com.cmg.idsmed.common.enums;

public enum ProcurementEnum {
	TRUE("Y", true),
	FALSE("N", false);

	private String canProcure;//get the product canProcure value from csv file
	private Boolean convertValueOfCanProcureToBoolean;//convert the product canProcure value get from csv file to boolean value

	ProcurementEnum(String canProcure, Boolean convertValueOfCanProcureToBoolean) {
		this.canProcure = canProcure;
		this.convertValueOfCanProcureToBoolean = convertValueOfCanProcureToBoolean;
	}

	public String getCanProcure() { return canProcure; }

	public Boolean getConvertValueOfCanProcureToBoolean() { return convertValueOfCanProcureToBoolean; }
}
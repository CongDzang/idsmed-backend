package com.cmg.idsmed.common.enums;

import java.util.Arrays;
import java.util.Optional;

public enum ProcurementOrderStatusEnum {

	//order be created, not pay
	CREATED(0, "Created"),
	//paid
	PAID(1, "Paid"),
	//delivered
	SHIPPED(2, "Shipped"),
	//Confirmed amount income
	RECEIVED(3, "Received"),
	//finish
	COMPLETED(8, "Completed"),
	//cancel
	CANCELLED(9, "Cancelled"),
	//vendor cancel
	VENDOR_CANCEL(10, "Vendor Cancelled");

	private Integer code;
	private String name;

	private ProcurementOrderStatusEnum(Integer code, String name) {
		this.code = code;
		this.name = name;
	}

	public Integer getCode() {
		return code;
	}

	public String getName() {
		return name;
	}
	
}
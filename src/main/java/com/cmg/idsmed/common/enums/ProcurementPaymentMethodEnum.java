package com.cmg.idsmed.common.enums;

import java.util.Arrays;
import java.util.Optional;

public enum ProcurementPaymentMethodEnum {

	WECHAT_PAYMENT(1, "WeChat payment"),
	ALIPAY(2, "Alipay"),
	OFFLINE_PAYMENTS(2, "Offline payments");

	private Integer code;
	private String name;

	private ProcurementPaymentMethodEnum(Integer code, String name) {
		this.code = code;
		this.name = name;
	}

	public Integer getCode() {
		return code;
	}

	public String getName() {
		return name;
	}
	
	public static String getStatusNameByCode(Integer code) {
		Optional<ProcurementPaymentMethodEnum> status = Arrays.asList(ProcurementPaymentMethodEnum.values()).stream().filter(s -> s.getCode() == code).findFirst();
		return status.isPresent() ? status.get().getName() : "";
	}
	
}
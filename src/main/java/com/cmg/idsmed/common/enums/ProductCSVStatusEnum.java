package com.cmg.idsmed.common.enums;

public enum ProductCSVStatusEnum {
	UPLOADED(0, "Uploaded"),
	UPLOAD_ERROR(1, "Upload Error"),
	PROCESS_IN_PROGRESS(2, "Process in progress"),
	PROCESS_DONE(3, "Process done");

	private Integer code;
	private String name;

	private ProductCSVStatusEnum(Integer code, String name) {
		this.code = code;
		this.name = name;
	}

	public Integer getCode() {
		return code;
	}

	public String getName() {
		return name;
	}
}
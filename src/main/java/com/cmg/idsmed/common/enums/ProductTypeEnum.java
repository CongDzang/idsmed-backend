package com.cmg.idsmed.common.enums;

public enum ProductTypeEnum {
	DOMESTIC(0, "Domestic"),
	IMPORTED(1, "Imported");
	private Integer code;
	private String name;

	private ProductTypeEnum(Integer code, String name) {
		this.code = code;
		this.name = name;
	}

	public Integer getCode() {
		return code;
	}

	public String getName() {
		return name;
	}
}

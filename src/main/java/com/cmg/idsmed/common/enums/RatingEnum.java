package com.cmg.idsmed.common.enums;

public enum RatingEnum {
    ZEROSTAR(0.0, "zero_star"),
    ONESTAR(1.0, "one_star"),
    TWOSTAR(2.0, "two_star"),
    THREESTAR(3.0, "three_star"),
    FOURSTAR(4.0, "four_star"),
    FIVESTAR(5.0, "five_star"),
    ALLSTAR(6.0, "all_star");

    private Double score;
    private String name;

    RatingEnum(Double score, String name) {
        this.score = score;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public Double getScore() { return score; }
}

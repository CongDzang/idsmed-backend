package com.cmg.idsmed.common.enums;

public enum ResponseStatusEnum {

	ERROR(0, "Error"),
	SUCCESS(1, "Success");

	private Integer code;
	private String name;

	ResponseStatusEnum(Integer code, String name) {
		this.code = code;
		this.name = name;
	}

	public Integer getCode() {
		return code;
	}

	public String getName() {
		return name;
	}
}
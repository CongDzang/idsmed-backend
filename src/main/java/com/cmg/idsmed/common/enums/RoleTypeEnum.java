package com.cmg.idsmed.common.enums;

public enum RoleTypeEnum {

	EXTERNAL(0, "EXTERNAL"),
	INTERNAL(1, "INTERNAL");

	private Integer code;
	private String name;

	private RoleTypeEnum(Integer code, String name) {
		this.code= code;
		this.name = name;
	}

	public Integer getCode() {
		return code;
	}

	public String getName() {
		return name;
	}
}

package com.cmg.idsmed.common.enums;

public enum SCPUserAccountNotificationSettingEnum {

	//this is for SCPUserAccountNotificationSetting
	PRODUCT_APPROVED_NOTIFICATION(0, "Product Approved Notification"),
	REQUEST_TO_APPROVE_VENDOR_REGISTRATION_NOTIFICATION(6, "Request To Approve Vendor Registration Notification"),
	REQUEST_TO_REVIEW_VENDOR_REGISTRATION_NOTIFICATION(7, "Request to Review Vendor Registration Notification"),
	VENDOR_USER_REGISTRATION_APPROVED_NOTIFICATION(8, "Vendor User Registration Approved Notification"),
	REQUEST_TO_APPROVE_VENDOR_CERTIFICATE_NOTIFICATION(25, "Request to Approve Vendor Certificate Notification");

	private Integer code;
	private String name;

	SCPUserAccountNotificationSettingEnum(Integer code, String name) {
		this.code = code;
		this.name = name;
	}

	public Integer getCode() {
		return code;
	}

	public String getName() {
		return name;
	}
}
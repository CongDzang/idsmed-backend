package com.cmg.idsmed.common.enums;

public enum SearchingHistoryUserProductTypeEnum {

	NEW("new"),
	HYSTORY("history"),
	BEST("best");

	private String type;

	SearchingHistoryUserProductTypeEnum(String type) {
		this.type = type;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}
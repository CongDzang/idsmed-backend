package com.cmg.idsmed.common.enums;

import java.util.Arrays;
import java.util.Optional;

public enum StatusEnum {

	DRAFT(0, "Draft"),
	PENDING_FOR_REVIEW(1, "Pending For Review"),
	PENDING_FOR_APPROVAL(2, "Pending For Approval"),
	APPROVED(3, "Approved"),
	REJECT(4, "Rejected"),
	INACTIVE(5, "Inactive"),
	ARCHIVED(6, "Archived"),
	TERMINATED(7, "Terminated"),
	PRODUCT_IMAGES_PROCESSING(8, "Product images processing"),
	PENDING_FOR_ACTIVATE_BY_REGISTER(9, "Pending for active by register");

	private Integer code;
	private String name;

	private StatusEnum(Integer code, String name) {
		this.code = code;
		this.name = name;
	}

	public Integer getCode() {
		return code;
	}

	public String getName() {
		return name;
	}
	
	public static String getStatusNameByCode(Integer code) {
		Optional<StatusEnum> status = Arrays.asList(StatusEnum.values()).stream().filter(s -> s.getCode() == code).findFirst();
		return status.isPresent() ? status.get().getName() : "";
	}
	
}
package com.cmg.idsmed.common.enums;

public enum TaskTypeEnum {
	VENDOR_REVIEW(1, "Vendor review", "Vendor"),
	VENDOR_APPROVE(2, "Vendor approve", "Vendor"),
	PRODUCT_REVIEW(3, "Product review", "Product"),
	PRODUCT_APPROVE(4, "Product approve", "Product"),
	USER_APPROVE(5, "User approve", "User");

	private Integer code;
	private String name;
	private String entityName;

	TaskTypeEnum(Integer code, String name, String entityName) {
		this.code = code;
		this.name = name;
		this.entityName = entityName;
	}

	public Integer getCode() {
		return code;
	}

	public String getName() {
		return name;
	}

	public String getEntityName() {
		return entityName;
	}
}

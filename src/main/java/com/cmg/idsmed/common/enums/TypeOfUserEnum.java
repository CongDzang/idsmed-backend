package com.cmg.idsmed.common.enums;

public enum TypeOfUserEnum {

	/**
	 * This is for type of user
	 *
	 */
	SCP_USER(1, "SCP User"),
	VENDOR_USER(2, "Vendor User");

	private Integer code;
	private String name;

	TypeOfUserEnum(Integer code, String name) {
		this.code = code;
		this.name = name;
	}

	public Integer getCode() {
		return code;
	}

	public String getName() {
		return name;
	}

}
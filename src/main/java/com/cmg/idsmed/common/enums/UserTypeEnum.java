package com.cmg.idsmed.common.enums;

public enum UserTypeEnum {
	SMART_SCP(1, "SmartSCP User"),
	VENDOR_USER(2, "Vendor User"),
	HOSPITAL_USER(3, "Hospital User"),
	CORPORATE_USER(4, "Company User"),
	INDIVIDUAL_USER(5, "Individual USer");
	private Integer code;
	private String name;
	UserTypeEnum(Integer code, String name) {
		this.code = code;
		this.name = name;
	}

	public Integer getCode() {
		return code;
	}
}

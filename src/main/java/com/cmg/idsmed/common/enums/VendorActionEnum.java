package com.cmg.idsmed.common.enums;

public enum VendorActionEnum {
	SUSPEND(3, "Suspend or Intactive"),
	TERMINATE(4, "Terminate"),
	RESUME(5, "Resume");
	private Integer code;
	private String name;
	VendorActionEnum(Integer code, String name) {
		this.code = code;
		this.name = name;
	}

	public Integer getCode() {
		return code;
	}

	public String getName() {
		return name;
	}
}

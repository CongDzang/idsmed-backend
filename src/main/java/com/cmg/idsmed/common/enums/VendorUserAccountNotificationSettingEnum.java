package com.cmg.idsmed.common.enums;

public enum VendorUserAccountNotificationSettingEnum {

	//this is for SCPUserAccountNotificationSetting
	PRODUCT_APPROVED_NOTIFICATION(0, "Product Approved Notification"),
	PRODUCT_BATCH_UPLOAD_RESULT_NOTIFICATION(2, "Product Batch Upload Result Notification"),
	PRODUCT_CERTIFICATE_APPROVAL_NOTIFICATION(3, "Product Certificate Approval Notification"),
	PRODUCT_CERTIFICATE_EXPIRY_NOTIFICATION(4, "Product Certificate Expiry Notification"),
	PRODUCT_REJECTED_NOTIFICATION(5, "Product Rejected Notification"),
	VENDOR_PROFILE_CERTIFICATE_APPROVAL_NOTIFICATION(11, "Vendor Profile Certificate Approval Notification"),
	VENDOR_PROFILE_CERTIFICATE_EXPIRY_NOTIFICATION(12, "Vendor Profile Certificate Expiry Notification"),
	NEW_VENDOR_STAFF_REGISTRATION_NOTIFICATION_FOR_VENDOR_ADMIN(17, "New Vendor Staff Registration Notification for Vendor Admin"),
	REQUEST_TO_APPROVE_PRODUCT_NOTIFICATION(23, "Request to Approve Product Notification"),
	REQUEST_TO_APPROVE_PRODUCT_CERTIFICATE_NOTIFICATION(24, "Request to Approve Product Certificate Notification");

	private Integer code;
	private String name;

	VendorUserAccountNotificationSettingEnum(Integer code, String name) {
		this.code = code;
		this.name = name;
	}

	public Integer getCode() {
		return code;
	}

	public String getName() {
		return name;
	}
}
package com.cmg.idsmed.common.enums;

//This enum define all verifying work flow
public enum VerifyingWorkFlowEnum {

	//VWFP stands for Verifying work flow product
	PRODUCT_VERIFYING("VWFP0001", "Product Verifying", "Product Verifying"),
	PRODUCT_CERTIFICATE_VERIFYING("VWFP0002", "Product Certificate Verifying","Product Certificate Verifying"),
	VENDOR_VERIFYING("VWV0001", "Vendor Verifying", "Vendor Verifying"),
	USER_VERIFYING("VWU0001", "User Verifying", "User Verifying");

	private String code;
	private String name;
	private String description;

	VerifyingWorkFlowEnum(String code, String name, String description) {
		this.code = code;
		this.name = name;
		this.description = description;
	}

}

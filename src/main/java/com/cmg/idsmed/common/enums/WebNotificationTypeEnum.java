package com.cmg.idsmed.common.enums;

public enum WebNotificationTypeEnum {

	/**
	 * NOTE: BE REMIND THIS NOTIFCATION TYPE ENUM CREATE FOR FUTURE PURPOSE
	 *
	 */
	USER_APPROVED(1, "User approved notification"),
	PRODUCT_APPROVED(2, "Product approved"),
	PRODUCT_REJECT(3, "Product reject"),
	VENDOR_REGISTRATION(4, "Vendor registration"),
	VENDOR_REGISTRATION_PENDING_REVIEW(5, "Vendor Registration Pending Review"),
	VENDOR_REGISTRATION_PENDING_APPROVED(6, "Vendor Registration Pending Approved"),
	VENDOR_REGISTRATION_APPROVED(7, "Vendor Registration Approved"),
	USER_REJECT(8, "User Registration Reject"),
	VENDOR_FILE_ATTACHMENT_EXPIRY_REMINDER(9, "Vendor file attachment expiry reminder"),
	PRODUCT_DOCUMENT_ATTACHMENT_EXPIRY_REMINDER(10, "Product document attachment expiry reminder"),
	VENDOR_PROFILE_CERTIFICATE_APPROVAL(11, "Vendor profile certificate approval"),
	PENDING_APPROVED_VENDOR_USER_TO_VENDOR_ADMIN(12, "New Vendor Staff Registration Notification for Vendor Admin");

	private Integer code;
	private String name;

	WebNotificationTypeEnum(Integer code, String name) {
		this.code = code;
		this.name = name;
	}

	public Integer getCode() {
		return code;
	}

	public String getName() {
		return name;
	}

}
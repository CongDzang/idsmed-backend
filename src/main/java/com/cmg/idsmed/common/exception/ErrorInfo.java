package com.cmg.idsmed.common.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.util.Pair;


public class ErrorInfo {

    @Autowired
    private MessageSource messageSource;

    private static final Logger logger = LoggerFactory.getLogger(ErrorInfo.class);

    // ====================================================================================
    // List error code greater than 1000 to avoid duplicate with http error code
    // ====================================================================================


    private int code;
    private String messages;

    // ****************************************************************************
    // !!! IMPORTANT
    // Dear my Lord! Please append new error definition at the end of this file, and
    // then increase error code by 1
    // ****************************************************************************

    public static final Pair<Integer, String> INTERNAL_SERVER_ERROR = Pair.of(1001, "error.internal.server");
    public static final Pair<Integer, String> UNKNOWN_ERROR = Pair.of(1002, "error.unknown");
    public static final Pair<Integer, String> WRONG_LOGIN_INFO = Pair.of(1003, "error.wrong.login.info");
    public static final Pair<Integer, String> GENERATE_TOKEN_ERROR = Pair.of(1004, "error.generate.login.token");
    public static final Pair<Integer, String> USER_LOGIN_ID_EXIST_ERROR = Pair.of(1005, "error.user.login.id.exist");
    public static final Pair<Integer, String> KEYWORD_IS_EMPTY_ERROR = Pair.of(1006, "error.keyword.is.empty");
    public static final Pair<Integer, String> PRODUCT_EXISTS_ERROR = Pair.of(1007, "error.product.exists");
    public static final Pair<Integer, String> UPLOAD_FILE_ERROR = Pair.of(1008, "error.upload.file");
    public static final Pair<Integer, String> MD5_ENCRYPTION_ERROR = Pair.of(1009, "error.md5.encryption");
    public static final Pair<Integer, String> IP_NOT_IN_WHITE_LIST_ERROR = Pair.of(1010, "error.ip.not.in.white.list");
    public static final Pair<Integer, String> APPKEY_NOT_MATCH_ERROR = Pair.of(1011, "error.app.key.not.match");
    public static final Pair<Integer, String> AUTH_PARAM_MISSING_ERROR = Pair.of(1012, "error.auth.param.missing");
    public static final Pair<Integer, String> PRODUCT_NOT_FOUND_ERROR = Pair.of(1013, "error.product.not.found");
    public static final Pair<Integer, String> VENDOR_EXISTS_ERROR = Pair.of(1014, "error.vendor.exists");
    public static final Pair<Integer, String> VENDOR_STATUS_NOT_VALID_ERROR = Pair.of(1015, "error.vendor.status.not.valid");
    public static final Pair<Integer, String> USER_ID_NOT_EXISTED = Pair.of(1016, "error.user.id.not.exist.valid");
    public static final Pair<Integer, String> LOGIN_ID_NOT_EXISTED = Pair.of(1017, "error.user.loginid.not.exist.valid");
    public static final Pair<Integer, String> VENDOR_NOT_APPROVED = Pair.of(1018, "error.user.approve.vendor.not.approved");
    public static final Pair<Integer, String> VENDOR_ID_NOT_EXISTED = Pair.of(1019, "error.vendor.id.not.exist.valid");
    public static final Pair<Integer, String> PRODUCT_ID_NOT_EXISTED = Pair.of(1020, "error.product.id.not.exist.valid");
    public static final Pair<Integer, String> VENDOR_NOT_FOUND_ERROR = Pair.of(1021, "error.vendor.not.found");
    public static final Pair<Integer, String> ROLE_NAME_EXIST_ERROR = Pair.of(1022, "error.role.name.exist.valid");
    public static final Pair<Integer, String> ROLE_NOT_FOUND_ERROR = Pair.of(1023, "error.role.not.found");
    public static final Pair<Integer, String> CANNOT_SEND_EMAILS_ERROR = Pair.of(1024, "error.send.email");
    public static final Pair<Integer, String> DOWNLOAD_FILE_ERROR_BROCHURE = Pair.of(1025, "error.download.file.brochure");
    public static final Pair<Integer, String> DOWNLOAD_FILE_ERROR_VIDEO = Pair.of(1026, "error.download.file.video");
    public static final Pair<Integer, String> DOWNLOAD_IMAGE_FILE_ERROR = Pair.of(1027, "error.download.file.image");
    public static final Pair<Integer, String> DOWNLOAD_FILE_ERROR = Pair.of(1028, "error.download.file");
    public static final Pair<Integer, String> PRODUCT_CATEGORY_NOT_FOUND = Pair.of(1029, "error.product.category.not.found");
    public static final Pair<Integer, String> PRODUCT_CARE_AREA_NOT_FOUND = Pair.of(1030, "error.product.care.area.not.found");
    public static final Pair<Integer, String> FILE_NOT_FOUND_ERROR = Pair.of(1031, "error.file.not.found");
    public static final Pair<Integer, String> ACCOUNT_NOT_EXIST_ERROR = Pair.of(1032, "error.account.not.exist");
    public static final Pair<Integer, String> WRONG_OLD_PASSWORD_ERROR = Pair.of(1033, "error.wrong.old.password");
    public static final Pair<Integer, String> DOWNLOAD_FILE_FROM_OSS_ERROR = Pair.of(1034, "error.download.file.from.oss");
    public static final Pair<Integer, String> ACCOUNT_SETTINGS_MISSING_PARAMETERS = Pair.of(1035, "error.account.settings.params.missing");
    public static final Pair<Integer, String> EXCEL_TEMPLATE_FORMAT_NOT_CORRECT = Pair.of(1036, "error.excel.template.format.not.correct");
    public static final Pair<Integer, String> COMPANY_CODE_NOT_EXIST = Pair.of(1037, "error.company.code.not.exist");
    public static final Pair<Integer, String> PRODUCT_INFO_CONFIG_NOT_FOUND_ERROR = Pair.of(1038, "error.product.info.config.not.found");
    public static final Pair<Integer, String> COUNTRY_CAN_NOT_FOUND_ERROR = Pair.of(1039, "error.country.not.found");
    public static final Pair<Integer, String> CONFIG_EXISTS_ERROR = Pair.of(1040, "error.config.exists");
    public static final Pair<Integer, String> PRODUCT_CONFIG_EXIST_ERROR = Pair.of(1041, "error.product.config.exist");
    public static final Pair<Integer, String> VENDOR_INFO_CONFIG_NOT_FOUND_ERROR = Pair.of(1042, "error.vendor.info.config.not.found");
    public static final Pair<Integer, String> EMAIL_TEMPLATE_ID_NOT_FOUND_ERROR = Pair.of(1043, "error.email.template.id.not.found");
    public static final Pair<Integer, String> EMAIL_TEMPLATE_NAME_EXIST_ERROR = Pair.of(1044, "error.email.template.name.exists");
    public static final Pair<Integer, String> DUPLICATE_FILE_NAME_ERROR = Pair.of(1045, "error.duplicate.file.name");
    public static final Pair<Integer, String> PRODUCT_LABEL_CONFIG_IS_EMPTY_ERROR = Pair.of(1046, "error.product.label.config.is.empty");
    public static final Pair<Integer, String> VENDOR_LABEL_CONFIG_IS_EMPTY_ERROR = Pair.of(1046, "error.vendor.label.config.is.empty");
    public static final Pair<Integer, String> VENDOR_RE_REGISTER_NOT_FOUND_ERROR = Pair.of(1047, "error.vendor.re.register.not.found");
    public static final Pair<Integer, String> USER_NOT_FOUND_ERROR = Pair.of(1048, "error.user.not.found");
    public static final Pair<Integer, String> EMAIL_NOT_VALID_ERROR = Pair.of(1049, "error.email.not.valid");
    public static final Pair<Integer, String> ACCOUNT_NOT_ACTIVE_ERROR = Pair.of(1050, "error.account.not.active");
    public static final Pair<Integer, String> EMAIL_TEMPLATE_ATTACHED_TO_ACTIVE_NOTIFICATION = Pair.of(1051, "error.email.template.attached.to.active.notification");
    public static final Pair<Integer, String> PRODUCT_TECH_FEATURE_NOT_FOUND = Pair.of(1052, "error.product.tech.feature.not.found");
    public static final Pair<Integer, String> CUSTOMER_NOT_FOUND_ERROR = Pair.of(1053, "error.customer.not.found.error");
    public static final Pair<Integer, String> PRODUCT_IMAGE_RESOLUTION_ERROR = Pair.of(1054, "error.product.image.resolution");
    public static final Pair<Integer, String> EMAIL_WILDCARD_NOT_FOUND_ERROR = Pair.of(1055, "error.email.wildcard.not.found");
    public static final Pair<Integer, String> PRODUCT_NUMBER_COMPARISION_NOT_VALID_ERROR = Pair.of(1056, "error.product.number.comparison.not.valid");
    public static final Pair<Integer, String> PRODUCT_NUMBER_UNIQUE_SELLING_ADVANTAGES_NOT_VALID_ERROR = Pair.of(1057, "error.product.number.unique.selling.advantages.not.valid");
    public static final Pair<Integer, String> PROVINCE_CAN_NOT_FOUND_ERROR = Pair.of(1058, "error.province.not.found");
    public static final Pair<Integer, String> PRODUCT_SEARCH_CRITERIA_NOT_VALID_ERROR = Pair.of(1059, "error.product.search.criteria.not.valid");
    public static final Pair<Integer, String> SMALL_ORDER_NOT_FOUND = Pair.of(1060, "error.small.order.not.found");
    public static final Pair<Integer, String> ORDER_ITEM_NOT_FOUND = Pair.of(1061, "error.order.item.not.found");
    public static final Pair<Integer, String> BIG_ORDER_NOT_FOUND = Pair.of(1062, "error.big.order.not.found");
    public static final Pair<Integer, String> ORDER_DELIVERY_NOT_FOUND = Pair.of(1063, "error.order.delivery.not.found");
    public static final Pair<Integer, String> WRONG_INACTIVE_USER = Pair.of(1064, "error.wrong.inactive.user");
    public static final Pair<Integer, String> USER_RE_REGISTER_NOT_FOUND_ERROR = Pair.of(1065, "error.user.re.register.not.found");
    public static final Pair<Integer, String> ROLE_IS_USING_ERROR = Pair.of(1066, "error.role.is.using.error");
    public static final Pair<Integer, String> PRODUCT_HIERARCHY_NOT_FOUND_ERROR = Pair.of(1067, "error.product.hierarchy.not.found");
    public static final Pair<Integer, String> PRODUCT_EXCEL_FORMAT_ERROR = Pair.of(1068, "error.product.excel.format");
    public static final Pair<Integer, String> USER_LOGIN_ID_CANNOT_CONTAIN_SPACE = Pair.of(1069, "error.login.id.cannot.contain.space");
    public static final Pair<Integer, String> CHANGE_EMAIL_PROVIDER_ERROR = Pair.of(1070, "error.change.email.provider");
    public static final Pair<Integer, String> HAVE_NO_PERMISSION_ERROR = Pair.of(1071, "error.have.no.permission");
    public static final Pair<Integer, String> VENDOR_IS_NOT_ACTIVE_ERROR = Pair.of(1072, "error.vendor.is.not.active");
    public static final Pair<Integer, String> USER_CAN_NOT_ACTIVATE_ERROR = Pair.of(1073, "error.hospital.user.can.not.active");
    public static final Pair<Integer, String> LOGIN_ID_NOT_VALID_WHEN_REGISTER = Pair.of(1074, "error.loginid.not.valid.when.register");
    public static final Pair<Integer, String> USER_REQUEST_INFO_NOT_CORRECT = Pair.of(1074, "error.hospital.user.request.info.not.correct");
    public static final Pair<Integer, String> HOSPITAL_NOT_FOUND = Pair.of(1075, "error.hospital.not.found");
    public static final Pair<Integer, String> HOSPITAL_DEPARMENT_NOT_FOUND = Pair.of(1076, "error.hospital.deparment.not.found");
    public static final Pair<Integer, String> EMAIL_NOTIFICATION_CONFIGURATION_NOT_FOUND = Pair.of(1077, "error.email.notification.configuration.not.found");
    public static final Pair<Integer, String> CORPORATE_NOT_FOUND = Pair.of(1078, "error.corporate.not.found");
    public static final Pair<Integer, String> CORPORATE_DEPARMENT_NOT_FOUND = Pair.of(1079, "error.corporate.department.not.found");
    public static final Pair<Integer, String> USER_HAS_NOT_BEEN_SET_ADDRESS_ERROR = Pair.of(1080, "error.user.has.not.been.set.address");

    public ErrorInfo(Integer code, String messages) {
        this.code = code;
        this.messages = messages;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return messages;
    }

}

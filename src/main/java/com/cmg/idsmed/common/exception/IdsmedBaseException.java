package com.cmg.idsmed.common.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class  IdsmedBaseException extends Exception{

    private static final long serialVersionUID = 1L;

    private static final Logger logger = LoggerFactory.getLogger(IdsmedBaseException.class);

    private ErrorInfo error;

    public IdsmedBaseException(Throwable cause) {
        super(cause);
        this.setError(new ErrorInfo(ErrorInfo.INTERNAL_SERVER_ERROR.getFirst(), ErrorInfo.INTERNAL_SERVER_ERROR.getSecond()));
        logger.error("IdsmedBaseException is cause by {} ", cause.getMessage());
    }

    public IdsmedBaseException(ErrorInfo error) {
        this.setError(error);
    }

    public ErrorInfo getError() {
        return error;
    }

    public void setError(ErrorInfo error) {
        this.error = error;
    }

    @Override
    public String getMessage() {
        if(error != null) return error.getMessage();
        return super.getMessage();
    }

}

package com.cmg.idsmed.common.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;

public class DateTimeHelper {

    public static final String DATE_FORMAT = "yyyy/MM/dd";
    public static final String DATE_TIME_FORMAT = "HH:mm dd MMM yyyy";
    public static final String DATE_TIME_FORMAT_IN_REPORT = "HH:mm dd MMM yyyy";
    public static final String HOUR_MIN_PATTERN = "HH:mm";

    public static final String DATE_PATTERN = "yyyy-MM-dd";
    public static final String DATE_TIME = "yyyyMMdd";
    public static final String DATE_MONTH = "dd-MMM";
    public static final String SHORT_DATE_PATTERN = "dd MMM yyyy";
    public static final String SHORT_DATE_PATTERN_2 = "dd MMM YY";
    public static final String SHORT_TIME_PATTERN = "dd MMM yyyy HH:mm:ss";
    public static final String TIME_PATTERN = "HH:mm:ss";
    public static final String DATE_TIME_PATTERN = "yyyy-MM-dd HH:mm:ss";
    public static final String DATE_TIME_FORMAT_IN_CSV = "dd/MM/yyyy h:m:s a";
    public static final String DATE_TIME_SHORT_FORMAT_IN_CSV = "dd-MMM-yyyy";
    public static final String DATE_PERIOD_FORMAT = "dd MMM";
    public static final String MONTH_YEAR_FORMAT = "MMM-yy";
    public static final String YEAR_FORMAT = "yyyy";

    public static final String YEAR = "year";
    public static final String MONTH = "month";
    public static final String DAY_OF_YEAR = "dayOfYear";
    public static final String WEEK = "week";

    public static final String HOUR = "hour";
    public static final String MINUTE = "minute";
    public static final String SECOND = "second";
    public static final String STATE_AU = "NSW";


    private DateTimeHelper() {
    }

    @SuppressWarnings("serial")
    public static final Map<String, String> TIMEZONE = new HashMap<String, String>() {
        {
            put("ACT", "Australia/Darwin");
            put("AET", "Australia/Sydney");
            put("AGT", "America/Argentina/Buenos_Aires");
            put("ART", "Africa/Cairo");
            put("AST", "America/Anchorage");
            put("BET", "America/Sao_Paulo");
            put("BST", "Asia/Dhaka");
            put("CAT", "Africa/Harare");
            put("CNT", "America/St_Johns");
            put("CST", "America/Chicago");
            put("CTT", "Asia/Shanghai");
            put("EAT", "Africa/Addis_Ababa");
            put("ECT", "Europe/Paris");
            put("IET", "America/Indiana/Indianapolis");
            put("IST", "Asia/Kolkata");
            put("JST", "Asia/Tokyo");
            put("MIT", "Pacific/Apia");
            put("NET", "Asia/Yerevan");
            put("NST", "Pacific/Auckland");
            put("PLT", "Asia/Karachi");
            put("PNT", "America/Phoenix");
            put("PRT", "America/Puerto_Rico");
            put("PST", "America/Los_Angeles");
            put("SST", "Pacific/Guadalcanal");
            put("VST", "Asia/Ho_Chi_Minh");
            put("EST", "-05:00");
            put("MST", "-07:00");
            put("HST", "-10:00");
        }
    };

    public static LocalDateTime convertDateToLocalDateTime(Date date) {
        return Instant.ofEpochMilli(date.getTime())
                .atZone(ZoneOffset.UTC)
                .toLocalDateTime();
    }

    public static Date convertLocalDateTimeToDate(LocalDateTime localDateTime) {
        return java.util.Date
                .from(localDateTime.atZone(ZoneOffset.UTC)
                        .toInstant());
    }

    //This method get current time of system and offset it to UTC
    public static LocalDateTime getCurrentTimeUTC() {
        return OffsetDateTime.now(ZoneOffset.UTC).toLocalDateTime();
    }

    //This method convert ZoneDateTime to LocalDateTime at UTC
    public static LocalDateTime convertZonedDateTimeToUTC(ZonedDateTime dateTimeToConvert) {
        OffsetDateTime offsetDateTime = dateTimeToConvert.toOffsetDateTime().withOffsetSameInstant(ZoneOffset.UTC);
        return offsetDateTime.toLocalDateTime();
    }

    OffsetDateTime now = OffsetDateTime.now(ZoneOffset.UTC);
    ZonedDateTime currentDateTime = now.toZonedDateTime();

    public static LocalDateTime addTimeInUTC(LocalDateTime targetDate, String typeOfTime, int value) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(targetDate.toInstant(ZoneOffset.UTC).toEpochMilli());

        if (YEAR.equals(typeOfTime)) {
            calendar.add(Calendar.YEAR, value);
        } else if (MONTH.equals(typeOfTime)) {
            calendar.add(Calendar.MONTH, value);
        } else if (DAY_OF_YEAR.equals(typeOfTime)) {
            calendar.add(Calendar.DAY_OF_YEAR, value);
        } else if (WEEK.equals(typeOfTime)) {
            calendar.add(Calendar.WEEK_OF_YEAR, value);
        } else if (HOUR.equals(typeOfTime)) {
            calendar.add(Calendar.HOUR, value);
        } else if (MINUTE.equals(typeOfTime)) {
            calendar.add(Calendar.MINUTE, value);
        } else if (SECOND.equals(typeOfTime)) {
            calendar.add(Calendar.SECOND, value);
        }

        return DateTimeHelper.convertDateToLocalDateTime(calendar.getTime());
    }
    //    public static ZonedDateTime addTime(ZonedDateTime);
    public static Date addTime(Date targetDate, String typeOfTime, int value) {

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(targetDate);

        if (YEAR.equals(typeOfTime)) {
            calendar.add(Calendar.YEAR, value);
        } else if (MONTH.equals(typeOfTime)) {
            calendar.add(Calendar.MONTH, value);
        } else if (DAY_OF_YEAR.equals(typeOfTime)) {
            calendar.add(Calendar.DAY_OF_YEAR, value);
        } else if (WEEK.equals(typeOfTime)) {
            calendar.add(Calendar.WEEK_OF_YEAR, value);
        } else if (HOUR.equals(typeOfTime)) {
            calendar.add(Calendar.HOUR, value);
        } else if (MINUTE.equals(typeOfTime)) {
            calendar.add(Calendar.MINUTE, value);
        } else if (SECOND.equals(typeOfTime)) {
            calendar.add(Calendar.SECOND, value);
        }

        return calendar.getTime();
    }

    public static Date convertStringToDate(String date, String pattern) {
        SimpleDateFormat formatter = new SimpleDateFormat(pattern);
        try {
            return formatter.parse(date);
        } catch (Exception e) {
            return null;
        }
    }

    public static Date convertStringEnglishToDate(String date, String pattern) {
        SimpleDateFormat formatter = new SimpleDateFormat(pattern, Locale.ENGLISH);
        try {
            return formatter.parse(date);
        } catch (Exception e) {
            return null;
        }
    }

    public static Date convertStringToDate(String date, TimeZone tz, String pattern) {
        SimpleDateFormat formatter = new SimpleDateFormat(pattern);
        tz.useDaylightTime();
        formatter.setTimeZone(tz);
        try {
            return formatter.parse(date);
        } catch (Exception e) {
            return null;
        }
    }

    public static String convertDateToString(Date date, TimeZone tz, String pattern) {
        SimpleDateFormat formatter = new SimpleDateFormat(pattern);
        formatter.setTimeZone(tz);
        return formatter.format(date);
    }

    public static String convertDateToString(Date date, String tzId, String pattern) {
        SimpleDateFormat formatter = new SimpleDateFormat(pattern);
        formatter.setTimeZone(TimeZone.getTimeZone(tzId));
        return formatter.format(date);
    }

    public static String convertDateToString(Date date, String pattern) {
        if (date != null) {
            SimpleDateFormat formatter = new SimpleDateFormat(pattern);
            return formatter.format(date);
        }
        return null;
    }

    public static int getDateOfWeek(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal.get(Calendar.DAY_OF_WEEK);
    }

    public static Date getStartDateOfWeek(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.DAY_OF_WEEK, cal.getActualMinimum(Calendar.DAY_OF_WEEK));
        return cal.getTime();
    }

    public static Date getEndDateOfWeek(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.DAY_OF_WEEK, cal.getActualMaximum(Calendar.DAY_OF_WEEK));

        return addTime(cal.getTime(), DAY_OF_YEAR, 1);
    }

    public static Date getStartDateOfMonth(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMinimum(Calendar.DAY_OF_MONTH));
        return cal.getTime();
    }

    public static Date getEndDateOfMonth(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
        return cal.getTime();
    }

    public static Date getEndTimeOfDay(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 999);
        return calendar.getTime();
    }

    public static Date getMidTimeOfDay(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 12);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    public static Date getStartTimeOfDay(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    public static Calendar getStartTimeOfDay(Calendar date) {
        Calendar calendar = (Calendar) date.clone();
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar;
    }

    public static Calendar getEndTimeOfDay(Calendar date) {
        Calendar calendar = getStartTimeOfDay(date);
        calendar.add(Calendar.DATE, 1);
        calendar.add(Calendar.MILLISECOND, -1);
        return calendar;
    }

    public static Date getDateInFirstWeekOfNextMonth(Date date, int dateOfWeek) {

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH) + 1);

        Date newDate = calendar.getTime();
        calendar.setTime(newDate);
        calendar.set(Calendar.DAY_OF_WEEK, dateOfWeek);

        return calendar.getTime();
    }

    public static int getDifferenceDays(Date startDate, Date endDate) {

        startDate = getStartTimeOfDay(startDate);
        endDate = getEndTimeOfDay(endDate);

        long diff = endDate.getTime() - startDate.getTime();
        long diffDays = diff / (24 * 60 * 60 * 1000);
        return (int) diffDays;
    }

    public static int getDifferenceMonths(Date d1, Date d2) {

        Calendar startCalendar = new GregorianCalendar();
        startCalendar.setTime(d1);
        Calendar endCalendar = new GregorianCalendar();
        endCalendar.setTime(d2);

        int diffYear = endCalendar.get(Calendar.YEAR) - startCalendar.get(Calendar.YEAR);

        int diffMonth = diffYear * 12 + endCalendar.get(Calendar.MONTH) - startCalendar.get(Calendar.MONTH);

        return diffMonth;
    }

    public static Date getNextDay(Date d) {
        return addDaysRange(d, 0);
    }

    public static Date getPrevDay(Date d) {
        return addDaysRange(d, -2);
    }

    public static Date getStartTimeNextDay(Date d) {
        Calendar cal = new GregorianCalendar();
        cal.setTime(d);
        cal.add(Calendar.DATE, 1);

        return DateTimeHelper.getStartTimeOfDay(cal.getTime());
    }

    public static Calendar getStartTimeNextDay(Calendar date) {
        Calendar calendar = (Calendar) date.clone();
        calendar.add(Calendar.DATE, 1);

        return DateTimeHelper.getStartTimeOfDay(calendar);
    }

    public static Date addDaysRange(Date date, int days) {
        Calendar cal = Calendar.getInstance();
        long unixPlusTime = (long) (days + 1) * 24 * 60 * 60 * 1000;
        cal.setTimeInMillis(date.getTime() + unixPlusTime);

        return cal.getTime();
    }

    public static Calendar addDaysRange(Calendar date, int days) {
        Calendar cal = (Calendar) date.clone();
        cal.add(Calendar.DATE, days);

        return cal;
    }

    public static Date minusDay(Date date, int days) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);

        cal.add(Calendar.DATE, 0 - days);
        return cal.getTime();
    }

    public static Date addDaysRangeWithNoUnixPlusTime(Date date, int days) {
        Calendar cal = Calendar.getInstance();
        long unix = (long) (days) * 24 * 60 * 60 * 1000;
        cal.setTimeInMillis(date.getTime() + unix);

        return cal.getTime();
    }

    public static Integer calculateDayRange(Date startTime, Date endTime) {
        long diff = Math.abs(startTime.getTime() - endTime.getTime());
        long diffDays = diff / (24 * 60 * 60 * 1000);
        return (int) diffDays;
    }

    public static Date convertOtherTzToServerTime(Date tzTime, String tzId) {
        TimeZone tz = TimeZone.getTimeZone(tzId);
        tz.useDaylightTime();
        int offset = tz.getOffset(tzTime.getTime());

        return new Date(tzTime.getTime() - offset);
    }

    public static Date getTimeInFirstSecond(Date time) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(time);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    public static Date getDateOnly(Date inputDate) throws ParseException {

        String dateString = convertDateToString(inputDate);

        SimpleDateFormat formatter = new SimpleDateFormat(DATE_PATTERN);

        return formatter.parse(dateString);
    }

    public static String convertDateToString(Date date) {
        SimpleDateFormat formatter = new SimpleDateFormat(DATE_PATTERN);
        return formatter.format(date);
    }

    public static Date getToday() {
        try {
            return getDateOnly(Calendar.getInstance().getTime());
        } catch (ParseException e) {
            return new Date();
        }
    }

    public static Calendar getFirstDayOfWeek(Calendar date) {

        Calendar first = (Calendar) date.clone();
        first.setFirstDayOfWeek(Calendar.MONDAY);
        first.add(Calendar.DAY_OF_WEEK, first.getFirstDayOfWeek() - first.get(Calendar.DAY_OF_WEEK));

        return first;
    }

    public static Calendar getLastDayOfWeek(Calendar date) {
        Calendar cal = getFirstDayOfWeek(date);
        cal.add(Calendar.DAY_OF_YEAR, 6);
        cal.set(Calendar.HOUR, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        cal.set(Calendar.MILLISECOND, 999);

        return cal;
    }

    public static int daysBetween(Calendar day1, Calendar day2) {
        Calendar dayOne = (Calendar) day1.clone(), dayTwo = (Calendar) day2.clone();

        if (dayOne.get(Calendar.YEAR) == dayTwo.get(Calendar.YEAR)) {
            return dayOne.get(Calendar.DAY_OF_YEAR) - dayTwo.get(Calendar.DAY_OF_YEAR);
        } else {
            if (dayTwo.get(Calendar.YEAR) > dayOne.get(Calendar.YEAR)) {
                // swap them
                Calendar temp = dayOne;
                dayOne = dayTwo;
                dayTwo = temp;
            }
            int extraDays = 0;

            int dayOneOriginalYearDays = dayOne.get(Calendar.DAY_OF_YEAR);

            while (dayOne.get(Calendar.YEAR) > dayTwo.get(Calendar.YEAR)) {
                dayOne.add(Calendar.YEAR, -1);
                // getActualMaximum() important for leap years
                extraDays += dayOne.getActualMaximum(Calendar.DAY_OF_YEAR);
            }

            return extraDays - dayTwo.get(Calendar.DAY_OF_YEAR) + dayOneOriginalYearDays;
        }
    }

    public static Date getMax(Date d1, Date d2) {
        if (d1 == null && d2 == null)
            return null;

        if (d1 == null)
            return d2;

        if (d2 == null)
            return d1;

        return d1.after(d2) ? d1 : d2;
    }

    public static boolean isInRange(Date d1, Date d2) {
        return false;
    }

    public static long zonedDayBetween(Date date1, Date date2, ZoneId zone) {
        ZonedDateTime zd1 = ZonedDateTime.ofInstant(date1.toInstant(), zone).truncatedTo(ChronoUnit.DAYS);
        ZonedDateTime zd2 = ZonedDateTime.ofInstant(date2.toInstant(), zone).truncatedTo(ChronoUnit.DAYS);
        return ChronoUnit.DAYS.between(zd1, zd2);
    }

    public static Date changeDateByAddOrSubtractDays(Date date, int addedDays) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DATE, addedDays);
        return calendar.getTime();
    }

    public static String convertZoneDateTimeToString(ZonedDateTime date, String format) {
        return DateTimeFormatter.ofPattern(format).format(date);
    }

}

package com.cmg.idsmed.common.utils;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @author congdang
 */
public final class EncryptionHelper {

	private static String convertByteArrayToHexString(byte[] arrayBytes) {

		StringBuffer stringBuffer = new StringBuffer();
		for (int i = 0; i < arrayBytes.length; i++) {
			stringBuffer.append(Integer.toString(
					(arrayBytes[i] & 0xff) + 0x100, 16).substring(1));
		}
		return stringBuffer.toString();
	}

	public static String md5Encrypt(String input) throws NoSuchAlgorithmException, UnsupportedEncodingException{

		String output = null;

		try {
			MessageDigest digest = MessageDigest.getInstance("MD5");
			digest.reset();
			digest.update(input.getBytes("UTF-8"));

			output = convertByteArrayToHexString(digest.digest());

		} catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
			throw e;
		}
		return output;
	}
}

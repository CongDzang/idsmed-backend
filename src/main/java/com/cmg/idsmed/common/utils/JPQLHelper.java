package com.cmg.idsmed.common.utils;

import java.util.Map;

public class JPQLHelper {
    public static StringBuilder generateJPQLStatement(Map<String, Object> mapCheckExist, StringBuilder jpql){
        if (!mapCheckExist.isEmpty()) {
            for (String key : mapCheckExist.keySet()) {
                Object value = mapCheckExist.get(key);

                if (value instanceof String) {
                    jpql.append("lower(" + key + ") LIKE LOWER (CONCAT ('%" + value + "%'))");
                }
                if (mapCheckExist.get(key) instanceof Integer) {
                    jpql.append(key + " = " + value);
                }
                jpql.append(" AND ");
            }
            jpql.replace(0, jpql.toString().length(), jpql.toString().substring(0, jpql.toString().length() - 4));
        }
        return jpql;
    }
}

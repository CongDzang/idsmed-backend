package com.cmg.idsmed.common.utils;

import com.cmg.idsmed.common.enums.CountryCodeEnum;
import com.cmg.idsmed.common.enums.LangEnum;
import org.springframework.util.StringUtils;

import java.util.Locale;

public class LangHelper {

    public static Locale getLocale(String langCode) {
        if (StringUtils.isEmpty(langCode)) {
            return Locale.US;
        }
        for (LangEnum le : LangEnum.values()) {
            if (langCode.equals(le.getCode())) {
                return le.getLocale();
            }
        }
        return Locale.US;
    }

    public static String getLangCode(String langCode) {
        if (StringUtils.isEmpty(langCode)) {
            return LangEnum.CHINA.getCode();
        }

        for (LangEnum lang : LangEnum.values()) {
            if (langCode.equals(lang.getCode())) return langCode;
        }

        return LangEnum.CHINA.getCode();
    }

    public static String getCountryCode(String countryCode) {
        if (StringUtils.isEmpty(countryCode)) {
            return CountryCodeEnum.CHINA.getCode();
        }

        for (CountryCodeEnum code : CountryCodeEnum.values()) {
            if (code.getCode().equals(countryCode)) return code.getCode();
        }

        return CountryCodeEnum.CHINA.getCode();
    }
}

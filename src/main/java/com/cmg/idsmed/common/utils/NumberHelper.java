package com.cmg.idsmed.common.utils;

import com.cmg.idsmed.model.entity.product.ProductRating;

import java.math.BigDecimal;

public class NumberHelper {
    public static Double round(Double num) {
        return BigDecimal.valueOf(num).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
    }

    public static Double roundUp(Double num) {
        return BigDecimal.valueOf(num).setScale(2, BigDecimal.ROUND_UP).doubleValue();
    }

    public static Double round1Decimal(Double num) {
        return Math.round(num * 2) / 2.0;
    }

    public static Double roundUp1Decimal(Double num) {
        return BigDecimal.valueOf(num).setScale(1, BigDecimal.ROUND_UP).doubleValue();
    }

    public static Double calculateAverageScore(ProductRating productRating){

        return round1Decimal(productRating.getTotalOfScore() / productRating.getTotalCountOfRating());
    }
}
package com.cmg.idsmed.common.utils;

import java.io.InputStream;

import com.cmg.idsmed.common.Constant.PropertiesLabelConst;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.aliyun.oss.ClientException;
import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.OSSException;
import com.aliyun.oss.model.CompleteMultipartUploadResult;
import com.aliyun.oss.model.UploadFileRequest;
import com.aliyun.oss.model.UploadFileResult;
import org.springframework.util.StringUtils;

/**
 * Examples of uploading with enabling checkpoint file.
 *
 */
@Service
@Transactional
@PropertySource({"classpath:${SYSTEM_ENVIRONMENT_VARIABLE}_fileconfig.properties", "classpath:${SYSTEM_ENVIRONMENT_VARIABLE}_security.properties"})
public class OssUtils {
	@Autowired
	private Environment env;

	@Value("${oss.endpoint}")
	private String ossEndPoint;

	@Value("${oss.access.key}")
	private String ossAccessKey;

	@Value("${oss.access.key.secret}")
	private String ossAccessKeySecret;

	@Value("${oss.storage.bucket.name}")
	private String bucketName;

	private static final Logger logger = LoggerFactory.getLogger(OssUtils.class);

    public static final String OSS_MAX_CONCURRENT_UPLOAD_TASK_LABEL = "oss.max.concurrent.upload.task";
    
    public static final String IDSMED_PRODUCT_OSS_UPLOAD_PATH = "oss.storage.idsmedmedia.product.path";
    public static final String IDSMED_PRODUCT_CSV_OSS_UPLOAD_PATH = "oss.storage.idsmedmedia.product.csv.path";
    public static final String IDSMED_PRODUCT_EXCEL_OSS_UPLOAD_PATH = "oss.storage.idsmedmedia.product.excel.path";
    public static final String IDSMED_VENDOR_OSS_UPLOAD_PATH = "oss.storage.idsmedmedia.vendor.path";
    public static final String IDSMED_USER_OSS_UPLOAD_PATH = "oss.storage.idsmedmedia.user.path";

    public String uploadFile(String uploadFilePath, String uploadFilename, String localFilePath) {
    	OSS ossClient = new OSSClientBuilder().build(ossEndPoint, ossAccessKey, ossAccessKeySecret);
    	
    	try {
    		String key = uploadFilePath + "/" + uploadFilename;
            UploadFileRequest uploadFileRequest = new UploadFileRequest(bucketName, key);
            // The local file to upload---it must exist.
            uploadFileRequest.setUploadFile(localFilePath);
            // Sets the concurrent upload task number.
			int concurrentUploadTaskNum = Integer.parseInt(env.getProperty(OSS_MAX_CONCURRENT_UPLOAD_TASK_LABEL));
            uploadFileRequest.setTaskNum(concurrentUploadTaskNum);
            // Sets the part size to 1MB.
            uploadFileRequest.setPartSize(1024 * 1024 * 1);
            // Enables the checkpoint file. By default it's off.
            uploadFileRequest.setEnableCheckpoint(true);
            
            UploadFileResult uploadResult = ossClient.uploadFile(uploadFileRequest);
            
            CompleteMultipartUploadResult multipartUploadResult = 
                    uploadResult.getMultipartUploadResult();
            String url = multipartUploadResult.getLocation();
            String relativePath = null;
            String ossBucketDomain = env.getProperty(PropertiesLabelConst.OSS_BUCKET_DOMAIN);
            if (!StringUtils.isEmpty(url) && !StringUtils.isEmpty(ossBucketDomain)) {
				relativePath = url.replace(ossBucketDomain, "");
			}

            return relativePath;
        } catch (OSSException oe) {
    		logger.error("Upload to Oss was rejected with Error message: {}; Error Code: {} ; Request ID: {}; Host ID: {}"
					, oe.getErrorCode(), oe.getErrorCode(), oe.getRequestId(), oe.getHostId());

        } catch (ClientException ce) {
    		logger.error("client encountered a serious internal problem while trying to communicate with OSS: Error Message: {}", ce.getMessage());
        } catch (Throwable e) {
            e.printStackTrace();
        } finally {
            ossClient.shutdown();
        }
    	return "";
    }

    public String uploadFileByInputStream(String uploadFilePath, String uploadFilename, InputStream inputStream) {
    	OSS ossClient = new OSSClientBuilder().build(ossEndPoint, ossAccessKey, ossAccessKeySecret);
    	try {
    		String key = uploadFilePath + "/" + uploadFilename;
    		ossClient.putObject(bucketName, key, inputStream);
    		if(ossEndPoint.contains("https"))
            	return ossEndPoint.replace("https://", "https://" + bucketName + ".").concat("/" + key);
    		return ossEndPoint.replace("http://", "http://" + bucketName + ".").concat("/" + key);
        } catch (OSSException oe) {
			logger.error("Upload to Oss was rejected with Error message: {}; Error Code: {} ; Request ID: {}; Host ID: {}"
					, oe.getErrorCode(), oe.getErrorCode(), oe.getRequestId(), oe.getHostId());
        } catch (ClientException ce) {
			logger.error("client encountered a serious internal problem while trying to communicate with OSS: Error Message: {}", ce.getMessage());
        } catch (Throwable e) {
            e.printStackTrace();
        } finally {
            ossClient.shutdown();
        }
    	return "";
    }
    
    public boolean deleteOssObjectByKey(String url) {
    	logger.info("Starting delete Oss object");
		String firstPart = null;
		if(ossEndPoint.contains("https"))
			 firstPart = ossEndPoint.replace("https://", "https://" + bucketName + ".");
		firstPart = ossEndPoint.replace("http://", "http://" + bucketName + ".");
    	if (url.indexOf(firstPart) != 0) {
    		logger.error("The url is not valid. Should contain the bucket url : " + firstPart);
    	}
    	
    	String fileKey = url.substring(firstPart.length()+1);
    	
    	OSS ossClient = new OSSClientBuilder().build(ossEndPoint, ossAccessKey, ossAccessKeySecret);
    	try {
    		ossClient.deleteObject(bucketName, fileKey);
    		logger.info("Oss Object deleted successfully");
    		return true;
        } catch (OSSException oe) {
			logger.error("Upload to Oss was rejected with Error message: {}; Error Code: {} ; Request ID: {}; Host ID: {}"
					, oe.getErrorCode(), oe.getErrorCode(), oe.getRequestId(), oe.getHostId());
        } catch (ClientException ce) {
			logger.error("client encountered a serious internal problem while trying to communicate with OSS: Error Message: {}", ce.getMessage());
        } catch (Throwable e) {
            e.printStackTrace();
        } finally {
            ossClient.shutdown();
        }
    	return false;
    }

    public String copyOssObjectFromOssUrl(String sourceUrl, String destinationBucketName) {
    	String destinationUrl = "";

    	return destinationUrl;
	}
}

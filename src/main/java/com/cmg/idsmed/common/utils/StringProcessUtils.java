package com.cmg.idsmed.common.utils;

import org.springframework.data.util.Pair;
import org.springframework.util.CollectionUtils;

import com.cmg.idsmed.common.enums.StatusEnum;
import org.springframework.util.StringUtils;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

public class StringProcessUtils {
	public static String createFullTextSearchQuery(String entityName, List<String> searchFields, List<Pair<String, String>> relatedSearchFields, Boolean hasKewword) {
		StringBuilder jpql = new StringBuilder();
		if (hasKewword.equals(true)) {

			jpql.append("SELECT e FROM ");
			jpql.append(entityName);
			jpql.append(" e WHERE (LOWER(e.");
			jpql.append(searchFields.get(0));
			jpql.append(") LIKE CONCAT('%',LOWER(:keyword),'%') ");
			if (!CollectionUtils.isEmpty(relatedSearchFields)) {

				for(Pair<String, String> p : relatedSearchFields) {
					jpql.append("OR LOWER(e.");
					jpql.append(p.getFirst());
					jpql.append(".");
					jpql.append(p.getSecond());
					jpql.append(") LIKE CONCAT('%',LOWER(:keyword),'%')");
				}
			}
			if (searchFields.size() > 1) {
				for (int i = 1; i < searchFields.size(); i++) {
					jpql.append(" OR ");
					jpql.append("LOWER(e.");
					jpql.append(searchFields.get(i));
					jpql.append(") LIKE CONCAT('%',LOWER(:keyword),'%')");
				}
			}
			jpql.append(")");
		} else {
			jpql.append("SELECT e FROM ");
			jpql.append(entityName);
			jpql.append(" e");
		}
		return jpql.toString();
	}

	public static String createFullTextSearchQueryForTotalCount(String entityName, List<String> searchFields, List<Pair<String, String>> relatedSearchFields, Boolean hasKewword) {
		StringBuilder jpql = new StringBuilder();
		if (hasKewword.equals(true)) {

			jpql.append("SELECT count(e) FROM ");
			jpql.append(entityName);
			jpql.append(" e WHERE (LOWER(e.");
			jpql.append(searchFields.get(0));
			jpql.append(") LIKE CONCAT('%',LOWER(:keyword),'%') ");
			if (!CollectionUtils.isEmpty(relatedSearchFields)) {

				for(Pair<String, String> p : relatedSearchFields) {
					jpql.append("OR LOWER(e.");
					jpql.append(p.getFirst());
					jpql.append(".");
					jpql.append(p.getSecond());
					jpql.append(") LIKE CONCAT('%',LOWER(:keyword),'%')");
				}
			}
			if (searchFields.size() > 1) {
				for (int i = 1; i < searchFields.size(); i++) {
					jpql.append(" OR ");
					jpql.append("LOWER(e.");
					jpql.append(searchFields.get(i));
					jpql.append(") LIKE CONCAT('%',LOWER(:keyword),'%')");
				}
			}
			jpql.append(")");
		} else {
			jpql.append("SELECT count(e) FROM ");
			jpql.append(entityName);
			jpql.append(" e");
		}
		return jpql.toString();
	}

	public static String createFullTextSearchQueryForProductRating(String entityName, List<String> searchFields, List<Pair<String, String>> relatedSearchFields, Boolean hasKewword) {
		StringBuilder jpql = new StringBuilder();
		if (hasKewword.equals(true)) {

			jpql.append("SELECT e FROM ");
			jpql.append(entityName);
			jpql.append(" pr JOIN pr.product e WHERE (LOWER(e.");
			jpql.append(searchFields.get(0));
			jpql.append(") LIKE CONCAT('%',LOWER(:keyword),'%') ");
			if (!CollectionUtils.isEmpty(relatedSearchFields)) {

				for(Pair<String, String> p : relatedSearchFields) {
					jpql.append("OR LOWER(e.");
					jpql.append(p.getFirst());
					jpql.append(".");
					jpql.append(p.getSecond());
					jpql.append(") LIKE CONCAT('%',LOWER(:keyword),'%')");
				}
			}
			if (searchFields.size() > 1) {
				for (int i = 1; i < searchFields.size(); i++) {
					jpql.append(" OR ");
					jpql.append("LOWER(e.");
					jpql.append(searchFields.get(i));
					jpql.append(") LIKE CONCAT('%',LOWER(:keyword),'%')");
				}
			}
			jpql.append(")");
		} else {
			jpql.append("SELECT e FROM ");
			jpql.append(entityName);
			jpql.append(" pr JOIN pr.product e");
		}
		return jpql.toString();
	}

	public static String createFullTextSearchQueryForTotalCountForProductRating(String entityName, List<String> searchFields, List<Pair<String, String>> relatedSearchFields, Boolean hasKewword) {
		StringBuilder jpql = new StringBuilder();
		if (hasKewword.equals(true)) {

			jpql.append("SELECT count(e) FROM ");
			jpql.append(entityName);
			jpql.append(" pr join pr.product e WHERE (LOWER(e.");
			jpql.append(searchFields.get(0));
			jpql.append(") LIKE CONCAT('%',LOWER(:keyword),'%') ");
			if (!CollectionUtils.isEmpty(relatedSearchFields)) {

				for(Pair<String, String> p : relatedSearchFields) {
					jpql.append("OR LOWER(e.");
					jpql.append(p.getFirst());
					jpql.append(".");
					jpql.append(p.getSecond());
					jpql.append(") LIKE CONCAT('%',LOWER(:keyword),'%')");
				}
			}
			if (searchFields.size() > 1) {
				for (int i = 1; i < searchFields.size(); i++) {
					jpql.append(" OR ");
					jpql.append("LOWER(e.");
					jpql.append(searchFields.get(i));
					jpql.append(") LIKE CONCAT('%',LOWER(:keyword),'%')");
				}
			}
			jpql.append(")");
		} else {
			jpql.append("SELECT count(e) FROM ");
			jpql.append(entityName);
			jpql.append(" pr JOIN pr.product e");
		}
		return jpql.toString();
	}

	public static String createFullTextSearchQueryForBestSeller(String entityName, List<String> searchFields, List<Pair<String, String>> relatedSearchFields, Boolean hasKewword) {
		StringBuilder jpql = new StringBuilder();
		if (hasKewword.equals(true)) {

			jpql.append("SELECT e FROM ");
			jpql.append(entityName);
			jpql.append(" bspg JOIN bspg.product e WHERE (LOWER(e.");
			jpql.append(searchFields.get(0));
			jpql.append(") LIKE CONCAT('%',LOWER(:keyword),'%') ");
			if (!CollectionUtils.isEmpty(relatedSearchFields)) {

				for(Pair<String, String> p : relatedSearchFields) {
					jpql.append("OR LOWER(e.");
					jpql.append(p.getFirst());
					jpql.append(".");
					jpql.append(p.getSecond());
					jpql.append(") LIKE CONCAT('%',LOWER(:keyword),'%')");
				}
			}
			if (searchFields.size() > 1) {
				for (int i = 1; i < searchFields.size(); i++) {
					jpql.append(" OR ");
					jpql.append("LOWER(e.");
					jpql.append(searchFields.get(i));
					jpql.append(") LIKE CONCAT('%',LOWER(:keyword),'%')");
				}
			}
			jpql.append(")");
		} else {
			jpql.append("SELECT e FROM ");
			jpql.append(entityName);
			jpql.append(" bspg JOIN bspg.product e");
		}
		return jpql.toString();
	}

	public static String createFullTextSearchQueryForTotalCountForBestSeller(String entityName, List<String> searchFields, List<Pair<String, String>> relatedSearchFields, Boolean hasKewword) {
		StringBuilder jpql = new StringBuilder();
		if (hasKewword.equals(true)) {

			jpql.append("SELECT count(e) FROM ");
			jpql.append(entityName);
			jpql.append(" bspg join bspg.product e WHERE (LOWER(e.");
			jpql.append(searchFields.get(0));
			jpql.append(") LIKE CONCAT('%',LOWER(:keyword),'%') ");
			if (!CollectionUtils.isEmpty(relatedSearchFields)) {

				for(Pair<String, String> p : relatedSearchFields) {
					jpql.append("OR LOWER(e.");
					jpql.append(p.getFirst());
					jpql.append(".");
					jpql.append(p.getSecond());
					jpql.append(") LIKE CONCAT('%',LOWER(:keyword),'%')");
				}
			}
			if (searchFields.size() > 1) {
				for (int i = 1; i < searchFields.size(); i++) {
					jpql.append(" OR ");
					jpql.append("LOWER(e.");
					jpql.append(searchFields.get(i));
					jpql.append(") LIKE CONCAT('%',LOWER(:keyword),'%')");
				}
			}
			jpql.append(")");
		} else {
			jpql.append("SELECT count(e) FROM ");
			jpql.append(entityName);
			jpql.append(" bspg JOIN bspg.product e");
		}
		return jpql.toString();
	}

	public static Boolean hasWhiteSpace(String stringToCheck){
		Pattern pattern = Pattern.compile("\\s");
		/*if(!StringUtils.isEmpty(stringToCheck)){
			return true;
		}

		if(pattern.matcher(stringToCheck).find()){
			return true;
		}

		return false;*/

	 	if (pattern.matcher(stringToCheck).find()) return true;
		if (StringUtils.isEmpty(stringToCheck)) return true;
		return false;
	}
}
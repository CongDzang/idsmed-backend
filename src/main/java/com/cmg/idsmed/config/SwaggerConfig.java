package com.cmg.idsmed.config;

import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.SecurityScheme;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.ApiKeyVehicle;
import springfox.documentation.swagger.web.SecurityConfiguration;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

	@Value("${SYSTEM_ENVIRONMENT_VARIABLE}")
	private String environment;

	@Bean
	public Docket newsApi() {

		if(environment.equals("dev") || environment.equals("test") || environment.equals("local")) {
			return new Docket(DocumentationType.SWAGGER_2)
					.select()
					.apis(RequestHandlerSelectors.any())
					.paths(PathSelectors.any())
					.build()
					.securitySchemes(Lists.newArrayList(apiKey()))
					.apiInfo(apiInfo());
		} else {
			return new Docket(DocumentationType.SWAGGER_2)
					.select()
					.apis(RequestHandlerSelectors.any())
					.paths(PathSelectors.none())
					.build()
					.securitySchemes(Lists.newArrayList(apiKey()))
					.apiInfo(apiInfo());
		}

	}

	@Bean
    SecurityConfiguration security() {
		return new SecurityConfiguration(
				null,
				null,
				null,
				null,
				"",
				ApiKeyVehicle.HEADER,
				"Bearer",
				null);
	}

	@Bean
    SecurityScheme apiKey() {
		return new ApiKey("token", "api_key", "header");
	}

	private ApiInfo apiInfo() {
		return new ApiInfoBuilder()
				.title("IDSMED")
				.description("Idsmed - a CMG's TM")
				.version("2.0")
				.build();
	}
}

package com.cmg.idsmed.config.dbconfig;

/**
 * @author congdang
 */
public interface DbConst {

    public static final String REPOSITORY_PACKAGE = "com.cmg.idsmed.model.repo";
    public static final String ENTITY_PACKAGE = "com.cmg.idsmed.model";
}

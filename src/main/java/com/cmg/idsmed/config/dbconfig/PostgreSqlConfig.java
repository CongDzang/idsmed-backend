package com.cmg.idsmed.config.dbconfig;

import com.cmg.idsmed.db.migration.config.DatabaseMigrationConfig;
import com.cmg.idsmed.dto.ResponseObject;
import com.cmg.idsmed.model.repo.CommonCustomRepositoryImpl;
import com.zaxxer.hikari.HikariDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.support.SimpleJobLauncher;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.query.spi.EvaluationContextExtension;
import org.springframework.jdbc.datasource.init.DataSourceInitializer;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.security.data.repository.query.SecurityEvaluationContextExtension;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.Properties;

/**
 * @author congdang
 */
@Configuration
@EnableTransactionManagement
@EnableBatchProcessing
@PropertySource({"classpath:postgresql.properties", "classpath:${SYSTEM_ENVIRONMENT_VARIABLE}_postgresql.properties"})
@EnableJpaRepositories(
        basePackages = {DbConst.REPOSITORY_PACKAGE},
        repositoryBaseClass = CommonCustomRepositoryImpl.class
)
public class PostgreSqlConfig {

//  @Value("classpath:/org/springframework/batch/core/schema-drop-postgresql.sql")
//  private Resource dropSpringBatchSchema;
    private static final Logger logger = LoggerFactory.getLogger(PostgreSqlConfig.class);

    @Autowired
    private Environment env;

    @Autowired
    private JobRepository jobRegistry;

    @Bean
    public JobLauncher jobLauncher() {
        SimpleJobLauncher launcher = new SimpleJobLauncher();
        launcher.setTaskExecutor(new SimpleAsyncTaskExecutor());
        launcher.setJobRepository(jobRegistry);
        return launcher;
    }

    @Bean
    public IdsmedBatchJobManager batchJobManager() {
        return new IdsmedBatchJobManager();
    }

    @Bean
    public EvaluationContextExtension securityExtension() {
        return new SecurityEvaluationContextExtension();
    }

    /**
     * We need Flyway to already have been created, do its thing before the
     * Entity Manager Factory kicks in
     */
    @Bean
    @DependsOn(DatabaseMigrationConfig.FLYWAY)
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() throws Exception {
        final LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(dataSource());
        em.setPackagesToScan(new String[] { DbConst.ENTITY_PACKAGE });

        final HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        vendorAdapter.setDatabase(Database.POSTGRESQL);
        em.setJpaVendorAdapter(vendorAdapter);
        em.setJpaProperties(additionalProperties());

        return em;
    }

    @Bean(destroyMethod = "close")
    public DataSource dataSource() throws Exception {
        final HikariDataSource dataSource = new HikariDataSource();
        dataSource.setDriverClassName(env.getProperty("dataSourceDriverClassName"));
        dataSource.setMaximumPoolSize(env.getProperty("pool.size", Integer.class, 20));
        dataSource.setConnectionTestQuery("/* ping */");
        dataSource.setIdleTimeout(env.getProperty("pool.idletime", Integer.class, 600000));
        dataSource.setMaxLifetime(4 * 60 * 60 * 1000);
        dataSource.setSchema(env.getProperty("jdbc.schema"));
        dataSource.setJdbcUrl(env.getProperty("jdbc.url"));
        dataSource.setUsername(env.getProperty("jdbc.user"));
        dataSource.setPassword(env.getProperty("jdbc.pass"));
        return dataSource;
    }

    @Bean
    public PlatformTransactionManager transactionManager() throws Exception {
        final JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory().getObject());

        return transactionManager;
    }

    @Bean
    public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
        return new PersistenceExceptionTranslationPostProcessor();
    }

    @Bean
    public DataSourceInitializer dataSourceInitializer(final DataSource dataSource) {
        final DataSourceInitializer initializer = new DataSourceInitializer();
        initializer.setDataSource(dataSource);

        final ResourceDatabasePopulator populator = new ResourceDatabasePopulator();
        initializer.setDatabasePopulator(populator);
        return initializer;
    }

    @SuppressWarnings("serial")
    private Properties additionalProperties() {

        return new Properties() {
            {
                /**
                 * Disable hbm2ddl of Hibernate, because it seems to run before
                 * Flyway runs the migration
                 */
                setProperty("hibernate.hbm2ddl.auto", env.getProperty("hibernate.hbm2ddl.auto"));
                setProperty("hibernate.dialect", env.getProperty("hibernate.dialect"));
                setProperty("hibernate.globally_quoted_identifiers", "true");
                setProperty("hibernate.jdbc.lob.non_contextual_creation", env.getProperty("hibernate.jdbc.lob.non_contextual_creation"));
            }
        };

    }

}

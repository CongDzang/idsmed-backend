package com.cmg.idsmed.config.rabbitmq;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.mail.MessagingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AcknowledgeMode;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import com.cmg.idsmed.common.exception.IdsmedBaseException;
import com.cmg.idsmed.dto.product.ProductCSVUploadedQueueMessage;
import com.cmg.idsmed.dto.product.ProductExcelUploadedQueueMessage;
import com.cmg.idsmed.mail.EmailObject;
import com.cmg.idsmed.mail.EmailService;
import com.cmg.idsmed.model.entity.common.MailSendingLog;
import com.cmg.idsmed.model.repo.common.MailSendingLogRepository;
// import com.cmg.idsmed.service.product.ProductCSVService;
import com.cmg.idsmed.service.product.ProductExcelService;

@Configuration
@EnableRabbit
@RabbitListener(
		queues = {
				"${rabbit.product.csv.process.queue}"
		},
		containerFactory = "idsmedRabbitFactory"

)

//@RabbitListener(
//		queues = {
//				"${rabbit.product.csv.process.queue}", "${rabbit.mail.service.process.queue}"
//		},
//		containerFactory = "idsmedRabbitFactory"
//
//)

@PropertySource({"classpath:${SYSTEM_ENVIRONMENT_VARIABLE}_rabbitmq.properties"})
public class IdsmedQueueConfig {
	private static final Logger logger = LoggerFactory.getLogger(IdsmedQueueConfig.class);
	public static final String RABBIT_PRODUCT_CSV_PROCESS_QUEUE = "rabbit.product.csv.process.queue";
	public static final String RABBIT_MAIL_SERVICE_PROCESS_QUEUE = "rabbit.mail.service.process.queue";
	
	private static final int MAX_CONCURRENT_CONSUMERS = 20;

	@Value("${rabbit.product.csv.process.queue}")
	private String productCSVQueueName;
	
	@Value("${rabbit.mail.service.process.queue}")
	private String mailServiceQueue;

	@Autowired
	private RabbitAdmin rabbitAdmin;

	@Autowired
	@Qualifier("rabbitMQConnectionFactory")
	private ConnectionFactory connection;

	// @Autowired
	// private ProductCSVService productCSVService;
	
	@Autowired
	private ProductExcelService productExcelService;
	
//	@Autowired
//	private EmailService emailService;
	
	@Autowired
	private MailSendingLogRepository mailLogRepository;

	private Map<String, Integer> retryCount;
	
	private static final Integer RETRY_LIMIT = 3;
	
	private void declareAndBinding(String name) {

		Queue queue = new Queue(name, true);
		rabbitAdmin.declareQueue(queue);

		TopicExchange exchange = new TopicExchange(name);
		rabbitAdmin.declareExchange(exchange);

		rabbitAdmin.declareBinding(BindingBuilder.bind(queue).to(exchange).with(name));
	}

	@PostConstruct
	public void init() {
		try {
			retryCount = new HashMap<String, Integer> ();
			declareAndBinding(productCSVQueueName);
//			declareAndBinding(mailServiceQueue);

		} catch (Exception e) {
			logger.error("Unable to init {} queues. Caused by {}", productCSVQueueName + " / " + mailServiceQueue, e.getMessage());
		}
	}

	@Bean(name = "idsmedRabbitFactory")
	public SimpleRabbitListenerContainerFactory rabbitListenerContainerFactory() {

		SimpleRabbitListenerContainerFactory container = new SimpleRabbitListenerContainerFactory();
		container.setConnectionFactory(connection);

		container.setMessageConverter(new Jackson2JsonMessageConverter());
		container.setAcknowledgeMode(AcknowledgeMode.AUTO);
		container.setMaxConcurrentConsumers(MAX_CONCURRENT_CONSUMERS);
		container.setConcurrentConsumers(MAX_CONCURRENT_CONSUMERS / 2);

		return container;
	}

	// @RabbitHandler
	// public void proccessProductCSVFile(ProductCSVUploadedQueueMessage message) {
	// 	try {
	// 		productCSVService.processProductCSV(message);
	// 	} catch (Exception e) {
	// 		logger.error("Error while process CSV file:", e.getMessage());
	// 		return;
	// 	}
	// }
	
	@RabbitHandler
	public void proccessProductExcelFile(ProductExcelUploadedQueueMessage message)
			throws Exception {
		try {
			productExcelService.processProductExcel(message);
		} catch (Exception e) {
			logger.error("Error while process Excel file:", e.getMessage());
			return;
		}

	}
	
//	@RabbitHandler
//	public void processMailSendingTask(EmailObject mailObject)
//			throws IdsmedBaseException, IOException {
//		try {
//			logger.info("Start processMailSendingTask method - send emails process from queue with {}", mailObject.getSubject());
//			emailService.sendEmail(mailObject);
//			MailSendingLog successMailLog = mailObject.generateSuccessMailSendingLog();
//			mailLogRepository.save(successMailLog);
//
//		} catch (MessagingException e) {
//			MailSendingLog failMailLog = mailObject.generateFailMailSendingLog(e.getMessage());
//			mailLogRepository.save(failMailLog);
//
//			EmailObject failingNotificationMail = mailObject.generateFailingNotificationMail(failMailLog.getId(), e.getMessage());
//			logger.error("Fail for sending the emails. Please check detail at mail sending log with Id " + failMailLog.getId());
//			try {
//				emailService.sendEmail(failingNotificationMail);
//			} catch (MessagingException e1) {
//				logger.info("Mail server error. Please check the mail server setting." + e1.getMessage());
//			}
//		} finally {
//			logger.info("End processMailSendingTask method - send emails process from queue with {}", mailObject.getSubject());
//		}
//	}

}

package com.cmg.idsmed.config.security;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.filter.GenericFilterBean;

import com.cmg.idsmed.common.enums.HTTPServletReturnStatusEnum;
import com.cmg.idsmed.service.share.WebServiceLogService;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;

public class JWTFilter extends GenericFilterBean {

    private static final String AUTHORIZATION_HEADER = "Authorization";
    private static final String AUTHORITIES_KEY = "permissions";

    private static final Logger logger = LoggerFactory.getLogger(JWTFilter.class);

    
    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) req;
        String authHeader = request.getHeader(AUTHORIZATION_HEADER);
        if (authHeader == null || !authHeader.startsWith("Bearer ")) {
            ((HttpServletResponse) res).sendError(HttpServletResponse.SC_UNAUTHORIZED, "Invalid Authorization header.");
            return;
        }

        String token = authHeader.substring(7);
        try {
        	Claims claims = Jwts.parser().setSigningKey("secretkey").parseClaimsJws(token).getBody();
        	request.setAttribute("claims", claims);
        	SecurityContextHolder.getContext().setAuthentication(getAuthentication(claims));
        	filterChain.doFilter(req, res);
		} catch (ExpiredJwtException e) {
			logger.error("Token expired. Please login again");
			((HttpServletResponse) res).setHeader("Access-Control-Allow-Origin", "*");
			((HttpServletResponse) res).setHeader("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS");
			((HttpServletResponse) res).setHeader("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With");
			((HttpServletResponse) res).setContentType("text/html");
			((HttpServletResponse) res).sendError(HttpServletResponse.SC_UNAUTHORIZED, "The token is not valid.");
            return ;
		}
    }

    public Authentication getAuthentication(Claims claims) {
        List<SimpleGrantedAuthority> authorities = new ArrayList<SimpleGrantedAuthority>();
        @SuppressWarnings("unchecked")
		List<String> permissionIds = (List<String>) claims.get(AUTHORITIES_KEY);
        for (String permissionId : permissionIds) {
            authorities.add(new SimpleGrantedAuthority(permissionId));
        }
        User principal = new User(claims.getSubject(), "", authorities);
        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(
                principal, "", authorities);
        return usernamePasswordAuthenticationToken;
    }
}

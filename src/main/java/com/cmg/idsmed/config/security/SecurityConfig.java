package com.cmg.idsmed.config.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
@ComponentScan(basePackages = {"com.cmg.idsmed.config.security"})
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    /**
     * This method for overriding configuration of web security
     * if you want to ignore some request or request pattern
     * you can specify in this method
     * when authorization feature completed please remove the "/api/** for security"
     */

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers(HttpMethod.OPTIONS, "/**")
                .antMatchers("/api/auth/**",
                        "/api/vendor/create/**", "/api/auth/facebook",
                        "/", "/resources/**", "/static/**", "/public/**",
                        "/webui/**", "/swagger-resources/configuration/ui**",
                        "/h2-console/**", "/swagger-resources/configuration/security**",
                        "/configuration/**", "/swagger-ui/**", "/swagger-resources/**",
                        "/api-docs", "/api-docs/**", "/v2/api-docs/**",
                        "/*.html", "/**/*.html", "/**/*.css", "/**/*.js", "/**/*.png",
                        "/**/*.jpg", "/**/*.gif", "/**/*.svg", "/**/*.ico", "/**/*.ttf",
                        "/**/*.woff",
                        "/api/user/create/**", "/socket/**", "/api/vendor/configuration/**",
                        "/api/product/configuration/**", "/api/country/**",
                        "/api/vendor/re-registration/**", "/api/vendor/file-attachment/**",
                        "/api/vendor/edit/**", "/api/auth/password/resetting/**",
                        "/api/masterdata/account-notification-setting/**",
                        "/api/masterdata/carearea/list/**", "/api/masterdata/verifying-work-flow-setting",
                        "/api/masterdata/currency-setting", "/api/dashboard/**",
                        "/api/dashboard/map/**", "/api/user/list-vendor-coordinator/**",
                        "/api/user/re-registration/**", "/api/user/edit/**", "/api/product/anonymous/search/**"
                        , "/api/product/top-rating-latest-approved", "/api/product/", "/api/product/add-product-info/**",
                        "/api/product/min-max-price/**", "/api/product/configuration/label-config"
                        , "/api/product/product-rating-comment/**",
                        "/api/product/comparison/**", "/api/product/anonymous/**", "/api/log/**",
                        "/api/hospital/**", "/api/vendor/vendor-coordinator-by-company-code/**", "/api/corporate/**",
                        "/api/user/individual-user", "/api/user/activation/**", "/api/product/popular-search-keyword/**");
    }

    /**
     * This method is used for overriding the HttpSecurity of the web application
     * you can specify your authentication criteria here.
     */
    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                //Authenticate all remaining URLS
                .anyRequest().fullyAuthenticated().and()
                //Add JWT filter
                .addFilterBefore(new JWTFilter(), UsernamePasswordAuthenticationFilter.class)
                //Enable basic authentication
                .httpBasic().and()
                // configuring the session as state less. Which means there is
                // no session in the server
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                // disabling the CSRF - Cross Site Request Forgery
                .csrf().disable();
    }

    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurer() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**").allowedMethods("GET", "HEAD", "OPTIONS", "POST", "PUT", "DELETE")
                        .allowedOrigins("*")
                        .allowedHeaders("*");
            }
        };
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

}

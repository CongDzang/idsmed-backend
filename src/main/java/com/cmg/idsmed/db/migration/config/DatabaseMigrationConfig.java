package com.cmg.idsmed.db.migration.config;

import org.flywaydb.core.Flyway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

import javax.sql.DataSource;

@Configuration
@PropertySource({"classpath:postgresql.properties", "classpath:${SYSTEM_ENVIRONMENT_VARIABLE}_postgresql.properties"})
public class DatabaseMigrationConfig {

    public static final String FLYWAY = "flyway";

    @Autowired
    private Environment env;

    @Autowired
    private DataSource dataSource;

    @Bean(name = FLYWAY, initMethod = "migrate")
    public Flyway flyway() throws Exception {
        Flyway flyway = new Flyway();
        flyway.setBaselineOnMigrate(true);
        flyway.setLocations("classpath:db/migration");
        flyway.setDataSource(dataSource);
        flyway.setOutOfOrder(true);
        flyway.setSqlMigrationPrefix("");
        flyway.repair();
        flyway.setValidateOnMigrate(false);
        return flyway;
    }
}

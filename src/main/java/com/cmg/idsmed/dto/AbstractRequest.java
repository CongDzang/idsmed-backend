package com.cmg.idsmed.dto;

/**
 * @author congdang
 */
public class AbstractRequest {
    private String langCode;
    private String loginId;

    public void setLangCode(String langCode) {
        this.langCode = langCode;
    }

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public String getLangCode() {
        return langCode;
    }
}

package com.cmg.idsmed.dto;

import com.cmg.idsmed.common.enums.ResponseStatusEnum;
import com.cmg.idsmed.common.exception.ErrorInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;

import java.text.MessageFormat;

public class ResponseObject<T> {

    private static final Logger logger = LoggerFactory.getLogger(ResponseObject.class);

    @Autowired
    private MessageSource messageSource;

    private Integer code = ResponseStatusEnum.SUCCESS.getCode();
    private ErrorInfo error;
    private T responseData;
    private T extraData;
    private String message;
    private String warningMessage;

    public ErrorInfo getError() {
        return error;
    }

    public void setError(ErrorInfo error) {
        this.error = error;
    }

    public T getResponseData() {
        return responseData;
    }

    public void setResponseData(T responseData) {
        this.responseData = responseData;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        Object [] objects = {};
        try {
            this.message = MessageFormat.format(message, objects);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    public T getExtraData() {
        return extraData;
    }

    public void setExtraData(T extraData) {
        this.extraData = extraData;
    }

    public String getWarningMessage() {
        return warningMessage;
    }

    public void setWarningMessage(String warningMessage) {
        this.warningMessage = warningMessage;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }
}

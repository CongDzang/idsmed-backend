package com.cmg.idsmed.dto.accessright;

import java.util.List;

import com.cmg.idsmed.dto.auth.IdsmedRoleResponse;

public class AccessRightListResponse {
	private Integer totalPage = null;
	private List<IdsmedRoleResponse> idsmedRoleResponses;
	private Integer totalCount = null;
	private Integer pageNo = null;
	private Integer pageSize = null;
	private IdsmedRoleResponse idsmedRoleResponse;

	public AccessRightListResponse(Integer pageNo, Integer pageSize) {
		this.totalPage = 0;
		this.totalCount = 0;
		this.pageSize = pageSize;
		this.pageNo = pageNo;
	}
	
	public AccessRightListResponse(Integer totalCount, Integer pageIndex, Integer pageSize, List<IdsmedRoleResponse> idsmedRoleResponse) {
		if (pageSize != null && !pageSize.equals(0)) {
			Integer calTotalPage = totalCount/pageSize;
			if(totalCount % pageSize > 0) {
				calTotalPage++;
			}
			this.totalPage = calTotalPage;
		}
		this.totalCount = totalCount;
		this.pageNo = pageIndex + 1;
		this.pageSize = pageSize;
		this.idsmedRoleResponses = idsmedRoleResponse;
	}

	public AccessRightListResponse(Integer totalCount, List<IdsmedRoleResponse> idsmedRoleResponse) {
		this.totalCount = totalCount;
		this.idsmedRoleResponses = idsmedRoleResponse;
		this.totalPage = 1;
		this.pageSize = totalCount;
		this.pageNo = 1;
	}
	
	public AccessRightListResponse(IdsmedRoleResponse idsmedRoleResponse) {
		this.idsmedRoleResponse = idsmedRoleResponse;
	}
	public Integer getTotalPage() {
		return totalPage;
	}

	public void setTotalPage(Integer totalPage) {
		this.totalPage = totalPage;
	}

	public List<IdsmedRoleResponse> getAccessRightList() {
		return idsmedRoleResponses;
	}

	public void setProductList(List<IdsmedRoleResponse> idsmedRoleResponses) {
		this.idsmedRoleResponses = idsmedRoleResponses;
	}

	public Integer getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}

	public Integer getPageNo() {
		return pageNo;
	}

	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public IdsmedRoleResponse getIdsmedRoleResponse() {
		return idsmedRoleResponse;
	}

	public void setIdsmedRoleResponse(IdsmedRoleResponse idsmedRoleResponse) {
		this.idsmedRoleResponse = idsmedRoleResponse;
	}
}

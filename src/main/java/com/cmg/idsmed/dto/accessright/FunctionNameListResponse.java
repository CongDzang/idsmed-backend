package com.cmg.idsmed.dto.accessright;

import java.util.List;

import com.cmg.idsmed.dto.auth.IdsmedRoleResponse;

public class FunctionNameListResponse {
	private List<String> functionName;

	public FunctionNameListResponse(List<String> functionName) {
			this.functionName = functionName;
	}

	public List<String> getFunctionName() {
		return functionName;
	}

	public void setFunctionName(List<String> functionName) {
		this.functionName = functionName;
	}
}

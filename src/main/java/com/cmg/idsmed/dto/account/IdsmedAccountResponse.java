package com.cmg.idsmed.dto.account;

import com.cmg.idsmed.common.Constant.IdsmedAccountCodeConst;
import com.cmg.idsmed.common.enums.StatusEnum;
import com.cmg.idsmed.dto.AbstractResponse;
import com.cmg.idsmed.model.entity.auth.IdsmedAccount;
import com.cmg.idsmed.model.entity.auth.IdsmedAccountSettings;
import com.cmg.idsmed.model.entity.auth.IdsmedUser;

;

public class IdsmedAccountResponse extends AbstractResponse {
	private Long idsmedAccountId;

	private String loginId;

	private String userFirstName;

	private String userLastName;

	public IdsmedAccountResponse() { }

	public IdsmedAccountResponse(IdsmedAccount idsmedAccount, IdsmedUser idsmedUser) {
		this.idsmedAccountId = idsmedAccount.getId();
		this.loginId = idsmedAccount.getLoginId();
		this.userFirstName = idsmedUser.getFirstName();
		if(idsmedAccount.getStatus() == StatusEnum.INACTIVE.getCode()) {
			this.userLastName = idsmedUser.getLastName() + " " + IdsmedAccountCodeConst.SUSPENDED;
		}else if(idsmedAccount.getStatus() == StatusEnum.APPROVED.getCode()){
			this.userLastName = idsmedUser.getLastName();
		}
	}

	public Long getIdsmedAccountId() { return idsmedAccountId; }

	public void setIdsmedAccountId(Long idsmedAccountId) { this.idsmedAccountId = idsmedAccountId;}

	public String getUserFirstName() { return userFirstName; }

	public void setUserFirstName(String userFirstName) { this.userFirstName = userFirstName; }

	public String getUserLastName() { return userLastName; }

	public void setUserLastName(String userLastName) { this.userLastName = userLastName; }

	public String getLoginId() { return loginId; }

	public void setLoginId(String loginId) { this.loginId = loginId; }
}

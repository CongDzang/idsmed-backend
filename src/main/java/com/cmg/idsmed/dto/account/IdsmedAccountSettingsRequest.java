package com.cmg.idsmed.dto.account;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.cmg.idsmed.dto.AbstractRequest;

public class IdsmedAccountSettingsRequest extends AbstractRequest {

    @NotNull
    private String userLoginId;

    @Size(max = 50)
    private String currentLangCode;

	public String getUserLoginId() {
		return userLoginId;
	}

	public void setUserLoginId(String userLoginId) {
		this.userLoginId = userLoginId;
	}

	public String getCurrentLangCode() {
		return currentLangCode;
	}

	public void setCurrentLangCode(String currentLangCode) {
		this.currentLangCode = currentLangCode;
	}
}

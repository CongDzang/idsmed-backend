package com.cmg.idsmed.dto.account;

import com.cmg.idsmed.dto.AbstractResponse;
import com.cmg.idsmed.model.entity.auth.IdsmedAccountSettings;;

public class IdsmedAccountSettingsResponse extends AbstractResponse {
	
	public IdsmedAccountSettingsResponse() {
		super();
	}
	
	public IdsmedAccountSettingsResponse(IdsmedAccountSettings accountSettings) {
		this.userLoginId = accountSettings.getIdsmedAccount().getLoginId();
		this.currentLangCode = accountSettings.getCurrentLangCode();
	}
	
    private String userLoginId;

    private String currentLangCode;

	public String getUserLoginId() {
		return userLoginId;
	}

	public void setUserLoginId(String userLoginId) {
		this.userLoginId = userLoginId;
	}

	public String getCurrentLangCode() {
		return currentLangCode;
	}

	public void setCurrentLangCode(String currentLangCode) {
		this.currentLangCode = currentLangCode;
	}
}

package com.cmg.idsmed.dto.auth;

import com.cmg.idsmed.dto.AbstractRequest;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class ApiAuthRequest extends AbstractRequest {

	public static final String COUNTRY_CODE_PARAMETER_NAME = "countryCode";
    public static final String CLIENT_ID_PARAMETER_NAME = "clientId";
    public static final String PASSWORD_PARAMETER_NAME = "password";
    public static final String APPKEY_PARAMETER_NAME = "appKey";
    public static final String LANGCODE_PARAMETER_NAME = "langCode";
    
	@NotNull
	@Size(max = 255)
	private String clientId;

	private String apiCode;

	@NotNull
	private String password;

	@NotNull
	private String appKey;

	private String countryCode;

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getAppKey() {
		return appKey;
	}

	public void setAppKey(String appKey) {
		this.appKey = appKey;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getApiCode() {
		return apiCode;
	}

	public void setApiCode(String apiCode) {
		this.apiCode = apiCode;
	}
}

package com.cmg.idsmed.dto.auth;

import com.cmg.idsmed.dto.AbstractResponse;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class ApiAuthResponse extends AbstractResponse {
	@NotNull
	@Size(max = 255)
	private String clientId;

	private String token;

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
}

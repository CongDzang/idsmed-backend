package com.cmg.idsmed.dto.auth;

import com.cmg.idsmed.dto.AbstractRequest;

import javax.validation.constraints.NotNull;

public class ChangePasswordRequest extends AbstractRequest {
	@NotNull
	private String loginId;

	@NotNull
	private String oldPassword;

	@NotNull
	private String newPassword;

	public ChangePasswordRequest() {
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getOldPassword() {
		return oldPassword;
	}

	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}
}

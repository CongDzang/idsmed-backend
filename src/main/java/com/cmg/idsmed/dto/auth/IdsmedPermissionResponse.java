package com.cmg.idsmed.dto.auth;

import org.springframework.beans.BeanUtils;

import com.cmg.idsmed.dto.AbstractResponse;
import com.cmg.idsmed.model.entity.auth.IdsmedPermission;

/**
 * @author ducpham
 */

public class IdsmedPermissionResponse extends AbstractResponse {

    private Long id;

    private String functionName;
    
    private String code;

    private String name;
    
    private String description;


	public IdsmedPermissionResponse() {
    }

    public IdsmedPermissionResponse(IdsmedPermission permission) {
        BeanUtils.copyProperties(permission, this);
        if(permission.getFunction() != null) {
        	this.functionName = permission.getFunction().getName();
        }
    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public String getFunctionName() {
		return functionName;
	}

	public void setFunctionName(String functionName) {
		this.functionName = functionName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}

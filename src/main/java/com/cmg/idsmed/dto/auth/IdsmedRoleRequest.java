package com.cmg.idsmed.dto.auth;

import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.cmg.idsmed.dto.AbstractRequest;

/**
 * @author chong sian
 */

public class IdsmedRoleRequest extends AbstractRequest {
    private Long id;
    private String inactiveReason;

	@NotNull
    @Size(max = 255)
    private String name;
    
    @Size(max = 1000)
    private String description;
    
    private Integer status;

    private Integer roleType;

	private Long roleId;
    @NotNull
    @Size(max = 10)
    private String countryCode;
    
    private List<Long> permissionIdList;

    public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public Integer getRoleType() {
		return roleType;
	}

	public void setRoleType(Integer roleType) {
		this.roleType = roleType;
	}

    public List<Long> getPermissionIdList() {
		return permissionIdList;
	}

	public void setPermissionIdList(List<Long> permissionIdList) {
		this.permissionIdList = permissionIdList;
	}

	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	public String getInactiveReason() {
		return inactiveReason;
	}

	public void setInactiveReason(String inactiveReason) {
		this.inactiveReason = inactiveReason;
	}
}

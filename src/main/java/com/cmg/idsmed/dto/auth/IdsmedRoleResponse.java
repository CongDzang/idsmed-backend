package com.cmg.idsmed.dto.auth;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;
import org.springframework.util.CollectionUtils;

import com.cmg.idsmed.dto.AbstractResponse;
import com.cmg.idsmed.model.entity.auth.IdsmedPermission;
import com.cmg.idsmed.model.entity.auth.IdsmedRole;
import com.cmg.idsmed.model.entity.auth.IdsmedRolePermission;

/**
 * @author congdang
 */

public class IdsmedRoleResponse extends AbstractResponse {

	private Long id;

	private String code;

	private String name;

	private String description;

	private Integer status;

	private Integer roleType;

	private String inactiveReason;

	private List<IdsmedPermissionResponse> permissions;

	Map<Long, List<IdsmedPermission>> map = new HashMap<Long, List<IdsmedPermission>>();
	
	List<IdsmedPermission> sortedPermissions = null;

	public IdsmedRoleResponse() {
	}

	public IdsmedRoleResponse(IdsmedRole role) {
		if (role != null) {
			String[] ignoreProperties = { "userRoles", "rolePermissions" };
			BeanUtils.copyProperties(role, this, ignoreProperties);
			if (!CollectionUtils.isEmpty(role.getRolePermissions())) {
				for (IdsmedRolePermission rolePermission : role.getRolePermissions()) {
					Long functionId = rolePermission.getIdsmedPermission().getFunction().getId();
					if (CollectionUtils.isEmpty(map.get(functionId))) {
						List<IdsmedPermission> tempList = new ArrayList<IdsmedPermission>();
						tempList.add(rolePermission.getIdsmedPermission());
						map.put(functionId, tempList);
					} else {
						map.get(functionId).add(rolePermission.getIdsmedPermission());
					}
				}
				Map<Long, List<IdsmedPermission>> sortedByKeyMap = new TreeMap<Long, List<IdsmedPermission>>(map);

				sortedPermissions = sortedByKeyMap.entrySet().stream().flatMap(entry -> {
					List<IdsmedPermission> permissions = entry.getValue();
					sortPermissionById(permissions);
					return permissions.stream();
				}).collect(Collectors.toList());

				this.permissions = sortedPermissions.stream().map(sp -> new IdsmedPermissionResponse(sp)).collect(Collectors.toList());
			}
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<IdsmedPermissionResponse> getPermissions() {
		return permissions;
	}

	public void setPermissions(List<IdsmedPermissionResponse> permissions) {
		this.permissions = permissions;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	private void sortPermissionById(List<IdsmedPermission> permissions) {
		permissions.sort(new Comparator<IdsmedPermission>() {
			@Override
			public int compare(IdsmedPermission o1, IdsmedPermission o2) {
				Long result = new Long(o1.getId() - o2.getId());
				return result.intValue();
			}
		});
	}

	public Integer getRoleType() {
		return roleType;
	}

	public void setRoleType(Integer roleType) {
		this.roleType = roleType;
	}

	public String getInactiveReason() {
		return inactiveReason;
	}

	public void setInactiveReason(String inactiveReason) {
		this.inactiveReason = inactiveReason;
	}
}

package com.cmg.idsmed.dto.auth;

import com.cmg.idsmed.dto.AbstractRequest;

import javax.validation.constraints.NotNull;

/**
 * @author congdang
 */

public class LoginRequest extends AbstractRequest {
    private String clientId;
    private String platformType;

    @NotNull
    private String loginId;

    @NotNull
    private String password;

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getPlatformType() {
        return platformType;
    }

    public void setPlatformType(String platformType) {
        this.platformType = platformType;
    }

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}

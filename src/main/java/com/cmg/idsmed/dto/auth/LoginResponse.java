package com.cmg.idsmed.dto.auth;

import java.util.List;

import com.cmg.idsmed.dto.AbstractResponse;

/**
 * @author congdang
 *
 */
public class LoginResponse extends AbstractResponse {

    private Long userId;
    private String userLoginId;
    private Long vendorId;
    private String currentLangCode;
    private Long accountId;
    private String companyCode;
   	private Integer userType;

	private List<IdsmedPermissionResponse> permissions;
    private List<IdsmedRoleResponse> roles;

	private String token;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserLoginId() {
        return userLoginId;
    }

    public void setUserLoginId(String userLoginId) {
        this.userLoginId = userLoginId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Long getVendorId() {
		return vendorId;
	}

	public void setVendorId(Long vendorId) {
		this.vendorId = vendorId;
	}
	
    public List<IdsmedRoleResponse> getRoles() {
		return roles;
	}

	public void setRoles(List<IdsmedRoleResponse> roles) {
		this.roles = roles;
	}

    public List<IdsmedPermissionResponse> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<IdsmedPermissionResponse> permissions) {
        this.permissions = permissions;
    }
    
    public String getCurrentLangCode() {
		return currentLangCode;
	}

	public void setCurrentLangCode(String currentLangCode) {
		this.currentLangCode = currentLangCode;
	}

	public Long getAccountId() {
		return accountId;
	}

	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}

    public String getCompanyCode() { return companyCode; }

    public void setCompanyCode(String companyCode) { this.companyCode = companyCode; }

	public Integer getUserType() {
		return userType;
	}

	public void setUserType(Integer userType) {
		this.userType = userType;
	}
}

package com.cmg.idsmed.dto.corporate;

import com.cmg.idsmed.dto.AbstractRequest;

public class CorporateDepartmentRequest extends AbstractRequest {
	private Long id;

	private String namePrimary;

	private String nameSecondary;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNamePrimary() {
		return namePrimary;
	}

	public void setNamePrimary(String namePrimary) {
		this.namePrimary = namePrimary;
	}

	public String getNameSecondary() {
		return nameSecondary;
	}

	public void setNameSecondary(String nameSecondary) {
		this.nameSecondary = nameSecondary;
	}
}

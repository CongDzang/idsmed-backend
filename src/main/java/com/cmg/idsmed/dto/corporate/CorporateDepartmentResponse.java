package com.cmg.idsmed.dto.corporate;

import com.cmg.idsmed.model.entity.corporate.CorporateDepartment;
import com.cmg.idsmed.model.entity.hospital.HospitalDepartment;

public class CorporateDepartmentResponse {
	private Long id;
	private String namePrimary;
	private String nameSecondary;
	private Long corporateId;
	private String code;
	private Integer status;

	public CorporateDepartmentResponse() {

	}

	public CorporateDepartmentResponse(CorporateDepartment corporateDepartment) {
		if (corporateDepartment != null) {
			this.id = corporateDepartment.getId();
			this.namePrimary = corporateDepartment.getNamePrimary();
			this.nameSecondary = corporateDepartment.getNameSecondary();
			if (corporateDepartment.getCorporate() != null) {
				this.corporateId = corporateDepartment.getCorporate().getId();
			}
			this.code = corporateDepartment.getCode();
			this.status = corporateDepartment.getStatus();
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNamePrimary() {
		return namePrimary;
	}

	public void setNamePrimary(String namePrimary) {
		this.namePrimary = namePrimary;
	}

	public String getNameSecondary() {
		return nameSecondary;
	}

	public void setNameSecondary(String nameSecondary) {
		this.nameSecondary = nameSecondary;
	}

	public Long getCorporateId() {
		return corporateId;
	}

	public void setCorporateId(Long corporateId) {
		this.corporateId = corporateId;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
}

package com.cmg.idsmed.dto.corporate;

import com.cmg.idsmed.dto.AbstractRequest;

public class CorporateRegistrationRequest extends AbstractRequest {
	private CorporateRequest corporateRequest;
	private CorporateUserRegistrationRequest corporateUserRegistrationRequest;
	private CorporateDepartmentRequest corporateDepartmentRequest;

	public CorporateRequest getCorporateRequest() {
		return corporateRequest;
	}

	public void setCorporateRequest(CorporateRequest corporateRequest) {
		this.corporateRequest = corporateRequest;
	}

	public CorporateUserRegistrationRequest getCorporateUserRegistrationRequest() {
		return corporateUserRegistrationRequest;
	}

	public void setCorporateUserRegistrationRequest(CorporateUserRegistrationRequest corporateUserRegistrationRequest) {
		this.corporateUserRegistrationRequest = corporateUserRegistrationRequest;
	}

	public CorporateDepartmentRequest getCorporateDepartmentRequest() {
		return corporateDepartmentRequest;
	}

	public void setCorporateDepartmentRequest(CorporateDepartmentRequest corporateDepartmentRequest) {
		this.corporateDepartmentRequest = corporateDepartmentRequest;
	}
}

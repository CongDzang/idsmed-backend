package com.cmg.idsmed.dto.corporate;

import com.cmg.idsmed.dto.AbstractResponse;
import com.cmg.idsmed.dto.masterdata.CountryResponse;
import com.cmg.idsmed.dto.masterdata.ProvinceResponse;
import com.cmg.idsmed.model.entity.corporate.Corporate;
import com.cmg.idsmed.model.entity.hospital.Hospital;

public class CorporateResponse extends AbstractResponse {
	private Long id;

	private String namePrimary;

	private String nameSecondary;

	private CountryResponse country;

	private ProvinceResponse province;

	private String code;

	private String postcode;

	private String firstAddress;

	private String secondAddress;

	private Integer status;

	public CorporateResponse() {

	}

	public CorporateResponse(Corporate corporate) {
		if (corporate != null) {
			this.id = corporate.getId();
			this.namePrimary = corporate.getNamePrimary();
			this.nameSecondary = corporate.getNameSecondary();
			if (corporate.getCountry() != null) {
				this.country = new CountryResponse(corporate.getCountry());
			}
			if (corporate.getProvince() != null) {
				this.province = new ProvinceResponse(corporate.getProvince());
			}
			this.code = corporate.getCode();
			this.postcode = corporate.getPostcode();
			this.firstAddress = corporate.getFirstAddress();
			this.secondAddress = corporate.getSecondAddress();
			this.status = corporate.getStatus();
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNamePrimary() {
		return namePrimary;
	}

	public void setNamePrimary(String namePrimary) {
		this.namePrimary = namePrimary;
	}

	public String getNameSecondary() {
		return nameSecondary;
	}

	public void setNameSecondary(String nameSecondary) {
		this.nameSecondary = nameSecondary;
	}

	public CountryResponse getCountry() {
		return country;
	}

	public void setCountry(CountryResponse country) {
		this.country = country;
	}

	public ProvinceResponse getProvince() {
		return province;
	}

	public void setProvince(ProvinceResponse province) {
		this.province = province;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getPostcode() {
		return postcode;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	public String getFirstAddress() {
		return firstAddress;
	}

	public void setFirstAddress(String firstAddress) {
		this.firstAddress = firstAddress;
	}

	public String getSecondAddress() {
		return secondAddress;
	}

	public void setSecondAddress(String secondAddress) {
		this.secondAddress = secondAddress;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
}

package com.cmg.idsmed.dto.corporate;

import com.cmg.idsmed.dto.user.UserRegistrationRequest;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

public class CorporateUserRegistrationRequest extends UserRegistrationRequest {

}

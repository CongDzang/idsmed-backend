package com.cmg.idsmed.dto.dashboard;

import com.cmg.idsmed.dto.AbstractResponse;

import java.math.BigDecimal;

public class DashboardBarChartResponse extends AbstractResponse {
  private String name;
  private Object data;

  public DashboardBarChartResponse(String name, Object data) {
    this.name = name;
    this.data = data == null ? 0: data;
  }

  public String getName() { return name; }

  public void setName(String name) { this.name = name; }

  public Object getData() { return data; }

  public void setData(Object data) { this.data = data; }
}

package com.cmg.idsmed.dto.dashboard;

import com.cmg.idsmed.dto.AbstractResponse;
import com.cmg.idsmed.model.entity.masterdata.Province;
import org.springframework.data.util.Pair;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class DashboardMapResponse extends AbstractResponse {
	private String provinceName = "";
	private Long totalVendor = 0L;
	private Long totalProduct = 0L;
	private Long totalHospitals = 0L;
	private Long totalCustomer = 0L;
	private Long totalNoOfTransaction = 0L;

	public DashboardMapResponse(String provinceName, Long totalVendor, Long totalProduct, Long totalCustomer, Long totalHospitals, Long totalNoOfTransaction) {
		this.provinceName = provinceName;
		this.totalVendor = totalVendor;
		this.totalProduct = totalProduct;
		this.totalCustomer = totalCustomer;
		this.totalHospitals = totalHospitals;
		this.totalNoOfTransaction = totalNoOfTransaction;
	}

	public String getProvinceName() {
		return provinceName;
	}

	public void setProvinceName(String provinceName) {
		this.provinceName = provinceName;
	}

	public Long getTotalVendor() {
		return totalVendor;
	}

	public void setTotalVendor(Long totalVendor) {
		this.totalVendor = totalVendor;
	}

	public Long getTotalProduct() {
		return totalProduct;
	}

	public void setTotalProduct(Long totalProduct) {
		this.totalProduct = totalProduct;
	}

	public Long getTotalHospitals() {
		return totalHospitals;
	}

	public void setTotalHospitals(Long totalHospitals) {
		this.totalHospitals = totalHospitals;
	}

	public Long getTotalCustomer() {
		return totalCustomer;
	}

	public void setTotalCustomer(Long totalCustomer) {
		this.totalCustomer = totalCustomer;
	}

	public Long getTotalNoOfTransaction() {
		return totalNoOfTransaction;
	}

	public void setTotalNoOfTransaction(Long totalNoOfTransaction) {
		this.totalNoOfTransaction = totalNoOfTransaction;
	}
}

package com.cmg.idsmed.dto.dashboard;

import com.cmg.idsmed.dto.AbstractResponse;

import java.util.ArrayList;
import java.util.List;

public class DashboardMapResponses extends AbstractResponse {
	private List<DashboardMapResponse> dashboardMapResponseList = new ArrayList<>();
	private Long totalNoOfTransactionRegardlessOfProvince = 0L;

	public DashboardMapResponses(List<DashboardMapResponse> dashboardMapResponseList, Long totalNoOfTransactionRegardlessOfProvince) {
		this.dashboardMapResponseList = dashboardMapResponseList;
		this.totalNoOfTransactionRegardlessOfProvince = totalNoOfTransactionRegardlessOfProvince;
	}

	public Long getTotalNoOfTransactionRegardlessOfProvince() {
		return totalNoOfTransactionRegardlessOfProvince;
	}

	public void setTotalNoOfTransactionRegardlessOfProvince(Long totalNoOfTransactionRegardlessOfProvince) {
		this.totalNoOfTransactionRegardlessOfProvince = totalNoOfTransactionRegardlessOfProvince;
	}

	public List<DashboardMapResponse> getDashboardMapResponseList() { return dashboardMapResponseList; }

	public void setDashboardMapResponseList(List<DashboardMapResponse> dashboardMapResponseList) { this.dashboardMapResponseList = dashboardMapResponseList; }
}

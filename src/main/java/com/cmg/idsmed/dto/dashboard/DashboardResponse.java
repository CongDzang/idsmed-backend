package com.cmg.idsmed.dto.dashboard;

import com.cmg.idsmed.dto.AbstractResponse;
import org.springframework.data.util.Pair;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class DashboardResponse extends AbstractResponse {

	private Long totalVendor;
	private Long totalProduct;
	private Long totalProductApprovedToday;
	private List<DashboardBarChartResponse> fiveMonthTotalApprovedProduct = new ArrayList<>();
	private List<DashboardBarChartResponse> fiveMonthTotalCustomer = new ArrayList<>();
	private List<DashboardBarChartResponse> fiveMonthTotalSales = new ArrayList<>();
	private List<DashboardBarChartResponse> fiveYearTotalRegisteredCustomer = new ArrayList<>();
	private List<DashboardBarChartResponse> fiveYearTotalHospital = new ArrayList<>();
	private List<DashboardBarChartResponse> fiveYearTotalSales = new ArrayList<>();

	public DashboardResponse(Long totalVendor, Long totalProduct, Long totalProductApprovedToday, List<DashboardBarChartResponse> fiveMonthTotalApprovedProduct,
							 List<DashboardBarChartResponse> fiveMonthTotalCustomer, List<DashboardBarChartResponse> fiveMonthTotalSales,
							 List<DashboardBarChartResponse> fiveYearTotalRegisteredCustomer, List<DashboardBarChartResponse> fiveYearTotalHospital,
							 List<DashboardBarChartResponse> fiveYearTotalSales) {
		this.totalVendor = totalVendor;
		this.totalProduct = totalProduct;
		this.totalProductApprovedToday = totalProductApprovedToday;
		this.fiveMonthTotalApprovedProduct = fiveMonthTotalApprovedProduct;
		this.fiveMonthTotalCustomer = fiveMonthTotalCustomer;
		this.fiveMonthTotalSales = fiveMonthTotalSales;
		this.fiveYearTotalRegisteredCustomer = fiveYearTotalRegisteredCustomer;
		this.fiveYearTotalHospital = fiveYearTotalHospital;
		this.fiveYearTotalSales = fiveYearTotalSales;
	}

	public Long getTotalVendor() {
		return totalVendor;
	}

	public void setTotalVendor(Long totalVendor) {
		this.totalVendor = totalVendor;
	}

	public Long getTotalProduct() {
		return totalProduct;
	}

	public void setTotalProduct(Long totalProduct) {
		this.totalProduct = totalProduct;
	}

	public Long getTotalProductApprovedToday() {
		return totalProductApprovedToday;
	}

	public void setTotalProductApprovedToday(Long totalProductApprovedToday) { this.totalProductApprovedToday = totalProductApprovedToday; }

	public List<DashboardBarChartResponse> getFiveMonthTotalApprovedProduct() {
		return fiveMonthTotalApprovedProduct;
	}

	public void setFiveMonthTotalApprovedProduct(List<DashboardBarChartResponse> fiveMonthTotalApprovedProduct) { this.fiveMonthTotalApprovedProduct = fiveMonthTotalApprovedProduct; }

	public List<DashboardBarChartResponse> getFiveMonthTotalCustomer() { return fiveMonthTotalCustomer; }

	public void setFiveMonthTotalCustomer(List<DashboardBarChartResponse> fiveMonthTotalCustomer) { this.fiveMonthTotalCustomer = fiveMonthTotalCustomer; }

	public List<DashboardBarChartResponse> getFiveMonthTotalSales() { return fiveMonthTotalSales; }

	public void setFiveMonthTotalSales(List<DashboardBarChartResponse> fiveMonthTotalSales) { this.fiveMonthTotalSales = fiveMonthTotalSales; }

	public List<DashboardBarChartResponse> getFiveYearTotalRegisteredCustomer() { return fiveYearTotalRegisteredCustomer; }

	public void setFiveYearTotalRegisteredCustomer(List<DashboardBarChartResponse> fiveYearTotalRegisteredCustomer) { this.fiveYearTotalRegisteredCustomer = fiveYearTotalRegisteredCustomer; }

	public List<DashboardBarChartResponse> getFiveYearTotalHospital() { return fiveYearTotalHospital; }

	public void setFiveYearTotalHospital(List<DashboardBarChartResponse> fiveYearTotalHospital) { this.fiveYearTotalHospital = fiveYearTotalHospital; }

	public List<DashboardBarChartResponse> getFiveYearTotalSales() { return fiveYearTotalSales; }

	public void setFiveYearTotalSales(List<DashboardBarChartResponse> fiveYearTotalSales) { this.fiveYearTotalSales = fiveYearTotalSales; }
}

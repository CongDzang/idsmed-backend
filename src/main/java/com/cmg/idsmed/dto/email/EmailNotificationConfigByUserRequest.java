package com.cmg.idsmed.dto.email;

import java.util.List;

import com.cmg.idsmed.dto.AbstractRequest;

public class EmailNotificationConfigByUserRequest extends AbstractRequest {

    private String loginId;
    
    private List<EmailNotificationConfigRequest> emailConfigs;

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public List<EmailNotificationConfigRequest> getEmailConfigs() {
		return emailConfigs;
	}

	public void setEmailConfigs(List<EmailNotificationConfigRequest> emailConfigs) {
		this.emailConfigs = emailConfigs;
	}
}

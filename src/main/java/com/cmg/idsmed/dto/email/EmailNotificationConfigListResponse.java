package com.cmg.idsmed.dto.email;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;

import com.cmg.idsmed.dto.AbstractResponse;
import com.cmg.idsmed.model.entity.email.EmailNotificationConfig;

public class EmailNotificationConfigListResponse extends AbstractResponse {
	
	public EmailNotificationConfigListResponse() {
		super();
	}

	public EmailNotificationConfigListResponse(List<EmailNotificationConfig> emailConfigs) {
		super();
		if (!CollectionUtils.isEmpty(emailConfigs)) {
	      this.emailConfigs = emailConfigs
				  .stream()
				  .map(ec -> new EmailNotificationConfigResponse(ec))
				  .collect(Collectors.toList());
	    }
	}

	private List<EmailNotificationConfigResponse> emailConfigs = new ArrayList<>();

	public List<EmailNotificationConfigResponse> getEmailConfigs() {
		return emailConfigs;
	}

	public void setEmailConfigs(List<EmailNotificationConfigResponse> emailConfigs) {
		this.emailConfigs = emailConfigs;
	}

}
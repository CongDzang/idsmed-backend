package com.cmg.idsmed.dto.email;

import javax.validation.constraints.Size;

import com.cmg.idsmed.dto.AbstractRequest;

public class EmailNotificationConfigRequest extends AbstractRequest {
	private Long id;

	private String notificationName;

	private Integer notificationConfigType;

	private Integer emailType;

	@Size(max = 500)
	private String recipientList;

	private Boolean enabled;

	private Long templateId;

	@Size(max = 10)
	private String countryCode;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNotificationName() {
		return notificationName;
	}

	public void setNotificationName(String notificationName) {
		this.notificationName = notificationName;
	}

	public Integer getNotificationConfigType() {
		return notificationConfigType;
	}

	public void setNotificationConfigType(Integer notificationConfigType) {
		this.notificationConfigType = notificationConfigType;
	}

	public Integer getEmailType() {
		return emailType;
	}

	public void setEmailType(Integer emailType) {
		this.emailType = emailType;
	}

	public String getRecipientList() {
		return recipientList;
	}

	public void setRecipientList(String recipientList) {
		this.recipientList = recipientList;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public Long getTemplateId() {
		return templateId;
	}

	public void setTemplateId(Long templateId) {
		this.templateId = templateId;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

}

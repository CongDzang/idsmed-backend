package com.cmg.idsmed.dto.email;

import com.cmg.idsmed.dto.AbstractResponse;
import com.cmg.idsmed.model.entity.email.EmailNotificationConfig;

public class EmailNotificationConfigResponse extends AbstractResponse {

	private Long id;

	private String notificationName;

	private Integer notificationConfigType;

	private Integer emailType;

	private String recipientList;
	
    private Boolean enabled;

	private EmailTemplateResponse emailTemplate;

    private String countryCode;

	public EmailNotificationConfigResponse() {
		super();
	}

	public EmailNotificationConfigResponse(EmailNotificationConfig enc) {
		super();
		this.id = enc.getId();
		this.notificationName = enc.getNotificationName();
		this.notificationConfigType = enc.getNotificationConfigType();
		this.emailType = enc.getEmailType().getCode();
		this.recipientList = enc.getRecipientList();
		this.enabled = enc.getEnabled();
		this.emailTemplate = new EmailTemplateResponse(enc.getEmailTemplate());
		this.countryCode = enc.getCountryCode();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNotificationName() {
		return notificationName;
	}

	public void setNotificationName(String notificationName) {
		this.notificationName = notificationName;
	}

	public Integer getNotificationConfigType() {
		return notificationConfigType;
	}

	public void setNotificationConfigType(Integer notificationConfigType) {
		this.notificationConfigType = notificationConfigType;
	}

	public Integer getEmailType() {
		return emailType;
	}

	public void setEmailType(Integer emailType) {
		this.emailType = emailType;
	}

	public String getRecipientList() {
		return recipientList;
	}

	public void setRecipientList(String recipientList) {
		this.recipientList = recipientList;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public EmailTemplateResponse getEmailTemplate() {
		return emailTemplate;
	}

	public void setEmailTemplate(EmailTemplateResponse emailTemplate) {
		this.emailTemplate = emailTemplate;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
}

package com.cmg.idsmed.dto.email;

import java.util.ArrayList;
import java.util.List;

public class EmailTemplateListResponse {
    private Integer totalCount = null;
    private Integer pageNo = null;
    private Integer pageSize = null;
    private Integer totalPage = null;
    private List<EmailTemplateResponse> emailTemplates = new ArrayList<>();

    public EmailTemplateListResponse(Integer pageNo, Integer pageSize) {
        this.totalPage = 0;
        this.totalCount = 0;
        this.pageSize = pageSize;
        this.pageNo = pageNo;
    }

    public EmailTemplateListResponse(List<EmailTemplateResponse> emailTemplates) {
        this.emailTemplates = emailTemplates;
    }

    public EmailTemplateListResponse(Integer totalCount, Integer pageIndex, Integer pageSize, List<EmailTemplateResponse> EmailTemplateResponses) {
        if (pageSize != null && !pageSize.equals(0)) {
            Integer calTotalPage = totalCount/pageSize;
            if(totalCount % pageSize > 0) {
                calTotalPage++;
            }
            this.totalPage = calTotalPage;
        }
        this.totalCount = totalCount;

        Integer pageCount = pageSize >= totalCount ? 1 : (totalCount%pageSize > 0 ? totalCount/pageSize +1 : totalCount/pageSize);
        Integer pageNo = pageIndex*pageSize >= totalCount ? pageCount-1 : pageIndex;

        this.pageNo = pageNo+1;
        this.pageSize = pageSize;
        this.emailTemplates = EmailTemplateResponses;
    }

    public EmailTemplateListResponse(Integer totalCount, List<EmailTemplateResponse> emailTemplates) {
        this.totalCount = totalCount;
        this.emailTemplates = emailTemplates;
        this.totalPage = 1;
        this.pageSize = totalCount;
        this.pageNo = 1;
    }

    public Integer getTotalCount() { return totalCount; }

    public void setTotalCount(Integer totalCount) { this.totalCount = totalCount; }

    public Integer getPageNo() { return pageNo; }

    public void setPageNo(Integer pageNo) { this.pageNo = pageNo; }

    public Integer getPageSize() { return pageSize; }

    public void setPageSize(Integer pageSize) { this.pageSize = pageSize; }

    public Integer getTotalPage() { return totalPage; }

    public void setTotalPage(Integer totalPage) { this.totalPage = totalPage; }

    public List<EmailTemplateResponse> getEmailTemplates() { return emailTemplates; }

    public void setEmailTemplates(List<EmailTemplateResponse> emailTemplates) { this.emailTemplates = emailTemplates; }
}
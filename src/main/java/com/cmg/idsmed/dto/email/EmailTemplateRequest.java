package com.cmg.idsmed.dto.email;

import com.cmg.idsmed.dto.AbstractRequest;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class EmailTemplateRequest extends AbstractRequest {

    private Long id;

    @NotNull
    @Size(max = 100)
    private String templateName;

    @NotNull
    @Size(max = 100)
    private String subject;

    private String content;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}

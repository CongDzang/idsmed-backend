package com.cmg.idsmed.dto.email;

import com.cmg.idsmed.dto.AbstractResponse;
import com.cmg.idsmed.model.entity.email.EmailTemplate;

import javax.validation.constraints.Size;

public class EmailTemplateResponse extends AbstractResponse {

    private Long id;

    @Size(max = 100)
    private String templateName;

    @Size(max = 100)
    private String subject;

    private String content;

    private Integer status;

    public EmailTemplateResponse(EmailTemplate emailTemplate) {
        if (emailTemplate != null) {
            this.id = emailTemplate.getId();
            this.templateName = emailTemplate.getTemplateName();
            this.subject = emailTemplate.getSubject();
            this.content = emailTemplate.getContent();
            this.status = emailTemplate.getStatus();
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getStatus() { return status; }

    public void setStatus(Integer status) { this.status = status; }
}

package com.cmg.idsmed.dto.email;

import java.util.List;

public class EmailWildcardListResponse {

    private List<EmailWildcardResponse> emailWildcardResponses;

    public EmailWildcardListResponse() {
    }

    public EmailWildcardListResponse(List<EmailWildcardResponse> emailWildcardResponses) {
        this.emailWildcardResponses = emailWildcardResponses;
    }

    public List<EmailWildcardResponse> getEmailWildcardResponses() {
        return emailWildcardResponses;
    }

    public void setEmailWildcardResponses(List<EmailWildcardResponse> emailWildcardResponses) {
        this.emailWildcardResponses = emailWildcardResponses;
    }
}

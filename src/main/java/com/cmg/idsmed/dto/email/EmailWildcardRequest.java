package com.cmg.idsmed.dto.email;

import com.cmg.idsmed.dto.AbstractRequest;

import javax.validation.constraints.Size;

public class EmailWildcardRequest extends AbstractRequest {

    private Long id;

    @Size(max = 100)
    private String wildcardName;

    @Size(max = 100)
    private String wildcardValue;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getWildcardName() {
        return wildcardName;
    }

    public void setWildcardName(String wildcardName) {
        this.wildcardName = wildcardName;
    }

    public String getWildcardValue() {
        return wildcardValue;
    }

    public void setWildcardValue(String wildcardValue) {
        this.wildcardValue = wildcardValue;
    }

}

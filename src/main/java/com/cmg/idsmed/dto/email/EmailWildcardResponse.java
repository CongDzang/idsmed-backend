package com.cmg.idsmed.dto.email;

import com.cmg.idsmed.dto.AbstractResponse;

import javax.validation.constraints.Size;

public class EmailWildcardResponse extends AbstractResponse {

    private Long id;

    @Size(max = 100)
    private String wildcardName;

    @Size(max = 100)
    private String wildcardValue;

    public EmailWildcardResponse() {
    }

    public EmailWildcardResponse(Long id, @Size(max = 100) String wildcardName, @Size(max = 100) String wildcardValue) {
        this.id = id;
        this.wildcardName = wildcardName;
        this.wildcardValue = wildcardValue;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getWildcardName() {
        return wildcardName;
    }

    public void setWildcardName(String wildcardName) {
        this.wildcardName = wildcardName;
    }

    public String getWildcardValue() {
        return wildcardValue;
    }

    public void setWildcardValue(String wildcardValue) {
        this.wildcardValue = wildcardValue;
    }

}

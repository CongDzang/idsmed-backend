package com.cmg.idsmed.dto.error;

import com.cmg.idsmed.dto.AbstractRequest;

public class LogErrorApiRequest extends AbstractRequest
{
    private String login;
    private String errorCode;
    private String description;
    private String url;
    private String browser;
    private String os;

    public String getLogin() {return login;}
    public void setLogin(String login) {this.login = login;}

    public String getErrorCode() {return errorCode;}
    public void setErrorCode(String errorCode) {this.errorCode = errorCode;}

    public String getDescription() {return description;}
    public void setDescription(String description) {this.description = description;}

    public String getUrl() {return url;}
    public void setUrl(String url) {this.url = url;}

    public String getBrowser() {return browser;}
    public void setBrowser(String browser) {this.browser = browser;}

    public String getOs() {return os;}
    public void setOs(String os) {this.os = os;}
}
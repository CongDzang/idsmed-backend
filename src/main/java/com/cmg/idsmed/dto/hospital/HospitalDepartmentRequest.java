package com.cmg.idsmed.dto.hospital;

import com.cmg.idsmed.dto.AbstractRequest;
import com.cmg.idsmed.model.entity.hospital.Hospital;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class HospitalDepartmentRequest extends AbstractRequest {
	private Long id;

	private String namePrimary;

	private String nameSecondary;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNamePrimary() {
		return namePrimary;
	}

	public void setNamePrimary(String namePrimary) {
		this.namePrimary = namePrimary;
	}

	public String getNameSecondary() {
		return nameSecondary;
	}

	public void setNameSecondary(String nameSecondary) {
		this.nameSecondary = nameSecondary;
	}
}

package com.cmg.idsmed.dto.hospital;

import com.cmg.idsmed.model.entity.hospital.HospitalDepartment;

public class HospitalDepartmentResponse {
	private Long id;
	private String namePrimary;
	private String nameSecondary;
	private Long hospitalId;
	private String code;
	private Integer status;

	public HospitalDepartmentResponse() {

	}

	public HospitalDepartmentResponse(HospitalDepartment hospitalDepartment) {
		if (hospitalDepartment != null) {
			this.id = hospitalDepartment.getId();
			this.namePrimary = hospitalDepartment.getNamePrimary();
			this.nameSecondary = hospitalDepartment.getNameSecondary();
			if (hospitalDepartment.getHospital() != null) {
				this.hospitalId = hospitalDepartment.getHospital().getId();
			}
			this.code = hospitalDepartment.getCode();
			this.status = hospitalDepartment.getStatus();
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNamePrimary() {
		return namePrimary;
	}

	public void setNamePrimary(String namePrimary) {
		this.namePrimary = namePrimary;
	}

	public String getNameSecondary() {
		return nameSecondary;
	}

	public void setNameSecondary(String nameSecondary) {
		this.nameSecondary = nameSecondary;
	}

	public Long getHospitalId() {
		return hospitalId;
	}

	public void setHospitalId(Long hospitalId) {
		this.hospitalId = hospitalId;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
}

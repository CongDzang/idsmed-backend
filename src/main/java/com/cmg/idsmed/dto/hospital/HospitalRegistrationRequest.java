package com.cmg.idsmed.dto.hospital;

import com.cmg.idsmed.dto.AbstractRequest;

public class HospitalRegistrationRequest extends AbstractRequest {
	private HospitalRequest hospitalRequest;
	private HospitalUserRegistrationRequest hospitalUserRegistrationRequest;
	private HospitalDepartmentRequest hospitalDepartmentRequest;

	public HospitalRequest getHospitalRequest() {
		return hospitalRequest;
	}

	public void setHospitalRequest(HospitalRequest hospitalRequest) {
		this.hospitalRequest = hospitalRequest;
	}

	public HospitalUserRegistrationRequest getHospitalUserRegistrationRequest() {
		return hospitalUserRegistrationRequest;
	}

	public void setHospitalUserRegistrationRequest(HospitalUserRegistrationRequest hospitalUserRegistrationRequest) {
		this.hospitalUserRegistrationRequest = hospitalUserRegistrationRequest;
	}

	public HospitalDepartmentRequest getHospitalDepartmentRequest() {
		return hospitalDepartmentRequest;
	}

	public void setHospitalDepartmentRequest(HospitalDepartmentRequest hospitalDepartmentRequest) {
		this.hospitalDepartmentRequest = hospitalDepartmentRequest;
	}
}

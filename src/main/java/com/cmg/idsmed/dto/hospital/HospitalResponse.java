package com.cmg.idsmed.dto.hospital;

import com.cmg.idsmed.dto.masterdata.CountryResponse;
import com.cmg.idsmed.dto.masterdata.ProvinceResponse;
import com.cmg.idsmed.model.entity.hospital.Hospital;
import com.cmg.idsmed.model.entity.masterdata.Province;

public class HospitalResponse {

	private Long id;

	private String namePrimary;

	private String nameSecondary;

	private CountryResponse country;

	private ProvinceResponse province;

	private String code;

	private String postcode;

	private String firstAddress;

	private String secondAddress;

	private Integer status;

	public HospitalResponse() {

	}

	public HospitalResponse(Hospital hospital) {
		if (hospital != null) {
			this.id = hospital.getId();
			this.namePrimary = hospital.getNamePrimary();
			this.nameSecondary = hospital.getNameSecondary();
			if (hospital.getCountry() != null) {
				this.country = new CountryResponse(hospital.getCountry());
			}
			if (hospital.getProvince() != null) {
				this.province = new ProvinceResponse(hospital.getProvince());
			}
			this.code = hospital.getCode();
			this.postcode = hospital.getPostcode();
			this.firstAddress = hospital.getFirstAddress();
			this.secondAddress = hospital.getSecondAddress();
			this.status = hospital.getStatus();
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNameSecondary() {
		return nameSecondary;
	}

	public void setNameSecondary(String nameSecondary) {
		this.nameSecondary = nameSecondary;
	}

	public CountryResponse getCountry() {
		return country;
	}

	public void setCountry(CountryResponse country) {
		this.country = country;
	}

	public ProvinceResponse getProvince() {
		return province;
	}

	public void setProvince(ProvinceResponse province) {
		this.province = province;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getPostcode() {
		return postcode;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	public String getFirstAddress() {
		return firstAddress;
	}

	public void setFirstAddress(String firstAddress) {
		this.firstAddress = firstAddress;
	}

	public String getSecondAddress() {
		return secondAddress;
	}

	public void setSecondAddress(String secondAddress) {
		this.secondAddress = secondAddress;
	}

	public String getNamePrimary() {
		return namePrimary;
	}

	public void setNamePrimary(String namePrimary) {
		this.namePrimary = namePrimary;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
}

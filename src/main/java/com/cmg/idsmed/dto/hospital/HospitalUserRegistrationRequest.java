package com.cmg.idsmed.dto.hospital;

import com.cmg.idsmed.dto.user.UserRegistrationRequest;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

public class HospitalUserRegistrationRequest extends UserRegistrationRequest {
}

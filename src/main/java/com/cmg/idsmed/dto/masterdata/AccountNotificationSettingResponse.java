package com.cmg.idsmed.dto.masterdata;

import com.cmg.idsmed.model.entity.masterdata.CompanyBackgroundInfo;
import org.springframework.beans.BeanUtils;

import java.math.BigDecimal;

public class AccountNotificationSettingResponse {
	private Integer notificationType;
	private String notificationName;

	public AccountNotificationSettingResponse(Integer notificationType, String notificationName){
		this.notificationType = notificationType;
		this.notificationName = notificationName;
	}
	public Integer getNotificationType() { return notificationType; }

	public void setNotificationType(Integer notificationType) { this.notificationType = notificationType; }

	public String getNotificationName() { return notificationName; }

	public void setNotificationName(String notificationName) { this.notificationName = notificationName; }
}

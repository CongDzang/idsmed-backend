package com.cmg.idsmed.dto.masterdata;

import java.math.BigDecimal;

import org.apache.poi.hpsf.Decimal;
import org.springframework.beans.BeanUtils;

import com.cmg.idsmed.model.entity.masterdata.CompanyBackgroundInfo;

public class CompanyBackgroundInfoResponse {
	
	public CompanyBackgroundInfoResponse(CompanyBackgroundInfo ci) {
		String [] ignoreProperties = {};
        BeanUtils.copyProperties(ci, this, ignoreProperties);
	}

	private Long id;

    private String companyCode;
    
    private BigDecimal annualSales;
    
    private Long noOfEmployee;
    
    private String primaryLine;
    
    private String mgmtDirector1;
    
    private String mgmtDirector2;
    
    private String mgmtDirector3;
    
    private String mgmtDirector4;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	public BigDecimal getAnnualSales() {
		return annualSales;
	}

	public void setAnnualSales(BigDecimal annualSales) {
		this.annualSales = annualSales;
	}

	public Long getNoOfEmployee() {
		return noOfEmployee;
	}

	public void setNoOfEmployee(Long noOfEmployee) {
		this.noOfEmployee = noOfEmployee;
	}

	public String getPrimaryLine() {
		return primaryLine;
	}

	public void setPrimaryLine(String primaryLine) {
		this.primaryLine = primaryLine;
	}

	public String getMgmtDirector1() {
		return mgmtDirector1;
	}

	public void setMgmtDirector1(String mgmtDirector1) {
		this.mgmtDirector1 = mgmtDirector1;
	}

	public String getMgmtDirector2() {
		return mgmtDirector2;
	}

	public void setMgmtDirector2(String mgmtDirector2) {
		this.mgmtDirector2 = mgmtDirector2;
	}

	public String getMgmtDirector3() {
		return mgmtDirector3;
	}

	public void setMgmtDirector3(String mgmtDirector3) {
		this.mgmtDirector3 = mgmtDirector3;
	}

	public String getMgmtDirector4() {
		return mgmtDirector4;
	}

	public void setMgmtDirector4(String mgmtDirector4) {
		this.mgmtDirector4 = mgmtDirector4;
	}
}

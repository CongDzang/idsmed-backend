package com.cmg.idsmed.dto.masterdata;

public class CountryProvincePairRequest {
	private Long countryId;
	private Long provinceId;

	public Long getCountryId() {
		return countryId;
	}

	public void setCountryId(Long countryId) {
		this.countryId = countryId;
	}

	public Long getProvinceId() {
		return provinceId;
	}

	public void setProvinceId(Long provinceId) {
		this.provinceId = provinceId;
	}
}

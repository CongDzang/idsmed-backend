package com.cmg.idsmed.dto.masterdata;


import com.cmg.idsmed.dto.vendor.VendorAddressResponse;
import com.cmg.idsmed.model.entity.masterdata.Country;
import com.cmg.idsmed.model.entity.masterdata.Province;
import com.cmg.idsmed.model.entity.vendor.VendorAddress;

import javax.validation.constraints.Size;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class CountryResponse {

	private Long id;

	@Size(max = 1000)
	private String englishName;

	@Size(max = 1000)
	private String chineseName;

	@Size(max = 500)
	private String englishBriefName;

	@Size(max = 500)
	private String chineseBriefName;

	@Size(max = 50)
	private String countryCode;

	private List<ProvinceResponse> provinces;

	public CountryResponse() {
	}

	public CountryResponse(Country country) {
		if (country != null) {
			this.id = country.getId();
			this.englishName = country.getEnglishName();
			this.chineseName = country.getChineseName();
			this.englishBriefName = country.getEnglishBriefName();
			this.chineseBriefName = country.getChineseBriefName();
			this.countryCode = country.getCountryCode();
			this.provinces = sortProvinceResponseList(country);
		}
	}

	public CountryResponse(VendorAddress vendorAddress) {
		if (vendorAddress.getCountry() != null) {
			this.id = vendorAddress.getCountry().getId();
			this.englishName = vendorAddress.getCountry().getEnglishName();
			this.chineseName = vendorAddress.getCountry().getChineseName();
			this.englishBriefName = vendorAddress.getCountry().getEnglishBriefName();
			this.chineseBriefName = vendorAddress.getCountry().getChineseBriefName();
			this.countryCode = vendorAddress.getCountry().getCountryCode();
		}
	}


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEnglishName() {
		return englishName;
	}

	public void setEnglishName(String englishName) {
		this.englishName = englishName;
	}

	public String getChineseName() {
		return chineseName;
	}

	public void setChineseName(String chineseName) {
		this.chineseName = chineseName;
	}

	public String getEnglishBriefName() {
		return englishBriefName;
	}

	public void setEnglishBriefName(String englishBriefName) {
		this.englishBriefName = englishBriefName;
	}

	public String getChineseBriefName() {
		return chineseBriefName;
	}

	public void setChineseBriefName(String chineseBriefName) {
		this.chineseBriefName = chineseBriefName;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public List<ProvinceResponse> getProvinces() {
		return provinces;
	}

	public void setProvinces(List<ProvinceResponse> provinces) {
		this.provinces = provinces;
	}

	public List<ProvinceResponse> sortProvinceResponseList(Country country){
		//Sort province english brief name order by asc
		List<ProvinceResponse> provinceResponseList = country.getProvinces()
				.stream().map(p -> new ProvinceResponse(p)).collect(Collectors.toList());
		provinceResponseList.sort(Comparator.comparing(o -> o.getEnglishBriefName()));

		return provinceResponseList;
	}
}

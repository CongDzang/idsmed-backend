package com.cmg.idsmed.dto.masterdata;

import com.cmg.idsmed.dto.AbstractRequest;

public class EmailProviderRequest extends AbstractRequest {
	private String emailProviderCode;

	public String getEmailProviderCode() {
		return emailProviderCode;
	}

	public void setEmailProviderCode(String emailProviderCode) {
		this.emailProviderCode = emailProviderCode;
	}
}

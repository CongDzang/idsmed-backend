package com.cmg.idsmed.dto.masterdata;

public class EmailProviderResponse {
	private String code;
	private String description;
	private Boolean isCurrent = false;

	public EmailProviderResponse(String code, String description) {
		this.code = code;
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Boolean getCurrent() {
		return isCurrent;
	}

	public void setCurrent(Boolean current) {
		isCurrent = current;
	}
}

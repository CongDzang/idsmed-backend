package com.cmg.idsmed.dto.masterdata;

import com.cmg.idsmed.model.entity.masterdata.Province;

import javax.validation.constraints.Size;

public class ProvinceResponse {

	private Long id;

	@Size(max = 1000)
	private String englishName;

	@Size(max = 1000)
	private String chineseName;

	@Size(max = 500)
	private String englishBriefName;

	@Size(max = 500)
	private String chineseBriefName;

	@Size(max = 50)
	private String provinceCode;

	private Long countryId;

	public ProvinceResponse() {
	}

	public ProvinceResponse(Province province) {
		if (province != null) {
			this.id = province.getId();
			this.englishName = province.getEnglishName();
			this.chineseName = province.getChineseName();
			this.englishBriefName = province.getEnglishBriefName();
			this.chineseBriefName = province.getChineseBriefName();
			this.provinceCode = province.getProvinceCode();
			this.countryId = province.getCountry().getId();
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEnglishName() {
		return englishName;
	}

	public void setEnglishName(String englishName) {
		this.englishName = englishName;
	}

	public String getChineseName() {
		return chineseName;
	}

	public void setChineseName(String chineseName) {
		this.chineseName = chineseName;
	}

	public String getEnglishBriefName() {
		return englishBriefName;
	}

	public void setEnglishBriefName(String englishBriefName) {
		this.englishBriefName = englishBriefName;
	}

	public String getChineseBriefName() {
		return chineseBriefName;
	}

	public void setChineseBriefName(String chineseBriefName) {
		this.chineseBriefName = chineseBriefName;
	}

	public String getProvinceCode() {
		return provinceCode;
	}

	public void setProvinceCode(String provinceCode) {
		this.provinceCode = provinceCode;
	}
}

package com.cmg.idsmed.dto.masterdata;

import com.cmg.idsmed.model.entity.masterdata.VerifyingWorkFlowSetting;

import javax.persistence.*;
import javax.validation.constraints.Size;

public class VerifyingWorkFlowSettingResponse {
	private Long id;

	private String countryCode;

	private String workFlowCode;

	private String processNodes;

	private String description;

	public VerifyingWorkFlowSettingResponse() {
	}

	public VerifyingWorkFlowSettingResponse(VerifyingWorkFlowSetting setting) {
		if (setting != null) {
			this.countryCode = setting.getCountryCode();
			this.workFlowCode = setting.getWorkFlowCode();
			this.processNodes = setting.getProcessNodes();
			this.description = setting.getDescription();
		}
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getWorkFlowCode() {
		return workFlowCode;
	}

	public void setWorkFlowCode(String workFlowCode) {
		this.workFlowCode = workFlowCode;
	}

	public String getProcessNodes() {
		return processNodes;
	}

	public void setProcessNodes(String processNodes) {
		this.processNodes = processNodes;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}

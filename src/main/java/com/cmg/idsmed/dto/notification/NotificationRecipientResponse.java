package com.cmg.idsmed.dto.notification;

public class NotificationRecipientResponse {
    private Long recipientId;

    private Integer status;

    public NotificationRecipientResponse() {}

    public Long getRecipientId() { return recipientId; }

    public void setRecipientId(Long recipientId) { this.recipientId = recipientId; }

    public Integer getStatus() { return status; }

    public void setStatus(Integer status) { this.status = status; }
}

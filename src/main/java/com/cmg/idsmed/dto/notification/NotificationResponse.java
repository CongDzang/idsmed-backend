package com.cmg.idsmed.dto.notification;

import com.cmg.idsmed.model.entity.auth.IdsmedAccount;
import com.cmg.idsmed.model.entity.notification.Notification;
import com.cmg.idsmed.model.entity.notification.NotificationRecipient;
import org.springframework.util.CollectionUtils;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class NotificationResponse {
    private Long id;

    private Integer type;

    private String subject;

    private String content;

    private Long senderId;

    private Integer status;

    private List<NotificationRecipientResponse> recipientList;

    private LocalDateTime releaseDate;

    public NotificationResponse(Notification notification) {
        this.id = notification.getId();
        this.type = notification.getType();
        this.subject = notification.getSubject();
        this.content = notification.getContent();
        this.senderId = notification.getSender().getId();
        releaseDate = notification.getCreatedDate();
//		this.status = notification.getStatus();
        List<NotificationRecipient> recipients = notification.getRecipients();
        if (!CollectionUtils.isEmpty(recipients)) {
//			this.recipientId = recipients.stream().map(r -> r.getRecipient().getId()).collect(Collectors.toList());
            this.recipientList = recipients
                    .stream()
                    .map(rrl -> {
                        NotificationRecipientResponse nrr = new NotificationRecipientResponse();
                        nrr.setRecipientId(rrl.getRecipient().getId());
                        nrr.setStatus(rrl.getRecipient().getStatus());
                        return nrr;
                    })
                    .collect(Collectors.toList());
        }
    }

    public NotificationResponse(Notification notification, IdsmedAccount account) {
        this.id = notification.getId();
        this.type = notification.getType();
        this.subject = notification.getSubject();
        this.content = notification.getContent();
        this.senderId = notification.getSender().getId();
        releaseDate = notification.getCreatedDate();
        List<NotificationRecipient> recipients = notification.getRecipients();
        if (!CollectionUtils.isEmpty(recipients)) {
            this.recipientList = recipients
                    .stream()
                    .filter(id -> {
                        if (id.getRecipient().getId().equals(account.getId())) return true;
                        return false;
                    })
                    .map(rrl -> {
                        NotificationRecipientResponse nrr = new NotificationRecipientResponse();
                        nrr.setRecipientId(rrl.getRecipient().getId());
                        nrr.setStatus(rrl.getRecipient().getStatus());
                        return nrr;
                    })
                    .collect(Collectors.toList());
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Long getSenderId() {
        return senderId;
    }

    public void setSenderId(Long senderId) {
        this.senderId = senderId;
    }

//	public List<Long> getRecipientId() {
//		return recipientId;
//	}
//
//	public void setRecipientId(List<Long> recipientId) {
//		this.recipientId = recipientId;
//	}

    public List<NotificationRecipientResponse> getRecipientList() {
        return recipientList;
    }

    public void setRecipientList(List<NotificationRecipientResponse> recipientList) {
        this.recipientList = recipientList;
    }

    public LocalDateTime getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(LocalDateTime releaseDate) {
        this.releaseDate = releaseDate;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}

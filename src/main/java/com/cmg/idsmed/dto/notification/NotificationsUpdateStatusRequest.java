package com.cmg.idsmed.dto.notification;

import com.cmg.idsmed.dto.AbstractRequest;

import java.util.List;

public class NotificationsUpdateStatusRequest extends AbstractRequest {
	private List<Long> notificationIds;
	private String langCode;
	private String loginId;
	private Integer status;

	public NotificationsUpdateStatusRequest() {

	}

	public NotificationsUpdateStatusRequest(List<Long> notificationIds, String langCode, String loginId, Integer status) {
		this.notificationIds = notificationIds;
		this.langCode = langCode;
		this.loginId = loginId;
		this.status = status;
	}

	public List<Long> getNotificationIds() {
		return notificationIds;
	}

	public void setNotificationIds(List<Long> notificationIds) {
		this.notificationIds = notificationIds;
	}

	@Override
	public String getLangCode() {
		return langCode;
	}

	public void setLangCode(String langCode) {
		this.langCode = langCode;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
}

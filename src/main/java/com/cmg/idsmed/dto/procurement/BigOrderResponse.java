package com.cmg.idsmed.dto.procurement;

import com.cmg.idsmed.model.entity.procurement.OdBigOrder;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.util.CollectionUtils;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class BigOrderResponse {

    private Long id;

    //Master Order No
    @Size(max = 32)
    private String bigOrderNo;

    //title
    @Size(max = 64)
    private String title;

    //user id: This is wedoctorCustomerId?
    private Integer userId;

    //Member phone number
    @Size(max = 16)
    private String mobile;

    /**
     * Order Status :
     * 0 - Created
     * 1 - Paid
     * 2 - Shipped
     * 3 - Received
     * 8 - Completed
     * 9 - Cancelled
     */
    private Integer orderStatus;

    /**
     * Pay Method :
     * 1 - WeChat Pay
     * 2 - AliPay
     * 3 - Offline Payment
     */
    private Integer payMethod;

    //Payment status
    private Integer payStatus;

    /**
     * Whether to self-operate orders
     * 1 - Yes
     * 0 - No
     */
    private Integer isSelfOrder;

    //Total amount
    private BigDecimal totalAmount;

    // total goods amount
    private BigDecimal totalGoodsAmount;

    // Order total shipping cost
    private BigDecimal totalFreight;

    //Total discount amount of the procurement
    private BigDecimal totalDiscountAmount;

    // The amount actually paid
    private BigDecimal realPaidAmount;

    // The time of order
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(pattern = "MM/dd/yyyy")
    private Date orderTime;

    // Payment time
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(pattern = "MM/dd/yyyy")
    private Date paidTime;

    // Payment serial number
    @Size(max = 32)
    private String payNo;

    // order notes
    @Size(max = 128)
    private String remark;

    // Creation time
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(pattern = "MM/dd/yyyy")
    private Date createTime;

    // update time
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(pattern = "MM/dd/yyyy")
    private Date updateTime;

    private List<SmallOrderResponse> smallOrderList = new ArrayList<>();

    public BigOrderResponse() {
    }

    public BigOrderResponse(OdBigOrder odBigOrder) {
        if (odBigOrder != null) {
            this.id = odBigOrder.getId();
            this.bigOrderNo = odBigOrder.getBigOrderNo();
            this.title = odBigOrder.getTitle();
            this.userId = odBigOrder.getUserId();
            this.mobile = odBigOrder.getMobile();
            this.orderStatus = odBigOrder.getOrderStatus();
            this.payMethod = odBigOrder.getPayMethod();
            this.payStatus = odBigOrder.getPayStatus();
            this.isSelfOrder = odBigOrder.getIsSelfOrder();
            this.totalAmount = odBigOrder.getTotalAmount();
            this.totalGoodsAmount = odBigOrder.getTotalGoodsAmount();
            this.totalFreight = odBigOrder.getTotalFreight();
            this.totalDiscountAmount = odBigOrder.getTotalDiscountAmount();
            this.realPaidAmount = odBigOrder.getRealPaidAmount();
            this.orderTime = odBigOrder.getOrderTime();
            this.paidTime = odBigOrder.getPaidTime();
            this.payNo = odBigOrder.getPayNo();
            this.remark = odBigOrder.getRemark();
            this.createTime = odBigOrder.getCreateTime();
            this.updateTime = odBigOrder.getUpdateTime();
            this.smallOrderList = CollectionUtils.isEmpty(odBigOrder.getOdSmallOrderList()) ? new ArrayList<SmallOrderResponse>() :
                    odBigOrder.getOdSmallOrderList()
                            .stream()
                            .map(so -> new SmallOrderResponse(so))
                            .collect(Collectors.toList());
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBigOrderNo() {
        return bigOrderNo;
    }

    public void setBigOrderNo(String bigOrderNo) {
        this.bigOrderNo = bigOrderNo;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Integer getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(Integer orderStatus) {
        this.orderStatus = orderStatus;
    }

    public Integer getPayMethod() {
        return payMethod;
    }

    public void setPayMethod(Integer payMethod) {
        this.payMethod = payMethod;
    }

    public Integer getPayStatus() {
        return payStatus;
    }

    public void setPayStatus(Integer payStatus) {
        this.payStatus = payStatus;
    }

    public Integer getIsSelfOrder() {
        return isSelfOrder;
    }

    public void setIsSelfOrder(Integer isSelfOrder) {
        this.isSelfOrder = isSelfOrder;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public BigDecimal getTotalGoodsAmount() {
        return totalGoodsAmount;
    }

    public void setTotalGoodsAmount(BigDecimal totalGoodsAmount) {
        this.totalGoodsAmount = totalGoodsAmount;
    }

    public BigDecimal getTotalFreight() {
        return totalFreight;
    }

    public void setTotalFreight(BigDecimal totalFreight) {
        this.totalFreight = totalFreight;
    }

    public BigDecimal getTotalDiscountAmount() {
        return totalDiscountAmount;
    }

    public void setTotalDiscountAmount(BigDecimal totalDiscountAmount) {
        this.totalDiscountAmount = totalDiscountAmount;
    }

    public BigDecimal getRealPaidAmount() {
        return realPaidAmount;
    }

    public void setRealPaidAmount(BigDecimal realPaidAmount) {
        this.realPaidAmount = realPaidAmount;
    }

    public Date getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(Date orderTime) {
        this.orderTime = orderTime;
    }

    public Date getPaidTime() {
        return paidTime;
    }

    public void setPaidTime(Date paidTime) {
        this.paidTime = paidTime;
    }

    public String getPayNo() {
        return payNo;
    }

    public void setPayNo(String payNo) {
        this.payNo = payNo;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public List<SmallOrderResponse> getSmallOrderList() {
        return smallOrderList;
    }

    public void setSmallOrderList(List<SmallOrderResponse> smallOrderList) {
        this.smallOrderList = smallOrderList;
    }

}

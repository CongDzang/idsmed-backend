package com.cmg.idsmed.dto.procurement;

import com.cmg.idsmed.dto.AbstractRequest;

import javax.validation.constraints.Size;
import java.util.List;

public class CancelOrderRequest extends AbstractRequest {

    @Size(max = 32)
    private String bigOrderNo;

    private List<CancelSmallOrderRequest> smallOrders;

    public CancelOrderRequest() {
    }

    public CancelOrderRequest(String bigOrderNo, List<CancelSmallOrderRequest> smallOrders) {
        this.bigOrderNo = bigOrderNo;
        this.smallOrders = smallOrders;
    }

    public String getBigOrderNo() {
        return bigOrderNo;
    }

    public void setBigOrderNo(String bigOrderNo) {
        this.bigOrderNo = bigOrderNo;
    }

    public List<CancelSmallOrderRequest> getSmallOrders() {
        return smallOrders;
    }

    public void setSmallOrders(List<CancelSmallOrderRequest> smallOrders) {
        this.smallOrders = smallOrders;
    }

}

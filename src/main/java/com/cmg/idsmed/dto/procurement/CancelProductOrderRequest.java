package com.cmg.idsmed.dto.procurement;

import com.cmg.idsmed.dto.AbstractRequest;

public class CancelProductOrderRequest extends AbstractRequest {

    //ProductId now is Long in the description of api it is
    private Long productId;

    private String cancelReason;

    public CancelProductOrderRequest() {
    }

    public CancelProductOrderRequest(Long productId, String cancelReason) {
        this.productId = productId;
        this.cancelReason = cancelReason;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getCancelReason() {
        return cancelReason;
    }

    public void setCancelReason(String cancelReason) {
        this.cancelReason = cancelReason;
    }

}

package com.cmg.idsmed.dto.procurement;

import com.cmg.idsmed.dto.AbstractRequest;

import javax.validation.constraints.Size;
import java.util.List;

public class CancelSmallOrderRequest extends AbstractRequest {

    // Small Order Number
    @Size(max = 32)
    private String smallOrderNo;

    private List<CancelProductOrderRequest> productList;

    public CancelSmallOrderRequest() {
    }

    public CancelSmallOrderRequest(String smallOrderNo, List<CancelProductOrderRequest> productList) {
        this.smallOrderNo = smallOrderNo;
        this.productList = productList;
    }

    public String getSmallOrderNo() {
        return smallOrderNo;
    }

    public void setSmallOrderNo(String smallOrderNo) {
        this.smallOrderNo = smallOrderNo;
    }

    public List<CancelProductOrderRequest> getProductList() {
        return productList;
    }

    public void setProductList(List<CancelProductOrderRequest> productList) {
        this.productList = productList;
    }

}

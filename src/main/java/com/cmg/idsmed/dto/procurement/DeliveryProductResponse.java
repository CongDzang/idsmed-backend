package com.cmg.idsmed.dto.procurement;

import com.cmg.idsmed.model.entity.procurement.OdDeliveryProduct;
import org.springframework.util.StringUtils;

public class DeliveryProductResponse {

    private Long id;

    private Long orderDeliveryId;

    private Long goodsId;

    private Integer num;

    public DeliveryProductResponse() {
    }

    public DeliveryProductResponse(OdDeliveryProduct odDeliveryProduct) {
        if (odDeliveryProduct != null) {
            this.id = odDeliveryProduct.getId();
            this.orderDeliveryId = odDeliveryProduct.getOdOrderDelivery().getId();
            this.goodsId = odDeliveryProduct.getGoodsId();
            this.num = odDeliveryProduct.getNum();
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOrderDeliveryId() {
        return orderDeliveryId;
    }

    public void setOrderDeliveryId(Long orderDeliveryId) {
        this.orderDeliveryId = orderDeliveryId;
    }

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

}

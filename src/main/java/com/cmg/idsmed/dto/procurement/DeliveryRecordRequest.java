package com.cmg.idsmed.dto.procurement;

import com.cmg.idsmed.dto.AbstractRequest;

import javax.validation.constraints.Size;

public class DeliveryRecordRequest extends AbstractRequest {

    @Size(max = 32)
    private String deliveryNo;

    @Size(max = 32)
    private String eTrackingNo;

    @Size(max = 32)
    private String deliveryTime;

    @Size(max = 512)
    private String deliveryInfo;

    public DeliveryRecordRequest() {
    }

    public DeliveryRecordRequest(String deliveryNo, String eTrackingNo, String deliveryTime, String deliveryInfo) {
        this.deliveryNo = deliveryNo;
        this.eTrackingNo = eTrackingNo;
        this.deliveryTime = deliveryTime;
        this.deliveryInfo = deliveryInfo;
    }

    public String getDeliveryNo() {
        return deliveryNo;
    }

    public void setDeliveryNo(String deliveryNo) {
        this.deliveryNo = deliveryNo;
    }

    public String geteTrackingNo() {
        return eTrackingNo;
    }

    public void seteTrackingNo(String eTrackingNo) {
        this.eTrackingNo = eTrackingNo;
    }

    public String getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(String deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public String getDeliveryInfo() {
        return deliveryInfo;
    }

    public void setDeliveryInfo(String deliveryInfo) {
        this.deliveryInfo = deliveryInfo;
    }

}

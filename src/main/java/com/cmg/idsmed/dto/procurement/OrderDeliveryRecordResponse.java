package com.cmg.idsmed.dto.procurement;

import com.cmg.idsmed.model.entity.procurement.OdOrderDeliveryRecord;
import org.springframework.util.StringUtils;

import javax.validation.constraints.Size;

public class OrderDeliveryRecordResponse {

    private Long id;

    private Long orderDeliveryId;

    private Long deliveryId;

    // Logistics transaction number
    @Size(max = 32)
    private String deliveryNo;

    @Size(max = 32)
    private String deliveryTime;

    // Logistics information
    @Size(max = 512)
    private String deliveryInfo;

    public OrderDeliveryRecordResponse() {
    }

    public OrderDeliveryRecordResponse(OdOrderDeliveryRecord odOrderDeliveryRecord) {
        if (odOrderDeliveryRecord != null) {
            this.id = odOrderDeliveryRecord.getId();
            this.orderDeliveryId = odOrderDeliveryRecord.getOdOrderDelivery().getId();
            this.deliveryId = odOrderDeliveryRecord.getDeliveryId();
            this.deliveryNo = odOrderDeliveryRecord.getDeliveryNo();
            this.deliveryTime = odOrderDeliveryRecord.getDeliveryTime();
            this.deliveryInfo = odOrderDeliveryRecord.getDeliveryInfo();
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOrderDeliveryId() {
        return orderDeliveryId;
    }

    public void setOrderDeliveryId(Long orderDeliveryId) {
        this.orderDeliveryId = orderDeliveryId;
    }

    public Long getDeliveryId() {
        return deliveryId;
    }

    public void setDeliveryId(Long deliveryId) {
        this.deliveryId = deliveryId;
    }

    public String getDeliveryNo() {
        return deliveryNo;
    }

    public void setDeliveryNo(String deliveryNo) {
        this.deliveryNo = deliveryNo;
    }

    public String getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(String deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public String getDeliveryInfo() {
        return deliveryInfo;
    }

    public void setDeliveryInfo(String deliveryInfo) {
        this.deliveryInfo = deliveryInfo;
    }
}

package com.cmg.idsmed.dto.procurement;

import com.cmg.idsmed.model.entity.procurement.OdOrderDelivery;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.util.CollectionUtils;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class OrderDeliveryResponse {

    private Long id;

    private Long smallOrderId;

    // Small order number
    @Size(max = 32)
    private String smallOrderNo;

    // Member id
    private Long memberId;

    // Shipping fee
    private BigDecimal freight;

    // Shipping type
    private Integer shipType;

    // Logistics company ID
    private Integer logisCompId;

    // Logistics company code
    @Size(max = 16)
    private String logisCompCode;

    // Logistics company name
    @Size(max = 64)
    private String logisCompName;


    // Logistics company delivery number
    @Size(max = 32)
    private String deliveryNo;

    // Consignee mobile number
    @Size(max = 16)
    private String mobile;

    // identity number
    @Size(max = 32)
    private String idcardNo;

    // Remarks
    @Size(max = 128)
    private String remark;

    // create time
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(pattern = "MM/dd/yyyy")
    private Date createTime;

    // Express delivery status, get updates from third-party interfaces
    private Integer mailStatus;

    // Transaction update time, used to prevent frequent calls to third-party logistics query interfaces
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(pattern = "MM/dd/yyyy")
    private Date mailUpdateTime;

    //supplier_delivery_no
    @Size(max = 32)
    private String supplierDeliveryNo;

    private OrderDeliveryRecordResponse orderDeliveryRecord;

    private List<DeliveryProductResponse> deliveryProductList = new ArrayList<>();

    public OrderDeliveryResponse() {
    }

    public OrderDeliveryResponse(OdOrderDelivery odOrderDelivery) {
        if (odOrderDelivery != null) {
            this.id = odOrderDelivery.getId();
            this.smallOrderId = odOrderDelivery.getOdSmallOrder().getId();
            this.smallOrderNo = odOrderDelivery.getSmallOrderNo();
            this.memberId = odOrderDelivery.getMemberId();
            this.freight = odOrderDelivery.getFreight();
            this.shipType = odOrderDelivery.getShipType();
            this.logisCompId = odOrderDelivery.getLogisCompId();
            this.logisCompCode = odOrderDelivery.getLogisCompCode();
            this.logisCompName = odOrderDelivery.getLogisCompName();
            this.deliveryNo = odOrderDelivery.getDeliveryNo();
            this.mobile = odOrderDelivery.getMobile();
            this.idcardNo = odOrderDelivery.getIdcardNo();
            this.remark = odOrderDelivery.getRemark();
            this.createTime = odOrderDelivery.getCreateTime();
            this.mailStatus = odOrderDelivery.getMailStatus();
            this.mailUpdateTime = odOrderDelivery.getMailUpdateTime();
            this.supplierDeliveryNo = odOrderDelivery.getSupplierDeliveryNo();
            this.orderDeliveryRecord = new OrderDeliveryRecordResponse(odOrderDelivery.getOdOrderDeliveryRecord());
            this.deliveryProductList = CollectionUtils.isEmpty(odOrderDelivery.getOdDeliveryProducts()) ? new ArrayList<>() :
                    odOrderDelivery.getOdDeliveryProducts()
                            .stream()
                            .map(dp -> new DeliveryProductResponse(dp))
                            .collect(Collectors.toList());
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSmallOrderId() {
        return smallOrderId;
    }

    public void setSmallOrderId(Long smallOrderId) {
        this.smallOrderId = smallOrderId;
    }

    public String getSmallOrderNo() {
        return smallOrderNo;
    }

    public void setSmallOrderNo(String smallOrderNo) {
        this.smallOrderNo = smallOrderNo;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public BigDecimal getFreight() {
        return freight;
    }

    public void setFreight(BigDecimal freight) {
        this.freight = freight;
    }

    public Integer getShipType() {
        return shipType;
    }

    public void setShipType(Integer shipType) {
        this.shipType = shipType;
    }

    public Integer getLogisCompId() {
        return logisCompId;
    }

    public void setLogisCompId(Integer logisCompId) {
        this.logisCompId = logisCompId;
    }

    public String getLogisCompCode() {
        return logisCompCode;
    }

    public void setLogisCompCode(String logisCompCode) {
        this.logisCompCode = logisCompCode;
    }

    public String getLogisCompName() {
        return logisCompName;
    }

    public void setLogisCompName(String logisCompName) {
        this.logisCompName = logisCompName;
    }

    public String getDeliveryNo() {
        return deliveryNo;
    }

    public void setDeliveryNo(String deliveryNo) {
        this.deliveryNo = deliveryNo;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getIdcardNo() {
        return idcardNo;
    }

    public void setIdcardNo(String idcardNo) {
        this.idcardNo = idcardNo;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getMailStatus() {
        return mailStatus;
    }

    public void setMailStatus(Integer mailStatus) {
        this.mailStatus = mailStatus;
    }

    public Date getMailUpdateTime() {
        return mailUpdateTime;
    }

    public void setMailUpdateTime(Date mailUpdateTime) {
        this.mailUpdateTime = mailUpdateTime;
    }

    public String getSupplierDeliveryNo() {
        return supplierDeliveryNo;
    }

    public void setSupplierDeliveryNo(String supplierDeliveryNo) {
        this.supplierDeliveryNo = supplierDeliveryNo;
    }

    public OrderDeliveryRecordResponse getOrderDeliveryRecord() {
        return orderDeliveryRecord;
    }

    public void setOrderDeliveryRecord(OrderDeliveryRecordResponse orderDeliveryRecord) {
        this.orderDeliveryRecord = orderDeliveryRecord;
    }

    public List<DeliveryProductResponse> getDeliveryProductList() {
        return deliveryProductList;
    }

    public void setDeliveryProductList(List<DeliveryProductResponse> deliveryProductList) {
        this.deliveryProductList = deliveryProductList;
    }

}

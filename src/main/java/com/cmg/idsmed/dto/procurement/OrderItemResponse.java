package com.cmg.idsmed.dto.procurement;

import com.cmg.idsmed.model.entity.procurement.OdOrderItem;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.util.Date;

public class OrderItemResponse {

    private Long id;

    private Long smallOrderId;

    // Small Order Number
    @Size(max = 32)
    private String smallOrderNo;

    //title
    @Size(max = 64)
    private String title;

    // Product ID
    private Long goodsId;

    // Product default picture
    @Size(max = 255)
    private String goodsPic;

    // SKU code, I have to give it default a bcs request has no
    @Size(max = 32)
    private String skuCode = "a";

    // Specification model
    @Size(max = 64)
    private String property;

    // price
    private BigDecimal price;

    // in stock
    private Integer num;

    // weight
    private Double weight;

    // The amount of goods
    private BigDecimal itemAmount;

    // Discount amount
    private BigDecimal discountAmount = BigDecimal.ZERO;

    // The amount actually paid
    private BigDecimal realPaidAmount = BigDecimal.ZERO;

    //create time
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(pattern = "MM/dd/yyyy")
    private Date createTime;

    // update time
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(pattern = "MM/dd/yyyy")
    private Date updateTime;

    //warehouse id
    private Integer warehouseId;

    /**
     * Delivery Status
     * 1 - YES
     * 0 - NO
     */
    private Integer sendOutStatus;

    // Order logistics information ID
    private Long deliveryId;

    /**
     * Order Status :
     * 0 - Created
     * 1 - Paid
     * 2 - Shipped
     * 3 - Received
     * 8 - Completed
     * 9 - Cancelled
     */
    private Integer orderStatus;

    private String cancelReason;

    public OrderItemResponse() {
    }

    public OrderItemResponse(OdOrderItem odOrderItem) {
        if (odOrderItem != null) {
            this.id = odOrderItem.getId();
            this.smallOrderId = odOrderItem.getOdSmallOrder().getId();
            this.smallOrderNo = odOrderItem.getSmallOrderNo();
            this.title = odOrderItem.getTitle();
            this.goodsId = odOrderItem.getGoodsId();
            this.goodsPic = odOrderItem.getGoodsPic();
            this.skuCode = odOrderItem.getSkuCode();
            this.property = odOrderItem.getProperty();
            this.price = odOrderItem.getPrice();
            this.num = odOrderItem.getNum();
            this.weight = odOrderItem.getWeight();
            this.itemAmount = odOrderItem.getItemAmount();
            this.discountAmount = odOrderItem.getDiscountAmount();
            this.realPaidAmount = odOrderItem.getRealPaidAmount();
            this.createTime = odOrderItem.getCreateTime();
            this.updateTime = odOrderItem.getUpdateTime();
            this.warehouseId = odOrderItem.getWarehouseId();
            this.sendOutStatus = odOrderItem.getSendOutStatus();
            this.deliveryId = odOrderItem.getDeliveryId();
            this.orderStatus = odOrderItem.getOrderStatus();
            this.cancelReason = odOrderItem.getCancelReason();
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSmallOrderId() {
        return smallOrderId;
    }

    public void setSmallOrderId(Long smallOrderId) {
        this.smallOrderId = smallOrderId;
    }

    public String getSmallOrderNo() {
        return smallOrderNo;
    }

    public void setSmallOrderNo(String smallOrderNo) {
        this.smallOrderNo = smallOrderNo;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public String getGoodsPic() {
        return goodsPic;
    }

    public void setGoodsPic(String goodsPic) {
        this.goodsPic = goodsPic;
    }

    public String getSkuCode() {
        return skuCode;
    }

    public void setSkuCode(String skuCode) {
        this.skuCode = skuCode;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public BigDecimal getItemAmount() {
        return itemAmount;
    }

    public void setItemAmount(BigDecimal itemAmount) {
        this.itemAmount = itemAmount;
    }

    public BigDecimal getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(BigDecimal discountAmount) {
        this.discountAmount = discountAmount;
    }

    public BigDecimal getRealPaidAmount() {
        return realPaidAmount;
    }

    public void setRealPaidAmount(BigDecimal realPaidAmount) {
        this.realPaidAmount = realPaidAmount;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(Integer warehouseId) {
        this.warehouseId = warehouseId;
    }

    public Integer getSendOutStatus() {
        return sendOutStatus;
    }

    public void setSendOutStatus(Integer sendOutStatus) {
        this.sendOutStatus = sendOutStatus;
    }

    public Long getDeliveryId() {
        return deliveryId;
    }

    public void setDeliveryId(Long deliveryId) {
        this.deliveryId = deliveryId;
    }

    public Integer getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(Integer orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getCancelReason() {
        return cancelReason;
    }

    public void setCancelReason(String cancelReason) {
        this.cancelReason = cancelReason;
    }

}

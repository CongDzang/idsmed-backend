package com.cmg.idsmed.dto.procurement;

import com.cmg.idsmed.model.entity.procurement.OdOrderReceiveAddress;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Date;

public class OrderReceiveAddressResponse {
    private Long id;

    private Long smallOrderId;

    // Small Order Number
    @Size(max = 32)
    private String smallOrderNo;

    // Member phone number
    @Size(max = 16)
    private String mobile;

    // Consignee name
    @Size(max = 32)
    private String receiverName;

    // Province Code
    @Size(max = 16)
    private String provinceCode;

    // City code
    @Size(max = 16)
    private String cityCode;

    // Area code
    @Size(max = 16)
    private String districtCode;

    // province name
    @Size(max = 16)
    private String provinceName;

    // City name
    @Size(max = 16)
    private String cityName;

    // Area name
    @Size(max = 16)
    private String districtName;

    // address
    @Size(max = 128)
    private String address;

    // identification number
    @Size(max = 32)
    private String idcardNo;

    @Size(max = 1000)
    private String billingAddress;

    // create time
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(pattern = "MM/dd/yyyy")
    private Date createTime;

    public OrderReceiveAddressResponse() {
    }

    public OrderReceiveAddressResponse(OdOrderReceiveAddress odOrderReceiveAddress) {
        if (odOrderReceiveAddress != null) {
            this.id = odOrderReceiveAddress.getId();
            this.smallOrderId = odOrderReceiveAddress.getOdSmallOrder().getId();
            this.smallOrderNo = odOrderReceiveAddress.getSmallOrderNo();
            this.mobile = odOrderReceiveAddress.getMobile();
            this.receiverName = odOrderReceiveAddress.getReceiverName();
            this.provinceCode = odOrderReceiveAddress.getProvinceCode();
            this.cityCode = odOrderReceiveAddress.getCityCode();
            this.districtCode = odOrderReceiveAddress.getDistrictCode();
            this.provinceName = odOrderReceiveAddress.getProvinceName();
            this.cityName = odOrderReceiveAddress.getCityName();
            this.districtName = odOrderReceiveAddress.getDistrictName();
            this.address = odOrderReceiveAddress.getAddress();
            this.idcardNo = odOrderReceiveAddress.getIdcardNo();
            this.billingAddress = odOrderReceiveAddress.getBillingAddress();
            this.createTime = odOrderReceiveAddress.getCreateTime();
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSmallOrderId() {
        return smallOrderId;
    }

    public void setSmallOrderId(Long smallOrderId) {
        this.smallOrderId = smallOrderId;
    }

    public String getSmallOrderNo() {
        return smallOrderNo;
    }

    public void setSmallOrderNo(String smallOrderNo) {
        this.smallOrderNo = smallOrderNo;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getReceiverName() {
        return receiverName;
    }

    public void setReceiverName(String receiverName) {
        this.receiverName = receiverName;
    }

    public String getProvinceCode() {
        return provinceCode;
    }

    public void setProvinceCode(String provinceCode) {
        this.provinceCode = provinceCode;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    public String getDistrictCode() {
        return districtCode;
    }

    public void setDistrictCode(String districtCode) {
        this.districtCode = districtCode;
    }

    public String getProvinceName() {
        return provinceName;
    }

    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getDistrictName() {
        return districtName;
    }

    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getIdcardNo() {
        return idcardNo;
    }

    public void setIdcardNo(String idcardNo) {
        this.idcardNo = idcardNo;
    }

    public String getBillingAddress() {
        return billingAddress;
    }

    public void setBillingAddress(String billingAddress) {
        this.billingAddress = billingAddress;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

}

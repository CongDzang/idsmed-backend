package com.cmg.idsmed.dto.procurement;

import com.cmg.idsmed.dto.AbstractRequest;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class OrderStatusRequest extends AbstractRequest {

    @Size(max = 32)
    private String bigOrderNo;

    /**
     * Order Status :
     * 0 - Order Created
     * 1 - Payment Paid
     * 2 - Order Shipped
     * 3 - Payment Received
     * 8 - Process Completed
     * 9 - Order Cancelled
     * 10 - Vendor Cancel Order
     */
    private Integer orderStatus;

    public String getBigOrderNo() {
        return bigOrderNo;
    }

    public void setBigOrderNo(String bigOrderNo) {
        this.bigOrderNo = bigOrderNo;
    }

    public Integer getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(Integer orderStatus) {
        this.orderStatus = orderStatus;
    }

}

package com.cmg.idsmed.dto.procurement;

public class POList {
    private String smallOrderNo;
    private String ePO;

    public POList() {
    }

    public POList(String smallOrderNo, String ePO) {
        this.smallOrderNo = smallOrderNo;
        this.ePO = ePO;
    }

    public String getSmallOrderNo() {
        return smallOrderNo;
    }

    public void setSmallOrderNo(String smallOrderNo) {
        this.smallOrderNo = smallOrderNo;
    }

    public String getePO() {
        return ePO;
    }

    public void setePO(String ePO) {
        this.ePO = ePO;
    }

}

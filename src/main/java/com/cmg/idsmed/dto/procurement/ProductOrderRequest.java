package com.cmg.idsmed.dto.procurement;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

public class ProductOrderRequest {

//	itemAmount - BigDecimal, Mandatory, product amount

	// Small Order Number
	@Size(max = 32)
	private String smallOrderNo;

	//Product name is productNamePrimary if langCode is 'en'
	//Product name is productNameSecondary if langCode is 'cn'
	@Size(max = 1000)
	private String title;

	//ProductId now is Long in the description of api it is
	private Long productId;

	// Product property
	private String property;

	// Product price
	private BigDecimal price;

	// Quantity of product bought
	private Integer num;

	// Total amount of bought product
	private BigDecimal itemAmount;

	private BigDecimal discountAmount;

	private BigDecimal realPaidAmount;

	//warehouse id
	private Integer warehouseId;

	private Integer sendOutStatus;

	/**
	 * Order Status :
	 * 0 - Created
	 * 1 - Paid
	 * 2 - Shipped
	 * 3 - Received
	 * 8 - Completed
	 * 9 - Cancelled
	 */
	@Column(name = "order_status")
	private Integer orderStatus;

	@Column(name = "cancel_reason")
	private String cancelReason;

	public ProductOrderRequest() {
	}

	public String getSmallOrderNo() {
		return smallOrderNo;
	}

	public void setSmallOrderNo(String smallOrderNo) {
		this.smallOrderNo = smallOrderNo;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public String getProperty() {
		return property;
	}

	public void setProperty(String property) {
		this.property = property;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Integer getNum() {
		return num;
	}

	public void setNum(Integer num) {
		this.num = num;
	}

	public BigDecimal getItemAmount() {
		return itemAmount;
	}

	public void setItemAmount(BigDecimal itemAmount) {
		this.itemAmount = itemAmount;
	}

	public BigDecimal getDiscountAmount() {
		return discountAmount;
	}

	public void setDiscountAmount(BigDecimal discountAmount) {
		this.discountAmount = discountAmount;
	}

	public BigDecimal getRealPaidAmount() {
		return realPaidAmount;
	}

	public void setRealPaidAmount(BigDecimal realPaidAmount) {
		this.realPaidAmount = realPaidAmount;
	}

	public Integer getWarehouseId() {
		return warehouseId;
	}

	public void setWarehouseId(Integer warehouseId) {
		this.warehouseId = warehouseId;
	}

	public Integer getSendOutStatus() {
		return sendOutStatus;
	}

	public void setSendOutStatus(Integer sendOutStatus) {
		this.sendOutStatus = sendOutStatus;
	}

	public Integer getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(Integer orderStatus) {
		this.orderStatus = orderStatus;
	}

	public String getCancelReason() {
		return cancelReason;
	}

	public void setCancelReason(String cancelReason) {
		this.cancelReason = cancelReason;
	}

}

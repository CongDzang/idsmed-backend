package com.cmg.idsmed.dto.procurement;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class PurchaseOrderRequest {

	@Size(max = 32)
	private String bigOrderNo;

	@Size(max = 64)
	private String title;

	// This is wedoctorCustomerId?
	// Answer: Yes, clarify with Vincent, is WeDoctor's Customer.
	// Question: we remain userId or change to wedoctorCustomerId(this is value we keep in DB in other table)
	private Integer userId;

	//Member phone number
	@Size(max = 16)
	private String mobile;

	/**
	 * Order Status :
	 * 0 - Order Created
	 * 1 - Payment Paid
	 * 2 - Order Shipped
	 * 3 - Payment Received
	 * 8 - Process Completed
	 * 9 - Order Cancelled
	 * 10 - Vendor Cancel Order
	 */
	@NotNull
	private Integer orderStatus;

	/**
	 * Pay Method :
	 * 1 - WeChat Pay
	 * 2 - AliPay
	 * 3 - Offline Payment
	 */
	private Integer payMethod;

	//Payment status
	@NotNull
	private Integer payStatus;

	/**
	 * Nobody can answer me what is this
	 * Possible is to indicate is this a personal order or company order
	 * 1 - Yes
	 * 0 - No
	 */
	private Integer isSelfOrder;

	//Total amount
	@NotNull
	private BigDecimal totalAmount;

	@NotNull
	private BigDecimal totalGoodsAmount;

	@NotNull
	private BigDecimal totalFreight;

	@NotNull
	private BigDecimal totalDiscountAmount;

	@NotNull
	private BigDecimal realPaidAmount;

	// The time of order
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "MM/dd/yyyy")
	@NotNull
	private Date orderTime;

	// Payment time
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "MM/dd/yyyy")
	@NotNull
	private Date paidTime;

	// Payment serial number
	@Size(max = 32)
	private String payNo;

	// Order notes
	@Size(max = 128)
	private String remark;

	@Size(max = 100)
	private String tenderNo;

	private List<SmallOrderRequest> smallOrders;

	public PurchaseOrderRequest() {
	}

	public String getBigOrderNo() {
		return bigOrderNo;
	}

	public void setBigOrderNo(String bigOrderNo) {
		this.bigOrderNo = bigOrderNo;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public Integer getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(Integer orderStatus) {
		this.orderStatus = orderStatus;
	}

	public Integer getPayMethod() {
		return payMethod;
	}

	public void setPayMethod(Integer payMethod) {
		this.payMethod = payMethod;
	}

	public Integer getPayStatus() {
		return payStatus;
	}

	public void setPayStatus(Integer payStatus) {
		this.payStatus = payStatus;
	}

	public Integer getIsSelfOrder() {
		return isSelfOrder;
	}

	public void setIsSelfOrder(Integer isSelfOrder) {
		this.isSelfOrder = isSelfOrder;
	}

	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	public BigDecimal getTotalGoodsAmount() {
		return totalGoodsAmount;
	}

	public void setTotalGoodsAmount(BigDecimal totalGoodsAmount) {
		this.totalGoodsAmount = totalGoodsAmount;
	}

	public BigDecimal getTotalFreight() {
		return totalFreight;
	}

	public void setTotalFreight(BigDecimal totalFreight) {
		this.totalFreight = totalFreight;
	}

	public BigDecimal getTotalDiscountAmount() {
		return totalDiscountAmount;
	}

	public void setTotalDiscountAmount(BigDecimal totalDiscountAmount) {
		this.totalDiscountAmount = totalDiscountAmount;
	}

	public BigDecimal getRealPaidAmount() {
		return realPaidAmount;
	}

	public void setRealPaidAmount(BigDecimal realPaidAmount) {
		this.realPaidAmount = realPaidAmount;
	}

	public Date getOrderTime() {
		return orderTime;
	}

	public void setOrderTime(Date orderTime) {
		this.orderTime = orderTime;
	}

	public Date getPaidTime() {
		return paidTime;
	}

	public void setPaidTime(Date paidTime) {
		this.paidTime = paidTime;
	}

	public String getPayNo() {
		return payNo;
	}

	public void setPayNo(String payNo) {
		this.payNo = payNo;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getTenderNo() {
		return tenderNo;
	}

	public void setTenderNo(String tenderNo) {
		this.tenderNo = tenderNo;
	}

	public List<SmallOrderRequest> getSmallOrders() {
		return smallOrders;
	}

	public void setSmallOrders(List<SmallOrderRequest> smallOrders) {
		this.smallOrders = smallOrders;
	}

}

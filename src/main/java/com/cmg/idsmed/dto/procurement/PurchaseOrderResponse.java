package com.cmg.idsmed.dto.procurement;

import java.util.ArrayList;
import java.util.List;

public class PurchaseOrderResponse {
	private Integer code;
	private List<POList> poList = new ArrayList<>();
	private String msg;

	public PurchaseOrderResponse() {
	}

	public PurchaseOrderResponse(Integer code, List<POList> poList, String msg) {
		this.code = code;
		this.poList = poList;
		this.msg = msg;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public List<POList> getPoList() {
		return poList;
	}

	public void setPoList(List<POList> poList) {
		this.poList = poList;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

}

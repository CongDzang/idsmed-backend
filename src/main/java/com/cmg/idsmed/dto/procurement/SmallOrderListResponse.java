package com.cmg.idsmed.dto.procurement;

import java.util.List;

public class SmallOrderListResponse {
    private Integer totalPage = null;
    private List<SmallOrderResponse> smallOrderList;
    private Integer totalCount = null;
    private Integer pageNo = null;
    private Integer pageSize = null;

    public SmallOrderListResponse() {
    }

    public SmallOrderListResponse(Integer pageNo, Integer pageSize) {
        this.totalPage = 0;
        this.totalCount = 0;
        this.pageNo = pageNo;
        this.pageSize = pageSize;
    }

    public SmallOrderListResponse(Integer totalCount, Integer pageIndex, Integer pageSize, List<SmallOrderResponse> smallOrderList) {
        if (pageSize != null && !pageSize.equals(0)) {
            Integer calTotalPage = totalCount/pageSize;
            if(totalCount % pageSize > 0) {
                calTotalPage++;
            }
            this.totalPage = calTotalPage;
        }
        this.totalCount = totalCount;

        Integer pageCount = pageSize >= totalCount ? 1 : (totalCount%pageSize > 0 ? totalCount/pageSize +1 : totalCount/pageSize);
        Integer pageNo = pageIndex*pageSize >= totalCount ? pageCount-1 : pageIndex;

        this.pageNo = pageNo+1;
        this.pageSize = pageSize;
        this.smallOrderList = smallOrderList;
    }

    public SmallOrderListResponse(Integer totalCount, List<SmallOrderResponse> smallOrderList) {
        this.smallOrderList = smallOrderList;
        this.totalCount = totalCount;
        this.totalPage = 1;
        this.pageSize = totalCount;
        this.pageNo = 1;
    }

    public Integer getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(Integer totalPage) {
        this.totalPage = totalPage;
    }

    public List<SmallOrderResponse> getSmallOrderList() {
        return smallOrderList;
    }

    public void setSmallOrderList(List<SmallOrderResponse> smallOrderList) {
        this.smallOrderList = smallOrderList;
    }

    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    public Integer getPageNo() {
        return pageNo;
    }

    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

}

package com.cmg.idsmed.dto.procurement;

import com.cmg.idsmed.dto.AbstractRequest;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class SmallOrderRequest extends AbstractRequest {
	// Merchant Id
	private Long merchantId;

	// Small Order Number
	@Size(max = 32)
	private String smallOrderNo;

	// title
	@Size(max = 64)
	private String title;

	/**
	 * Order Status :
	 * 0 - Order Created
	 * 1 - Payment Paid
	 * 2 - Order Shipped
	 * 3 - Payment Received
	 * 8 - Process Completed
	 * 9 - Order Cancelled
	 * 10 - Vendor Cancel Order
	 */
	private Integer orderStatus;

	/**
	 * Pay Method :
	 * 1 - WeChat Pay
	 * 2 - AliPay
	 * 3 - Offline Payment
	 */
	private Integer payStatus;

	// Order item amount
	private BigDecimal totalItemAmount;

	//Total discount amount of the procurement
	private BigDecimal totalDiscountAmount;

	// The amount actually paid
	private BigDecimal realPaidAmount;

	// Order total shipping cost
	private BigDecimal totalFreight;

	// order notes
	@Size(max = 128)
	private String remark;

	// Payment time
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "MM/dd/yyyy")
	private Date paidTime;

	//rpo - this field is key in by user
	@Size(max = 32)
	private String rpo;

	private OrderReceiveInfo orderReceiveInfo;

	private List<ProductOrderRequest> productList;

	public SmallOrderRequest() {
	}

	public Long getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(Long merchantId) {
		this.merchantId = merchantId;
	}

	public String getSmallOrderNo() {
		return smallOrderNo;
	}

	public void setSmallOrderNo(String smallOrderNo) {
		this.smallOrderNo = smallOrderNo;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Integer getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(Integer orderStatus) {
		this.orderStatus = orderStatus;
	}

	public Integer getPayStatus() {
		return payStatus;
	}

	public void setPayStatus(Integer payStatus) {
		this.payStatus = payStatus;
	}

	public BigDecimal getTotalItemAmount() {
		return totalItemAmount;
	}

	public void setTotalItemAmount(BigDecimal totalItemAmount) {
		this.totalItemAmount = totalItemAmount;
	}

	public BigDecimal getTotalDiscountAmount() {
		return totalDiscountAmount;
	}

	public void setTotalDiscountAmount(BigDecimal totalDiscountAmount) {
		this.totalDiscountAmount = totalDiscountAmount;
	}

	public BigDecimal getRealPaidAmount() {
		return realPaidAmount;
	}

	public void setRealPaidAmount(BigDecimal realPaidAmount) {
		this.realPaidAmount = realPaidAmount;
	}

	public Date getPaidTime() {
		return paidTime;
	}

	public void setPaidTime(Date paidTime) {
		this.paidTime = paidTime;
	}

	public OrderReceiveInfo getOrderReceiveInfo() {
		return orderReceiveInfo;
	}

	public void setOrderReceiveInfo(OrderReceiveInfo orderReceiveInfo) {
		this.orderReceiveInfo = orderReceiveInfo;
	}

	public List<ProductOrderRequest> getProductList() {
		return productList;
	}

	public void setProductList(List<ProductOrderRequest> productList) {
		this.productList = productList;
	}

	public BigDecimal getTotalFreight() {
		return totalFreight;
	}

	public void setTotalFreight(BigDecimal totalFreight) {
		this.totalFreight = totalFreight;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getRpo() {
		return rpo;
	}

	public void setRpo(String rpo) {
		this.rpo = rpo;
	}
}

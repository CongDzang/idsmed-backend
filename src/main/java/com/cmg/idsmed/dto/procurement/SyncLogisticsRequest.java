package com.cmg.idsmed.dto.procurement;

import com.cmg.idsmed.dto.AbstractRequest;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.util.List;

public class SyncLogisticsRequest extends AbstractRequest {

    @Size(max = 32)
    private String smallOrderNo;

    private BigDecimal freight;

    private Integer shipType;

    private Integer logisCompId;

    @Size(max = 16)
    private String logisCompCode;

    @Size(max = 64)
    private String logisCompName;

    @Size(max = 32)
    private String deliveryNo;

    @Size(max = 16)
    private String mobile;

    @Size(max = 128)
    private String remark;

    private Integer mailStatus;

    private List<DeliveryRecordRequest> deliveryRecords;

    public SyncLogisticsRequest() {
    }

    public String getSmallOrderNo() {
        return smallOrderNo;
    }

    public void setSmallOrderNo(String smallOrderNo) {
        this.smallOrderNo = smallOrderNo;
    }

    public BigDecimal getFreight() {
        return freight;
    }

    public void setFreight(BigDecimal freight) {
        this.freight = freight;
    }

    public Integer getShipType() {
        return shipType;
    }

    public void setShipType(Integer shipType) {
        this.shipType = shipType;
    }

    public Integer getLogisCompId() {
        return logisCompId;
    }

    public void setLogisCompId(Integer logisCompId) {
        this.logisCompId = logisCompId;
    }

    public String getLogisCompCode() {
        return logisCompCode;
    }

    public void setLogisCompCode(String logisCompCode) {
        this.logisCompCode = logisCompCode;
    }

    public String getLogisCompName() {
        return logisCompName;
    }

    public void setLogisCompName(String logisCompName) {
        this.logisCompName = logisCompName;
    }

    public String getDeliveryNo() {
        return deliveryNo;
    }

    public void setDeliveryNo(String deliveryNo) {
        this.deliveryNo = deliveryNo;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getMailStatus() {
        return mailStatus;
    }

    public void setMailStatus(Integer mailStatus) {
        this.mailStatus = mailStatus;
    }

    public List<DeliveryRecordRequest> getDeliveryRecords() {
        return deliveryRecords;
    }

    public void setDeliveryRecords(List<DeliveryRecordRequest> deliveryRecords) {
        this.deliveryRecords = deliveryRecords;
    }

}

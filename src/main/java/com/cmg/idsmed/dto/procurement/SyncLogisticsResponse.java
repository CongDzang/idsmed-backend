package com.cmg.idsmed.dto.procurement;

public class SyncLogisticsResponse {

    private Integer code;
    private String msg;

    public SyncLogisticsResponse() {
    }

    public SyncLogisticsResponse(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

}

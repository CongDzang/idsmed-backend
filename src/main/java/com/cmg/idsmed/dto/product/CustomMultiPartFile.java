package com.cmg.idsmed.dto.product;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import org.springframework.web.multipart.MultipartFile;

public class CustomMultiPartFile implements MultipartFile {
	private final byte[] imgContent;

	private String originalFilename;

	public CustomMultiPartFile(byte[] imgContent, String originalFilename) {
		this.imgContent = imgContent;
		this.originalFilename = originalFilename;
	}

	@Override
	public String getName() {
		// TODO - implementation depends on your requirements
		return null;
	}

	@Override
	public String getOriginalFilename() {
		return originalFilename;
	}

	@Override
	public String getContentType() {
		return null;
	}

	@Override
	public boolean isEmpty() {
		return imgContent == null || imgContent.length == 0;
	}

	@Override
	public long getSize() {
		return imgContent.length;
	}

	@Override
	public byte[] getBytes() throws IOException {
		return imgContent;
	}

	@Override
	public InputStream getInputStream() throws IOException {
		return new ByteArrayInputStream(imgContent);
	}

	@Override
	public void transferTo(File dest) throws IOException, IllegalStateException {

	}

	public void setOriginalFilename(String originalFilename) {
		this.originalFilename = originalFilename;
	}
}
package com.cmg.idsmed.dto.product;

import java.util.ArrayList;
import java.util.List;

import com.cmg.idsmed.dto.AbstractResponse;

public class ProductAddInfoDetailResponse extends AbstractResponse{
	private List<ProductCategoryResponse> productCategoryList = new ArrayList<>();
	private List<ProductBrandResponse> productBrandList = new ArrayList<>();
	private List<ProductCareAreaResponse> productCareAreaList = new ArrayList<>();
	private List<ProductSecondCategoryResponse> productSecondCategoryList = new ArrayList<>();

	public ProductAddInfoDetailResponse(List<ProductCategoryResponse> productCategoryList,
										List<ProductBrandResponse> productBrandList,
										List<ProductCareAreaResponse> productCareAreaList,
										List<ProductSecondCategoryResponse> productSecondCategoryList) {
		this.productCategoryList = productCategoryList;
		this.productBrandList = productBrandList;
		this.productCareAreaList = productCareAreaList;
		this.productSecondCategoryList = productSecondCategoryList;
	}

	public List<ProductCategoryResponse> getProductCategoryList() {
		return productCategoryList;
	}

	public void setProductCategoryList(List<ProductCategoryResponse> productCategoryList) {
		this.productCategoryList = productCategoryList;
	}

	public List<ProductBrandResponse> getProductBrandList() {
		return productBrandList;
	}

	public void setProductBrandList(List<ProductBrandResponse> productBrandList) {
		this.productBrandList = productBrandList;
	}

	public List<ProductCareAreaResponse> getProductCareAreaList() {
		return productCareAreaList;
	}

	public void setProductCareAreaList(List<ProductCareAreaResponse> productCareAreaList) {
		this.productCareAreaList = productCareAreaList;
	}

	public List<ProductSecondCategoryResponse> getProductSecondCategoryList() {
		return productSecondCategoryList;
	}

	public void setProductSecondCategoryList(List<ProductSecondCategoryResponse> productSecondCategoryList) {
		this.productSecondCategoryList = productSecondCategoryList;
	}
}

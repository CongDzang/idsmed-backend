package com.cmg.idsmed.dto.product;

import com.cmg.idsmed.service.product.ProductServiceImpl;

import java.util.ArrayList;
import java.util.List;

public class ProductApiListResponse {
    private Integer totalPage = null;
    private List<ProductApiResponse> productApiList = new ArrayList<>();
    private Integer totalCount = null;
    private Integer pageNo = null;
    private Integer pageSize = null;

    public ProductApiListResponse(Integer pageNo, Integer pageSize) {
        this.totalPage = 0;
        this.totalCount = 0;
        this.pageSize = pageSize;
        this.pageNo = pageNo;
    }

    public ProductApiListResponse(Integer totalCount, Integer pageIndex, Integer pageSize, List<ProductApiResponse> productList) {
        if (pageSize != null && !pageSize.equals(0)) {
            Integer calTotalPage = totalCount/pageSize;
            if(totalCount % pageSize > 0) {
                calTotalPage++;
            }
            this.totalPage = calTotalPage;
        }
        this.totalCount = totalCount;

        Integer pageCount = pageSize >= totalCount ? 1 : (totalCount%pageSize > 0 ? totalCount/pageSize +1 : totalCount/pageSize);
        Integer pageNo = pageIndex*pageSize >= totalCount ? pageCount-1 : pageIndex;

        this.pageNo = pageNo+1;
        this.pageSize = pageSize;
        this.productApiList = productList;
    }

    public ProductApiListResponse(Integer totalCount, List<ProductApiResponse> productApiList) {
        this.totalCount = totalCount;
        this.productApiList = productApiList;
        this.totalPage = 1;
        this.pageSize = totalCount;
        this.pageNo = 1;
    }

    public ProductApiListResponse(List<ProductApiResponse> productApiList){
        this.pageSize = ProductServiceImpl.PAGE_INDEX_DEFAULT_FOR_NEW_API;
        this.productApiList = productApiList;
    }

    public Integer getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(Integer totalPage) {
        this.totalPage = totalPage;
    }

    public List<ProductApiResponse> getProductList() {
        return productApiList;
    }

    public void setProductList(List<ProductApiResponse> productList) {
        this.productApiList = productApiList;
    }

    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    public Integer getPageNo() {
        return pageNo;
    }

    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

}

package com.cmg.idsmed.dto.product;

import com.cmg.idsmed.dto.AbstractResponse;
import com.cmg.idsmed.dto.product.details.ProductMarketingResponse;
import com.cmg.idsmed.model.entity.product.Product;
import com.cmg.idsmed.model.entity.product.ProductFeatures;
import com.cmg.idsmed.model.entity.product.ProductMedia;
import com.cmg.idsmed.model.entity.product.ProductTechnical;
import org.springframework.beans.BeanUtils;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ProductApiResponse extends AbstractResponse {
    private Long productId;

    private String productNamePrimary;

    private String productNameSecondary;

    private String productDescription;

    private List<ProductMediaResponse> productMediaList = new ArrayList<>();

    private BigDecimal price;

    private List<ProductFeaturesResponse> productFeaturesList = new ArrayList<>();

    private List<ProductTechnicalResponse> productTechnicalList = new ArrayList<>();

    public ProductApiResponse() {
    }

    public ProductApiResponse(Product product, String ossBucketDomain) {
        this.productId = product.getId();
        this.productNamePrimary = product.getProductNamePrimary();
        this.productNameSecondary = product.getProductNameSecondary();
        this.productDescription = product.getProductDescription();
        this.productMediaList = product.getProductMediaList()
                .stream()
                .filter(pm -> pm.getDocumentType().equals(ProductMedia.MEDIA_TYPE.IMAGE.getType())).map(pm -> new ProductMediaResponse(pm, ossBucketDomain))
                .collect(Collectors.toList());
        this.price = product.getPrice();
        this.productFeaturesList = CollectionUtils.isEmpty(product.getProductFeaturesList()) ? new ArrayList<ProductFeaturesResponse>() :
                product.getProductFeaturesList()
                        .stream()
                        .map(pf -> new ProductFeaturesResponse(pf))
                        .collect(Collectors.toList());
        this.productTechnicalList = CollectionUtils.isEmpty(product.getProductTechnicalList()) ? new ArrayList<ProductTechnicalResponse>() :
                product.getProductTechnicalList()
                        .stream()
                        .map(pt -> new ProductTechnicalResponse(pt))
                        .collect(Collectors.toList());
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public List<ProductMediaResponse> getProductMediaList() {
        return productMediaList;
    }

    public void setProductMediaList(List<ProductMediaResponse> productMediaList) {
        this.productMediaList = productMediaList;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getProductNamePrimary() {
        return productNamePrimary;
    }

    public void setProductNamePrimary(String productName) {
        this.productNamePrimary = productNamePrimary;
    }

    public String getProductNameSecondary() {
        return productNameSecondary;
    }

    public void setProductNameSecondary(String productNameSecondary) {
        this.productNameSecondary = productNameSecondary;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public List<ProductFeaturesResponse> getProductFeaturesList() {
        return productFeaturesList;
    }

    public void setProductFeaturesList(List<ProductFeaturesResponse> productFeaturesList) {
        this.productFeaturesList = productFeaturesList;
    }

    public List<ProductTechnicalResponse> getProductTechnicalList() {
        return productTechnicalList;
    }

    public void setProductTechnicalList(List<ProductTechnicalResponse> productTechnicalList) {
        this.productTechnicalList = productTechnicalList;
    }
}
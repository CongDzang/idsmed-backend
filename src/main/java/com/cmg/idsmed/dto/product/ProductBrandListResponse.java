package com.cmg.idsmed.dto.product;

import java.util.List;

public class ProductBrandListResponse {
	private Integer totalPage = null;
	private List<ProductBrandResponse> productBrandList;
	private Integer totalCount = null;
	private Integer pageNo = null;
	private Integer pageSize = null;
	private ProductResponse productResponse;

	public ProductBrandListResponse(Integer pageNo, Integer pageSize) {
		this.totalPage = 0;
		this.totalCount = 0;
		this.pageSize = pageSize;
		this.pageNo = pageNo;
	}

	public ProductBrandListResponse(Integer totalCount, Integer pageIndex, Integer pageSize, List<ProductBrandResponse> productBrandList) {
		if (pageSize != null && !pageSize.equals(0)) {
			Integer calTotalPage = totalCount/pageSize;
			if(totalCount % pageSize > 0) {
				calTotalPage++;
			}
			this.totalPage = calTotalPage;
		}
		this.totalCount = totalCount;
		
		Integer pageCount = pageSize >= totalCount ? 1 : (totalCount%pageSize > 0 ? totalCount/pageSize +1 : totalCount/pageSize);
		Integer pageNo = pageIndex*pageSize >= totalCount ? pageCount-1 : pageIndex;
		
		this.pageNo = pageNo+1;
		this.pageSize = pageSize;
		this.productBrandList = productBrandList;
	}

	public ProductBrandListResponse(Integer totalCount, List<ProductBrandResponse> productBrandList) {
		this.totalCount = totalCount;
		this.productBrandList = productBrandList;
		this.totalPage = 1;
		this.pageSize = totalCount;
		this.pageNo = 1;
	}

	public Integer getTotalPage() {
		return totalPage;
	}

	public void setTotalPage(Integer totalPage) {
		this.totalPage = totalPage;
	}

	public List<ProductBrandResponse> getProductBrandList() {
		return productBrandList;
	}

	public void setProductBrandList(List<ProductBrandResponse> productBrandList) {
		this.productBrandList = productBrandList;
	}

	public Integer getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}

	public Integer getPageNo() {
		return pageNo;
	}

	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public ProductResponse getProductResponse() {
		return productResponse;
	}

	public void setProductResponse(ProductResponse productResponse) {
		this.productResponse = productResponse;
	}
}

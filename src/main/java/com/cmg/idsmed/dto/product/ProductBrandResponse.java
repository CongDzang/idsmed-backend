package com.cmg.idsmed.dto.product;

import com.cmg.idsmed.common.Constant.PropertiesLabelConst;
import com.cmg.idsmed.common.utils.OssUtils;
import com.cmg.idsmed.dto.AbstractResponse;
import com.cmg.idsmed.model.entity.product.ProductBrand;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.util.StringUtils;

public class ProductBrandResponse extends AbstractResponse {

	private Long id;

	private String brandName;

	private String brandCode;

	private String brandImageURL;

	public ProductBrandResponse(ProductBrand productBrand, String ossBucketDomain) {
		this.id = productBrand.getId();
		this.brandName = productBrand.getBrandName();
		this.brandCode = productBrand.getBrandCode();
		if (!StringUtils.isEmpty(ossBucketDomain)) {
			this.brandImageURL = ossBucketDomain + productBrand.getBrandImageURL();
		} else {
			this.brandImageURL = null;
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public String getBrandCode() {
		return brandCode;
	}

	public void setBrandCode(String brandCode) {
		this.brandCode = brandCode;
	}

	public String getBrandImageURL() {
		return brandImageURL;
	}

	public void setBrandImageURL(String brandImageURL) {
		this.brandImageURL = brandImageURL;
	}
}

package com.cmg.idsmed.dto.product;

import org.springframework.data.util.Pair;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.Arrays;
import java.util.List;

public class ProductCSVProcessStatus {
	private List<Long> productIds;
	private List<Integer> errorCSVRowNum;
	private List<Pair<Integer, List<String>>> errorResizedImageInfo;

	public String getCSVErrorRowsInfo() {
		if (CollectionUtils.isEmpty(errorCSVRowNum)) {
			return null;
		}
		Object [] errorNums  = errorCSVRowNum.toArray();
		String csvErrorRowsInfo = Arrays.toString(errorNums);
		return !StringUtils.isEmpty(csvErrorRowsInfo) ? csvErrorRowsInfo : null;
	}

	public List<Long> getProductIds() {
		return productIds;
	}

	public void setProductIds(List<Long> productIds) {
		this.productIds = productIds;
	}

	public List<Integer> getErrorCSVRowNum() {
		return errorCSVRowNum;
	}

	public void setErrorCSVRowNum(List<Integer> errorCSVRowNum) {
		this.errorCSVRowNum = errorCSVRowNum;
	}

	public List<Pair<Integer, List<String>>> getErrorResizedImageInfo() {
		return errorResizedImageInfo;
	}

	public void setErrorResizedImageInfo(List<Pair<Integer, List<String>>> errorResizedImageInfo) {
		this.errorResizedImageInfo = errorResizedImageInfo;
	}
}

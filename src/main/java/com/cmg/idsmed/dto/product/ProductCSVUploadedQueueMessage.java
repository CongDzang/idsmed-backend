package com.cmg.idsmed.dto.product;

public class ProductCSVUploadedQueueMessage {

	private String productCSVOssUrl;
	private Long productCsvLogId;
	private Long vendorId;
	private String uploadBy;

	public ProductCSVUploadedQueueMessage() {
	}

	public ProductCSVUploadedQueueMessage(String productCSVOssUrl, Long productCsvLogId, Long vendorId, String uploadBy) {
		this.productCSVOssUrl = productCSVOssUrl;
		this.productCsvLogId = productCsvLogId;
		this.vendorId = vendorId;
		this.uploadBy = uploadBy;
	}

	public String getProductCSVOssUrl() {
		return productCSVOssUrl;
	}

	public void setProductCSVOssUrl(String productCSVOssUrl) {
		this.productCSVOssUrl = productCSVOssUrl;
	}

	public Long getProductCsvLogId() {
		return productCsvLogId;
	}

	public void setProductCsvLogId(Long productCsvLogId) {
		this.productCsvLogId = productCsvLogId;
	}

	public Long getVendorId() {
		return vendorId;
	}

	public void setVendorId(Long vendorId) {
		this.vendorId = vendorId;
	}

	public String getUploadBy() {
		return uploadBy;
	}

	public void setUploadBy(String uploadBy) {
		this.uploadBy = uploadBy;
	}
}

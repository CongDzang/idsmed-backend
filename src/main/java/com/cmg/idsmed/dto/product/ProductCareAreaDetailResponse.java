package com.cmg.idsmed.dto.product;

import org.springframework.beans.BeanUtils;

import com.cmg.idsmed.model.entity.product.ProductCareAreaDetail;

public class ProductCareAreaDetailResponse {

    private Long id;
    
    private String careAreaName;
    
    private ProductCareAreaResponse productCareArea;

	public ProductCareAreaDetailResponse(ProductCareAreaDetail productCareAreaDetail, String ossBucketDomain) {
		BeanUtils.copyProperties(productCareAreaDetail, this);

		if (productCareAreaDetail.getProductCareArea() != null) {
			this.productCareArea = new ProductCareAreaResponse(productCareAreaDetail.getProductCareArea(), ossBucketDomain);
			this.careAreaName = productCareArea.getCareAreaName();
		}
	}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

	public ProductCareAreaResponse getProductCareArea() {
		return productCareArea;
	}

	public void setProductCareArea(ProductCareAreaResponse productCareArea) {
		this.productCareArea = productCareArea;
	}

	public String getCareAreaName() {
		return careAreaName;
	}

	public void setCareAreaName(String careAreaName) {
		this.careAreaName = careAreaName;
	}
}

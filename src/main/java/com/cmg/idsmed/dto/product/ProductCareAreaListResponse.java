package com.cmg.idsmed.dto.product;

import java.util.List;

public class ProductCareAreaListResponse {
	private Integer totalPage = null;
	private List<ProductCareAreaResponse> careAreaList;
	private Integer totalCount = null;
	private Integer pageNo = null;
	private Integer pageSize = null;

	public ProductCareAreaListResponse(Integer totalCount, Integer pageIndex, Integer pageSize, List<ProductCareAreaResponse> careAreaList) {
		if (pageSize != null && !pageSize.equals(0)) {
			Integer calTotalPage = totalCount/pageSize;
			if(totalCount % pageSize > 0) {
				calTotalPage++;
			}
			this.totalPage = calTotalPage;
		}
		this.totalCount = totalCount;
		this.pageNo = pageIndex + 1;
		this.pageSize = pageSize;
		this.careAreaList = careAreaList;
	}

	public ProductCareAreaListResponse(Integer totalCount, List<ProductCareAreaResponse> careAreaList) {
		this.totalCount = totalCount;
		this.careAreaList = careAreaList;
		this.totalPage = 1;
		this.pageSize = totalCount;
		this.pageNo = 1;
	}

	public Integer getTotalPage() {
		return totalPage;
	}

	public void setTotalPage(Integer totalPage) {
		this.totalPage = totalPage;
	}

	public List<ProductCareAreaResponse> getCareAreaList() {
		return careAreaList;
	}

	public void setCareAreaList(List<ProductCareAreaResponse> careAreaList) {
		this.careAreaList = careAreaList;
	}

	public Integer getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}

	public Integer getPageNo() {
		return pageNo;
	}

	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}
}

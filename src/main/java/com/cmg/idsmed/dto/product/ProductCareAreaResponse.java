package com.cmg.idsmed.dto.product;

import com.cmg.idsmed.common.enums.LangEnum;
import com.cmg.idsmed.model.entity.product.ProductCareArea;
import org.apache.commons.codec.language.bm.Lang;
import org.springframework.beans.BeanUtils;
import org.springframework.util.StringUtils;

public class ProductCareAreaResponse {

    private Long id;

    private String careAreaName;

    private String documentPath;
    
    private String documentUrl;

    private String documentUrlWedoctor;

    private Long totalProducts = 0L;

    public ProductCareAreaResponse(ProductCareArea productCareArea) {
        BeanUtils.copyProperties(productCareArea, this);
    }

	public ProductCareAreaResponse(ProductCareArea productCareArea, String ossBucketDomain) {
		String [] ignoreProperties = {"documentUrl"};
		BeanUtils.copyProperties(productCareArea, this, ignoreProperties);
		if (!StringUtils.isEmpty(productCareArea.getDocumentUrl()) && !StringUtils.isEmpty(ossBucketDomain)) {
			this.documentUrl = ossBucketDomain + productCareArea.getDocumentUrl();
		}
	}


	public ProductCareAreaResponse(ProductCareArea productCareArea, String langCode, String ossBucketDomain) {
		this.id = productCareArea.getId();
		this.documentPath = productCareArea.getDocumentPath();
		if (!StringUtils.isEmpty(productCareArea.getDocumentUrl()) && !StringUtils.isEmpty(ossBucketDomain)) {
			this.documentUrl = ossBucketDomain + productCareArea.getDocumentUrl();
		}
		this.documentUrlWedoctor = productCareArea.getDocumentUrlWedoctor();

		LangEnum langEnum = LangEnum.CHINA;
		if (langCode != null) {
			for (LangEnum lang : LangEnum.values()) {
				if (langCode.equals(lang.getCode())) {
					langEnum = lang;
					break;
				}
			}
		}

		switch (langEnum) {
			case CHINA: this.careAreaName = productCareArea.getCareAreaNameZhCn(); break;
			case ENGLISH: this.careAreaName = productCareArea.getCareAreaName(); break;
			case CHINA_TAIWAN: this.careAreaName = productCareArea.getCareAreaNameZhTw(); break;
			case VIETNAMESE: this.careAreaName = productCareArea.getCareAreaNameVi(); break;
			case THAI: this.careAreaName = productCareArea.getCareAreaNameTh(); break;
			case INDONESIAN: this.careAreaName = productCareArea.getCareAreaNameId(); break;
		}
	}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCareAreaName() {
        return careAreaName;
    }

    public void setCareAreaName(String careAreaName) {
        this.careAreaName = careAreaName;
    }

	public String getDocumentPath() {
		return documentPath;
	}

	public void setDocumentPath(String documentPath) {
		this.documentPath = documentPath;
	}

	public String getDocumentUrl() {
		return documentUrl;
	}

	public void setDocumentUrl(String documentUrl) {
		this.documentUrl = documentUrl;
	}

	public String getDocumentUrlWedoctor() {
		return documentUrlWedoctor;
	}

	public void setDocumentUrlWedoctor(String documentUrlWedoctor) {
		this.documentUrlWedoctor = documentUrlWedoctor;
	}

	public Long getTotalProducts() {
		return totalProducts;
	}

	public void setTotalProducts(Long totalProducts) {
		this.totalProducts = totalProducts;
	}
}

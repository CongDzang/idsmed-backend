package com.cmg.idsmed.dto.product;

import java.util.List;

public class ProductCategoryListResponse {
	private Integer totalPage = null;
	private List<ProductCategoryResponse> productCategoryList;
	private Integer totalCount = null;
	private Integer pageNo = null;
	private Integer pageSize = null;
	private ProductResponse productResponse;

	public ProductCategoryListResponse(Integer pageNo, Integer pageSize) {
		this.totalPage = 0;
		this.totalCount = 0;
		this.pageSize = pageSize;
		this.pageNo = pageNo;
	}

	public ProductCategoryListResponse(Integer totalCount, Integer pageIndex, Integer pageSize, List<ProductCategoryResponse> productCategoryList) {
		if (pageSize != null && !pageSize.equals(0)) {
			Integer calTotalPage = totalCount/pageSize;
			if(totalCount % pageSize > 0) {
				calTotalPage++;
			}
			this.totalPage = calTotalPage;
		}
		this.totalCount = totalCount;
		
		Integer pageCount = pageSize >= totalCount ? 1 : (totalCount%pageSize > 0 ? totalCount/pageSize +1 : totalCount/pageSize);
		Integer pageNo = pageIndex*pageSize >= totalCount ? pageCount-1 : pageIndex;
		
		this.pageNo = pageNo+1;
		this.pageSize = pageSize;
		this.productCategoryList = productCategoryList;
	}

	public ProductCategoryListResponse(Integer totalCount, List<ProductCategoryResponse> productCategoryList) {
		this.totalCount = totalCount;
		this.productCategoryList = productCategoryList;
		this.totalPage = 1;
		this.pageSize = totalCount;
		this.pageNo = 1;
	}

	public Integer getTotalPage() {
		return totalPage;
	}

	public void setTotalPage(Integer totalPage) {
		this.totalPage = totalPage;
	}

	public List<ProductCategoryResponse> getProductCategoryList() {
		return productCategoryList;
	}

	public void setProductCategoryList(List<ProductCategoryResponse> productCategoryList) {
		this.productCategoryList = productCategoryList;
	}

	public Integer getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}

	public Integer getPageNo() {
		return pageNo;
	}

	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public ProductResponse getProductResponse() {
		return productResponse;
	}

	public void setProductResponse(ProductResponse productResponse) {
		this.productResponse = productResponse;
	}
}

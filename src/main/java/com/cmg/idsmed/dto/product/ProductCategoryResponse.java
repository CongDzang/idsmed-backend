package com.cmg.idsmed.dto.product;

import com.cmg.idsmed.common.enums.LangEnum;
import com.cmg.idsmed.dto.AbstractResponse;
import com.cmg.idsmed.model.entity.product.ProductCategory;
import org.springframework.beans.BeanUtils;


public class ProductCategoryResponse extends AbstractResponse {

	private Long id;

	private String categoryName;

	private Long totalProducts = 0L;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ProductCategoryResponse(ProductCategory productCategory) {
		BeanUtils.copyProperties(productCategory, this);
	}

	public ProductCategoryResponse(ProductCategory productCategory, String langCode) {
		if (langCode == null || langCode.equalsIgnoreCase(LangEnum.ENGLISH.getCode())) {
			this.id = productCategory.getId();
			this.categoryName = productCategory.getCategoryName();
		}

		if (langCode.equalsIgnoreCase(LangEnum.CHINA.getCode())) {
			this.id = productCategory.getId();
			this.categoryName = productCategory.getCategoryNameZhCn();
		}

		if (langCode.equalsIgnoreCase(LangEnum.CHINA_TAIWAN.getCode())) {
			this.id = productCategory.getId();
			this.categoryName = productCategory.getCategoryNameZhTw();
		}

		if (langCode.equalsIgnoreCase(LangEnum.THAI.getCode())) {
			this.id = productCategory.getId();
			this.categoryName = productCategory.getCategoryNameTh();
		}

		if (langCode.equalsIgnoreCase(LangEnum.VIETNAMESE.getCode())) {
			this.id = productCategory.getId();
			this.categoryName = productCategory.getCategoryNameVi();
		}

		if (langCode.equalsIgnoreCase(LangEnum.INDONESIAN.getCode())) {
			this.id = productCategory.getId();
			this.categoryName = productCategory.getCategoryNameId();
		}

	}

	public Long getTotalProducts() {
		return totalProducts;
	}

	public void setTotalProducts(Long totalProducts) {
		this.totalProducts = totalProducts;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
}

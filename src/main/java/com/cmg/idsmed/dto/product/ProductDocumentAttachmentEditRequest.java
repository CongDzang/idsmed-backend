package com.cmg.idsmed.dto.product;

import com.cmg.idsmed.dto.AbstractRequest;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.validation.constraints.Size;
import java.io.IOException;
import java.util.List;

public class ProductDocumentAttachmentEditRequest extends AbstractRequest {
    private Long productId;

    private List<ProductDocumentAttachmentRequest> productDocumentAttachmentRequestList;

    public ProductDocumentAttachmentEditRequest() {
        super();
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public List<ProductDocumentAttachmentRequest> getProductDocumentAttachmentRequestList() { return productDocumentAttachmentRequestList; }

    public void setProductDocumentAttachmentRequestList(List<ProductDocumentAttachmentRequest> productDocumentAttachmentRequestList) { this.productDocumentAttachmentRequestList = productDocumentAttachmentRequestList; }

}

package com.cmg.idsmed.dto.product;

import com.cmg.idsmed.dto.AbstractRequest;

/**
 * @author DucPham
 */
public class ProductDocumentAttachmentReminderRequest extends AbstractRequest {

	private Long id;

	private Integer daysBefore;
	
	private Integer daysAfter;
	
	private Boolean inExpiryDate;
	
	private Boolean byEmail;
	
	private Boolean byWeb;
	
	private Boolean bySms;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getDaysBefore() {
		return daysBefore;
	}

	public void setDaysBefore(Integer daysBefore) {
		this.daysBefore = daysBefore;
	}

	public Integer getDaysAfter() {
		return daysAfter;
	}

	public void setDaysAfter(Integer daysAfter) {
		this.daysAfter = daysAfter;
	}

	public Boolean getInExpiryDate() {
		return inExpiryDate;
	}

	public void setInExpiryDate(Boolean inExpiryDate) {
		this.inExpiryDate = inExpiryDate;
	}

	public Boolean getByEmail() {
		return byEmail;
	}

	public void setByEmail(Boolean byEmail) {
		this.byEmail = byEmail;
	}

	public Boolean getByWeb() {
		return byWeb;
	}

	public void setByWeb(Boolean byWeb) {
		this.byWeb = byWeb;
	}

	public Boolean getBySms() {
		return bySms;
	}

	public void setBySms(Boolean bySms) {
		this.bySms = bySms;
	}
}

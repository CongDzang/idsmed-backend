package com.cmg.idsmed.dto.product;

import java.time.ZonedDateTime;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.validation.constraints.Size;

import com.cmg.idsmed.dto.AbstractRequest;

/**
 * @author Chong Sian
 */
public class ProductDocumentAttachmentRequest extends AbstractRequest {

	private Long id;

	private Long productId;

	private String documentName;
	@Size(max = 500)
	private String documentSecondaryName;

	@Size(max = 3000)
	private String remarks;

	private Long productLabelConfigId;

	@Size(max = 1000)
	private String originalFileName;

	@Size(max = 1000)
	private String documentPath;

	private ZonedDateTime expiryDate;

	private String expDateTimeZone;

	private Integer status;

	private Boolean isVerify;

	private Integer fileIndex;
	
	private List<ProductDocumentAttachmentReminderRequest> productDocumentAttachmentReminderRequests;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public String getDocumentName() {
		return documentName;
	}

	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Long getProductLabelConfigId() {
		return productLabelConfigId;
	}

	public void setProductLabelConfigId(Long productLabelConfigId) { this.productLabelConfigId = productLabelConfigId; }

	public String getOriginalFileName() {
		return originalFileName;
	}

	public void setOriginalFileName(String originalFileName) {
		this.originalFileName = originalFileName;
	}

	public String getDocumentSecondaryName() {
		return documentSecondaryName;
	}

	public void setDocumentSecondaryName(String documentSecondaryName) {
		this.documentSecondaryName = documentSecondaryName;
	}

	public ZonedDateTime getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(ZonedDateTime expiryDate) {
		this.expiryDate = expiryDate;
	}

	public Boolean getIsVerify() { return isVerify; }

	public void setIsVerify(Boolean isVerify) { this.isVerify = isVerify; }

	public Integer getStatus() { return status; }

	public void setStatus(Integer status) { this.status = status; }

	public Boolean getVerify() {
		return isVerify;
	}

	public void setVerify(Boolean verify) {
		isVerify = verify;
	}

	public Integer getFileIndex() {
		return fileIndex;
	}

	public void setFileIndex(Integer fileIndex) {
		this.fileIndex = fileIndex;
	}

	public String getDocumentPath() {
		return documentPath;
	}

	public void setDocumentPath(String documentPath) {
		this.documentPath = documentPath;
	}

	public List<ProductDocumentAttachmentReminderRequest> getProductDocumentAttachmentReminderRequests() {
		return productDocumentAttachmentReminderRequests;
	}

	public void setProductDocumentAttachmentReminderRequests(
			List<ProductDocumentAttachmentReminderRequest> productDocumentAttachmentReminderRequests) {
		this.productDocumentAttachmentReminderRequests = productDocumentAttachmentReminderRequests;
	}

	public String getExpDateTimeZone() {
		return expDateTimeZone;
	}

	public void setExpDateTimeZone(String expDateTimeZone) {
		this.expDateTimeZone = expDateTimeZone;
	}
}

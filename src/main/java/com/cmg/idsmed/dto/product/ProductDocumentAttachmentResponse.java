package com.cmg.idsmed.dto.product;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;

import com.cmg.idsmed.dto.AbstractResponse;
import com.cmg.idsmed.model.entity.product.ProductDocumentAttachment;
import org.springframework.util.StringUtils;

/**
 * @author Chong Sian
 */
public class ProductDocumentAttachmentResponse extends AbstractResponse {

	private Long id;

	private String documentName;

	private String documentPath;

	private Integer documentSeq;

	private String remarks;

	private LocalDateTime expiryDate;

	private String countryCode;

	private Boolean isVerify;

	private Integer status;
	
	private List<ProductDocumentAttachmentReminderResponse> documentAttachmentReminders;

	public ProductDocumentAttachmentResponse(ProductDocumentAttachment productDocumentAttachment, String ossBucketDomain) {
		if (productDocumentAttachment != null) {
			this.id = productDocumentAttachment.getId();
			this.documentName = productDocumentAttachment.getDocumentName();
			if (!StringUtils.isEmpty(ossBucketDomain)) {
				this.documentPath = ossBucketDomain + productDocumentAttachment.getDocumentPath();
			}
			this.remarks = productDocumentAttachment.getRemarks();
			this.expiryDate = productDocumentAttachment.getExpiryDate();
			this.countryCode = productDocumentAttachment.getCountryCode();
			this.isVerify = productDocumentAttachment.getIsVerify();
			this.status = productDocumentAttachment.getStatus();
		}

		if (!CollectionUtils.isEmpty(productDocumentAttachment.getProductDocumentAttachmentReminderList())) {
			this.documentAttachmentReminders = productDocumentAttachment.getProductDocumentAttachmentReminderList().stream().map(pdar -> new ProductDocumentAttachmentReminderResponse(pdar)).collect(Collectors.toList());
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDocumentName() {
		return documentName;
	}

	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}

	public String getDocumentPath() {
		return documentPath;
	}

	public void setDocumentPath(String documentPath) {
		this.documentPath = documentPath;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public LocalDateTime getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(LocalDateTime expiryDate) {
		this.expiryDate = expiryDate;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public Integer getDocumentSeq() { return documentSeq; }

	public void setDocumentSeq(Integer documentSeq) { this.documentSeq = documentSeq; }

	public Boolean getIsVerify() { return isVerify; }

	public void setIsVerify(Boolean isVerify) { this.isVerify = isVerify; }

	public Integer getStatus() { return status; }

	public void setStatus(Integer status) { this.status = status; }

	public List<ProductDocumentAttachmentReminderResponse> getDocumentAttachmentReminders() {
		return documentAttachmentReminders;
	}

	public void setDocumentAttachmentReminders(
			List<ProductDocumentAttachmentReminderResponse> documentAttachmentReminders) {
		this.documentAttachmentReminders = documentAttachmentReminders;
	}
}

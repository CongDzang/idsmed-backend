package com.cmg.idsmed.dto.product;

import com.cmg.idsmed.dto.AbstractRequest;

public class ProductExcelUploadRequest extends AbstractRequest {
	private String fileType;
	private Long vendorId;
	private String uploadBy;

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public Long getVendorId() {
		return vendorId;
	}

	public void setVendorId(Long vendorId) {
		this.vendorId = vendorId;
	}

	public String getUploadBy() {
		return uploadBy;
	}

	public void setUploadBy(String uploadBy) {
		this.uploadBy = uploadBy;
	}
}

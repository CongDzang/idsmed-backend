package com.cmg.idsmed.dto.product;

public class ProductExcelUploadedQueueMessage {

	private String productExcelOssUrl;
	private Long productExcelLogId;
	private Long vendorId;
	private String uploadBy;

	public ProductExcelUploadedQueueMessage() {
	}

	public ProductExcelUploadedQueueMessage(String productExcelOssUrl, Long productExcelLogId, Long vendorId, String uploadBy) {
		this.productExcelOssUrl = productExcelOssUrl;
		this.productExcelLogId = productExcelLogId;
		this.vendorId = vendorId;
		this.uploadBy = uploadBy;
	}

	public String getProductExcelOssUrl() {
		return productExcelOssUrl;
	}

	public void setProductExcelOssUrl(String productExcelOssUrl) {
		this.productExcelOssUrl = productExcelOssUrl;
	}

	public Long getProductExcelLogId() {
		return productExcelLogId;
	}

	public void setProductCsvLogId(Long productExcelLogId) {
		this.productExcelLogId = productExcelLogId;
	}

	public Long getVendorId() {
		return vendorId;
	}

	public void setVendorId(Long vendorId) {
		this.vendorId = vendorId;
	}

	public String getUploadBy() { return uploadBy; }

	public void setUploadBy(String uploadBy) { this.uploadBy = uploadBy; }
}

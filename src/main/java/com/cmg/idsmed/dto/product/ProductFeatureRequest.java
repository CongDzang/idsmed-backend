package com.cmg.idsmed.dto.product;

import java.io.IOException;

import javax.validation.constraints.Size;

import com.cmg.idsmed.dto.AbstractRequest;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ProductFeatureRequest extends AbstractRequest {
	
	public ProductFeatureRequest() {
		super();
	}
	
	public ProductFeatureRequest(String jsonString) {
		ObjectMapper mapper = new ObjectMapper();

		//JSON from String to Object
		try {
			ProductFeatureRequest pfd = mapper.readValue(jsonString, ProductFeatureRequest.class);
			this.featureSequence = pfd.getFeatureSequence();
			this.featureLabel = pfd.getFeatureLabel();
			this.featureValue = pfd.getFeatureValue();
			this.sellingPoint = pfd.getSellingPoint();
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public ProductFeatureRequest(Integer featureSequence, String featureLabel, String featureValue, Boolean sellingPoint) {
		this.featureSequence = featureSequence;
		this.featureLabel = featureLabel;
		this.featureValue = featureValue;
		this.sellingPoint = sellingPoint;
	}
	
	private Integer featureSequence;
	
	@Size(max=100)
	private String featureLabel;
	
	@Size(max=3000)
	private String featureValue;

	private Boolean sellingPoint;
	
	public Integer getFeatureSequence() {
		return featureSequence;
	}
	
	public void setFeatureSequence(Integer featureSequence) {
		this.featureSequence = featureSequence;
	}
	
	public String getFeatureLabel() {
		return featureLabel;
	}
	
	public void setFeatureLabel(String featureLabel) {
		this.featureLabel = featureLabel;
	}
	
	public String getFeatureValue() {
		return featureValue;
	}
	
	public void setFeatureValue(String featureValue) {
		this.featureValue = featureValue;
	}

	public Boolean getSellingPoint() { return sellingPoint; }

	public void setSellingPoint(Boolean sellingPoint) { this.sellingPoint = sellingPoint; }
}

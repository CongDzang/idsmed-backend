package com.cmg.idsmed.dto.product;
import com.cmg.idsmed.dto.AbstractResponse;
import com.cmg.idsmed.model.entity.auth.IdsmedUser;
import com.cmg.idsmed.model.entity.product.ProductRating;
import com.cmg.idsmed.model.entity.product.ProductRatingDetail;

public class ProductGeneralInfoRateResponse extends AbstractResponse {
    private Long productRateId;

    private Long totalCountOfRating;

    private Long totalCountOfFiveStar;

    private Long totalCountOfFourStar;

    private Long totalCountOfThreeStar;

    private Long totalCountOfTwoStar;

    private Long totalCountOfOneStar;

    private Long totalCountOfZeroStar;

    private ProductRatingDetailResponse productRatingDetail;

    private Double averageScore;

    private Double totalOfScore;

    public ProductGeneralInfoRateResponse(Long productRateId, ProductRatingDetail productRatingDetail, IdsmedUser idsmedUser, ProductRating productRating){
        if (productRateId != null) {
            this.productRateId = productRateId;
        }
        this.averageScore = productRating.getAverageScore();
        this.totalOfScore = productRating.getTotalOfScore();
        this.totalCountOfZeroStar = productRating.getTotalCountOfZeroStar();
        this.totalCountOfOneStar = productRating.getTotalCountOfOneStar();
        this.totalCountOfTwoStar = productRating.getTotalCountOfTwoStar();
        this.totalCountOfThreeStar = productRating.getTotalCountOfThreeStar();
        this.totalCountOfFourStar = productRating.getTotalCountOfFourStar();
        this.totalCountOfFiveStar = productRating.getTotalCountOfFiveStar();
        this.totalCountOfRating = productRating.getTotalCountOfRating();
        this.productRatingDetail = new ProductRatingDetailResponse(productRatingDetail, idsmedUser);
    }

    public ProductGeneralInfoRateResponse(ProductRating productRating){
        if (productRating != null) {
            this.totalCountOfFiveStar = productRating.getTotalCountOfFiveStar();
            this.totalCountOfFourStar = productRating.getTotalCountOfFourStar();
            this.totalCountOfThreeStar = productRating.getTotalCountOfThreeStar();
            this.totalCountOfTwoStar = productRating.getTotalCountOfTwoStar();
            this.totalCountOfOneStar = productRating.getTotalCountOfOneStar();
            this.totalCountOfZeroStar = productRating.getTotalCountOfZeroStar();
            this.totalCountOfRating = productRating.getTotalCountOfRating();
            this.totalOfScore = productRating.getTotalOfScore();
            this.averageScore = productRating.getAverageScore();
        } else {
            this.totalCountOfFiveStar = 0L;
            this.totalCountOfFourStar = 0L;
            this.totalCountOfThreeStar = 0L;
            this.totalCountOfTwoStar = 0L;
            this.totalCountOfOneStar = 0L;
            this.totalCountOfZeroStar = 0L;
            this.totalCountOfRating = 0L;
            this.totalOfScore = 0.0D;
            this.averageScore = 0.0D;
        }
    }


    public Long getProductRateId() {
        return productRateId;
    }

    public void setProductRateId(Long productRateId) {
        this.productRateId = productRateId;
    }

    public Double getAverageScore() { return averageScore; }

    public void setAverageScore(Double averageScore) { this.averageScore = averageScore; }

    public ProductRatingDetailResponse getProductRatingDetail() { return productRatingDetail; }

    public void setProductRatingDetail(ProductRatingDetailResponse productRatingDetail) { this.productRatingDetail = productRatingDetail; }

    public Long getTotalCountOfRating() { return totalCountOfRating; }

    public void setTotalCountOfRating(Long totalCountOfRating) { this.totalCountOfRating = totalCountOfRating; }

    public Long getTotalCountOfFiveStar() { return totalCountOfFiveStar; }

    public void setTotalCountOfFiveStar(Long totalCountOfFiveStar) { this.totalCountOfFiveStar = totalCountOfFiveStar; }

    public Long getTotalCountOfFourStar() { return totalCountOfFourStar; }

    public void setTotalCountOfFourStar(Long totalCountOfFourStar) { this.totalCountOfFourStar = totalCountOfFourStar; }

    public Long getTotalCountOfThreeStar() { return totalCountOfThreeStar; }

    public void setTotalCountOfThreeStar(Long totalCountOfThreeStar) { this.totalCountOfThreeStar = totalCountOfThreeStar; }

    public Long getTotalCountOfTwoStar() { return totalCountOfTwoStar; }

    public void setTotalCountOfTwoStar(Long totalCountOfTwoStar) { this.totalCountOfTwoStar = totalCountOfTwoStar; }

    public Long getTotalCountOfOneStar() { return totalCountOfOneStar; }

    public void setTotalCountOfOneStar(Long totalCountOfOneStar) { this.totalCountOfOneStar = totalCountOfOneStar; }

    public Long getTotalCountOfZeroStar() { return totalCountOfZeroStar; }

    public void setTotalCountOfZeroStar(Long totalCountOfZeroStar) { this.totalCountOfZeroStar = totalCountOfZeroStar; }

    public Double getTotalOfScore() { return totalOfScore; }

    public void setTotalOfScore(Double totalOfScore) { this.totalOfScore = totalOfScore; }
}

package com.cmg.idsmed.dto.product;

import com.cmg.idsmed.dto.AbstractResponse;
import com.cmg.idsmed.model.entity.product.ProductHierarchyLevelFour;
import com.cmg.idsmed.model.entity.product.ProductHierarchyLevelThree;

public class ProductHierarchyLevelThreeResponse extends AbstractResponse {
    private String code;

    private String name;

    private String chineseName;

    public ProductHierarchyLevelThreeResponse() {
    }

    public ProductHierarchyLevelThreeResponse(ProductHierarchyLevelThree productHierarchyLevelThree){
        this.code = productHierarchyLevelThree.getCode();
        this.name = productHierarchyLevelThree.getName();
        this.chineseName = productHierarchyLevelThree.getNameZhCn();
    }

    public String getCode() { return code; }

    public void setCode(String code) { this.code = code; }

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public String getChineseName() {
        return chineseName;
    }

    public void setChineseName(String chineseName) {
        this.chineseName = chineseName;
    }
}
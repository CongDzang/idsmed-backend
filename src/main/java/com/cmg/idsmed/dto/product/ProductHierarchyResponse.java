package com.cmg.idsmed.dto.product;

import com.cmg.idsmed.dto.AbstractResponse;
import com.cmg.idsmed.model.entity.product.ProductHierarchyLevelFour;

public class ProductHierarchyResponse extends AbstractResponse {
    private String code;

    private String name;

    private String chineseName;

    public ProductHierarchyResponse() {
    }

    public ProductHierarchyResponse(ProductHierarchyLevelFour productHierarchy){
        this.code = productHierarchy.getCode();
        this.name = productHierarchy.getName();
        this.chineseName = productHierarchy.getNameZhCn();
    }

    public String getCode() { return code; }

    public void setCode(String code) { this.code = code; }

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public String getChineseName() {
        return chineseName;
    }

    public void setChineseName(String chineseName) {
        this.chineseName = chineseName;
    }
}
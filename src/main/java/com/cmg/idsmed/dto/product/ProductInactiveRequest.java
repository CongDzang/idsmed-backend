package com.cmg.idsmed.dto.product;

import com.cmg.idsmed.dto.AbstractRequest;

import java.util.List;

public class ProductInactiveRequest extends AbstractRequest {
    private List<Long> id;
    private String inactiveReason;

    public List<Long> getId() {
        return id;
    }
    public void setId(List<Long> id) {
        this.id = id;
    }

    public String getInactiveReason() { return inactiveReason; }

    public void setInactiveReason(String inactiveReason) { this.inactiveReason = inactiveReason; }
}

package com.cmg.idsmed.dto.product;

import com.cmg.idsmed.dto.AbstractResponse;

import java.util.List;

public class ProductListSearchFieldsResponse extends AbstractResponse {
	private List<String> productModels;
	private List<String> vendorsName;

	public ProductListSearchFieldsResponse(List<String> productModels, List<String> vendorsName) {
		this.productModels = productModels;
		this.vendorsName = vendorsName;
	}

	public List<String> getProductModels() {
		return productModels;
	}

	public void setProductModels(List<String> productModels) {
		this.productModels = productModels;
	}

	public List<String> getVendorsName() {
		return vendorsName;
	}

	public void setVendorsName(List<String> vendorsName) {
		this.vendorsName = vendorsName;
	}
}

package com.cmg.idsmed.dto.product;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;

import com.cmg.idsmed.dto.AbstractResponse;
import com.cmg.idsmed.model.entity.product.Product;
import com.cmg.idsmed.model.entity.product.ProductMedia;
import com.cmg.idsmed.model.entity.product.ProductMedia.MEDIA_TYPE;

public class ProductMediaObjectResponse extends AbstractResponse {

	private ProductMediaResponse brochure;

	private ProductMediaResponse certificate;
	
	private ProductMediaResponse video;
	
	private List<ProductMediaResponse> images;

	public ProductMediaObjectResponse (Product product, String ossBucketDomain) {
		super();
		List<ProductMedia> pms = product.getProductMediaList();
		if (!CollectionUtils.isEmpty(pms)) {
			List<ProductMediaResponse> brochures = pms.stream().filter(pm -> MEDIA_TYPE.BROCHURE.getType().equalsIgnoreCase(pm.getDocumentType()))
					.map(pm -> new ProductMediaResponse(pm, ossBucketDomain)).collect(Collectors.toList());
			List<ProductMediaResponse> certificates = pms.stream().filter(pm -> MEDIA_TYPE.CERTIFICATE.getType().equalsIgnoreCase(pm.getDocumentType()))
					.map(pm -> new ProductMediaResponse(pm, ossBucketDomain)).collect(Collectors.toList());
			List<ProductMediaResponse> videos = pms.stream().filter(pm -> MEDIA_TYPE.VIDEO.getType().equalsIgnoreCase(pm.getDocumentType()))
					.map(pm -> new ProductMediaResponse(pm, ossBucketDomain)).collect(Collectors.toList());

			List<ProductMediaResponse> images = pms.stream()
					.filter(pm -> MEDIA_TYPE.IMAGE.getType().equalsIgnoreCase(pm.getDocumentType()))
					.map(pm -> new ProductMediaResponse(pm, ossBucketDomain))
					.sorted(Comparator.comparing(ProductMediaResponse::getDocumentSeq).thenComparing(ProductMediaResponse::getVersion))
					.collect(Collectors.toList());

			this.brochure = CollectionUtils.isEmpty(brochures) ? null : brochures.get(0);
			this.certificate = CollectionUtils.isEmpty(certificates) ? null : certificates.get(0);
			this.video = CollectionUtils.isEmpty(videos) ? null : videos.get(0);
			this.images = images;
		}
	}

	public ProductMediaResponse getBrochure() {
		return brochure;
	}

	public void setBrochure(ProductMediaResponse brochure) {
		this.brochure = brochure;
	}

	public ProductMediaResponse getCertificate() {
		return certificate;
	}

	public void setCertificate(ProductMediaResponse certificate) {
		this.certificate = certificate;
	}

	public ProductMediaResponse getVideo() {
		return video;
	}

	public void setVideo(ProductMediaResponse video) {
		this.video = video;
	}

	public List<ProductMediaResponse> getImages() {
		return images;
	}

	public void setImages(List<ProductMediaResponse> images) {
		this.images = images;
	}
}

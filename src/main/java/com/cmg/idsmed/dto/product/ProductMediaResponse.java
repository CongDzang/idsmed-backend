package com.cmg.idsmed.dto.product;

import org.springframework.beans.BeanUtils;

import com.cmg.idsmed.dto.AbstractResponse;
import com.cmg.idsmed.model.entity.product.ProductMedia;
import org.springframework.util.StringUtils;

public class ProductMediaResponse extends AbstractResponse {

	private Long id;

	private String documentName;

	private String documentUrl;
	
	private String documentType;
	
	private Integer version;

	private Integer documentSeq;
	
	private String gid;

	public ProductMediaResponse(ProductMedia productMedia, String ossBucketDomain) {
		if (productMedia != null) {
			this.id = productMedia.getId();
			this.documentName = productMedia.getDocumentName();
			if (!StringUtils.isEmpty(productMedia.getDocumentUrl()) && !StringUtils.isEmpty(ossBucketDomain)) {
				this.documentUrl = ossBucketDomain + productMedia.getDocumentUrl();
			}
			this.documentType = productMedia.getDocumentType();
			this.version = productMedia.getVersion();
			this.documentSeq = productMedia.getDocumentSeq();
			this.gid = productMedia.getGid();
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	public String getDocumentName() {
		return documentName;
	}

	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}
	
	public String getDocumentType() {
		return documentType;
	}

	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public String getGid() {
		return gid;
	}

	public void setGid(String gid) {
		this.gid = gid;
	}

	public Integer getDocumentSeq() {
		return documentSeq;
	}

	public void setDocumentSeq(Integer documentSeq) {
		this.documentSeq = documentSeq;
	}

	public String getDocumentUrl() {
		return documentUrl;
	}

	public void setDocumentUrl(String documentUrl) {
		this.documentUrl = documentUrl;
	}
}

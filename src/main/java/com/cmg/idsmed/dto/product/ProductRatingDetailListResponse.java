package com.cmg.idsmed.dto.product;

import com.cmg.idsmed.model.entity.product.ProductRatingDetail;

import java.util.ArrayList;
import java.util.List;

public class ProductRatingDetailListResponse {
    private List<ProductRatingDetailResponse> productRatingDetailList = new ArrayList<>();
    private Integer totalCount = null;
    private Integer pageNo = null;
    private Integer pageSize = null;
    private Integer totalPage = null;

    public ProductRatingDetailListResponse(){}

    public ProductRatingDetailListResponse(Integer pageNo, Integer pageSize){
        this.totalPage = 0;
        this.totalCount = 0;
        this.pageSize = pageSize;
        this.pageNo = pageNo;
    }
    public ProductRatingDetailListResponse(Integer totalCount, List<ProductRatingDetailResponse> productRatingDetailList) {
        this.totalCount = totalCount;
        this.productRatingDetailList = productRatingDetailList;
        this.totalPage = 1;
        this.pageSize = totalCount;
        this.pageNo = 1;
    }

    public ProductRatingDetailListResponse(Integer totalCount, Integer pageIndex, Integer pageSize, List<ProductRatingDetailResponse> productRatingDetailList){
        if (pageSize != null && !pageSize.equals(0)) {
            Integer calTotalPage = totalCount/pageSize;
            if(totalCount % pageSize > 0) {
                calTotalPage++;
            }
            this.totalPage = calTotalPage;
        }
        this.totalCount = totalCount;

        Integer pageCount = pageSize >= totalCount ? 1 : (totalCount%pageSize > 0 ? totalCount/pageSize +1 : totalCount/pageSize);
        Integer pageNo = pageIndex*pageSize >= totalCount ? pageCount-1 : pageIndex;

        this.pageNo = pageNo+1;
        this.pageSize = pageSize;
        this.productRatingDetailList = productRatingDetailList;
    }

    public List<ProductRatingDetailResponse> getProductRatingDetailList() {
        return productRatingDetailList;
    }

    public void setProductRatingDetailList(List<ProductRatingDetailResponse> productRatingDetailList) { this.productRatingDetailList = productRatingDetailList; }

    public Integer getTotalCount() { return totalCount; }

    public void setTotalCount(Integer totalCount) { this.totalCount = totalCount; }

    public Integer getPageNo() { return pageNo; }

    public void setPageNo(Integer pageNo) { this.pageNo = pageNo; }

    public Integer getPageSize() { return pageSize; }

    public void setPageSize(Integer pageSize) { this.pageSize = pageSize; }

    public Integer getTotalPage() { return totalPage; }

    public void setTotalPage(Integer totalPage) { this.totalPage = totalPage; }
}

package com.cmg.idsmed.dto.product;

import com.cmg.idsmed.dto.AbstractRequest;
import com.cmg.idsmed.dto.AbstractResponse;
import com.cmg.idsmed.model.entity.auth.IdsmedUser;
import com.cmg.idsmed.model.entity.product.ProductRating;
import com.cmg.idsmed.model.entity.product.ProductRatingDetail;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.Date;

public class ProductRatingDetailResponse extends AbstractResponse {
    private Double score;
    private String comment;
    private String userName;
    private LocalDateTime createdDate;

    public ProductRatingDetailResponse(ProductRatingDetail productRatingDetail, IdsmedUser idsmedUser){
        this.score = productRatingDetail.getScore();
        this.comment = productRatingDetail.getComment();
        this.userName = idsmedUser.getFirstName() + " " + idsmedUser.getLastName();
        this.createdDate = productRatingDetail.getCreatedDate();
    }

    public Double getScore() { return score; }

    public void setScore(Double score) { this.score = score; }

    public String getComment() { return comment; }

    public void setComment(String comment) { this.comment = comment; }

    public LocalDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public String getUserName() { return userName; }

    public void setUserName(String userName) { this.userName = userName; }
}

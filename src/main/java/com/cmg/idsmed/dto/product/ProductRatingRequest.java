package com.cmg.idsmed.dto.product;

import com.cmg.idsmed.dto.AbstractRequest;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class ProductRatingRequest extends AbstractRequest {
    private Long productId;

    private Double score;

    @Size (max = 3000)
    private String comment;

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        this.score = score;
    }
}

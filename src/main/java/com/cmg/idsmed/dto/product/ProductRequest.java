package com.cmg.idsmed.dto.product;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.cmg.idsmed.dto.AbstractRequest;

/**
 * @author congdang
 */
public class ProductRequest extends AbstractRequest {

	private Long id;

	@NotNull
	@Size(max = 1000)
	private String productNamePrimary;

	@Size(max = 1000)
	private String productNameSecondary;

	@Size(max = 100)
	private String productCode;

	@Size(max = 100)
	private String productModel;

	@Size(max = 1000)
	private String productDescription;

	@Size(max = 500)
	private String registrationNumber;

	@Size(max = 5000)
	private String hashTag;

	@Size(max = 100)
	private String unit;

	@Size(max = 200)
	private String packaging;

	private Integer minOfOrder;

	@Size(max = 1000)
	private String productBrochurePath;

	@Size(max = 1000)
	private String productVideoPath;

	@Size(max = 3000)
	private String remarks;

	private BigDecimal price;
	
	private Long productBrandId;

	@Size(max = 1000)
	private String productBrochureUrl;

	@Size(max = 1000)
	private String productVideoUrl;

	private Long productCategoryId;

	private List<Long> productSecondCategoryIds;

	private List<Long> productCareAreaList;

	private Long vendorId;
	
	private Integer status;
	
	private Long approvedBy;
	
	private Date approvedDate;
	
	private LocalDateTime expDate;
	
	@Size(max = 300)
	private String rejectReason;
	
	@Size(max = 100)
	private String brandName;

	@Size(max = 1000)
	private String otherDescription;

	private Boolean isGovtSubsidized;

	private Boolean canProcure;

	private Long deviceCategory;

	@Size(max = 50)
	private String deliveryLeadTime;

	private String skuNumber;

	private String productFunctionlityAndClaims;

	private String disclaimer;

	private String instructionForUse;

	private String productWebsite;

	private Integer productType;

	@Size(max = 50)
	private String manufacturerCountryCode;

	@Size(max = 50)
	private String manufacturerProvinceCode;

	private Integer equipmentType;

	@Size(max = 1000)
	private String packOfSize;

	@Size(max = 1000)
	private String manufacturerLocation;

	private String productHierarchyName;

	private String productHierarchyCode;
	
    private List<ProductFeatureRequest> productFeatureParameters;
    
	private List<ProductTechnicalRequest> productTechnicalParameters;

	//This variable mapping between title of a product document and uploaded file name
	private List<ProductDocumentAttachmentRequest> productDocumentAttachmentRequests;

	public Long getId() { return id; }

	public void setId(Long id) { this.id = id; }

	public String getProductNamePrimary() { return productNamePrimary; }

	public void setProductNamePrimary(String productNamePrimary) { this.productNamePrimary = productNamePrimary; }

	public String getProductNameSecondary() { return productNameSecondary; }

	public void setProductNameSecondary(String productNameSecondary) { this.productNameSecondary = productNameSecondary; }

	public String getProductCode() { return productCode; }

	public void setProductCode(String productCode) { this.productCode = productCode; }

	public String getProductModel() { return productModel; }

	public void setProductModel(String productModel) { this.productModel = productModel; }

	public String getProductDescription() { return productDescription; }

	public void setProductDescription(String productDescription) { this.productDescription = productDescription; }

	public String getRegistrationNumber() { return registrationNumber; }

	public void setRegistrationNumber(String registrationNumber) { this.registrationNumber = registrationNumber; }

	public String getHashTag() { return hashTag; }

	public void setHashTag(String hashTag) { this.hashTag = hashTag; }

	public String getUnit() { return unit; }

	public void setUnit(String unit) { this.unit = unit; }

	public String getPackaging() { return packaging; }

	public void setPackaging(String packaging) { this.packaging = packaging; }

	public int getMinOfOrder() { return minOfOrder; }

	public void setMinOfOrder(int minOfOrder) { this.minOfOrder = minOfOrder; }

	public String getProductBrochurePath() { return productBrochurePath; }

	public void setProductBrochurePath(String productBrochurePath) { this.productBrochurePath = productBrochurePath; }

	public String getProductVideoPath() { return productVideoPath; }

	public void setProductVideoPath(String productVideoPath) { this.productVideoPath = productVideoPath; }

	public BigDecimal getPrice() { return price; }

	public void setPrice(BigDecimal price) { this.price = price; }

	public Long getProductBrandId() { return productBrandId; }

	public void setProductBrandId(Long productBrandId) { this.productBrandId = productBrandId; }

	public String getProductBrochureUrl() { return productBrochureUrl; }

	public void setProductBrochureUrl(String productBrochureUrl) { this.productBrochureUrl = productBrochureUrl; }

	public String getProductVideoUrl() { return productVideoUrl; }

	public void setProductVideoUrl(String productVideoUrl) { this.productVideoUrl = productVideoUrl; }

	public Long getProductCategoryId() { return productCategoryId; }

	public void setProductCategoryId(Long productCategoryId) { this.productCategoryId = productCategoryId; }

	public List<Long> getProductCareAreaList() { return productCareAreaList; }

	public void setProductCareAreaList(List<Long> productCareAreaList) { this.productCareAreaList = productCareAreaList; }

	public Long getVendorId() { return vendorId; }

	public void setVendorId(Long vendorId) { this.vendorId = vendorId; }
	
	public List<ProductFeatureRequest> getProductFeatureParameters() { return productFeatureParameters; }

	public String getRemarks() { return remarks; }

	public void setRemarks(String remarks) { this.remarks = remarks; }

	public Integer getStatus() { return status; }

	public void setStatus(Integer status) { this.status = status; }

	public void setMinOfOrder(Integer minOfOrder) { this.minOfOrder = minOfOrder; }

	public String getRejectReason() { return rejectReason; }

	public void setRejectReason(String rejectReason) { this.rejectReason = rejectReason; }

	public Long getApprovedBy() { return approvedBy; }

	public void setApprovedBy(Long approvedBy) { this.approvedBy = approvedBy; }

	public Date getApprovedDate() { return approvedDate; }

	public void setApprovedDate(Date approvedDate) { this.approvedDate = approvedDate; }

	public String getBrandName() { return brandName; }

	public void setBrandName(String brandName) { this.brandName = brandName; }

	public void setProductFeatureParameters(List<ProductFeatureRequest> productFeatureParameters) { this.productFeatureParameters = productFeatureParameters; }

	public List<ProductTechnicalRequest> getProductTechnicalParameters() { return productTechnicalParameters; }

	public void setProductTechnicalParameters(List<ProductTechnicalRequest> productTechnicalParameters) { this.productTechnicalParameters = productTechnicalParameters; }

	public LocalDateTime getExpDate() {
		return expDate;
	}

	public void setExpDate(LocalDateTime expDate) {
		this.expDate = expDate;
	}

	public String getOtherDescription() { return otherDescription; }

	public void setOtherDescription(String otherDescription) { this.otherDescription = otherDescription; }

	public Boolean getIsGovtSubsidized() { return isGovtSubsidized; }

	public void setIsGovtSubsidized(Boolean isGovtSubsidized) { this.isGovtSubsidized = isGovtSubsidized; }

	public List<ProductDocumentAttachmentRequest> getProductDocumentAttachmentRequests() { return productDocumentAttachmentRequests; }

	public void setProductDocumentAttachmentRequests(List<ProductDocumentAttachmentRequest> productDocumentAttachmentRequests) { this.productDocumentAttachmentRequests = productDocumentAttachmentRequests; }

	public Boolean getCanProcure() { return canProcure; }

	public void setCanProcure(Boolean canProcure) { this.canProcure = canProcure; }

	public Long getDeviceCategory() { return deviceCategory; }

	public void setDeviceCategory(Long deviceCategory) { this.deviceCategory = deviceCategory; }

	public String getDeliveryLeadTime() { return deliveryLeadTime; }

	public void setDeliveryLeadTime(String deliveryLeadTime) { this.deliveryLeadTime = deliveryLeadTime; }

	public String getSkuNumber() { return skuNumber; }

	public void setSkuNumber(String skuNumber) { this.skuNumber = skuNumber; }

	public String getProductFunctionlityAndClaims() { return productFunctionlityAndClaims; }

	public void setProductFunctionlityAndClaims(String productFunctionlityAndClaims) { this.productFunctionlityAndClaims = productFunctionlityAndClaims; }

	public String getDisclaimer() { return disclaimer; }

	public void setDisclaimer(String disclaimer) { this.disclaimer = disclaimer; }

	public String getInstructionForUse() { return instructionForUse; }

	public void setInstructionForUse(String instructionForUse) { this.instructionForUse = instructionForUse; }

	public String getProductWebsite() { return productWebsite; }

	public void setProductWebsite(String productWebsite) { this.productWebsite = productWebsite; }

	public String getManufacturerCountryCode() { return manufacturerCountryCode; }

	public void setManufacturerCountryCode(String manufacturerCountryCode) { this.manufacturerCountryCode = manufacturerCountryCode; }

	public String getManufacturerProvinceCode() { return manufacturerProvinceCode; }

	public void setManufacturerProvinceCode(String manufacturerProvinceCode) { this.manufacturerProvinceCode = manufacturerProvinceCode; }

	public List<Long> getProductSecondCategoryIds() {
		return productSecondCategoryIds;
	}

	public void setProductSecondCategoryIds(List<Long> productSecondCategoryIds) { this.productSecondCategoryIds = productSecondCategoryIds; }

	public Integer getProductType() {
		return productType;
	}

	public void setProductType(Integer productType) {
		this.productType = productType;
	}

	public Integer getEquipmentType() { return equipmentType; }

	public void setEquipmentType(Integer equipmentType) { this.equipmentType = equipmentType; }

	public String getPackOfSize() { return packOfSize; }

	public void setPackOfSize(String packOfSize) { this.packOfSize = packOfSize; }

	public String getManufacturerLocation() { return manufacturerLocation; }

	public void setManufacturerLocation(String manufacturerLocation) { this.manufacturerLocation = manufacturerLocation; }

	public String getProductHierarchyName() { return productHierarchyName; }

	public void setProductHierarchyName(String productHierarchyName) { this.productHierarchyName = productHierarchyName; }

	public String getProductHierarchyCode() { return productHierarchyCode; }

	public void setProductHierarchyCode(String productHierarchyCode) { this.productHierarchyCode = productHierarchyCode;}

}

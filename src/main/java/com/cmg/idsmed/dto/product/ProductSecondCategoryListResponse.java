package com.cmg.idsmed.dto.product;

import java.util.List;

public class ProductSecondCategoryListResponse {
	private Integer totalPage = null;
	private List<ProductSecondCategoryResponse> productSecondCategoryList;
	private Integer totalCount = null;
	private Integer pageNo = null;
	private Integer pageSize = null;

	public ProductSecondCategoryListResponse(Integer totalCount, Integer pageIndex, Integer pageSize, List<ProductSecondCategoryResponse> productSecondCategoryList) {
		if (pageSize != null && !pageSize.equals(0)) {
			Integer calTotalPage = totalCount/pageSize;
			if(totalCount % pageSize > 0) {
				calTotalPage++;
			}
			this.totalPage = calTotalPage;
		}
		this.totalCount = totalCount;
		this.pageNo = pageIndex + 1;
		this.pageSize = pageSize;
		this.productSecondCategoryList = productSecondCategoryList;
	}

	public ProductSecondCategoryListResponse(Integer totalCount, List<ProductSecondCategoryResponse> productSecondCategoryList) {
		this.totalCount = totalCount;
		this.productSecondCategoryList = productSecondCategoryList;
		this.totalPage = 1;
		this.pageSize = totalCount;
		this.pageNo = 1;
	}

	public Integer getTotalPage() {
		return totalPage;
	}

	public void setTotalPage(Integer totalPage) {
		this.totalPage = totalPage;
	}

	public List<ProductSecondCategoryResponse> getProductSecondCategoryList() {
		return productSecondCategoryList;
	}

	public void setProductSecondCategoryList(List<ProductSecondCategoryResponse> productSecondCategoryList) {
		this.productSecondCategoryList = productSecondCategoryList;
	}

	public Integer getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}

	public Integer getPageNo() {
		return pageNo;
	}

	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}
}

package com.cmg.idsmed.dto.product;

import com.cmg.idsmed.common.enums.LangEnum;
import com.cmg.idsmed.dto.AbstractResponse;
import com.cmg.idsmed.model.entity.product.ProductSecondCategory;
import com.cmg.idsmed.model.entity.product.ProductSecondCategoryDetail;

public class ProductSecondCategoryResponse extends AbstractResponse {

    private Long id;

    private String categoryName;

    private Long totalProducts = 0L;

    public ProductSecondCategoryResponse(ProductSecondCategoryDetail productSecondCategoryDetail) {
        this.id = productSecondCategoryDetail.getProductSecondCategory().getId();
        this.categoryName = productSecondCategoryDetail.getProductSecondCategory().getCategoryName();
    }

    public ProductSecondCategoryResponse(ProductSecondCategory productSecondCategory) {
        this.id = productSecondCategory.getId();
        this.categoryName = productSecondCategory.getCategoryName();
    }

    public ProductSecondCategoryResponse(ProductSecondCategory productSecondCategory, String langCode) {
        if (langCode == null || langCode.equalsIgnoreCase(LangEnum.ENGLISH.getCode())) {
            this.id = productSecondCategory.getId();
            this.categoryName = productSecondCategory.getCategoryName();
        }

        if (langCode.equalsIgnoreCase(LangEnum.CHINA.getCode())) {
            this.id = productSecondCategory.getId();
            this.categoryName = productSecondCategory.getCategoryNameZhCn();
        }

        if (langCode.equalsIgnoreCase(LangEnum.CHINA_TAIWAN.getCode())) {
            this.id = productSecondCategory.getId();
            this.categoryName = productSecondCategory.getCategoryNameZhTw();
        }

        if (langCode.equalsIgnoreCase(LangEnum.THAI.getCode())) {
            this.id = productSecondCategory.getId();
            this.categoryName = productSecondCategory.getCategoryNameTh();
        }

        if (langCode.equalsIgnoreCase(LangEnum.VIETNAMESE.getCode())) {
            this.id = productSecondCategory.getId();
            this.categoryName = productSecondCategory.getCategoryNameVi();
        }

        if (langCode.equalsIgnoreCase(LangEnum.INDONESIAN.getCode())) {
            this.id = productSecondCategory.getId();
            this.categoryName = productSecondCategory.getCategoryNameId();
        }

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public Long getTotalProducts() {
        return totalProducts;
    }

    public void setTotalProducts(Long totalProducts) {
        this.totalProducts = totalProducts;
    }

}

package com.cmg.idsmed.dto.product;

import com.cmg.idsmed.common.enums.LangEnum;
import com.cmg.idsmed.model.entity.product.ProductTechnicalFeatureLabel;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ProductTechFeatureResponse {
    private List<String> productTechnicalLabels = new ArrayList<String>();
    private List<String> productFeatureLabels = new ArrayList<String>();

    public ProductTechFeatureResponse() {
    }

    public ProductTechFeatureResponse(List<String> productFeatureLabels, List<String> productTechnicalLabels) {
        this.productFeatureLabels = productFeatureLabels;
        this.productTechnicalLabels = productTechnicalLabels;
    }

    public ProductTechFeatureResponse(List<ProductTechnicalFeatureLabel> productFeatureLabels, List<ProductTechnicalFeatureLabel> productTechnicalLabels, String langCode) {
        List<String> featureLabels = productFeatureLabels.stream().map(lb -> {
            LangEnum langEnum = LangEnum.CHINA;
            if (langCode != null) {
                for (LangEnum lang : LangEnum.values()) {
                    if (langCode.equals(lang.getCode())) {
                        langEnum = lang;
                        break;
                    }
                }
            }

            switch (langEnum) {
                case CHINA: return lb.getLabelNameZhCn();
                case ENGLISH: return lb.getLabelName();
                case CHINA_TAIWAN: return lb.getLabelNameZhTw();
                case VIETNAMESE: return lb.getLabelNameVi();
                case THAI: return lb.getLabelNameTh();
                case INDONESIAN: return lb.getLabelNameId();
            }

            return lb.getLabelNameZhCn();
        }).collect(Collectors.toList());

        List<String> technicalLabels = productTechnicalLabels.stream().map(lb -> {
            LangEnum langEnum = LangEnum.CHINA;
            if (langCode != null) {
                for (LangEnum lang : LangEnum.values()) {
                    if (langCode.equals(lang.getCode())) {
                        langEnum = lang;
                        break;
                    }
                }
            }

            switch (langEnum) {
                case CHINA: return lb.getLabelNameZhCn();
                case ENGLISH: return lb.getLabelName();
                case CHINA_TAIWAN: return lb.getLabelNameZhTw();
                case VIETNAMESE: return lb.getLabelNameVi();
                case THAI: return lb.getLabelNameTh();
                case INDONESIAN: return lb.getLabelNameId();
            }

            return lb.getLabelNameZhCn();
        }).collect(Collectors.toList());

        this.productFeatureLabels = featureLabels;
        this.productTechnicalLabels = technicalLabels;
    }

    public List<String> getProductTechnicalLabels() {
        return productTechnicalLabels;
    }

    public void setProductTechnicalLabels(List<String> productTechnicalLabels) {
        this.productTechnicalLabels = productTechnicalLabels;
    }

    public List<String> getProductFeatureLabels() {
        return productFeatureLabels;
    }

    public void setProductFeatureLabels(List<String> productFeatureLabels) {
        this.productFeatureLabels = productFeatureLabels;
    }
}

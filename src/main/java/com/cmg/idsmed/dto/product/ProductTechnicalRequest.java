package com.cmg.idsmed.dto.product;

import java.io.IOException;

import javax.validation.constraints.Size;

import com.cmg.idsmed.dto.AbstractRequest;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ProductTechnicalRequest extends AbstractRequest {
	
	public ProductTechnicalRequest() {
		super();
	}
	
	public ProductTechnicalRequest(Integer technicalSequence, String technicalLabel, String technicalValue, Boolean sellingPoint) {
		this.technicalSequence = technicalSequence;
		this.technicalLabel = technicalLabel;
		this.technicalValue = technicalValue;
		this.sellingPoint = sellingPoint;
	}
	
	public ProductTechnicalRequest(String jsonString) {
		ObjectMapper mapper = new ObjectMapper();

		//JSON from String to Object
		try {
			ProductTechnicalRequest ptd = mapper.readValue(jsonString, ProductTechnicalRequest.class);
			this.technicalSequence = ptd.getTechnicalSequence();
			this.technicalLabel = ptd.getTechnicalLabel();
			this.technicalValue = ptd.getTechnicalValue();
			this.sellingPoint = ptd.getSellingPoint();
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private Integer technicalSequence;
	
	@Size(max=100)
	private String technicalLabel;
	
	@Size(max=3000)
	private String technicalValue;

	private Boolean sellingPoint;

	public Integer getTechnicalSequence() {
		return technicalSequence;
	}

	public void setTechnicalSequence(Integer technicalSequence) {
		this.technicalSequence = technicalSequence;
	}

	public String getTechnicalLabel() {
		return technicalLabel;
	}

	public void setTechnicalLabel(String technicalLabel) {
		this.technicalLabel = technicalLabel;
	}

	public String getTechnicalValue() {
		return technicalValue;
	}

	public void setTechnicalValue(String technicalValue) {
		this.technicalValue = technicalValue;
	}

	public Boolean getSellingPoint() { return sellingPoint; }

	public void setSellingPoint(Boolean sellingPoint) { this.sellingPoint = sellingPoint; }
}

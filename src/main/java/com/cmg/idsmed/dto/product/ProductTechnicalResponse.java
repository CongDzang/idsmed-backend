package com.cmg.idsmed.dto.product;

import com.cmg.idsmed.dto.AbstractResponse;
import com.cmg.idsmed.model.entity.product.ProductTechnical;
import org.springframework.beans.BeanUtils;

public class ProductTechnicalResponse extends AbstractResponse {

	private Long id;

	private String productLabel;

	private String productValue;

	private Integer productLabelSequence;

	private Boolean sellingPoint;

	public ProductTechnicalResponse(ProductTechnical productTechnical) {
		BeanUtils.copyProperties(productTechnical, this);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getProductLabel() {
		return productLabel;
	}

	public void setProductLabel(String productLabel) {
		this.productLabel = productLabel;
	}

	public String getProductValue() {
		return productValue;
	}

	public void setProductValue(String productValue) {
		this.productValue = productValue;
	}

	public Integer getProductLabelSequence() {
		return productLabelSequence;
	}

	public void setProductLabelSequence(Integer productLabelSequence) {
		this.productLabelSequence = productLabelSequence;
	}

	public Boolean getSellingPoint() { return sellingPoint; }

	public void setSellingPoint(Boolean sellingPoint) { this.sellingPoint = sellingPoint; }
}

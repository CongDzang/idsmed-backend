package com.cmg.idsmed.dto.product;

import com.cmg.idsmed.dto.AbstractResponse;
import com.cmg.idsmed.model.entity.product.Product;
import com.cmg.idsmed.model.entity.product.ProductMedia;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ProductTopRatingAndLatestApprovedResponse extends AbstractResponse {
    private List<ProductResponse> topFiveProductRating = new ArrayList<>();
    private List<ProductResponse> latestFiveApprovedProduct = new ArrayList<>();

    public ProductTopRatingAndLatestApprovedResponse() {
    }

    public List<ProductResponse> getTopFiveProductRating() {
        return topFiveProductRating;
    }

    public void setTopFiveProductRating(List<ProductResponse> topFiveProductRating) {
        this.topFiveProductRating = topFiveProductRating;
    }

    public List<ProductResponse> getLatestFiveApprovedProduct() {
        return latestFiveApprovedProduct;
    }

    public void setLatestFiveApprovedProduct(List<ProductResponse> latestFiveApprovedProduct) {
        this.latestFiveApprovedProduct = latestFiveApprovedProduct;
    }
}
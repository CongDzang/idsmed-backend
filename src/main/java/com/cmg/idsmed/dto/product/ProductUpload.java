package com.cmg.idsmed.dto.product;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@SuppressWarnings("serial")
public class ProductUpload implements Serializable {

	private String careArea;
	
	private String category;
	
	private String brand;
	
	@NotNull
	private String productNamePrimary;

	private String productNameSecondary;

	@Size(max = 100)
	private String productCode;

	@Size(max = 100)
	private String productModel;
	
	private String registrationNumber;

	@Size(max = 1000)
	private String productDescription;

	private String hashTag;

	@Size(max = 100)
	private String unit;
	
	@Size(max = 100)
	private Double price;

	@Size(max = 200)
	private String packaging;

	private Integer minOfOrder;

	private String vendorName;
	
	@Size(max = 1000)
	private String productBrochureURL;

	@Size(max = 1000)
	private String productVideoURL;
	
	@Size(max = 1000)
	private String certificateURL;

	private String image1URL;
	
	private String image2URL;
	
	private String image3URL;
	
	private String image4URL;
	
	private String image5URL;
	
	private String image6URL;
	
	private String technicalLabel1;
	
	private String technicalValue1;

	private String technicalLabel2;
	
	private String technicalValue2;
	
	private String technicalLabel3;
	
	private String technicalValue3;
	
	private String technicalLabel4;
	
	private String technicalValue4;
	
	private String technicalLabel5;
	
	private String technicalValue5;
	
	private String technicalLabel6;
	
	private String technicalValue6;
	
	private String featureLabel1;
	
	private String featureValue1;

	private String featureLabel2;
	
	private String featureValue2;
	
	private String featureLabel3;
	
	private String featureValue3;
	
	private String featureLabel4;
	
	private String featureValue4;
	
	private String featureLabel5;

	private String featureValue5;
	
	private String featureLabel6;
	
	private String featureValue6;
	
	private String order;	
	
	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}

	public String getCareArea() {
		return careArea;
	}

	public void setCareArea(String careArea) {
		this.careArea = careArea;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getProductNamePrimary() {
		return productNamePrimary;
	}

	public void setProductNamePrimary(String productNamePrimary) {
		this.productNamePrimary = productNamePrimary;
	}

	public String getProductNameSecondary() {
		return productNameSecondary;
	}

	public void setProductNameSecondary(String productNameSecondary) {
		this.productNameSecondary = productNameSecondary;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductModel() {
		return productModel;
	}

	public void setProductModel(String productModel) {
		this.productModel = productModel;
	}

	public String getRegistrationNumber() {
		return registrationNumber;
	}

	public void setRegistrationNumber(String registrationNumber) {
		this.registrationNumber = registrationNumber;
	}

	public String getProductDescription() {
		return productDescription;
	}

	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}

	public String getHashTag() {
		return hashTag;
	}

	public void setHashTag(String hashTag) {
		this.hashTag = hashTag;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public String getPackaging() {
		return packaging;
	}

	public void setPackaging(String packaging) {
		this.packaging = packaging;
	}

	public Integer getMinOfOrder() {
		return minOfOrder;
	}

	public void setMinOfOrder(Integer minOfOrder) {
		this.minOfOrder = minOfOrder;
	}

	public String getVendorName() {
		return vendorName;
	}

	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

	public String getProductBrochureURL() {
		return productBrochureURL;
	}

	public void setProductBrochureURL(String productBrochureURL) {
		this.productBrochureURL = productBrochureURL;
	}

	public String getProductVideoURL() {
		return productVideoURL;
	}

	public void setProductVideoURL(String productVideoURL) {
		this.productVideoURL = productVideoURL;
	}

	public String getCertificateURL() {
		return certificateURL;
	}

	public void setCertificateURL(String certificateURL) {
		this.certificateURL = certificateURL;
	}

	public String getImage1URL() {
		return image1URL;
	}

	public void setImage1URL(String image1url) {
		image1URL = image1url;
	}

	public String getImage2URL() {
		return image2URL;
	}

	public void setImage2URL(String image2url) {
		image2URL = image2url;
	}

	public String getImage3URL() {
		return image3URL;
	}

	public void setImage3URL(String image3url) {
		image3URL = image3url;
	}

	public String getImage4URL() {
		return image4URL;
	}

	public void setImage4URL(String image4url) {
		image4URL = image4url;
	}

	public String getImage5URL() {
		return image5URL;
	}

	public void setImage5URL(String image5url) {
		image5URL = image5url;
	}

	public String getImage6URL() {
		return image6URL;
	}

	public void setImage6URL(String image6url) {
		image6URL = image6url;
	}

	public String getFeatureLabel1() {
		return featureLabel1;
	}

	public void setFeatureLabel1(String featureLabel1) {
		this.featureLabel1 = featureLabel1;
	}

	public String getFeatureValue1() {
		return featureValue1;
	}

	public void setFeatureValue1(String featureValue1) {
		this.featureValue1 = featureValue1;
	}

	public String getFeatureLabel2() {
		return featureLabel2;
	}

	public void setFeatureLabel2(String featureLabel2) {
		this.featureLabel2 = featureLabel2;
	}

	public String getFeatureValue2() {
		return featureValue2;
	}

	public void setFeatureValue2(String featureValue2) {
		this.featureValue2 = featureValue2;
	}

	public String getFeatureLabel3() {
		return featureLabel3;
	}

	public void setFeatureLabel3(String featureLabel3) {
		this.featureLabel3 = featureLabel3;
	}

	public String getFeatureValue3() {
		return featureValue3;
	}

	public void setFeatureValue3(String featureValue3) {
		this.featureValue3 = featureValue3;
	}

	public String getFeatureLabel4() {
		return featureLabel4;
	}

	public void setFeatureLabel4(String featureLabel4) {
		this.featureLabel4 = featureLabel4;
	}

	public String getFeatureValue4() {
		return featureValue4;
	}

	public void setFeatureValue4(String featureValue4) {
		this.featureValue4 = featureValue4;
	}

	public String getFeatureLabel5() {
		return featureLabel5;
	}

	public void setFeatureLabel5(String featureLabel5) {
		this.featureLabel5 = featureLabel5;
	}

	public String getFeatureValue5() {
		return featureValue5;
	}

	public void setFeatureValue5(String featureValue5) {
		this.featureValue5 = featureValue5;
	}

	public String getFeatureLabel6() {
		return featureLabel6;
	}

	public void setFeatureLabel6(String featureLabel6) {
		this.featureLabel6 = featureLabel6;
	}

	public String getFeatureValue6() {
		return featureValue6;
	}

	public void setFeatureValue6(String featureValue6) {
		this.featureValue6 = featureValue6;
	}

	public String getTechnicalLabel1() {
		return technicalLabel1;
	}

	public void setTechnicalLabel1(String technicalLabel1) {
		this.technicalLabel1 = technicalLabel1;
	}

	public String getTechnicalValue1() {
		return technicalValue1;
	}

	public void setTechnicalValue1(String technicalValue1) {
		this.technicalValue1 = technicalValue1;
	}

	public String getTechnicalLabel2() {
		return technicalLabel2;
	}

	public void setTechnicalLabel2(String technicalLabel2) {
		this.technicalLabel2 = technicalLabel2;
	}

	public String getTechnicalValue2() {
		return technicalValue2;
	}

	public void setTechnicalValue2(String technicalValue2) {
		this.technicalValue2 = technicalValue2;
	}

	public String getTechnicalLabel3() {
		return technicalLabel3;
	}

	public void setTechnicalLabel3(String technicalLabel3) {
		this.technicalLabel3 = technicalLabel3;
	}

	public String getTechnicalValue3() {
		return technicalValue3;
	}

	public void setTechnicalValue3(String technicalValue3) {
		this.technicalValue3 = technicalValue3;
	}

	public String getTechnicalLabel4() {
		return technicalLabel4;
	}

	public void setTechnicalLabel4(String technicalLabel4) {
		this.technicalLabel4 = technicalLabel4;
	}

	public String getTechnicalValue4() {
		return technicalValue4;
	}

	public void setTechnicalValue4(String technicalValue4) {
		this.technicalValue4 = technicalValue4;
	}

	public String getTechnicalLabel5() {
		return technicalLabel5;
	}

	public void setTechnicalLabel5(String technicalLabel5) {
		this.technicalLabel5 = technicalLabel5;
	}

	public String getTechnicalValue5() {
		return technicalValue5;
	}

	public void setTechnicalValue5(String technicalValue5) {
		this.technicalValue5 = technicalValue5;
	}

	public String getTechnicalLabel6() {
		return technicalLabel6;
	}

	public void setTechnicalLabel6(String technicalLabel6) {
		this.technicalLabel6 = technicalLabel6;
	}

	public String getTechnicalValue6() {
		return technicalValue6;
	}

	public void setTechnicalValue6(String technicalValue6) {
		this.technicalValue6 = technicalValue6;
	}
	
}

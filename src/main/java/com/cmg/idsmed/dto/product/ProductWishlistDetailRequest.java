package com.cmg.idsmed.dto.product;

import com.cmg.idsmed.dto.AbstractRequest;

public class ProductWishlistDetailRequest extends AbstractRequest {

    private Long id;

    private Long productId;

    private Long wedoctorCustomerId;

    public Long getId() { return id; }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Long getWedoctorCustomerId() {
        return wedoctorCustomerId;
    }

    public void setWedoctorCustomerId(Long wedoctorCustomerId) {
        this.wedoctorCustomerId = wedoctorCustomerId;
    }
}

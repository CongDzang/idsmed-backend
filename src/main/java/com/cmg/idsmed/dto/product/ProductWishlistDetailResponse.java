package com.cmg.idsmed.dto.product;

import com.cmg.idsmed.dto.AbstractResponse;
import com.cmg.idsmed.model.entity.product.ProductWishlistDetail;

public class ProductWishlistDetailResponse extends AbstractResponse {

    private Long id;

    private Long productId;

    private Long idsmedAccountId;

    private Long wedoctorCustomerId;

    public ProductWishlistDetailResponse(ProductWishlistDetail productWishlistDetail) {
        if (productWishlistDetail != null) {
            this.id = productWishlistDetail.getId();
            this.productId = productWishlistDetail.getProduct().getId();
            this.idsmedAccountId = productWishlistDetail.getIdsmedAccount().getId();
            this.wedoctorCustomerId = productWishlistDetail.getWedoctorCustomerId();
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Long getIdsmedAccountId() {
        return idsmedAccountId;
    }

    public void setIdsmedAccountId(Long idsmedAccountId) {
        this.idsmedAccountId = idsmedAccountId;
    }
}

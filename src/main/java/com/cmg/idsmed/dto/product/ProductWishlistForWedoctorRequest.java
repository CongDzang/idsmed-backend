package com.cmg.idsmed.dto.product;

import com.cmg.idsmed.dto.AbstractRequest;

public class ProductWishlistForWedoctorRequest {

    private Long id;

    private Long productId;

    private Long wedoctorCustomerId;

    private Boolean isLike;

    private String langCode;

    public Long getId() { return id; }

    public void setId(Long id) { this.id = id; }

    public Long getProductId() { return productId; }

    public void setProductId(Long productId) { this.productId = productId; }

    public Boolean getIsLike() {
        return isLike;
    }

    public void setIsLike(Boolean isLike) {
        this.isLike = isLike;
    }

    public Long getWedoctorCustomerId() {
        return wedoctorCustomerId;
    }

    public void setWedoctorCustomerId(Long wedoctorCustomerId) {
        this.wedoctorCustomerId = wedoctorCustomerId;
    }

    public String getLangCode() {
        return langCode;
    }

    public void setLangCode(String langCode) {
        this.langCode = langCode;
    }
}

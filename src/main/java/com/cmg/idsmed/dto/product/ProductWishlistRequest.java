package com.cmg.idsmed.dto.product;

import com.cmg.idsmed.dto.AbstractRequest;
import com.cmg.idsmed.model.entity.product.ProductWishlistDetail;

import java.util.List;

public class ProductWishlistRequest extends AbstractRequest {

    private Long id;

    private Long productId;

    private Boolean isLike;

    public Long getId() { return id; }

    public void setId(Long id) { this.id = id; }

    public Long getProductId() { return productId; }

    public void setProductId(Long productId) { this.productId = productId; }

    public Boolean getIsLike() {
        return isLike;
    }

    public void setIsLike(Boolean isLike) {
        this.isLike = isLike;
    }
}

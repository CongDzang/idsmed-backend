package com.cmg.idsmed.dto.product;

import com.cmg.idsmed.dto.AbstractResponse;
import com.cmg.idsmed.model.entity.product.Product;
import com.cmg.idsmed.model.entity.product.ProductWishlist;
import com.cmg.idsmed.model.entity.product.ProductWishlistDetail;

public class ProductWishlistResponse extends AbstractResponse {
    private Long id;

    private Long productId;

    private Long totalCountInWishlist;

    private Boolean isWishList = false;

    public ProductWishlistResponse() {}

    public ProductWishlistResponse(Product product, Boolean isWishList) {
        ProductWishlist productWishlist = product.getProductWishlist();
        if (productWishlist != null) {
            this.id = productWishlist.getId();
            this.productId = productWishlist.getProduct().getId();
            this.totalCountInWishlist = productWishlist.getTotalWishlist();
            if (isWishList != null && isWishList == true) {
                this.isWishList = isWishList;
            }
        } else {
            this.productId = product.getId();
            this.totalCountInWishlist = 0L;
        }
    }

    public ProductWishlistResponse(ProductWishlist productWishlist) {
        if (productWishlist != null) {
            this.id = productWishlist.getId();
            this.totalCountInWishlist = productWishlist.getTotalWishlist();
            this.productId = productWishlist.getProduct().getId();
        }
    }

    public Long getId() { return id; }

    public void setId(Long id) { this.id = id; }

    public Long getProductId() { return productId; }

    public void setProductId(Long productId) { this.productId = productId; }

    public Long getTotalCountInWishlist() { return totalCountInWishlist; }

    public void setTotalCountInWishlist(Long totalCountInWishlist) { this.totalCountInWishlist = totalCountInWishlist; }

    public Boolean getIsWishList() { return isWishList; }

    public void setIsWishList(Boolean isWishList) { isWishList = isWishList; }
}

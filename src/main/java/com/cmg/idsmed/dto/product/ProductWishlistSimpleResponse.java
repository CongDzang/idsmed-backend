package com.cmg.idsmed.dto.product;

import com.cmg.idsmed.dto.AbstractResponse;
import com.cmg.idsmed.model.entity.auth.IdsmedAccount;
import com.cmg.idsmed.model.entity.product.Product;
import com.cmg.idsmed.model.entity.product.ProductWishlist;

public class ProductWishlistSimpleResponse extends AbstractResponse {
    private Long id;

    private Long productId;

    private Long totalCountInWishlist;

    private Boolean isWishList = false;

    public ProductWishlistSimpleResponse() {}

    public ProductWishlistSimpleResponse(ProductWishlist productWishlist, Boolean isWishList) {
        this.id = productWishlist.getId();
        this.productId = productWishlist.getProduct().getId();
        this.totalCountInWishlist = productWishlist.getTotalWishlist();
        this.isWishList = isWishList;
    }

    public ProductWishlistSimpleResponse(Product product, Boolean isWishList) {
        ProductWishlist productWishlist = product.getProductWishlist();
        if (productWishlist != null) {
            this.id = productWishlist.getId();
            this.productId = productWishlist.getProduct().getId();
            this.totalCountInWishlist = productWishlist.getTotalWishlist();
            this.isWishList = isWishList;
        } else {
            this.productId = product.getId();
            this.totalCountInWishlist = 0L;
        }

    }

    public Long getId() { return id; }

    public void setId(Long id) { this.id = id; }

    public Long getProductId() { return productId; }

    public void setProductId(Long productId) { this.productId = productId; }

    public Long getTotalCountInWishlist() { return totalCountInWishlist; }

    public void setTotalCountInWishlist(Long totalCountInWishlist) { this.totalCountInWishlist = totalCountInWishlist; }

    public Boolean getIsWishList() { return isWishList; }

    public void setWishList(Boolean isWishList) { this.isWishList = isWishList; }
}

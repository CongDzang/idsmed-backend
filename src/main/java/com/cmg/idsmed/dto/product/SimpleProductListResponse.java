package com.cmg.idsmed.dto.product;

import java.util.ArrayList;
import java.util.List;

public class SimpleProductListResponse {
	private Integer totalPage = null;
	private List<SimpleProductSearchResponse> productList = new ArrayList<>();
	private Integer totalCount = null;
	private Integer pageNo = null;
	private Integer pageSize = null;

	public SimpleProductListResponse(Integer pageNo, Integer pageSize) {
		this.totalPage = 0;
		this.totalCount = 0;
		this.pageSize = pageSize;
		this.pageNo = pageNo;
	}

	public SimpleProductListResponse(Integer totalCount, Integer pageIndex, Integer pageSize, List<SimpleProductSearchResponse> productList) {
		if (pageSize != null && !pageSize.equals(0)) {
			Integer calTotalPage = totalCount/pageSize;
			if(totalCount % pageSize > 0) {
				calTotalPage++;
			}
			this.totalPage = calTotalPage;
		}
		this.totalCount = totalCount;
		
		Integer pageCount = pageSize >= totalCount ? 1 : (totalCount%pageSize > 0 ? totalCount/pageSize +1 : totalCount/pageSize);
		Integer pageNo = pageIndex*pageSize >= totalCount ? pageCount-1 : pageIndex;
		
		this.pageNo = pageNo+1;
		this.pageSize = pageSize;
		this.productList = productList;
	}

	public SimpleProductListResponse(Integer totalCount, List<SimpleProductSearchResponse> productList) {
		this.totalCount = totalCount;
		this.productList = productList;
		this.totalPage = 1;
		this.pageSize = totalCount;
		this.pageNo = 1;
	}
	
	public Integer getTotalPage() {
		return totalPage;
	}

	public void setTotalPage(Integer totalPage) {
		this.totalPage = totalPage;
	}

	public List<SimpleProductSearchResponse> getProductList() {
		return productList;
	}

	public void setProductList(List<SimpleProductSearchResponse> productList) {
		this.productList = productList;
	}

	public Integer getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}

	public Integer getPageNo() {
		return pageNo;
	}

	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}
}

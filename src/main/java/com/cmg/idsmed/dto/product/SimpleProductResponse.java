package com.cmg.idsmed.dto.product;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.util.CollectionUtils;

import com.cmg.idsmed.dto.AbstractResponse;
import com.cmg.idsmed.model.entity.product.Product;
import com.cmg.idsmed.model.entity.product.ProductMedia.MEDIA_TYPE;

public class SimpleProductResponse extends AbstractResponse {

    private Long id;
    
    private Integer productVersion;
    
    private String vendorName;
    
    private List<String> careAreas;
    
    private String category;

    private String productCode;

    private String productModel;

    private String registrationNumber;
    
    private String brand;

    private String productNamePrimary;
    
    private String productNameSecondary;
    
    public String description;
    
    private String hashTag;

    private String units;

    private int minOfOrder;
    
    private String packaging;

    private BigDecimal price;

	private List<ProductMediaResponse> images;

	public SimpleProductResponse(Product product, String ossBucketDomain) {
		if (product != null) {
			this.id = product.getId();
			this.productVersion = product.getProductVersion();
			this.vendorName = product.getVendor().getCompanyNamePrimary();
			this.careAreas = CollectionUtils.isEmpty(product.getProductCareAreaList()) ? new ArrayList<String> () :
					product.getProductCareAreaList().stream().map(pca -> pca.getProductCareArea().getCareAreaName()).collect(Collectors.toList());
			this.category = product.getProductCategory().getCategoryName();
			this.productCode = product.getProductCode();
			this.productModel = product.getProductModel();
			this.registrationNumber = product.getRegistrationNumber();
			this.brand = product.getProductBrand().getBrandName();
			this.productNamePrimary = product.getProductNamePrimary();
			this.productNameSecondary = product.getProductNameSecondary();
			this.description = product.getProductDescription();
			this.hashTag = product.getHashTag();
			this.units = product.getUnit();
			this.minOfOrder = product.getMinOfOrder();
			this.packaging = product.getPackaging();
			this.price = product.getPrice();
			this.images = CollectionUtils.isEmpty(product.getProductMediaList()) ? new ArrayList<ProductMediaResponse> () :
					product.getProductMediaList().stream().filter(pm -> MEDIA_TYPE.IMAGE.getType().equalsIgnoreCase(pm.getDocumentType())).map(pm -> new ProductMediaResponse(pm, ossBucketDomain)).collect(Collectors.toList());
		}
	}

	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public Integer getProductVersion() {
		return productVersion;
	}


	public void setProductVersion(Integer productVersion) {
		this.productVersion = productVersion;
	}


	public String getVendorName() {
		return vendorName;
	}


	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}


	public List<String> getCareAreas() {
		return careAreas;
	}


	public void setCareAreas(List<String> careAreas) {
		this.careAreas = careAreas;
	}


	public String getCategory() {
		return category;
	}


	public void setCategory(String category) {
		this.category = category;
	}


	public String getProductCode() {
		return productCode;
	}


	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}


	public String getProductModel() {
		return productModel;
	}


	public void setProductModel(String productModel) {
		this.productModel = productModel;
	}


	public String getRegistrationNumber() {
		return registrationNumber;
	}


	public void setRegistrationNumber(String registrationNumber) {
		this.registrationNumber = registrationNumber;
	}


	public String getBrand() {
		return brand;
	}


	public void setBrand(String brand) {
		this.brand = brand;
	}


	public String getProductNamePrimary() {
		return productNamePrimary;
	}


	public void setProductNamePrimary(String productNamePrimary) {
		this.productNamePrimary = productNamePrimary;
	}


	public String getProductNameSecondary() {
		return productNameSecondary;
	}


	public void setProductNameSecondary(String productNameSecondary) {
		this.productNameSecondary = productNameSecondary;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getHashTag() {
		return hashTag;
	}


	public void setHashTag(String hashTag) {
		this.hashTag = hashTag;
	}


	public String getUnits() {
		return units;
	}


	public void setUnits(String units) {
		this.units = units;
	}


	public int getMinOfOrder() {
		return minOfOrder;
	}


	public void setMinOfOrder(int minOfOrder) {
		this.minOfOrder = minOfOrder;
	}


	public String getPackaging() {
		return packaging;
	}


	public void setPackaging(String packaging) {
		this.packaging = packaging;
	}


	public BigDecimal getPrice() {
		return price;
	}


	public void setPrice(BigDecimal price) {
		this.price = price;
	}


	public List<ProductMediaResponse> getImages() {
		return images;
	}


	public void setImages(List<ProductMediaResponse> images) {
		this.images = images;
	}
}

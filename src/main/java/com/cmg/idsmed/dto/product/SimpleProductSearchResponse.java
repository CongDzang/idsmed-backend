package com.cmg.idsmed.dto.product;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.cmg.idsmed.model.entity.product.ProductFeatures;
import com.cmg.idsmed.model.entity.product.ProductTechnical;
import org.springframework.util.CollectionUtils;

import com.cmg.idsmed.dto.AbstractResponse;
import com.cmg.idsmed.model.entity.product.Product;
import com.cmg.idsmed.model.entity.product.ProductMedia.MEDIA_TYPE;

public class SimpleProductSearchResponse extends AbstractResponse {

    private Long id;

    private String productNamePrimary;

    private String productNameSecondary;

    public String productDescription;

    private List<ProductMediaResponse> productMediaList = new ArrayList<>();

    private BigDecimal price;

    private List<ProductFeaturesResponse> productFeaturesList = new ArrayList<>();

    private List<ProductTechnicalResponse> productTechnicalList = new ArrayList<>();

    public SimpleProductSearchResponse() {
    }

    public SimpleProductSearchResponse(Product product, String ossBucketDomain) {
        if (product != null) {
            this.id = product.getId();
            this.productNamePrimary = product.getProductNamePrimary();
            this.productNameSecondary = product.getProductNameSecondary();
            this.productDescription = product.getProductDescription();
            this.productMediaList = CollectionUtils.isEmpty(product.getProductMediaList()) ? new ArrayList<ProductMediaResponse>() :
                    product.getProductMediaList()
                            .stream()
                            .filter(pm -> MEDIA_TYPE.IMAGE.getType().equalsIgnoreCase(pm.getDocumentType()))
                            .map(pm -> new ProductMediaResponse(pm, ossBucketDomain))
                            .collect(Collectors.toList());
            this.price = product.getPrice();
            this.productFeaturesList = CollectionUtils.isEmpty(product.getProductFeaturesList()) ? new ArrayList<ProductFeaturesResponse>() :
                    product.getProductFeaturesList()
                            .stream()
                            .map(pf -> new ProductFeaturesResponse(pf))
                            .collect(Collectors.toList());
            this.productTechnicalList = CollectionUtils.isEmpty(product.getProductTechnicalList()) ? new ArrayList<ProductTechnicalResponse>() :
                    product.getProductTechnicalList()
                            .stream()
                            .map(pt -> new ProductTechnicalResponse(pt))
                            .collect(Collectors.toList());
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProductNamePrimary() {
        return productNamePrimary;
    }

    public void setProductNamePrimary(String productNamePrimary) {
        this.productNamePrimary = productNamePrimary;
    }

    public String getProductNameSecondary() {
        return productNameSecondary;
    }

    public void setProductNameSecondary(String productNameSecondary) {
        this.productNameSecondary = productNameSecondary;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public List<ProductMediaResponse> getProductMediaList() {
        return productMediaList;
    }

    public void setProductMediaList(List<ProductMediaResponse> productMediaList) {
        this.productMediaList = productMediaList;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public List<ProductFeaturesResponse> getProductFeaturesList() {
        return productFeaturesList;
    }

    public void setProductFeaturesList(List<ProductFeaturesResponse> productFeaturesList) {
        this.productFeaturesList = productFeaturesList;
    }

    public List<ProductTechnicalResponse> getProductTechnicalList() {
        return productTechnicalList;
    }

    public void setProductTechnicalList(List<ProductTechnicalResponse> productTechnicalList) {
        this.productTechnicalList = productTechnicalList;
    }
}

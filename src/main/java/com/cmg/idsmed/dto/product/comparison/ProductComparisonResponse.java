package com.cmg.idsmed.dto.product.comparison;

import com.cmg.idsmed.common.enums.DeviceCategoryEnum;
import com.cmg.idsmed.common.enums.ImageVersionEnum;
import com.cmg.idsmed.model.entity.product.*;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class ProductComparisonResponse {
	private Long id;
	private String thirdVersionImgUrl; //done
	private String productName; //done
	private List<String> careAreaNames = new ArrayList<>(); //Done
	private String categoryName;//done
	private String secondCategoryName;//done
	private String brandName; //done
	private BigDecimal price;//done
	private String packaging;
	private String productCode;
	private String productModel; //done
	private String deviceCategoryName;
	private List<String> uniqueSellingAdvantages = new ArrayList<>();

	private List<ProductCompareDataResponse> comparedData = new ArrayList<>();

	public ProductComparisonResponse() {

	}

	public ProductComparisonResponse(Product product, String langCode, String ossBucketDomain) {
		this.id = product.getId();

		this.productName = product.getNameByLangCode(langCode);
		this.productCode = product.getProductCode();
		this.productModel = product.getProductModel();
		List<ProductMedia> mediaList = product.getProductMediaList();
		if (!CollectionUtils.isEmpty(mediaList)) {
			Optional<ProductMedia> thirdVersionImg = mediaList.stream().filter(m -> {
				if (m.getDocumentType().equals(ProductMedia.MEDIA_TYPE.IMAGE.getType())
						&& m.getVersion() == ImageVersionEnum.VERSION3.getVersion()) {
					return true;
				}
				return false;
			}).findFirst();
			if (thirdVersionImg.isPresent() && !StringUtils.isEmpty(ossBucketDomain)) {
				this.thirdVersionImgUrl = ossBucketDomain + thirdVersionImg.get().getDocumentUrl();
			}
		}

		List<ProductCareArea> productCareAreas = product.getProductCareAreaList().stream()
				.map(ProductCareAreaDetail::getProductCareArea).collect(Collectors.toList());

		if (!CollectionUtils.isEmpty(productCareAreas)) {
			List<String> careAreaNames = new ArrayList<>();
			for (ProductCareArea careArea : productCareAreas) {
				careAreaNames.add(careArea.getNameByLangCode(langCode));
			}
			this.careAreaNames = careAreaNames;
		}

		this.categoryName = product.getProductCategory() != null ? product.getProductCategory().getNameByLangCode(langCode) : null;
		this.brandName = product.getProductBrand() != null ? product.getProductBrand().getBrandName() : null;
		this.packaging = product.getPackaging();
		this.productCode = product.getProductCode();
		this.price = product.getPrice();
		if (product.getDeviceCategory() != null) {
			for (DeviceCategoryEnum deviceCategoryEnum : DeviceCategoryEnum.values()) {
				if (product.getDeviceCategory().equals(deviceCategoryEnum.getCode())) {
					this.deviceCategoryName = deviceCategoryEnum.getName();
					break;
				}
			}
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductModel() {
		return productModel;
	}

	public void setProductModel(String productModel) {
		this.productModel = productModel;
	}

	public String getThirdVersionImgUrl() {
		return thirdVersionImgUrl;
	}

	public void setThirdVersionImgUrl(String thirdVersionImgUrl) {
		this.thirdVersionImgUrl = thirdVersionImgUrl;
	}

	public List<ProductCompareDataResponse> getComparedData() {
		return comparedData;
	}

	public void setComparedData(List<ProductCompareDataResponse> comparedData) {
		this.comparedData = comparedData;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public List<String> getCareAreaNames() {
		return careAreaNames;
	}

	public void setCareAreaNames(List<String> careAreaNames) {
		this.careAreaNames = careAreaNames;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getSecondCategoryName() {
		return secondCategoryName;
	}

	public void setSecondCategoryName(String secondCategoryName) {
		this.secondCategoryName = secondCategoryName;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public String getPackaging() {
		return packaging;
	}

	public void setPackaging(String packaging) {
		this.packaging = packaging;
	}

	public String getDeviceCategoryName() {
		return deviceCategoryName;
	}

	public void setDeviceCategoryName(String deviceCategoryName) {
		this.deviceCategoryName = deviceCategoryName;
	}

	public List<String> getUniqueSellingAdvantages() {
		return uniqueSellingAdvantages;
	}

	public void setUniqueSellingAdvantages(List<String> uniqueSellingAdvantages) {
		this.uniqueSellingAdvantages = uniqueSellingAdvantages;
	}
}

package com.cmg.idsmed.dto.product.config;


import java.util.ArrayList;
import java.util.List;

public class ProductInfoConfigurationListResponse {
    private Integer totalPage = null;
    private List<ProductInfoConfigurationResponse> productInfoConfigs = new ArrayList<>();
    private Integer totalCount = null;
    private Integer pageNo = null;
    private Integer pageSize = null;

    public ProductInfoConfigurationListResponse(){
    }

    public ProductInfoConfigurationListResponse(Integer pageNo, Integer pageSize) {
        this.totalPage = 0;
        this.totalCount = 0;
        this.pageSize = pageSize;
        this.pageNo = pageNo;
    }

    public ProductInfoConfigurationListResponse(Integer totalCount, Integer pageIndex, Integer pageSize, List<ProductInfoConfigurationResponse> productInfoConfigs) {
        if (pageSize != null && !pageSize.equals(0)) {
            Integer calTotalPage = totalCount/pageSize;
            if(totalCount % pageSize > 0) {
                calTotalPage++;
            }
            this.totalPage = calTotalPage;
        }
        this.totalCount = totalCount;

        Integer pageCount = pageSize >= totalCount ? 1 : (totalCount%pageSize > 0 ? totalCount/pageSize +1 : totalCount/pageSize);
        Integer pageNo = pageIndex*pageSize >= totalCount ? pageCount-1 : pageIndex;

        this.pageNo = pageNo+1;
        this.pageSize = pageSize;
        this.productInfoConfigs = productInfoConfigs;
    }

    public ProductInfoConfigurationListResponse(Integer totalCount, List<ProductInfoConfigurationResponse> productInfoConfigs) {
        this.totalCount = totalCount;
        this.productInfoConfigs = productInfoConfigs;
        this.totalPage = 1;
        this.pageSize = totalCount;
        this.pageNo = 1;
    }

    public Integer getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(Integer totalPage) {
        this.totalPage = totalPage;
    }

    public List<ProductInfoConfigurationResponse> getProductInfoConfigs() {
        return productInfoConfigs;
    }

    public void setProductConfigurationList(List<ProductInfoConfigurationResponse> productInfoConfigs) {
        this.productInfoConfigs = productInfoConfigs;
    }

    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    public Integer getPageNo() {
        return pageNo;
    }

    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }
}

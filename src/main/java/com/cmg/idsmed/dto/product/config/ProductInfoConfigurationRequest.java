package com.cmg.idsmed.dto.product.config;

import com.cmg.idsmed.dto.AbstractRequest;
import com.cmg.idsmed.model.entity.product.config.ProductLabelConfig;

import java.util.List;

public class ProductInfoConfigurationRequest extends AbstractRequest {
    private Long id;
    private Long countryId;
    private Long provinceId;
    private List<ProductLabelConfigurationRequest> productLabelConfigs;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCountryId() {
        return countryId;
    }

    public void setCountryId(Long countryId) {
        this.countryId = countryId;
    }

    public Long getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(Long provinceId) {
        this.provinceId = provinceId;
    }

    public List<ProductLabelConfigurationRequest> getProductLabelConfigurationRequestList() {
        return productLabelConfigs;
    }

    public void setProductLabelConfigList(List<ProductLabelConfigurationRequest> productLabelConfigs) { this.productLabelConfigs = productLabelConfigs; }

    public List<ProductLabelConfigurationRequest> getProductLabelConfigs() { return productLabelConfigs; }

    public void setProductLabelConfigurationList(List<ProductLabelConfigurationRequest> productLabelConfigurationList) { this.productLabelConfigs = productLabelConfigs; }
}

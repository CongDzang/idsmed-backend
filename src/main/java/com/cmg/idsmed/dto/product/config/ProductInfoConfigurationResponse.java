package com.cmg.idsmed.dto.product.config;

import com.cmg.idsmed.model.entity.masterdata.Country;
import com.cmg.idsmed.model.entity.masterdata.Province;
import com.cmg.idsmed.model.entity.product.config.ProductInfoConfig;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class ProductInfoConfigurationResponse {
    private Long id;
    private Long countryId;
    private String countryCode;
    private String countryEnglishBriefName;
    private String countryChineseBriefName;
    private Long provinceId;
    private String provinceCode;
    private String provinceEnglishBriefName;
    private String provinceChineseBriefName;
    private Integer status;
    private List<ProductLabelConfigResponse> productInfoLabelConfigs = new ArrayList<>();

    public ProductInfoConfigurationResponse(ProductInfoConfig productInfoConfig){
        if(productInfoConfig != null) {
            Country country = productInfoConfig.getCountry();
            Province province = productInfoConfig.getProvince();
            this.id = productInfoConfig.getId();
            this.countryId = country.getId();
            this.countryCode = country.getCountryCode();
            this.countryEnglishBriefName = country.getEnglishBriefName();
            this.countryChineseBriefName = country.getChineseBriefName();
            if(province != null) {
                this.provinceId = province.getId();
                this.provinceCode = province.getProvinceCode();
                this.provinceEnglishBriefName = province.getEnglishBriefName();
                this.provinceChineseBriefName = province.getChineseBriefName();
            }
            this.status = productInfoConfig.getStatus();
            this.productInfoLabelConfigs = productInfoConfig.getProductLabelConfigs()
                    .stream()
                    .map(plc -> new ProductLabelConfigResponse(plc)).sorted(Comparator.comparing(ProductLabelConfigResponse::getSequence))
                    .collect(Collectors.toList());

        }
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Long getId() { return id; }

    public void setId(Long id) { this.id = id; }

    public Long getCountryId() { return countryId; }

    public void setCountryId(Long countryId) { this.countryId = countryId; }

    public String getCountryCode() { return countryCode; }

    public void setCountryCode(String countryCode) { this.countryCode = countryCode; }

    public Long getProvinceId() { return provinceId; }

    public void setProvinceId(Long provinceId) { this.provinceId = provinceId; }

    public String getProvinceCode() { return provinceCode; }

    public void setProvinceCode(String provinceCode) { this.provinceCode = provinceCode; }

    public List<ProductLabelConfigResponse> getProductInfoLabelConfigs() { return productInfoLabelConfigs; }

    public void setProductLabelConfigs(List<ProductLabelConfigResponse> productInfoLabelConfigs) { this.productInfoLabelConfigs = productInfoLabelConfigs; }

    public String getCountryEnglishBriefName() { return countryEnglishBriefName; }

    public void setCountryEnglishBriefName(String countryEnglishBriefName) { this.countryEnglishBriefName = countryEnglishBriefName; }

    public String getCountryChineseBriefName() { return countryChineseBriefName; }

    public void setCountryChineseBriefName(String countryChineseBriefName) { this.countryChineseBriefName = countryChineseBriefName; }

    public String getProvinceEnglishBriefName() { return provinceEnglishBriefName; }

    public void setProvinceEnglishBriefName(String provinceEnglishBriefName) { this.provinceEnglishBriefName = provinceEnglishBriefName; }

    public String getProvinceChineseBriefName() { return provinceChineseBriefName; }

    public void setProvinceChineseBriefName(String provinceChineseBriefName) { this.provinceChineseBriefName = provinceChineseBriefName; }
}

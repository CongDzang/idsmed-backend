package com.cmg.idsmed.dto.product.config;

import com.cmg.idsmed.model.entity.product.config.ProductLabelConfig;

public class ProductLabelConfigResponse {
    private Long id;
    private String labelPrimaryName;
    private String labelSecondaryName;
    private Boolean isMandatory;
    /**
     * This column determine this label has expiry date or not
     */
    private Boolean isExpired = false;
    private Integer sequence;
    private Integer status;

    public ProductLabelConfigResponse(ProductLabelConfig productLabelConfig){
        this.id = productLabelConfig.getId();
        this.labelPrimaryName = productLabelConfig.getLabelPrimaryName();
        this.labelSecondaryName = productLabelConfig.getLabelSecondaryName();
        this.isMandatory = productLabelConfig.getIsMandatory();
        this.isExpired = productLabelConfig.getIsExpired();
        this.sequence = productLabelConfig.getSequence();
        this.status = productLabelConfig.getStatus();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLabelPrimaryName() {
        return labelPrimaryName;
    }

    public void setLabelPrimaryName(String labelPrimaryName) {
        this.labelPrimaryName = labelPrimaryName;
    }

    public String getLabelSecondaryName() {
        return labelSecondaryName;
    }

    public void setLabelSecondaryName(String labelSecondaryName) {
        this.labelSecondaryName = labelSecondaryName;
    }

    public Boolean getMandatory() {
        return isMandatory;
    }

    public void setMandatory(Boolean mandatory) {
        this.isMandatory = mandatory;
    }

    public Boolean getExpired() { return isExpired; }

    public void setExpired(Boolean expired) {
        this.isExpired = isExpired;
    }

    public Integer getSequence() {
        return sequence;
    }

    public void setSequence(Integer sequence) {
        this.sequence = sequence;
    }

    public Integer getStatus() { return status; }

    public void setStatus(Integer status) { this.status = status; }

}

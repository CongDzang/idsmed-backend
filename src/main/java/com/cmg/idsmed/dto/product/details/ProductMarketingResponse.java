package com.cmg.idsmed.dto.product.details;

import java.util.List;

public class ProductMarketingResponse {
	
	private List<ProductSellingRateResponse> productSellingRates;
	private List<RelatedProductResponse> relatedProducts;
	private List<ProductSellingHistoryResponse> productSellingHistories;

	public ProductMarketingResponse() {
		super();
	}
	public ProductMarketingResponse(List<ProductSellingRateResponse> productSellingRates, List<RelatedProductResponse> relatedProducts, List<ProductSellingHistoryResponse> productSellingHistories) {
		super();
		this.productSellingRates = productSellingRates;
		this.relatedProducts = relatedProducts;
		this.productSellingHistories = productSellingHistories;
	}

	public List<ProductSellingRateResponse> getProductSellingRates() {
		return productSellingRates;
	}

	public void setProductSellingRates(List<ProductSellingRateResponse> productSellingRates) {
		this.productSellingRates = productSellingRates;
	}

	public List<RelatedProductResponse> getRelatedProducts() {
		return relatedProducts;
	}

	public void setRelatedProducts(List<RelatedProductResponse> relatedProducts) {
		this.relatedProducts = relatedProducts;
	}

	public List<ProductSellingHistoryResponse> getProductSellingHistories() {
		return productSellingHistories;
	}

	public void setProductSellingHistories(List<ProductSellingHistoryResponse> productSellingHistories) {
		this.productSellingHistories = productSellingHistories;
	}
}

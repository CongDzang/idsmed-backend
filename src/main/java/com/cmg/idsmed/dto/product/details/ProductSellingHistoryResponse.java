package com.cmg.idsmed.dto.product.details;

import java.util.Date;
import java.util.List;

import org.springframework.beans.BeanUtils;

import com.cmg.idsmed.dto.AbstractResponse;
import com.cmg.idsmed.model.entity.product.details.ProductSellingHistory;

public class ProductSellingHistoryResponse extends AbstractResponse {
	
	public ProductSellingHistoryResponse(ProductSellingHistory psh, String productNamePrimary, List<String> mediaUrls) {
		super();
		String [] ignoreProperties = {};
        BeanUtils.copyProperties(psh, this, ignoreProperties);
        this.productNamePrimary = productNamePrimary;
        this.mediaUrls = mediaUrls;
	}
	
	public ProductSellingHistoryResponse(Long productId, Long relatedProductId, String productPrimaryName, Date purchaseDate, List<String> mediaUrls) {
		super();
		this.productId = productId;
		this.relatedProductId = relatedProductId;
		this.productNamePrimary = productPrimaryName;
		this.purchaseDate = purchaseDate;
		this.mediaUrls = mediaUrls;
	}

	Long productId;
	
	Long relatedProductId;
	
	String productNamePrimary;
	
	Date purchaseDate;
	
	List<String> mediaUrls;

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public String getProductNamePrimary() {
		return productNamePrimary;
	}

	public void setProductNamePrimary(String productNamePrimary) {
		this.productNamePrimary = productNamePrimary;
	}

	public Date getPurchaseDate() {
		return purchaseDate;
	}

	public void setPurchaseDate(Date purchaseDate) {
		this.purchaseDate = purchaseDate;
	}
	
	public List<String> getMediaUrls() {
		return mediaUrls;
	}

	public void setMediaUrls(List<String> mediaUrls) {
		this.mediaUrls = mediaUrls;
	}

	public Long getRelatedProductId() {
		return relatedProductId;
	}

	public void setRelatedProductId(Long relatedProductId) {
		this.relatedProductId = relatedProductId;
	}
}

package com.cmg.idsmed.dto.product.details;

import java.util.List;

import org.springframework.beans.BeanUtils;

import com.cmg.idsmed.dto.AbstractResponse;
import com.cmg.idsmed.model.entity.product.details.ProductSellingRate;

public class ProductSellingRateResponse extends AbstractResponse {
	
	public ProductSellingRateResponse(ProductSellingRate productSellingRate, String productNamePrimay, List<String> mediaUrls) {
		super();
		String [] ignoreProperties = {};
        BeanUtils.copyProperties(productSellingRate, this, ignoreProperties);
        this.productNamePrimary = productNamePrimay;
        this.mediaUrls = mediaUrls;
	}
	
	public ProductSellingRateResponse(Long productId,Long relatedProductId, String productNamePrimary, Long purchaseFeq, List<String> mediaUrls) {
		super();
		this.productId = productId;
		this.relatedProductId = relatedProductId;
		this.productNamePrimary = productNamePrimary;
		this.purchaseFeq = purchaseFeq;
		this.mediaUrls = mediaUrls;
	}

	Long productId;
	
	Long relatedProductId;
	
	String productNamePrimary;
	
	Long purchaseFeq;
	
	List<String> mediaUrls;
	
	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public String getProductNamePrimary() {
		return productNamePrimary;
	}

	public void setProductNamePrimary(String productNamePrimary) {
		this.productNamePrimary = productNamePrimary;
	}

	public Long getPurchaseFeq() {
		return purchaseFeq;
	}

	public void setPurchaseFeq(Long purchaseFeq) {
		this.purchaseFeq = purchaseFeq;
	}

	public List<String> getMediaUrls() {
		return mediaUrls;
	}

	public void setMediaUrls(List<String> mediaUrls) {
		this.mediaUrls = mediaUrls;
	}

	public Long getRelatedProductId() {
		return relatedProductId;
	}

	public void setRelatedProductId(Long relatedProductId) {
		this.relatedProductId = relatedProductId;
	}
}

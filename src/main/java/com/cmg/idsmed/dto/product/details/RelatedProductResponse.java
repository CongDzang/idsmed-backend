package com.cmg.idsmed.dto.product.details;

import java.util.List;

import org.springframework.beans.BeanUtils;

import com.cmg.idsmed.dto.AbstractResponse;
import com.cmg.idsmed.model.entity.product.details.RelatedProduct;

public class RelatedProductResponse extends AbstractResponse {

	public RelatedProductResponse(RelatedProduct rp, String relatedProductNamePrimary, List<String> mediaUrls) {
		super();
		String [] ignoreProperties = {};
        BeanUtils.copyProperties(rp, this, ignoreProperties);
        this.relatedProductNamePrimary = relatedProductNamePrimary;
        this.mediaUrls = mediaUrls;
	}
	
	public RelatedProductResponse(Long productId, Long relatedProductId, String relatedProductNamePrimary, List<String> mediaUrls) {
		super();
		this.productId = productId;
		this.relatedProductId = relatedProductId;
		this.relatedProductNamePrimary = relatedProductNamePrimary;
		this.mediaUrls = mediaUrls;
	}
	
	Long productId;
	
	Long relatedProductId;
	
	String relatedProductNamePrimary;
	
	List<String> mediaUrls;
	
	public List<String> getMediaUrls() {
		return mediaUrls;
	}

	public void setMediaUrls(List<String> mediaUrls) {
		this.mediaUrls = mediaUrls;
	}

	public Long getRelatedProductId() {
		return relatedProductId;
	}

	public void setRelatedProductId(Long relatedProductId) {
		this.relatedProductId = relatedProductId;
	}

	public String getRelatedProductNamePrimary() {
		return relatedProductNamePrimary;
	}

	public void setRelatedProductNamePrimary(String relatedProductNamePrimary) {
		this.relatedProductNamePrimary = relatedProductNamePrimary;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}
}

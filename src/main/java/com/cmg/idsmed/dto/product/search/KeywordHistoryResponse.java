package com.cmg.idsmed.dto.product.search;

import com.cmg.idsmed.dto.AbstractResponse;
import com.cmg.idsmed.model.entity.product.search.KeywordHistory;

public class KeywordHistoryResponse extends AbstractResponse {

	String keyword;
	Long count;

	public KeywordHistoryResponse(String keyword) {
		this.keyword = keyword;
	}

	public KeywordHistoryResponse(String keyword, Long count) {
		this.keyword = keyword;
		this.count = count;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public Long getCount() {
		return count;
	}

	public void setCount(Long count) {
		this.count = count;
	}
}

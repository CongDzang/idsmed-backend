package com.cmg.idsmed.dto.product.wedoctor;

import com.cmg.idsmed.common.enums.ImageVersionEnum;
import com.cmg.idsmed.model.entity.product.ProductMedia;
import org.springframework.util.CollectionUtils;

import java.util.List;

public class ProductImagesWedoctorResponse {
	private String original;
	private String small;
	private String big;
	private String tiny;

	public ProductImagesWedoctorResponse() {

	}

	public ProductImagesWedoctorResponse(List<ProductMedia> images) {
		if (!CollectionUtils.isEmpty(images)) {
			for (ProductMedia media : images) {
				if (media.getVersion() == ImageVersionEnum.VERSION1.getVersion()) {
					this.original = media.getDocumentUrl();
				}
				if (media.getVersion() == ImageVersionEnum.VERSION3.getVersion()) {
					this.small = media.getDocumentUrl();
				}

				if (media.getVersion() == ImageVersionEnum.VERSION2.getVersion()) {
					this.big = media.getDocumentUrl();
				}

				if (media.getVersion() == ImageVersionEnum.VERSION4.getVersion()) {
					this.tiny = media.getDocumentUrl();
				}
			}
		}
	}

	public String getOriginal() {
		return original;
	}

	public void setOriginal(String original) {
		this.original = original;
	}

	public String getSmall() {
		return small;
	}

	public void setSmall(String small) {
		this.small = small;
	}

	public String getBig() {
		return big;
	}

	public void setBig(String big) {
		this.big = big;
	}

	public String getTiny() {
		return tiny;
	}

	public void setTiny(String tiny) {
		this.tiny = tiny;
	}
}

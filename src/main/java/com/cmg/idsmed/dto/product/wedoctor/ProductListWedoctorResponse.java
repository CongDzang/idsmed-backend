package com.cmg.idsmed.dto.product.wedoctor;

import com.cmg.idsmed.dto.product.ProductResponse;

import java.util.List;

public class ProductListWedoctorResponse {
	private Integer totalPage = null;
	private List<ProductWedoctorResponse> productList;
	private Integer totalCount = null;
	private Integer pageNo = null;
	private Integer pageSize = null;

	public ProductListWedoctorResponse(Integer pageNo, Integer pageSize) {
		this.totalPage = 0;
		this.totalCount = 0;
		this.pageSize = pageSize;
		this.pageNo = pageNo;
	}

	public ProductListWedoctorResponse(Integer totalCount, Integer pageIndex, Integer pageSize, List<ProductWedoctorResponse> productList) {
		if (pageSize != null && !pageSize.equals(0)) {
			Integer calTotalPage = totalCount/pageSize;
			if(totalCount % pageSize > 0) {
				calTotalPage++;
			}
			this.totalPage = calTotalPage;
		}
		this.totalCount = totalCount;

		Integer pageCount = pageSize >= totalCount ? 1 : (totalCount%pageSize > 0 ? totalCount/pageSize +1 : totalCount/pageSize);
		Integer pageNo = pageIndex*pageSize >= totalCount ? pageCount-1 : pageIndex;

		this.pageNo = pageNo+1;
		this.pageSize = pageSize;
		this.productList = productList;
	}

	public ProductListWedoctorResponse(Integer totalCount, List<ProductWedoctorResponse> productList) {
		this.totalCount = totalCount;
		this.productList = productList;
		this.totalPage = 1;
		this.pageSize = totalCount;
		this.pageNo = 1;
	}

	// Inactive product based on list of product id
	public ProductListWedoctorResponse(List<ProductWedoctorResponse> productList) { this.productList = productList; }

	public Integer getTotalPage() {
		return totalPage;
	}

	public void setTotalPage(Integer totalPage) {
		this.totalPage = totalPage;
	}

	public List<ProductWedoctorResponse> getProductList() {
		return productList;
	}

	public void setProductList(List<ProductWedoctorResponse> productList) {
		this.productList = productList;
	}

	public Integer getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}

	public Integer getPageNo() {
		return pageNo;
	}

	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}
}

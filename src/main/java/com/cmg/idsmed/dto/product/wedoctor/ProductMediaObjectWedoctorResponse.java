package com.cmg.idsmed.dto.product.wedoctor;

import com.cmg.idsmed.dto.product.ProductMediaResponse;
import com.cmg.idsmed.model.entity.product.Product;
import com.cmg.idsmed.model.entity.product.ProductMedia;
import org.apache.commons.collections.CollectionUtils;

import java.util.*;
import java.util.stream.Collectors;

public class ProductMediaObjectWedoctorResponse {

	private ProductMediaResponse brochure;

	private ProductMediaResponse certificate;

	private ProductMediaResponse video;

	private List<ProductImagesWedoctorResponse> images = new ArrayList<>();

	public ProductMediaObjectWedoctorResponse(Product product, String ossBucketDomain) {
		super();
		List<ProductMedia> pms = product.getProductMediaList();
		if (!CollectionUtils.isEmpty(pms)) {
			List<ProductMediaResponse> brochures = pms.stream().filter(pm -> ProductMedia.MEDIA_TYPE.BROCHURE.getType().equalsIgnoreCase(pm.getDocumentType()))
					.map(pm -> new ProductMediaResponse(pm, ossBucketDomain)).collect(Collectors.toList());
			List<ProductMediaResponse> certificates = pms.stream().filter(pm -> ProductMedia.MEDIA_TYPE.CERTIFICATE.getType().equalsIgnoreCase(pm.getDocumentType()))
					.map(pm -> new ProductMediaResponse(pm, ossBucketDomain)).collect(Collectors.toList());
			List<ProductMediaResponse> videos = pms.stream().filter(pm -> ProductMedia.MEDIA_TYPE.VIDEO.getType().equalsIgnoreCase(pm.getDocumentType()))
					.map(pm -> new ProductMediaResponse(pm, ossBucketDomain)).collect(Collectors.toList());

			List<ProductMedia> imagesMedia = pms.stream().filter(pm -> ProductMedia.MEDIA_TYPE.IMAGE.getType()
					.equalsIgnoreCase(pm.getDocumentType())).collect(Collectors.toList());

			List<ProductImagesWedoctorResponse> imagesWedoctorResponses = new ArrayList<>();

			if (!CollectionUtils.isEmpty(imagesMedia)) {
				Map<String, List<ProductMedia>> imagesMap = imagesMedia.stream().collect(Collectors.groupingBy(ProductMedia::getGid));
				if (imagesMap != null) {
					Iterator<Map.Entry<String, List<ProductMedia>>> iterator = imagesMap.entrySet().iterator();
					while (iterator.hasNext()) {
						Map.Entry<String, List<ProductMedia>> entry = iterator.next();
						List<ProductMedia> imgs = entry.getValue();
						if (!CollectionUtils.isEmpty(imgs)) {
							ProductImagesWedoctorResponse imagesWedoctorResponse = new ProductImagesWedoctorResponse(imgs);
							imagesWedoctorResponses.add(imagesWedoctorResponse);
						}

					}
				}
			}

			this.brochure = CollectionUtils.isEmpty(brochures) ? null : brochures.get(0);
			this.certificate = CollectionUtils.isEmpty(certificates) ? null : certificates.get(0);
			this.video = CollectionUtils.isEmpty(videos) ? null : videos.get(0);
			this.images = imagesWedoctorResponses;
		}
	}

	public ProductMediaResponse getBrochure() {
		return brochure;
	}

	public void setBrochure(ProductMediaResponse brochure) {
		this.brochure = brochure;
	}

	public ProductMediaResponse getCertificate() {
		return certificate;
	}

	public void setCertificate(ProductMediaResponse certificate) {
		this.certificate = certificate;
	}

	public ProductMediaResponse getVideo() {
		return video;
	}

	public void setVideo(ProductMediaResponse video) {
		this.video = video;
	}

	public List<ProductImagesWedoctorResponse> getImages() {
		return images;
	}

	public void setImages(List<ProductImagesWedoctorResponse> images) {
		this.images = images;
	}
}

package com.cmg.idsmed.dto.product.wedoctor;

import com.cmg.idsmed.dto.masterdata.CountryResponse;
import com.cmg.idsmed.dto.masterdata.ProvinceResponse;
import com.cmg.idsmed.dto.product.*;
import com.cmg.idsmed.dto.product.details.ProductMarketingResponse;
import com.cmg.idsmed.model.entity.product.Product;
import com.cmg.idsmed.model.entity.product.ProductDocumentAttachment;
import org.springframework.beans.BeanUtils;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

public class ProductWedoctorResponse {

	private Long id;

	private String productNamePrimary;

	private String productNameSecondary;

	private String productCode;

	private String productModel;

	private String productDescription;

	private String registrationNumber;

	private String hashTag;

	private Date expDate;

	private String unit;

	private String packaging;

	private int minOfOrder;

	private String productBrochurePath;

	private String productVideoPath;

	private String remarks;

	private String countryCode;

	private BigDecimal price;

	private String productBrochureUrl;

	private String productVideoUrl;

	private Integer status;

	private String companyPrimaryName;

	private String rejectReason;

	private Integer approvedBy;

	private Date approvedDate;

	private Integer productVersion;

	private String otherDescription;

	private Boolean isGovtSubsidized;

	private Long vendorId;

	private Integer vendorStatus;

	private Boolean canProcure;

	private Long deviceCategory;

	private String deliveryLeadTime;

	private String skuNumber;

	private String productFunctionlityAndClaims;

	private String disclaimer;

	private String instructionForUse;

	private String productWebsite;

	private Integer productType;

	private CountryResponse manufacturerCountry;

	private ProvinceResponse manufacturerProvince;

	private ProductMediaObjectWedoctorResponse productMediaObj;

	private List<ProductFeaturesResponse> productFeaturesList;

	private List<ProductTechnicalResponse> productTechnicalList;

	private ProductCategoryResponse productCategory;

	private List<ProductSecondCategoryResponse> productSecondCategories;

	private List<ProductCareAreaDetailResponse> productCareAreaList;

	private ProductBrandResponse productBrand;

	private ProductMarketingResponse productMarketingResponse;

	private List<ProductDocumentAttachmentResponse> productDocumentAttachments;

	private ProductGeneralInfoRateResponse productGeneralInfoRate;

	private ProductWishlistResponse productWishlist;

	public ProductWedoctorResponse(Product product, String ossBucketDomain) {
		if (product != null) {

			String [] ignoreProperties = {"productMediaList",
					"productCertificateList",
					"productFeaturesList",
					"productTechnicalList",
					"productCareAreaDetailList",
					"vendor",
					"productDocumentAttachmentList",
					"productRating",
					"productSecondCategoryDetails",
					"uniqueSellingAdvantageList",
					"manufacturerCountryCode",
					"manufacturerProvinceCode"};
			BeanUtils.copyProperties(product, this, ignoreProperties);

			if (!CollectionUtils.isEmpty(product.getProductMediaList())) {
				this.productMediaObj = new ProductMediaObjectWedoctorResponse(product, ossBucketDomain);
			}

			if (!CollectionUtils.isEmpty(product.getProductFeaturesList())) {
				this.productFeaturesList = product.getProductFeaturesList()
						.stream()
						.map(pf -> new ProductFeaturesResponse(pf)).collect(Collectors.toList());
			}

			if (!CollectionUtils.isEmpty(product.getProductTechnicalList())) {
				this.productTechnicalList = product.getProductTechnicalList()
						.stream()
						.map(pt -> new ProductTechnicalResponse(pt)).collect(Collectors.toList());
			}

			if (product.getProductCategory() != null) {
				this.productCategory = new ProductCategoryResponse(product.getProductCategory());
			}

			if (!CollectionUtils.isEmpty(product.getProductCareAreaList())) {
				this.productCareAreaList = product.getProductCareAreaList().stream()
						.map(pcadl -> new ProductCareAreaDetailResponse(pcadl, ossBucketDomain)).collect(Collectors.toList());
			}

			if (product.getProductBrand() != null) {
				this.productBrand = new ProductBrandResponse(product.getProductBrand(), ossBucketDomain);
			}

			//For each type of ProductLabelConfig we just return the ProductDocumentAttachment with latest createdDate.
			List<ProductDocumentAttachment> allDocumentAttachments = product.getProductDocumentAttachmentList();
			List<ProductDocumentAttachment> validDocumentAttachments = new ArrayList<>();

			if(!CollectionUtils.isEmpty(allDocumentAttachments)) {

				List<Long> productLabelConfigIds = allDocumentAttachments
						.stream()
						.map(fat -> fat.getProductLabelConfig().getId()).distinct()
						.collect(Collectors.toList());

				if (!CollectionUtils.isEmpty(productLabelConfigIds)) {
					for (Long id : productLabelConfigIds) {
						Optional<ProductDocumentAttachment> fileAttachmentOpt = allDocumentAttachments.stream().filter(f -> {
							if (f.getProductLabelConfig().getId().equals(id)) {
								return true;
							}
							return false;
						}).max(Comparator.comparing(ProductDocumentAttachment::getCreatedDate));
						if (fileAttachmentOpt.isPresent()) {
							validDocumentAttachments.add(fileAttachmentOpt.get());
						}
					}
				}

				if (!CollectionUtils.isEmpty(validDocumentAttachments)) {
					this.productDocumentAttachments = validDocumentAttachments
							.stream()
							.map(at -> new ProductDocumentAttachmentResponse(at, ossBucketDomain)).collect(Collectors.toList());
				}
			}
			//============================================================

			this.productGeneralInfoRate = new ProductGeneralInfoRateResponse(product.getProductRating());

			if (product.getVendor() != null){
				this.vendorId = product.getVendor().getId();
				this.companyPrimaryName = StringUtils.isEmpty(product.getVendor().getCompanyNamePrimary())?"":product.getVendor().getCompanyNamePrimary();
				this.vendorStatus = product.getVendor().getStatus();
			}

			if (!CollectionUtils.isEmpty(product.getProductSecondCategoryDetails())){
				this.productSecondCategories = product.getProductSecondCategoryDetails()
						.stream()
						.map(pscd -> new ProductSecondCategoryResponse(pscd)).collect(Collectors.toList());
			}
		}
	}

	public ProductWedoctorResponse(Product product, ProductWishlistResponse wishlistResponse, String ossBucketDomain) {
		if (product != null) {

			String [] ignoreProperties = {"productMediaList",
					"productCertificateList",
					"productFeaturesList",
					"productTechnicalList",
					"productCareAreaDetailList",
					"vendor",
					"productDocumentAttachmentList",
					"productRating",
					"productSecondCategoryDetails",
					"uniqueSellingAdvantageList",
					"manufacturerCountryCode",
					"manufacturerProvinceCode"};
			BeanUtils.copyProperties(product, this, ignoreProperties);

			if (!CollectionUtils.isEmpty(product.getProductMediaList())) {
				this.productMediaObj = new ProductMediaObjectWedoctorResponse(product, ossBucketDomain);
			}

			if (!CollectionUtils.isEmpty(product.getProductFeaturesList())) {
				this.productFeaturesList = product.getProductFeaturesList()
						.stream()
						.map(pf -> new ProductFeaturesResponse(pf)).collect(Collectors.toList());
			}

			if (!CollectionUtils.isEmpty(product.getProductTechnicalList())) {
				this.productTechnicalList = product.getProductTechnicalList()
						.stream()
						.map(pt -> new ProductTechnicalResponse(pt)).collect(Collectors.toList());
			}

			if (product.getProductCategory() != null) {
				this.productCategory = new ProductCategoryResponse(product.getProductCategory());
			}

			if (!CollectionUtils.isEmpty(product.getProductCareAreaList())) {
				this.productCareAreaList = product.getProductCareAreaList().stream()
						.map(pcadl -> new ProductCareAreaDetailResponse(pcadl, ossBucketDomain)).collect(Collectors.toList());
			}

			if (product.getProductBrand() != null) {
				this.productBrand = new ProductBrandResponse(product.getProductBrand(), ossBucketDomain);
			}

			//For each type of ProductLabelConfig we just return the ProductDocumentAttachment with latest createdDate.
			List<ProductDocumentAttachment> allDocumentAttachments = product.getProductDocumentAttachmentList();
			List<ProductDocumentAttachment> validDocumentAttachments = new ArrayList<>();

			if(!CollectionUtils.isEmpty(allDocumentAttachments)) {

				List<Long> productLabelConfigIds = allDocumentAttachments
						.stream()
						.map(fat -> fat.getProductLabelConfig().getId()).distinct()
						.collect(Collectors.toList());

				if (!CollectionUtils.isEmpty(productLabelConfigIds)) {
					for (Long id : productLabelConfigIds) {
						Optional<ProductDocumentAttachment> fileAttachmentOpt = allDocumentAttachments.stream().filter(f -> {
							if (f.getProductLabelConfig().getId().equals(id)) {
								return true;
							}
							return false;
						}).max(Comparator.comparing(ProductDocumentAttachment::getCreatedDate));
						if (fileAttachmentOpt.isPresent()) {
							validDocumentAttachments.add(fileAttachmentOpt.get());
						}
					}
				}

				if (!CollectionUtils.isEmpty(validDocumentAttachments)) {
					this.productDocumentAttachments = validDocumentAttachments
							.stream()
							.map(at -> new ProductDocumentAttachmentResponse(at, ossBucketDomain)).collect(Collectors.toList());
				}
			}
			//============================================================
			this.productGeneralInfoRate = new ProductGeneralInfoRateResponse(product.getProductRating());

			if (product.getVendor() != null){
				this.vendorId = product.getVendor().getId();
				this.companyPrimaryName = StringUtils.isEmpty(product.getVendor().getCompanyNamePrimary())?"":product.getVendor().getCompanyNamePrimary();
				this.vendorStatus = product.getVendor().getStatus();
			}

			this.productWishlist = wishlistResponse;

			if (!CollectionUtils.isEmpty(product.getProductSecondCategoryDetails())){
				this.productSecondCategories = product.getProductSecondCategoryDetails()
						.stream()
						.map(pscd -> new ProductSecondCategoryResponse(pscd)).collect(Collectors.toList());
			}
		}
	}

	public Long getId() { return id; }

	public void setId(Long id) { this.id = id; }

	public String getProductNamePrimary() { return productNamePrimary; }

	public void setProductNamePrimary(String productNamePrimary) { this.productNamePrimary = productNamePrimary; }

	public String getProductNameSecondary() { return productNameSecondary; }

	public void setProductNameSecondary(String productNameSecondary) { this.productNameSecondary = productNameSecondary; }

	public String getProductCode() { return productCode; }

	public void setProductCode(String productCode) { this.productCode = productCode; }

	public String getProductModel() { return productModel; }

	public void setProductModel(String productModel) { this.productModel = productModel; }

	public String getProductDescription() { return productDescription; }

	public void setProductDescription(String productDescription) { this.productDescription = productDescription; }

	public String getRegistrationNumber() { return registrationNumber; }

	public void setRegistrationNumber(String registrationNumber) { this.registrationNumber = registrationNumber; }

	public String getHashTag() { return hashTag; }

	public void setHashTag(String hashTag) { this.hashTag = hashTag; }

	public Date getExpDate() { return expDate; }

	public void setExpDate(Date expDate) { this.expDate = expDate; }

	public String getUnit() { return unit; }

	public void setUnit(String unit) { this.unit = unit; }

	public String getPackaging() { return packaging; }

	public void setPackaging(String packaging) { this.packaging = packaging; }

	public int getMinOfOrder() { return minOfOrder; }

	public void setMinOfOrder(int minOfOrder) { this.minOfOrder = minOfOrder; }

	public String getProductBrochurePath() { return productBrochurePath; }

	public void setProductBrochurePath(String productBrochurePath) { this.productBrochurePath = productBrochurePath; }

	public String getProductVideoPath() { return productVideoPath; }

	public void setProductVideoPath(String productVideoPath) { this.productVideoPath = productVideoPath; }

	public String getRemarks() { return remarks; }

	public void setRemarks(String remarks) { this.remarks = remarks; }

	public String getCountryCode() { return countryCode; }

	public void setCountryCode(String countryCode) { this.countryCode = countryCode; }

	public BigDecimal getPrice() { return price; }

	public void setPrice(BigDecimal price) { this.price = price; }

	public ProductBrandResponse getProductBrand() { return productBrand; }

	public void setProductBrand(ProductBrandResponse productBrand) { this.productBrand = productBrand; }

	public String getProductBrochureUrl() { return productBrochureUrl; }

	public void setProductBrochureUrl(String productBrochureUrl) { this.productBrochureUrl = productBrochureUrl; }

	public String getProductVideoUrl() { return productVideoUrl; }

	public void setProductVideoUrl(String productVideoUrl) { this.productVideoUrl = productVideoUrl; }

	public Integer getStatus() { return status; }

	public void setStatus(Integer status) { this.status = status; }

	public List<ProductFeaturesResponse> getProductFeaturesList() { return productFeaturesList; }

	public void setProductFeaturesList(List<ProductFeaturesResponse> productFeaturesList) { this.productFeaturesList = productFeaturesList; }

	public List<ProductTechnicalResponse> getProductTechnicalList() { return productTechnicalList; }

	public void setProductTechnicalList(List<ProductTechnicalResponse> productTechnicalList) { this.productTechnicalList = productTechnicalList; }

	public ProductCategoryResponse getProductCategory() { return productCategory; }

	public void setProductCategory(ProductCategoryResponse productCategory) { this.productCategory = productCategory; }

	public String getCompanyPrimaryName() { return companyPrimaryName; }

	public void setCompanyPrimaryName(String companyPrimaryName) { this.companyPrimaryName = companyPrimaryName; }

	public String getRejectReason() { return rejectReason; }

	public void setRejectReason(String rejectReason) { this.rejectReason = rejectReason; }

	public Integer getApprovedBy() { return approvedBy; }

	public void setApprovedBy(Integer approvedBy) { this.approvedBy = approvedBy; }

	public Date getApprovedDate() { return approvedDate; }

	public void setApprovedDate(Date approvedDate) { this.approvedDate = approvedDate; }

	public List<ProductCareAreaDetailResponse> getProductCareAreaList() { return productCareAreaList; }

	public void setProductCareAreaDetailList(List<ProductCareAreaDetailResponse> productCareAreaList) { this.productCareAreaList = productCareAreaList; }

	public ProductMarketingResponse getProductMarketingResponse() { return productMarketingResponse; }

	public void setProductMarketingResponse(ProductMarketingResponse productMarketingResponse) { this.productMarketingResponse = productMarketingResponse; }

	public Integer getProductVersion() { return productVersion; }

	public void setProductVersion(Integer productVersion) { this.productVersion = productVersion; }

	public ProductMediaObjectWedoctorResponse getProductMediaObj() {
		return productMediaObj;
	}

	public void setProductMediaObj(ProductMediaObjectWedoctorResponse productMediaObj) {
		this.productMediaObj = productMediaObj;
	}

	public void setProductCareAreaList(List<ProductCareAreaDetailResponse> productCareAreaList) { this.productCareAreaList = productCareAreaList; }

	public List<ProductDocumentAttachmentResponse> getProductDocumentAttachments() { return productDocumentAttachments; }

	public void setProductDocumentAttachments(List<ProductDocumentAttachmentResponse> productDocumentAttachments) { this.productDocumentAttachments = productDocumentAttachments; }

	public String getOtherDescription() { return otherDescription; }

	public void setOtherDescription(String otherDescription) { this.otherDescription = otherDescription; }

	public Boolean getIsGovtSubsidized() { return isGovtSubsidized; }

	public void setIsGovtSubsidized(Boolean isGovtSubsidized) { this.isGovtSubsidized = isGovtSubsidized; }

	public Long getVendorId() { return vendorId; }

	public void setVendorId(Long vendorId) { this.vendorId = vendorId; }

	public ProductGeneralInfoRateResponse getProductGeneralInfoRate() { return productGeneralInfoRate; }

	public void setProductGeneralInfoRate(ProductGeneralInfoRateResponse productGeneralInfoRate) { this.productGeneralInfoRate = productGeneralInfoRate; }

	public ProductWishlistResponse getProductWishlist() { return productWishlist; }

	public void setProductWishlist(ProductWishlistResponse productWishlist) { this.productWishlist = productWishlist; }

	public Integer getVendorStatus() { return vendorStatus; }

	public void setVendorStatus(Integer vendorStatus) { this.vendorStatus = vendorStatus; }

	public Boolean getCanProcure() { return canProcure; }

	public void setCanProcure(Boolean canProcure) { this.canProcure = canProcure; }

	public Long getDeviceCategory() { return deviceCategory; }

	public void setDeviceCategory(Long deviceCategory) { this.deviceCategory = deviceCategory; }

	public String getDeliveryLeadTime() { return deliveryLeadTime; }

	public void setDeliveryLeadTime(String deliveryLeadTime) { this.deliveryLeadTime = deliveryLeadTime; }

	public String getSkuNumber() { return skuNumber; }

	public void setSkuNumber(String skuNumber) { this.skuNumber = skuNumber; }

	public String getProductFunctionlityAndClaims() { return productFunctionlityAndClaims; }

	public void setProductFunctionlityAndClaims(String productFunctionlityAndClaims) { this.productFunctionlityAndClaims = productFunctionlityAndClaims; }

	public String getDisclaimer() { return disclaimer; }

	public void setDisclaimer(String disclaimer) { this.disclaimer = disclaimer; }

	public String getInstructionForUse() { return instructionForUse; }

	public void setInstructionForUse(String instructionForUse) { this.instructionForUse = instructionForUse; }

	public String getProductWebsite() { return productWebsite; }

	public void setProductWebsite(String productWebsite) { this.productWebsite = productWebsite; }

	public List<ProductSecondCategoryResponse> getProductSecondCategories() {
		return productSecondCategories;
	}

	public void setProductSecondCategories(List<ProductSecondCategoryResponse> productSecondCategories) {
		this.productSecondCategories = productSecondCategories;
	}

	public Integer getProductType() {
		return productType;
	}

	public void setProductType(Integer productType) {
		this.productType = productType;
	}

	public CountryResponse getManufacturerCountry() {
		return manufacturerCountry;
	}

	public void setManufacturerCountry(CountryResponse manufacturerCountry) {
		this.manufacturerCountry = manufacturerCountry;
	}

	public ProvinceResponse getManufacturerProvince() {
		return manufacturerProvince;
	}

	public void setManufacturerProvince(ProvinceResponse manufacturerProvince) {
		this.manufacturerProvince = manufacturerProvince;
	}

}

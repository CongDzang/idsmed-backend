package com.cmg.idsmed.dto.task;


import java.util.ArrayList;
import java.util.List;

public class IdsmedTaskListResponse {

	private Integer totalPage = null;
	private List<IdsmedTaskResponse> tasks =  new ArrayList<>();
	private Integer totalCount = null;
	private Integer pageNo = null;
	private Integer pageSize = null;

	public IdsmedTaskListResponse(Integer pageNo, Integer pageSize) {
		this.totalPage = 0;
		this.totalCount = 0;
		this.pageSize = pageSize;
		this.pageNo = pageNo;
	}

	public IdsmedTaskListResponse(Integer totalCount, Integer pageIndex, Integer pageSize, List<IdsmedTaskResponse> tasks) {
		if (pageSize != null && !pageSize.equals(0)) {
			Integer calTotalPage = totalCount/pageSize;
			if(totalCount % pageSize > 0) {
				calTotalPage++;
			}
			this.totalPage = calTotalPage;
		}
		this.totalCount = totalCount;
		this.pageNo = pageIndex + 1;
		this.pageSize = pageSize;
		this.tasks = tasks;
	}

	public IdsmedTaskListResponse(Integer totalCount, List<IdsmedTaskResponse> tasks) {
		this.totalCount = totalCount;
		this.tasks = tasks;
		this.totalPage = 1;
		this.pageSize = totalCount;
		this.pageNo = 1;
	}

	public Integer getTotalPage() {
		return totalPage;
	}

	public void setTotalPage(Integer totalPage) {
		this.totalPage = totalPage;
	}

	public List<IdsmedTaskResponse> getTasks() {
		return tasks;
	}

	public void setTasks(List<IdsmedTaskResponse> tasks) {
		this.tasks = tasks;
	}

	public Integer getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}

	public Integer getPageNo() {
		return pageNo;
	}

	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}
}

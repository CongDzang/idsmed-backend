package com.cmg.idsmed.dto.task;

import com.cmg.idsmed.dto.AbstractResponse;
import com.cmg.idsmed.model.entity.task.IdsmedTask;
import com.cmg.idsmed.model.entity.task.IdsmedTaskExecutor;
import org.springframework.util.CollectionUtils;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class IdsmedTaskResponse extends AbstractResponse {

	private Long id;

	private String actionLink;

	private String taskCode;

	private Integer taskTypeCode;

	private Long entityId;

	private String doneBy;

	private LocalDateTime createdDate;

	private String description;

	private List<String> executorLoginId;

	public IdsmedTaskResponse(IdsmedTask task) {
		if (task != null) {
			this.id = task.getId();
			this.actionLink = task.getActionLink();
			this.taskTypeCode = task.getTaskTypeCode();
			this.entityId = task.getEntityId();
			this.doneBy = task.getDoneBy();
			List<IdsmedTaskExecutor> executors = task.getTaskExecutors();
			if (!CollectionUtils.isEmpty(executors))
			this.executorLoginId = executors.stream().map(e -> e.getIdsmedAccount().getLoginId()).collect(Collectors.toList());
			this.createdDate = task.getCreatedDate();
			this.description = task.getDescription();
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getActionLink() {
		return actionLink;
	}

	public void setActionLink(String actionLink) {
		this.actionLink = actionLink;
	}

	public String getTaskCode() {
		return taskCode;
	}

	public void setTaskCode(String taskCode) {
		this.taskCode = taskCode;
	}

	public Integer getTaskTypeCode() {
		return taskTypeCode;
	}

	public void setTaskTypeCode(Integer taskTypeCode) {
		this.taskTypeCode = taskTypeCode;
	}

	public Long getEntityId() {
		return entityId;
	}

	public void setEntityId(Long entityId) {
		this.entityId = entityId;
	}

	public String getDoneBy() {
		return doneBy;
	}

	public void setDoneBy(String doneBy) {
		this.doneBy = doneBy;
	}

	public List<String> getExecutorLoginId() {
		return executorLoginId;
	}

	public void setExecutorLoginId(List<String> executorLoginId) {
		this.executorLoginId = executorLoginId;
	}

	public LocalDateTime getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(LocalDateTime createdDate) {
		this.createdDate = createdDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}

package com.cmg.idsmed.dto.thirdparty;

import com.cmg.idsmed.dto.AbstractRequest;
import com.cmg.idsmed.dto.product.ProductDocumentAttachmentRequest;
import com.cmg.idsmed.dto.product.ProductFeatureRequest;
import com.cmg.idsmed.dto.product.ProductTechnicalRequest;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

/**
 * @author Chong Sian
 */
public class CustomerRequest{

	private Long id;

	private Long customerId;

	@Size(max = 11)
	private String mobile;

	@Size(max = 32)
	private String nickName;

	@Size(max = 32)
	private String realName;

	private Integer sex;

	@Size(max = 16)
	private String birthDate;

	@Size(max = 64)
	private String email;

	@Size(max = 255)
	private String headPic;

	@Size(max = 32)
	private String idCardNo;

	@Size(max = 64)
	private String companyName;

	private Integer companyType;

	@Size(max = 32)
	private String contactName;

	@Size(max = 32)
	private String contactTel;

	@Size(max = 32)
	private String telephone;

	@Size(max = 16)
	private String provinceCode;

	@Size(max = 16)
	private String cityCode;

	@Size(max = 16)
	private String districtCode;

	@Size(max = 16)
	private String provinceName;

	@Size(max = 16)
	private String cityName;

	@Size(max = 16)
	private String districtName;
	
	private Integer status;

	private Date createdDate;

	private Date updatedDate;

	@Size(max = 128)
	private String address;

	public Long getId() { return id; }

	public void setId(Long id) { this.id = id; }

	public Long getCustomerId() { return customerId; }

	public void setCustomerId(Long customerId) { this.customerId = customerId; }

	public String getMobile() { return mobile; }

	public void setMobile(String mobile) { this.mobile = mobile; }

	public String getNickName() { return nickName; }

	public void setNickName(String nickName) { this.nickName = nickName; }

	public String getRealName() { return realName; }

	public void setRealName(String realName) { this.realName = realName; }

	public Integer getSex() { return sex; }

	public void setSex(Integer sex) { this.sex = sex; }

	public String getBirthDate() { return birthDate; }

	public void setBirthDate(String birthDate) { this.birthDate = birthDate; }

	public String getEmail() { return email; }

	public void setEmail(String email) { this.email = email; }

	public String getHeadPic() { return headPic; }

	public void setHeadPic(String headPic) { this.headPic = headPic; }

	public String getIdCardNo() { return idCardNo; }

	public void setIdCardNo(String idCardNo) { this.idCardNo = idCardNo; }

	public String getCompanyName() { return companyName; }

	public void setCompanyName(String companyName) { this.companyName = companyName; }

	public Integer getCompanyType() { return companyType; }

	public void setCompanyType(Integer companyType) { this.companyType = companyType; }

	public String getContactName() { return contactName; }

	public void setContactName(String contactName) { this.contactName = contactName; }

	public String getContactTel() { return contactTel; }

	public void setContactTel(String contactTel) { this.contactTel = contactTel; }

	public String getTelephone() { return telephone; }

	public void setTelephone(String telephone) { this.telephone = telephone; }

	public String getProvinceCode() { return provinceCode; }

	public void setProvinceCode(String provinceCode) { this.provinceCode = provinceCode; }

	public String getCityCode() { return cityCode; }

	public void setCityCode(String cityCode) { this.cityCode = cityCode; }

	public String getDistrictCode() { return districtCode; }

	public void setDistrictCode(String districtCode) { this.districtCode = districtCode; }

	public String getProvinceName() { return provinceName; }

	public void setProvinceName(String provinceName) { this.provinceName = provinceName; }

	public String getCityName() { return cityName; }

	public void setCityName(String cityName) { this.cityName = cityName; }

	public String getDistrictName() { return districtName; }

	public void setDistrictName(String districtName) { this.districtName = districtName; }

	public Integer getStatus() { return status; }

	public void setStatus(Integer status) { this.status = status; }

	public Date getCreatedDate() { return createdDate; }

	public void setCreatedDate(Date createdDate) { this.createdDate = createdDate; }

	public Date getUpdatedDate() { return updatedDate; }

	public void setUpdatedDate(Date updatedDate) { this.updatedDate = updatedDate; }

	public String getAddress() { return address; }

	public void setAddress(String address) { this.address = address; }
}

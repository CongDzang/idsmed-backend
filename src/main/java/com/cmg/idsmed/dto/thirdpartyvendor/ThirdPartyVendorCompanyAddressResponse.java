package com.cmg.idsmed.dto.thirdpartyvendor;

public class ThirdPartyVendorCompanyAddressResponse {
    private String street;

    private String zipCode;

    private String countryRegionId;

    private String state;

    public ThirdPartyVendorCompanyAddressResponse(String street, String zipCode, String countryRegionId, String state) {
        this.street = street;
        this.zipCode = zipCode;
        this.countryRegionId = countryRegionId;
        this.state = state;
    }

    public String getStreet() { return street; }

    public void setStreet(String street) { this.street = street; }

    public String getZipCode() { return zipCode; }

    public void setZipCode(String zipCode) { this.zipCode = zipCode; }

    public String getCountryRegionId() { return countryRegionId; }

    public void setCountryRegionId(String countryRegionId) { this.countryRegionId = countryRegionId; }

    public String getState() { return state; }

    public void setState(String state) { this.state = state; }
}

package com.cmg.idsmed.dto.thirdpartyvendor;

public class ThirdPartyVendorMainContactResponse {
    private String middleName;

    private String phone;

    private String email;

    public ThirdPartyVendorMainContactResponse(String middleName, String phone, String email) {
        this.middleName = middleName;
        this.phone = phone;
        this.email = email;
    }

    public String getMiddleName() { return middleName; }

    public void setMiddleName(String middleName) { this.middleName = middleName; }

    public String getPhone() { return phone; }

    public void setPhone(String phone) { this.phone = phone; }

    public String getEmail() { return email; }

    public void setEmail(String email) { this.email = email; }
}

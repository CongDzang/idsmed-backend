package com.cmg.idsmed.dto.thirdpartyvendor;

import java.util.List;

public class ThirdPartyVendorResponse {

    private String accountNum;

    private String custGroup;

    private String currency;

    private String identificationNumber;

    private String name;

    private String nameKana;

    private String phone;

    private String email;

    private Integer partyCountry;

    private Integer tmyCustCategory;

    private List<ThirdPartyVendorCompanyAddressResponse> companyAddress;

    private List<ThirdPartyVendorBillingAddressResponse> billingAddress;

    private List<ThirdPartyVendorMainContactResponse> mainContact;

    private List<ThirdPartyVendorSecondContactResponse> secondContact;

    public ThirdPartyVendorResponse(String accountNum, String custGroup, String currency,
                                    String identificationNumber, String name, String nameKana,
                                    String phone, String email, Integer partyCountry,
                                    Integer tmyCustCategory,
                                    List<ThirdPartyVendorCompanyAddressResponse> companyAddress,
                                    List<ThirdPartyVendorBillingAddressResponse> billingAddress,
                                    List<ThirdPartyVendorMainContactResponse> mainContact,
                                    List<ThirdPartyVendorSecondContactResponse> secondContact) {
        this.accountNum = accountNum;
        this.custGroup = custGroup;
        this.currency = currency;
        this.identificationNumber = identificationNumber;
        this.name = name;
        this.nameKana = nameKana;
        this.phone = phone;
        this.email = email;
        this.partyCountry = partyCountry;
        this.tmyCustCategory = tmyCustCategory;
        this.companyAddress = companyAddress;
        this.billingAddress = billingAddress;
        this.mainContact = mainContact;
        this.secondContact = secondContact;
    }

    public String getAccountNum() { return accountNum; }

    public void setAccountNum(String accountNum) { this.accountNum = accountNum; }

    public String getCustGroup() { return custGroup; }

    public void setCustGroup(String custGroup) { this.custGroup = custGroup; }

    public String getCurrency() { return currency; }

    public void setCurrency(String currency) { this.currency = currency; }

    public String getIdentificationNumber() { return identificationNumber; }

    public void setIdentificationNumber(String identificationNumber) { this.identificationNumber = identificationNumber; }

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public String getNameKana() { return nameKana; }

    public void setNameKana(String nameKana) { this.nameKana = nameKana; }

    public String getPhone() { return phone; }

    public void setPhone(String phone) { this.phone = phone; }

    public String getEmail() { return email; }

    public void setEmail(String email) { this.email = email; }

    public Integer getPartyCountry() { return partyCountry; }

    public void setPartyCountry(Integer partyCountry) { this.partyCountry = partyCountry; }

    public Integer getTmyCustCategory() { return tmyCustCategory; }

    public void setTmyCustCategory(Integer tmyCustCategory) { this.tmyCustCategory = tmyCustCategory; }

    public List<ThirdPartyVendorCompanyAddressResponse> getCompanyAddress() { return companyAddress; }

    public void setCompanyAddress(List<ThirdPartyVendorCompanyAddressResponse> companyAddress) { this.companyAddress = companyAddress; }

    public List<ThirdPartyVendorBillingAddressResponse> getBillingAddress() { return billingAddress; }

    public void setBillingAddress(List<ThirdPartyVendorBillingAddressResponse> billingAddress) { this.billingAddress = billingAddress; }

    public List<ThirdPartyVendorMainContactResponse> getMainContact() { return mainContact; }

    public void setMainContact(List<ThirdPartyVendorMainContactResponse> mainContact) { this.mainContact = mainContact; }

    public List<ThirdPartyVendorSecondContactResponse> getSecondContact() { return secondContact; }

    public void setSecondContact(List<ThirdPartyVendorSecondContactResponse> secondContact) { this.secondContact = secondContact; }

}

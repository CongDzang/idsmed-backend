package com.cmg.idsmed.dto.user;

import com.cmg.idsmed.dto.corporate.CorporateDepartmentResponse;
import com.cmg.idsmed.dto.corporate.CorporateResponse;

import java.util.ArrayList;
import java.util.List;

public class IdsmedCorporateUserResponse {

	private IdsmedUserResponse idsmedUserResponse;
	private CorporateResponse corporateResponse;
	private List<CorporateDepartmentResponse> corporateDepartmentResponses = new ArrayList<>();

	public IdsmedUserResponse getIdsmedUserResponse() {
		return idsmedUserResponse;
	}

	public void setIdsmedUserResponse(IdsmedUserResponse idsmedUserResponse) {
		this.idsmedUserResponse = idsmedUserResponse;
	}

	public CorporateResponse getCorporateResponse() {
		return corporateResponse;
	}

	public void setCorporateResponse(CorporateResponse corporateResponse) {
		this.corporateResponse = corporateResponse;
	}

	public List<CorporateDepartmentResponse> getCorporateDepartmentResponses() {
		return corporateDepartmentResponses;
	}

	public void setCorporateDepartmentResponses(List<CorporateDepartmentResponse> corporateDepartmentResponses) {
		this.corporateDepartmentResponses = corporateDepartmentResponses;
	}
}

package com.cmg.idsmed.dto.user;

import com.cmg.idsmed.dto.hospital.HospitalDepartmentResponse;
import com.cmg.idsmed.dto.hospital.HospitalResponse;

import java.util.ArrayList;
import java.util.List;

public class IdsmedHospitalUserResponse {
	private IdsmedUserResponse idsmedUserResponse;
	private HospitalResponse hospitalResponse;
	private List<HospitalDepartmentResponse> hospitalDepartmentResponse = new ArrayList<>();

	public IdsmedUserResponse getIdsmedUserResponse() {
		return idsmedUserResponse;
	}

	public void setIdsmedUserResponse(IdsmedUserResponse idsmedUserResponse) {
		this.idsmedUserResponse = idsmedUserResponse;
	}

	public HospitalResponse getHospitalResponse() {
		return hospitalResponse;
	}

	public void setHospitalResponse(HospitalResponse hospitalResponse) {
		this.hospitalResponse = hospitalResponse;
	}

	public List<HospitalDepartmentResponse> getHospitalDepartmentResponse() {
		return hospitalDepartmentResponse;
	}

	public void setHospitalDepartmentResponse(List<HospitalDepartmentResponse> hospitalDepartmentResponse) {
		this.hospitalDepartmentResponse = hospitalDepartmentResponse;
	}
}

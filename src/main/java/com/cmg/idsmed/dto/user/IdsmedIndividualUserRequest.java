package com.cmg.idsmed.dto.user;


import javax.validation.constraints.NotNull;

public class IdsmedIndividualUserRequest extends UserRegistrationRequest {

	@NotNull
	private Long countryId;

	@NotNull
	private Long provinceId;

	@NotNull
	private String firstAddress;

	private String secondAddress;

	private String postcode;

	public Long getCountryId() {
		return countryId;
	}

	public void setCountryId(Long countryId) {
		this.countryId = countryId;
	}

	public Long getProvinceId() {
		return provinceId;
	}

	public void setProvinceId(Long provinceId) {
		this.provinceId = provinceId;
	}

	public String getFirstAddress() {
		return firstAddress;
	}

	public void setFirstAddress(String firstAddress) {
		this.firstAddress = firstAddress;
	}

	public String getSecondAddress() {
		return secondAddress;
	}

	public void setSecondAddress(String secondAddress) {
		this.secondAddress = secondAddress;
	}

	public String getPostcode() {
		return postcode;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}
}

package com.cmg.idsmed.dto.user;

import com.cmg.idsmed.dto.masterdata.CountryResponse;
import com.cmg.idsmed.dto.masterdata.ProvinceResponse;
import com.cmg.idsmed.model.entity.auth.IdsmedUser;
import com.cmg.idsmed.model.entity.auth.UserAddress;
import com.cmg.idsmed.model.entity.masterdata.Country;
import com.cmg.idsmed.model.entity.masterdata.Province;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class IdsmedIndividualUserResponse extends IdsmedUserResponse {
	private List<UserAddressResponse> userAddressResponses = new ArrayList<>();

	public IdsmedIndividualUserResponse() {

	}

	public IdsmedIndividualUserResponse(IdsmedUser user, String ossBucketDomain, List<UserAddress> userAddresses) {
		super(user, ossBucketDomain);
		if (!CollectionUtils.isEmpty(userAddresses)) {
			this.userAddressResponses = userAddresses.stream().map(ua -> new UserAddressResponse(ua)).collect(Collectors.toList());
		}
	}

	public List<UserAddressResponse> getUserAddressResponses() {
		return userAddressResponses;
	}

	public void setUserAddressResponses(List<UserAddressResponse> userAddressResponses) {
		this.userAddressResponses = userAddressResponses;
	}
}

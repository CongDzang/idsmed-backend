package com.cmg.idsmed.dto.user;

import java.util.List;

public class IdsmedUserListResponse {
	private Integer totalPage = null;
	private List<IdsmedUserResponse> userList;
	private Integer totalCount = null;
	private Integer pageNo = null;
	private Integer pageSize = null;

	public IdsmedUserListResponse(Integer pageNo, Integer pageSize) {
		this.totalPage = 0;
		this.totalCount = 0;
		this.pageSize = pageSize;
		this.pageNo = pageNo;
	}
	
	public IdsmedUserListResponse(Integer totalCount, Integer pageIndex, Integer pageSize, List<IdsmedUserResponse> userList) {
		if (pageSize != null && !pageSize.equals(0)) {
			Integer calTotalPage = totalCount/pageSize;
			if(totalCount % pageSize > 0) {
				calTotalPage++;
			}
			this.totalPage = calTotalPage;
		}
		this.totalCount = totalCount;
		this.pageNo = pageIndex + 1;
		this.pageSize = pageSize;
		this.userList = userList;
	}

	public IdsmedUserListResponse(Integer totalCount, List<IdsmedUserResponse> userList) {
		this.totalCount = totalCount;
		this.userList = userList;
		this.totalPage = 1;
		this.pageSize = totalCount;
		this.pageNo = 1;
	}

	public Integer getTotalPage() {
		return totalPage;
	}

	public void setTotalPage(Integer totalPage) {
		this.totalPage = totalPage;
	}

	public List<IdsmedUserResponse> getUserList() {
		return userList;
	}

	public void setProductList(List<IdsmedUserResponse> userList) {
		this.userList = userList;
	}

	public Integer getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}

	public Integer getPageNo() {
		return pageNo;
	}

	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}
}

package com.cmg.idsmed.dto.user;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.cmg.idsmed.dto.AbstractRequest;

public class IdsmedUserRequest extends AbstractRequest {

    private Long id;

    @NotNull
    private String userLoginId;

    @NotNull
    @Size(min = 6, max = 50)
    private String password;

    @Size(max = 100)
    private String firstName;

    @Size(max = 200)
    private String lastName;

    @Size(max = 200)
    private String email;

    @Size(max = 50)
    private String countryCode;
    
    @Size(max = 100)
    private String phoneNumber;
    
    @Size(max = 100)
    private String companyCode;
    
    @Size(max = 100)
    private String wechat;
    
    @Size(max = 100)
    private String qq;

    @Size(max = 300)
    private String rejectReason;
    
	private Integer status = 1;

	private Date approvedDate;

    private Long roleId;

    private Boolean vendorCoordinator;

    private List<Integer> accountEmailNotificationPreferences;

    private List<Long> careAreaId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserLoginId() {
        return userLoginId;
    }

    public void setUserLoginId(String userLoginId) {
        this.userLoginId = userLoginId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	public String getWechat() {
		return wechat;
	}

	public void setWechat(String wechat) {
		this.wechat = wechat;
	}

	public String getQq() {
		return qq;
	}

	public void setQq(String qq) {
		this.qq = qq;
	}
	
	public String getRejectReason() {
		return rejectReason;
	}

	public void setRejectReason(String rejectReason) {
		this.rejectReason = rejectReason;
	}

	public Date getApprovedDate() {
		return approvedDate;
	}

	public void setApprovedDate(Date approvedDate) {
		this.approvedDate = approvedDate;
	}

    public Boolean getVendorCoordinator() { return vendorCoordinator; }

    public void setVendorCoordinator(Boolean vendorCoordinator) { this.vendorCoordinator = vendorCoordinator; }

    public List<Integer> getAccountEmailNotificationPreferences() { return accountEmailNotificationPreferences; }

    public void setAccountEmailNotificationPreferences(List<Integer> accountEmailNotificationPreferences) { this.accountEmailNotificationPreferences = accountEmailNotificationPreferences; }

    public List<Long> getCareAreaId() { return careAreaId; }

    public void setCareAreaId(List<Long> careAreaId) { this.careAreaId = careAreaId; }
}

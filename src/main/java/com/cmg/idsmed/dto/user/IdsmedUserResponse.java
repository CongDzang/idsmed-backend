package com.cmg.idsmed.dto.user;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import com.cmg.idsmed.model.entity.auth.IdsmedAccount;
import org.springframework.beans.BeanUtils;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.util.CollectionUtils;

import com.cmg.idsmed.common.enums.CountryCodeEnum;
import com.cmg.idsmed.dto.AbstractResponse;
import com.cmg.idsmed.dto.auth.IdsmedRoleResponse;
import com.cmg.idsmed.model.entity.auth.IdsmedRole;
import com.cmg.idsmed.model.entity.auth.IdsmedUser;
import org.springframework.util.StringUtils;

import javax.persistence.Column;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

public class IdsmedUserResponse extends AbstractResponse {

    //We prefer sequence generator than identity. If you use identity it may disable JDBC batching

    private Long id;

    private String userLoginId;

    private String password;

    private String firstName;

    private String lastName;

    private String email;

    private String countryCode;
    
    private String rejectReason;
    
    private String companyCode;
    
    private String qq;
    
    private String wechat;
    
    private String phoneNumber;

	private int status = 1;
	
	private String companyPrimaryName;
	
	private Integer approvedBy;
	
	private Date approvedDate;

	private String profileImageUrl;

	private Integer previousStatus;

	private String inactiveReason;

    private List<IdsmedRoleResponse> roles;
    
    private String currentLangCode;

    private Boolean vendorCoordinator;

    private List<Integer> accountEmailNotificationPreferencesIdList;

    private List<Long> tenderCategoryIdList;

	private Integer gender;

	private Date birthday;

	private String mobilePhoneNumber;

	private String idCardNumber;

	private String idCardFrontPic;

	private String idCardBackPic;

	private String title;

	private Integer typeOfUser;

	public IdsmedUserResponse() {
    }

	public IdsmedUserResponse(IdsmedUser user, String ossBucketDomain) {
		if(user != null) {
			String [] ignoreProperties = {"profileImageUrl","idCardFrontPic", "idCardBackPic"};
			BeanUtils.copyProperties(user, this, ignoreProperties);

			if (!StringUtils.isEmpty(user.getProfileImageUrl()) && !StringUtils.isEmpty(ossBucketDomain)) {
				this.profileImageUrl = ossBucketDomain + user.getProfileImageUrl();
			}

			if (!StringUtils.isEmpty(user.getIdCardFrontPic()) && !StringUtils.isEmpty(ossBucketDomain)) {
				this.idCardFrontPic = ossBucketDomain + user.getIdCardFrontPic();
			}

			if (!StringUtils.isEmpty(user.getIdCardBackPic()) && !StringUtils.isEmpty(ossBucketDomain)) {
				this.idCardBackPic = ossBucketDomain + user.getIdCardBackPic();
			}
			this.userLoginId = CollectionUtils.isEmpty(user.getIdsmedAccounts()) ? "" : user.getIdsmedAccounts().get(0).getLoginId();
			this.currentLangCode = user.getIdsmedAccounts().get(0).getAccountSettings() == null ?
					CountryCodeEnum.CHINA.getCode() : user.getIdsmedAccounts().get(0).getAccountSettings().getCurrentLangCode();
		}
	}

	public IdsmedUserResponse(IdsmedUser user, List<IdsmedRole> roles, String ossBucketDomain) {
		String [] ignoreProperties = {"profileImageUrl","idCardFrontPic", "idCardBackPic"};
		BeanUtils.copyProperties(user, this, ignoreProperties);
		if (!StringUtils.isEmpty(user.getProfileImageUrl()) && !StringUtils.isEmpty(ossBucketDomain)) {
			this.profileImageUrl = ossBucketDomain + user.getProfileImageUrl();
		}

		if (!StringUtils.isEmpty(user.getIdCardFrontPic()) && !StringUtils.isEmpty(ossBucketDomain)) {
			this.idCardFrontPic = ossBucketDomain + user.getIdCardFrontPic();
		}

		if (!StringUtils.isEmpty(user.getIdCardBackPic()) && !StringUtils.isEmpty(ossBucketDomain)) {
			this.idCardBackPic = ossBucketDomain + user.getIdCardBackPic();
		}

		this.userLoginId = CollectionUtils.isEmpty(user.getIdsmedAccounts()) ? "" : user.getIdsmedAccounts().get(0).getLoginId();
		this.roles = roles.stream().map(ir -> new IdsmedRoleResponse(ir)).collect(Collectors.toList());
		this.currentLangCode = user.getIdsmedAccounts().get(0).getAccountSettings() == null ? CountryCodeEnum.CHINA.getCode() : user.getIdsmedAccounts().get(0).getAccountSettings().getCurrentLangCode();
	}


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserLoginId() {
        return userLoginId;
    }

    public void setUserLoginId(String userLoginId) {
        this.userLoginId = userLoginId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
    
    public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	public String getQq() {
		return qq;
	}

	public void setQq(String qq) {
		this.qq = qq;
	}

	public String getWechat() {
		return wechat;
	}

	public void setWechat(String wechat) {
		this.wechat = wechat;
	}
	
	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

    public List<IdsmedRoleResponse> getRoles() {
        return roles;
    }

    public void setRoles(List<IdsmedRoleResponse> roles) {
        this.roles = roles;
    }
    
    public String getRejectReason() {
		return rejectReason;
	}

	public void setRejectReason(String rejectReason) {
		this.rejectReason = rejectReason;
	}

	public String getCompanyPrimaryName() {
		return companyPrimaryName;
	}

	public void setCompanyPrimaryName(String companyPrimaryName) {
		this.companyPrimaryName = companyPrimaryName;
	}

	public Integer getApprovedBy() {
		return approvedBy;
	}

	public void setApprovedBy(Integer approvedBy) {
		this.approvedBy = approvedBy;
	}

	public Date getApprovedDate() {
		return approvedDate;
	}

	public void setApprovedDate(Date approvedDate) {
		this.approvedDate = approvedDate;
	}

	public String getProfileImageUrl() {
		return profileImageUrl;
	}

	public void setProfileImageUrl(String profileImageUrl) {
		this.profileImageUrl = profileImageUrl;
	}

    public Integer getPreviousStatus() {
        return previousStatus;
    }

    public void setPreviousStatus(Integer previousStatus) {
        this.previousStatus = previousStatus;
    }

    public String getCurrentLangCode() {
		return currentLangCode;
	}

	public void setCurrentLangCode(String currentLangCode) {
		this.currentLangCode = currentLangCode;
	}

    public Boolean getVendorCoordinator() { return vendorCoordinator; }

    public void setVendorCoordinator(Boolean vendorCoordinator) { this.vendorCoordinator = vendorCoordinator; }

    public List<Integer> getAccountEmailNotificationPreferencesIdList() { return accountEmailNotificationPreferencesIdList; }

    public void setAccountEmailNotificationPreferencesIdList(List<Integer> accountEmailNotificationPreferencesIdList) { this.accountEmailNotificationPreferencesIdList = accountEmailNotificationPreferencesIdList; }

    public List<Long> getTenderCategoryIdList() { return tenderCategoryIdList; }

    public void setTenderCategoryIdList(List<Long> tenderCategoryIdList) { this.tenderCategoryIdList = tenderCategoryIdList; }

	public Integer getGender() {
		return gender;
	}

	public void setGender(Integer gender) {
		this.gender = gender;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public String getMobilePhoneNumber() {
		return mobilePhoneNumber;
	}

	public void setMobilePhoneNumber(String mobilePhoneNumber) {
		this.mobilePhoneNumber = mobilePhoneNumber;
	}

	public String getIdCardNumber() {
		return idCardNumber;
	}

	public void setIdCardNumber(String idCardNumber) {
		this.idCardNumber = idCardNumber;
	}

	public String getIdCardFrontPic() {
		return idCardFrontPic;
	}

	public void setIdCardFrontPic(String idCardFrontPic) {
		this.idCardFrontPic = idCardFrontPic;
	}

	public String getIdCardBackPic() {
		return idCardBackPic;
	}

	public void setIdCardBackPic(String idCardBackPic) {
		this.idCardBackPic = idCardBackPic;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Integer getTypeOfUser() {
		return typeOfUser;
	}

	public void setTypeOfUser(Integer typeOfUser) {
		this.typeOfUser = typeOfUser;
	}

	public String getInactiveReason() {
		return inactiveReason;
	}

	public void setInactiveReason(String inactiveReason) {
		this.inactiveReason = inactiveReason;
	}
}

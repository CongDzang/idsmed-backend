package com.cmg.idsmed.dto.user;

import com.cmg.idsmed.dto.masterdata.CountryResponse;
import com.cmg.idsmed.dto.masterdata.ProvinceResponse;
import com.cmg.idsmed.model.entity.auth.UserAddress;

import java.time.LocalDateTime;

public class UserAddressResponse {
	private Long id;

	private String firstAddress;

	private String secondAddress;

	private String thirdAddress;

	private String postcode;

	private LocalDateTime fromDate;

	private LocalDateTime toDate;

	private CountryResponse country;

	private ProvinceResponse province;

	public UserAddressResponse() {

	}

	public UserAddressResponse(UserAddress userAddress) {
		if(userAddress != null) {
			this.id = userAddress.getId();

			this.firstAddress = userAddress.getFirstAddress();

			this.secondAddress = userAddress.getSecondAddress();

			this.thirdAddress = userAddress.getThirdAddress();

			this.postcode = userAddress.getPostcode();

			if (userAddress.getCountry() != null) {
				this.country = new CountryResponse(userAddress.getCountry());
			}

			if (userAddress.getProvince() != null) {
				this.province = new ProvinceResponse(userAddress.getProvince());
			}
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstAddress() {
		return firstAddress;
	}

	public void setFirstAddress(String firstAddress) {
		this.firstAddress = firstAddress;
	}

	public String getSecondAddress() {
		return secondAddress;
	}

	public void setSecondAddress(String secondAddress) {
		this.secondAddress = secondAddress;
	}

	public String getThirdAddress() {
		return thirdAddress;
	}

	public void setThirdAddress(String thirdAddress) {
		this.thirdAddress = thirdAddress;
	}

	public String getPostcode() {
		return postcode;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	public LocalDateTime getFromDate() {
		return fromDate;
	}

	public void setFromDate(LocalDateTime fromDate) {
		this.fromDate = fromDate;
	}

	public LocalDateTime getToDate() {
		return toDate;
	}

	public void setToDate(LocalDateTime toDate) {
		this.toDate = toDate;
	}

	public CountryResponse getCountry() {
		return country;
	}

	public void setCountry(CountryResponse country) {
		this.country = country;
	}

	public ProvinceResponse getProvince() {
		return province;
	}

	public void setProvince(ProvinceResponse province) {
		this.province = province;
	}
}

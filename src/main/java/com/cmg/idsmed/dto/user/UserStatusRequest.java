package com.cmg.idsmed.dto.user;

import com.cmg.idsmed.dto.AbstractRequest;

public class UserStatusRequest extends AbstractRequest {
    private Long userId;
    private String inactiveReason;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getInactiveReason() { return inactiveReason; }

    public void setInactiveReason(String inactiveReason) { this.inactiveReason = inactiveReason; }
}

package com.cmg.idsmed.dto.user;

import com.cmg.idsmed.dto.AbstractRequest;

import javax.validation.constraints.NotNull;

public class UserUpdateRoleSimpleRequest  extends AbstractRequest {
	@NotNull
	private Long userId;

	@NotNull
	private Long roleId;

	@NotNull
	private Boolean vendorCoordinator;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	public Boolean getVendorCoordinator() { return vendorCoordinator; }

	public void setVendorCoordinator(Boolean vendorCoordinator) { this.vendorCoordinator = vendorCoordinator; }
}

package com.cmg.idsmed.dto.vendor;

import com.cmg.idsmed.model.entity.vendor.VendorFileAttachmentReminder;
import org.springframework.beans.BeanUtils;

import com.cmg.idsmed.model.entity.vendor.FileAttachment;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class FileAttachmentResponse {
	private Long id;

	private String documentName;

    private String documentType;

    private String documentPath;

    private String remarks;

    private String countryCode;

    private Boolean isVerify;

	private LocalDateTime expiryDate;

	private Integer status;

	private List<VendorFileAttachmentReminderResponse> reminders = new ArrayList<>();

	public FileAttachmentResponse(FileAttachment fileAttachment, String ossBucketDomain) {

		if (fileAttachment != null) {
			this.id = fileAttachment.getId();
			this.documentName = fileAttachment.getDocumentName();
			this.documentType = fileAttachment.getDocumentType();
			if (!StringUtils.isEmpty(ossBucketDomain)) {
				this.documentPath = ossBucketDomain + fileAttachment.getDocumentPath();
			}
			this.remarks = fileAttachment.getRemarks();
			this.countryCode = fileAttachment.getCountryCode();
			this.expiryDate = fileAttachment.getExpiryDate();
			this.isVerify = fileAttachment.getVerify();
			this.status = fileAttachment.getStatus();
		}

		List<VendorFileAttachmentReminder> reminders = fileAttachment.getVendorFileAttachmentReminders();
		if (!CollectionUtils.isEmpty(reminders)) {
			List<VendorFileAttachmentReminderResponse> reminderResponses= reminders
					.stream()
					.map(r -> new VendorFileAttachmentReminderResponse(r))
					.collect(Collectors.toList());
			this.reminders = reminderResponses;
		}
	}
    
    public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDocumentName() {
		return documentName;
	}

	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}

	public String getDocumentType() {
		return documentType;
	}

	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}

	public String getDocumentPath() {
		return documentPath;
	}

	public void setDocumentPath(String documentPath) {
		this.documentPath = documentPath;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public Boolean getVerify() { return isVerify; }

	public void setVerify(Boolean verify) { isVerify = verify; }

	public LocalDateTime getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(LocalDateTime expiryDate) {
		this.expiryDate = expiryDate;
	}

	public Integer getStatus() { return status; }

	public void setStatus(Integer status) { this.status = status; }

	public List<VendorFileAttachmentReminderResponse> getReminders() {
		return reminders;
	}

	public void setReminders(List<VendorFileAttachmentReminderResponse> reminders) {
		this.reminders = reminders;
	}
}

package com.cmg.idsmed.dto.vendor;

import com.cmg.idsmed.model.entity.vendor.Vendor;
import com.cmg.idsmed.model.entity.vendor.config.VendorLabelConfig;

import javax.persistence.*;
import javax.validation.constraints.Size;

public class VendorAdditionalInfoRequest {

	private Long id;

	private Long dId = 0L;

	@Size(max = 500)
	private String fieldName;

	@Size(max = 500)
	private String fieldSecondaryName;

	private String  infoValue;

	private Long vendorId;

	private Long vendorLabelConfigId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getdId() {
		return dId;
	}

	public void setdId(Long dId) {
		this.dId = dId;
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public String getFieldSecondaryName() {
		return fieldSecondaryName;
	}

	public void setFieldSecondaryName(String fieldSecondaryName) {
		this.fieldSecondaryName = fieldSecondaryName;
	}

	public String getInfoValue() {
		return infoValue;
	}

	public void setInfoValue(String infoValue) {
		this.infoValue = infoValue;
	}

	public Long getVendorId() {
		return vendorId;
	}

	public void setVendorId(Long vendorId) {
		this.vendorId = vendorId;
	}

	public Long getVendorLabelConfigId() {
		return vendorLabelConfigId;
	}

	public void setVendorLabelConfigId(Long vendorLabelConfigId) {
		this.vendorLabelConfigId = vendorLabelConfigId;
	}
}

package com.cmg.idsmed.dto.vendor;

import com.cmg.idsmed.model.entity.vendor.Vendor;
import com.cmg.idsmed.model.entity.vendor.VendorAdditionalInfo;
import org.springframework.beans.BeanUtils;

import javax.persistence.*;
import javax.validation.constraints.Size;

public class VendorAdditionalInfoResponse {

	private Long id;

	private Long dId;

	@Size(max = 500)
	private String fieldName;

	private String  infoValue;

	public VendorAdditionalInfoResponse(VendorAdditionalInfo vendorAdditionalInfo) {
		BeanUtils.copyProperties(vendorAdditionalInfo, this);
	}
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getdId() {
		return dId;
	}

	public void setdId(Long dId) {
		this.dId = dId;
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public String getInfoValue() {
		return infoValue;
	}

	public void setInfoValue(String infoValue) {
		this.infoValue = infoValue;
	}
}

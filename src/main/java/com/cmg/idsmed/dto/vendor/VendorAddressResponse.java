package com.cmg.idsmed.dto.vendor;

import com.cmg.idsmed.dto.AbstractResponse;
import com.cmg.idsmed.dto.masterdata.CountryResponse;
import com.cmg.idsmed.dto.masterdata.ProvinceResponse;
import com.cmg.idsmed.model.entity.vendor.VendorAddress;
import org.springframework.beans.BeanUtils;

import java.util.Date;

public class VendorAddressResponse extends AbstractResponse {

	private Long id;

	private Integer type;

	private String firstAddress;

	private String secondAddress;

	private String thirdAddress;

	private String postcode;

	private Date fromDate;

	private Date toDate;

	private Long vendorId;

	private CountryResponse country;

	private ProvinceResponse province;

	public VendorAddressResponse() {
	}

	public VendorAddressResponse(VendorAddress address) {
		String [] ignoreProperties = {"country"
				,"province"};
		BeanUtils.copyProperties(address, this, ignoreProperties);
		if (address.getCountry() != null) {
			this.country = new CountryResponse(address);
		}

		if (address.getProvince() != null) {
			this.province = new ProvinceResponse(address.getProvince());
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getFirstAddress() {
		return firstAddress;
	}

	public void setFirstAddress(String firstAddress) {
		this.firstAddress = firstAddress;
	}

	public String getSecondAddress() {
		return secondAddress;
	}

	public void setSecondAddress(String secondAddress) {
		this.secondAddress = secondAddress;
	}

	public String getThirdAddress() {
		return thirdAddress;
	}

	public void setThirdAddress(String thirdAddress) {
		this.thirdAddress = thirdAddress;
	}

	public String getPostcode() {
		return postcode;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public Long getVendorId() {
		return vendorId;
	}

	public void setVendorId(Long vendorId) {
		this.vendorId = vendorId;
	}

	public CountryResponse getCountry() {
		return country;
	}

	public void setCountry(CountryResponse country) {
		this.country = country;
	}

	public ProvinceResponse getProvince() {
		return province;
	}

	public void setProvince(ProvinceResponse province) {
		this.province = province;
	}
}

package com.cmg.idsmed.dto.vendor;

import com.cmg.idsmed.model.entity.vendor.VendorFileAttachmentReminder;

import javax.persistence.*;

public class VendorFileAttachmentReminderResponse {

	private Long id;

	private Integer daysBefore;

	private Integer daysAfter;

	private Boolean inExpiryDate;

	public VendorFileAttachmentReminderResponse() {
	}

	public VendorFileAttachmentReminderResponse(VendorFileAttachmentReminder reminder) {
		if (reminder != null) {
			this.id = reminder.getId();
			this.daysBefore = reminder.getDaysBefore();
			this.daysAfter = reminder.getDaysAfter();
			this.inExpiryDate = reminder.getInExpiryDate();
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getDaysBefore() {
		return daysBefore;
	}

	public void setDaysBefore(Integer daysBefore) {
		this.daysBefore = daysBefore;
	}

	public Integer getDaysAfter() {
		return daysAfter;
	}

	public void setDaysAfter(Integer daysAfter) {
		this.daysAfter = daysAfter;
	}

	public Boolean getInExpiryDate() {
		return inExpiryDate;
	}

	public void setInExpiryDate(Boolean inExpiryDate) {
		this.inExpiryDate = inExpiryDate;
	}
}

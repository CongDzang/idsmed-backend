package com.cmg.idsmed.dto.vendor;

import javax.persistence.Column;
import javax.validation.constraints.Size;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.List;

public class VendorFileAttachmentRequest {
	private Long id;

	private Long vendorId;

	private Integer fileIndex;

	private String documentName;

	@Size(max = 500)
	private String documentSecondaryName;

	private String documentType;

	@Size(max = 1000)
	private String originalFileName;

	@Size(max = 1000)
	private String documentPath;

	@Size(max = 3000)
	private String remarks;

	List<VendorFileAttachmentReminderRequest> vendorFileAttachmentReminders;

	private Long vendorLabelConfigId;

	private Boolean isVerify;

	private ZonedDateTime expiryDate;

	@Column(name = "exp_date_tz")
	private String expDateTimeZone;

	private Integer status;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getVendorId() {
		return vendorId;
	}

	public void setVendorId(Long vendorId) {
		this.vendorId = vendorId;
	}

	public String getDocumentName() {
		return documentName;
	}

	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}

	public String getDocumentType() {
		return documentType;
	}

	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}

	public String getDocumentPath() {
		return documentPath;
	}

	public void setDocumentPath(String documentPath) {
		this.documentPath = documentPath;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Long getVendorLabelConfigId() {
		return vendorLabelConfigId;
	}

	public void setVendorLabelConfigId(Long vendorLabelConfigId) {
		this.vendorLabelConfigId = vendorLabelConfigId;
	}

	public String getOriginalFileName() {
		return originalFileName;
	}

	public void setOriginalFileName(String originalFileName) {
		this.originalFileName = originalFileName;
	}

	public String getDocumentSecondaryName() {
		return documentSecondaryName;
	}

	public void setDocumentSecondaryName(String documentSecondaryName) {
		this.documentSecondaryName = documentSecondaryName;
	}

	public ZonedDateTime getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(ZonedDateTime expiryDate) {
		this.expiryDate = expiryDate;
	}

	public Boolean getVerify() {
		return isVerify;
	}

	public void setVerify(Boolean verify) {
		isVerify = verify;
	}

	public Integer getFileIndex() {
		return fileIndex;
	}

	public void setFileIndex(Integer fileIndex) {
		this.fileIndex = fileIndex;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public List<VendorFileAttachmentReminderRequest> getVendorFileAttachmentReminders() {
		return vendorFileAttachmentReminders;
	}

	public void setVendorFileAttachmentReminders(List<VendorFileAttachmentReminderRequest> vendorFileAttachmentReminders) {
		this.vendorFileAttachmentReminders = vendorFileAttachmentReminders;
	}

	public String getExpDateTimeZone() {
		return expDateTimeZone;
	}

	public void setExpDateTimeZone(String expDateTimeZone) {
		this.expDateTimeZone = expDateTimeZone;
	}
}

package com.cmg.idsmed.dto.vendor;

import com.cmg.idsmed.dto.AbstractRequest;

import java.util.List;

public class VendorFileAttachmentsEditRequest extends AbstractRequest {
	private Long vendorId;
	private List<VendorFileAttachmentRequest> fileAttachments;

	public Long getVendorId() {
		return vendorId;
	}

	public void setVendorId(Long vendorId) {
		this.vendorId = vendorId;
	}

	public List<VendorFileAttachmentRequest> getFileAttachments() {
		return fileAttachments;
	}

	public void setFileAttachments(List<VendorFileAttachmentRequest> fileAttachments) {
		this.fileAttachments = fileAttachments;
	}
}

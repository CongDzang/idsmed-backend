package com.cmg.idsmed.dto.vendor;

import com.cmg.idsmed.dto.product.ProductResponse;

import java.util.ArrayList;
import java.util.List;

public class VendorInfoConfigListResponse {
	private Integer totalCount = 0;
	private Integer pageNo = 0;
	private Integer pageSize = 0;
	private Integer totalPage = 0;
	private List<VendorInfoConfigResponse>  vendorInfoConfigs = new ArrayList<>();

	public VendorInfoConfigListResponse(Integer pageNo, Integer pageSize) {
		this.pageSize = pageSize;
		this.pageNo = pageNo;
	}

	public VendorInfoConfigListResponse() {
	}

	public VendorInfoConfigListResponse(Integer totalCount, Integer pageIndex, Integer pageSize, List<VendorInfoConfigResponse> vendorInfoConfigResponses) {
		if (pageSize != null && !pageSize.equals(0)) {
			Integer calTotalPage = totalCount/pageSize;
			if(totalCount % pageSize > 0) {
				calTotalPage++;
			}
			this.totalPage = calTotalPage;
		}
		this.totalCount = totalCount;

		Integer pageCount = pageSize >= totalCount ? 1 : (totalCount%pageSize > 0 ? totalCount/pageSize +1 : totalCount/pageSize);
		Integer pageNo = pageIndex*pageSize >= totalCount ? pageCount-1 : pageIndex;

		this.pageNo = pageNo+1;
		this.pageSize = pageSize;
		this.vendorInfoConfigs = vendorInfoConfigResponses;
	}

	public VendorInfoConfigListResponse(Integer totalCount, List<VendorInfoConfigResponse> vendorInfoConfigs) {
		this.totalCount = totalCount;
		this.vendorInfoConfigs = vendorInfoConfigs;
		this.totalPage = 1;
		this.pageSize = totalCount;
		this.pageNo = 1;
	}

	public Integer getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}

	public Integer getPageNo() {
		return pageNo;
	}

	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public Integer getTotalPage() {
		return totalPage;
	}

	public void setTotalPage(Integer totalPage) {
		this.totalPage = totalPage;
	}

	public List<VendorInfoConfigResponse> getVendorInfoConfigs() {
		return vendorInfoConfigs;
	}

	public void setVendorInfoConfigs(List<VendorInfoConfigResponse> vendorInfoConfigs) {
		this.vendorInfoConfigs = vendorInfoConfigs;
	}
}

package com.cmg.idsmed.dto.vendor;

import com.cmg.idsmed.dto.AbstractRequest;

import java.util.List;

public class VendorInfoConfigRequest extends AbstractRequest {
	private Long id;
	private Long countryId;
	private Long provinceId;
	private Integer location;
	private Integer vendorType;
	List<VendorLabelConfigRequest> vendorLabelConfigs;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCountryId() {
		return countryId;
	}

	public void setCountryId(Long countryId) {
		this.countryId = countryId;
	}

	public Long getProvinceId() {
		return provinceId;
	}

	public void setProvinceId(Long provinceId) {
		this.provinceId = provinceId;
	}

	public List<VendorLabelConfigRequest> getVendorLabelConfigs() {
		return vendorLabelConfigs;
	}

	public void setVendorLabelConfigs(List<VendorLabelConfigRequest> vendorLabelConfigs) {
		this.vendorLabelConfigs = vendorLabelConfigs;
	}

	public Integer getLocation() {
		return location;
	}

	public void setLocation(Integer location) {
		this.location = location;
	}

	public Integer getVendorType() {
		return vendorType;
	}

	public void setVendorType(Integer vendorType) {
		this.vendorType = vendorType;
	}
}

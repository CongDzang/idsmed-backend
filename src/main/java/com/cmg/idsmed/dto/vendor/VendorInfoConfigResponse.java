package com.cmg.idsmed.dto.vendor;

import com.cmg.idsmed.model.entity.masterdata.Country;
import com.cmg.idsmed.model.entity.masterdata.Province;
import com.cmg.idsmed.model.entity.vendor.config.VendorInfoConfig;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class VendorInfoConfigResponse {
	private Long id;
	private Long countryId;
	private String countryCode;
	private String countryEnglishBriefName;
	private String countryChineseBriefName;
	private Long provinceId;
	private String provinceCode;
	private String provinceEnglishBriefName;
	private String provinceChineseBriefName;
	private Integer status;

	private Integer location;
	private Integer vendorType;

	private List<VendorLabelConfigResponse> vendorInfoLabelConfigs = new ArrayList<>();

	public VendorInfoConfigResponse() {
	}

	public VendorInfoConfigResponse(VendorInfoConfig vendorInfoConfig) {
		if (vendorInfoConfig != null) {
			Country country = vendorInfoConfig.getCountry();
			Province province = vendorInfoConfig.getProvince();

			this.id = vendorInfoConfig.getId();
			this.countryId = country.getId();
			this.countryCode = country.getCountryCode();
			this.countryEnglishBriefName = country.getEnglishBriefName();
			this.countryChineseBriefName = country.getChineseBriefName();
			this.location = vendorInfoConfig.getLocation();
			this.vendorType = vendorInfoConfig.getVendorType();

			if (province != null) {
				this.provinceId = province.getId();
				this.provinceCode = province.getProvinceCode();
				this.provinceEnglishBriefName = province.getEnglishBriefName();
				this.provinceChineseBriefName = province.getChineseBriefName();
			}
			this.status = vendorInfoConfig.getStatus();
			this.vendorInfoLabelConfigs = vendorInfoConfig.getVendorLabelConfigs()
					.stream().map(vlc -> new VendorLabelConfigResponse(vlc)).sorted(Comparator.comparing(VendorLabelConfigResponse::getSequence)).collect(Collectors.toList());
			}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCountryId() {
		return countryId;
	}

	public void setCountryId(Long countryId) {
		this.countryId = countryId;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getCountryEnglishBriefName() {
		return countryEnglishBriefName;
	}

	public void setCountryEnglishBriefName(String countryEnglishBriefName) {
		this.countryEnglishBriefName = countryEnglishBriefName;
	}

	public String getCountryChineseBriefName() {
		return countryChineseBriefName;
	}

	public void setCountryChineseBriefName(String countryChineseBriefName) {
		this.countryChineseBriefName = countryChineseBriefName;
	}

	public Long getProvinceId() {
		return provinceId;
	}

	public void setProvinceId(Long provinceId) {
		this.provinceId = provinceId;
	}

	public String getProvinceCode() {
		return provinceCode;
	}

	public void setProvinceCode(String provinceCode) {
		this.provinceCode = provinceCode;
	}

	public String getProvinceEnglishBriefName() {
		return provinceEnglishBriefName;
	}

	public void setProvinceEnglishBriefName(String provinceEnglishBriefName) {
		this.provinceEnglishBriefName = provinceEnglishBriefName;
	}

	public String getProvinceChineseBriefName() {
		return provinceChineseBriefName;
	}

	public void setProvinceChineseBriefName(String provinceChineseBriefName) {
		this.provinceChineseBriefName = provinceChineseBriefName;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public List<VendorLabelConfigResponse> getVendorInfoLabelConfigs() {
		return vendorInfoLabelConfigs;
	}

	public void setVendorInfoLabelConfigs(List<VendorLabelConfigResponse> vendorInfoLabelConfigs) {
		this.vendorInfoLabelConfigs = vendorInfoLabelConfigs;
	}

	public Integer getLocation() {
		return location;
	}

	public void setLocation(Integer location) {
		this.location = location;
	}

	public Integer getVendorType() {
		return vendorType;
	}

	public void setVendorType(Integer vendorType) {
		this.vendorType = vendorType;
	}
}

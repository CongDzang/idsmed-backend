package com.cmg.idsmed.dto.vendor;


import javax.validation.constraints.Size;

public class VendorLabelConfigRequest {

	private Long id;

	@Size(max = 500)
	private String labelPrimaryName;

	@Size(max = 500)
	private String labelSecondaryName;

	private Boolean isMandatory = false;

	private Boolean isExpired = false;

	private Integer type;

	private Integer sequence;

	private Integer status;

	private Long vendorInfoConfigId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLabelPrimaryName() {
		return labelPrimaryName;
	}

	public void setLabelPrimaryName(String labelPrimaryName) {
		this.labelPrimaryName = labelPrimaryName;
	}

	public String getLabelSecondaryName() {
		return labelSecondaryName;
	}

	public void setLabelSecondaryName(String labelSecondaryName) {
		this.labelSecondaryName = labelSecondaryName;
	}

	public Boolean getMandatory() {
		return isMandatory;
	}

	public void setMandatory(Boolean mandatory) {
		isMandatory = mandatory;
	}

	public Boolean getExpired() {
		return isExpired;
	}

	public void setExpired(Boolean expired) {
		isExpired = expired;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getSequence() {
		return sequence;
	}

	public void setSequence(Integer sequence) {
		this.sequence = sequence;
	}

	public Long getVendorInfoConfigId() {
		return vendorInfoConfigId;
	}

	public void setVendorInfoConfigId(Long vendorInfoConfigId) {
		this.vendorInfoConfigId = vendorInfoConfigId;
	}

	public Integer getStatus() { return status; }

	public void setStatus(Integer status) { this.status = status; }
}

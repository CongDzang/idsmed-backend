package com.cmg.idsmed.dto.vendor;

import com.cmg.idsmed.model.entity.vendor.config.VendorLabelConfig;

public class VendorLabelConfigResponse {

	private Long id;

	private String labelPrimaryName;

	private String labelSecondaryName;

	private Boolean isMandatory;

	/**
	 * This column determine this label has expiry date or not
	 */
	private Boolean isExpired = false;

	private Integer type;

	private Integer sequence;

	private Integer status;

	public VendorLabelConfigResponse() {

	}

	public VendorLabelConfigResponse(VendorLabelConfig vendorLabelConfig) {
		this.id = vendorLabelConfig.getId();
		this.labelPrimaryName = vendorLabelConfig.getLabelPrimaryName();
		this.labelSecondaryName = vendorLabelConfig.getLabelSecondaryName();
		this.isMandatory = vendorLabelConfig.getMandatory();
		this.isExpired = vendorLabelConfig.getExpired();
		this.type = vendorLabelConfig.getType();
		this.sequence = vendorLabelConfig.getSequence();
		this.status = vendorLabelConfig.getStatus();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLabelPrimaryName() {
		return labelPrimaryName;
	}

	public void setLabelPrimaryName(String labelPrimaryName) {
		this.labelPrimaryName = labelPrimaryName;
	}

	public String getLabelSecondaryName() {
		return labelSecondaryName;
	}

	public void setLabelSecondaryName(String labelSecondaryName) {
		this.labelSecondaryName = labelSecondaryName;
	}

	public Boolean getMandatory() {
		return isMandatory;
	}

	public void setMandatory(Boolean mandatory) {
		isMandatory = mandatory;
	}

	public Boolean getExpired() {
		return isExpired;
	}

	public void setExpired(Boolean expired) {
		isExpired = expired;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getSequence() {
		return sequence;
	}

	public void setSequence(Integer sequence) {
		this.sequence = sequence;
	}


	public Integer getStatus() { return status; }

	public void setStatus(Integer status) { this.status = status; }
}

package com.cmg.idsmed.dto.vendor;

import java.util.List;

public class VendorListResponse {
	private Integer totalPage = null;
	private List<VendorResponse> vendorList;
	private Integer totalCount = null;
	private Integer pageNo = null;
	private Integer pageSize = null;

	public VendorListResponse(Integer pageNo, Integer pageSize) {
		this.totalPage = 0;
		this.totalCount = 0;
		this.pageSize = pageSize;
		this.pageNo = pageNo;
	}
	
	public VendorListResponse(Integer totalCount, Integer pageIndex, Integer pageSize, List<VendorResponse> vendorList) {
		if (pageSize != null && !pageSize.equals(0)) {
			Integer calTotalPage = totalCount/pageSize;
			if(totalCount % pageSize > 0) {
				calTotalPage++;
			}
			this.totalPage = calTotalPage;
		}
		this.totalCount = totalCount;
		this.pageNo = pageIndex + 1;
		this.pageSize = pageSize;
		this.vendorList = vendorList;
	}

	public VendorListResponse(Integer totalCount, List<VendorResponse> vendorList) {
		this.totalCount = totalCount;
		this.vendorList = vendorList;
		this.totalPage = 1;
		this.pageSize = totalCount;
		this.pageNo = 1;
	}

	public Integer getTotalPage() {
		return totalPage;
	}

	public void setTotalPage(Integer totalPage) {
		this.totalPage = totalPage;
	}

	public List<VendorResponse> getVendorList() {
		return vendorList;
	}

	public void setProductList(List<VendorResponse> vendorList) {
		this.vendorList = vendorList;
	}

	public Integer getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}

	public Integer getPageNo() {
		return pageNo;
	}

	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}
	
	public void setVendorList(List<VendorResponse> vendorList) {
		this.vendorList = vendorList;
	}
}

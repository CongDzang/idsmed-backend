package com.cmg.idsmed.dto.vendor;

import org.springframework.beans.BeanUtils;

import com.cmg.idsmed.model.entity.vendor.VendorReject;

public class VendorRejectResponse {
	private Long id;
    
    private String remarks;

	private String countryCode;
	
	public VendorRejectResponse(VendorReject vendorReject) {
		BeanUtils.copyProperties(vendorReject, this);
	}
	
	public Long getId() {
			return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getCountryCode() {
		return countryCode;
	}

		public void setCountryCode(String countryCode) {
			this.countryCode = countryCode;
		}
}

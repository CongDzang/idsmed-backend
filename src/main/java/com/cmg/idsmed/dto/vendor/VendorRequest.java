package com.cmg.idsmed.dto.vendor;

import java.time.ZonedDateTime;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.cmg.idsmed.dto.AbstractRequest;
import com.cmg.idsmed.model.entity.vendor.Vendor;

public class VendorRequest extends AbstractRequest {
	private Long id;
	
	@Size(max = 200)
	private String companyNamePrimary;

	@Size(max = 200)
    private String companyNameSecondary;

	@Size(max = 1000)
	private String address;

	@Size(max = 100)
	private String phoneNumber;
	    
	@Size(max = 100)
	private String email;

    @Size(max = 100)
	private String personIncharge1Name;
    
    @Size(max = 100)
	private String personIncharge1Phone;
	    
    @Size(max = 100)
	private String personIncharge1Email;
	    
    @Size(max = 100)
	private String personIncharge1Wechat;

    @Size(max = 100)
	private String personIncharge1Qq;
	    
    @Size(max = 100)
	private String personIncharge2Name;

	@Size(max = 100)
	private String personIncharge2Phone;
	    
	@Size(max = 100)
	private String personIncharge2Email;
	    
	@Size(max = 100)
	private String personIncharge2Wechat;
	    
	@Size(max = 100)
	private String personIncharge2Qq;

	@Size(max = 100)
	private String companyCode;
	
	@NotNull
	@Size(max = 10)
	private String countryCode;
	    
	@Size(max = 1000)
	private String companyLogoPath;

	private String langCode;

	//This variable mapping between title of a vendor file attachment and uploaded file name
	private List<VendorFileAttachmentRequest> fileAttachments;

	private List<VendorAdditionalInfoRequest> additionalInfos;

	private List<VendorAddressRequest> vendorAddressRequests;

	private Integer status;
	
	private String rejectReason;
	
	private String loginId;
	
	private ZonedDateTime approvedDate;

	// private Integer location;

	private String accountNumber;

	private Integer type;

	private String vatGst;

	private Long vendorCoordinator;

	private String bankOfDeposit;

	private String billingInfoCompanyName;

	private String billingInfoTelephone;

	public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCompanyNamePrimary() {
		return companyNamePrimary;
	}

	public void setCompanyNamePrimary(String companyNamePrimary) {
		this.companyNamePrimary = companyNamePrimary;
	}

	public String getCompanyNameSecondary() {
		return companyNameSecondary;
	}

	public void setCompanyNameSecondary(String companyNameSecondary) {
		this.companyNameSecondary = companyNameSecondary;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPersonIncharge1Name() {
		return personIncharge1Name;
	}

	public void setPersonIncharge1Name(String personIncharge1Name) {
		this.personIncharge1Name = personIncharge1Name;
	}

	public String getPersonIncharge1Phone() {
		return personIncharge1Phone;
	}

	public void setPersonIncharge1Phone(String personIncharge1Phone) {
		this.personIncharge1Phone = personIncharge1Phone;
	}

	public String getPersonIncharge1Email() {
		return personIncharge1Email;
	}

	public void setPersonIncharge1Email(String personIncharge1Email) {
		this.personIncharge1Email = personIncharge1Email;
	}

	public String getPersonIncharge1Wechat() {
		return personIncharge1Wechat;
	}

	public void setPersonIncharge1Wechat(String personIncharge1Wechat) {
		this.personIncharge1Wechat = personIncharge1Wechat;
	}

	public String getPersonIncharge1Qq() {
		return personIncharge1Qq;
	}

	public void setPersonIncharge1Qq(String personIncharge1Qq) {
		this.personIncharge1Qq = personIncharge1Qq;
	}

	public String getPersonIncharge2Name() {
		return personIncharge2Name;
	}

	public void setPersonIncharge2Name(String personIncharge2Name) {
		this.personIncharge2Name = personIncharge2Name;
	}

	public String getPersonIncharge2Phone() {
		return personIncharge2Phone;
	}

	public void setPersonIncharge2Phone(String personIncharge2Phone) {
		this.personIncharge2Phone = personIncharge2Phone;
	}

	public String getPersonIncharge2Email() {
		return personIncharge2Email;
	}

	public void setPersonIncharge2Email(String personIncharge2Email) {
		this.personIncharge2Email = personIncharge2Email;
	}

	public String getPersonIncharge2Wechat() {
		return personIncharge2Wechat;
	}

	public void setPersonIncharge2Wechat(String personIncharge2Wechat) {
		this.personIncharge2Wechat = personIncharge2Wechat;
	}

	public String getPersonIncharge2Qq() {
		return personIncharge2Qq;
	}

	public void setPersonIncharge2Qq(String personIncharge2Qq) {
		this.personIncharge2Qq = personIncharge2Qq;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getCompanyLogoPath() {
		return companyLogoPath;
	}

	public void setCompanyLogoPath(String companyLogoPath) {
		this.companyLogoPath = companyLogoPath;
	}

	public String getRejectReason() {
		return rejectReason;
	}

	public void setRejectReason(String rejectReason) {
		this.rejectReason = rejectReason;
	}

	@Override
	public String getLangCode() {
		return langCode;
	}

	@Override
	public void setLangCode(String langCode) {
		this.langCode = langCode;
	}

	@Override
	public String getLoginId() {
		return loginId;
	}

	@Override
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public List<VendorFileAttachmentRequest> getFileAttachments() {
		return fileAttachments;
	}

	public void setFileAttachments(List<VendorFileAttachmentRequest> fileAttachments) { this.fileAttachments = fileAttachments; }

	public List<VendorAdditionalInfoRequest> getAdditionalInfos() {
		return additionalInfos;
	}

	// public Integer getLocation() {
	// 	return location;
	// }
	//
	// public void setLocation(Integer location) {
	// 	this.location = location;
	// }

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getVatGst() { return vatGst; }

	public void setVatGst(String vatGst) { this.vatGst = vatGst; }

	public void setAdditionalInfos(List<VendorAdditionalInfoRequest> additionalInfos) { this.additionalInfos = additionalInfos; }

	public List<VendorAddressRequest> getVendorAddressRequests() {
		return vendorAddressRequests;
	}

	public void setVendorAddressRequests(List<VendorAddressRequest> vendorAddressRequests) { this.vendorAddressRequests = vendorAddressRequests; }

	public Long getVendorCoordinator() { return vendorCoordinator; }

	public void setVendorCoordinator(Long vendorCoordinator) { this.vendorCoordinator = vendorCoordinator; }

	public String getBankOfDeposit() { return bankOfDeposit; }

	public void setBankOfDeposit(String bankOfDeposit) { this.bankOfDeposit = bankOfDeposit; }

	public String getBillingInfoCompanyName() { return billingInfoCompanyName; }

	public void setBillingInfoCompanyName(String billingInfoCompanyName) { this.billingInfoCompanyName = billingInfoCompanyName; }

	public String getBillingInfoTelephone() { return billingInfoTelephone; }

	public void setBillingInfoTelephone(String billingInfoTelephone) { this.billingInfoTelephone = billingInfoTelephone; }

	public String getAccountNumber() { return accountNumber; }

	public void setAccountNumber(String accountNumber) { this.accountNumber = accountNumber; }

	public ZonedDateTime getApprovedDate() {
		return approvedDate;
	}

	public void setApprovedDate(ZonedDateTime approvedDate) {
		this.approvedDate = approvedDate;
	}
}

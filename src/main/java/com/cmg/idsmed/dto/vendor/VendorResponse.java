package com.cmg.idsmed.dto.vendor;


import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

import com.cmg.idsmed.common.enums.VendorActionEnum;
import com.cmg.idsmed.dto.account.IdsmedAccountResponse;
import com.cmg.idsmed.model.entity.vendor.FileAttachment;
import com.cmg.idsmed.model.entity.vendor.VendorActionLog;
import org.springframework.beans.BeanUtils;
import org.springframework.util.CollectionUtils;

import com.cmg.idsmed.dto.AbstractResponse;
import com.cmg.idsmed.model.entity.vendor.Vendor;
import com.cmg.idsmed.model.entity.vendor.VendorReject;
import org.springframework.util.StringUtils;

public class VendorResponse extends AbstractResponse {

	private Long id;

	private String companyNamePrimary;

	private String companyNameSecondary;

	private String address;

	private String phoneNumber;
	    
	private String email;

	private String personIncharge1Name;

	private String personIncharge1Phone;
	    
	private String personIncharge1Email;
	
	private String personIncharge1Wechat;
	   
	private String personIncharge1Qq;
	    
	private String personIncharge2Name;
	
	private String personIncharge2Phone;

	private String personIncharge2Email;

	private String personIncharge2Wechat;

	private String personIncharge2Qq;
	    
    private String companyCode;
	    
    private String countryCode;
	
    private String companyLogoPath;
    
    private Integer status;
    
	private String vendorRejectReason;
	
	private Integer approvedBy;
	
	private LocalDateTime approvedDate;

	private Integer type;

	// private Integer location;

	private String accountNumber;

	private String custGroup;

	private String currency;

	private String vatGst;

	private String bankOfDeposit;

	private String billingInfoCompanyName;

	private String billingInfoTelephone;

	private String inactiveReason;

	//This account for VendorCoodinator.
	private IdsmedAccountResponse idsmedAccountResponse;

	private List<VendorAddressResponse> vendorAddresses;
    
    private List<FileAttachmentResponse> fileAttachments = new ArrayList<>();

    private List<VendorAdditionalInfoResponse> addtionalInfos = new ArrayList<>();

	public VendorResponse() {
	}

	public VendorResponse(Vendor vendor, String ossBucketDomain) {
		if(vendor != null) {
			String [] ignoreProperties = {"fileAttachmentList"
					,"vendorRejectReason"
					, "addtionalInfos"
					, "vendorAddresses"
					, "companyLogoPath"};

			BeanUtils.copyProperties(vendor, this, ignoreProperties);

			if(!CollectionUtils.isEmpty(vendor.getVendorActionLogs())) {
				for (VendorActionLog vendorActionLog : vendor.getVendorActionLogs()) {
					if (vendorActionLog.getActionCode() == VendorActionEnum.SUSPEND.getCode()) {
						this.inactiveReason = vendorActionLog.getReason();
					}
				}
			}

			if (!CollectionUtils.isEmpty(vendor.getVendorRejectList())) {
				VendorReject vr = Collections.max(vendor.getVendorRejectList(), Comparator.comparing(VendorReject::getId));
				this.vendorRejectReason = vr.getRemarks();
			}

			if(!StringUtils.isEmpty(ossBucketDomain) && !StringUtils.isEmpty(vendor.getCompanyLogoPath())) {
				this.companyLogoPath = ossBucketDomain + vendor.getCompanyLogoPath();
			}

			if (!CollectionUtils.isEmpty(vendor.getVendorAddresses())) {
				this.vendorAddresses = vendor.getVendorAddresses()
						.stream()
						.map(adr -> new VendorAddressResponse(adr))
						.collect(Collectors.toList());
			}

			//For each type of VendorLabelConfig we just return the FileAttachment with latest createdDate.
			List<FileAttachment> allFileAttachments = vendor.getFileAttachmentList();
			List<FileAttachment> validFileAttachment = new ArrayList<>();

			if(!CollectionUtils.isEmpty(allFileAttachments)) {

				List<Long> vendorLabelConfigIds = allFileAttachments
						.stream()
						.map(fat -> fat.getVendorLabelConfig().getId()).distinct()
						.collect(Collectors.toList());

				if (!CollectionUtils.isEmpty(vendorLabelConfigIds)) {
					for (Long id : vendorLabelConfigIds) {
						Optional<FileAttachment> fileAttachmentOpt = allFileAttachments.stream().filter(f -> {
							if (f.getVendorLabelConfig().getId().equals(id)) {
								return true;
							}
							return false;
						}).max(Comparator.comparing(FileAttachment::getCreatedDate));
						if (fileAttachmentOpt.isPresent()) {
							validFileAttachment.add(fileAttachmentOpt.get());
						}
					}
				}

				if (!CollectionUtils.isEmpty(validFileAttachment)) {
					this.fileAttachments = validFileAttachment
							.stream()
							.map(m -> new FileAttachmentResponse(m, ossBucketDomain)).collect(Collectors.toList());
				}
			}
			//============================================================

			if (!CollectionUtils.isEmpty(vendor.getAdditionalInfos())) {
				this.addtionalInfos = vendor.getAdditionalInfos()
						.stream().map(adi -> new VendorAdditionalInfoResponse(adi)).collect(Collectors.toList());
			}
			this.accountNumber = vendor.getBankAccountNumber();
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCompanyNamePrimary() {
		return companyNamePrimary;
	}

	public void setCompanyNamePrimary(String companyNamePrimary) {
		this.companyNamePrimary = companyNamePrimary;
	}

	public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	public String getCompanyNameSecondary() {
		return companyNameSecondary;
	}

	public void setCompanyNameSecondary(String companyNameSecondary) {
		this.companyNameSecondary = companyNameSecondary;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPersonIncharge1Name() {
		return personIncharge1Name;
	}

	public void setPersonIncharge1Name(String personIncharge1Name) {
		this.personIncharge1Name = personIncharge1Name;
	}

	public String getPersonIncharge1Phone() {
		return personIncharge1Phone;
	}

	public void setPersonIncharge1Phone(String personIncharge1Phone) {
		this.personIncharge1Phone = personIncharge1Phone;
	}

	public String getPersonIncharge1Email() {
		return personIncharge1Email;
	}

	public void setPersonIncharge1Email(String personIncharge1Email) {
		this.personIncharge1Email = personIncharge1Email;
	}

	public String getPersonIncharge1Wechat() {
		return personIncharge1Wechat;
	}

	public void setPersonIncharge1Wechat(String personIncharge1Wechat) {
		this.personIncharge1Wechat = personIncharge1Wechat;
	}

	public String getPersonIncharge1Qq() {
		return personIncharge1Qq;
	}

	public void setPersonIncharge1Qq(String personIncharge1Qq) {
		this.personIncharge1Qq = personIncharge1Qq;
	}

	public String getPersonIncharge2Name() {
		return personIncharge2Name;
	}

	public void setPersonIncharge2Name(String personIncharge2Name) {
		this.personIncharge2Name = personIncharge2Name;
	}

	public String getPersonIncharge2Phone() {
		return personIncharge2Phone;
	}

	public void setPersonIncharge2Phone(String personIncharge2Phone) {
		this.personIncharge2Phone = personIncharge2Phone;
	}

	public String getPersonIncharge2Email() {
		return personIncharge2Email;
	}

	public void setPersonIncharge2Email(String personIncharge2Email) {
		this.personIncharge2Email = personIncharge2Email;
	}

	public String getPersonIncharge2Wechat() {
		return personIncharge2Wechat;
	}

	public void setPersonIncharge2Wechat(String personIncharge2Wechat) {
		this.personIncharge2Wechat = personIncharge2Wechat;
	}

	public String getPersonIncharge2Qq() {
		return personIncharge2Qq;
	}

	public void setPersonIncharge2Qq(String personIncharge2Qq) {
		this.personIncharge2Qq = personIncharge2Qq;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getCompanyLogoPath() {
		return companyLogoPath;
	}

	public void setCompanyLogoPath(String companyLogoPath) {
		this.companyLogoPath = companyLogoPath;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public List<VendorAddressResponse> getVendorAddresses() {
		return vendorAddresses;
	}

	public void setVendorAddresses(List<VendorAddressResponse> vendorAddresses) {
		this.vendorAddresses = vendorAddresses;
	}

	public List<FileAttachmentResponse> getFileAttachments() {
		return fileAttachments;
	}

	public void setFileAttachments(List<FileAttachmentResponse> fileAttachments) {
		this.fileAttachments = fileAttachments;
	}

	public List<VendorAdditionalInfoResponse> getAddtionalInfos() {
		return addtionalInfos;
	}

	public void setAddtionalInfos(List<VendorAdditionalInfoResponse> addtionalInfos) {
		this.addtionalInfos = addtionalInfos;
	}

	public String getVendorRejectReason() {
		return vendorRejectReason;
	}

	public void setVendorRejectReason(String vendorRejectReason) {
		this.vendorRejectReason = vendorRejectReason;
	}

	public Integer getApprovedBy() {
		return approvedBy;
	}

	public void setApprovedBy(Integer approvedBy) {
		this.approvedBy = approvedBy;
	}

	public LocalDateTime getApprovedDate() {
		return approvedDate;
	}

	public void setApprovedDate(LocalDateTime approvedDate) {
		this.approvedDate = approvedDate;
	}

	public Integer getType() { return type; }

	public void setType(Integer type) { this.type = type; }

	// public Integer getLocation() { return location; }
	//
	// public void setLocation(Integer location) { this.location = location; }

	public String getAccountNumber() { return accountNumber; }

	public void setAccountNumber(String accountNumber) { this.accountNumber = accountNumber; }

	public String getCustGroup() { return custGroup; }

	public void setCustGroup(String custGroup) { this.custGroup = custGroup; }

	public String getCurrency() { return currency; }

	public void setCurrency(String currency) { this.currency = currency; }

	public String getVatGst() { return vatGst; }

	public void setVatGst(String vatGst) { this.vatGst = vatGst; }

	public IdsmedAccountResponse getIdsmedAccountResponse() { return idsmedAccountResponse; }

	public void setIdsmedAccountResponse(IdsmedAccountResponse idsmedAccountResponse) { this.idsmedAccountResponse = idsmedAccountResponse; }

	public String getBankOfDeposit() { return bankOfDeposit; }

	public void setBankOfDeposit(String bankOfDeposit) { this.bankOfDeposit = bankOfDeposit; }

	public String getBillingInfoCompanyName() { return billingInfoCompanyName; }

	public void setBillingInfoCompanyName(String billingInfoCompanyName) { this.billingInfoCompanyName = billingInfoCompanyName; }

	public String getBillingInfoTelephone() { return billingInfoTelephone;}

	public void setBillingInfoTelephone(String billingInfoTelephone) { this.billingInfoTelephone = billingInfoTelephone; }

	public String getInactiveReason() {
		return inactiveReason;
	}

	public void setInactiveReason(String inactiveReason) {
		this.inactiveReason = inactiveReason;
	}
}

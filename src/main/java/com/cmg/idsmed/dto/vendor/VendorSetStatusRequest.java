package com.cmg.idsmed.dto.vendor;

import com.cmg.idsmed.dto.AbstractRequest;

public class VendorSetStatusRequest extends AbstractRequest {
	private Long vendorId;
	private Integer actionCode;
	private String reason;

	public Long getVendorId() {
		return vendorId;
	}

	public void setVendorId(Long vendorId) {
		this.vendorId = vendorId;
	}

	public Integer getActionCode() {
		return actionCode;
	}

	public void setActionCode(Integer actionCode) {
		this.actionCode = actionCode;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}
}

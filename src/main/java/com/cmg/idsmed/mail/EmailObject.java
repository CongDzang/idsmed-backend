package com.cmg.idsmed.mail;

import com.cmg.idsmed.model.entity.common.MailSendingLog;

import java.util.List;

/**
 *
 */
public abstract class EmailObject {
	private String uuid;
	private List<String> recipients;
	private List<String> ccRecipient;
	private List<String> bccRecipients;
	private String sendFrom;
	private String subject;
	private String content;

	public EmailObject() {
	}

	public EmailObject(List<String> recipients
			, List<String> ccRecipient
			, List<String> bccRecipients
			, String sendFrom
			, String subject
			, String content) {
		this.recipients = recipients;
		this.ccRecipient = ccRecipient;
		this.bccRecipients = bccRecipients;
		this.sendFrom = sendFrom;
		this.subject = subject;
		this.content = content;
	}
	
	public abstract EmailObject generateFailingNotificationMail (Long logId, String errorMessage);
	
	public abstract MailSendingLog generateFailMailSendingLog (String errorMessage);
	
	public abstract MailSendingLog generateSuccessMailSendingLog ();


	public List<String> getRecipients() {
		return recipients;
	}

	public void setRecipients(List<String> recipients) {
		this.recipients = recipients;
	}

	public List<String> getCcRecipient() {
		return ccRecipient;
	}

	public void setCcRecipient(List<String> ccRecipient) {
		this.ccRecipient = ccRecipient;
	}

	public List<String> getBccRecipients() {
		return bccRecipients;
	}

	public void setBccRecipients(List<String> bccRecipients) {
		this.bccRecipients = bccRecipients;
	}

	public String getSendFrom() {
		return sendFrom;
	}

	public void setSendFrom(String sendFrom) {
		this.sendFrom = sendFrom;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
}

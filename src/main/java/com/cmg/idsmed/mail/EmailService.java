package com.cmg.idsmed.mail;

import com.cmg.idsmed.common.Constant.NotificationWildcardConstant;
import com.cmg.idsmed.common.Constant.PropertiesLabelConst;
import com.cmg.idsmed.common.enums.DefaultPasswordEnum;
import com.cmg.idsmed.common.enums.EmailTypeEnum;
import com.cmg.idsmed.common.enums.LangEnum;
import com.cmg.idsmed.common.exception.IdsmedBaseException;
import com.cmg.idsmed.dto.email.EmailTemplateResponse;
import com.cmg.idsmed.mail.sender.MailSender;
import com.cmg.idsmed.mail.sender.MailSenderFactory;
import com.cmg.idsmed.model.entity.auth.IdsmedAccount;
import com.cmg.idsmed.model.entity.auth.IdsmedUser;
import com.cmg.idsmed.model.entity.notification.AccountNotificationSetting;
import com.cmg.idsmed.model.entity.notification.Notification;
import com.cmg.idsmed.model.entity.product.Product;
import com.cmg.idsmed.model.entity.vendor.Vendor;
import com.cmg.idsmed.model.repo.masterdata.AccountNotificationSettingRepository;
import com.cmg.idsmed.service.email.EmailNotificationConfigService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.mail.MessagingException;
import java.util.*;

@Service
@PropertySource({"classpath:${SYSTEM_ENVIRONMENT_VARIABLE}_mailconfig.properties"})
public class EmailService {
	private static final Logger logger = LoggerFactory.getLogger(EmailService.class);

	@Value("${SYSTEM_ENVIRONMENT_VARIABLE}")
	private String environment;

	@Autowired
	private Environment env;

	@Autowired
	private EmailNotificationConfigService emailNotificationConfigService;

	@Autowired
	private AccountNotificationSettingRepository accountNotificationSettingRepository;

	public void sendEmail(EmailObject emailObject) throws MessagingException, IdsmedBaseException {
		logger.info("Start sendEmail method");
		MailSender sender = MailSenderFactory.newInstance(env);
		if (emailObject == null) {
			//TODO throw exception here
		}


		List<String> recipients =  emailObject.getRecipients();
		List<String> ccRecipients = emailObject.getCcRecipient();
		List<String> bccRecipients = emailObject.getBccRecipients();

		if (!CollectionUtils.isEmpty(recipients)) {
			for (String rcp : recipients) {
				if (!StringUtils.isEmpty(rcp)) {
					sender.send(rcp
							, emailObject.getSendFrom()
							, emailObject.getSubject()
							, emailObject.getContent());
				}
			}
		}

		if (!CollectionUtils.isEmpty(ccRecipients)) {
			for (String rcp : ccRecipients) {
				if (!StringUtils.isEmpty(rcp)) {
					sender.send(rcp
							, emailObject.getSendFrom()
							, emailObject.getSubject()
							, emailObject.getContent());
				}
			}
		}

		if (!CollectionUtils.isEmpty(bccRecipients)) {
			for (String rcp : bccRecipients) {
				if (!StringUtils.isEmpty(rcp)) {
					sender.send(rcp
							, emailObject.getSendFrom()
							, emailObject.getSubject()
							, emailObject.getContent());
				}
			}
		}
	}

	public Pair<String, String> generatedProductApprovedEmailSubjectAndContent(String name, String vendorName, String productNamePrimary,
																			   String productModel, String productCode, String status)throws IdsmedBaseException{
		EmailTemplateResponse template = emailNotificationConfigService.getEmailNotificationConfigByEmailType(EmailTypeEnum.PRODUCT_APPROVED_NOTIFICATION).getEmailTemplate();

		String emailSubject = template.getSubject();
		String emailContent = template.getContent();
		emailContent = emailContent.replace(NotificationWildcardConstant.RECIPIENT_NAME, name);
		emailContent = emailContent.replace(NotificationWildcardConstant.VENDOR_NAME, vendorName );
		emailContent = emailContent.replace(NotificationWildcardConstant.PRODUCT_NAME, productNamePrimary);
		productModel = !StringUtils.isEmpty(productModel) ? productModel : "";
		emailContent = emailContent.replace(NotificationWildcardConstant.PRODUCT_MODEL, productModel );
		emailContent = emailContent.replace(NotificationWildcardConstant.PRODUCT_CODE, productCode);
		emailContent = emailContent.replace(NotificationWildcardConstant.PRODUCT_STATUS, status);

		Pair<String, String> emailPair = Pair.of(emailSubject, emailContent);
		return emailPair;
	}

	public Pair<String, String> generatedVendorRegistrationEmailSubjectAndContent(String username, String vendorName) throws IdsmedBaseException{
		EmailTemplateResponse template = emailNotificationConfigService.getEmailNotificationConfigByEmailType(EmailTypeEnum.VENDOR_REGISTRATION_NOTIFICATION).getEmailTemplate();

		String emailSubject = template.getSubject();
		String emailContent = template.getContent();
		Map<String, String> wildCard = new HashMap<>();
		wildCard.put(NotificationWildcardConstant.RECIPIENT_NAME, username);
		wildCard.put(NotificationWildcardConstant.VENDOR_NAME, vendorName);
		emailContent = processReplaceEmailTemplateWildCard(wildCard, emailContent);
		Pair<String, String> emailPair = Pair.of(emailSubject, emailContent);
		return emailPair;
	}

	public Pair<String, String> generatedVendorRegistrationPendingReviewEmailSubjectAndContent(String userName, String vendorName)throws IdsmedBaseException{
		EmailTemplateResponse template = emailNotificationConfigService.getEmailNotificationConfigByEmailType(EmailTypeEnum.REQUEST_TO_REVIEW_VENDOR_REGISTRATION_NOTIFICATION).getEmailTemplate();
		String emailSubject = template.getSubject();
		String emailContent = template.getContent();
		emailContent = emailContent.replace(NotificationWildcardConstant.RECIPIENT_NAME, userName);
		emailContent = emailContent.replace(NotificationWildcardConstant.VENDOR_NAME, vendorName);
		Pair<String, String> emailPair = Pair.of(emailSubject, emailContent);

		return emailPair;
	}

	public Pair<String, String> generatedVendorRegistrationPendingApproveEmailSubjectAndContent(String userName, String vendorName) throws IdsmedBaseException{
		EmailTemplateResponse template = emailNotificationConfigService.getEmailNotificationConfigByEmailType(EmailTypeEnum.REQUEST_TO_APPROVE_VENDOR_REGISTRATION_NOTIFICATION).getEmailTemplate();
		String emailSubject = template.getSubject();
		String emailContent = template.getContent();
		emailContent = emailContent.replace(NotificationWildcardConstant.RECIPIENT_NAME, userName);
		emailContent = emailContent.replace(NotificationWildcardConstant.VENDOR_NAME, vendorName);
		Pair<String, String> emailPair = Pair.of(emailSubject, emailContent);

		return emailPair;
	}

	public Pair<String, String> generatedVendorRegistrationApprovedEmailSubjectAndContent(String userName, String vendorName, String status, String companyCode) throws IdsmedBaseException{
		EmailTemplateResponse template = emailNotificationConfigService.getEmailNotificationConfigByEmailType(EmailTypeEnum.VENDOR_REGISTRATION_APPROVED_NOTIFICATION).getEmailTemplate();

		String emailSubject = template.getSubject();
		String emailContent = template.getContent();
		emailContent = emailContent.replace(NotificationWildcardConstant.RECIPIENT_NAME, userName);
		emailContent = emailContent.replace(NotificationWildcardConstant.VENDOR_NAME, vendorName);
		emailContent = emailContent.replace(NotificationWildcardConstant.VENDOR_STATUS, status);
		emailContent = emailContent.replace(NotificationWildcardConstant.COMPANY_CODE, companyCode);
		Pair<String, String> emailPair = Pair.of(emailSubject, emailContent);
		return emailPair;
	}

//	public Pair<String, String> generatedUserRegistrationApprovedEmailSubjectAndContent(String userName, String status) throws IdsmedBaseException{
//		EmailTemplateResponse template = emailNotificationConfigService.getEmailNotificationConfigByEmailType(EmailTypeEnum.VENDOR_USER_REGISTRATION_APPROVED_NOTIFICATION).getEmailTemplate();
//
//		String emailSubject = template.getSubject();
//		String emailContent = template.getContent();
//		emailContent = emailContent.replace(NotificationWildcardConstant.USER_NAME, userName);
//		emailContent = emailContent.replace(NotificationWildcardConstant.USER_STATUS, status);
//		Pair<String, String> emailPair = Pair.of(emailSubject, emailContent);
//		return emailPair;
//	}
	
	public Pair<String, String> generatedVendorRegistrationRejectEmailSubjectAndContent(String userName, String vendorName, String status, String rejectReason, String resubmitLink, String langCode) throws IdsmedBaseException{
		EmailTemplateResponse template = emailNotificationConfigService.getEmailNotificationConfigByEmailType(EmailTypeEnum.VENDOR_REGISTRATION_REJECTED_NOTIFICATION).getEmailTemplate();

		String emailSubject = template.getSubject();
		String emailContent = template.getContent();
		emailContent = emailContent.replace(NotificationWildcardConstant.RECIPIENT_NAME, userName);
		emailContent = emailContent.replace(NotificationWildcardConstant.VENDOR_NAME, vendorName);
		emailContent = emailContent.replace(NotificationWildcardConstant.VENDOR_STATUS, status);
		emailContent = emailContent.replace(NotificationWildcardConstant.VENDOR_REJECT_REASON, rejectReason);
		emailContent = emailContent.replace(NotificationWildcardConstant.VENDOR_RESUBMIT_PROFILE_URL, compileURL(resubmitLink, langCode));
		Pair<String, String> emailPair = Pair.of(emailSubject, emailContent);
		return emailPair;
	}

	public Pair<String, String> generatedVendorUserRegistrationEmailSubjectAndContent(String userName) throws IdsmedBaseException{
		EmailTemplateResponse template = emailNotificationConfigService.getEmailNotificationConfigByEmailType(EmailTypeEnum.VENDOR_USER_REGISTRATION_NOTIFICATION).getEmailTemplate();

		String emailSubject = template.getSubject();
		String emailContent = template.getContent();
		String fullEditLink = emailContent.replace(NotificationWildcardConstant.RECIPIENT_NAME, userName);
		Pair<String, String> emailPair = Pair.of(emailSubject, fullEditLink);
		return emailPair;
	}
	
	public Pair<String, String> generatedProductBatchUploadReportEmail(String userName, String vendorName, String uploadFilename, Integer totalCreatedProduct, String errorRowInfo) throws IdsmedBaseException{
		EmailTemplateResponse template = emailNotificationConfigService.getEmailNotificationConfigByEmailType(EmailTypeEnum.PRODUCT_BATCH_UPLOAD_RESULT_NOTIFICATION).getEmailTemplate();

		String emailSubject = template.getSubject();
		String emailContent = template.getContent();
		StringBuilder uploadResultStr = new StringBuilder();
		uploadResultStr.append("Total Created Product : " + String.valueOf(totalCreatedProduct) + " <br>");
		if(!StringUtils.isEmpty(errorRowInfo)) {
			uploadResultStr.append("Error Row Info : " + errorRowInfo + " <br>");
		}
		
		emailContent = emailContent.replace(NotificationWildcardConstant.RECIPIENT_NAME, StringUtils.isEmpty(userName) ? "there" : userName);
		emailContent = emailContent.replace(NotificationWildcardConstant.VENDOR_NAME, vendorName);
		emailContent = emailContent.replace(NotificationWildcardConstant.PRODUCT_BATCH_UPLOAD_FILENAME, uploadFilename);
		emailContent = emailContent.replace(NotificationWildcardConstant.PRODUCT_BATCH_UPLOAD_RESULT, uploadResultStr);
		Pair<String, String> emailPair = Pair.of(emailSubject, emailContent);
		return emailPair;
	}

	// Get email template and replace wildcard with necessary value for email notification
	public Pair<String, String> generatedProductApprovedEmailSubjectAndContentForSCPAdmin(
			String name, String vendorName, String productNamePrimary,
			String productModel, String productCode)throws IdsmedBaseException{
		EmailTemplateResponse template = emailNotificationConfigService
				.getEmailNotificationConfigByEmailType(EmailTypeEnum.PRODUCT_APPROVED_NOTIFICATION_FOR_SCP_ADMIN)
				.getEmailTemplate();

		String emailSubject = template.getSubject();
		String emailContent = template.getContent();
		emailContent = emailContent.replace(NotificationWildcardConstant.RECIPIENT_NAME, name);
		emailContent = emailContent.replace(NotificationWildcardConstant.VENDOR_NAME, vendorName );
		emailContent = emailContent.replace(NotificationWildcardConstant.PRODUCT_NAME, productNamePrimary);
		emailContent = emailContent.replace(NotificationWildcardConstant.PRODUCT_MODEL, StringUtils.isEmpty(productModel) ? "" : productModel);
		emailContent = emailContent.replace(NotificationWildcardConstant.PRODUCT_CODE, productCode);

		Pair<String, String> emailPair = Pair.of(emailSubject, emailContent);
		return emailPair;
	}

	public Pair<String, String> generatedProductRejectedEmailSubjectAndContent(
			String name, String vendorName, String productNamePrimary,
			String productModel, String productCode, String status, String rejectReason)throws IdsmedBaseException{
		EmailTemplateResponse template = emailNotificationConfigService
				.getEmailNotificationConfigByEmailType(EmailTypeEnum.PRODUCT_REJECTED_NOTIFICATION)
				.getEmailTemplate();

		String emailSubject = template.getSubject();
		String emailContent = template.getContent();
		emailContent = emailContent.replace(NotificationWildcardConstant.RECIPIENT_NAME, name);
		emailContent = emailContent.replace(NotificationWildcardConstant.VENDOR_NAME, vendorName);
		emailContent = emailContent.replace(NotificationWildcardConstant.PRODUCT_NAME, productNamePrimary);
		productModel = !StringUtils.isEmpty(productModel) ? productModel : "";
		emailContent = emailContent.replace(NotificationWildcardConstant.PRODUCT_MODEL, productModel);
		emailContent = emailContent.replace(NotificationWildcardConstant.PRODUCT_CODE, productCode);
		emailContent = emailContent.replace(NotificationWildcardConstant.PRODUCT_STATUS, status);
		emailContent = emailContent.replace(NotificationWildcardConstant.PRODUCT_REJECT_REASON, rejectReason);

		Pair<String, String> emailPair = Pair.of(emailSubject, emailContent);
		return emailPair;
	}

	public Pair<String, String> generateProductCertificateApprovalEmailSubjectAndContent(
			String vendorName, String productDocumentName, String documentAppStatus) throws IdsmedBaseException {

		EmailTemplateResponse template = emailNotificationConfigService
				.getEmailNotificationConfigByEmailType(EmailTypeEnum.PRODUCT_CERTIFICATE_APPROVAL_NOTIFICATION)
				.getEmailTemplate();

		String emailSubject = template.getSubject();
		String emailContent = template.getContent();
		emailContent = emailContent.replace(NotificationWildcardConstant.VENDOR_NAME, vendorName);
		emailContent = emailContent.replace(NotificationWildcardConstant.PRODUCT_DOCUMENT_NAME, productDocumentName);
		emailContent = emailContent.replace(NotificationWildcardConstant.PRODUCT_DOCUMENT_APP_STATUS, documentAppStatus);

		Pair<String, String> emailPair = Pair.of(emailSubject, emailContent);

		return emailPair;
	}

	public Pair<String, String> generatedVendorUserApprovedEmailSubjectAndContent(
			String name, String userAppStatus) throws IdsmedBaseException {

		EmailTemplateResponse template = emailNotificationConfigService
				.getEmailNotificationConfigByEmailType(EmailTypeEnum.VENDOR_USER_REGISTRATION_APPROVED_NOTIFICATION)
				.getEmailTemplate();

		String emailSubject = template.getSubject();
		String emailContent = template.getContent();
		emailContent = emailContent.replace(NotificationWildcardConstant.RECIPIENT_NAME, name);
		emailContent = emailContent.replace(NotificationWildcardConstant.USER_STATUS, userAppStatus);

		Pair<String, String> emailPair = Pair.of(emailSubject, emailContent);

		return emailPair;
	}

	public Pair<String, String> generatedUserApprovedEmailSubjectAndContentForSCPAdmin(
			String scpAdminName, String approvedUserName, String vendorName) throws IdsmedBaseException {

		EmailTemplateResponse template = emailNotificationConfigService
				.getEmailNotificationConfigByEmailType(EmailTypeEnum.USER_REGISTRATION_APPROVED_NOTIFICATION_FOR_SCP_ADMIN)
				.getEmailTemplate();

		String emailSubject = template.getSubject();
		String emailContent = template.getContent();
		emailContent = emailContent.replace(NotificationWildcardConstant.RECIPIENT_NAME, scpAdminName);
		emailContent = emailContent.replace(NotificationWildcardConstant.USER_NAME, approvedUserName);
		emailContent = emailContent.replace(NotificationWildcardConstant.VENDOR_NAME, vendorName);

		Pair<String, String> emailPair = Pair.of(emailSubject, emailContent);

		return emailPair;
	}

	public Pair<String, String> generatedUserRejectedEmailSubjectAndContent(String name,
																			String userAppStatus,
																			String userRejectReason,
																			String resubmitLink,
																			String langCode) throws  IdsmedBaseException{
		EmailTemplateResponse template = emailNotificationConfigService
				.getEmailNotificationConfigByEmailType(EmailTypeEnum.USER_REGISTRATION_REJECTED_NOTIFICATION)
				.getEmailTemplate();
		String emailSubject = template.getSubject();
		String emailContent = template.getContent();
		emailContent = emailContent.replace(NotificationWildcardConstant.RECIPIENT_NAME, name);
		emailContent = emailContent.replace(NotificationWildcardConstant.USER_STATUS, userAppStatus);
		emailContent = emailContent.replace(NotificationWildcardConstant.USER_REJECT_REASON, userRejectReason);
		emailContent = emailContent.replace(NotificationWildcardConstant.USER_REGISTRATION_RESUBMIT_URL, compileURL(resubmitLink, langCode));

		Pair<String, String> emailPair = Pair.of(emailSubject, emailContent);

		return emailPair;
	}

	public Pair<String, String> generateVendorFileAttachmentExpiryEmailSubjectAndContent(String vendorName, String documentName, Date expiryDate)
			throws  IdsmedBaseException {
		EmailTemplateResponse template = emailNotificationConfigService
				.getEmailNotificationConfigByEmailType(EmailTypeEnum.VENDOR_PROFILE_CERTIFICATE_EXPIRY_NOTIFICATION)
				.getEmailTemplate();

		String emailSubject = template.getSubject();
		String emailContent = template.getContent();
		emailContent = emailContent.replace(NotificationWildcardConstant.VENDOR_NAME, vendorName);
		emailContent = emailContent.replace(NotificationWildcardConstant.VENDOR_DOCUMENT_NAME, documentName);
		emailContent = emailContent.replace(NotificationWildcardConstant.VENDOR_DOCUMENT_EXP_DATE, expiryDate.toString());

		Pair<String, String> emailPair = Pair.of(emailSubject, emailContent);
		return emailPair;
	}

	/**
	 * Generate email template for new vendor staff registration without using vendor admin email
	 * Scenario case: when new vendor staff register, company code is provided but email registered is
	 * not vendor admin /senior email
	 */
	public Pair<String, String> generateNewVendorStaffRegistrationForVendorAdminEmailSubjectAndContent(
			String vendorFirstName, String vendorLastName,
			String userFirstName, String userLastName, String approvalStatus) throws IdsmedBaseException {
		EmailTemplateResponse template = emailNotificationConfigService
				.getEmailNotificationConfigByEmailType(EmailTypeEnum.NEW_VENDOR_STAFF_REGISTRATION_NOTIFICATION_FOR_VENDOR_ADMIN)
				.getEmailTemplate();

		String emailSubject = template.getSubject();
		String emailContent = template.getContent();

		Map<String, String> wildCard = new HashMap<>();
		wildCard.put(NotificationWildcardConstant.VENDOR_NAME, vendorFirstName + " " + vendorLastName);
		wildCard.put(NotificationWildcardConstant.USER_NAME, userFirstName + " " + userLastName);
		wildCard.put(NotificationWildcardConstant.USER_STATUS, approvalStatus);

		emailContent = processReplaceEmailTemplateWildCard(wildCard, emailContent);

		Pair<String, String> emailPair = Pair.of(emailSubject, emailContent);

		return emailPair;
	}

	private String processReplaceEmailTemplateWildCard(Map<String, String> wildCard, String content) {
		Set<String> keys = wildCard.keySet();

		for (String key: keys) {
			content = content.replace(key, wildCard.get(key));
		}

		return content;
	}

	public Pair<String, String> generateProductDocumentAttachmentExpiryEmailSubjectAndContent(String vendorName, String productNamePrimary, String productCode, String documentName, Date expiryDate)
			throws  IdsmedBaseException {
		EmailTemplateResponse template = emailNotificationConfigService
				.getEmailNotificationConfigByEmailType(EmailTypeEnum.PRODUCT_CERTIFICATE_EXPIRY_NOTIFICATION)
				.getEmailTemplate();

		String emailSubject = template.getSubject();
		String emailContent = template.getContent();
		Map<String, String> wildCard = new HashMap<>();
		wildCard.put(NotificationWildcardConstant.VENDOR_NAME, vendorName);
		wildCard.put(NotificationWildcardConstant.PRODUCT_NAME, productNamePrimary);
		wildCard.put(NotificationWildcardConstant.PRODUCT_CODE, productCode);
		wildCard.put(NotificationWildcardConstant.PRODUCT_DOCUMENT_NAME, documentName);
		wildCard.put(NotificationWildcardConstant.PRODUCT_DOCUMENT_EXP_DATE, expiryDate.toString());

		emailContent = processReplaceEmailTemplateWildCard(wildCard, emailContent);

		Pair<String, String> emailPair = Pair.of(emailSubject, emailContent);
		return emailPair;
	}

	public Pair<String, String> generateUserResetPasswordEmailSubjectAndContent(String userFirstName, String userLastName, String defaultPassword) throws IdsmedBaseException{
		EmailTemplateResponse template = emailNotificationConfigService
				.getEmailNotificationConfigByEmailType(EmailTypeEnum.USER_RESET_PASSWORD_EMAIL)
				.getEmailTemplate();
		String emailSubject = template.getSubject();
		String emailContent = template.getContent();
		emailContent = emailContent.replace(NotificationWildcardConstant.RECIPIENT_NAME, userFirstName + " " + userLastName);
		emailContent = emailContent.replace(NotificationWildcardConstant.USER_DEFAULT_PASSWORD, defaultPassword);

		Pair<String, String> emailPair = Pair.of(emailSubject, emailContent);

		return emailPair;
	}

	/**
	 * Generate default password email for SCP User once approved
	 * Scenario case: When SCP user has been approved, send email notification with default password
	 * to SCP user with email registered in its account
	 */
	public Pair<String, String> generateApprovedSCPUserWithDefaultPasswordEmailSubjectAndContent(
			String userFirstName, String userLastName,
			String loginId, String status) throws IdsmedBaseException {

		EmailTemplateResponse template = emailNotificationConfigService
				.getEmailNotificationConfigByEmailType(EmailTypeEnum.SCP_USER_REGISTRATION_APPROVED_NOTIFICATION)
				.getEmailTemplate();

		String emailSubject = template.getSubject();
		String emailContent = template.getContent();

		Map<String, String> wildCard = new HashMap<>();
		wildCard.put(NotificationWildcardConstant.RECIPIENT_NAME, userFirstName + " " + userLastName);
		wildCard.put(NotificationWildcardConstant.LOGIN_ID, loginId);
		wildCard.put(NotificationWildcardConstant.USER_DEFAULT_PASSWORD, DefaultPasswordEnum.DEFAULT_PASSWORD.getPassword());
		wildCard.put(NotificationWildcardConstant.USER_STATUS, status);

		emailContent = processReplaceEmailTemplateWildCard(wildCard, emailContent);

		Pair<String, String> emailPair = Pair.of(emailSubject, emailContent);

		return emailPair;
	}

	public Pair<String, String> generatePendingApprovalVendorUserEmailSubjectAndContent(String name) throws IdsmedBaseException {
		EmailTemplateResponse template = emailNotificationConfigService
				.getEmailNotificationConfigByEmailType(EmailTypeEnum.VENDOR_USER_REGISTRATION_NOTIFICATION)
				.getEmailTemplate();

		String emailSubject = template.getSubject();
		String emailContent = template.getContent();

		Map<String, String> wildCard = new HashMap<>();
		wildCard.put(NotificationWildcardConstant.RECIPIENT_NAME, name);

		emailContent = processReplaceEmailTemplateWildCard(wildCard, emailContent);

		Pair<String, String> emailPair = Pair.of(emailSubject, emailContent);

		return emailPair;
	}

	// check whether this user email address dun want send email to him or not.
	public Boolean checkSendEmail(List<IdsmedAccount> idsmedAccounts, EmailTypeEnum emailType) {
		List<AccountNotificationSetting> setting = null;

		for (IdsmedAccount idsmedAccount : idsmedAccounts) {
			setting = accountNotificationSettingRepository.findAllByIdsmedAccount(idsmedAccount);
			if (CollectionUtils.isEmpty(setting)) {
				return true;
			}
			for (AccountNotificationSetting st : setting) {
				if (st.getNotificationType().equals(emailType.getCode()))
					return false;
			}
		}


		return true;
	}

	public Pair<String, String> generateRequestToApproveProductEmailSubjectAndContent(Product product, IdsmedUser vendorSenior) throws IdsmedBaseException {
		EmailTemplateResponse template = emailNotificationConfigService
				.getEmailNotificationConfigByEmailType(EmailTypeEnum.REQUEST_TO_APPROVE_PRODUCT_NOTIFICATION)
				.getEmailTemplate();

		String emailSubject = template.getSubject();
		String emailContent = template.getContent();

		Map<String, String> wildCard = new HashMap<>();
		wildCard.put(NotificationWildcardConstant.RECIPIENT_NAME, vendorSenior.getFirstName() + " " + vendorSenior.getLastName());
		wildCard.put(NotificationWildcardConstant.PRODUCT_NAME, product.getProductNamePrimary());
		wildCard.put(NotificationWildcardConstant.PRODUCT_MODEL, !StringUtils.isEmpty(product.getProductModel()) ? product.getProductModel() : "");
		wildCard.put(NotificationWildcardConstant.PRODUCT_CODE, product.getProductCode());

		emailContent = processReplaceEmailTemplateWildCard(wildCard, emailContent);

		Pair<String, String> emailPair = Pair.of(emailSubject, emailContent);

		return emailPair;
	}

	public Pair<String, String> generateRequestToApproveProductCertificateEmailSubjectAndContent(Product product,
																								 String attachmentDocumentName,
																								 IdsmedUser vendorSenior) throws IdsmedBaseException {
		EmailTemplateResponse template = emailNotificationConfigService
				.getEmailNotificationConfigByEmailType(EmailTypeEnum.REQUEST_TO_APPROVE_PRODUCT_CERTIFICATE_NOTIFICATION)
				.getEmailTemplate();

		String emailSubject = template.getSubject();
		String emailContent = template.getContent();

		Map<String, String> wildCard = new HashMap<>();
		wildCard.put(NotificationWildcardConstant.RECIPIENT_NAME, vendorSenior.getFirstName() + " " + vendorSenior.getLastName());
		wildCard.put(NotificationWildcardConstant.PRODUCT_NAME, product.getProductNamePrimary());
		wildCard.put(NotificationWildcardConstant.PRODUCT_DOCUMENT_NAME, attachmentDocumentName);

		emailContent = processReplaceEmailTemplateWildCard(wildCard, emailContent);

		Pair<String, String> emailPair = Pair.of(emailSubject, emailContent);

		return emailPair;
	}

	public Pair<String, String> generateRequestToApproveVendorCertificateEmailSubjectAndContent(IdsmedUser vendorCoordinator,
																								String attachmentDocumentName,
																								String status) throws IdsmedBaseException {
		EmailTemplateResponse template = emailNotificationConfigService
				.getEmailNotificationConfigByEmailType(EmailTypeEnum.REQUEST_TO_APPROVE_VENDOR_CERTIFICATE_NOTIFICATION)
				.getEmailTemplate();

		String emailSubject = template.getSubject();
		String emailContent = template.getContent();

		Map<String, String> wildCard = new HashMap<>();
		wildCard.put(NotificationWildcardConstant.RECIPIENT_NAME, vendorCoordinator.getFirstName() + " " + vendorCoordinator.getLastName());
		wildCard.put(NotificationWildcardConstant.VENDOR_DOCUMENT_NAME, attachmentDocumentName);
		wildCard.put(NotificationWildcardConstant.VENDOR_DOCUMENT_APP_STATUS, status);

		emailContent = processReplaceEmailTemplateWildCard(wildCard, emailContent);

		Pair<String, String> emailPair = Pair.of(emailSubject, emailContent);

		return emailPair;
	}

    public Pair<String, String> generateVendorProfileCertificateApprovalEmailSubjectAndContent(Vendor vendor,
                                                                                               String attachmentDocumentName,
                                                                                               String status) throws IdsmedBaseException {
        EmailTemplateResponse template = emailNotificationConfigService
                .getEmailNotificationConfigByEmailType(EmailTypeEnum.VENDOR_PROFILE_CERTIFICATE_APPROVAL_NOTIFICATION)
                .getEmailTemplate();

        String emailSubject = template.getSubject();
        String emailContent = template.getContent();

        Map<String, String> wildCard = new HashMap<>();
        wildCard.put(NotificationWildcardConstant.VENDOR_NAME, vendor.getCompanyNamePrimary());
        wildCard.put(NotificationWildcardConstant.VENDOR_DOCUMENT_NAME, attachmentDocumentName);
        wildCard.put(NotificationWildcardConstant.VENDOR_DOCUMENT_APP_STATUS, status);

        emailContent = processReplaceEmailTemplateWildCard(wildCard, emailContent);

        Pair<String, String> emailPair = Pair.of(emailSubject, emailContent);

        return emailPair;
    }

    public Pair<String, String> generateHospitalUserRegistrationActivateLinkEmailSubjectAndContent(IdsmedUser hospitalUser, String langCode) throws IdsmedBaseException {
		EmailTemplateResponse template = emailNotificationConfigService
				.getEmailNotificationConfigByEmailType(EmailTypeEnum.HOSPITAL_USER_REGISTRATION_ACTIVATION_LINK)
				.getEmailTemplate();
		String emailSubject = template.getSubject();
		String emailContent = template.getContent();
		Map<String, String> wildCard = new HashMap<>();
		String firstName = !StringUtils.isEmpty(hospitalUser.getFirstName()) ? hospitalUser.getFirstName(): "";
		String lastName = !StringUtils.isEmpty(hospitalUser.getLastName()) ? hospitalUser.getLastName() : "";
		String userName = firstName + lastName;
		String langCodeParam = !StringUtils.isEmpty(langCode) ? langCode : LangEnum.ENGLISH.getCode();
		String baseLink = env.getProperty(PropertiesLabelConst.EMAIL_USER_REGISTRATION_ACTIVATION_LINK);
		if (environment.equals("local")) {
			baseLink = baseLink.replace("/frontend", "");
		}
		String activationLink = baseLink + "?uuid=" + hospitalUser.getUuid()+ "&langCode=" + langCodeParam + "&type=" + hospitalUser.getTypeOfUser();

		wildCard.put(NotificationWildcardConstant.USER_NAME, userName);
		wildCard.put(NotificationWildcardConstant.USER_ACTIVATION_URL, activationLink);
		emailContent = processReplaceEmailTemplateWildCard(wildCard, emailContent);

		Pair<String, String> emailPair = Pair.of(emailSubject, emailContent);
		return emailPair;
	}

	public Pair<String, String> generateHospitalUserRegistrationActivationSuccessEmailSubjectAndContent(IdsmedUser hospitalUser) throws IdsmedBaseException {
		EmailTemplateResponse template = emailNotificationConfigService
				.getEmailNotificationConfigByEmailType(EmailTypeEnum.HOSPITAL_USER_REGISTRATION_SUCCESS)
				.getEmailTemplate();
		String emailSubject = template.getSubject();
		String emailContent = template.getContent();
		Map<String, String> wildCard = new HashMap<>();
		String firstName = !StringUtils.isEmpty(hospitalUser.getFirstName()) ? hospitalUser.getFirstName(): "";
		String lastName = !StringUtils.isEmpty(hospitalUser.getLastName()) ? hospitalUser.getLastName() : "";
		String userName = firstName + lastName;

		wildCard.put(NotificationWildcardConstant.USER_NAME, userName);

		emailContent = processReplaceEmailTemplateWildCard(wildCard, emailContent);

		Pair<String, String> emailPair = Pair.of(emailSubject, emailContent);
		return emailPair;
	}

	public Pair<String, String> generatedInactiveOrSuspendedVendorEmailSubjectAndContent(IdsmedUser idsmedUser, String vendorName, String suspendedReason)throws IdsmedBaseException{
		EmailTemplateResponse template = emailNotificationConfigService.getEmailNotificationConfigByEmailType(EmailTypeEnum.INACTIVE_SUSPENDED_VENDOR_PROFILE).getEmailTemplate();
		String emailSubject = template.getSubject();
		String emailContent = template.getContent();
		Map<String, String> wildCard = new HashMap<>();
		String userName = idsmedUser.getFirstName() + " " + idsmedUser.getLastName();
		wildCard.put(NotificationWildcardConstant.RECIPIENT_NAME, userName);
		wildCard.put(NotificationWildcardConstant.VENDOR_NAME, vendorName);
		wildCard.put(NotificationWildcardConstant.INACTIVE_SUSPENDED_REASON, suspendedReason);
		emailContent = processReplaceEmailTemplateWildCard(wildCard, emailContent);
		Pair<String, String> emailPair = Pair.of(emailSubject, emailContent);

		return emailPair;
	}

	public Pair<String, String> generatedTerminateVendorEmailSubjectAndContent(IdsmedUser idsmedUser, String vendorName, String terminateReason)throws IdsmedBaseException{
		EmailTemplateResponse template = emailNotificationConfigService.getEmailNotificationConfigByEmailType(EmailTypeEnum.TERMINATE_VENDOR_PROFILE).getEmailTemplate();
		String emailSubject = template.getSubject();
		String emailContent = template.getContent();
		Map<String, String> wildCard = new HashMap<>();
		String userName = idsmedUser.getFirstName() + " " + idsmedUser.getLastName();
		wildCard.put(NotificationWildcardConstant.RECIPIENT_NAME, userName);
		wildCard.put(NotificationWildcardConstant.VENDOR_NAME, vendorName);
		wildCard.put(NotificationWildcardConstant.TERMINATION_REASON, terminateReason);
		emailContent = processReplaceEmailTemplateWildCard(wildCard, emailContent);
		Pair<String, String> emailPair = Pair.of(emailSubject, emailContent);

		return emailPair;
	}

	public Pair<String, String> generateCorporateUserRegistrationActivateLinkEmailSubjectAndContent(IdsmedUser corporateUser, String langCode) throws IdsmedBaseException {
		EmailTemplateResponse template = emailNotificationConfigService
				.getEmailNotificationConfigByEmailType(EmailTypeEnum.CORPORATE_USER_REGISTRATION_ACTIVATION_LINK)
				.getEmailTemplate();
		String emailSubject = template.getSubject();
		String emailContent = template.getContent();
		Map<String, String> wildCard = new HashMap<>();
		String firstName = !StringUtils.isEmpty(corporateUser.getFirstName()) ? corporateUser.getFirstName(): "";
		String lastName = !StringUtils.isEmpty(corporateUser.getLastName()) ? corporateUser.getLastName() : "";
		String userName = firstName + lastName;
		String langCodeParam = !StringUtils.isEmpty(langCode) ? langCode : LangEnum.ENGLISH.getCode();
		String baseLink = env.getProperty(PropertiesLabelConst.EMAIL_USER_REGISTRATION_ACTIVATION_LINK);
		if (environment.equals("local")) {
			baseLink = baseLink.replace("/frontend", "");
		}
		String activationLink = baseLink + "?uuid=" + corporateUser.getUuid()+ "&langCode=" + langCodeParam + "&type=" + corporateUser.getTypeOfUser();

		wildCard.put(NotificationWildcardConstant.USER_NAME, userName);
		wildCard.put(NotificationWildcardConstant.USER_ACTIVATION_URL, activationLink);
		emailContent = processReplaceEmailTemplateWildCard(wildCard, emailContent);

		Pair<String, String> emailPair = Pair.of(emailSubject, emailContent);
		return emailPair;
	}

	public Pair<String, String> generateCorporateUserRegistrationActivationSuccessEmailSubjectAndContent(IdsmedUser corporateUser) throws IdsmedBaseException {
		EmailTemplateResponse template = emailNotificationConfigService
				.getEmailNotificationConfigByEmailType(EmailTypeEnum.CORPORATE_USER_REGISTRATION_SUCCESS)
				.getEmailTemplate();
		String emailSubject = template.getSubject();
		String emailContent = template.getContent();
		Map<String, String> wildCard = new HashMap<>();
		String firstName = !StringUtils.isEmpty(corporateUser.getFirstName()) ? corporateUser.getFirstName(): "";
		String lastName = !StringUtils.isEmpty(corporateUser.getLastName()) ? corporateUser.getLastName() : "";
		String userName = firstName + lastName;

		wildCard.put(NotificationWildcardConstant.USER_NAME, userName);

		emailContent = processReplaceEmailTemplateWildCard(wildCard, emailContent);

		Pair<String, String> emailPair = Pair.of(emailSubject, emailContent);
		return emailPair;
	}

	public Pair<String, String> generateIndividualUserRegistrationActivateLinkEmailSubjectAndContent(IdsmedUser individualUser, String langCode) throws IdsmedBaseException {
		EmailTemplateResponse template = emailNotificationConfigService
				.getEmailNotificationConfigByEmailType(EmailTypeEnum.INDIVIDUAL_USER_REGISTRATION_ACTIVATION_LINK)
				.getEmailTemplate();
		String emailSubject = template.getSubject();
		String emailContent = template.getContent();
		Map<String, String> wildCard = new HashMap<>();
		String firstName = !StringUtils.isEmpty(individualUser.getFirstName()) ? individualUser.getFirstName(): "";
		String lastName = !StringUtils.isEmpty(individualUser.getLastName()) ? individualUser.getLastName() : "";
		String userName = firstName + lastName;
		String langCodeParam = !StringUtils.isEmpty(langCode) ? langCode : LangEnum.ENGLISH.getCode();
		String baseLink = env.getProperty(PropertiesLabelConst.EMAIL_USER_REGISTRATION_ACTIVATION_LINK);
		if (environment.equals("local")) {
			baseLink = baseLink.replace("/frontend", "");
		}
		String activationLink = baseLink + "?uuid=" + individualUser.getUuid()+ "&langCode=" + langCodeParam + "&type=" + individualUser.getTypeOfUser();

		wildCard.put(NotificationWildcardConstant.USER_NAME, userName);
		wildCard.put(NotificationWildcardConstant.USER_ACTIVATION_URL, activationLink);
		emailContent = processReplaceEmailTemplateWildCard(wildCard, emailContent);

		Pair<String, String> emailPair = Pair.of(emailSubject, emailContent);
		return emailPair;
	}

	public Pair<String, String> generateIndividualUserRegistrationActivationSuccessEmailSubjectAndContent(IdsmedUser individualUser) throws IdsmedBaseException {
		EmailTemplateResponse template = emailNotificationConfigService
				.getEmailNotificationConfigByEmailType(EmailTypeEnum.INDIVIDUAL_USER_REGISTRATION_SUCCESS)
				.getEmailTemplate();
		String emailSubject = template.getSubject();
		String emailContent = template.getContent();
		Map<String, String> wildCard = new HashMap<>();
		String firstName = !StringUtils.isEmpty(individualUser.getFirstName()) ? individualUser.getFirstName(): "";
		String lastName = !StringUtils.isEmpty(individualUser.getLastName()) ? individualUser.getLastName() : "";
		String userName = firstName + lastName;

		wildCard.put(NotificationWildcardConstant.USER_NAME, userName);

		emailContent = processReplaceEmailTemplateWildCard(wildCard, emailContent);

		Pair<String, String> emailPair = Pair.of(emailSubject, emailContent);
		return emailPair;
	}

	public Pair<String, String> generatedInactiveOrSuspendedProductEmailSubjectAndContent(IdsmedUser idsmedUser, String vendorName, Product product, String suspendedReason)throws IdsmedBaseException{
		EmailTemplateResponse template = emailNotificationConfigService.getEmailNotificationConfigByEmailType(EmailTypeEnum.INACTIVE_SUSPENDED_PRODUCT).getEmailTemplate();
		String emailSubject = template.getSubject();
		String emailContent = template.getContent();
		Map<String, String> wildCard = new HashMap<>();
		String userName = idsmedUser.getFirstName() + " " + idsmedUser.getLastName();
		wildCard.put(NotificationWildcardConstant.RECIPIENT_NAME, userName);
		wildCard.put(NotificationWildcardConstant.VENDOR_NAME, vendorName);
		wildCard.put(NotificationWildcardConstant.PRODUCT_CODE, product.getProductCode() != null?product.getProductCode():null);
		wildCard.put(NotificationWildcardConstant.INACTIVE_SUSPENDED_REASON, suspendedReason != null?suspendedReason:null);
		emailContent = processReplaceEmailTemplateWildCard(wildCard, emailContent);
		Pair<String, String> emailPair = Pair.of(emailSubject, emailContent);

		return emailPair;
	}

    private String compileURL(String url, String langCode) {
		String hyperlink = "";

		if (LangEnum.ENGLISH.getCode().equalsIgnoreCase(langCode)) {
			hyperlink = "<a href=\"" + url + "\"><strong><font size=\"4\">here</font></strong></a>";
		}

		if (LangEnum.CHINA.getCode().equalsIgnoreCase(langCode)) {
			hyperlink = "<a href=\"" + url + "\"><strong><font size=\"4\">链接</font></strong></a>";
		}

		if (LangEnum.CHINA_TAIWAN.getCode().equalsIgnoreCase(langCode)) {
			hyperlink = "<a href=\"" + url + "\"><strong><font size=\"4\">鏈接</font></strong></a>";
		}

		if (LangEnum.THAI.getCode().equalsIgnoreCase(langCode)) {
			hyperlink = "<a href=\"" + url + "\"><strong><font size=\"4\">here</font></strong></a>";
		}

		if (LangEnum.VIETNAMESE.getCode().equalsIgnoreCase(langCode)) {
			hyperlink = "<a href=\"" + url + "\"><strong><font size=\"4\">here</font></strong></a>";
		}

		if (LangEnum.INDONESIAN.getCode().equalsIgnoreCase(langCode)) {
			hyperlink = "<a href=\"" + url + "\"><strong><font size=\"4\">here</font></strong></a>";
		}

		return hyperlink;
	}
}

package com.cmg.idsmed.mail;

import org.springframework.util.CollectionUtils;


public class EmailUtils {
	public static String generateRecipientsString(EmailObject emailObject) {
		StringBuilder recipientsStr = new StringBuilder();
		if (!CollectionUtils.isEmpty(emailObject.getRecipients())) {
			emailObject.getRecipients().forEach(rcp -> {
				recipientsStr.append(rcp);
				recipientsStr.append(";");
			});
		}

		if (!CollectionUtils.isEmpty(emailObject.getCcRecipient())) {
			emailObject.getCcRecipient().forEach(rcp -> {
				recipientsStr.append(rcp);
				recipientsStr.append(";");
			});
		}

		if (!CollectionUtils.isEmpty(emailObject.getBccRecipients())) {
			emailObject.getBccRecipients().forEach(rcp -> {
				recipientsStr.append(rcp);
				recipientsStr.append(";");
			});
		}
		return recipientsStr.toString();
	}
}

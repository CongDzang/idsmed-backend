package com.cmg.idsmed.mail;

import com.cmg.idsmed.model.entity.common.MailSendingLog;

/**
 *
 */
public class GeneralEmailObject extends EmailObject {
	public GeneralEmailObject() {
		super();
	}

	
	@Override
	public EmailObject generateFailingNotificationMail (Long logId, String errorMessage) {
		//do nothing
		return null;
	}
	
	@Override
	public MailSendingLog generateFailMailSendingLog (String errorMessage) {
		//do nothing
		return null;
	}
	
	@Override
	public MailSendingLog generateSuccessMailSendingLog () {
		//do nothing
		return null;
	}
}

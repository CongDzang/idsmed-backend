package com.cmg.idsmed.mail;

import java.util.ArrayList;
import java.util.List;

import com.cmg.idsmed.common.enums.EmailErrorEnum;
import org.springframework.data.util.Pair;
import org.springframework.util.StringUtils;

import com.cmg.idsmed.common.enums.EmailStatusEnum;
import com.cmg.idsmed.model.entity.common.MailSendingLog;

public class ProductCSVProcessReportEmailObject extends EmailObject {

	public ProductCSVProcessReportEmailObject() {
		super();
	}

	public ProductCSVProcessReportEmailObject(List<String> recipients, String sendFrom, Pair<String, String> emailPair) {
		super();
		this.setRecipients(recipients);
		this.setSendFrom(sendFrom);
		this.setSubject(emailPair.getFirst());
		this.setContent(emailPair.getSecond());
	}

	@Override
	public EmailObject generateFailingNotificationMail (Long logId, String errorMessage) {

		EmailObject failNotificationMail = new GeneralEmailObject();

		List<String> recipients = new ArrayList<>();

		if (StringUtils.isEmpty(this.getSendFrom())) {
			recipients.add(this.getSendFrom());
		}
		String recipientsStr = EmailUtils.generateRecipientsString(this);
		failNotificationMail.setRecipients(recipients);
		failNotificationMail.setSendFrom("Auto generate mail service");
		failNotificationMail.setSubject("Fail for batch upload products email");
		failNotificationMail.setContent("The emai sending to " + recipientsStr + " has error: <br/>" + errorMessage + "<br /> Please check detail in mail sending log with Id : " + logId.toString());

		return failNotificationMail;
	}

	@Override
	public MailSendingLog generateFailMailSendingLog (String errorMessage) {

		MailSendingLog failLog = new MailSendingLog();
		failLog.setMailPurpose("Product Batch Upload failure notification mail");
		failLog.setSender(this.getSendFrom());
		String recipientsStr = EmailUtils.generateRecipientsString(this);
		failLog.setRecipients(recipientsStr);
		failLog.setResult(EmailStatusEnum.SEND_FAILD.getName());
		failLog.setErrorCode(EmailErrorEnum.UNKNOWN_ERROR.getCode());
		failLog.setErrorDescription(errorMessage);
		failLog.setFollowUpAction("Contact vendor for manual notification or send mail again");

		return failLog;
	}

	@Override
	public MailSendingLog generateSuccessMailSendingLog () {

		MailSendingLog failLog = new MailSendingLog();
		failLog.setMailPurpose("Product batch upload successful mail");
		failLog.setSender(this.getSendFrom());
		String recipientsStr = EmailUtils.generateRecipientsString(this);
		failLog.setRecipients(recipientsStr);
		failLog.setResult("Success");
		failLog.setFollowUpAction("No action");

		return failLog;
	}
}

package com.cmg.idsmed.mail;

import com.cmg.idsmed.common.enums.EmailErrorEnum;
import com.cmg.idsmed.common.enums.EmailStatusEnum;
import com.cmg.idsmed.common.exception.IdsmedBaseException;
import com.cmg.idsmed.model.entity.common.MailSendingLog;
import org.springframework.data.util.Pair;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class ProductCertificateApprovalEmailObject extends EmailObject {

    private String loginId;

    private String vendorName;

    private String productNamePrimary;

    private String productModel;

    private String productCode;

    private String status;

    public ProductCertificateApprovalEmailObject() {
        super();
    }

    public ProductCertificateApprovalEmailObject(List<String> recipients, String sendFrom, Pair<String, String> emailPair) throws IdsmedBaseException {
        super();

        this.setSubject(emailPair.getFirst());
        this.setContent(emailPair.getSecond());
        this.setSendFrom(sendFrom);
        this.setRecipients(recipients);
    }

    @Override
    public MailSendingLog generateSuccessMailSendingLog() {
        String recipientsStr = EmailUtils.generateRecipientsString(this);
        MailSendingLog failLog = new MailSendingLog();
        failLog.setMailPurpose("Product Attachment Approval Notification Mail");
        failLog.setSender(this.getSendFrom());
        failLog.setRecipients(recipientsStr);
        failLog.setResult("Success");
        failLog.setFollowUpAction("No action. Product approved : " + this.productNamePrimary);

        return failLog;
    }

    @Override
    public EmailObject generateFailingNotificationMail(Long logId, String errorMessage) {
        EmailObject failNotificationMail = new GeneralEmailObject();
        List<String> recipients = new ArrayList<>();

        if (StringUtils.isEmpty(this.getSendFrom())) {
            recipients.add(this.getSendFrom());
        }
        failNotificationMail.setRecipients(recipients);
        failNotificationMail.setSendFrom("Auto generate mail service");
        failNotificationMail.setSubject("Fail for sending product certificate approval email");
        failNotificationMail.setContent("The email sending to " + errorMessage + " can not deliver. <br /> Please check detail in mail sending log with Id : " + logId.toString());

        return failNotificationMail;
    }

    @Override
    public MailSendingLog generateFailMailSendingLog(String errorMessage) {
        MailSendingLog failLog = new MailSendingLog();
        failLog.setMailPurpose("Product Certificate Approval Mail");
        failLog.setSender(this.getSendFrom());

        String recipientsStr = EmailUtils.generateRecipientsString(this);
        failLog.setRecipients(recipientsStr);
        failLog.setResult(EmailStatusEnum.SEND_FAILD.getName());
        failLog.setErrorCode(EmailErrorEnum.UNKNOWN_ERROR.getCode());
        failLog.setErrorDescription(errorMessage);
        failLog.setFollowUpAction("Contact vendor and remind them by phone");
        failLog.setSubject(this.getSubject());
        failLog.setContent(this.getContent());

        return failLog;
    }

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getProductNamePrimary() {
        return productNamePrimary;
    }

    public void setProductNamePrimary(String productNamePrimary) {
        this.productNamePrimary = productNamePrimary;
    }

    public String getProductModel() {
        return productModel;
    }

    public void setProductModel(String productModel) {
        this.productModel = productModel;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}

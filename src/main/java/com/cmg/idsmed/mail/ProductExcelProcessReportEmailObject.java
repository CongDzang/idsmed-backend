package com.cmg.idsmed.mail;

/**
 *
 */
//public class ProductExcelProcessReportEmailObject extends EmailObject
public class ProductExcelProcessReportEmailObject {
//	private String companyCode;
//
//	public ProductExcelProcessReportEmailObject() {
//		super();
//	}
//
//	public ProductExcelProcessReportEmailObject(String recipient, String sendFrom, String subject, String content) {
//		super();
//		this.setRecipient(recipient);
//		this.setSendFrom(sendFrom);
//		this.setSubject(subject);
//		this.setContent(content);
//	}
//
//	public ProductExcelProcessReportEmailObject(String vendorEmail, String sendFrom, String originalFileName, Integer totalCreatedProduct,String errorRowInfo, String companyCode) {
//		super();
//		this.setSubject("IDSMed - Product Excel process report");
//		StringBuilder content = new StringBuilder();
//		content.append("Thank you for using Idsmed System. Your Excel file ");
//		content.append(originalFileName);
//		content.append(" has been processed, the result as below:");
//		content.append("<br /> Total created product: ");
//		content.append(totalCreatedProduct);
//		content.append("<br /> Total Fail created product on Excel file: ");
//		content.append(errorRowInfo == null?0:errorRowInfo);
//		this.setContent(content.toString());
//		this.setSendFrom(sendFrom);
//		this.setRecipient(vendorEmail);
//		this.setCompanyCode(companyCode);
//	}
//
//
//	@Override
//	public EmailObject generateFailingNotificationMail (Long logId) {
//
//		EmailObject failNotificationMail = new GeneralEmailObject();
//		failNotificationMail.setRecipient(this.getSendFrom());
//		failNotificationMail.setSendFrom("Auto generate mail service");
//		failNotificationMail.setSubject("Fail for sending ProductExcel process report email");
//		failNotificationMail.setContent("The email sending to " + this.getRecipient() + " can not deliver. <br /> Please check detail in mail sending log with Id : " + logId.toString());
//
//		return failNotificationMail;
//	}
//
//	@Override
//	public MailSendingLog generateFailMailSendingLog () {
//
//		MailSendingLog failLog = new MailSendingLog();
//		failLog.setMailPurpose(this.getSubject());
//		failLog.setSender(this.getSendFrom());
//		failLog.setRecipients(this.getRecipient());
//		failLog.setResult(EmailStatusEnum.SEND_FAILD.getName());
//		failLog.setErrorCode(EmailErrorEnum.UNKNOWN_ERROR.getCode());
//		failLog.setErrorDescription(EmailErrorEnum.UNKNOWN_ERROR.getName());
//		failLog.setFollowUpAction("Contact vendor to support them, Vendor code:" + this.companyCode);
//
//		return failLog;
//	}
//
//	@Override
//	public MailSendingLog generateSuccessMailSendingLog () {
//
//		MailSendingLog failLog = new MailSendingLog();
//		failLog.setMailPurpose(EmailTypeEnum.PRODUCT_EXCEL_PROCESS_REPORT.getName());
//		failLog.setSender(this.getSendFrom());
//		failLog.setRecipients(this.getRecipient());
//		failLog.setResult(EmailStatusEnum.SENT.getName());
//		failLog.setFollowUpAction("Finish");
//
//		return failLog;
//	}
//
//	public String getCompanyCode() {
//		return companyCode;
//	}
//
//	public void setCompanyCode(String companyCode) {
//		this.companyCode = companyCode;
//	}
}

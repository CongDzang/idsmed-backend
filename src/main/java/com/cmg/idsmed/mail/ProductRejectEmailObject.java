package com.cmg.idsmed.mail;

import com.cmg.idsmed.common.enums.EmailErrorEnum;
import com.cmg.idsmed.common.enums.EmailStatusEnum;
import com.cmg.idsmed.common.exception.IdsmedBaseException;
import com.cmg.idsmed.model.entity.common.MailSendingLog;
import org.springframework.data.util.Pair;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class ProductRejectEmailObject extends EmailObject{

	private String productNamePrimary;

	public ProductRejectEmailObject() {
		super();
	}

    public ProductRejectEmailObject(List<String> recipients, String sendFrom, String subject, String content) {
		super();
		this.setRecipients(recipients);
		this.setSendFrom(sendFrom);
		this.setSubject(subject);
		this.setContent(content);
	}

	public ProductRejectEmailObject(String recipient, String sendFrom, String productNamePrimary) {
		super();
        List<String> receipients = new ArrayList<>();

        if (!StringUtils.isEmpty(recipient)) {
            receipients.add(recipient);
        }
		this.setSubject("IDSMed - Product Rejected Notification");
		this.setContent("Your product has been rejected by IDSMed : " + productNamePrimary);
		this.setSendFrom(sendFrom);
		this.setRecipients(receipients);
		this.productNamePrimary = productNamePrimary;
	}

	public ProductRejectEmailObject(List<String> recipients, String sendFrom, Pair<String, String> emailPair) throws IdsmedBaseException {
		super();

		this.setSubject(emailPair.getFirst());
		this.setContent(emailPair.getSecond());
		this.setSendFrom(sendFrom);
		this.setRecipients(recipients);

	}

	@Override
	public EmailObject generateFailingNotificationMail (Long logId, String errorMessage) {

        EmailObject failNotificationMail = new GeneralEmailObject();
        List<String> recipients = new ArrayList<>();

        if (StringUtils.isEmpty(this.getSendFrom())) {
            recipients.add(this.getSendFrom());
        }
		failNotificationMail.setRecipients(recipients);
		failNotificationMail.setSendFrom("Auto generate mail service");
		failNotificationMail.setSubject("Fail for sending product reject email");
		failNotificationMail.setContent("The email sending to " + errorMessage + " can not deliver. <br /> Please check detail in mail sending log with Id : " + logId.toString());

		return failNotificationMail;
	}

	@Override
	public MailSendingLog generateSuccessMailSendingLog () {
        String recipientsStr = EmailUtils.generateRecipientsString(this);
        MailSendingLog failLog = new MailSendingLog();
		failLog.setMailPurpose("Product reject notification mail");
		failLog.setSender(this.getSendFrom());
		failLog.setRecipients(recipientsStr);
		failLog.setResult("Success");
		failLog.setFollowUpAction("No action. Product reject : " + this.productNamePrimary);

		return failLog;
	}

    @Override
    public MailSendingLog generateFailMailSendingLog(String errorMessage) {
        MailSendingLog failLog = new MailSendingLog();
        failLog.setMailPurpose("Product Reject notification mail");
        failLog.setSender(this.getSendFrom());

        String recipientsStr = EmailUtils.generateRecipientsString(this);
        failLog.setRecipients(recipientsStr);
		failLog.setResult(EmailStatusEnum.SEND_FAILD.getName());
		failLog.setErrorCode(EmailErrorEnum.UNKNOWN_ERROR.getCode());
		failLog.setErrorDescription(errorMessage);
		failLog.setFollowUpAction("Contact vendor and remind them by phone");
		failLog.setSubject(this.getSubject());
		failLog.setContent(this.getContent());

        return failLog;
    }

	public String getProductNamePrimary() {
		return productNamePrimary;
	}

	public void setProductNamePrimary(String productNamePrimary) {
		this.productNamePrimary = productNamePrimary;
	}

}

package com.cmg.idsmed.mail;

import com.cmg.idsmed.common.enums.EmailErrorEnum;
import com.cmg.idsmed.common.enums.EmailStatusEnum;
import com.cmg.idsmed.model.entity.common.MailSendingLog;
import org.springframework.data.util.Pair;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class RequestToApproveProductCertificateEmailObject extends EmailObject {

    public RequestToApproveProductCertificateEmailObject() {}

    public RequestToApproveProductCertificateEmailObject(List<String> recipients, String sendFrom, Pair<String, String> emailPair) {
        super();
        this.setSubject(emailPair.getFirst());
        this.setContent(emailPair.getSecond());
        this.setSendFrom(sendFrom);
        this.setRecipients(recipients);
    }

    @Override
    public EmailObject generateFailingNotificationMail(Long logId, String errorMessage) {
        EmailObject failNotificationMail = new GeneralEmailObject();
        List<String> recipients = new ArrayList<>();

        if (StringUtils.isEmpty(this.getSendFrom())) {
            recipients.add(this.getSendFrom());
        }
        failNotificationMail.setRecipients(recipients);
        failNotificationMail.setSendFrom("Auto generate mail service");
        failNotificationMail.setSubject("Fail to send request to approve product certificate email");
        failNotificationMail.setContent("The email sending to " + errorMessage + " can not deliver. <br /> Please check detail in mail sending log with Id : " + logId.toString());

        return failNotificationMail;
    }

    @Override
    public MailSendingLog generateFailMailSendingLog(String errorMessage) {
        MailSendingLog failLog = new MailSendingLog();
        failLog.setMailPurpose("Request to Approve Product Certificate Mail");
        failLog.setSender(this.getSendFrom());

        String recipientsStr = EmailUtils.generateRecipientsString(this);
        failLog.setRecipients(recipientsStr);
        failLog.setResult(EmailStatusEnum.SEND_FAILD.getName());
        failLog.setErrorCode(EmailErrorEnum.UNKNOWN_ERROR.getCode());
        failLog.setErrorDescription(errorMessage);
        failLog.setFollowUpAction("Contact any product approver to approve product certificate");
        failLog.setSubject(this.getSubject());
        failLog.setContent(this.getContent());

        return failLog;
    }

    @Override
    public MailSendingLog generateSuccessMailSendingLog() {
        String recipientsStr = EmailUtils.generateRecipientsString(this);
        MailSendingLog failLog = new MailSendingLog();
        failLog.setMailPurpose("Request to Approve Product Certificate notification mail");
        failLog.setSender(this.getSendFrom());
        failLog.setRecipients(recipientsStr);
        failLog.setResult("Success");
        failLog.setFollowUpAction("No action.");

        return failLog;
    }
}

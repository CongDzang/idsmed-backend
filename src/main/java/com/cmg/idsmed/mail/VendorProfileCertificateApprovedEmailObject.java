package com.cmg.idsmed.mail;

import com.cmg.idsmed.common.enums.EmailErrorEnum;
import com.cmg.idsmed.common.enums.EmailStatusEnum;
import com.cmg.idsmed.model.entity.common.MailSendingLog;
import org.springframework.data.util.Pair;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class VendorProfileCertificateApprovedEmailObject extends EmailObject {
	private String companyCode;
	public VendorProfileCertificateApprovedEmailObject() {
		super();
	}


	public VendorProfileCertificateApprovedEmailObject(List<String> recipients, String sendFrom, Pair<String, String> emailPair) {
		super();
		this.setSubject(emailPair.getFirst());
		this.setContent(emailPair.getSecond());
		this.setSendFrom(sendFrom);
		this.setRecipients(recipients);
	}
	@Override
	public EmailObject generateFailingNotificationMail (Long logId, String errorMessage) {

		EmailObject failNotificationMail = new GeneralEmailObject();

		List<String> recipients = new ArrayList<>();

		if (StringUtils.isEmpty(this.getSendFrom())) {
			recipients.add(this.getSendFrom());
		}
		String recipientsStr = EmailUtils.generateRecipientsString(this);
		failNotificationMail.setRecipients(recipients);
		failNotificationMail.setSendFrom("Auto generate mail service");
		failNotificationMail.setSubject("Fail for sending VendorProfileCertificateApproved email");
		failNotificationMail.setContent("The email sending to " + recipientsStr + " has error: <br/>" + errorMessage + "<br /> Please check detail in mail sending log with Id : " + logId.toString());

		return failNotificationMail;
	}

	@Override
	public MailSendingLog generateFailMailSendingLog (String errorMessage) {

		MailSendingLog failLog = new MailSendingLog();
		failLog.setMailPurpose("Vendor Profile Certificate Approved mail");
		failLog.setSender(this.getSendFrom());
		String recipientsStr = EmailUtils.generateRecipientsString(this);
		failLog.setRecipients(recipientsStr);
		failLog.setResult(EmailStatusEnum.SEND_FAILD.getName());
		failLog.setErrorCode(EmailErrorEnum.UNKNOWN_ERROR.getCode());
		failLog.setErrorDescription(errorMessage);
		failLog.setFollowUpAction("Contact vendor for manual notification or send mail again. Company Code: " + this.companyCode);

		return failLog;
	}

	@Override
	public MailSendingLog generateSuccessMailSendingLog () {

		MailSendingLog failLog = new MailSendingLog();
		failLog.setMailPurpose("Vendor Profile Certificate Approved mail");
		failLog.setSender(this.getSendFrom());
		String recipientsStr = EmailUtils.generateRecipientsString(this);
		failLog.setRecipients(recipientsStr);
		failLog.setResult("Success");
		failLog.setFollowUpAction("No action. Company code sent : " + this.companyCode);

		return failLog;
	}

	public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}
}

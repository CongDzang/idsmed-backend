package com.cmg.idsmed.mail.sender;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.mail.MessagingException;
import java.io.IOException;

public abstract class MailSender {

	protected static Logger logger = LoggerFactory.getLogger(MailSender.class);

	public static final String TYPE_SMTP = "smtp";
	public static final String TYPE_SENDGRID = "sendgrid";

	public abstract void send(String recipient, String sendFrom, String subject, String content) throws MessagingException;


}

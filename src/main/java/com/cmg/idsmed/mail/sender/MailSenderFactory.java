package com.cmg.idsmed.mail.sender;

import com.cmg.idsmed.mail.sender.smtp.SmtpMailSender;
import org.springframework.core.env.Environment;

public final class MailSenderFactory {

	private MailSenderFactory() {

	}
	public static MailSender newInstance(Environment env) {

		if (env == null) {
			throw new IllegalArgumentException("Environment argument not configured properly");
		}

		String mailType = env.getRequiredProperty("mail.type");

		MailSender mailSender = null;

		if (MailSender.TYPE_SMTP.equalsIgnoreCase(mailType)) {
			mailSender = new SmtpMailSender(env);
		}

		return mailSender;
	}
}

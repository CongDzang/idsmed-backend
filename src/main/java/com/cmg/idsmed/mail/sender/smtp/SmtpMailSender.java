package com.cmg.idsmed.mail.sender.smtp;

import com.cmg.idsmed.common.Constant.PropertiesLabelConst;
import com.cmg.idsmed.mail.sender.MailSender;
import com.fasterxml.jackson.databind.annotation.JsonAppend;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.util.Properties;

public class SmtpMailSender extends MailSender {
	private static Logger logger = LoggerFactory.getLogger(SmtpMailSender.class);

	private JavaMailSenderImpl sender;

	public SmtpMailSender(Environment env) {
		sender = new JavaMailSenderImpl();
		sender.setHost(env.getProperty(PropertiesLabelConst.SMTP_MAIL_HOST_LABEL));

		sender.setPort(Integer.parseInt(env.getProperty(PropertiesLabelConst.SMTP_MAIL_PORT_LABEL)));

		sender.setUsername(env.getProperty(PropertiesLabelConst.SMTP_MAIL_USERNAME_LABEL));
		sender.setPassword(env.getProperty(PropertiesLabelConst.SMTP_MAIL_PASSWORD_LABEL));

		Properties props = sender.getJavaMailProperties();
		props.put("mail.transport.protocol", "smtps");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.startssl.enable", "true");
		props.put("mail.debug", "true");
	}

	@Override
	public void send(String recipient, String sendFrom, String subject, String content) throws MessagingException {

		logger.info("Start sending email with subject: {} from queue with recipient {},", subject, recipient);
		MimeMessage mineMessage = sender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(mineMessage, false, "utf-8");
		mineMessage.setContent(content, "text/html; charset=UTF-8");
		helper.setSubject(subject);
		helper.setTo(recipient);
		helper.setFrom(sendFrom);

		sender.send(mineMessage);
		logger.info("Start sending email with subject: {} from queue with recipient {},", subject, recipient);
	}
}

package com.cmg.idsmed.model.entity;

import com.cmg.idsmed.common.utils.DateTimeHelper;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.time.*;
import java.util.Date;

@MappedSuperclass
public class AbstractEntity implements Serializable {

//    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(pattern = "MM/dd/yyyy HH:mm:ss.SSS")
    @Column(name="created_datetime")
    private LocalDateTime createdDate;

//    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(pattern = "MM/dd/yyyy HH:mm:ss.SSS")
    @Column(name="updated_datetime")
    @JsonIgnore
    private LocalDateTime updatedDate;

    @Column(name = "created_by")
    @NotNull
    private Long createdBy = -1L;

    @Column(name = "updated_by")
    @NotNull
    private Long updatedBy = -1L;

    @Column(name = "status")
    private Integer status = 0;

    @PrePersist
    protected void onCreate() {
        this.createdDate = this.updatedDate = DateTimeHelper.getCurrentTimeUTC();
    }

    @PreUpdate
    protected void onUpdate() {
        this.updatedDate = DateTimeHelper.getCurrentTimeUTC();
    }

    public LocalDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public LocalDateTime getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(LocalDateTime updatedDate) {
        this.updatedDate = updatedDate;
    }

    public Long getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public Long getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Long updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

}

package com.cmg.idsmed.model.entity.auth;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.cmg.idsmed.model.entity.AbstractEntity;
import com.cmg.idsmed.model.entity.notification.AccountNotificationSetting;
import com.cmg.idsmed.model.entity.notification.Notification;
import com.cmg.idsmed.model.entity.notification.NotificationRecipient;
import com.cmg.idsmed.model.entity.product.ProductRatingDetail;
import com.cmg.idsmed.model.entity.subscriber.Subscriber;
import com.cmg.idsmed.model.entity.task.IdsmedTaskExecutor;
import com.cmg.idsmed.model.entity.vendor.TenderCategory;

import java.util.List;

@Entity
@Table(name = "idsmed_account")
public class IdsmedAccount extends AbstractEntity {
	/**
	 * 
	 */
	private static final long serialVersionUID = -9143408511062216690L;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "idsmed_account_id_seq")
	@SequenceGenerator(name = "idsmed_account_id_seq",
			sequenceName = "idsmed_account_id_seq",
			allocationSize = 1)
	@Column(updatable = false)
	private Long id;

	@Column(name = "login_id")
	@NotNull
	@Size(max = 255)
	private String loginId;

	@ManyToOne
	@JoinColumn(name = "idsmed_user_id", referencedColumnName = "id")
	private IdsmedUser idsmedUser;

	@ManyToOne
	@JoinColumn(name = "subscriber_id", referencedColumnName = "id")
	private Subscriber subscriber;

	@Column(name = "password")
	@NotNull
	private String password;

	@Column(name = "type")
	@NotNull
	private Integer type;

	@Column(name = "previous_status")
	private Integer previousStatus;

	// This field is determine whether the idsmed_user is vendor coordinator or not
	@Column(name = "vendor_coordinator")
	private Boolean vendorCoordinator;
	
	@OneToOne(mappedBy = "idsmedAccount")
	private IdsmedAccountSettings accountSettings;

	@OneToMany(mappedBy = "sender")
	private List<Notification> notifications;

	@OneToMany(mappedBy = "recipient")
	private List<NotificationRecipient> notificationRecipients;

	@OneToMany(mappedBy = "idsmedAccount")
	private List<ProductRatingDetail> productRatingDetailList;

	@OneToMany(mappedBy = "idsmedAccount")
	private List<IdsmedTaskExecutor> taskExecutors;

	@OneToMany(mappedBy = "idsmedAccount")
	private List<AccountNotificationSetting> accountNotificationSettingList;

	@OneToMany(mappedBy = "idsmedAccount")
	private List<TenderCategory> tenderCategoryList;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public IdsmedUser getIdsmedUser() {
		return idsmedUser;
	}

	public void setIdsmedUser(IdsmedUser idsmedUser) {
		this.idsmedUser = idsmedUser;
	}

	public Subscriber getSubscriber() {
		return subscriber;
	}

	public void setSubscriber(Subscriber subscriber) {
		this.subscriber = subscriber;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}
	
	public IdsmedAccountSettings getAccountSettings() {
		return accountSettings;
	}

	public void setAccountSettings(IdsmedAccountSettings accountSettings) {
		this.accountSettings = accountSettings;
	}

	public List<Notification> getNotifications() {
		return notifications;
	}

	public void setNotifications(List<Notification> notifications) {
		this.notifications = notifications;
	}

	public List<NotificationRecipient> getNotificationRecipients() { return notificationRecipients; }

	public void setNotificationRecipients(List<NotificationRecipient> notificationRecipients) { this.notificationRecipients = notificationRecipients; }

	public List<ProductRatingDetail> getProductRatingDetailList() { return productRatingDetailList; }

	public void setProductRatingDetailList(List<ProductRatingDetail> productRatingDetailList) { this.productRatingDetailList = productRatingDetailList; }

	public Integer getPreviousStatus() {
		return previousStatus;
	}

	public void setPreviousStatus(Integer previousStatus) {
		this.previousStatus = previousStatus;
	}

	public List<IdsmedTaskExecutor> getTaskExecutors() {
		return taskExecutors;
	}

	public void setTaskExecutors(List<IdsmedTaskExecutor> taskExecutors) {
		this.taskExecutors = taskExecutors;
	}

	public List<AccountNotificationSetting> getAccountNotificationSettingList() { return accountNotificationSettingList; }

	public void setAccountNotificationSettingList(List<AccountNotificationSetting> accountNotificationSettingList) { this.accountNotificationSettingList = accountNotificationSettingList; }

	public List<TenderCategory> getTenderCategoryList() { return tenderCategoryList; }

	public void setTenderCategoryList(List<TenderCategory> tenderCategoryList) { this.tenderCategoryList = tenderCategoryList; }

	public Boolean getVendorCoordinator() { return vendorCoordinator; }

	public void setVendorCoordinator(Boolean vendorCoordinator) { this.vendorCoordinator = vendorCoordinator; }

}

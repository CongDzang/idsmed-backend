package com.cmg.idsmed.model.entity.auth;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import com.cmg.idsmed.model.entity.AbstractEntity;

@Entity
@Table(name = "idsmed_account_settings")
public class IdsmedAccountSettings extends AbstractEntity {
	private static final long serialVersionUID = -2399150737857699745L;

	public IdsmedAccountSettings () {
		super();
	}
	
	public IdsmedAccountSettings (IdsmedAccount account, String currentLangCode) {
		this.idsmedAccount = account;
		this.currentLangCode = currentLangCode;
	}
	
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "idsmed_account_settings_id_seq")
    @SequenceGenerator(name = "idsmed_account_settings_id_seq",
            sequenceName = "idsmed_account_settings_id_seq",
            allocationSize = 1)
    @Column(updatable = false)
    private Long id;

    @Column(name = "current_lang_code")
    @Size(max = 50)
    private String currentLangCode;
    
    @OneToOne
    @JoinColumn(name = "account_id", nullable = true)
    private IdsmedAccount idsmedAccount;
    
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCurrentLangCode() {
		return currentLangCode;
	}

	public void setCurrentLangCode(String currentLangCode) {
		this.currentLangCode = currentLangCode;
	}

	public IdsmedAccount getIdsmedAccount() {
		return idsmedAccount;
	}

	public void setIdsmedAccount(IdsmedAccount idsmedAccount) {
		this.idsmedAccount = idsmedAccount;
	}

}

package com.cmg.idsmed.model.entity.auth;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.cmg.idsmed.model.entity.AbstractEntity;

@Entity
@Table(name = "idsmed_function")
public class IdsmedFunction extends AbstractEntity {
	private static final long serialVersionUID = 8288163902937998173L;

	@Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "idsmed_function_id_seq")
    @SequenceGenerator(name = "idsmed_function_id_seq",
            sequenceName = "idsmed_function_id_seq",
            allocationSize = 1)
    @Column(updatable = false)
    private Long id;
    
    @OneToMany(mappedBy = "function")
    private List<IdsmedPermission> permissionList;

	@Column(name = "code")
    @NotNull
    @Size(max = 100)
    private String code;

    @Column(name = "name")
    @NotNull
    @Size(max = 255)
    private String name;
    
    @Column(name = "description")
    @NotNull
    @Size(max = 3000)
    private String description;
    
    public List<IdsmedPermission> getPermissionList() {
		return permissionList;
	}

	public void setPermissionList(List<IdsmedPermission> permissionList) {
		this.permissionList = permissionList;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}

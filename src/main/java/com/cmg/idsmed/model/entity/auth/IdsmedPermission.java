package com.cmg.idsmed.model.entity.auth;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.cmg.idsmed.model.entity.AbstractEntity;

@Entity
@Table(name = "idsmed_permission")
public class IdsmedPermission extends AbstractEntity {
	private static final long serialVersionUID = -2763162179217640618L;

	@Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "idsmed_permission_id_seq")
    @SequenceGenerator(name = "idsmed_permission_id_seq",
            sequenceName = "idsmed_permission_id_seq",
            allocationSize = 1)
    @Column(updatable = false)
    private Long id;

	
	@ManyToOne
	@JoinColumn(name = "function_id")
	private IdsmedFunction function;

	@Column(name = "code")
    @NotNull
    @Size(max = 100)
    private String code;

    @Column(name = "name")
    @NotNull
    @Size(max = 255)
    private String name;
    
    @Column(name = "description")
    @NotNull
    @Size(max = 3000)
    private String description;
    
    @Column(name = "seq")
    @NotNull
    private Integer seq = 1;
    
    @Column(name = "country_code")
    @Size(max = 10)
    private String countryCode;
    
    @OneToMany(mappedBy = "idsmedPermission")
    private List<IdsmedRolePermission> rolePermissions;

    public IdsmedFunction getFunction() {
		return function;
	}

	public void setFunction(IdsmedFunction function) {
		this.function = function;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getSeq() {
		return seq;
	}

	public void setSeq(Integer seq) {
		this.seq = seq;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<IdsmedRolePermission> getRolePermissions() {
        return rolePermissions;
    }

    public void setRolePermissions(List<IdsmedRolePermission> rolePermissions) {
        this.rolePermissions = rolePermissions;
    }
}

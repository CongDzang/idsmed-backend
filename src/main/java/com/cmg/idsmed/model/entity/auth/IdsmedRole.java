package com.cmg.idsmed.model.entity.auth;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.beans.BeanUtils;

import com.cmg.idsmed.model.entity.AbstractEntity;

/**
 * This entity is using StatusEnum
 */
@Entity
@Table(name = "idsmed_role")
public class IdsmedRole extends AbstractEntity {
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "idsmed_role_id_seq")
    @SequenceGenerator(name = "idsmed_role_id_seq",
            sequenceName = "idsmed_role_id_seq",
            allocationSize = 1)
    @Column(updatable = false)
    private Long id;

    @Column(name = "code")
    @NotNull
    @Size(max = 100)
    private String code;

    @Column(name = "name")
    @NotNull
    @Size(max = 255)
    private String name;
    
    @Column(name = "description")
    @Size(max = 1000)
    private String description;
    
    @Column(name = "country_code")
    @NotNull
    @Size(max = 10)
    private String countryCode;

    //This field uses RoleTypeEnum
    @Column(name = "role_type")
    private Integer roleType;

    @Column(name = "inactive_reason")
    private String inactiveReason;

    @OneToMany(mappedBy = "idsmedRole")
    private List<IdsmedUserRole> userRoles;
    
    @OneToMany(mappedBy = "idsmedRole")
    private List<IdsmedRolePermission> rolePermissions;
    
    public IdsmedRole() {
    	
    }
    
    public IdsmedRole(IdsmedRole role) {
    	BeanUtils.copyProperties(role, this);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCountryCode() {
		return countryCode;
	}

    public Integer getRoleType() {
        return roleType;
    }

    public void setRoleType(Integer roleType) {
        this.roleType = roleType;
    }

    public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public List<IdsmedUserRole> getUserRoles() {
        return userRoles;
    }

    public void setUserRoles(List<IdsmedUserRole> userRoles) {
        this.userRoles = userRoles;
    }
    
    public List<IdsmedRolePermission> getRolePermissions() {
        return rolePermissions;
    }

    public void setRolePermissions(List<IdsmedRolePermission> rolePermissions) {
        this.rolePermissions = rolePermissions;
    }

    public String getInactiveReason() {
        return inactiveReason;
    }

    public void setInactiveReason(String inactiveReason) {
        this.inactiveReason = inactiveReason;
    }
}

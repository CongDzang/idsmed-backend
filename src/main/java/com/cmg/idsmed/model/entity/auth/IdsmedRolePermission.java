package com.cmg.idsmed.model.entity.auth;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import com.cmg.idsmed.model.entity.AbstractEntity;

@Entity
@Table(name = "idsmed_role_permission")
public class IdsmedRolePermission extends AbstractEntity{
	@Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "idsmed_role_permission_id_seq")
    @SequenceGenerator(name = "idsmed_role_permission_id_seq",
            sequenceName = "idsmed_role_permission_id_seq",
            allocationSize = 1)
    @Column(updatable = false)
    private Long id;
	
	@Column(name = "country_code")
	@Size(max = 10)
	private String country_code;
	
    @ManyToOne
    @JoinColumn(name = "role_id", referencedColumnName = "id")
    private IdsmedRole idsmedRole;

    @ManyToOne
    @JoinColumn(name = "permission_id", referencedColumnName = "id")
    private IdsmedPermission idsmedPermission;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public IdsmedRole getIdsmedRole() {
        return idsmedRole;
    }

    public void setIdsmedRole(IdsmedRole idsmedRole) {
        this.idsmedRole = idsmedRole;
    }

    public IdsmedPermission getIdsmedPermission() {
        return idsmedPermission;
    }

    public void setIdsmedPermission(IdsmedPermission idsmedPermission) {
        this.idsmedPermission = idsmedPermission;
    }

	public String getCountry_code() {
		return country_code;
	}

	public void setCountry_code(String country_code) {
		this.country_code = country_code;
	}
}

package com.cmg.idsmed.model.entity.auth;

import com.cmg.idsmed.model.entity.AbstractEntity;
import com.cmg.idsmed.model.entity.subscriber.Subscriber;

import javax.persistence.*;

@Entity
@Table(name = "idsmed_subscriber_role")
public class IdsmedSubscriberRole extends AbstractEntity {
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "idsmed_subscriber_role_id_seq")
    @SequenceGenerator(name = "idsmed_subscriber_role_id_seq",
            sequenceName = "idsmed_subscriber_role_id_seq",
            allocationSize = 1)
    @Column(updatable = false)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "subscriber_id", referencedColumnName = "id")
    private Subscriber subscriber;

    @ManyToOne
    @JoinColumn(name = "role_id", referencedColumnName = "id")
    private IdsmedRole idsmedRole;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Subscriber getSubscriber() {
        return subscriber;
    }

    public void setSubscriber(Subscriber subscriber) {
        this.subscriber = subscriber;
    }

    public IdsmedRole getIdsmedRole() {
        return idsmedRole;
    }

    public void setIdsmedRole(IdsmedRole idsmedRole) {
        this.idsmedRole = idsmedRole;
    }
}

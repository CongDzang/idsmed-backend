package com.cmg.idsmed.model.entity.auth;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.Size;

import com.cmg.idsmed.common.enums.UserTypeEnum;
import com.cmg.idsmed.dto.corporate.CorporateUserRegistrationRequest;
import com.cmg.idsmed.dto.hospital.HospitalUserRegistrationRequest;
import com.cmg.idsmed.dto.user.IdsmedIndividualUserRequest;
import com.cmg.idsmed.dto.user.UserRegistrationRequest;
import com.cmg.idsmed.model.entity.corporate.Corporate;
import com.cmg.idsmed.model.entity.hospital.Hospital;
import org.springframework.beans.BeanUtils;
import org.springframework.format.annotation.DateTimeFormat;

import com.cmg.idsmed.model.entity.AbstractEntity;

/**
 * This entity is using StatusEnum
 */
@Entity
@Table(name = "idsmed_user")
public class IdsmedUser extends AbstractEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2399150737857699745L;

	//We prefer sequence generator than identity. If you use identity it may disable JDBC batching
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "idsmed_user_id_seq")
    @SequenceGenerator(name = "idsmed_user_id_seq",
            sequenceName = "idsmed_user_id_seq",
            allocationSize = 1)
    @Column(updatable = false)
    private Long id;

    @Column(name = "first_name")
    @Size(max = 100)
    private String firstName;

    @Column(name = "last_name")
    @Size(max = 200)
    private String lastName;

    @Column(name = "uuid")
	private String uuid;

    @Column(name = "email")
    @Size(max = 200)
    private String email;

    @Column(name = "country_code")
    @Size(max = 50)
    private String countryCode;
    
    @Column(name = "reject_reason")
    @Size(max = 300)
    private String rejectReason;
    
    @Column(name = "company_code")
    @Size(max = 100)
    private String companyCode;
    
    @Column(name = "qq")
    @Size(max = 100)
    private String qq;
    
    @Column(name = "wechat")
    @Size(max = 100)
    private String wechat;
    
    @Column(name = "phone_number")
    @Size(max = 50)
    private String phoneNumber;

    @Column(name = "profile_image_url")
	@Size(max = 1000)
	private String profileImageUrl;
    
    @Column(name = "approved_by")
	private Long approvedBy;

//    This's using UserTypeEnum
    @Column(name = "type_of_user")
    private Integer typeOfUser;

    @Column(name = "previous_status")
    private Integer previousStatus;

    @DateTimeFormat(pattern = "MM/dd/yyyy HH:mm:ss.SSS")
	@Column(name = "approved_datetime")
	private LocalDateTime approvedDate;

	@OneToMany(mappedBy = "idsmedUser")
    private List<IdsmedUserRole> userRoles;

    @OneToMany(mappedBy = "idsmedUser")
    private List<IdsmedAccount> idsmedAccounts;

//	GenderEnum
    @Column(name = "gender")
	private Integer gender;

	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern = "MM/dd/yyyy")
    @Column(name = "birthday")
	private Date birthday;

	@Column(name = "mobile_phone_number")
	@Size(max = 50)
	private String mobilePhoneNumber;

	@Column(name = "id_card_number")
	private String idCardNumber;

	@Column(name = "id_card_front_pic")
	private String idCardFrontPic;

	@Column(name = "id_card_back_pic")
	private String idCardBackPic;

	@Column(name = "title")
	private String title;

	@Column(name = "inactive_reason")
	private String inactiveReason;

    @ManyToOne()
	@JoinColumn(name = "hospital_id", referencedColumnName = "id")
	private Hospital hospital;

	@ManyToOne()
	@JoinColumn(name = "corporate_id", referencedColumnName = "id")
	private Corporate corporate;

	@OneToMany(mappedBy = "idsmedUser")
	private List<UserAddress> userAddresses;

    public IdsmedUser(HospitalUserRegistrationRequest request) {
    	this.firstName = request.getFirstName();
    	this.lastName = request.getLastName();
		this.email = request.getEmail();
		this.phoneNumber = request.getPhoneNumber();
		this.gender = request.getGender();
		this.birthday = request.getBirthday();
		this.mobilePhoneNumber = request.getMobilePhoneNumber();
		this.idCardNumber = request.getIdCardNumber();
		this.title = request.getTitle();
		this.typeOfUser = UserTypeEnum.HOSPITAL_USER.getCode();
	}

	public IdsmedUser(CorporateUserRegistrationRequest request) {
		this.firstName = request.getFirstName();
		this.lastName = request.getLastName();
		this.email = request.getEmail();
		this.phoneNumber = request.getPhoneNumber();
		this.gender = request.getGender();
		this.birthday = request.getBirthday();
		this.mobilePhoneNumber = request.getMobilePhoneNumber();
		this.idCardNumber = request.getIdCardNumber();
		this.title = request.getTitle();
		this.typeOfUser = UserTypeEnum.CORPORATE_USER.getCode();
	}

	public IdsmedUser(IdsmedIndividualUserRequest request) {
		this.firstName = request.getFirstName();
		this.lastName = request.getLastName();
		this.email = request.getEmail();
		this.phoneNumber = request.getPhoneNumber();
		this.gender = request.getGender();
		this.birthday = request.getBirthday();
		this.mobilePhoneNumber = request.getMobilePhoneNumber();
		this.idCardNumber = request.getIdCardNumber();
		this.title = request.getTitle();
		this.typeOfUser = UserTypeEnum.INDIVIDUAL_USER.getCode();
	}

    public IdsmedUser() {
    	
    }

    public IdsmedUser updateRegistrationUser(UserRegistrationRequest request) {
    	if (request != null) {
			this.firstName = request.getFirstName();
			this.lastName = request.getLastName();
			this.phoneNumber = request.getPhoneNumber();
			this.phoneNumber = request.getPhoneNumber();
			this.gender = request.getGender();
			this.title = request.getTitle();
			this.birthday = request.getBirthday();
			this.mobilePhoneNumber = request.getMobilePhoneNumber();
			this.idCardNumber = request.getIdCardNumber();
			this.typeOfUser = request.getTypeOfUser();
		}
		return this;
	}


    
    public IdsmedUser(IdsmedUser user) {
    	BeanUtils.copyProperties(user, this);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }
    
    public String getRejectReason() {
		return rejectReason;
	}

	public void setRejectReason(String rejectReason) {
		this.rejectReason = rejectReason;
	}
	
	public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	public String getQq() {
		return qq;
	}

	public void setQq(String qq) {
		this.qq = qq;
	}

	public String getWechat() {
		return wechat;
	}

	public void setWechat(String wechat) {
		this.wechat = wechat;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
	public Long getApprovedBy() {
		return approvedBy;
	}

	public void setApprovedBy(Long approvedBy) {
		this.approvedBy = approvedBy;
	}

	public List<IdsmedUserRole> getUserRoles() {
        return userRoles;
    }

    public void setUserRoles(List<IdsmedUserRole> userRoles) {
        this.userRoles = userRoles;
    }

    public List<IdsmedAccount> getIdsmedAccounts() {
        return idsmedAccounts;
    }

    public void setIdsmedAccounts(List<IdsmedAccount> idsmedAccounts) {
        this.idsmedAccounts = idsmedAccounts;
    }

	public String getProfileImageUrl() {
		return profileImageUrl;
	}

	public void setProfileImageUrl(String profileImageUrl) {
		this.profileImageUrl = profileImageUrl;
	}

    public Integer getTypeOfUser() { return typeOfUser; }

    public void setTypeOfUser(Integer typeOfUser) { this.typeOfUser = typeOfUser; }

    public Integer getPreviousStatus() {
        return previousStatus;
    }

    public void setPreviousStatus(Integer previousStatus) {
        this.previousStatus = previousStatus;
    }

    public LocalDateTime getApprovedDate() {
        return approvedDate;
    }

    public void setApprovedDate(LocalDateTime approvedDate) {
        this.approvedDate = approvedDate;
    }

	public Hospital getHospital() {
		return hospital;
	}

	public void setHospital(Hospital hospital) {
		this.hospital = hospital;
	}

	public Integer getGender() {
		return gender;
	}

	public void setGender(Integer gender) {
		this.gender = gender;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public String getMobilePhoneNumber() {
		return mobilePhoneNumber;
	}

	public void setMobilePhoneNumber(String mobilePhoneNumber) {
		this.mobilePhoneNumber = mobilePhoneNumber;
	}

	public String getIdCardNumber() {
		return idCardNumber;
	}

	public void setIdCardNumber(String idCardNumber) {
		this.idCardNumber = idCardNumber;
	}

	public String getIdCardFrontPic() {
		return idCardFrontPic;
	}

	public void setIdCardFrontPic(String idCardFrontPic) {
		this.idCardFrontPic = idCardFrontPic;
	}

	public String getIdCardBackPic() {
		return idCardBackPic;
	}

	public void setIdCardBackPic(String idCardBackPic) {
		this.idCardBackPic = idCardBackPic;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public Corporate getCorporate() {
		return corporate;
	}

	public void setCorporate(Corporate corporate) {
		this.corporate = corporate;
	}

	public List<UserAddress> getUserAddresses() {
		return userAddresses;
	}

	public void setUserAddresses(List<UserAddress> userAddresses) {
		this.userAddresses = userAddresses;
	}

	public String getInactiveReason() {
		return inactiveReason;
	}

	public void setInactiveReason(String inactiveReason) {
		this.inactiveReason = inactiveReason;
	}
}

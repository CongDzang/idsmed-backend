package com.cmg.idsmed.model.entity.auth;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.cmg.idsmed.model.entity.AbstractEntity;

@Entity
@Table(name = "idsmed_user_role")
public class IdsmedUserRole extends AbstractEntity {
	private static final long serialVersionUID = -627825568289554289L;

	@Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "idsmed_user_role_id_seq")
    @SequenceGenerator(name = "idsmed_user_role_id_seq",
            sequenceName = "idsmed_user_role_id_seq",
            allocationSize = 1)
    @Column(updatable = false)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    private IdsmedUser idsmedUser;

    @ManyToOne
    @JoinColumn(name = "role_id", referencedColumnName = "id")
    private IdsmedRole idsmedRole;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public IdsmedUser getIdsmedUser() {
        return idsmedUser;
    }

    public void setIdsmedUser(IdsmedUser idsmedUser) {
        this.idsmedUser = idsmedUser;
    }

    public IdsmedRole getIdsmedRole() {
        return idsmedRole;
    }

    public void setIdsmedRole(IdsmedRole idsmedRole) {
        this.idsmedRole = idsmedRole;
    }
}

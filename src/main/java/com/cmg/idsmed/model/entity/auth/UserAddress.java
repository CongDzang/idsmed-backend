package com.cmg.idsmed.model.entity.auth;

import com.cmg.idsmed.common.enums.EntitySimpleStatusEnum;
import com.cmg.idsmed.model.entity.AbstractEntity;
import com.cmg.idsmed.model.entity.masterdata.Country;
import com.cmg.idsmed.model.entity.masterdata.Province;
import com.cmg.idsmed.model.entity.vendor.Vendor;
import org.springframework.util.StringUtils;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;


//This Entity uses EntitySimpleStatusEnum
@Entity
@Table(name = "user_address")
public class UserAddress extends AbstractEntity {

	private static final long serialVersionUID = -8369248221195641298L;

	// We prefer sequence generator than identity. If you use identity it may
	// disable JDBC batching
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_address_id_seq")
	@SequenceGenerator(name = "user_address_id_seq", sequenceName = "user_address_id_seq", allocationSize = 1)
	@Column(updatable = false)
	private Long id;

	@Column(name = "first_address")
	private String firstAddress;

	@Column(name = "second_address")
	private String secondAddress;

	@Column(name = "third_address")
	private String thirdAddress;

	@Column(name = "postcode")
	private String postcode;

	@Column(name = "from_date")
	private LocalDateTime fromDate;

	@Column(name = "to_date")
	private LocalDateTime toDate;

	@ManyToOne
	@JoinColumn(name = "idsmed_user_id")
	private IdsmedUser idsmedUser;

	@ManyToOne
	@JoinColumn(name = "country_id")
	private Country country;

	@ManyToOne
	@JoinColumn(name = "province_id")
	private Province province;

	public UserAddress() {

	}

	public UserAddress(String firstAddress, String secondAddress, IdsmedUser user, Country country, Province province, String postcode) {
		if (!StringUtils.isEmpty(postcode)) {
			this.postcode = postcode;
		}
		if (user != null) {
			this.idsmedUser = user;
		}
		if (!StringUtils.isEmpty(firstAddress)) {
			this.firstAddress = firstAddress;
		}
		this.firstAddress = firstAddress;
		if (!StringUtils.isEmpty(secondAddress)) {
			this.secondAddress = secondAddress;
		}
		if (country != null) {
			this.country = country;
		}
		if (province != null) {
			this.province = province;
		}
		this.setStatus(EntitySimpleStatusEnum.ACTIVE.getCode());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstAddress() {
		return firstAddress;
	}

	public void setFirstAddress(String firstAddress) {
		this.firstAddress = firstAddress;
	}

	public String getSecondAddress() {
		return secondAddress;
	}

	public void setSecondAddress(String secondAddress) {
		this.secondAddress = secondAddress;
	}

	public String getThirdAddress() {
		return thirdAddress;
	}

	public void setThirdAddress(String thirdAddress) {
		this.thirdAddress = thirdAddress;
	}

	public String getPostcode() {
		return postcode;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	public LocalDateTime getFromDate() {
		return fromDate;
	}

	public void setFromDate(LocalDateTime fromDate) {
		this.fromDate = fromDate;
	}

	public LocalDateTime getToDate() {
		return toDate;
	}

	public void setToDate(LocalDateTime toDate) {
		this.toDate = toDate;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	public Province getProvince() {
		return province;
	}

	public void setProvince(Province province) {
		this.province = province;
	}

	public IdsmedUser getIdsmedUser() {
		return idsmedUser;
	}

	public void setIdsmedUser(IdsmedUser idsmedUser) {
		this.idsmedUser = idsmedUser;
	}


}

package com.cmg.idsmed.model.entity.common;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import com.cmg.idsmed.model.entity.AbstractEntity;

@Entity
@Table(name = "mail_sending_log")
public class MailSendingLog extends AbstractEntity {
	private static final long serialVersionUID = -226952265066780807L;

	//We prefer sequence generator than identity. If you use identity it may disable JDBC batching
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "mail_sending_log_id_seq")
    @SequenceGenerator(name = "mail_sending_log_id_seq",
            sequenceName = "mail_sending_log_id_seq",
            allocationSize = 1)
    @Column(updatable = false)
    private Long id;
    
    @Column(name = "mail_purpose")
    @Size(max = 100)
    private String mailPurpose;

	@Column(name = "sender")
    @Size(max = 100)
    private String sender;
    
    @Column(name = "recipients")
    @Size(max = 500)
    private String recipients;
    
    @Column(name = "result")
    @Size(max = 10)
    private String result;
    
    @Column(name = "error_code")
    private Integer errorCode;
    
    @Column(name = "error_description")
    @Size(max = 500)
    private String errorDescription;
    
    @Column(name = "followup_action")
    @Size(max = 500)
    private String followUpAction;

    @Column(name = "subject")
	private String subject;

    @Column(name = "content")
	private String content;
    
    public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMailPurpose() {
		return mailPurpose;
	}

	public void setMailPurpose(String mailPurpose) {
		this.mailPurpose = mailPurpose;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public String getRecipients() {
		return recipients;
	}

	public void setRecipients(String recipients) {
		this.recipients = recipients;
	}


	public Integer getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(Integer errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorDescription() {
		return errorDescription;
	}

	public void setErrorDescription(String errorDescription) {
		this.errorDescription = errorDescription;
	}

	public String getFollowUpAction() {
		return followUpAction;
	}

	public void setFollowUpAction(String followUpAction) {
		this.followUpAction = followUpAction;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
}


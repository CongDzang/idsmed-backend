package com.cmg.idsmed.model.entity.corporate;

import com.cmg.idsmed.dto.corporate.CorporateDepartmentRequest;
import com.cmg.idsmed.model.entity.AbstractEntity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "corporate_department")
public class CorporateDepartment extends AbstractEntity {

	private static final long serialVersionUID = -8369248221195641298L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "corporate_department_id_seq")
	@SequenceGenerator(name = "corporate_department_id_seq", sequenceName = "corporate_department_id_seq", allocationSize = 1)
	@Column(updatable = false)
	private Long id;

	@Column(name = "name_primary")
	private String namePrimary;

	@Column(name = "name_secondary")
	private String nameSecondary;

	@ManyToOne()
	@JoinColumn(name = "corporate_id", referencedColumnName = "id")
	private Corporate corporate;

	@Column(name = "code")
	@Size(max = 250)
	@NotNull
	private String code;

	public CorporateDepartment() {

	}

	public CorporateDepartment(CorporateDepartmentRequest request) {
		if (request != null) {
			this.namePrimary = request.getNamePrimary();
			this.nameSecondary = request.getNameSecondary();
		}
	}

	public CorporateDepartment CorporateDepartment(CorporateDepartmentRequest request) {
		if (request != null) {
			this.namePrimary = request.getNamePrimary();
			this.nameSecondary = request.getNameSecondary();
		}
		return this;
	}

	public CorporateDepartment update(CorporateDepartmentRequest request) {
		if (request != null) {
			this.namePrimary = request.getNamePrimary();
			this.nameSecondary = request.getNameSecondary();
		}
		return this;
	}


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNamePrimary() {
		return namePrimary;
	}

	public void setNamePrimary(String namePrimary) {
		this.namePrimary = namePrimary;
	}

	public String getNameSecondary() {
		return nameSecondary;
	}

	public void setNameSecondary(String nameSecondary) {
		this.nameSecondary = nameSecondary;
	}

	public Corporate getCorporate() {
		return corporate;
	}

	public void setCorporate(Corporate corporate) {
		this.corporate = corporate;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

}

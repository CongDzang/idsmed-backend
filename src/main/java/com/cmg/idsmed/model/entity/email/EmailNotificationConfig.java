package com.cmg.idsmed.model.entity.email;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.cmg.idsmed.common.enums.EmailTypeEnum;
import com.cmg.idsmed.model.entity.AbstractEntity;

@Entity
@Table(name = "email_notification_config")
public class EmailNotificationConfig extends AbstractEntity {
	private static final long serialVersionUID = 2118160727698279536L;

	public enum NotificationConfigTypeEnum {
		USER(1, "User"),
		VENDOR(2, "Vendor"),
		PRODUCT(3, "Product");

		private Integer code;
		private String name;

		NotificationConfigTypeEnum(Integer code, String name) {
			this.code = code;
			this.name = name;
		}

		public Integer getCode() {
			return code;
		}

		public void setCode(Integer code) {
			this.code = code;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

	}

	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "email_notification_config_id_seq")
    @SequenceGenerator(name = "email_notification_config_id_seq",
            sequenceName = "email_notification_config_id_seq",
            allocationSize = 1)
    @Column(updatable = false)
    private Long id;

	@Column(name = "notification_name")
	@NotNull
	private String notificationName;

	@Column(name = "notification_config_type")
	private Integer notificationConfigType;
    
	@Column(name = "email_type")
    private EmailTypeEnum emailType;
	
    @Column(name = "recipient_list")
    @Size(max = 500)
    private String recipientList;
    
    @Column(name = "enabled")
    private Boolean enabled;
    
    @ManyToOne
    @JoinColumn(name = "template_id")
    private EmailTemplate emailTemplate;

    @Column(name = "country_code")
	@Size(max = 10)
	private String countryCode;

	public EmailNotificationConfig() {
	}

	public EmailNotificationConfig(String notificationName, Integer notificationConfigType,
								   EmailTypeEnum emailType, String recipientList, Boolean enabled,
								   EmailTemplate emailTemplate, String countryCode) {
		this.notificationName = notificationName;
		this.notificationConfigType = notificationConfigType;
		this.emailType = emailType;
		this.recipientList = recipientList;
		this.enabled = enabled;
		this.emailTemplate = emailTemplate;
		this.countryCode = countryCode;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNotificationName() {
		return notificationName;
	}

	public void setNotificationName(String notificationName) {
		this.notificationName = notificationName;
	}

	public Integer getNotificationConfigType() {
		return notificationConfigType;
	}

	public void setNotificationConfigType(Integer notificationConfigType) {
		this.notificationConfigType = notificationConfigType;
	}

	public EmailTypeEnum getEmailType() {
		return emailType;
	}

	public void setEmailType(EmailTypeEnum emailType) {
		this.emailType = emailType;
	}

	public String getRecipientList() {
		return recipientList;
	}

	public void setRecipientList(String recipientList) {
		this.recipientList = recipientList;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public EmailTemplate getEmailTemplate() {
		return emailTemplate;
	}

	public void setEmailTemplate(EmailTemplate emailTemplate) {
		this.emailTemplate = emailTemplate;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

}

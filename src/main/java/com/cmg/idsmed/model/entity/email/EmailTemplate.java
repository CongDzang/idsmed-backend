package com.cmg.idsmed.model.entity.email;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.cmg.idsmed.model.entity.AbstractEntity;
import com.cmg.idsmed.model.entity.product.Product;

@Entity
@Table(name = "email_template")
public class EmailTemplate extends AbstractEntity {

    //    Enum for EmailTemplate status
    public enum EmailTemplateStatusEnum {
        ACTIVE(1, "active"),
        INACTIVE(10, "inactive");
        private Integer code;
        private String name;

        EmailTemplateStatusEnum(Integer code, String name) {
            this.code = code;
            this.name = name;
        }

        public Integer getCode() {
            return code;
        }

        public void setCode(Integer code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "email_template_id_seq")
    @SequenceGenerator(name = "email_template_id_seq",
            sequenceName = "email_template_id_seq",
            allocationSize = 1)
    @Column(updatable = false)
    private Long id;

    @Column(name = "template_name")
    @Size(max = 100)
    @NotNull
    private String templateName;

    @Column(name = "subject")
    @Size(max = 100)
    @NotNull
    private String subject;

    @Column(name = "content")
    private String content;

    @OneToMany(mappedBy = "emailTemplate")
    private List<EmailNotificationConfig> emailNotificationConfigs;

    public EmailTemplate() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}

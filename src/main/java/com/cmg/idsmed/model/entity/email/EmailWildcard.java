package com.cmg.idsmed.model.entity.email;

import com.cmg.idsmed.model.entity.AbstractEntity;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Entity
@Table(name = "notification_wildcard")
public class EmailWildcard extends AbstractEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "notification_wildcard_id_seq")
    @SequenceGenerator(name = "notification_wildcard_id_seq",
            sequenceName = "notification_wildcard_id_seq",
            allocationSize = 1)
    @Column(updatable = false)
    private Long id;

    @Column(name = "wildcard_name")
    @Size(max = 100)
    private String wildcardName;

    @Column(name = "wildcard_value")
    @Size(max = 100)
    private String wildcardValue;

    public EmailWildcard() {
    }

    public EmailWildcard(Long id, @Size(max = 100) String wildcardName, @Size(max = 100) String wildcardValue) {
        this.id = id;
        this.wildcardName = wildcardName;
        this.wildcardValue = wildcardValue;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getWildcardName() {
        return wildcardName;
    }

    public void setWildcardName(String wildcardName) {
        this.wildcardName = wildcardName;
    }

    public String getWildcardValue() {
        return wildcardValue;
    }

    public void setWildcardValue(String wildcardValue) {
        this.wildcardValue = wildcardValue;
    }
    
}

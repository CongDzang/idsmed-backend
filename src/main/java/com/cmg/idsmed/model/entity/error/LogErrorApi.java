package com.cmg.idsmed.model.entity.error;

import com.cmg.idsmed.model.entity.AbstractEntity;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Entity
@Table(name = "log_error_api")
public class LogErrorApi extends AbstractEntity
{
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "log_error_api_id_seq")
    @SequenceGenerator(name = "log_error_api_id_seq", sequenceName = "log_error_api_id_seq", allocationSize = 1)
    @Column(updatable = false)
    private Long id;

    @Column(name = "login_id")
    @Size(max = 100)
    private String loginId;

    @Column(name = "error_code")
    @Size(max = 10)
    private String errorCode;

    @Column(name = "url")
    @Size(max = 500)
    private String url;

    @Column(name = "description")
    @Size(max = 1000)
    private String description;

    @Column(name = "ip")
    @Size(max = 20)
    private String ip;

    @Column(name = "browser")
    @Size(max = 100)
    private String browser;

    @Column(name = "os")
    @Size(max = 100)
    private String os;

    public Long getId() {return id;}
    public void setId(Long id) {this.id = id;}

    public String getLoginId() {return loginId;}
    public void setLoginId(String loginId) {this.loginId = loginId;}

    public String getErrorCode() {return errorCode;}
    public void setErrorCode(String errorCode) {this.errorCode = errorCode;}

    public String getUrl() {return url;}
    public void setUrl(String url) {this.url = url;}

    public String getDescription() {return description;}
    public void setDescription(String description) {this.description = description;}

    public String getIp() {return ip;}
    public void setIp(String ip) {this.ip = ip;}

    public String getBrowser() {return browser;}
    public void setBrowser(String browser) {this.browser = browser;}

    public String getOs() {return os;}
    public void setOs(String os) {this.os = os;}
}
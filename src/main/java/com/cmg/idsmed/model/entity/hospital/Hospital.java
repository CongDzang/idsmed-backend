package com.cmg.idsmed.model.entity.hospital;

import com.cmg.idsmed.dto.hospital.HospitalRegistrationRequest;
import com.cmg.idsmed.dto.hospital.HospitalRequest;
import com.cmg.idsmed.model.entity.AbstractEntity;
import com.cmg.idsmed.model.entity.auth.IdsmedUser;
import com.cmg.idsmed.model.entity.masterdata.Country;
import com.cmg.idsmed.model.entity.masterdata.Province;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

//This entity using StatusEnum
@Entity
@Table(name="hospital")
public class Hospital extends AbstractEntity {
	private static final long serialVersionUID = -8369248221195641298L;
	// We prefer sequence generator than identity. If you use identity it may
	// disable JDBC batching
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "hospital_id_seq")
	@SequenceGenerator(name = "hospital_id_seq", sequenceName = "hospital_id_seq", allocationSize = 1)
	@Column(updatable = false)
	private Long id;

	@Column(name = "name_primary")
	private String namePrimary;

	@Column(name = "name_secondary")
	private String nameSecondary;

	@ManyToOne()
	@JoinColumn(name = "country_id", referencedColumnName = "id")
	private Country country;

	@ManyToOne()
	@JoinColumn(name = "province_id", referencedColumnName = "id")
	private Province province;

	@Column(name = "code")
	@NotNull
	@Size(max = 250)
	private String code;

	@Column(name = "postcode")
	@Size(max = 200)
	private String postcode;

	@Column(name = "first_address")
	private String firstAddress;

	@Column(name = "second_address")
	private String secondAddress;

	@OneToMany(mappedBy = "hospital")
	private List<HospitalDepartment> hospitalDepartments;

	@OneToMany(mappedBy = "hospital")
	private List<IdsmedUser> idsmedUsers;

	public Hospital() {
	}

	public Hospital(HospitalRequest request) {
		this.namePrimary = request.getNamePrimary();
		this.nameSecondary = request.getNameSecondary();
		this.postcode = request.getPostcode();
		this.firstAddress = request.getFirstAddress();
		this.secondAddress = request.getSecondAddress();
	}

	public Hospital update(HospitalRequest request, Country country, Province province) {
		if (request != null) {
			this.namePrimary = request.getNamePrimary();
			this.nameSecondary = request.getNameSecondary();
			this.postcode = request.getPostcode();
			this.firstAddress = request.getFirstAddress();
			this.secondAddress = request.getSecondAddress();
			this.setCountry(country);
			this.setProvince(province);
		}
		return this;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNamePrimary() {
		return namePrimary;
	}

	public void setNamePrimary(String namePrimary) {
		this.namePrimary = namePrimary;
	}

	public String getNameSecondary() {
		return nameSecondary;
	}

	public void setNameSecondary(String nameSecondary) {
		this.nameSecondary = nameSecondary;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	public Province getProvince() {
		return province;
	}

	public void setProvince(Province province) {
		this.province = province;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getPostcode() {
		return postcode;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	public String getFirstAddress() {
		return firstAddress;
	}

	public void setFirstAddress(String firstAddress) {
		this.firstAddress = firstAddress;
	}

	public String getSecondAddress() {
		return secondAddress;
	}

	public void setSecondAddress(String secondAddress) {
		this.secondAddress = secondAddress;
	}

	public List<IdsmedUser> getIdsmedUsers() {
		return idsmedUsers;
	}

	public void setIdsmedUsers(List<IdsmedUser> idsmedUsers) {
		this.idsmedUsers = idsmedUsers;
	}

	public List<HospitalDepartment> getHospitalDepartments() {
		return hospitalDepartments;
	}

	public void setHospitalDepartments(List<HospitalDepartment> hospitalDepartments) {
		this.hospitalDepartments = hospitalDepartments;
	}
}

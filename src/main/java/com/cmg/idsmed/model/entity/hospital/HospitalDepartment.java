package com.cmg.idsmed.model.entity.hospital;

import com.cmg.idsmed.dto.hospital.HospitalDepartmentRequest;
import com.cmg.idsmed.model.entity.AbstractEntity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


//This Entity uses StatusEnum
@Entity
@Table(name = "hospital_department")
public class HospitalDepartment extends AbstractEntity {

	private static final long serialVersionUID = -8369248221195641298L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "hospital_department_id_seq")
	@SequenceGenerator(name = "hospital_department_id_seq", sequenceName = "hospital_department_id_seq", allocationSize = 1)
	@Column(updatable = false)
	private Long id;

	@Column(name = "name_primary")
	private String namePrimary;

	@Column(name = "name_secondary")
	private String nameSecondary;

	@ManyToOne()
	@JoinColumn(name = "hospital_id", referencedColumnName = "id")
	private Hospital hospital;

	@Column(name = "code")
	@Size(max = 250)
	@NotNull
	private String code;

	public HospitalDepartment() {
	}

	public HospitalDepartment(HospitalDepartmentRequest request) {
		if (request != null) {
			this.namePrimary = request.getNamePrimary();
			this.nameSecondary = request.getNameSecondary();
		}
	}

	public HospitalDepartment update(HospitalDepartmentRequest request) {
		if (request != null) {
			this.namePrimary = request.getNamePrimary();
			this.nameSecondary = request.getNameSecondary();
		}
		return this;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNamePrimary() {
		return namePrimary;
	}

	public void setNamePrimary(String namePrimary) {
		this.namePrimary = namePrimary;
	}

	public String getNameSecondary() {
		return nameSecondary;
	}

	public void setNameSecondary(String nameSecondary) {
		this.nameSecondary = nameSecondary;
	}

	public Hospital getHospital() {
		return hospital;
	}

	public void setHospital(Hospital hospital) {
		this.hospital = hospital;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
}

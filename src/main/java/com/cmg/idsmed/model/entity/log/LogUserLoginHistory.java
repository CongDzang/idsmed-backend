package com.cmg.idsmed.model.entity.log;

import com.cmg.idsmed.common.utils.DateTimeHelper;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

@Entity
@Table(name = "log_user_login_history")
public class LogUserLoginHistory {
	public enum LoginStatusEnum {
		FAIL(0, "Fail"),
		SUCCESS(1, "Success");
		private Integer code;
		private String name;
		LoginStatusEnum(Integer code, String name) {
			this.code = code;
			this.name = name;
		}

		public Integer getCode() {
			return code;
		}

		public String getName() {
			return name;
		}
	}
	private static final long serialVersionUID = -8369248221195641298L;
	// We prefer sequence generator than identity. If you use identity it may
	// disable JDBC batching
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "log_user_login_history_id_seq")
	@SequenceGenerator(name = "log_user_login_history_id_seq", sequenceName = "log_user_login_history_id_seq", allocationSize = 1)
	@Column(updatable = false)
	private Long id;

	@Column(name = "ip")
	private String ip;

	@Column(name = "login_id")
	@Size(max = 255)
	private String loginId;

	@Column(name = "country_code")
	private String countryCode;

	@Column(name = "region")
	private String region;

	@Column(name = "time_zone")
	private String timezone;

	@Column(name = "browser")
	private String browser;

	@Column(name = "os")
	private String os;

	@DateTimeFormat(pattern = "MM/dd/yyyy HH:mm:ss.SSS")
	@Column(name="created_datetime")
	private LocalDateTime createdDate;

	//This Entity uses LoginStatusEnum
	@Column(name = "login_status")
	private Integer loginStatus;

	@PrePersist
	protected void onCreate() {
		this.createdDate = DateTimeHelper.getCurrentTimeUTC();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getTimezone() {
		return timezone;
	}

	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}

	public String getBrowser() {
		return browser;
	}

	public void setBrowser(String browser) {
		this.browser = browser;
	}

	public String getOs() {
		return os;
	}

	public void setOs(String os) {
		this.os = os;
	}

	public LocalDateTime getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(LocalDateTime createdDate) {
		this.createdDate = createdDate;
	}

	public Integer getLoginStatus() {
		return loginStatus;
	}

	public void setLoginStatus(Integer loginStatus) {
		this.loginStatus = loginStatus;
	}
}

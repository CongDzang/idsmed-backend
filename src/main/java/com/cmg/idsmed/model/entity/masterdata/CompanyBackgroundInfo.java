package com.cmg.idsmed.model.entity.masterdata;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import com.cmg.idsmed.model.entity.AbstractEntity;

@Entity
@Table(name = "company_background_info")
public class CompanyBackgroundInfo extends AbstractEntity {
	private static final long serialVersionUID = -226952265066780807L;

	//We prefer sequence generator than identity. If you use identity it may disable JDBC batching
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "company_background_info_id_seq")
    @SequenceGenerator(name = "company_background_info_id_seq",
            sequenceName = "company_background_info_id_seq",
            allocationSize = 1)
    @Column(updatable = false)
    private Long id;

    @Column(name = "company_code")
    @Size(max = 10)
    private String companyCode;
    
    @Column(name = "annual_sales", precision = 19, scale = 2)
    private BigDecimal annualSales;
    
    @Column(name = "no_of_employee")
    private Long noOfEmployee;
    
    @Column(name = "primary_line")
    @Size(max = 500)
    private String primaryLine;
    
    @Column(name = "mgmt_director_1")
    @Size(max = 500)
    private String mgmtDirector1;
    
    @Column(name = "mgmt_director_2")
    @Size(max = 500)
    private String mgmtDirector2;
    
    @Column(name = "mgmt_director_3")
    @Size(max = 500)
    private String mgmtDirector3;
    
    @Column(name = "mgmt_director_4")
    @Size(max = 500)
    private String mgmtDirector4;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	public BigDecimal getAnnualSales() {
		return annualSales;
	}

	public void setAnnualSales(BigDecimal annualSales) {
		this.annualSales = annualSales;
	}

	public Long getNoOfEmployee() {
		return noOfEmployee;
	}

	public void setNoOfEmployee(Long noOfEmployee) {
		this.noOfEmployee = noOfEmployee;
	}

	public String getPrimaryLine() {
		return primaryLine;
	}

	public void setPrimaryLine(String primaryLine) {
		this.primaryLine = primaryLine;
	}

	public String getMgmtDirector1() {
		return mgmtDirector1;
	}

	public void setMgmtDirector1(String mgmtDirector1) {
		this.mgmtDirector1 = mgmtDirector1;
	}

	public String getMgmtDirector2() {
		return mgmtDirector2;
	}

	public void setMgmtDirector2(String mgmtDirector2) {
		this.mgmtDirector2 = mgmtDirector2;
	}

	public String getMgmtDirector3() {
		return mgmtDirector3;
	}

	public void setMgmtDirector3(String mgmtDirector3) {
		this.mgmtDirector3 = mgmtDirector3;
	}

	public String getMgmtDirector4() {
		return mgmtDirector4;
	}

	public void setMgmtDirector4(String mgmtDirector4) {
		this.mgmtDirector4 = mgmtDirector4;
	}
}


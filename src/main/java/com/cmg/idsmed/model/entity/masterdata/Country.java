package com.cmg.idsmed.model.entity.masterdata;

import com.cmg.idsmed.model.entity.AbstractEntity;
import com.cmg.idsmed.model.entity.auth.IdsmedUser;
import com.cmg.idsmed.model.entity.auth.UserAddress;
import com.cmg.idsmed.model.entity.corporate.Corporate;
import com.cmg.idsmed.model.entity.hospital.Hospital;
import com.cmg.idsmed.model.entity.vendor.VendorAddress;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * Status of this entity uses Simple
 */

@Entity
@Table(name = "country")
public class Country extends AbstractEntity {

	private static final long serialVersionUID = 5771796917019428678L;

	//We prefer sequence generator than identity. If you use identity it may disable JDBC batching
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "country_id_seq")
	@SequenceGenerator(name = "country_id_seq",
			sequenceName = "country_id_seq",
			allocationSize = 1)
	@Column(updatable = false)
	private Long id;

	@Column(name = "english_name")
	@Size(max = 1000)
	private String englishName;

	@Column(name = "chinese_name")
	@Size(max = 1000)
	private String chineseName;

	@Column(name = "english_brief_name")
	@Size(max = 500)
	private String englishBriefName;

	@Column(name = "chinese_brief_name")
	@Size(max = 500)
	private String chineseBriefName;

	@Column(name = "country_code")
	@Size(max = 50)
	private String countryCode;

	@OneToMany(mappedBy = "country")
	private List<Province> provinces;

	@OneToMany(mappedBy = "country")
	private List<VendorAddress> vendorAddresses;

	@OneToMany(mappedBy = "country")
	private List<Hospital> hospitals;

	@OneToMany(mappedBy = "country")
	private List<Corporate> corporates;

	@OneToMany(mappedBy = "country")
	private List<UserAddress> userAddresses;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEnglishName() {
		return englishName;
	}

	public void setEnglishName(String englishName) {
		this.englishName = englishName;
	}

	public String getChineseName() {
		return chineseName;
	}

	public void setChineseName(String chineseName) {
		this.chineseName = chineseName;
	}

	public String getEnglishBriefName() {
		return englishBriefName;
	}

	public void setEnglishBriefName(String englishBriefName) {
		this.englishBriefName = englishBriefName;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public List<Province> getProvinces() {
		return provinces;
	}

	public void setProvinces(List<Province> provinces) {
		this.provinces = provinces;
	}

	public String getChineseBriefName() {
		return chineseBriefName;
	}

	public void setChineseBriefName(String chineseBriefName) {
		this.chineseBriefName = chineseBriefName;
	}

	public List<VendorAddress> getVendorAddresses() {
		return vendorAddresses;
	}

	public void setVendorAddresses(List<VendorAddress> vendorAddresses) {
		this.vendorAddresses = vendorAddresses;
	}

	public List<Hospital> getHospitals() {
		return hospitals;
	}

	public void setHospitals(List<Hospital> hospitals) {
		this.hospitals = hospitals;
	}

	public List<Corporate> getCorporates() {
		return corporates;
	}

	public void setCorporates(List<Corporate> corporates) {
		this.corporates = corporates;
	}

	public List<UserAddress> getUserAddresses() {
		return userAddresses;
	}

	public void setUserAddresses(List<UserAddress> userAddresses) {
		this.userAddresses = userAddresses;
	}


}

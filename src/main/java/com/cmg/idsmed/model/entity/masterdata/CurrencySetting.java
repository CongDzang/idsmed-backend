package com.cmg.idsmed.model.entity.masterdata;

import com.cmg.idsmed.model.entity.AbstractEntity;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Entity
@Table(name = "currency_setting")
public class CurrencySetting extends AbstractEntity {

	private static final long serialVersionUID = 5771796917019428678L;

	//We prefer sequence generator than identity. If you use identity it may disable JDBC batching
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "currency_setting_id_seq")
	@SequenceGenerator(name = "currency_setting_id_seq",
			sequenceName = "currency_setting_id_seq",
			allocationSize = 1)
	@Column(updatable = false)
	private Long id;

	@Column(name = "country_code")
	@Size(max = 50)
	private String countryCode;

	@Column(name = "primary_currency_code")
	@Size(max = 50)
	private String primaryCurrencyCode;

	@Column(name = "primary_currency_name")
	private String primaryCurrencyName;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getPrimaryCurrencyCode() {
		return primaryCurrencyCode;
	}

	public void setPrimaryCurrencyCode(String primaryCurrencyCode) {
		this.primaryCurrencyCode = primaryCurrencyCode;
	}

	public String getPrimaryCurrencyName() {
		return primaryCurrencyName;
	}

	public void setPrimaryCurrencyName(String primaryCurrencyName) {
		this.primaryCurrencyName = primaryCurrencyName;
	}
}

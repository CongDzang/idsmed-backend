package com.cmg.idsmed.model.entity.masterdata;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import com.cmg.idsmed.model.entity.AbstractEntity;

@Entity
@Table(name = "favourite_product_brand")
public class FavouriteProductBrand extends AbstractEntity {

	/**
	 *
	 */
	private static final long serialVersionUID = -226952265066780807L;


	//We prefer sequence generator than identity. If you use identity it may disable JDBC batching
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "favourite_product_brand_id_seq")
    @SequenceGenerator(name = "favourite_product_brand_id_seq",
            sequenceName = "favourite_product_brand_id_seq",
            allocationSize = 1)
    @Column(updatable = false)
    private Long id;

    @Column(name = "user_id")
    @Size(max = 50)
    private Long userId;
    
    @Column(name = "brand_id")
    @Size(max = 50)
    private Long brandId;

    @Column(name = "wedoctor_customer_id")
	private Long wedoctorCustomerId;

    @Column(name = "user_third_party")
    @Size(max = 100)
    private String userThirdParty;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getBrandId() {
		return brandId;
	}

	public void setBrandId(Long brandId) {
		this.brandId = brandId;
	}

	public String getUserPhirdParty() {
		return userThirdParty;
	}

	public void setUserThirdParty(String userThirdParty) {
		this.userThirdParty = userThirdParty;
	}

	public Long getWedoctorCustomerId() {
		return wedoctorCustomerId;
	}

	public void setWedoctorCustomerId(Long wedoctorCustomerId) {
		this.wedoctorCustomerId = wedoctorCustomerId;
	}

	public String getUserThirdParty() {
		return userThirdParty;
	}
}


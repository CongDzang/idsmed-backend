package com.cmg.idsmed.model.entity.masterdata;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import com.cmg.idsmed.model.entity.AbstractEntity;

@Entity
@Table(name = "favourite_product_care_area")
public class FavouriteProductCareArea extends AbstractEntity {

	private static final long serialVersionUID = -226952265066780807L;


	//We prefer sequence generator than identity. If you use identity it may disable JDBC batching
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "favourite_product_care_area_id_seq")
    @SequenceGenerator(name = "favourite_product_care_area_id_seq",
            sequenceName = "favourite_product_care_area_id_seq",
            allocationSize = 1)
    @Column(updatable = false)
    private Long id;

    @Column(name = "user_id")
    @Size(max = 50)
    private Long userId;
    
    @Column(name = "care_area_id")
    @Size(max = 50)
    private Long careAreaId;

	@Column(name = "wedoctor_customer_id")
	private Long wedoctorCustomerId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getCareAreaId() {
		return careAreaId;
	}

	public void setCareAreaId(Long careAreaId) {
		this.careAreaId = careAreaId;
	}

	public Long getWedoctorCustomerId() {
		return wedoctorCustomerId;
	}

	public void setWedoctorCustomerId(Long wedoctorCustomerId) {
		this.wedoctorCustomerId = wedoctorCustomerId;
	}
}


package com.cmg.idsmed.model.entity.masterdata;

import com.cmg.idsmed.model.entity.AbstractEntity;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

@Entity
@Table(name = "notification_wildcard")
public class NotificationWildcard extends AbstractEntity {
	private static final long serialVersionUID = -226952265066780807L;

	//We prefer sequence generator than identity. If you use identity it may disable JDBC batching
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "notification_wildcard_id_seq")
    @SequenceGenerator(name = "notification_wildcard_id_seq",
            sequenceName = "notification_wildcard_id_seq",
            allocationSize = 1)
    @Column(updatable = false)
    private Long id;

    @Column(name = "wildcard_name")
    @Size(max = 100)
    private String wildCardName;
    
    @Column(name = "wildcard_value")
	@Size(max = 100)
    private String wildCardValue;

	public Long getId() { return id; }

	public void setId(Long id) { this.id = id; }


	public String getWildCardName() { return wildCardName; }

	public void setWildCardName(String wildCardName) { this.wildCardName = wildCardName; }

	public String getWildCardValue() { return wildCardValue; }

	public void setWildCardValue(String wildCardValue) { this.wildCardValue = wildCardValue; }
}


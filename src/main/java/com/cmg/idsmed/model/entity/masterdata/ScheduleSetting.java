package com.cmg.idsmed.model.entity.masterdata;

import com.cmg.idsmed.model.entity.AbstractEntity;

import javax.persistence.*;

@Entity
@Table(name = "schedule_setting")
public class ScheduleSetting extends AbstractEntity {

	//We prefer sequence generator than identity. If you use identity it may disable JDBC batching
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "schedule_setting_id_seq")
	@SequenceGenerator(name = "schedule_setting_id_seq",
			sequenceName = "schedule_setting_id_seq",
			allocationSize = 1)
	@Column(updatable = false)
	private Long id;

	@Column(name = "schedule_task_code")
	private String scheduleTaskCode;

	@Column(name = "cron_expression")
	private String cronExpression;

	@Column(name = "fixed_rate")
	private Integer fixedRate;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getScheduleTaskCode() {
		return scheduleTaskCode;
	}

	public void setScheduleTaskCode(String scheduleTaskCode) {
		this.scheduleTaskCode = scheduleTaskCode;
	}

	public String getCronExpression() {
		return cronExpression;
	}

	public void setCronExpression(String cronExpression) {
		this.cronExpression = cronExpression;
	}

	public Integer getFixedRate() {
		return fixedRate;
	}

	public void setFixedRate(Integer fixedRate) {
		this.fixedRate = fixedRate;
	}
}

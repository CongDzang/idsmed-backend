package com.cmg.idsmed.model.entity.masterdata;

import com.cmg.idsmed.model.entity.AbstractEntity;

import javax.persistence.*;
import javax.validation.constraints.Size;

//This Entity using EntitySimpleStatusEnum.
@Entity
@Table(name = "verifying_work_flow_setting")
public class VerifyingWorkFlowSetting extends AbstractEntity {
	private static final long serialVersionUID = 5771796917019428678L;

	//We prefer sequence generator than identity. If you use identity it may disable JDBC batching
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "currency_setting_id_seq")
	@SequenceGenerator(name = "currency_setting_id_seq",
			sequenceName = "currency_setting_id_seq",
			allocationSize = 1)
	@Column(updatable = false)
	private Long id;

	@Column(name = "country_code")
	@Size(max = 50)
	private String countryCode;

	//This save code in VerifyingWorkFlowEnum
	@Column(name = "work_flow_code")
	@Size(max = 50)
	private String workFlowCode;

	@Column(name = "process_nodes")
	private String processNodes;

	@Column(name = "description")
	private String description;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getWorkFlowCode() {
		return workFlowCode;
	}

	public void setWorkFlowCode(String workFlowCode) {
		this.workFlowCode = workFlowCode;
	}

	public String getProcessNodes() {
		return processNodes;
	}

	public void setProcessNodes(String processNodes) {
		this.processNodes = processNodes;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}

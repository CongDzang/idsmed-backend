package com.cmg.idsmed.model.entity.notification;

import com.cmg.idsmed.model.entity.AbstractEntity;
import com.cmg.idsmed.model.entity.auth.IdsmedAccount;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "account_notification_setting")
public class AccountNotificationSetting extends AbstractEntity {

	private static final long serialVersionUID = 5771796917019428678L;

	//We prefer sequence generator than identity. If you use identity it may disable JDBC batching
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "account_notification_setting_id_seq")
	@SequenceGenerator(name = "account_notification_setting_id_seq",
			sequenceName = "account_notification_setting_id_seq",
			allocationSize = 1)
	@Column(updatable = false)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "idsmed_account_id")
	private IdsmedAccount idsmedAccount;

	@Column(name = "notification_type")
	private Integer notificationType;

	public AccountNotificationSetting() { }

	public AccountNotificationSetting(IdsmedAccount idsmedAccount, Integer notificationType){
		this.idsmedAccount = idsmedAccount;
		this.notificationType = notificationType;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public IdsmedAccount getIdsmedAccount() { return idsmedAccount; }

	public void setIdsmedAccount(IdsmedAccount idsmedAccount) { this.idsmedAccount = idsmedAccount; }

	public Integer getNotificationType() { return notificationType; }

	public void setNotificationType(Integer notificationType) { this.notificationType = notificationType; }
}

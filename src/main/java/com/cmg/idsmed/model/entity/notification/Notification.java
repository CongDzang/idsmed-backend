package com.cmg.idsmed.model.entity.notification;

import com.cmg.idsmed.model.entity.AbstractEntity;
import com.cmg.idsmed.model.entity.auth.IdsmedAccount;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "notification")
public class Notification extends AbstractEntity {

	private static final long serialVersionUID = 5771796917019428678L;

	public enum NotificationStatus {
		CREATE(0, "created"),
		SENT(1, "sent"),
		ERROR(2, "error"),
		DELIVERED(3, "delivered");
		private Integer code;
		private String name;
		NotificationStatus(Integer code, String name) {
			this.code = code;
			this.name = name;
		}

		public Integer getCode() {
			return code;
		}

		public void setCode(Integer code) {
			this.code = code;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}
	}

	//We prefer sequence generator than identity. If you use identity it may disable JDBC batching
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "notification_id_seq")
	@SequenceGenerator(name = "notification_id_seq",
			sequenceName = "notification_id_seq",
			allocationSize = 1)
	@Column(updatable = false)
	private Long id;

	/**
	 * This column save WebNotificationTypeEnum code.
	 */
	@Column(name = "type")
	private Integer type;

	@Column(name = "subject")
	private String subject;

	@Column(name = "content")
	private String content;

	@ManyToOne
	@JoinColumn(name = "idsmed_account_id", nullable = false)
	private IdsmedAccount sender;

	@OneToMany(mappedBy = "notification")
	private List<NotificationRecipient> recipients = new ArrayList<>();

	public Notification() {

	}
	public Notification(Integer type, String subject, String content, IdsmedAccount sender) {
		this.type = type;
		this.subject = subject;
		this.content = content;
		this.sender = sender;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public IdsmedAccount getSender() {
		return sender;
	}

	public void setSender(IdsmedAccount sender) {
		this.sender = sender;
	}

	public List<NotificationRecipient> getRecipients() {
		return recipients;
	}

	public void setRecipients(List<NotificationRecipient> recipients) {
		this.recipients = recipients;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

}

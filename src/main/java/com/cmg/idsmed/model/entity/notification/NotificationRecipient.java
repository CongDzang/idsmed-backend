package com.cmg.idsmed.model.entity.notification;

import com.cmg.idsmed.model.entity.AbstractEntity;
import com.cmg.idsmed.model.entity.auth.IdsmedAccount;

import javax.persistence.*;

@SuppressWarnings("serial")
@Entity
@Table(name = "notification_recipient")
public class NotificationRecipient extends AbstractEntity {

	public enum NotificationRecipientStatus {
		CREATE(0, "created"),
		SENT(1, "sent"),
		ERROR(2, "error"),
		DELIVERED(3, "delivered"),
		READ(4, "read"),
		ARCHIVED(5, "archived");
		private Integer code;
		private String name;
		NotificationRecipientStatus(Integer code, String name) {
			this.code = code;
			this.name = name;
		}

		public Integer getCode() {
			return code;
		}

		public void setCode(Integer code) {
			this.code = code;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}
	}

	//We prefer sequence generator than identity. If you use identity it may disable JDBC batching
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "notification_id_seq")
	@SequenceGenerator(name = "notification_id_seq",
			sequenceName = "notification_id_seq",
			allocationSize = 1)
	@Column(updatable = false)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "idsmed_account_id", nullable = false)
	private IdsmedAccount recipient;

	@ManyToOne
	@JoinColumn(name = "notification_id", nullable = false)
	private Notification notification;

	public NotificationRecipient() {
	}

	public NotificationRecipient(IdsmedAccount recipient, Notification notification) {
		this.recipient = recipient;
		this.notification = notification;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public IdsmedAccount getRecipient() {
		return recipient;
	}

	public void setRecipient(IdsmedAccount recipient) {
		this.recipient = recipient;
	}

	public Notification getNotification() {
		return notification;
	}

	public void setNotification(Notification notification) {
		this.notification = notification;
	}

}

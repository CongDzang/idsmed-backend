package com.cmg.idsmed.model.entity.procurement;

import com.cmg.idsmed.dto.procurement.PurchaseOrderRequest;
import com.cmg.idsmed.model.entity.AbstractEntity;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * This entity use ProcurementOrderStatusEnum for order status
 * This entity use ProcurementPaymentMethodEnum for payment method
 */
//Main Order Table
@Entity
@Table(name = "od_big_order")
public class OdBigOrder extends AbstractEntity implements Serializable {
    private static final long serialVersionUID = 5771796917019428678L;

    //We prefer sequence generator than identity. If you use identity it may disable JDBC batching
    //Logical primary key
    @Id
    @GeneratedValue(strategy= GenerationType.SEQUENCE, generator = "od_big_order_id_seq")
    @SequenceGenerator(name = "od_big_order_id_seq",
            sequenceName = "od_big_order_id_seq",
            allocationSize = 1)
    @Column(updatable = false)
    private Long id;

    //Master Order No
    @Column(name = "big_order_no")
    @Size(max = 32)
    @NotNull
    private String bigOrderNo;

    //title
    @Column(name = "title")
    @Size(max = 64)
    @NotNull
    private String title;

    //user id: This is wedoctorCustomerId?
    @Column(name = "user_id")
    private Integer userId;

    //Member phone number
    @Column(name = "mobile")
    @Size(max = 16)
    @NotNull
    private String mobile;

    /**
     * Order Status :
     * 0 - Created
     * 1 - Paid
     * 2 - Shipped
     * 3 - Received
     * 8 - Completed
     * 9 - Cancelled
     * 10 - Vendor Cancelled
     */
    @Column(name = "order_status")
    @NotNull
    private Integer orderStatus;

    /**
     * Pay Method :
     * 1 - WeChat Pay
     * 2 - AliPay
     * 3 - Offline Payment
     */
    @Column(name = "pay_method")
    private Integer payMethod;

    //Payment status
    @Column(name = "pay_status")
    @NotNull
    private Integer payStatus;

    /**
     * Whether to self-operate orders
     * 1 - Yes
     * 0 - No
     */
    @Column(name = "is_self_order")
    private Integer isSelfOrder;

    //Total amount
    @Column(name = "total_amount", precision = 18, scale = 2)
    @NotNull
    private BigDecimal totalAmount;

    // total goods amount
    @Column(name = "total_goods_amount", precision = 18, scale = 2)
    @NotNull
    private BigDecimal totalGoodsAmount;

    // Order total shipping cost
    @Column(name = "total_freight", precision = 18, scale = 2)
    @NotNull
    private BigDecimal totalFreight;

    //Total discount amount of the procurement
    @Column(name = "total_discount_amount", precision = 18, scale = 2)
    @NotNull
    private BigDecimal totalDiscountAmount;

    // The amount actually paid
    @Column(name = "real_paid_amount", precision = 18, scale = 2)
    @NotNull
    private BigDecimal realPaidAmount;

    // The time of order
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(pattern = "MM/dd/yyyy")
    @Column(name="order_time")
    @NotNull
    private Date orderTime;

    // Payment time
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(pattern = "MM/dd/yyyy")
    @Column(name="paid_time")
    @NotNull
    private Date paidTime;

    // Payment serial number
    @Column(name = "pay_no")
    @Size(max = 32)
    private String payNo;

    // order notes
    @Column(name = "remark")
    @Size(max = 128)
    private String remark;

    @Column(name = "tender_no")
    @Size(max = 100)
    private String tenderNo;

    // Creation time
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(pattern = "MM/dd/yyyy")
    @Column(name="create_time")
    @NotNull
    private Date createTime;

    // update time
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(pattern = "MM/dd/yyyy")
    @Column(name="update_time")
    private Date updateTime;

    @OneToMany(mappedBy = "odBigOrder")
    private List<OdSmallOrder> odSmallOrderList;

    public OdBigOrder() {
    }

    public OdBigOrder(PurchaseOrderRequest request) {
        this.bigOrderNo = request.getBigOrderNo();
        this.title = request.getTitle();
        this.userId = request.getUserId();
        this.mobile = request.getMobile();
        this.orderStatus = request.getOrderStatus();
        this.payMethod = request.getPayMethod();
        this.payStatus = request.getPayStatus();
        this.isSelfOrder = request.getIsSelfOrder();
        this.totalAmount = request.getTotalAmount();
        this.totalGoodsAmount = request.getTotalGoodsAmount();
        this.totalFreight = request.getTotalFreight();
        this.totalDiscountAmount = request.getTotalDiscountAmount();
        this.realPaidAmount = request.getRealPaidAmount();
        this.orderTime = request.getOrderTime();
        this.paidTime = request.getPaidTime();
        this.payNo = request.getPayNo();
        this.remark = request.getRemark();
        this.tenderNo = request.getTenderNo();
        this.createTime = new Date();
        this.updateTime = new Date();
    }

    public Long getId() { return id; }

    public void setId(Long id) { this.id = id; }

    public String getBigOrderNo() {
        return bigOrderNo;
    }

    public void setBigOrderNo(String bigOrderNo) { this.bigOrderNo = bigOrderNo; }

    public String getTitle() { return title; }

    public void setTitle(String title) { this.title = title; }

    public Integer getUserId() { return userId; }

    public void setUserId(Integer userId) { this.userId = userId; }

    public String getMobile() { return mobile; }

    public void setMobile(String mobile) { this.mobile = mobile; }

    public Integer getOrderStatus() { return orderStatus; }

    public void setOrderStatus(Integer orderStatus) { this.orderStatus = orderStatus; }

    public Integer getPayMethod() { return payMethod; }

    public void setPayMethod(Integer payMethod) { this.payMethod = payMethod; }

    public Integer getPayStatus() { return payStatus; }

    public void setPayStatus(Integer payStatus) { this.payStatus = payStatus; }

    public Integer getIsSelfOrder() {
        return isSelfOrder;
    }

    public void setIsSelfOrder(Integer isSelfOrder) {
        this.isSelfOrder = isSelfOrder;
    }

    public BigDecimal getTotalAmount() { return totalAmount; }

    public void setTotalAmount(BigDecimal totalAmount) { this.totalAmount = totalAmount; }

    public BigDecimal getTotalGoodsAmount() { return totalGoodsAmount; }

    public void setTotalGoodsAmount(BigDecimal totalGoodsAmount) { this.totalGoodsAmount = totalGoodsAmount; }

    public BigDecimal getTotalFreight() { return totalFreight; }

    public void setTotalFreight(BigDecimal totalFreight) { this.totalFreight = totalFreight; }

    public BigDecimal getTotalDiscountAmount() { return totalDiscountAmount; }

    public void setTotalDiscountAmount(BigDecimal totalDiscountAmount) { this.totalDiscountAmount = totalDiscountAmount; }

    public BigDecimal getRealPaidAmount() { return realPaidAmount; }

    public void setRealPaidAmount(BigDecimal realPaidAmount) { this.realPaidAmount = realPaidAmount; }

    public Date getOrderTime() { return orderTime; }

    public void setOrderTime(Date orderTime) { this.orderTime = orderTime; }

    public Date getPaidTime() { return paidTime; }

    public void setPaidTime(Date paidTime) { this.paidTime = paidTime; }

    public String getPayNo() { return payNo; }

    public void setPayNo(String payNo) { this.payNo = payNo; }

    public String getRemark() { return remark; }

    public void setRemark(String remark) { this.remark = remark; }

    public String getTenderNo() {
        return tenderNo;
    }

    public void setTenderNo(String tenderNo) {
        this.tenderNo = tenderNo;
    }

    public Date getCreateTime() { return createTime; }

    public void setCreateTime(Date createTime) { this.createTime = createTime; }

    public Date getUpdateTime() { return updateTime; }

    public void setUpdateTime(Date updateTime) { this.updateTime = updateTime; }

    public List<OdSmallOrder> getOdSmallOrderList() { return odSmallOrderList; }

    public void setOdSmallOrderList(List<OdSmallOrder> odSmallOrderList) { this.odSmallOrderList = odSmallOrderList; }

}

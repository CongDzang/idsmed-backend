package com.cmg.idsmed.model.entity.procurement;

import com.cmg.idsmed.model.entity.AbstractEntity;

import javax.persistence.*;

@Entity
@Table(name = "od_delivery_product")
public class OdDeliveryProduct extends AbstractEntity {

	private static final long serialVersionUID = 5771796917019428678L;

	//We prefer sequence generator than identity. If you use identity it may disable JDBC batching
	// Logical Primary Key
	@Id
	@GeneratedValue(strategy= GenerationType.SEQUENCE, generator = "od_delivery_product_id_seq")
	@SequenceGenerator(name = "od_delivery_product_id_seq",
			sequenceName = "od_delivery_product_id_seq",
			allocationSize = 1)
	@Column(updatable = false)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "od_order_delivery_id")
	private OdOrderDelivery odOrderDelivery;

	@Column(name = "goods_id")
	private Long goodsId;

	@Column(name = "num")
	private Integer num;

	public OdDeliveryProduct() {
	}

	public OdDeliveryProduct(OdOrderItem odOrderItem) {
		if (odOrderItem != null) {
			this.goodsId = odOrderItem.getGoodsId();
			this.num = odOrderItem.getNum();
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public OdOrderDelivery getOdOrderDelivery() {
		return odOrderDelivery;
	}

	public void setOdOrderDelivery(OdOrderDelivery odOrderDelivery) {
		this.odOrderDelivery = odOrderDelivery;
	}

	public Long getGoodsId() {
		return goodsId;
	}

	public void setGoodsId(Long goodsId) {
		this.goodsId = goodsId;
	}

	public Integer getNum() {
		return num;
	}

	public void setNum(Integer num) {
		this.num = num;
	}
}

package com.cmg.idsmed.model.entity.procurement;

import com.cmg.idsmed.dto.procurement.OrderReceiveInfo;
import com.cmg.idsmed.model.entity.AbstractEntity;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

//Order delivery information form
@Entity
@Table(name = "od_order_delivery")
public class OdOrderDelivery extends AbstractEntity implements Serializable {
    private static final long serialVersionUID = 5771796917019428678L;

    //We prefer sequence generator than identity. If you use identity it may disable JDBC batching
    // Logical Primary Key
    @Id
    @GeneratedValue(strategy= GenerationType.SEQUENCE, generator = "od_order_delivery_id_seq")
    @SequenceGenerator(name = "od_order_delivery_id_seq",
            sequenceName = "od_order_delivery_id_seq",
            allocationSize = 1)
    @Column(updatable = false)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "od_small_order_id")
    private OdSmallOrder odSmallOrder;

    // Small order number
    @Column(name = "small_order_no")
    @Size(max = 32)
    private String smallOrderNo;

    // Member id
    @Column(name = "member_id")
    @NotNull
    private Long memberId;

    // Shipping fee
    @Column(name = "freight", precision = 18, scale = 2)
    @NotNull
    private BigDecimal freight;

    // Shipping type
    @Column(name = "ship_type")
    private Integer shipType;

    // Logistics company ID
    @Column(name = "logis_comp_id")
    private Integer logisCompId;

    // Logistics company code
    @Column(name = "logis_comp_code")
    @Size(max = 16)
    private String logisCompCode;

    // Logistics company name
    @Column(name = "logis_comp_name")
    @Size(max = 64)
    private String logisCompName;

    // Logistics company delivery number
    @Column(name = "delivery_no")
    @Size(max = 32)
    private String deliveryNo;

    // Consignee mobile number
    @Column(name = "mobile")
    @Size(max = 16)
    @NotNull
    private String mobile;

    // identity number
    @Column(name = "idcard_no")
    @Size(max = 32)
    private String idcardNo;

    // Remarks
    @Column(name = "remark")
    @Size(max = 128)
    private String remark;

    // create time
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(pattern = "MM/dd/yyyy")
    @Column(name="create_time")
    @NotNull
    private Date createTime;

    // Express delivery status, get updates from third-party interfaces
    @Column(name = "mail_status")
    private Integer mailStatus;

    // Transaction update time, used to prevent frequent calls to third-party logistics query interfaces
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(pattern = "MM/dd/yyyy")
    @Column(name="mail_update_time")
    private Date mailUpdateTime;

    //supplier_delivery_no
    @Column(name="supplier_delivery_no")
    @Size(max = 32)
    private String supplierDeliveryNo;

    @OneToOne(mappedBy = "odOrderDelivery")
    private OdOrderDeliveryRecord odOrderDeliveryRecord;

    @OneToMany(mappedBy = "odOrderDelivery")
    private List<OdDeliveryProduct> odDeliveryProducts;

    public OdOrderDelivery() {
    }

    public OdOrderDelivery(OrderReceiveInfo orderReceiveInfo){
        this.mobile = orderReceiveInfo.getMobile();
        this.memberId = orderReceiveInfo.getMemberId();
        this.freight = orderReceiveInfo.getFreight();
    }

    public Long getId() { return id; }

    public void setId(Long id) { this.id = id; }

    public String getMobile() { return mobile; }

    public void setMobile(String mobile) { this.mobile = mobile; }

    public Date getCreateTime() { return createTime; }

    public void setCreateTime(Date createTime) { this.createTime = createTime; }

    public String getSmallOrderNo() { return smallOrderNo; }

    public void setSmallOrderNo(String smallOrderNo) { this.smallOrderNo = smallOrderNo; }

    public OdSmallOrder getOdSmallOrder() { return odSmallOrder; }

    public void setOdSmallOrder(OdSmallOrder odSmallOrder) { this.odSmallOrder = odSmallOrder; }

    public String getIdcardNo() { return idcardNo; }

    public void setIdcardNo(String idcardNo) { this.idcardNo = idcardNo; }

    public Long getMemberId() { return memberId; }

    public void setMemberId(Long memberId) { this.memberId = memberId; }

    public BigDecimal getFreight() { return freight; }

    public void setFreight(BigDecimal freight) { this.freight = freight; }

    public Integer getShipType() { return shipType; }

    public void setShipType(Integer shipType) { this.shipType = shipType; }

    public Integer getLogisCompId() { return logisCompId; }

    public void setLogisCompId(Integer logisCompId) { this.logisCompId = logisCompId; }

    public String getLogisCompCode() { return logisCompCode; }

    public void setLogisCompCode(String logisCompCode) { this.logisCompCode = logisCompCode; }

    public String getLogisCompName() { return logisCompName; }

    public void setLogisCompName(String logisCompName) { this.logisCompName = logisCompName; }

    public String getDeliveryNo() { return deliveryNo; }

    public void setDeliveryNo(String deliveryNo) { this.deliveryNo = deliveryNo; }

    public String getRemark() { return remark; }

    public void setRemark(String remark) { this.remark = remark; }

    public Integer getMailStatus() { return mailStatus; }

    public void setMailStatus(Integer mailStatus) { this.mailStatus = mailStatus; }

    public Date getMailUpdateTime() { return mailUpdateTime; }

    public void setMailUpdateTime(Date mailUpdateTime) { this.mailUpdateTime = mailUpdateTime; }

    public OdOrderDeliveryRecord getOdOrderDeliveryRecord() { return odOrderDeliveryRecord; }

    public void setOdOrderDeliveryRecord(OdOrderDeliveryRecord odOrderDeliveryRecord) { this.odOrderDeliveryRecord = odOrderDeliveryRecord; }

    public String getSupplierDeliveryNo() {
        return supplierDeliveryNo;
    }

    public void setSupplierDeliveryNo(String supplierDeliveryNo) {
        this.supplierDeliveryNo = supplierDeliveryNo;
    }

    public List<OdDeliveryProduct> getOdDeliveryProducts() {
        return odDeliveryProducts;
    }

    public void setOdDeliveryProducts(List<OdDeliveryProduct> odDeliveryProducts) {
        this.odDeliveryProducts = odDeliveryProducts;
    }
}

package com.cmg.idsmed.model.entity.procurement;

import com.cmg.idsmed.model.entity.AbstractEntity;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

//Order logistics record
@Entity
@Table(name = "od_order_delivery_record")
public class OdOrderDeliveryRecord extends AbstractEntity implements Serializable {
    private static final long serialVersionUID = 5771796917019428678L;

    //We prefer sequence generator than identity. If you use identity it may disable JDBC batching
    // Logical Primary Key
    @Id
    @GeneratedValue(strategy= GenerationType.SEQUENCE, generator = "od_order_delivery_record_id_seq")
    @SequenceGenerator(name = "od_order_delivery_record_id_seq",
            sequenceName = "od_order_delivery_record_id_seq",
            allocationSize = 1)
    @Column(updatable = false)
    private Long id;

    @OneToOne
    @JoinColumn(name = "od_order_delivery_id")
    private OdOrderDelivery odOrderDelivery;

    @Column(name = "delivery_id ")
    private Long deliveryId;

    // Logistics transaction number
    @Column(name = "delivery_no")
    @Size(max = 32)
    private String deliveryNo;

    @Column(name = "delivery_time")
    @Size(max = 32)
    private String deliveryTime;

    // Logistics information
    @Column(name = "delivery_info")
    @Size(max = 512)
    private String deliveryInfo;

    public OdOrderDeliveryRecord() {
    }

    public OdOrderDeliveryRecord(OdOrderDelivery odOrderDelivery) {
        if (odOrderDelivery != null) {
            this.odOrderDelivery = odOrderDelivery;
            this.deliveryNo = odOrderDelivery.getDeliveryNo();
        }
    }

    public Long getId() { return id; }

    public void setId(Long id) { this.id = id; }

    public String getDeliveryNo() { return deliveryNo; }

    public void setDeliveryNo(String deliveryNo) { this.deliveryNo = deliveryNo; }

    public OdOrderDelivery getOdOrderDelivery() { return odOrderDelivery; }

    public void setOdOrderDelivery(OdOrderDelivery odOrderDelivery) { this.odOrderDelivery = odOrderDelivery; }

    public Long getDeliveryId() { return deliveryId; }

    public void setDeliveryId(Long deliveryId) { this.deliveryId = deliveryId; }

    public String getDeliveryTime() { return deliveryTime; }

    public void setDeliveryTime(String deliveryTime) { this.deliveryTime = deliveryTime; }

    public String getDeliveryInfo() { return deliveryInfo; }

    public void setDeliveryInfo(String deliveryInfo) { this.deliveryInfo = deliveryInfo; }

}

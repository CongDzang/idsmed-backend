package com.cmg.idsmed.model.entity.procurement;

import com.cmg.idsmed.dto.procurement.ProductOrderRequest;
import com.cmg.idsmed.model.entity.AbstractEntity;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

//Order item list
@Entity
@Table(name = "od_order_item")
public class OdOrderItem extends AbstractEntity implements Serializable {
    private static final long serialVersionUID = 5771796917019428678L;

    //We prefer sequence generator than identity. If you use identity it may disable JDBC batching
    // Logical primary key
    @Id
    @GeneratedValue(strategy= GenerationType.SEQUENCE, generator = "od_order_item_id_seq")
    @SequenceGenerator(name = "od_order_item_id_seq",
            sequenceName = "od_order_item_id_seq",
            allocationSize = 1)
    @Column(updatable = false)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "od_small_order_id")
    private OdSmallOrder odSmallOrder;

    // Small Order Number
    @Column(name = "small_order_no")
    @Size(max = 32)
    @NotNull
    private String smallOrderNo;

    //title
    @Column(name = "title")
    @Size(max = 64)
    @NotNull
    private String title;

    // Product ID
    @Column(name = "goods_id")
    @NotNull
    private Long goodsId;

    // Product default picture
    @Column(name = "goods_pic")
    @Size(max = 255)
    private String goodsPic;

    // SKU code, I have to give it default a bcs request has no
    @Column(name = "sku_code")
    @Size(max = 32)
    @NotNull
    private String skuCode = "a";

    // Specification model
    @Column(name = "property")
    @Size(max = 64)
    @NotNull
    private String property;

    // price
    @Column(name = "price", precision = 8, scale = 2)
    @NotNull
    private BigDecimal price;

    // in stock
    @Column(name = "num")
    @NotNull
    private Integer num;

    // weight
    @Column(name = "weight")
    private Double weight;

    // The amount of goods
    @Column(name = "item_amount", precision = 8, scale = 2)
    @NotNull
    private BigDecimal itemAmount;

    // Discount amount
    @Column(name = "discount_amount", precision = 8, scale = 2)
    private BigDecimal discountAmount = BigDecimal.ZERO;

    // The amount actually paid
    @Column(name = "real_paid_amount", precision = 8, scale = 2)
    private BigDecimal realPaidAmount = BigDecimal.ZERO;

    //create time
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(pattern = "MM/dd/yyyy")
    @Column(name="create_time")
    @NotNull
    private Date createTime;

    // update time
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(pattern = "MM/dd/yyyy")
    @Column(name="update_time")
    private Date updateTime;

    //warehouse id
    @Column(name = "warehouse_id")
    private Integer warehouseId;

    /**
     * Delivery Status
     * 1 - YES
     * 0 - NO
     */
    @Column(name = "send_out_status ")
    private Integer sendOutStatus;

    // Order logistics information ID
    @Column(name = "delivery_id")
    private Long deliveryId;

    /**
     * Order Status :
     * 0 - Created
     * 1 - Paid
     * 2 - Shipped
     * 3 - Received
     * 8 - Completed
     * 9 - Cancelled
     * 10 - Vendor Cancelled
     */
    @Column(name = "order_status")
    @NotNull
    private Integer orderStatus;

    @Column(name = "cancel_reason")
    private String cancelReason;

    public OdOrderItem() {
    }

    public OdOrderItem(ProductOrderRequest request) {

        this.smallOrderNo = request.getSmallOrderNo();
        this.title = request.getTitle();
        this.goodsId = request.getProductId();
        this.property = request.getProperty();
        this.price = request.getPrice();
        this.discountAmount = request.getDiscountAmount();
        this.realPaidAmount = request.getRealPaidAmount();

        this.num = request.getNum();
        this.itemAmount = request.getItemAmount();
        this.createTime = new Date();
        this.updateTime = new Date();
        this.warehouseId = request.getWarehouseId();
        this.sendOutStatus = request.getSendOutStatus();
//        // Order logistics information ID
//        @Column(name = "delivery_id")
//        private Long deliveryId;
    }

    public Long getId() { return id; }

    public void setId(Long id) { this.id = id; }

    public String getTitle() { return title; }

    public void setTitle(String title) { this.title = title; }

    public BigDecimal getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(BigDecimal discountAmount) {
        this.discountAmount = discountAmount;
    }

    public BigDecimal getRealPaidAmount() { return realPaidAmount; }

    public void setRealPaidAmount(BigDecimal realPaidAmount) { this.realPaidAmount = realPaidAmount; }

    public Date getCreateTime() { return createTime; }

    public void setCreateTime(Date createTime) { this.createTime = createTime; }

    public Date getUpdateTime() { return updateTime; }

    public void setUpdateTime(Date updateTime) { this.updateTime = updateTime; }

    public String getSmallOrderNo() { return smallOrderNo; }

    public void setSmallOrderNo(String smallOrderNo) { this.smallOrderNo = smallOrderNo; }

    public OdSmallOrder getOdSmallOrder() { return odSmallOrder; }

    public void setOdSmallOrder(OdSmallOrder odSmallOrder) { this.odSmallOrder = odSmallOrder; }

    public Long getGoodsId() { return goodsId; }

    public void setGoodsId(Long goodsId) { this.goodsId = goodsId; }

    public String getGoodsPic() { return goodsPic; }

    public void setGoodsPic(String goodsPic) { this.goodsPic = goodsPic; }

    public String getSkuCode() { return skuCode; }

    public void setSkuCode(String skuCode) { this.skuCode = skuCode; }

    public String getProperty() { return property; }

    public void setProperty(String property) { this.property = property; }

    public BigDecimal getPrice() { return price; }

    public void setPrice(BigDecimal price) { this.price = price; }

    public Integer getNum() { return num; }

    public void setNum(Integer num) { this.num = num; }

    public Double getWeight() { return weight; }

    public void setWeight(Double weight) { this.weight = weight; }

    public BigDecimal getItemAmount() { return itemAmount; }

    public void setItemAmount(BigDecimal itemAmount) { this.itemAmount = itemAmount; }

    public Integer getWarehouseId() { return warehouseId; }

    public void setWarehouseId(Integer warehouseId) { this.warehouseId = warehouseId; }

    public Integer getSendOutStatus() { return sendOutStatus; }

    public void setSendOutStatus(Integer sendOutStatus) { this.sendOutStatus = sendOutStatus; }

    public Long getDeliveryId() { return deliveryId; }

    public void setDeliveryId(Long deliveryId) { this.deliveryId = deliveryId; }

    public Integer getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(Integer orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getCancelReason() {
        return cancelReason;
    }

    public void setCancelReason(String cancelReason) {
        this.cancelReason = cancelReason;
    }

}

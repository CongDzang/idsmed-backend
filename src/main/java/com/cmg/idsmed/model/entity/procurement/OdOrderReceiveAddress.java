package com.cmg.idsmed.model.entity.procurement;

import com.cmg.idsmed.dto.procurement.OrderReceiveInfo;
import com.cmg.idsmed.model.entity.AbstractEntity;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

//Order receipt information form
@Entity
@Table(name = "od_order_receive_address")
public class OdOrderReceiveAddress extends AbstractEntity implements Serializable {
    private static final long serialVersionUID = 5771796917019428678L;

    //We prefer sequence generator than identity. If you use identity it may disable JDBC batching
    //Logical primary key
    @Id
    @GeneratedValue(strategy= GenerationType.SEQUENCE, generator = "od_order_receive_address_id_seq")
    @SequenceGenerator(name = "od_order_receive_address_id_seq",
            sequenceName = "od_order_receive_address_id_seq",
            allocationSize = 1)
    @Column(updatable = false)
    private Long id;

    @OneToOne
    @JoinColumn(name = "od_small_order_id")
    private OdSmallOrder odSmallOrder;

    // Small Order Number
    @Column(name = "small_order_no")
    @Size(max = 32)
    @NotNull
    private String smallOrderNo;

    // Member phone number
    @Column(name = "mobile")
    @Size(max = 16)
    @NotNull
    private String mobile;

    // Consignee name
    @Column(name = "receiver_name")
    @Size(max = 32)
    @NotNull
    private String receiverName;

    // Province Code
    @Column(name = "province_code")
    @Size(max = 16)
    @NotNull
    private String provinceCode;

    // City code
    @Column(name = "city_code")
    @Size(max = 16)
    private String cityCode;

    // Area code
    @Column(name = "district_code")
    @Size(max = 16)
    private String districtCode;

    // province name
    @Column(name = "province_name")
    @Size(max = 16)
    @NotNull
    private String provinceName;

    // City name
    @Column(name = "city_name")
    @Size(max = 16)
    private String cityName;

    // Area name
    @Column(name = "district_name")
    @Size(max = 16)
    private String districtName;

    // address
    @Column(name = "address")
    @Size(max = 128)
    private String address;

    // identification number
    @Column(name = "idcard_no")
    @Size(max = 32)
    private String idcardNo;

    @Column(name = "billing_address")
    @Size(max = 1000)
    private String billingAddress;

    // create time
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(pattern = "MM/dd/yyyy")
    @Column(name="create_time")
    @NotNull
    private Date createTime;

    public OdOrderReceiveAddress() {
    }

    public OdOrderReceiveAddress(OrderReceiveInfo orderReceiveInfo){
        this.receiverName = orderReceiveInfo.getReceiverName();
        this.provinceCode = orderReceiveInfo.getProvinceCode();
        this.districtCode = orderReceiveInfo.getDistrictCode();
        this.cityCode = orderReceiveInfo.getCityCode();
        this.provinceName = orderReceiveInfo.getProvinceName();
        this.districtName = orderReceiveInfo.getDistrictName();
        this.cityName = orderReceiveInfo.getCityName();
        this.address = orderReceiveInfo.getAddress();
        this.createTime = orderReceiveInfo.getCreateTime();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public OdSmallOrder getOdSmallOrder() {
        return odSmallOrder;
    }

    public void setOdSmallOrder(OdSmallOrder odSmallOrder) {
        this.odSmallOrder = odSmallOrder;
    }

    public String getSmallOrderNo() {
        return smallOrderNo;
    }

    public void setSmallOrderNo(String smallOrderNo) {
        this.smallOrderNo = smallOrderNo;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getReceiverName() {
        return receiverName;
    }

    public void setReceiverName(String receiverName) {
        this.receiverName = receiverName;
    }

    public String getProvinceCode() {
        return provinceCode;
    }

    public void setProvinceCode(String provinceCode) {
        this.provinceCode = provinceCode;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    public String getDistrictCode() {
        return districtCode;
    }

    public void setDistrictCode(String districtCode) {
        this.districtCode = districtCode;
    }

    public String getProvinceName() {
        return provinceName;
    }

    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getDistrictName() {
        return districtName;
    }

    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getIdcardNo() {
        return idcardNo;
    }

    public void setIdcardNo(String idcardNo) {
        this.idcardNo = idcardNo;
    }

    public String getBillingAddress() {
        return billingAddress;
    }

    public void setBillingAddress(String billingAddress) {
        this.billingAddress = billingAddress;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}

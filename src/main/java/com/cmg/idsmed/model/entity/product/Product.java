package com.cmg.idsmed.model.entity.product;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.cmg.idsmed.common.enums.EquipmentTypeEnum;
import com.cmg.idsmed.common.enums.LangEnum;
import com.cmg.idsmed.model.entity.product.list.BestSellerProductGlobal;
import com.cmg.idsmed.model.entity.product.list.HistoryUserProductBySearch;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.springframework.format.annotation.DateTimeFormat;

import com.cmg.idsmed.common.enums.CountryCodeEnum;
import com.cmg.idsmed.dto.product.ProductUpload;
import com.cmg.idsmed.model.entity.AbstractEntity;
import com.cmg.idsmed.model.entity.vendor.Vendor;

@Entity
@Table(name = "product")
public class Product extends AbstractEntity implements Serializable {

    /**
	 * Status of this entity use StatusEnum pls go to see in detail..
	 */
	private static final long serialVersionUID = 5771796917019428678L;

	//We prefer sequence generator than identity. If you use identity it may disable JDBC batching
	@Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "product_id_seq")
    @SequenceGenerator(name = "product_id_seq",
            sequenceName = "product_id_seq",
            allocationSize = 1)
    @Column(updatable = false)
    private Long id;

    @Column(name = "product_version")
    private int productVersion;

    //Done
    @Column(name = "product_identity", unique = true)
    @NotNull
    @Size(max = 500)
    private String productIdentity;

    @Column(name = "product_name_primary")
    @NotNull
    @Size(max = 1000)
    private String productNamePrimary;

    @Column(name = "product_name_secondary")
    @Size(max = 1000)
    private String productNameSecondary;

    @Column(name = "product_code")
    @Size(max = 100)
    private String productCode;

    @Column(name = "product_model")
    @Size(max = 100)
    private String productModel;

    @Column(name = "product_description")
    @NotNull
    @Size(max = 10000)
    private String productDescription;

    @Column(name = "registration_number")
    @Size(max = 500)
    private String registrationNumber;

    @Column(name = "hash_tag")
    @Size(max = 5000)
    private String hashTag;

    @Column(name = "exp_date", columnDefinition = "DATE")
    private LocalDateTime expDate;

    @Column(name = "unit")
    @Size(max = 100)
    private String unit;

    @Column(name = "packaging")
    @Size(max = 200)
    private String packaging;

    @Column(name = "min_of_order")
    private int minOfOrder;

    @Column(name = "verified_by")
    private Long verifiedBy;

    @Column(name = "approved_by")
    private Long approvedBy;

//    @Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "MM/dd/yyyy HH:mm:ss.SSS")
	@Column(name = "approved_datetime")
	private LocalDateTime approvedDate;

    @Column(name = "remarks")
    @Size(max = 3000)
    private String remarks;

    @Column(name = "country_code")
    @Size(max = 10)
    private String countryCode;

    @Column(name = "price", precision = 18, scale = 2)
    private BigDecimal price;

    @Column(name = "area")
    @Size(max = 1000)
    private String area;

	//This field using ProductTypeEnum
    @Column(name = "product_type")
	private Integer productType;

    @ManyToOne
    @JoinColumn(name = "product_brand_id")
    private ProductBrand productBrand;

	@Column(name = "other_description")
	@Size(max = 1000)
	private String otherDescription;

    @Column(name = "is_govt_subsidized")
	private Boolean isGovtSubsidized = false;

    @Column(name = "previous_status")
	private Integer previousStatus;

    @Column(name="can_procure")
	private Boolean canProcure = false;

    //Using DeviceCategoryEnum
    @Column(name="device_category")
	private Long deviceCategory;

	@Column(name="delivery_lead_time")
	@Size(max = 50)
	private String deliveryLeadTime;

	@Column(name="sku_number")
	private String skuNumber;

	@Column(name="product_functionality_and_claims")
	private String productFunctionlityAndClaims;

	@Column(name="disclaimer")
	private String disclaimer;

	@Column(name="instructions_for_use")
	private String instructionForUse;

	@Column(name="product_website")
	private String productWebsite;

	@Column(name="manufacturer_country_code")
	@Size(max = 50)
	private String manufacturerCountryCode;

	@Column(name="manufacturer_province_code")
	@Size(max = 50)
	private String manufacturerProvinceCode;

	@Column(name="equipment_type")
	private Integer equipmentType;

	@Column(name="pack_of_size")
	@Size(max = 1000)
	private String packOfSize;

	@Column(name = "manufacturer_location")
	@Size(max = 1000)
	private String manufacturerLocation;

	@Column(name = "patent")
	private String patent;

	@Column(name = "vendor_unit_price", precision = 18, scale = 2)
	private BigDecimal vendorUnitPrice;

	@Column(name = "vendor_price", precision = 18, scale = 2)
	private BigDecimal vendorPrice;

	@Column(name = "note")
	private String note;

	@Column(name = "inactive_reason")
	private String inactiveReason;

	@ManyToOne
	@JoinColumn(name = "zh_hans_l4_product_hierarchy_id")
	private ProductHierarchyLevelFour productHierarchyLevelFour;

	@NotFound(action = NotFoundAction.IGNORE)
    @OneToMany(mappedBy = "product")
	private List<ProductMedia> productMediaList;

    @OneToMany(mappedBy = "product")
    private List<ProductReject> productRejectList;
    
    @OneToMany(mappedBy = "product")
    private List<ProductFeatures> productFeaturesList;
    
    @OneToMany(mappedBy = "product")
    private List<ProductTechnical> productTechnicalList;

    @OneToMany(mappedBy = "product")
	private List<BestSellerProductGlobal> bestSellerProductGlobals;

    @OneToMany(mappedBy = "product")
	private List<HistoryUserProductBySearch> historyUserProductBySearches;

    @ManyToOne
    @JoinColumn(name = "product_category_id")
    private ProductCategory productCategory;

    @OneToMany(mappedBy = "product")
    private List<ProductCareAreaDetail> productCareAreaList;
    
    @ManyToOne
    @JoinColumn(name = "vendor_id")
    private Vendor vendor;

	@OneToMany(mappedBy = "product")
	private List<ProductDocumentAttachment> productDocumentAttachmentList;

	@OneToOne(mappedBy = "product")
	private ProductRating productRating;

	@OneToMany(mappedBy = "product")
	private List<ProductRatingDetail> productRatingDetailList;

	@OneToOne(mappedBy = "product")
	private ProductWishlist productWishlist;

	@OneToMany(mappedBy = "product")
	private List<ProductWishlistDetail> productWishlistDetailList;

	@OneToMany(mappedBy = "product")
	private List<ProductSecondCategoryDetail> productSecondCategoryDetails;

    public Product(ProductRawData data) {
		this.productVersion = 1;
		this.productIdentity = UUID.randomUUID().toString();
    	this.productNamePrimary = data.getProductNamePrimary();
		this.productNameSecondary = data.getProductNameSecondary();
		this.productCode = data.getProductCode();
		this.productModel = data.getProductModel();
		this.productDescription = data.getProductDes();
		this.registrationNumber = data.getProductRegNo();
		this.hashTag = data.getHashTag();
		this.unit = data.getUnit();
		this.packaging = data.getPackaging();
		this.minOfOrder = data.getMinOfOrder();
		this.price = data.getPrice();
		this.canProcure = data.getCanProcure();
		this.deviceCategory = data.getDeviceCategory();
	}
    
    public Product(ProductExcelRawData data) {
		this.productVersion = 1;
		this.productIdentity = UUID.randomUUID().toString();
		this.productNamePrimary = data.getProductNamePrimary();
		this.productNameSecondary = data.getProductNameSecondary();
		this.productCode = data.getProductCode();
		this.productModel = data.getProductModel();
		this.productDescription = data.getProductDes();
		this.registrationNumber = data.getProductRegNo();
		this.hashTag = data.getHashTag();
		this.unit = data.getUnit();
		this.packaging = data.getPackaging();
		this.minOfOrder = data.getMinOfOrder();
		this.price = data.getPrice();
		this.canProcure = data.getCanProcure();
		if(data.getEquipmentType().equals(EquipmentTypeEnum.MEDICAL_EQUIPMENT.getCode())) {
			this.deviceCategory = data.getDeviceCategory();
		}
		this.equipmentType = data.getEquipmentType();
		this.skuNumber = data.getSkuNumber();
		this.patent = data.getPatent();
		this.vendorUnitPrice = data.getVendorUnitPrice();
		this.vendorPrice = data.getVendorPrice();
		this.disclaimer = data.getDisclaimer();
		this.instructionForUse = data.getInstructionsForUse();
		this.productWebsite = data.getProductWebsite();
		this.note = data.getNote();
    }

    public Product() {
    	this.productVersion = 1;
    	this.productIdentity = UUID.randomUUID().toString();
    	this.countryCode = CountryCodeEnum.CHINA.getCode();
    }

    public Product(ProductUpload productUpload) { }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getProductVersion() {
		return productVersion;
	}

	public void setProductVersion(int productVersion) {
		this.productVersion = productVersion;
	}

	public String getProductIdentity() {
		return productIdentity;
	}

	public void setProductIdentity(String productIdentity) {
		this.productIdentity = productIdentity;
	}

	public String getProductNamePrimary() {
		return productNamePrimary;
	}

	public void setProductNamePrimary(String productNamePrimary) {
		this.productNamePrimary = productNamePrimary;
	}

	public String getProductNameSecondary() {
		return productNameSecondary;
	}

	public void setProductNameSecondary(String productNameSecondary) {
		this.productNameSecondary = productNameSecondary;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductModel() {
		return productModel;
	}

	public void setProductModel(String productModel) {
		this.productModel = productModel;
	}

	public String getProductDescription() {
		return productDescription;
	}

	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}

	public String getRegistrationNumber() {
		return registrationNumber;
	}

	public void setRegistrationNumber(String registrationNumber) {
		this.registrationNumber = registrationNumber;
	}

	public String getHashTag() {
		return hashTag;
	}

	public void setHashTag(String hashTag) {
		this.hashTag = hashTag;
	}

	public LocalDateTime getExpDate() {
		return expDate;
	}

	public void setExpDate(LocalDateTime expDate) {
		this.expDate = expDate;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getPackaging() {
		return packaging;
	}

	public void setPackaging(String packaging) {
		this.packaging = packaging;
	}

	public int getMinOfOrder() {
		return minOfOrder;
	}

	public void setMinOfOrder(int minOfOrder) {
		this.minOfOrder = minOfOrder;
	}

	public Long getVerifiedBy() {
		return verifiedBy;
	}

	public void setVerifiedBy(Long verifiedBy) {
		this.verifiedBy = verifiedBy;
	}

	public Long getApprovedBy() {
		return approvedBy;
	}

	public void setApprovedBy(Long approvedBy) {
		this.approvedBy = approvedBy;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public LocalDateTime getApprovedDate() {
		return approvedDate;
	}

	public void setApprovedDate(LocalDateTime approvedDate) {
		this.approvedDate = approvedDate;
	}

	public List<ProductMedia> getProductMediaList() {
		return productMediaList;
	}

	public void setProductMediaList(List<ProductMedia> productMediaList) {
		this.productMediaList = productMediaList;
	}

	public ProductBrand getProductBrand() {
		return productBrand;
	}

	public void setProductBrand(ProductBrand productBrand) {
		this.productBrand = productBrand;
	}

	public ProductCategory getProductCategory() {
		return productCategory;
	}

	public void setProductCategory(ProductCategory productCategory) {
		this.productCategory = productCategory;
	}

	public Vendor getVendor() {
		return vendor;
	}

	public void setVendor(Vendor vendor) {
		this.vendor = vendor;
	}

	public List<ProductReject> getProductRejectList() {
		return productRejectList;
	}

	public void setProductRejectList(List<ProductReject> productRejectList) {
		this.productRejectList = productRejectList;
	}

	public List<ProductFeatures> getProductFeaturesList() {
		return productFeaturesList;
	}

	public void setProductFeaturesList(List<ProductFeatures> productFeaturesList) { this.productFeaturesList = productFeaturesList; }
	
	public List<ProductTechnical> getProductTechnicalList() {
		return productTechnicalList;
	}

	public void setProductTechnicalList(List<ProductTechnical> productTechnicalList) { this.productTechnicalList = productTechnicalList; }

	public List<ProductCareAreaDetail> getProductCareAreaList() {
		return productCareAreaList;
	}

	public void setProductCareAreaDetailList(List<ProductCareAreaDetail> productCareAreaList) { this.productCareAreaList = productCareAreaList; }


	public String getOtherDescription() {
		return otherDescription;
	}

	public void setOtherDescription(String otherDescription) {
		this.otherDescription = otherDescription;
	}

	public Boolean getIsGovtSubsidized() {
		return isGovtSubsidized;
	}

	public void setIsGovtSubsidized(Boolean isGovtSubsidized) {
		this.isGovtSubsidized = isGovtSubsidized;
	}

	public List<ProductDocumentAttachment> getProductDocumentAttachmentList() { return productDocumentAttachmentList; }

	public void setProductDocumentAttachmentList(List<ProductDocumentAttachment> productDocumentAttachmentList) { this.productDocumentAttachmentList = productDocumentAttachmentList; }

	public ProductRating getProductRating() { return productRating; }

	public void setProductRating(ProductRating productRating) { this.productRating = productRating; }

	public List<ProductRatingDetail> getProductRatingDetailList() { return productRatingDetailList; }

	public void setProductRatingDetailList(List<ProductRatingDetail> productRatingDetailList) { this.productRatingDetailList = productRatingDetailList; }

	public ProductWishlist getProductWishlist() { return productWishlist; }

	public void setProductWishlist(ProductWishlist productWishlist) { this.productWishlist = productWishlist; }

	public List<ProductWishlistDetail> getProductWishlistDetailList() { return productWishlistDetailList; }

	public void setProductWishlistDetailList(List<ProductWishlistDetail> productWishlistDetailList) { this.productWishlistDetailList = productWishlistDetailList; }

	public Integer getPreviousStatus() {
		return previousStatus;
	}

	public void setPreviousStatus(Integer previousStatus) {
		this.previousStatus = previousStatus;
	}

	public boolean isCanProcure() {
		return canProcure;
	}

	public void setCanProcure(boolean canProcure) {
		this.canProcure = canProcure;
	}

	public Long getDeviceCategory() { return deviceCategory; }

	public void setDeviceCategory(Long deviceCategory) { this.deviceCategory = deviceCategory; }

	public String getDeliveryLeadTime() { return deliveryLeadTime; }

	public void setDeliveryLeadTime(String deliveryLeadTime) { this.deliveryLeadTime = deliveryLeadTime; }
	
	public String getSkuNumber() { return skuNumber; }

	public void setSkuNumber(String skuNumber) { this.skuNumber = skuNumber; }

	public String getProductFunctionlityAndClaims() { return productFunctionlityAndClaims; }

	public void setProductFunctionlityAndClaims(String productFunctionlityAndClaims) { this.productFunctionlityAndClaims = productFunctionlityAndClaims; }

	public String getDisclaimer() { return disclaimer; }

	public void setDisclaimer(String disclaimer) { this.disclaimer = disclaimer; }

	public String getInstructionForUse() { return instructionForUse; }

	public void setInstructionForUse(String instructionForUse) { this.instructionForUse = instructionForUse; }

	public String getProductWebsite() { return productWebsite; }

	public void setProductWebsite(String productWebsite) { this.productWebsite = productWebsite; }

	public String getManufacturerCountryCode() { return manufacturerCountryCode; }

	public void setManufacturerCountryCode(String manufacturerCountryCode) { this.manufacturerCountryCode = manufacturerCountryCode; }

	public String getManufacturerProvinceCode() { return manufacturerProvinceCode; }

	public void setManufacturerProvinceCode(String manufacturerProvinceCode) { this.manufacturerProvinceCode = manufacturerProvinceCode; }

	public Boolean getCanProcure() {
		return canProcure;
	}

	public void setCanProcure(Boolean canProcure) {
		this.canProcure = canProcure;
	}

	public List<BestSellerProductGlobal> getBestSellerProductGlobals() {
		return bestSellerProductGlobals;
	}

	public void setBestSellerProductGlobals(List<BestSellerProductGlobal> bestSellerProductGlobals) {
		this.bestSellerProductGlobals = bestSellerProductGlobals;
	}

	public List<HistoryUserProductBySearch> getHistoryUserProductBySearches() {
		return historyUserProductBySearches;
	}

	public void setHistoryUserProductBySearches(List<HistoryUserProductBySearch> historyUserProductBySearches) {
		this.historyUserProductBySearches = historyUserProductBySearches;
	}

	public void setProductCareAreaList(List<ProductCareAreaDetail> productCareAreaList) {
		this.productCareAreaList = productCareAreaList;
	}

	public List<ProductSecondCategoryDetail> getProductSecondCategoryDetails() {
		return productSecondCategoryDetails;
	}

	public void setProductSecondCategoryDetails(List<ProductSecondCategoryDetail> productSecondCategoryDetails) {
		this.productSecondCategoryDetails = productSecondCategoryDetails;
	}

	public Integer getProductType() {
		return productType;
	}

	public void setProductType(Integer productType) {
		this.productType = productType;
	}

	// this is always get product name primary regardless langcode according to teow.
	public String getNameByLangCode(String langCode) {
    	if (langCode == null) {
			return this.getProductNamePrimary();
		}
		if (LangEnum.ENGLISH.getCode().equals(langCode)) {
			return this.getProductNamePrimary();
		}

		if (LangEnum.CHINA.getCode().equals(langCode)) {
			return this.getProductNamePrimary();
		}

		return this.productNamePrimary;
	}

	public Integer getEquipmentType() { return equipmentType; }

	public void setEquipmentType(Integer equipmentType) { this.equipmentType = equipmentType; }

	public String getPackOfSize() { return packOfSize; }

	public void setPackOfSize(String packOfSize) { this.packOfSize = packOfSize; }

	public String getManufacturerLocation() { return manufacturerLocation; }

	public void setManufacturerLocation(String manufacturerLocation) { this.manufacturerLocation = manufacturerLocation; }

	public String getPatent() { return patent; }

	public void setPatent(String patent) { this.patent = patent; }

	public BigDecimal getVendorUnitPrice() { return vendorUnitPrice; }

	public void setVendorUnitPrice(BigDecimal vendorUnitPrice) { this.vendorUnitPrice = vendorUnitPrice; }

	public BigDecimal getVendorPrice() { return vendorPrice; }

	public void setVendorPrice(BigDecimal vendorPrice) { this.vendorPrice = vendorPrice; }

	public String getNote() { return note; }

	public void setNote(String note) { this.note = note; }

	public ProductHierarchyLevelFour getProductHierarchyLevelFour() {
		return productHierarchyLevelFour;
	}

	public void setProductHierarchyLevelFour(ProductHierarchyLevelFour productHierarchyLevelFour) {
		this.productHierarchyLevelFour = productHierarchyLevelFour;
	}

	public String getInactiveReason() {
		return inactiveReason;
	}

	public void setInactiveReason(String inactiveReason) {
		this.inactiveReason = inactiveReason;
	}
}

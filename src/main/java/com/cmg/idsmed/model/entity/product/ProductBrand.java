package com.cmg.idsmed.model.entity.product;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.cmg.idsmed.model.entity.AbstractEntity;

import java.util.List;

@Entity
@Table(name = "product_brand")
public class ProductBrand extends AbstractEntity {

	public static final String OTHERS_PRODUCT_BRAND_NAME = "Others";

    //We prefer sequence generator than identity. If you use identity it may disable JDBC batching
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "product_brand_id_seq")
    @SequenceGenerator(name = "product_brand_id_seq",
            sequenceName = "product_brand_id_seq",
            allocationSize = 1)
    @Column(updatable = false)
    private Long id;

    @Column(name = "brand_name")
    @NotNull
    @Size(max = 100)
    private String brandName;

    @Column(name = "brand_code")
    @NotNull
    @Size(max = 100)
    private String brandCode;

    @Column(name = "country_code")
    @Size(max = 10)
    private String countryCode;
    
    @Column(name = "area")
    @Size(max = 1000)
    private String area;

    @Column(name = "brand_image_url")
	private String brandImageURL;

    @OneToMany(mappedBy = "productBrand")
    private List<Product> productList;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public String getBrandCode() {
		return brandCode;
	}

	public void setBrandCode(String brandCode) {
		this.brandCode = brandCode;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getBrandImageURL() {
		return brandImageURL;
	}

	public void setBrandImageURL(String brandImageURL) {
		this.brandImageURL = brandImageURL;
	}

	public List<Product> getProductList() {
		return productList;
	}

	public void setProductList(List<Product> productList) {
		this.productList = productList;
	}

}

package com.cmg.idsmed.model.entity.product;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import com.cmg.idsmed.common.enums.LangEnum;
import com.cmg.idsmed.model.entity.AbstractEntity;

@Entity
@Table(name = "product_care_area")
public class ProductCareArea extends AbstractEntity {

	/**
	 *
	 */
	private static final long serialVersionUID = -226952265066780807L;

	public static final String OTHER_PRODUCT_CARE_AREA_NAME = "OTHERS";

	//We prefer sequence generator than identity. If you use identity it may disable JDBC batching
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "product_care_area_id_seq")
    @SequenceGenerator(name = "product_care_area_id_seq",
            sequenceName = "product_care_area_id_seq",
            allocationSize = 1)
    @Column(updatable = false)
    private Long id;

    @Column(name = "care_area_name_zh_cn")
    @Size(max = 300)
    private String careAreaNameZhCn;

    @Column(name = "care_area_name_zh_tw")
    @Size(max = 300)
    private String careAreaNameZhTw;

    @Column(name = "care_area_name_th")
    @Size(max = 300)
    private String careAreaNameTh;

    @Column(name = "care_area_name_id")
    @Size(max = 300)
    private String careAreaNameId;

    @Column(name = "care_area_name_vi")
    @Size(max = 300)
    private String careAreaNameVi;

    @Column(name = "care_area_name")
    @Size(max = 300)
    private String careAreaName;
    
    @Column(name = "document_path")
    @Size(max = 1000)
    private String documentPath;
    
    @Column(name = "document_url")
    @Size(max = 1000)
    private String documentUrl;

    @Column(name = "document_url_wedoctor")
    @Size(max = 1000)
    private String documentUrlWedoctor;
    
    @OneToMany(mappedBy = "productCareArea")
    private List<ProductCareAreaDetail> productCareAreaDetailList;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCareAreaNameZhCn() {
        return careAreaNameZhCn;
    }

    public void setCareAreaNameZhCn(String careAreaNameZhCn) {
        this.careAreaNameZhCn = careAreaNameZhCn;
    }

    public String getCareAreaNameZhTw() {
        return careAreaNameZhTw;
    }

    public void setCareAreaNameZhTw(String careAreaNameZhTw) {
        this.careAreaNameZhTw = careAreaNameZhTw;
    }

    public String getCareAreaNameTh() {
        return careAreaNameTh;
    }

    public void setCareAreaNameTh(String careAreaNameTh) {
        this.careAreaNameTh = careAreaNameTh;
    }

    public String getCareAreaNameId() {
        return careAreaNameId;
    }

    public void setCareAreaNameId(String careAreaNameId) {
        this.careAreaNameId = careAreaNameId;
    }

    public String getCareAreaNameVi() {
        return careAreaNameVi;
    }

    public void setCareAreaNameVi(String careAreaNameVi) {
        this.careAreaNameVi = careAreaNameVi;
    }

    public String getCareAreaName() {
        return careAreaName;
    }

    public void setCareAreaName(String careAreaName) {
        this.careAreaName = careAreaName;
    }

	public List<ProductCareAreaDetail> getProductCareAreaDetailList() {
		return productCareAreaDetailList;
	}

	public void setProductCareAreaDetailList(List<ProductCareAreaDetail> productCareAreaDetailList) {
		this.productCareAreaDetailList = productCareAreaDetailList;
	}

	public String getDocumentPath() {
		return documentPath;
	}

	public void setDocumentPath(String documentPath) {
		this.documentPath = documentPath;
	}

	public String getDocumentUrl() {
		return documentUrl;
	}

	public void setDocumentUrl(String documentUrl) {
		this.documentUrl = documentUrl;
	}

    public String getDocumentUrlWedoctor() {
        return documentUrlWedoctor;
    }

    public void setDocumentUrlWedoctor(String documentUrlWedoctor) {
        this.documentUrlWedoctor = documentUrlWedoctor;
    }

    public String getNameByLangCode(String langCode) {
    	if (langCode == null || langCode.equals(LangEnum.CHINA.getCode())) {
    		return this.getCareAreaNameZhCn();
		}

		if (langCode.equals(LangEnum.ENGLISH.getCode())) {
    		return this.getCareAreaName();
		}

		if (langCode.equals(LangEnum.CHINA_TAIWAN.getCode())) {
    		return this.getCareAreaNameZhTw();
		}

		if (langCode.equals(LangEnum.INDONESIAN.getCode())) {
			return this.getCareAreaNameId();
		}

		if (langCode.equals(LangEnum.THAI.getCode())) {
			return this.getCareAreaNameTh();
		}

		if (langCode.equals(LangEnum.VIETNAMESE.getCode())) {
    		return this.getCareAreaNameVi();
		}

		return this.getCareAreaNameZhCn();
	}
}


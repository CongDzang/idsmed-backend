package com.cmg.idsmed.model.entity.product;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.cmg.idsmed.common.enums.CountryCodeEnum;
import com.cmg.idsmed.model.entity.AbstractEntity;

@Entity
@Table(name = "product_care_area_detail")
public class ProductCareAreaDetail extends AbstractEntity {

    //We prefer sequence generator than identity. If you use identity it may disable JDBC batching
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "product_care_area_detail_id_seq")
    @SequenceGenerator(name = "product_care_area_detail_id_seq",
            sequenceName = "product_care_area_detail_id_seq",
            allocationSize = 1)
    @Column(updatable = false)
    private Long id;
    
    @Column(name="country_code")
    @NotNull
    @Size(max = 10)
    private String countryCode  = CountryCodeEnum.CHINA.getCode();

    @ManyToOne
    @JoinColumn(name = "product_id", referencedColumnName = "id")
    private Product product;

    @ManyToOne
    @JoinColumn(name = "product_care_area_id", referencedColumnName = "id")
    private ProductCareArea productCareArea;

    public ProductCareAreaDetail() {
    	
    }
    
    public ProductCareAreaDetail(ProductCareArea productCareArea, Product product) {
    	if(productCareArea != null) {
    		this.setProductCareArea(productCareArea);
    	}
    	if(product != null) {
    		this.setProduct(product);
    	}
    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public ProductCareArea getProductCareArea() {
		return productCareArea;
	}

	public void setProductCareArea(ProductCareArea productCareArea) {
		this.productCareArea = productCareArea;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

}


package com.cmg.idsmed.model.entity.product;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.cmg.idsmed.common.enums.LangEnum;
import com.cmg.idsmed.model.entity.AbstractEntity;

@Entity
@Table(name = "product_category")
public class ProductCategory extends AbstractEntity {

	public static final String OTHERS_PRODUCT_CATEGORY_NAME = "OTHERS";

    //We prefer sequence generator than identity. If you use identity it may disable JDBC batching
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "product_category_id_seq")
    @SequenceGenerator(name = "product_category_id_seq",
            sequenceName = "product_category_id_seq",
            allocationSize = 1)
    @Column(updatable = false)
    private Long id;

    @Column(name = "category_name")
    @Size(max = 300)
    private String categoryName;

    @Column(name = "category_name_zh_cn")
    @Size(max = 300)
    private String categoryNameZhCn;

    @Column(name = "category_name_zh_tw")
    @Size(max = 300)
    private String categoryNameZhTw;

    @Column(name = "category_name_th")
    @Size(max = 300)
    private String categoryNameTh;

    @Column(name = "category_name_id")
    @Size(max = 300)
    private String categoryNameId;
    
    @Column(name = "category_name_vi")
    @Size(max = 300)
    private String categoryNameVi;
    
    @OneToMany(mappedBy = "productCategory")
    private List<Product> productList;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getCategoryNameZhCn() {
		return categoryNameZhCn;
	}

	public void setCategoryNameZhCn(String categoryNameZhCn) {
		this.categoryNameZhCn = categoryNameZhCn;
	}

	public String getCategoryNameZhTw() {
		return categoryNameZhTw;
	}

	public void setCategoryNameZhTw(String categoryNameZhTw) {
		this.categoryNameZhTw = categoryNameZhTw;
	}

	public String getCategoryNameTh() {
		return categoryNameTh;
	}

	public void setCategoryNameTh(String categoryNameTh) {
		this.categoryNameTh = categoryNameTh;
	}

	public String getCategoryNameId() {
		return categoryNameId;
	}

	public void setCategoryNameId(String categoryNameId) {
		this.categoryNameId = categoryNameId;
	}

	public String getCategoryNameVi() {
		return categoryNameVi;
	}

	public void setCategoryNameVi(String categoryNameVi) {
		this.categoryNameVi = categoryNameVi;
	}

	public List<Product> getProductList() {
		return productList;
	}

	public void setProductList(List<Product> productList) {
		this.productList = productList;
	}

	public String getNameByLangCode(String langCode) {
		if (langCode == null || langCode.equals(LangEnum.CHINA.getCode())) {
			return this.getCategoryNameZhCn();
		}

		if (langCode.equals(LangEnum.ENGLISH.getCode())) {
			return this.getCategoryName();
		}

		if (langCode.equals(LangEnum.CHINA_TAIWAN.getCode())) {
			return this.getCategoryNameZhTw();
		}

		if (langCode.equals(LangEnum.INDONESIAN.getCode())) {
			return this.getCategoryNameId();
		}

		if (langCode.equals(LangEnum.THAI.getCode())) {
			return this.getCategoryNameTh();
		}

		if (langCode.equals(LangEnum.VIETNAMESE.getCode())) {
			return this.getCategoryNameVi();
		}

		return this.getCategoryNameZhCn();
	}

}

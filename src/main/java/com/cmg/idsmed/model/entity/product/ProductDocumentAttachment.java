package com.cmg.idsmed.model.entity.product;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.cmg.idsmed.common.utils.DateTimeHelper;
import com.cmg.idsmed.dto.product.ProductDocumentAttachmentRequest;
import com.cmg.idsmed.model.entity.AbstractEntity;
import com.cmg.idsmed.model.entity.product.config.ProductLabelConfig;
import org.springframework.util.StringUtils;

@Entity
@Table(name = "product_document_attachment")
public class ProductDocumentAttachment extends AbstractEntity implements Serializable {

	/**
	 * Status of this entity use FileAttachmentStatusEnum pls go to see in detail
	 */
	private static final long serialVersionUID = 5771796917019428678L;

	//We prefer sequence generator than identity. If you use identity it may disable JDBC batching
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "product_document_attachment_id_seq")
	@SequenceGenerator(name = "product_document_attachment_id_seq",
			sequenceName = "product_document_attachment_id_seq",
			allocationSize = 1)
	@Column(updatable = false)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "product_id")
	private Product product;

	@ManyToOne
	@JoinColumn (name = "product_label_config_id")
	private ProductLabelConfig productLabelConfig;
	
	@OneToMany(mappedBy = "productDocumentAttachment")
	private List<ProductDocumentAttachmentReminder> productDocumentAttachmentReminderList;

	@Column (name = "original_file_name")
	@Size(max = 1000)
	private String originalFileName;

	@Column (name = "document_name")
	@Size(max = 500)
	private String documentName;

	@Column (name = "document_secondary_name")
	@Size(max = 500)
	private String documentSecondaryName;

	@Column (name = "expiry_date")
	private LocalDateTime expiryDate;

	@Column(name = "exp_date_tz")
	private String expDateTimeZone;

	@Column (name = "document_type")
	@Size(max = 100)
	private String documentType;

	@Column (name = "document_path")
	@Size(max = 1000)
	private String documentPath;

	@Column(name = "remarks")
	@Size(max = 3000)
	private String remarks;

	@Column(name = "country_code")
	@NotNull
	@Size(max = 10)
	private String countryCode;

	@Column(name = "is_verify")
	private Boolean isVerify;

	@Column(name = "verify_by")
	private Long verifyBy;

	@Column(name = "verify_datetime")
	private LocalDateTime verifyDate;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public ProductLabelConfig getProductLabelConfig() {
		return productLabelConfig;
	}

	public void setProductLabelConfig(ProductLabelConfig productLabelConfig) {
		this.productLabelConfig = productLabelConfig;
	}

	public String getOriginalFileName() {
		return originalFileName;
	}

	public void setOriginalFileName(String originalFileName) {
		this.originalFileName = originalFileName;
	}

	public String getDocumentName() {
		return documentName;
	}

	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}

	public String getDocumentSecondaryName() {
		return documentSecondaryName;
	}

	public void setDocumentSecondaryName(String documentSecondaryName) {
		this.documentSecondaryName = documentSecondaryName;
	}

	public LocalDateTime getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(LocalDateTime expiryDate) {
		this.expiryDate = expiryDate;
	}

	public String getDocumentType() {
		return documentType;
	}

	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}

	public String getDocumentPath() {
		return documentPath;
	}

	public void setDocumentPath(String documentPath) {
		this.documentPath = documentPath;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public Boolean getIsVerify() { return isVerify; }

	public void setIsVerify(Boolean isVerify) { this.isVerify = isVerify; }

	public Long getVerifyBy() { return verifyBy; }

	public void setVerifyBy(Long verifyBy) { this.verifyBy = verifyBy; }

	public LocalDateTime getVerifyDate() {
		return verifyDate;
	}

	public void setVerifyDate(LocalDateTime verifyDate) {
		this.verifyDate = verifyDate;
	}

	public Boolean getVerify() {
		return isVerify;
	}

	public void setVerify(Boolean verify) {
		isVerify = verify;
	}
	
	public List<ProductDocumentAttachmentReminder> getProductDocumentAttachmentReminderList() {
		return productDocumentAttachmentReminderList;
	}

	public void setProductDocumentAttachmentReminderList(
			List<ProductDocumentAttachmentReminder> productDocumentAttachmentReminderList) {
		this.productDocumentAttachmentReminderList = productDocumentAttachmentReminderList;
	}

	public String getExpDateTimeZone() {
		return expDateTimeZone;
	}

	public void setExpDateTimeZone(String expDateTimeZone) {
		this.expDateTimeZone = expDateTimeZone;
	}

	public void updateDocumentAttachment(ProductDocumentAttachmentRequest request, Long updatedBy, String ossBucketDomain) {
		this.setStatus(request.getStatus());
		this.setVerify(request.getVerify());
		this.setExpiryDate(request.getExpiryDate() != null ? DateTimeHelper.convertZonedDateTimeToUTC(request.getExpiryDate()) : null);
		this.setExpDateTimeZone(request.getExpDateTimeZone());
		this.setOriginalFileName(request.getOriginalFileName());
		this.setDocumentSecondaryName(request.getDocumentSecondaryName());
		this.setDocumentName(request.getDocumentName());
		if (!StringUtils.isEmpty(request.getDocumentPath()) && !StringUtils.isEmpty(ossBucketDomain)) {
			this.setDocumentPath(request.getDocumentPath().replace(ossBucketDomain, ""));
		}
		this.setRemarks(request.getRemarks());
		if (updatedBy != null) {
			this.setUpdatedBy(updatedBy);
		}
	}
}

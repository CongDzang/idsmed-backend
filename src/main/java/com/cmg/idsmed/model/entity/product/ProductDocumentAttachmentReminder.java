package com.cmg.idsmed.model.entity.product;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.cmg.idsmed.common.enums.EntitySimpleStatusEnum;
import com.cmg.idsmed.dto.product.ProductDocumentAttachmentReminderRequest;
import com.cmg.idsmed.model.entity.AbstractEntity;

@Entity
@Table(name = "product_document_attachment_reminder")
public class ProductDocumentAttachmentReminder extends AbstractEntity implements Serializable {

	private static final long serialVersionUID = 5771796917019428678L;

	//We prefer sequence generator than identity. If you use identity it may disable JDBC batching
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "product_document_attachment_reminder_id_seq")
	@SequenceGenerator(name = "product_document_attachment_reminder_id_seq",
			sequenceName = "product_document_attachment_reminder_id_seq",
			allocationSize = 1)
	@Column(updatable = false)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "product_document_attachment_id")
	private ProductDocumentAttachment productDocumentAttachment;

	@Column (name = "days_before")
	private Integer daysBefore;

	@Column (name = "days_after")
	private Integer daysAfter;

	@Column (name = "in_expiry_date")
	private Boolean inExpiryDate;

	@Column (name = "by_email")
	private Boolean byEmail;

	@Column (name = "by_web")
	private Boolean byWeb;

	@Column (name = "by_sms")
	private Boolean bySms;

	public Long getId() {
		return id;
	}

	public ProductDocumentAttachment getProductDocumentAttachment() {
		return productDocumentAttachment;
	}

	public void setProductDocumentAttachment(ProductDocumentAttachment productDocumentAttachment) {
		this.productDocumentAttachment = productDocumentAttachment;
	}

	public Integer getDaysBefore() {
		return daysBefore;
	}

	public void setDaysBefore(Integer daysBefore) {
		this.daysBefore = daysBefore;
	}

	public Integer getDaysAfter() {
		return daysAfter;
	}

	public void setDaysAfter(Integer daysAfter) {
		this.daysAfter = daysAfter;
	}

	public Boolean getInExpiryDate() {
		return inExpiryDate;
	}

	public void setInExpiryDate(Boolean inExpiryDate) {
		this.inExpiryDate = inExpiryDate;
	}

	public Boolean getByEmail() {
		return byEmail;
	}

	public void setByEmail(Boolean byEmail) {
		this.byEmail = byEmail;
	}

	public Boolean getByWeb() {
		return byWeb;
	}

	public void setByWeb(Boolean byWeb) {
		this.byWeb = byWeb;
	}

	public Boolean getBySms() {
		return bySms;
	}

	public void setBySms(Boolean bySms) {
		this.bySms = bySms;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ProductDocumentAttachmentReminder() {
		super();
	}

	public ProductDocumentAttachmentReminder(ProductDocumentAttachmentReminderRequest reminderRequest){
		this.daysAfter = reminderRequest.getDaysAfter();
		this.daysBefore = reminderRequest.getDaysBefore();
		this.inExpiryDate = reminderRequest.getInExpiryDate();
	}
	public ProductDocumentAttachmentReminder(ProductDocumentAttachmentReminderRequest request, Long createdBy) {
		super();
		this.setDaysBefore(request.getDaysBefore());
		this.setDaysAfter(request.getDaysAfter());
		this.setInExpiryDate(request.getInExpiryDate());
		this.setByEmail(byEmail);
		this.setByWeb(byWeb);
		this.setBySms(bySms);
		this.setStatus(EntitySimpleStatusEnum.ACTIVE.getCode());
		if (createdBy != null) {
			this.setCreatedBy(createdBy);
		}
	}
	
	public ProductDocumentAttachmentReminder(Integer daysBefore, Integer daysAfter, Boolean inExpiryDate, Boolean byEmail, Boolean byWeb, Boolean bySms, Long createdBy) {
		super();
		this.setDaysBefore(daysBefore);
		this.setDaysAfter(daysAfter);
		this.setInExpiryDate(inExpiryDate);
		this.setByEmail(byEmail);
		this.setByWeb(byWeb);
		this.setBySms(bySms);
		this.setStatus(EntitySimpleStatusEnum.ACTIVE.getCode());
		if (createdBy != null) {
			this.setCreatedBy(createdBy);
		}
	}	
	
	public void updateProductDocumentAttachmentReminder(Integer daysBefore, Integer daysAfter, Boolean inExpiryDate, Boolean byEmail, Boolean byWeb, Boolean bySms, Long updatedBy) {
		this.setDaysBefore(daysBefore);
		this.setDaysAfter(daysAfter);
		this.setInExpiryDate(inExpiryDate);
		this.setByEmail(byEmail);
		this.setByWeb(byWeb);
		this.setBySms(bySms);
		this.setStatus(EntitySimpleStatusEnum.ACTIVE.getCode());
		if (updatedBy != null) {
			this.setUpdatedBy(updatedBy);
		}
	}
}

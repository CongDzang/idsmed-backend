package com.cmg.idsmed.model.entity.product;

import org.springframework.data.util.Pair;

import java.math.BigDecimal;
import java.util.List;

public class ProductExcelRawData {
	private Integer excelRowNum;
	private String careArea;
	private String category;
	private String brand;
	private String productNamePrimary;
	private String productNameSecondary;
	private String productCode;
	private String productModel;
	private String productRegNo;
	private String productDes;
	private String hashTag;
	private String unit;
	private BigDecimal price;
	private String packaging;
	private Integer minOfOrder;
	private String vendorName;
	private String brochureAttachmentUrl;
	private String videoAttachmentUrl;
	private List<String> imagesUrl;
	private String productTechnicalLabel;
	private String productTechnicalValue;
	private String productTechnicalLabelTwo;
	private String productTechnicalValueTwo;
	private String productTechnicalLabelThree;
	private String productTechnicalValueThree;
	private String productTechnicalLabelFour;
	private String productTechnicalValueFour;
	private String productTechnicalLabelFive;
	private String productTechnicalValueFive;
	private String productTechnicalLabelSix;
	private String productTechnicalValueSix;
	private List<Pair<String, String>> productTechnicals;
	private String productFeatureLabel;
	private String productFeatureValue;
	private String productFeatureLabelTwo;
	private String productFeatureValueTwo;
	private String productFeatureLabelThree;
	private String productFeatureValueThree;
	private String productFeatureLabelFour;
	private String productFeatureValueFour;
	private String productFeatureLabelFive;
	private String productFeatureValueFive;
	private String productFeatureLabelSix;
	private String productFeatureValueSix;
	private List<Pair<String, String>> productFeatures;
	private Boolean canProcure;
	private Long deviceCategory;
	private Integer equipmentType;
	private String productHierarchyCode;
	private String skuNumber;
	private String patent;
	private BigDecimal vendorUnitPrice;
	private BigDecimal vendorPrice;
	private String disclaimer;
	private String instructionsForUse;
	private String productWebsite;
	private String note;

	public Integer getExcelRowNum() { return excelRowNum; }

	public void setExcelRowNum(Integer excelRowNum) { this.excelRowNum = excelRowNum; }

	public String getCareArea() { return careArea; }

	public void setCareArea(String careArea) { this.careArea = careArea; }

	public String getCategory() { return category; }

	public void setCategory(String category) { this.category = category; }

	public String getBrand() { return brand; }

	public void setBrand(String brand) { this.brand = brand; }

	public String getProductNamePrimary() { return productNamePrimary; }

	public void setProductNamePrimary(String productNamePrimary) { this.productNamePrimary = productNamePrimary; }

	public String getProductNameSecondary() { return productNameSecondary; }

	public void setProductNameSecondary(String productNameSecondary) { this.productNameSecondary = productNameSecondary; }

	public String getProductCode() { return productCode; }

	public void setProductCode(String productCode) { this.productCode = productCode; }

	public String getProductModel() { return productModel; }

	public void setProductModel(String productModel) { this.productModel = productModel; }

	public String getProductRegNo() { return productRegNo; }

	public void setProductRegNo(String productRegNo) { this.productRegNo = productRegNo; }

	public String getProductDes() { return productDes; }

	public void setProductDes(String productDes) { this.productDes = productDes; }

	public String getHashTag() { return hashTag; }

	public void setHashTag(String hashTag) { this.hashTag = hashTag; }

	public String getUnit() { return unit; }

	public void setUnit(String unit) { this.unit = unit; }

	public BigDecimal getPrice() { return price; }

	public void setPrice(BigDecimal price) { this.price = price; }

	public String getPackaging() { return packaging; }

	public void setPackaging(String packaging) { this.packaging = packaging; }

	public Integer getMinOfOrder() { return minOfOrder; }

	public void setMinOfOrder(Integer minOfOrder) { this.minOfOrder = minOfOrder; }

	public String getVendorName() { return vendorName; }

	public void setVendorName(String vendorName) { this.vendorName = vendorName; }

	public String getBrochureAttachmentUrl() { return brochureAttachmentUrl; }

	public void setBrochureAttachmentUrl(String brochureAttachmentUrl) { this.brochureAttachmentUrl = brochureAttachmentUrl; }

	public String getVideoAttachmentUrl() { return videoAttachmentUrl; }

	public void setVideoAttachmentUrl(String videoAttachmentUrl) { this.videoAttachmentUrl = videoAttachmentUrl; }

	public List<String> getImagesUrl() { return imagesUrl; }

	public void setImagesUrl(List<String> imagesUrl) { this.imagesUrl = imagesUrl; }

	public List<Pair<String, String>> getProductTechnicals() { return productTechnicals; }

	public void setProductTechnicals(List<Pair<String, String>> productTechnicals) { this.productTechnicals = productTechnicals; }

	public List<Pair<String, String>> getProductFeatures() { return productFeatures; }

	public void setProductFeatures(List<Pair<String, String>> productFeatures) { this.productFeatures = productFeatures; }

	public Boolean getCanProcure() { return canProcure; }

	public void setCanProcure(Boolean canProcure) { this.canProcure = canProcure; }

	public Long getDeviceCategory() { return deviceCategory; }

	public void setDeviceCategory(Long deviceCategory) { this.deviceCategory = deviceCategory; }

	public Integer getEquipmentType() { return equipmentType; }

	public void setEquipmentType(Integer equipmentType) { this.equipmentType = equipmentType; }

	public String getProductHierarchyCode() { return productHierarchyCode; }

	public void setProductHierarchyCode(String productHierarchyCode) { this.productHierarchyCode = productHierarchyCode; }

	public String getSkuNumber() { return skuNumber; }

	public void setSkuNumber(String skuNumber) { this.skuNumber = skuNumber; }

	public String getPatent() { return patent; }

	public void setPatent(String patent) { this.patent = patent; }

	public BigDecimal getVendorUnitPrice() { return vendorUnitPrice; }

	public void setVendorUnitPrice(BigDecimal vendorUnitPrice) { this.vendorUnitPrice = vendorUnitPrice; }

	public BigDecimal getVendorPrice() { return vendorPrice; }

	public void setVendorPrice(BigDecimal vendorPrice) { this.vendorPrice = vendorPrice; }

	public String getDisclaimer() { return disclaimer; }

	public void setDisclaimer(String disclaimer) { this.disclaimer = disclaimer; }

	public String getInstructionsForUse() { return instructionsForUse; }

	public void setInstructionsForUse(String instructionsForUse) { this.instructionsForUse = instructionsForUse; }

	public String getProductWebsite() { return productWebsite; }

	public void setProductWebsite(String productWebsite) { this.productWebsite = productWebsite; }

	public String getNote() { return note; }

	public void setNote(String note) { this.note = note; }

	public String getProductTechnicalLabel() {
		return productTechnicalLabel;
	}

	public void setProductTechnicalLabel(String productTechnicalLabel) {
		this.productTechnicalLabel = productTechnicalLabel;
	}

	public String getProductTechnicalValue() {
		return productTechnicalValue;
	}

	public void setProductTechnicalValue(String productTechnicalValue) {
		this.productTechnicalValue = productTechnicalValue;
	}

	public String getProductTechnicalLabelTwo() { return productTechnicalLabelTwo; }

	public void setProductTechnicalLabelTwo(String productTechnicalLabelTwo) { this.productTechnicalLabelTwo = productTechnicalLabelTwo; }

	public String getProductTechnicalValueTwo() { return productTechnicalValueTwo; }

	public void setProductTechnicalValueTwo(String productTechnicalValueTwo) { this.productTechnicalValueTwo = productTechnicalValueTwo; }

	public String getProductTechnicalLabelThree() { return productTechnicalLabelThree; }

	public void setProductTechnicalLabelThree(String productTechnicalLabelThree) { this.productTechnicalLabelThree = productTechnicalLabelThree; }

	public String getProductTechnicalValueThree() { return productTechnicalValueThree; }

	public void setProductTechnicalValueThree(String productTechnicalValueThree) { this.productTechnicalValueThree = productTechnicalValueThree; }

	public String getProductTechnicalLabelFour() { return productTechnicalLabelFour; }

	public void setProductTechnicalLabelFour(String productTechnicalLabelFour) { this.productTechnicalLabelFour = productTechnicalLabelFour; }

	public String getProductTechnicalValueFour() { return productTechnicalValueFour; }

	public void setProductTechnicalValueFour(String productTechnicalValueFour) { this.productTechnicalValueFour = productTechnicalValueFour; }

	public String getProductTechnicalLabelFive() { return productTechnicalLabelFive; }

	public void setProductTechnicalLabelFive(String productTechnicalLabelFive) { this.productTechnicalLabelFive = productTechnicalLabelFive; }

	public String getProductTechnicalValueFive() { return productTechnicalValueFive; }

	public void setProductTechnicalValueFive(String productTechnicalValueFive) { this.productTechnicalValueFive = productTechnicalValueFive; }

	public String getProductTechnicalLabelSix() { return productTechnicalLabelSix; }

	public void setProductTechnicalLabelSix(String productTechnicalLabelSix) { this.productTechnicalLabelSix = productTechnicalLabelSix; }

	public String getProductTechnicalValueSix() { return productTechnicalValueSix; }

	public void setProductTechnicalValueSix(String productTechnicalValueSix) { this.productTechnicalValueSix = productTechnicalValueSix; }

	public String getProductFeatureLabel() { return productFeatureLabel; }

	public void setProductFeatureLabel(String productFeatureLabel) { this.productFeatureLabel = productFeatureLabel; }

	public String getProductFeatureValue() { return productFeatureValue; }

	public void setProductFeatureValue(String productFeatureValue) { this.productFeatureValue = productFeatureValue; }

	public String getProductFeatureLabelTwo() { return productFeatureLabelTwo; }

	public void setProductFeatureLabelTwo(String productFeatureLabelTwo) { this.productFeatureLabelTwo = productFeatureLabelTwo; }

	public String getProductFeatureValueTwo() { return productFeatureValueTwo; }

	public void setProductFeatureValueTwo(String productFeatureValueTwo) { this.productFeatureValueTwo = productFeatureValueTwo; }

	public String getProductFeatureLabelThree() { return productFeatureLabelThree; }

	public void setProductFeatureLabelThree(String productFeatureLabelThree) { this.productFeatureLabelThree = productFeatureLabelThree; }

	public String getProductFeatureValueThree() { return productFeatureValueThree; }

	public void setProductFeatureValueThree(String productFeatureValueThree) { this.productFeatureValueThree = productFeatureValueThree; }

	public String getProductFeatureLabelFour() { return productFeatureLabelFour; }

	public void setProductFeatureLabelFour(String productFeatureLabelFour) { this.productFeatureLabelFour = productFeatureLabelFour; }

	public String getProductFeatureValueFour() { return productFeatureValueFour; }

	public void setProductFeatureValueFour(String productFeatureValueFour) { this.productFeatureValueFour = productFeatureValueFour; }

	public String getProductFeatureLabelFive() { return productFeatureLabelFive; }

	public void setProductFeatureLabelFive(String productFeatureLabelFive) { this.productFeatureLabelFive = productFeatureLabelFive; }

	public String getProductFeatureValueFive() { return productFeatureValueFive; }

	public void setProductFeatureValueFive(String productFeatureValueFive) { this.productFeatureValueFive = productFeatureValueFive; }

	public String getProductFeatureLabelSix() { return productFeatureLabelSix; }

	public void setProductFeatureLabelSix(String productFeatureLabelSix) { this.productFeatureLabelSix = productFeatureLabelSix; }

	public String getProductFeatureValueSix() { return productFeatureValueSix; }

	public void setProductFeatureValueSix(String productFeatureValueSix) { this.productFeatureValueSix = productFeatureValueSix; }
}

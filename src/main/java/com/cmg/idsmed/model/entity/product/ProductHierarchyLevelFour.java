package com.cmg.idsmed.model.entity.product;

import com.cmg.idsmed.model.entity.AbstractEntity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
@Table(name = "zh_hans_l4_product_hierarchy")
public class ProductHierarchyLevelFour extends AbstractEntity {

    //We prefer sequence generator than identity. If you use identity it may disable JDBC batching
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "product_hierarchy_id_seq")
    @SequenceGenerator(name = "product_hierarchy_id_seq",
            sequenceName = "product_hierarchy_id_seq",
            allocationSize = 1)
    @Column(updatable = false)
    private Long id;

    //comment
    @ManyToOne
    @JoinColumn(name = "zh_hans_l3_product_hierarchy_id")
    private ProductHierarchyLevelThree productHierarchyLevelThree;

    @Column(name = "name")
    private String name;

    @Column(name = "code")
    private String code;

    @Column(name = "name_zh_cn")
    private String nameZhCn;

    @OneToMany(mappedBy = "productHierarchyLevelFour")
    private List<Product> productList;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

	public List<Product> getProductList() {
		return productList;
	}

	public void setProductList(List<Product> productList) {
		this.productList = productList;
	}

	public String getName() { return name; }

	public void setName(String name) { this.name = name; }

	public String getCode() { return code; }

	public void setCode(String code) { this.code = code; }

    public String getNameZhCn() { return nameZhCn; }

    public void setNameZhCn(String nameZhCn) { this.nameZhCn = nameZhCn; }

    public ProductHierarchyLevelThree getProductHierarchyLevelThree() {
        return productHierarchyLevelThree;
    }

    public void setProductHierarchyLevelThree(ProductHierarchyLevelThree productHierarchyLevelThree) {
        this.productHierarchyLevelThree = productHierarchyLevelThree;
    }
}

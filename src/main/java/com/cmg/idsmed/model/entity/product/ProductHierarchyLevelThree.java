package com.cmg.idsmed.model.entity.product;

import com.cmg.idsmed.model.entity.AbstractEntity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "zh_hans_l3_product_hierarchy")
public class ProductHierarchyLevelThree extends AbstractEntity {

    //We prefer sequence generator than identity. If you use identity it may disable JDBC batching
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "zh_hans_l3_product_hierarchy_id_seq")
    @SequenceGenerator(name = "zh_hans_l3_product_hierarchy_id_seq",
            sequenceName = "zh_hans_l3_product_hierarchy_id_seq",
            allocationSize = 1)
    @Column(updatable = false)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "zh_hans_l2_product_hierarchy_id")
    private ProductHierarchyLevelTwo productHierarchyLevelTwo;

    @Column(name = "name")
    private String name;

    @Column(name = "code")
    private String code;

    @Column(name = "name_zh_cn")
    private String nameZhCn;

    @OneToMany(mappedBy = "productHierarchyLevelThree")
    private List<ProductHierarchyLevelFour> productHierarchyLevelFourList;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

	public String getName() { return name; }

	public void setName(String name) { this.name = name; }

	public String getCode() { return code; }

	public void setCode(String code) { this.code = code; }

    public String getNameZhCn() { return nameZhCn; }

    public void setNameZhCn(String nameZhCn) { this.nameZhCn = nameZhCn; }

    public ProductHierarchyLevelTwo getProductHierarchyLevelTwo() {
        return productHierarchyLevelTwo;
    }

    public void setProductHierarchyLevelTwo(ProductHierarchyLevelTwo productHierarchyLevelTwo) {
        this.productHierarchyLevelTwo = productHierarchyLevelTwo;
    }

    public List<ProductHierarchyLevelFour> getProductHierarchyLevelFourList() {
        return productHierarchyLevelFourList;
    }

    public void setProductHierarchyLevelFourList(List<ProductHierarchyLevelFour> productHierarchyLevelFourList) {
        this.productHierarchyLevelFourList = productHierarchyLevelFourList;
    }
}

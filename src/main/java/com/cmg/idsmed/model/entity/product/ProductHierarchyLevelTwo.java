package com.cmg.idsmed.model.entity.product;

import com.cmg.idsmed.model.entity.AbstractEntity;
import com.cmg.idsmed.model.entity.product.Product;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "zh_hans_l2_product_hierarchy")
public class ProductHierarchyLevelTwo extends AbstractEntity {

    //We prefer sequence generator than identity. If you use identity it may disable JDBC batching
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "zh_hans_l2_product_hierarchy_id_seq")
    @SequenceGenerator(name = "zh_hans_l2_product_hierarchy_id_seq",
            sequenceName = "zh_hans_l2_product_hierarchy_id_seq",
            allocationSize = 1)
    @Column(updatable = false)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "zh_hans_l1_product_hierarchy_id")
    private ProductHierarchyLevelOne productHierarchyLevelOne;

    @Column(name = "name")
    private String name;

    @Column(name = "code")
    private String code;

    @Column(name = "name_zh_cn")
    private String nameZhCn;

    @OneToMany(mappedBy = "productHierarchyLevelTwo")
    private List<ProductHierarchyLevelThree> productHierarchyLevelThreeList;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

	public String getName() { return name; }

	public void setName(String name) { this.name = name; }

	public String getCode() { return code; }

	public void setCode(String code) { this.code = code; }

    public String getNameZhCn() { return nameZhCn; }

    public void setNameZhCn(String nameZhCn) { this.nameZhCn = nameZhCn; }

    public ProductHierarchyLevelOne getProductHierarchyLevelOne() {
        return productHierarchyLevelOne;
    }

    public void setProductHierarchyLevelOne(ProductHierarchyLevelOne productHierarchyLevelOne) {
        this.productHierarchyLevelOne = productHierarchyLevelOne;
    }

    public List<ProductHierarchyLevelThree> getProductHierarchyLevelThreeList() {
        return productHierarchyLevelThreeList;
    }

    public void setProductHierarchyLevelThreeList(List<ProductHierarchyLevelThree> productHierarchyLevelThreeList) {
        this.productHierarchyLevelThreeList = productHierarchyLevelThreeList;
    }
}

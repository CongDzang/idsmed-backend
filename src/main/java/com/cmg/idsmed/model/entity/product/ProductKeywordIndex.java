//package com.cmg.idsmed.model.entity.product;
//
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//import javax.persistence.JoinColumn;
//import javax.persistence.ManyToOne;
//import javax.persistence.SequenceGenerator;
//import javax.persistence.Table;
//import javax.validation.constraints.NotNull;
//import javax.validation.constraints.Size;
//
//import org.hibernate.validator.constraints.UniqueElements;
//
//import com.cmg.idsmed.model.entity.AbstractEntity;
//
//@Entity
//@Table(name = "product_keyword_index")
//public class ProductKeywordIndex extends AbstractEntity {
//
//    //We prefer sequence generator than identity. If you use identity it may disable JDBC batching
//    @Id
//    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "product_keyword_index_id_seq")
//    @SequenceGenerator(name = "product_keyword_index_id_seq",
//            sequenceName = "product_keyword_index_id_seq",
//            allocationSize = 1)
//    @Column(updatable = false)
//    private Long id;
//
//    @Column(name = "product_term")
//    @NotNull
//    @Size(max = 200)
//    private String productTerm;
//
//    @ManyToOne
//    @JoinColumn(name = "product_identity")
//    private Product product;
//
//    @Column(name = "country_code")
//    @Size(max = 10)
//    private String countryCode;
//
//    public Long getId() {
//        return id;
//    }
//
//    public void setId(Long id) {
//        this.id = id;
//    }
//
//    public String getProductTerm() {
//        return productTerm;
//    }
//
//    public void setProductTerm(String productTerm) {
//        this.productTerm = productTerm;
//    }
//
//    public Product getProduct() {
//        return product;
//    }
//
//    public void setProduct(Product product) {
//        this.product = product;
//    }
//
//    public String getCountryCode() {
//        return countryCode;
//    }
//
//    public void setCountryCode(String countryCode) {
//        this.countryCode = countryCode;
//    }
//}

package com.cmg.idsmed.model.entity.product;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.cmg.idsmed.model.entity.AbstractEntity;

@Entity
@Table(name = "product_media")
public class ProductMedia extends AbstractEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -226952265066780807L;

	public enum MEDIA_TYPE {
		IMAGE("img"), VIDEO("video"), BROCHURE("brochure"), CERTIFICATE("certificate");
		
		MEDIA_TYPE (String type){
			this.type = type;
		}
		
		private String type;
		
		public String getType() {
			return type;
		}
	}
	public ProductMedia() {
		super();
	}
	

	public ProductMedia(String documentName, String documentType, String documentPath, String documentUrl, String countryCode) {
		this.documentName = documentName;
		this.documentType = documentType;
		this.documentPath = documentPath;
		this.documentUrl = documentUrl;
		this.countryCode = countryCode;
	}
	
    //We prefer sequence generator than identity. If you use identity it may disable JDBC batching
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "product_media_id_seq")
    @SequenceGenerator(name = "product_media_id_seq",
            sequenceName = "product_media_id_seq",
            allocationSize = 1)
    @Column(updatable = false)
    private Long id;

	@ManyToOne
	@JoinColumn(name = "product_id")
	private Product product;

    @Column(name = "version")
    private Integer version = 1;

    @Column(name = "document_name")
    @Size(max = 100)
    private String documentName;

	/**
	 * Use MEDIA_TYPE Enum
	 * IMAGE("img"),
	 * VIDEO("video"),
	 * BROCHURE("brochure"),
	 * CERTIFICATE("certificate")
	 */
	@Column(name = "document_type")
    private String documentType;

    @Column(name = "document_path")
    @Size(max = 10000)
    private String documentPath;
    
    @Column(name = "document_seq")
    @NotNull
    private Integer documentSeq = 1;
    
    @Column(name = "remarks")
    @Size(max = 3000)
    private String remarks;

    @Column(name = "country_code")
    @Size(max = 10)
    private String countryCode;
    
    @Column(name = "document_url")
    @Size(max = 10000)
    private String documentUrl;
    
    @Column(name = "gid")
    @Size(max = 100)
    private String gid;

	@Column(name = "customer_url")
    @Size(max = 10000)
    private String customerUrl;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public String getDocumentName() {
		return documentName;
	}

	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}

	public String getDocumentType() {
		return documentType;
	}

	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}

	public String getDocumentPath() {
		return documentPath;
	}

	public void setDocumentPath(String documentPath) {
		this.documentPath = documentPath;
	}

	public int getDocumentSeq() {
		return documentSeq;
	}

	public void setDocumentSeq(int documentSeq) {
		this.documentSeq = documentSeq;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getDocumentUrl() {
		return documentUrl;
	}

	public void setDocumentUrl(String documentUrl) {
		this.documentUrl = documentUrl;
	}

	public String getGid() {
		return gid;
	}

	public void setGid(String gid) {
		this.gid = gid;
	}

	public String getCustomerUrl() { return customerUrl; }

	public void setCustomerUrl(String customerUrl) { this.customerUrl = customerUrl; }

	public void setVersion(Integer version) {
		this.version = version;
	}

	public void setDocumentSeq(Integer documentSeq) {
		this.documentSeq = documentSeq;
	}
}

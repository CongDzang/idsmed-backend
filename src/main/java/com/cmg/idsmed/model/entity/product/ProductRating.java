package com.cmg.idsmed.model.entity.product;

import com.cmg.idsmed.common.enums.CountryCodeEnum;
import com.cmg.idsmed.common.enums.StatusEnum;
import com.cmg.idsmed.dto.product.ProductUpload;
import com.cmg.idsmed.model.entity.AbstractEntity;
import com.cmg.idsmed.model.entity.product.list.BestSellerProductGlobal;
import com.cmg.idsmed.model.entity.product.list.HistoryUserProductBySearch;
import com.cmg.idsmed.model.entity.vendor.Vendor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "product_rating")
public class ProductRating extends AbstractEntity implements Serializable {

    /**
	 * Status of this entity use StatusEnum pls go to see in detail
	 */
	private static final long serialVersionUID = 5771796917019428678L;

	//We prefer sequence generator than identity. If you use identity it may disable JDBC batching
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "product_rating_id_seq")
    @SequenceGenerator(name = "product_rating_id_seq",
            sequenceName = "product_rating_id_seq",
            allocationSize = 1)
    @Column(updatable = false)
    private Long id;

	@OneToOne
	@JoinColumn(name = "product_id")
    private Product product;

    @Column(name = "average_score")
    private Double averageScore;

    @Column(name = "total_count_of_rating")
    private Long totalCountOfRating;

	@Column(name = "total_count_of_five_star")
	private Long totalCountOfFiveStar;

	@Column(name = "total_count_of_four_star")
	private Long totalCountOfFourStar;

	@Column(name = "total_count_of_three_star")
	private Long totalCountOfThreeStar;

	@Column(name = "total_count_of_two_star")
	private Long totalCountOfTwoStar;

	@Column(name = "total_count_of_one_star")
	private Long totalCountOfOneStar;

	@Column(name = "total_count_of_zero_star")
	private Long totalCountOfZeroStar;

	@Column(name = "total_of_score")
	private Double totalOfScore;

	@OneToMany(mappedBy = "productRating")
	private List<ProductRatingDetail> productRatingDetailList;

	public ProductRating(){};
	public ProductRating(Product product, Long currentAccountId){
		this.product = product;
		this.averageScore = 0.0;
		this.totalCountOfZeroStar = 0L;
		this.totalCountOfOneStar = 0L;
		this.totalCountOfTwoStar = 0L;
		this.totalCountOfThreeStar = 0L;
		this.totalCountOfFourStar = 0L;
		this.totalCountOfFiveStar = 0L;
		this.totalOfScore = 0.0;
		this.totalCountOfRating = 0L;
		this.setCreatedBy(currentAccountId);
		this.setUpdatedBy(currentAccountId);
		this.setStatus(StatusEnum.APPROVED.getCode());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Product getProduct() { return product; }

	public void setProduct(Product product) { this.product = product; }

	public Double getAverageScore() { return averageScore; }

	public void setAverageScore(Double averageScore) { this.averageScore = averageScore; }

	public Long getTotalCountOfRating() { return totalCountOfRating; }

	public void setTotalCountOfRating(Long totalCountOfRating) { this.totalCountOfRating = totalCountOfRating; }

	public Long getTotalCountOfFiveStar() { return totalCountOfFiveStar; }

	public void setTotalCountOfFiveStar(Long totalCountOfFiveStar) { this.totalCountOfFiveStar = totalCountOfFiveStar; }

	public Long getTotalCountOfFourStar() { return totalCountOfFourStar; }

	public void setTotalCountOfFourStar(Long totalCountOfFourStar) { this.totalCountOfFourStar = totalCountOfFourStar; }

	public Long getTotalCountOfThreeStar() { return totalCountOfThreeStar; }

	public void setTotalCountOfThreeStar(Long totalCountOfThreeStar) { this.totalCountOfThreeStar = totalCountOfThreeStar; }

	public Long getTotalCountOfTwoStar() { return totalCountOfTwoStar; }

	public void setTotalCountOfTwoStar(Long totalCountOfTwoStar) { this.totalCountOfTwoStar = totalCountOfTwoStar; }

	public Long getTotalCountOfOneStar() { return totalCountOfOneStar; }

	public void setTotalCountOfOneStar(Long totalCountOfOneStar) { this.totalCountOfOneStar = totalCountOfOneStar; }

	public List<ProductRatingDetail> getProductRatingDetailList() { return productRatingDetailList; }

	public void setProductRatingDetailList(List<ProductRatingDetail> productRatingDetailList) { this.productRatingDetailList = productRatingDetailList; }

	public Long getTotalCountOfZeroStar() { return totalCountOfZeroStar; }

	public void setTotalCountOfZeroStar(Long totalCountOfZeroStar) { this.totalCountOfZeroStar = totalCountOfZeroStar; }

	public Double getTotalOfScore() { return totalOfScore; }

	public void setTotalOfScore(Double totalOfScore) { this.totalOfScore = totalOfScore; }
}

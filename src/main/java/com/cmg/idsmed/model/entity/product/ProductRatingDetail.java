package com.cmg.idsmed.model.entity.product;

import com.cmg.idsmed.dto.product.ProductRatingRequest;
import com.cmg.idsmed.model.entity.AbstractEntity;
import com.cmg.idsmed.model.entity.auth.IdsmedAccount;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Entity
@Table(name = "product_rating_detail")
public class ProductRatingDetail extends AbstractEntity implements Serializable {

    /**
	 * Status of this entity use StatusEnum pls go to see in detail
	 */
	private static final long serialVersionUID = 5771796917019428678L;

	//We prefer sequence generator than identity. If you use identity it may disable JDBC batching
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "product_rating_detail_id_seq")
    @SequenceGenerator(name = "product_rating_detail_id_seq",
            sequenceName = "product_rating_detail_id_seq",
            allocationSize = 1)
    @Column(updatable = false)
    private Long id;

	@ManyToOne
	@JoinColumn(name = "product_id")
    private Product product;

	@ManyToOne
	@JoinColumn(name = "idsmed_account_id")
    private IdsmedAccount idsmedAccount;

	@ManyToOne
	@JoinColumn(name = "product_rating_id")
    private ProductRating productRating;

	@Column(name = "score")
	private Double score;

	@Column(name = "comment")
	@Size (max = 3000)
	private String comment;

	public ProductRatingDetail(){}

	public ProductRatingDetail(ProductRatingRequest productRatingRequest, IdsmedAccount idsmedAccount, Product product){
		this.score = productRatingRequest.getScore();
		this.comment = productRatingRequest.getComment();
		this.setCreatedBy(idsmedAccount.getId());
		this.setUpdatedBy(idsmedAccount.getId());
		this.product = product;
		this.idsmedAccount = idsmedAccount;
	}
	public Long getId() { return id; }

	public void setId(Long id) { this.id = id; }

	public IdsmedAccount getIdsmedAccount() { return idsmedAccount; }

	public void setIdsmedAccount(IdsmedAccount idsmedAccount) { this.idsmedAccount = idsmedAccount; }

	public ProductRating getProductRating() { return productRating; }

	public void setProductRating(ProductRating productRating) { this.productRating = productRating; }

	public Double getScore() { return score; }

	public void setScore(Double score) { this.score = score; }

	public String getComment() { return comment; }

	public void setComment(String comment) { this.comment = comment; }

	public Product getProduct() { return product; }

	public void setProduct(Product product) { this.product = product; }
}

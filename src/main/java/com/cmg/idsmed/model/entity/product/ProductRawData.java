package com.cmg.idsmed.model.entity.product;

import org.springframework.data.util.Pair;

import java.math.BigDecimal;
import java.util.List;

public class ProductRawData {
	private Integer csvRowNum;
	private String careArea;
	private String category;
	private String brand;
	private String productNamePrimary;
	private String productNameSecondary;
	private String productCode;
	private String productModel;
	private String productRegNo;
	private String productDes;
	private String hashTag;
	private String unit;
	private BigDecimal price;
	private String packaging;
	private Integer minOfOrder;
	private String vendorName;
	private String brochureAttachmentUrl;
	private String videoAttachmentUrl;
	private List<String> imagesUrl;
	private List<Pair<String, String>> productTechnicals;
	private List<Pair<String, String>> productFeatures;
	private Boolean canProcure;
	private Long deviceCategory;

	public String getCareArea() {
		return careArea;
	}

	public void setCareArea(String careArea) {
		this.careArea = careArea;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getProductNamePrimary() {
		return productNamePrimary;
	}

	public void setProductNamePrimary(String productNamePrimary) {
		this.productNamePrimary = productNamePrimary;
	}

	public String getProductNameSecondary() {
		return productNameSecondary;
	}

	public void setProductNameSecondary(String productNameSecondary) {
		this.productNameSecondary = productNameSecondary;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductModel() {
		return productModel;
	}

	public void setProductModel(String productModel) {
		this.productModel = productModel;
	}

	public String getProductRegNo() {
		return productRegNo;
	}

	public void setProductRegNo(String productRegNo) {
		this.productRegNo = productRegNo;
	}

	public String getProductDes() {
		return productDes;
	}

	public void setProductDes(String productDes) {
		this.productDes = productDes;
	}

	public String getHashTag() {
		return hashTag;
	}

	public void setHashTag(String hashTag) {
		this.hashTag = hashTag;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public String getPackaging() {
		return packaging;
	}

	public void setPackaging(String packaging) {
		this.packaging = packaging;
	}

	public Integer getMinOfOrder() {
		return minOfOrder;
	}

	public void setMinOfOrder(Integer minOfOrder) {
		this.minOfOrder = minOfOrder;
	}

	public String getVendorName() {
		return vendorName;
	}

	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

	public String getBrochureAttachmentUrl() {
		return brochureAttachmentUrl;
	}

	public void setBrochureAttachmentUrl(String brochureAttachmentUrl) {
		this.brochureAttachmentUrl = brochureAttachmentUrl;
	}

	public String getVideoAttachmentUrl() {
		return videoAttachmentUrl;
	}

	public void setVideoAttachmentUrl(String videoAttachmentUrl) {
		this.videoAttachmentUrl = videoAttachmentUrl;
	}

	public List<String> getImagesUrl() {
		return imagesUrl;
	}

	public void setImagesUrl(List<String> imagesUrl) {
		this.imagesUrl = imagesUrl;
	}

	public List<Pair<String, String>> getProductTechnicals() {
		return productTechnicals;
	}

	public void setProductTechnicals(List<Pair<String, String>> productTechnicals) {
		this.productTechnicals = productTechnicals;
	}

	public List<Pair<String, String>> getProductFeatures() {
		return productFeatures;
	}

	public void setProductFeatures(List<Pair<String, String>> productFeatures) {
		this.productFeatures = productFeatures;
	}

	public Integer getCsvRowNum() {
		return csvRowNum;
	}

	public void setCsvRowNum(Integer csvRowNum) {
		this.csvRowNum = csvRowNum;
	}

	public Boolean getCanProcure() { return canProcure; }

	public void setCanProcure(Boolean canProcure) { this.canProcure = canProcure; }

	public Long getDeviceCategory() { return deviceCategory; }

	public void setDeviceCategory(Long deviceCategory) { this.deviceCategory = deviceCategory; }
}

package com.cmg.idsmed.model.entity.product;

import com.cmg.idsmed.model.entity.AbstractEntity;

import javax.persistence.*;

@Entity
@Table(name  = "product_second_category_detail")
public class ProductSecondCategoryDetail extends AbstractEntity {

	//We prefer sequence generator than identity. If you use identity it may disable JDBC batching
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "product_second_category_detail_id_seq")
	@SequenceGenerator(name = "product_second_category_detail_id_seq",
			sequenceName = "product_second_category_detail_id_seq",
			allocationSize = 1)
	@Column(updatable = false)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "product_id", referencedColumnName = "id")
	private Product product;

	@ManyToOne
	@JoinColumn(name = "product_second_category_id", referencedColumnName = "id")
	private ProductSecondCategory productSecondCategory;

	public ProductSecondCategoryDetail() {

	}

	public ProductSecondCategoryDetail(ProductSecondCategory productSecondCategory, Product product) {
		if(productSecondCategory != null) {
			this.productSecondCategory = productSecondCategory;
		}
		if(product != null) {
			this.setProduct(product);
		}
	}
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public ProductSecondCategory getProductSecondCategory() {
		return productSecondCategory;
	}

	public void setProductSecondCategory(ProductSecondCategory productSecondCategory) {
		this.productSecondCategory = productSecondCategory;
	}
}

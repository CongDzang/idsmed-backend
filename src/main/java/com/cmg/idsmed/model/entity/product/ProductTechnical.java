package com.cmg.idsmed.model.entity.product;

import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.cmg.idsmed.model.entity.AbstractEntity;

@Entity
@Table(name = "product_technical")
public class ProductTechnical extends AbstractEntity {

	public ProductTechnical() {
		super();
	}
    //We prefer sequence generator than identity. If you use identity it may disable JDBC batching
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "product_technical_id_seq")
    @SequenceGenerator(name = "product_technical_id_seq",
            sequenceName = "product_technical_id_seq",
            allocationSize = 1)
    @Column(updatable = false)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "product_id")
    private Product product;

    @Column(name = "version")
    private Integer version = 1;

    @Column(name = "product_label")
    @NotNull
    @Size(max = 100)
    private String productLabel;

    @Column(name = "product_value")
    @NotNull
    @Size(max = 1000)
    private String productValue;

    @Column(name = "product_label_sequence")
    @NotNull
    private Integer productLabelSequence = 1;

    @Column(name = "country_code")
    @Size(max = 10)
    private String countryCode;

    @Column(name = "selling_point")
	private Boolean sellingPoint;

	public ProductTechnical(Product product,
							String productLabel,
							String productValue,
							Integer productLabelSequence,
							String countryCode,
							Integer version,
							Boolean sellingPoint) {
		this.product = product;
		this.productLabel = productLabel;
		this.productValue = productValue;
		this.productLabelSequence = productLabelSequence;
		this.countryCode = countryCode;
		this.version = version;
		this.sellingPoint = sellingPoint;
	}

	public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public String getProductLabel() {
		return productLabel;
	}

	public void setProductLabel(String productLabel) {
		this.productLabel = productLabel;
	}

	public String getProductValue() {
		return productValue;
	}

	public void setProductValue(String productValue) {
		this.productValue = productValue;
	}

	public int getProductLabelSequence() {
		return productLabelSequence;
	}

	public void setProductLabelSequence(int productLabelSequence) {
		this.productLabelSequence = productLabelSequence;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public Boolean getSellingPoint() { return sellingPoint; }

	public void setSellingPoint(Boolean sellingPoint) { this.sellingPoint = sellingPoint; }
}

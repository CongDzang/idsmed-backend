package com.cmg.idsmed.model.entity.product;

import com.cmg.idsmed.model.entity.AbstractEntity;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Entity
@Table(name = "product_tech_feature")
public class ProductTechnicalFeatureLabel extends AbstractEntity {

    public enum LabelTypeEnum {
        PRODUCT_FEATURE(1, "Product Feature"),
        PRODUCT_TECHNICAL(2, "Product Technical");

        private Integer code;
        private String name;

        LabelTypeEnum(Integer code, String name) {
            this.code = code;
            this.name = name;
        }

        public Integer getCode() {
            return code;
        }

        public void setCode(Integer code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "product_tech_feature_id_seq")
    @SequenceGenerator(name = "product_tech_feature_id_seq",
            sequenceName = "product_tech_feature_id_seq",
            allocationSize = 1)

    @Column(updatable = false)
    private Long id;

    @Column(name = "label_name")
    @Size(max = 100)
    private String labelName;

    @Column(name = "label_name_zh_cn")
    @Size(max = 100)
    private String labelNameZhCn;

    @Column(name = "label_name_zh_tw")
    @Size(max = 100)
    private String labelNameZhTw;

    @Column(name = "label_name_vi")
    @Size(max = 100)
    private String labelNameVi;

    @Column(name = "label_name_th")
    @Size(max = 100)
    private String labelNameTh;

    @Column(name = "label_name_id")
    @Size(max = 100)
    private String labelNameId;

    @Column(name = "label_type")
    private Integer labelType;

    @Column(name = "for_medical_consumables")
    private Integer forMedicalConsumables;

    public ProductTechnicalFeatureLabel() {
    }

    public ProductTechnicalFeatureLabel(String labelName, String labelNameZhCn, String labelNameZhTw,
                                        String labelNameVi, String labelNameTh, String labelNameId,
                                        Integer labelType, Integer forMedicalConsumables) {
        this.labelName = labelName;
        this.labelNameZhCn = labelNameZhCn;
        this.labelNameZhTw = labelNameZhTw;
        this.labelNameVi = labelNameVi;
        this.labelNameTh = labelNameTh;
        this.labelNameId = labelNameId;
        this.labelType = labelType;
        this.forMedicalConsumables = forMedicalConsumables;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLabelName() {
        return labelName;
    }

    public void setLabelName(String labelName) {
        this.labelName = labelName;
    }

    public String getLabelNameZhCn() {
        return labelNameZhCn;
    }

    public void setLabelNameZhCn(String labelNameZhCn) {
        this.labelNameZhCn = labelNameZhCn;
    }

    public String getLabelNameZhTw() {
        return labelNameZhTw;
    }

    public void setLabelNameZhTw(String labelNameZhTw) {
        this.labelNameZhTw = labelNameZhTw;
    }

    public String getLabelNameVi() {
        return labelNameVi;
    }

    public void setLabelNameVi(String labelNameVi) {
        this.labelNameVi = labelNameVi;
    }

    public String getLabelNameTh() {
        return labelNameTh;
    }

    public void setLabelNameTh(String labelNameTh) {
        this.labelNameTh = labelNameTh;
    }

    public String getLabelNameId() {
        return labelNameId;
    }

    public void setLabelNameId(String labelNameId) {
        this.labelNameId = labelNameId;
    }

    public Integer getLabelType() {
        return labelType;
    }

    public void setLabelType(Integer labelType) {
        this.labelType = labelType;
    }

    public Integer getForMedicalConsumables() {
        return forMedicalConsumables;
    }

    public void setForMedicalConsumables(Integer forMedicalConsumables) {
        this.forMedicalConsumables = forMedicalConsumables;
    }

}

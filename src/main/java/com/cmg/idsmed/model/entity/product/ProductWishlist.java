package com.cmg.idsmed.model.entity.product;

import com.cmg.idsmed.model.entity.AbstractEntity;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "product_wishlist")
public class ProductWishlist extends AbstractEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "product_wishlist_id_seq")
    @SequenceGenerator(name = "product_wishlist_id_seq",
            sequenceName = "product_wishlist_id_seq",
            allocationSize = 1)

    @Column(updatable = false)
    private Long id;

    @OneToOne
    @JoinColumn(name = "product_id")
    private Product product;

    @Column(name = "total_wishlist")
    private Long totalWishlist;

    @OneToMany(mappedBy = "productWishlist")
    private List<ProductWishlistDetail> productWishlistDetailList;

    public ProductWishlist() { }

    public ProductWishlist(Product product, Long totalWishlist, Long loginId) {
        this.product = product;
        this.totalWishlist = totalWishlist;
        this.setCreatedBy(loginId);
        this.setUpdatedBy(loginId);
    }

    public ProductWishlist(Product product, Long totalWishlist, Long wedoctorCustomerId, Boolean isWedoctor) {
        this.product = product;
        this.totalWishlist = totalWishlist;
        this.setCreatedBy(wedoctorCustomerId);
        this.setUpdatedBy(wedoctorCustomerId);
    }

    public Long getId() { return id; }

    public void setId(Long id) { this.id = id; }

    public Product getProduct() { return product; }

    public void setProduct(Product product) { this.product = product; }

    public Long getTotalWishlist() { return totalWishlist; }

    public void setTotalWishlist(Long totalWishlist) { this.totalWishlist = totalWishlist; }

    public List<ProductWishlistDetail> getProductWishlistDetailList() { return productWishlistDetailList; }

    public void setProductWishlistDetailList(List<ProductWishlistDetail> productWishlistDetailList) {
        this.productWishlistDetailList = productWishlistDetailList;
    }

}

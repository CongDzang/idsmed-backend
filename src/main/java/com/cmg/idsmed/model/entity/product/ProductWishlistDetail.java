package com.cmg.idsmed.model.entity.product;

import com.cmg.idsmed.model.entity.AbstractEntity;
import com.cmg.idsmed.model.entity.auth.IdsmedAccount;

import javax.persistence.*;

@Entity
@Table(name = "product_wishlist_detail")
public class ProductWishlistDetail extends AbstractEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "product_wishlist_detail_id_seq")
    @SequenceGenerator(name = "product_wishlist_detail_id_seq",
            sequenceName = "product_wishlist_detail_id_seq",
            allocationSize = 1)

    @Column(updatable = false)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "idsmed_account_id")
    private IdsmedAccount idsmedAccount;

    @Column(name = "wedoctor_customer_id")
    private Long wedoctorCustomerId;

    @ManyToOne
    @JoinColumn(name = "product_id")
    private Product product;

    @ManyToOne
    @JoinColumn(name = "product_wishlist_id")
    private ProductWishlist productWishlist;

    public ProductWishlistDetail() {
    }

    public ProductWishlistDetail(Long wedoctorCustomerId, Product product, ProductWishlist productWishlist) {
        this.wedoctorCustomerId = wedoctorCustomerId;
        this.product = product;
        this.productWishlist = productWishlist;
        this.setCreatedBy(wedoctorCustomerId);
    }

    public ProductWishlistDetail(IdsmedAccount idsmedAccount, Product product, ProductWishlist productWishlist) {
        this.idsmedAccount = idsmedAccount;
        this.product = product;
        this.productWishlist = productWishlist;
        this.setCreatedBy(idsmedAccount.getId());
    }

    public Long getId() { return id; }

    public void setId(Long id) { this.id = id; }

    public IdsmedAccount getIdsmedAccount() { return idsmedAccount; }

    public void setIdsmedAccount(IdsmedAccount idsmedAccount) { this.idsmedAccount = idsmedAccount; }

    public Product getProduct() { return product; }

    public void setProduct(Product product) { this.product = product; }

    public ProductWishlist getProductWishlist() {
        return productWishlist;
    }

    public void setProductWishlist(ProductWishlist productWishlist) { this.productWishlist = productWishlist; }

    public Long getWedoctorCustomerId() {
        return wedoctorCustomerId;
    }

    public void setWedoctorCustomerId(Long wedoctorCustomerId) {
        this.wedoctorCustomerId = wedoctorCustomerId;
    }

}

package com.cmg.idsmed.model.entity.product.config;

import com.cmg.idsmed.model.entity.AbstractEntity;
import com.cmg.idsmed.model.entity.masterdata.Country;
import com.cmg.idsmed.model.entity.masterdata.Province;
import org.springframework.beans.BeanUtils;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "product_info_config")
public class ProductInfoConfig extends AbstractEntity {
    public enum ProductConfigStatusEnum{
        ACTIVE(1, "active"),

        INACTIVE(10, "inactive");

        ProductConfigStatusEnum(Integer code, String name){
            this.code = code;
            this.name = name;
        }
        private Integer code;
        private String name;

        public Integer getCode() {
            return code;
        }

        public String getName() {
            return name;
        }
    }
    private static final long serialVersionUID = 5771796917019428678L;
    //We prefer sequence generator than identity. If you use identity it may disable JDBC batching
    @Id
    @GeneratedValue(strategy= GenerationType.SEQUENCE, generator = "product_info_config_id_seq")
    @SequenceGenerator(name = "product_info_config_id_seq",
            sequenceName = "product_info_config_id_seq",
            allocationSize = 1)
    @Column(updatable = false)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "country_id")
    private Country country;

    @ManyToOne
    @JoinColumn(name = "province_id")
    private Province province;

    @OneToMany(mappedBy = "productInfoConfig")
    private List<ProductLabelConfig> productLabelConfigs;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public Province getProvince() {
        return province;
    }

    public void setProvince(Province province) {
        this.province = province;
    }

    public List<ProductLabelConfig> getProductLabelConfigs() {
        return productLabelConfigs;
    }

    public void setProductLabelConfigs(List<ProductLabelConfig> productLabelConfigs) {
        this.productLabelConfigs = productLabelConfigs;
    }
}

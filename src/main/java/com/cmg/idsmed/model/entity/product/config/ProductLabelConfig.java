package com.cmg.idsmed.model.entity.product.config;

import com.cmg.idsmed.dto.product.config.ProductLabelConfigurationRequest;
import com.cmg.idsmed.model.entity.AbstractEntity;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Entity
@Table(name = "product_label_config")
/*
    Please refer to ProductConfigStatusEnum for detail of status
 */
public class ProductLabelConfig extends AbstractEntity {
    private static final long serialVersionUID = 5771796917019428678L;
    //We prefer sequence generator than identity. If you use identity it may disable JDBC batching
    @Id
    @GeneratedValue(strategy= GenerationType.SEQUENCE, generator = "product_label_config_id_seq")
    @SequenceGenerator(name = "product_label_config_id_seq",
            sequenceName = "product_label_config_id_seq",
            allocationSize = 1)
    @Column(updatable = false)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "product_info_config_id")
    private ProductInfoConfig productInfoConfig;

    @Column(name = "label_primary_name")
    @Size(max = 500)
    private String labelPrimaryName;

    @Column(name = "label_secondary_name")
    @Size(max = 500)
    private String labelSecondaryName;

    @Column(name = "is_mandatory")
    private Boolean isMandatory = false;

    @Column(name = "is_expired")
    private Boolean isExpired = false;

    @Column(name = "type")
    private Integer type;

    @Column(name = "sequence")
    private Integer sequence;

    public ProductLabelConfig(){ }
    public ProductLabelConfig(ProductLabelConfigurationRequest request){
        this.labelPrimaryName = request.getLabelPrimaryName();
        this.labelSecondaryName = request.getLabelSecondaryName();
        this.isMandatory = request.getMandatory();
        this.isExpired = request.getExpired();
        this.sequence = request.getSequence();
    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ProductInfoConfig getProductInfoConfig() {
        return productInfoConfig;
    }

    public void setProductInfoConfig(ProductInfoConfig productInfoConfig) {
        this.productInfoConfig = productInfoConfig;
    }

    public String getLabelPrimaryName() {
        return labelPrimaryName;
    }

    public void setLabelPrimaryName(String labelPrimaryName) {
        this.labelPrimaryName = labelPrimaryName;
    }

    public String getLabelSecondaryName() {
        return labelSecondaryName;
    }

    public void setLabelSecondaryName(String labelSecondaryName) {
        this.labelSecondaryName = labelSecondaryName;
    }

    public Boolean getIsMandatory() {
        return isMandatory;
    }

    public void setIsMandatory(Boolean isMandatory) {
        this.isMandatory = isMandatory;
    }

    public Boolean getIsExpired() {
        return isExpired;
    }

    public void setIsExpired(Boolean isExpired) {
        this.isExpired = isExpired;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getSequence() {
        return sequence;
    }

    public void setSequence(Integer sequence) {
        this.sequence = sequence;
    }


}

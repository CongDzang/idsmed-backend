package com.cmg.idsmed.model.entity.product.details;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import com.cmg.idsmed.model.entity.AbstractEntity;

@Entity
@Table(name = "product_selling_history")
public class ProductSellingHistory extends AbstractEntity implements Serializable {
	private static final long serialVersionUID = 5771796917019428678L;

	//We prefer sequence generator than identity. If you use identity it may disable JDBC batching
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "product_selling_history_id_seq")
    @SequenceGenerator(name = "product_selling_history_id_seq",
            sequenceName = "product_selling_history_id_seq",
            allocationSize = 1)
    @Column(updatable = false)
    private Long id;

    //SCP user_id who bought this product
    @Column(name = "user_id")
    private Long userId;

    @Column(name = "wedoctor_customer_id")
	private Long wedoctorCustomerId;
    
    @Column(name = "product_id")
    private Long productId;
    
    @Column(name = "related_product_id")
    private Long relatedProductId;
    
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "MM/dd/yyyy")
    @Column(name="purchase_date")
    private Date purchaseDate;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public Long getRelatedProductId() {
		return relatedProductId;
	}

	public void setRelatedProductId(Long relatedProductId) {
		this.relatedProductId = relatedProductId;
	}

	public Date getPurchaseDate() {
		return purchaseDate;
	}

	public void setPurchaseDate(Date purchaseDate) {
		this.purchaseDate = purchaseDate;
	}

	public Long getWedoctorCustomerId() {
		return wedoctorCustomerId;
	}

	public void setWedoctorCustomerId(Long wedoctorCustomerId) {
		this.wedoctorCustomerId = wedoctorCustomerId;
	}
}

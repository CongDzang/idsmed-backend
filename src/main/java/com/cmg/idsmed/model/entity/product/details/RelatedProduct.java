package com.cmg.idsmed.model.entity.product.details;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.cmg.idsmed.model.entity.AbstractEntity;

@Entity
@Table(name = "product_related")
public class RelatedProduct extends AbstractEntity implements Serializable {

	private static final long serialVersionUID = 5771796917019428678L;

	//We prefer sequence generator than identity. If you use identity it may disable JDBC batching
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "product_related_id_seq")
    @SequenceGenerator(name = "product_related_id_seq",
            sequenceName = "product_related_id_seq",
            allocationSize = 1)
    @Column(updatable = false)
    private Long id;

    @Column(name = "product_id")
    private Long productId;
    
    @Column(name = "related_product_id")
    private Long relatedProductId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public Long getRelatedProductId() {
		return relatedProductId;
	}

	public void setRelatedProductId(Long relatedProductId) {
		this.relatedProductId = relatedProductId;
	}
}

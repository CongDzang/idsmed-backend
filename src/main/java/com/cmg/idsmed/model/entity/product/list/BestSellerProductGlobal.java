package com.cmg.idsmed.model.entity.product.list;

import java.io.Serializable;

import javax.persistence.*;

import com.cmg.idsmed.model.entity.AbstractEntity;
import com.cmg.idsmed.model.entity.product.Product;

@Entity
@Table(name = "best_seller_product_global")
public class BestSellerProductGlobal extends AbstractEntity implements Serializable {

	private static final long serialVersionUID = 5771796917019428678L;

	//We prefer sequence generator than identity. If you use identity it may disable JDBC batching
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "best_seller_product_global_id_seq")
    @SequenceGenerator(name = "best_seller_product_global_id_seq",
            sequenceName = "best_seller_product_global_id_seq",
            allocationSize = 1)
    @Column(updatable = false)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "product_id")
    private Product product;
    
    @Column(name = "frequency")
    private Integer frequency;
    
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Integer getFrequency() {
		return frequency;
	}

	public void setFrequency(Integer frequency) {
		this.frequency = frequency;
	}
	
}

package com.cmg.idsmed.model.entity.product.list;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;
import javax.validation.constraints.Size;

import com.cmg.idsmed.model.entity.AbstractEntity;
import com.cmg.idsmed.model.entity.product.Product;

@Entity
@Table(name = "history_user_product_by_search")
public class HistoryUserProductBySearch extends AbstractEntity implements Serializable {

	private static final long serialVersionUID = 5771796917019428678L;

	//We prefer sequence generator than identity. If you use identity it may disable JDBC batching
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "history_user_product_by_search_id_seq")
    @SequenceGenerator(name = "history_user_product_by_search_id_seq",
            sequenceName = "history_user_product_by_search_id_seq",
            allocationSize = 1)
    @Column(updatable = false)
    private Long id;

    @Column(name = "user_id")
    private Long userId;

    @Column(name = "third_party_id")
    @Size(max = 100)
    private String thirdPartyId;

    // Third party customer id (wedoctor's customer id)
    @Column(name = "customer_id")
	private Long customerId;

    @ManyToOne
    @JoinColumn(name = "product_id")
    private Product product;
    
    @Column(name = "frequency")
    private Integer frequency;

	public HistoryUserProductBySearch() {
	}

	public HistoryUserProductBySearch(Long userId, @Size(max = 100) String thirdPartyId, Long customerId, Product product, Integer frequency) {
		this.userId = userId;
		this.thirdPartyId = thirdPartyId;
		this.customerId = customerId;
		this.product = product;
		this.frequency = frequency;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Integer getFrequency() {
		return frequency;
	}

	public void setFrequency(Integer frequency) {
		this.frequency = frequency;
	}

	public String getThirdPartyId() {
		return thirdPartyId;
	}

	public void setThirdPartyId(String thirdPartyId) {
		this.thirdPartyId = thirdPartyId;
	}
}

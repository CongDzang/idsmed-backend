package com.cmg.idsmed.model.entity.product.log;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.cmg.idsmed.model.entity.AbstractEntity;
import org.springframework.util.CollectionUtils;

import java.util.Arrays;
import java.util.List;

/**
 * For the status of this entity.
 * we use ProductCSVStatusEnum.
 */
@Entity
@Table(name = "product_csv_log")
public class ProductCSVLog extends AbstractEntity {

	private static final long serialVersionUID = 5195420515705142490L;

	//We prefer sequence generator than identity. If you use identity it may disable JDBC batching
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "product_csv_log_id_seq")
    @SequenceGenerator(name = "product_csv_log_id_seq",
            sequenceName = "product_csv_log_id_seq",
            allocationSize = 1)
    @Column(updatable = false)
    private Long id;
    
    @Column(name="uuid")
    @NotNull
    @Size(max = 500)
    private String uuid;

    @Column(name="original_file_name")
    @Size(max = 1000)
    private String originalFileName;

    @Column(name="file_url")
	private String fileUrl;
    
    @Column(name="error_description")
    private String errorDescription;

    @Column(name = "error_row_info")
	private String errorRowInfo;

    @Column(name = "product_created_info")
	private String productCreatedInfo;

    @Column(name = "total_product_created")
	private Integer totalProductCreated;

    @Column(name = "vendor_id")
	private Long vendorId;

	@Column(name = "vendor_email")
	private String vendorEmail;

	@Column(name = "company_code")
	private String companyCode;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getOriginalFileName() {
		return originalFileName;
	}

	public void setOriginalFileName(String originalFileName) {
		this.originalFileName = originalFileName;
	}

	public String getFileUrl() {
		return fileUrl;
	}

	public void setFileUrl(String fileUrl) {
		this.fileUrl = fileUrl;
	}

	public String getErrorDescription() {
		return errorDescription;
	}

	public void setErrorDescription(String errorDescription) {
		this.errorDescription = errorDescription;
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public String getErrorRowInfo() {
		return errorRowInfo;
	}

	public void setErrorRowInfo(String errorRowInfo) {
		this.errorRowInfo = errorRowInfo;
	}

	public String getProductCreatedInfo() {
		return productCreatedInfo;
	}

	public void setProductCreatedInfo(String productCreatedInfo) {
		this.productCreatedInfo = productCreatedInfo;
	}

	public void setProductCreatedInfoByList(List<Long> createdProductIds) {
		if (!CollectionUtils.isEmpty(createdProductIds)) {
			Object [] ids = createdProductIds.toArray();
			String productCreatedInfo = Arrays.toString(ids);
			this.productCreatedInfo = productCreatedInfo;
		}
	}

	public Integer getTotalProductCreated() {
		return totalProductCreated;
	}

	public void setTotalProductCreated(Integer totalProductCreated) {
		this.totalProductCreated = totalProductCreated;
	}

	public Long getVendorId() {
		return vendorId;
	}

	public void setVendorId(Long vendorId) {
		this.vendorId = vendorId;
	}

	public String getVendorEmail() {
		return vendorEmail;
	}

	public void setVendorEmail(String vendorEmail) {
		this.vendorEmail = vendorEmail;
	}

	public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}
}


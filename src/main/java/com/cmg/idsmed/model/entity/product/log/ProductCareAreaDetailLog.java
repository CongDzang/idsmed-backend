package com.cmg.idsmed.model.entity.product.log;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.cmg.idsmed.model.entity.AbstractEntity;
import com.cmg.idsmed.model.entity.product.ProductCareAreaDetail;

@Entity
@Table(name = "product_care_area_detail_log")
public class ProductCareAreaDetailLog extends AbstractEntity {

    /**
	 * 
	 */
	private static final long serialVersionUID = 5195420515705142490L;

	//We prefer sequence generator than identity. If you use identity it may disable JDBC batching
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "product_care_area_detail_log_id_seq")
    @SequenceGenerator(name = "product_care_area_detail_log_id_seq",
            sequenceName = "product_care_area_detail_log_id_seq",
            allocationSize = 1)
    @Column(updatable = false)
    private Long id;
    
    @Column(name="country_code")
    @Size(max = 10)
    private String countryCode;

    @Column(name="product_id")
    @NotNull
    private Long productId;

    @Column(name="product_care_area_id")
    @NotNull
	private Long productCareAreaId;

    @Column(name="version")
	private Integer version;

    public ProductCareAreaDetailLog() { }

    public ProductCareAreaDetailLog(ProductCareAreaDetail productCareAreaDetail){
		this.countryCode = productCareAreaDetail.getCountryCode();

		if(productCareAreaDetail.getProduct() != null){
			this.productId = productCareAreaDetail.getProduct().getId();
			this.version = productCareAreaDetail.getProduct().getProductVersion();
		}

		if(productCareAreaDetail.getProductCareArea() != null){
			this.productCareAreaId = productCareAreaDetail.getProductCareArea().getId();
		}
	}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public Long getProductCareAreaId() {
		return productCareAreaId;
	}

	public void setProductCareAreaId(Long productCareAreaId) {
		this.productCareAreaId = productCareAreaId;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}
}


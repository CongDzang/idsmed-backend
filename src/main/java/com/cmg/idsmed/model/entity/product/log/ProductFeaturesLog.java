package com.cmg.idsmed.model.entity.product.log;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.cmg.idsmed.model.entity.AbstractEntity;
import com.cmg.idsmed.model.entity.product.ProductFeatures;

@Entity
@Table(name = "product_features_log")
public class ProductFeaturesLog extends AbstractEntity {

    //We prefer sequence generator than identity. If you use identity it may disable JDBC batching
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "product_features_log_id_seq")
    @SequenceGenerator(name = "product_features_log_id_seq",
            sequenceName = "product_features_log_id_seq",
            allocationSize = 1)
    @Column(updatable = false)
    private Long id;

    @Column(name = "product_id")
    private Long productId;

    @Column(name = "version")
    private int version;

    @Column(name = "product_label")
    @Size(max = 100)
    private String productLabel;

    @Column(name = "product_value")
    @Size(max = 1000)
    private String productValue;

    @Column(name = "product_label_sequence")
    private int productLabelSequence;

    @Column(name = "country_code")
    @Size(max = 10)
    private String countryCode;

    public ProductFeaturesLog(ProductFeatures productFeatures){
    	if(productFeatures.getProduct() != null){
    		this.productId = productFeatures.getProduct().getId();
    		this.version = productFeatures.getProduct().getProductVersion();
		}
		this.productLabel = productFeatures.getProductLabel();
    	this.productValue = productFeatures.getProductValue();
    	this.productLabelSequence = productFeatures.getProductLabelSequence();
    	this.countryCode = productFeatures.getCountryCode();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public String getProductLabel() {
		return productLabel;
	}

	public void setProductLabel(String productLabel) {
		this.productLabel = productLabel;
	}

	public String getProductValue() {
		return productValue;
	}

	public void setProductValue(String productValue) {
		this.productValue = productValue;
	}

	public int getProductLabelSequence() {
		return productLabelSequence;
	}

	public void setProductLabelSequence(int productLabelSequence) {
		this.productLabelSequence = productLabelSequence;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
}

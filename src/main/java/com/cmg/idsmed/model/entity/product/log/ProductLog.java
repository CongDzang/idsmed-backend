package com.cmg.idsmed.model.entity.product.log;

import java.math.BigDecimal;
import java.sql.Date;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.cmg.idsmed.model.entity.AbstractEntity;
import com.cmg.idsmed.model.entity.product.Product;
import com.cmg.idsmed.model.entity.vendor.Vendor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.util.CollectionUtils;

@Entity
@Table(name = "product_log")
public class ProductLog extends AbstractEntity {

    //We prefer sequence generator than identity. If you use identity it may disable JDBC batching
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "product_log_id_seq")
    @SequenceGenerator(name = "product_log_id_seq",
            sequenceName = "product_log_id_seq",
            allocationSize = 1)
    @Column(updatable = false)
    private Long id;

    @Column(name = "product_id")
    private Long productId;

    @Column(name = "product_version")
    private int productVersion;

    @Column(name = "product_identity")
    @Size(max = 500)
    private String productIdentity;

	@Column(name = "product_name_primary")
	@Size(max = 1000)
	private String productNamePrimary;

	@Column(name = "product_name_secondary")
	@Size(max = 1000)
	private String productNameSecondary;

	@Column(name = "product_code")
	@Size(max = 100)
	private String productCode;

	@Column(name = "product_model")
	@Size(max = 100)
	private String productModel;

	@Column(name = "product_description")
	@Size(max = 10000)
	private String productDescription;

	@Column(name = "registration_number")
	@Size(max = 500)
	private String registrationNumber;

	@Column(name = "hash_tag")
	@Size(max = 5000)
	private String hashTag;

	@DateTimeFormat(pattern = "MM/dd/yyyy HH:mm:ss.SSS")
	@Column(name = "exp_date")
	private LocalDateTime expDate;

	@Column(name = "packaging")
	@Size(max = 200)
	private String packaging;

	@Column(name = "min_of_order")
	private int minOfOrder;

	@Column(name = "verified_by")
	private Long verifiedBy;

	@Column(name = "approved_by")
	private Long approvedBy;

//	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "MM/dd/yyyy HH:mm:ss.SSS")
	@Column(name = "approved_datetime")
	private LocalDateTime approvedDate;

	@Column(name = "remarks")
	@Size(max = 3000)
	private String remarks;

	@Column(name = "country_code")
	@Size(max = 10)
	private String countryCode;

	@Column(name = "price", precision = 18, scale = 2)
	private BigDecimal price;

	@Column(name = "area")
	@Size(max = 1000)
	private String area;

	@Column(name = "product_brand_id")
	private Long productBrandId;

	@Column(name = "other_description")
	@Size(max = 1000)
	private String otherDescription;

	@Column(name = "previous_status")
	private Integer previousStatus;

	@Column(name="can_procure")
	private Boolean canProcure = false;

	@Column(name="device_category")
	private Long deviceCategory = 1L;
    
    @Column(name = "product_category_id")
    private Long productCategoryId;

	@Column(name = "vendor_id")
	private Long vendorId;

    @Column(name = "unit")
    @Size(max = 100)
    private String unit;

    @Column(name = "document_attachment_id")
	private String documentAttachmentId;

	@Column(name = "delivery_lead_time")
	@Size(max = 50)
	private String deliveryLeadTime;

	public ProductLog() {
	}

	public ProductLog(Product product) {
		this.productId = product.getId();
		this.productVersion = product.getProductVersion();
		this.productIdentity = product.getProductIdentity();
		this.productNamePrimary = product.getProductNamePrimary();
		this.productNameSecondary = product.getProductNameSecondary();
		this.productCode = product.getProductCode();
		this.productModel = product.getProductModel();
		this.productDescription = product.getProductDescription();
		this.registrationNumber = product.getRegistrationNumber();
		this.hashTag = product.getHashTag();
		this.expDate = product.getExpDate();
		this.packaging = product.getPackaging();
		this.minOfOrder = product.getMinOfOrder();
		this.verifiedBy = product.getVerifiedBy();
		this.approvedBy = product.getApprovedBy();
		this.approvedDate = product.getApprovedDate();
		this.remarks = product.getRemarks();
		this.countryCode = product.getCountryCode();
		this.price = product.getPrice();
		this.area = product.getArea();
		if(product.getProductBrand() != null){
			this.productBrandId = product.getProductBrand().getId();
		}
		this.otherDescription = product.getOtherDescription();
		this.previousStatus = product.getPreviousStatus();
		this.canProcure = product.isCanProcure();
		this.deviceCategory = product.getDeviceCategory();
		if(product.getProductCategory() != null){
			this.productCategoryId = product.getProductCategory().getId();
		}
		if(product.getVendor() != null){
			this.vendorId = product.getVendor().getId();
		}
		this.unit = product.getUnit();
		if(!CollectionUtils.isEmpty(product.getProductDocumentAttachmentList())) {
			List<Long> productDocumentAttachmentIds = product.getProductDocumentAttachmentList().stream().map(pdal -> pdal.getId()).collect(Collectors.toList());
			this.documentAttachmentId = productDocumentAttachmentIds.stream().map(pdaid -> String.valueOf(pdaid)).collect(Collectors.joining(",", "(", ")"));
		}
		this.deliveryLeadTime = product.getDeliveryLeadTime();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getProductVersion() {
		return productVersion;
	}

	public void setProductVersion(int productVersion) {
		this.productVersion = productVersion;
	}

	public String getProductIdentity() {
		return productIdentity;
	}

	public void setProductIdentity(String productIdentity) {
		this.productIdentity = productIdentity;
	}

	public String getProductNamePrimary() {
		return productNamePrimary;
	}

	public void setProductNamePrimary(String productNamePrimary) {
		this.productNamePrimary = productNamePrimary;
	}

	public String getProductNameSecondary() {
		return productNameSecondary;
	}

	public void setProductNameSecondary(String productNameSecondary) {
		this.productNameSecondary = productNameSecondary;
	}

	public LocalDateTime getExpDate() {
		return expDate;
	}

	public void setExpDate(LocalDateTime expDate) {
		this.expDate = expDate;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductModel() {
		return productModel;
	}

	public void setProductModel(String productModel) {
		this.productModel = productModel;
	}

	public String getProductDescription() {
		return productDescription;
	}

	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}

	public String getRegistrationNumber() {
		return registrationNumber;
	}

	public void setRegistrationNumber(String registrationNumber) {
		this.registrationNumber = registrationNumber;
	}

	public String getHashTag() {
		return hashTag;
	}

	public void setHashTag(String hashTag) {
		this.hashTag = hashTag;
	}


	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getPackaging() {
		return packaging;
	}

	public void setPackaging(String packaging) {
		this.packaging = packaging;
	}

	public int getMinOfOrder() {
		return minOfOrder;
	}

	public void setMinOfOrder(int minOfOrder) {
		this.minOfOrder = minOfOrder;
	}

	public Long getVerifiedBy() {
		return verifiedBy;
	}

	public void setVerifiedBy(Long verifiedBy) {
		this.verifiedBy = verifiedBy;
	}

	public Long getApprovedBy() {
		return approvedBy;
	}

	public void setApprovedBy(Long approvedBy) {
		this.approvedBy = approvedBy;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Long getProductBrandId() {
		return productBrandId;
	}

	public void setProductBrandId(Long productBrandId) {
		this.productBrandId = productBrandId;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public Long getProductCategoryId() {
		return productCategoryId;
	}

	public void setProductCategoryId(Long productCategoryId) {
		this.productCategoryId = productCategoryId;
	}

	public LocalDateTime getApprovedDate() {
		return approvedDate;
	}

	public void setApprovedDate(LocalDateTime approvedDate) {
		this.approvedDate = approvedDate;
	}

	public String getOtherDescription() {
		return otherDescription;
	}

	public void setOtherDescription(String otherDescription) {
		this.otherDescription = otherDescription;
	}

	public Integer getPreviousStatus() {
		return previousStatus;
	}

	public void setPreviousStatus(Integer previousStatus) {
		this.previousStatus = previousStatus;
	}

	public Boolean getCanProcure() {
		return canProcure;
	}

	public void setCanProcure(Boolean canProcure) {
		this.canProcure = canProcure;
	}

	public Long getDeviceCategory() {
		return deviceCategory;
	}

	public void setDeviceCategory(Long deviceCategory) {
		this.deviceCategory = deviceCategory;
	}

	public Long getVendorId() {
		return vendorId;
	}

	public void setVendorId(Long vendorId) {
		this.vendorId = vendorId;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public String getDocumentAttachmentId() {
		return documentAttachmentId;
	}

	public void setDocumentAttachmentId(String documentAttachmentId) {
		this.documentAttachmentId = documentAttachmentId;
	}

	public String getDeliveryLeadTime() {
		return deliveryLeadTime;
	}

	public void setDeliveryLeadTime(String deliveryLeadTime) {
		this.deliveryLeadTime = deliveryLeadTime;
	}
}

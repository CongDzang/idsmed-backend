package com.cmg.idsmed.model.entity.product.log;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.cmg.idsmed.model.entity.AbstractEntity;
import com.cmg.idsmed.model.entity.product.ProductMedia;

@Entity
@Table(name = "product_media_log")
public class ProductMediaLog extends AbstractEntity {

    //We prefer sequence generator than identity. If you use identity it may disable JDBC batching
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "product_media_log_id_seq")
    @SequenceGenerator(name = "product_media_log_id_seq",
            sequenceName = "product_media_log_id_seq",
            allocationSize = 1)
    @Column(updatable = false)
    private Long id;

	@Column(name = "product_id")
	private Long productId;

    @Column(name = "version")
    private int version;

    @Column(name = "document_name")
    @Size(max = 100)
    private String documentName;

    @Column(name = "document_type")
    @Size(max = 100)
    private String documentType;

    @Column(name = "document_path")
    @Size(max = 1000)
    private String documentPath;

    @Column(name = "document_seq")
    private int documentSeq;

    @Column(name = "remarks")
    @Size(max = 3000)
    private String remarks;

    @Column(name = "country_code")
    @Size(max = 10)
    private String countryCode;

    @Column(name = "document_url")
    @Size(max = 1000)
    private String documentUrl;

    @Column(name = "gid")
    @Size(max = 100)
    private String gid;

	@Column(name = "customer_url")
    @Size(max = 1000)
    private String customerUrl;

    public ProductMediaLog(ProductMedia productMedia){
    	if(productMedia.getProduct() != null){
    		this.productId = productMedia.getProduct().getId();
			this.version = productMedia.getProduct().getProductVersion();
		}
		this.documentName = productMedia.getDocumentName();
    	this.documentType = productMedia.getDocumentType();
    	this.documentPath = productMedia.getDocumentPath();
    	this.documentSeq = productMedia.getDocumentSeq();
    	this.remarks = productMedia.getRemarks();
    	this.countryCode = productMedia.getCountryCode();
    	this.documentUrl = productMedia.getDocumentUrl();
		this.gid = productMedia.getGid();
		this.customerUrl = productMedia.getCustomerUrl();
	}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}
	
	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public String getDocumentName() {
		return documentName;
	}

	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}

	public String getDocumentType() {
		return documentType;
	}

	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}

	public String getDocumentPath() {
		return documentPath;
	}

	public void setDocumentPath(String documentPath) {
		this.documentPath = documentPath;
	}

	public int getDocumentSeq() {
		return documentSeq;
	}

	public void setDocumentSeq(int documentSeq) {
		this.documentSeq = documentSeq;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getDocumentUrl() {
		return documentUrl;
	}

	public void setDocumentUrl(String documentUrl) {
		this.documentUrl = documentUrl;
	}

	public String getGid() {
		return gid;
	}

	public void setGid(String gid) {
		this.gid = gid;
	}

	public String getCustomerUrl() {
		return customerUrl;
	}

	public void setCustomerUrl(String customerUrl) {
		this.customerUrl = customerUrl;
	}
}

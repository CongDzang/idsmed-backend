package com.cmg.idsmed.model.entity.product.log;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import com.cmg.idsmed.model.entity.AbstractEntity;
import com.cmg.idsmed.model.entity.product.ProductReject;

@Entity
@Table(name = "product_reject_log")
public class ProductRejectLog extends AbstractEntity {

    //We prefer sequence generator than identity. If you use identity it may disable JDBC batching
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "product_reject_log_id_seq")
    @SequenceGenerator(name = "product_reject_log_id_seq",
            sequenceName = "product_reject_log_id_seq",
            allocationSize = 1)
    @Column(updatable = false)
    private Long id;

    @Column(name = "product_id")
    private Long productId;

    @Column(name = "version")
    private int version;

    @Column(name = "remarks")
    @Size(max = 3000)
    private String remarks;

    @Column(name = "country_code")
    @Size(max = 10)
    private String countryCode;

    public ProductRejectLog(ProductReject productReject){
    	if(productReject.getProduct() != null){
    		this.productId = productReject.getProduct().getId();
    		this.version = productReject.getProduct().getProductVersion();
		}
		this.remarks = productReject.getRemarks();
    	this.countryCode = productReject.getCountryCode();
	}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
}

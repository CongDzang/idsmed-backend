package com.cmg.idsmed.model.entity.product.search;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import com.cmg.idsmed.model.entity.AbstractEntity;
import org.springframework.util.StringUtils;

@Entity
@Table(name = "keyword_history")
public class KeywordHistory extends AbstractEntity {

	private static final long serialVersionUID = 5771796917019428678L;

	//We prefer sequence generator than identity. If you use identity it may disable JDBC batching
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "keyword_history_id_seq")
    @SequenceGenerator(name = "keyword_history_id_seq",
            sequenceName = "keyword_history_id_seq",
            allocationSize = 1)
    @Column(updatable = false)
    private Long id;

    @Column(name = "user_id")
    private Long userId;

	@Column(name = "wedoctor_customer_id")
	private  Long wedoctorCustomerId;

    @Column(name = "keyword")
    @Size(max = 500)
    private String keyword;

    @Column(name = "care_area_ids")
	private String careAreaIds;

    @Column(name = "category_ids")
	private String categoryIds;

    @Column(name = "brand_ids")
	private String brandIds;

    @Column(name = "product_ids")
	private String productIds;

	public KeywordHistory() {
	}

	public KeywordHistory(Long wedoctorCustomerId, String keyword, String careAreaIds, String categoryIds, String brandIds, String productIds) {
		if (wedoctorCustomerId != null) {
			this.wedoctorCustomerId = wedoctorCustomerId;
		}

		this.keyword = keyword;
		if (!StringUtils.isEmpty(careAreaIds)) {
			this.careAreaIds = careAreaIds;
		}

		if (!StringUtils.isEmpty(brandIds)) {
			this.brandIds = brandIds;
		}

		if (!StringUtils.isEmpty(categoryIds)) {
			this.categoryIds = categoryIds;
		}

		if (!StringUtils.isEmpty(productIds)) {
			this.productIds = productIds;
		}
	}

	public KeywordHistory(Long userId, String keyword, String careAreaIds, String categoryIds
			, String brandIds, String productIds, Boolean isForOurSide){
		this.userId = userId;
		this.keyword = keyword;
		if (!StringUtils.isEmpty(careAreaIds)) {
			this.careAreaIds = careAreaIds;
		}

		if (!StringUtils.isEmpty(brandIds)) {
			this.brandIds = brandIds;
		}

		if (!StringUtils.isEmpty(categoryIds)) {
			this.categoryIds = categoryIds;
		}

		if (!StringUtils.isEmpty(productIds)) {
			this.productIds = productIds;
		}


	}
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public Long getWedoctorCustomerId() {
		return wedoctorCustomerId;
	}

	public void setWedoctorCustomerId(Long wedoctorCustomerId) {
		this.wedoctorCustomerId = wedoctorCustomerId;
	}

	public String getCareAreaIds() { return careAreaIds; }

	public void setCareAreaIds(String careAreaIds) { this.careAreaIds = careAreaIds; }

	public String getCategoryIds() { return categoryIds; }

	public void setCategoryIds(String categoryIds) { this.categoryIds = categoryIds; }

	public String getBrandIds() { return brandIds; }

	public void setBrandIds(String brandIds) { this.brandIds = brandIds; }

	public String getProductIds() { return productIds; }

	public void setProductIds(String productIds) { this.productIds = productIds; }
}

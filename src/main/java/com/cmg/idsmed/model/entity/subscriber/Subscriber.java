package com.cmg.idsmed.model.entity.subscriber;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.cmg.idsmed.model.entity.AbstractEntity;
import com.cmg.idsmed.model.entity.auth.IdsmedAccount;

@Entity
@Table(name = "subscriber")
public class Subscriber extends AbstractEntity {

    //We prefer sequence generator than identity. If you use identity it may disable JDBC batching
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "subscriber_id_seq")
    @SequenceGenerator(name = "subscriber_id_seq",
            sequenceName = "subscriber_id_seq",
            allocationSize = 1)
    @Column(updatable = false)
    private Long id;

    @Column(name = "name_primary")
    @NotNull
    @Size(max = 250)
    private String namePrimary;

    @Column(name = "name_secondary")
    @Size(max = 250)
    private String nameSecondary;

    @Column(name = "client_id")
    @NotNull
    @Size(max = 250)
    private String clientId;


    @Column(name = "exp_date")
    private Timestamp expDate;
    
    @Column(name = "country_code")
    @NotNull
    @Size(max = 10)
    private String countryCode;
    
    @Column(name = "primary_lang")
    @NotNull
    @Size(max = 10)
    private String primaryLang;

    @OneToMany(mappedBy = "subscriber")
	private List<IdsmedAccount> accountList;
    
    @OneToMany(mappedBy = "subscriber")
	private List<SubscriberIp> subscriberIpList;

    @OneToMany(mappedBy = "subscriber")
    private List<TokenInformation> tokenInfomationList;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNamePrimary() {
		return namePrimary;
	}

	public void setNamePrimary(String namePrimary) {
		this.namePrimary = namePrimary;
	}

	public String getNameSecondary() {
		return nameSecondary;
	}

	public void setNameSecondary(String nameSecondary) {
		this.nameSecondary = nameSecondary;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public Timestamp getExpDate() {
		return expDate;
	}

	public void setExpDate(Timestamp expDate) {
		this.expDate = expDate;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getPrimaryLang() {
		return primaryLang;
	}

	public void setPrimaryLang(String primaryLang) {
		this.primaryLang = primaryLang;
	}

	public List<SubscriberIp> getSubscriberIpList() {
		return subscriberIpList;
	}

	public void setSubscriberIpList(List<SubscriberIp> subscriberIpList) {
		this.subscriberIpList = subscriberIpList;
	}

	public List<TokenInformation> getTokenInfomationList() {
		return tokenInfomationList;
	}

	public void setTokenInfomationList(List<TokenInformation> tokenInfomationList) {
		this.tokenInfomationList = tokenInfomationList;
	}

	public List<IdsmedAccount> getAccountList() {
		return accountList;
	}

	public void setAccountList(List<IdsmedAccount> accountList) {
		this.accountList = accountList;
	}
}

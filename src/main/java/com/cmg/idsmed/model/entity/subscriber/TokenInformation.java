package com.cmg.idsmed.model.entity.subscriber;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

import com.cmg.idsmed.model.entity.AbstractEntity;

@Entity
@Table(name = "token_information")
public class TokenInformation extends AbstractEntity {

    //We prefer sequence generator than identity. If you use identity it may disable JDBC batching
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "token_information_id_seq")
    @SequenceGenerator(name = "token_information_id_seq",
            sequenceName = "token_information_id_seq",
            allocationSize = 1)
    @Column(updatable = false)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "subscriber_id")
    private Subscriber subscriber;

    @Column(name = "token_string")
    @NotNull
    @Size(max = 250)
    private String tokenString;

    @Column(name = "refresh_token_string")
    @Size(max = 250)
    private String refreshTokenString;

    @Column(name = "remarks")
    @Size(max = 500)
    private String remarks;
    
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(pattern = "MM/dd/yyyy")
    @Column(name = "token_string_exp_date")
    @NotNull
    private Date tokenStringExpDate;
    
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(pattern = "MM/dd/yyyy")
    @Column(name = "refresh_token_string_exp_date")
    @NotNull
    private Date refreshTokenStringExpDate;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Subscriber getSubscriber() {
		return subscriber;
	}

	public void setSubscriber(Subscriber subscriber) {
		this.subscriber = subscriber;
	}

	public String getTokenString() {
		return tokenString;
	}

	public void setTokenString(String tokenString) {
		this.tokenString = tokenString;
	}

	public String getRefreshTokenString() {
		return refreshTokenString;
	}

	public void setRefreshTokenString(String refreshTokenString) {
		this.refreshTokenString = refreshTokenString;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public void onTokenStringExpDate() {
		this.tokenStringExpDate = new Date();
	}
	
	public Date getTokenStringExpDate() {
		return tokenStringExpDate;
	}
	public void setTokenStringExpDate(Date tokenStringExpDate) {
		this.tokenStringExpDate = tokenStringExpDate;
	}

	public void onRefreshTokenStringExpDate() {
		this.refreshTokenStringExpDate = new Date();
	}
	
	public Date getRefreshTokenStringExpDate() {
		return refreshTokenStringExpDate;
	}

	public void setRefreshTokenStringExpDate(Date refreshTokenStringExpDate) {
		this.refreshTokenStringExpDate = refreshTokenStringExpDate;
	}
}

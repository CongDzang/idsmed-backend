package com.cmg.idsmed.model.entity.task;

import com.cmg.idsmed.model.entity.AbstractEntity;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * This entity using TaskStatusEnum
 */


@Entity
@Table(name = "idsmed_task")
public class IdsmedTask extends AbstractEntity {

	public enum TaskStatusEnum {
		INCOMPLETED(0, "Incompleted"),
		COMPLETED(1, "Completed");

		private Integer code;
		private String name;

		TaskStatusEnum(Integer code, String name) {
			this.code = code;
			this.name = name;
		}

		public Integer getCode() {
			return code;
		}

		public String getName() {
			return name;
		}
	}

	//We prefer sequence generator than identity. If you use identity it may disable JDBC batching
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "idsmed_task_id_seq")
	@SequenceGenerator(name = "idsmed_task_id_seq",
			sequenceName = "idsmed_task_id_seq",
			allocationSize = 1)
	@Column(updatable = false)
	private Long id;

	@Column(name = "action_link")
	private String actionLink;

	@Column(name = "task_code")
	private String taskCode;

	/**
	 * TaskTypeEnum
	 */
	@Column(name = "task_type_code")
	private Integer taskTypeCode;

	@Column(name = "entity_id")
	private Long entityId;

	@Column(name = "description")
	private String description;

	/**
	 * Save loginId of account which done the task.
	 */
	@Column(name = "done_by")
	@Size(max = 255)
	private String doneBy;

	@OneToMany(mappedBy = "idsmedTask")
	private List<IdsmedTaskExecutor> taskExecutors;


	public IdsmedTask() {
	}

	public IdsmedTask(String actionLink, String taskCode, Integer taskTypeCode
			, Long entityId, String description) {
		this.actionLink = actionLink;
		this.taskCode = taskCode;
		this.taskTypeCode = taskTypeCode;
		this.entityId = entityId;
		this.description = description;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getActionLink() {
		return actionLink;
	}

	public void setActionLink(String actionLink) {
		this.actionLink = actionLink;
	}

	public String getTaskCode() {
		return taskCode;
	}

	public Long getEntityId() {
		return entityId;
	}

	public void setEntityId(Long entityId) {
		this.entityId = entityId;
	}

	public String getDoneBy() {
		return doneBy;
	}

	public void setDoneBy(String doneBy) {
		this.doneBy = doneBy;
	}

	public List<IdsmedTaskExecutor> getTaskExecutors() {
		return taskExecutors;
	}

	public void setTaskExecutors(List<IdsmedTaskExecutor> taskExecutors) {
		this.taskExecutors = taskExecutors;
	}

	public void setTaskCode(String taskCode) {
		this.taskCode = taskCode;
	}

	public Integer getTaskTypeCode() {
		return taskTypeCode;
	}

	public void setTaskTypeCode(Integer taskTypeCode) {
		this.taskTypeCode = taskTypeCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}

package com.cmg.idsmed.model.entity.task;


import com.cmg.idsmed.model.entity.AbstractEntity;
import com.cmg.idsmed.model.entity.auth.IdsmedAccount;

import javax.persistence.*;

@Entity
@Table(name = "idsmed_task_executor")
public class IdsmedTaskExecutor extends AbstractEntity {

	//We prefer sequence generator than identity. If you use identity it may disable JDBC batching
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "idsmed_task_executor_id_seq")
	@SequenceGenerator(name = "idsmed_task_executor_id_seq",
			sequenceName = "idsmed_task_executor_id_seq",
			allocationSize = 1)
	@Column(updatable = false)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "idsmed_task_id")
	private IdsmedTask idsmedTask;

	@ManyToOne
	@JoinColumn(name = "idsmed_account_id")
	private IdsmedAccount idsmedAccount;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public IdsmedTask getIdsmedTask() {
		return idsmedTask;
	}

	public void setIdsmedTask(IdsmedTask idsmedTask) {
		this.idsmedTask = idsmedTask;
	}

	public IdsmedAccount getIdsmedAccount() {
		return idsmedAccount;
	}

	public void setIdsmedAccount(IdsmedAccount idsmedAccount) {
		this.idsmedAccount = idsmedAccount;
	}
}

package com.cmg.idsmed.model.entity.thirdparty;

import com.cmg.idsmed.common.enums.CountryCodeEnum;
import com.cmg.idsmed.dto.product.ProductUpload;
import com.cmg.idsmed.dto.thirdparty.CustomerRequest;
import com.cmg.idsmed.model.entity.AbstractEntity;
import com.cmg.idsmed.model.entity.product.*;
import com.cmg.idsmed.model.entity.product.list.BestSellerProductGlobal;
import com.cmg.idsmed.model.entity.product.list.HistoryUserProductBySearch;
import com.cmg.idsmed.model.entity.vendor.Vendor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.UUID;

//This is we doctor customer
@Entity
@Table(name = "customer")
public class Customer extends AbstractEntity implements Serializable {

    /**
	 * Status of this entity use CustomerStatusEnum pls go to see in detail
	 */
	private static final long serialVersionUID = 5771796917019428678L;

	//We prefer sequence generator than identity. If you use identity it may disable JDBC batching
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "customer_id_seq")
    @SequenceGenerator(name = "customer_id_seq",
            sequenceName = "customer_id_seq",
            allocationSize = 1)
    @Column(updatable = false)
    private Long id;

    @Column(name = "customer_id")
    private Long customerId;

    @Column(name = "mobile")
    @Size (max = 11)
	private String mobile;

    @Column(name = "nick_name")
    @Size(max = 32)
    private String nickName;
    
    @Column(name = "real_name")
    @Size(max = 32)
    private String realName;
    
    @Column(name = "sex")
    private Integer sex;
    
    @Column(name = "birth_date")
    @Size(max = 16)
    private String birthDate;
    
    @Column(name = "email")
    @Size(max = 64)
    private String email;
    
    @Column(name = "head_pic")
    @Size(max = 255)
    private String headPic;
    
    @Column(name = "id_card_no")
    @Size(max = 32)
    private String idCardNo;
    
    @Column(name = "company_name")
	@Size(max = 64)
    private String companyName;

    @Column(name = "company_type")
    private Integer companyType;
    
    @Column(name = "contact_name")
    @Size(max = 32)
    private String contactName;
    
    @Column(name = "contact_tel")
	@Size(max = 32)
    private String contactTel;
    
    @Column(name = "telephone")
	@Size(max = 32)
    private String telephone;
    
    @Column(name = "province_code")
	@Size(max = 16)
    private String provinceCode;

	@Column(name = "city_code")
	@Size(max = 16)
	private String cityCode;

    @Column(name = "district_code")
    @Size(max = 16)
    private String districtCode;

    @Column(name = "province_name")
    @Size(max = 16)
    private String provinceName;
    
    @Column(name = "city_name")
	@Size(max = 16)
    private String cityName;
    
    @Column(name = "district_name")
    @Size(max = 16)
    private String districtName;

	@Column(name = "address")
	@Size(max = 128)
	private String address;

	public Customer(){}

	public Customer(CustomerRequest customerRequest){
		this.customerId = customerRequest.getCustomerId();
		this.mobile = customerRequest.getMobile();
		this.nickName = customerRequest.getNickName();
		this.realName = customerRequest.getRealName();
		this.sex = customerRequest.getSex();
		this.birthDate = customerRequest.getBirthDate();
		this.email = customerRequest.getEmail();
		this.headPic = customerRequest.getHeadPic();
		this.idCardNo = customerRequest.getIdCardNo();
		this.companyName = customerRequest.getCompanyName();
		this.companyType = customerRequest.getCompanyType();
		this.contactName = customerRequest.getContactName();
		this.contactTel = customerRequest.getContactTel();
		this.telephone = customerRequest.getTelephone();
		this.provinceCode = customerRequest.getProvinceCode();
		this.cityCode = customerRequest.getCityCode();
		this.districtCode = customerRequest.getDistrictCode();
		this.provinceName = customerRequest.getProvinceName();
		this.cityName = customerRequest.getCityName();
		this.districtName = customerRequest.getDistrictName();
		this.address = customerRequest.getAddress();
	}


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	public Long getCustomerId() { return customerId; }

	public void setCustomerId(Long customerId) { this.customerId = customerId; }

	public String getMobile() { return mobile; }

	public void setMobile(String mobile) { this.mobile = mobile; }

	public String getNickName() { return nickName; }

	public void setNickName(String nickName) { this.nickName = nickName; }

	public String getRealName() { return realName; }

	public void setRealName(String realName) { this.realName = realName; }

	public Integer getSex() { return sex; }

	public void setSex(Integer sex) { this.sex = sex; }

	public String getBirthDate() { return birthDate; }

	public void setBirthDate(String birthDate) { this.birthDate = birthDate; }

	public String getEmail() { return email; }

	public void setEmail(String email) { this.email = email; }

	public String getHeadPic() { return headPic; }

	public void setHeadPic(String headPic) { this.headPic = headPic; }

	public String getIdCardNo() { return idCardNo; }

	public void setIdCardNo(String idCardNo) { this.idCardNo = idCardNo; }

	public String getCompanyName() { return companyName; }

	public void setCompanyName(String companyName) { this.companyName = companyName; }

	public String getContactName() { return contactName; }

	public void setContactName(String contactName) { this.contactName = contactName; }

	public String getContactTel() { return contactTel; }

	public void setContactTel(String contactTel) { this.contactTel = contactTel; }

	public String getTelephone() { return telephone; }

	public void setTelephone(String telephone) { this.telephone = telephone; }

	public String getProvinceCode() { return provinceCode; }

	public void setProvinceCode(String provinceCode) { this.provinceCode = provinceCode; }

	public String getCityCode() { return cityCode; }

	public void setCityCode(String cityCode) { this.cityCode = cityCode; }

	public String getDistrictCode() { return districtCode; }

	public void setDistrictCode(String districtCode) { this.districtCode = districtCode; }

	public String getProvinceName() { return provinceName; }

	public void setProvinceName(String provinceName) { this.provinceName = provinceName; }

	public String getCityName() { return cityName; }

	public void setCityName(String cityName) { this.cityName = cityName; }

	public String getDistrictName() { return districtName; }

	public void setDistrictName(String districtName) { this.districtName = districtName; }

	public Integer getCompanyType() { return companyType; }

	public void setCompanyType(Integer companyType) { this.companyType = companyType; }

	public String getAddress() { return address; }

	public void setAddress(String address) { this.address = address; }
}

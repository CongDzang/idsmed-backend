package com.cmg.idsmed.model.entity.vendor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.cmg.idsmed.common.utils.DateTimeHelper;
import com.cmg.idsmed.dto.vendor.VendorFileAttachmentRequest;
import com.cmg.idsmed.model.entity.AbstractEntity;
import com.cmg.idsmed.model.entity.vendor.config.VendorLabelConfig;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

/**
 * Status of this entity using FileAttachmentStatusEnum
 */

@Entity
@Table(name = "file_attachment")
public class FileAttachment extends AbstractEntity {

    //We prefer sequence generator than identity. If you use identity it may disable JDBC batching
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "file_attachment_id_seq")
    @SequenceGenerator(name = "file_attachment_id_seq",
            sequenceName = "file_attachment_id_seq",
            allocationSize = 1)
    @Column(updatable = false)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "vendor_id")
    private Vendor vendor;
    
    @Column(name = "document_name")
    @Size(max = 100)
    private String documentName;

	@Column(name = "document_secondary_name")
	@Size(max = 500)
	private String documentSecondaryName;

	@Column(name = "original_file_name")
	@Size(max = 1000)
	private String originalFileName;

    @Column(name = "document_type")
    @Size(max = 100)
    private String documentType;

    @Column(name = "document_path")
    @Size(max = 1000)
    private String documentPath;

    @Column(name = "remarks")
    @Size(max = 3000)
    private String remarks;

    @Column(name = "country_code")
    @NotNull
    @Size(max = 10)
    private String countryCode;

    @Column(name = "is_verify")
	private Boolean isVerify;

   	@ManyToOne
	@JoinColumn(name = "vendor_label_config_id")
	private VendorLabelConfig vendorLabelConfig;

   	@OneToMany(mappedBy = "fileAttachment")
	List<VendorFileAttachmentReminder> vendorFileAttachmentReminders;

   	@Column(name = "expiry_date")
	private LocalDateTime expiryDate;

	@Column(name = "exp_date_tz")
	private String expDateTimeZone;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Vendor getVendorId() {
		return vendor;
	}

	public void setVendor(Vendor vendor) {
		this.vendor = vendor;
	}

	public String getDocumentName() {
		return documentName;
	}

	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}

	public String getDocumentType() {
		return documentType;
	}

	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}

	public String getDocumentPath() {
		return documentPath;
	}

	public void setDocumentPath(String documentPath) {
		this.documentPath = documentPath;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public VendorLabelConfig getVendorLabelConfig() {
		return vendorLabelConfig;
	}

	public void setVendorLabelConfig(VendorLabelConfig vendorLabelConfig) {
		this.vendorLabelConfig = vendorLabelConfig;
	}

	public Vendor getVendor() {
		return vendor;
	}

	public String getOriginalFileName() {
		return originalFileName;
	}

	public void setOriginalFileName(String originalFileName) {
		this.originalFileName = originalFileName;
	}

	public String getDocumentSecondaryName() {
		return documentSecondaryName;
	}

	public void setDocumentSecondaryName(String documentSecondaryName) {
		this.documentSecondaryName = documentSecondaryName;
	}

	public LocalDateTime getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(LocalDateTime expiryDate) {
		this.expiryDate = expiryDate;
	}

	public Boolean getVerify() {
		return isVerify;
	}

	public void setVerify(Boolean verify) {
		isVerify = verify;
	}

	public void updateFileAttachment(VendorFileAttachmentRequest request, Long updatedBy, String ossBucketDomain) {
		this.setStatus(request.getStatus());
		this.setVerify(request.getVerify());
		this.setExpiryDate(request.getExpiryDate() != null ? DateTimeHelper.convertZonedDateTimeToUTC(request.getExpiryDate()) : null);
		this.setExpDateTimeZone(request.getExpDateTimeZone());
		this.setOriginalFileName(request.getOriginalFileName());
		this.setDocumentSecondaryName(request.getDocumentSecondaryName());
		this.setDocumentName(request.getDocumentName());

		if (!StringUtils.isEmpty(request.getDocumentPath()) && !StringUtils.isEmpty(ossBucketDomain)) {
			this.setDocumentPath(request.getDocumentPath().replace(ossBucketDomain, ""));
		}

		this.setRemarks(request.getRemarks());
		this.setDocumentType(request.getDocumentType());
		if (updatedBy != null) {
			this.setUpdatedBy(updatedBy);
		}
	}

	public String getExpDateTimeZone() {
		return expDateTimeZone;
	}

	public void setExpDateTimeZone(String expDateTimeZone) {
		this.expDateTimeZone = expDateTimeZone;
	}

	public List<VendorFileAttachmentReminder> getVendorFileAttachmentReminders() {
		return vendorFileAttachmentReminders;
	}

	public void setVendorFileAttachmentReminders(List<VendorFileAttachmentReminder> vendorFileAttachmentReminders) {
		this.vendorFileAttachmentReminders = vendorFileAttachmentReminders;
	}
}

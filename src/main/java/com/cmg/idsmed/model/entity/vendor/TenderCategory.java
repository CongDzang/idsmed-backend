package com.cmg.idsmed.model.entity.vendor;

import com.cmg.idsmed.dto.vendor.VendorAddressRequest;
import com.cmg.idsmed.model.entity.AbstractEntity;
import com.cmg.idsmed.model.entity.auth.IdsmedAccount;
import com.cmg.idsmed.model.entity.masterdata.Country;
import com.cmg.idsmed.model.entity.masterdata.Province;
import org.springframework.util.StringUtils;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Date;

/**
 * Status of this entity is using EntitySimpleStatusEnum
 *
 */

@Entity
@Table(name = "tender_category")
public class TenderCategory extends AbstractEntity {

	private static final long serialVersionUID = -8369248221195641298L;

	// We prefer sequence generator than identity. If you use identity it may
	// disable JDBC batching
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tender_category_id_seq")
	@SequenceGenerator(name = "tender_category_id_seq", sequenceName = "tender_category_id_seq", allocationSize = 1)
	@Column(updatable = false)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "idsmed_account_id")
	private IdsmedAccount idsmedAccount;

	@Column(name = "care_area_id")
	private Long careAreaId;

	public TenderCategory() { }

	public TenderCategory(IdsmedAccount idsmedAccount, Long careAreaId){
		this.idsmedAccount = idsmedAccount;
		this.careAreaId = careAreaId;
	}

	public Long getId() { return id; }

	public void setId(Long id) { this.id = id; }

	public IdsmedAccount getIdsmedAccount() { return idsmedAccount; }

	public void setIdsmedAccount(IdsmedAccount idsmedAccount) { this.idsmedAccount = idsmedAccount; }

	public Long getCareAreaId() { return careAreaId; }

	public void setCareAreaId(Long careAreaId) { this.careAreaId = careAreaId; }
}

package com.cmg.idsmed.model.entity.vendor;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.cmg.idsmed.dto.vendor.VendorRequest;
import org.springframework.format.annotation.DateTimeFormat;

import com.cmg.idsmed.model.entity.AbstractEntity;
import com.cmg.idsmed.model.entity.product.Product;

/**
 * Status of this entity using StatusEnum
 */

@Entity
@Table(name = "vendor")
public class Vendor extends AbstractEntity {

	private static final long serialVersionUID = -8369248221195641298L;

	public LocalDateTime getApprovedDate() {
		return approvedDate;
	}

	public void setApprovedDate(LocalDateTime approvedDate) {
		this.approvedDate = approvedDate;
	}

	public enum VendorTypeEnum {
		/**
		 * 0 - all vendor type
		 * 1 - manufacturer
		 * 2 - distributor/reseller
		 * 3 - main distributor
		 */
		ALL(0, "All"),
		MANUFACTURER(1, "Manufacturer"),
		DISTRIBUTOR(2, "Distributor - Reseller"),
		MAIN_DISTRIBUTOR(3, "Main Distributor");
		private Integer code;
		private String name;
		VendorTypeEnum(Integer code, String name) {
			this.code = code;
			this.name = name;
		}

		public Integer getCode() {
			return code;
		}

		public String getName() {
			return name;
		}
	}

	public enum CUSTGROUP {
		TRAD("TRAD");

		private String name;

		CUSTGROUP (String name) { this.name = name; }

		public String getName() { return name; }
	}

	public enum CURRENCY {
		CNY("CNY");

		private String name;

		CURRENCY (String name) { this.name = name; }

		public String getName() { return name; }
	}

	// We prefer sequence generator than identity. If you use identity it may
	// disable JDBC batching
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "vendor_id_seq")
	@SequenceGenerator(name = "vendor_id_seq", sequenceName = "vendor_id_seq", allocationSize = 1)
	@Column(updatable = false)
	private Long id;

	@Column(name = "company_name_primary")
	@Size(max = 200)
	private String companyNamePrimary;

	@Column(name = "company_name_secondary")
	@Size(max = 200)
	private String companyNameSecondary;

	@Column(name = "address")
	@Size(max = 1000)
	private String address;

	@Column(name = "phone_number")
	@Size(max = 100)
	private String phoneNumber;

	@Column(name = "email")
	@Size(max = 100)
	private String email;

	@Column(name = "person_incharge1_name")
	@Size(max = 100)
	private String personIncharge1Name;

	@Column(name = "person_incharge1_phone")
	@Size(max = 100)
	private String personIncharge1Phone;

	@Column(name = "person_incharge1_email")
	@Size(max = 100)
	private String personIncharge1Email;

	@Column(name = "person_incharge1_wechat")
	@Size(max = 100)
	private String personIncharge1Wechat;

	@Column(name = "person_incharge1_qq")
	@Size(max = 100)
	private String personIncharge1Qq;

	@Column(name = "person_incharge2_name")
	@Size(max = 100)
	private String personIncharge2Name;

	@Column(name = "person_incharge2_phone")
	@Size(max = 100)
	private String personIncharge2Phone;

	@Column(name = "person_incharge2_email")
	@Size(max = 100)
	private String personIncharge2Email;

	@Column(name = "person_incharge2_wechat")
	@Size(max = 100)
	private String personIncharge2Wechat;

	@Column(name = "person_incharge2_qq")
	@Size(max = 100)
	private String personIncharge2Qq;

	@Column(name = "company_code")
	@Size(max = 100)
	private String companyCode;

	@Column(name = "country_code")
	@NotNull
	@Size(max = 10)
	private String countryCode;

	@Column(name = "company_logo_path")
	@Size(max = 1000)
	private String companyLogoPath;
	
	@Column(name = "approved_by")
	private Long approvedBy;

//	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "MM/dd/yyyy HH:mm:ss.SSS")
	@Column(name = "approved_datetime")
	private LocalDateTime approvedDate;

	/**
	 * 1 - local
	 * 2 - oversea
	 * LocationTypeEnum
	 */
	// Comment this because currently this field need to remove for this spring, but din't drop this column because maybe future still need this field.
	// Todo After few month, if boss din't mention about this field then will drop this column from database.
	// @Column(name = "location")
	// private Integer location;

	/**
	 * 1 - manufacturer
	 * 2 - distributor
	 * 3 - main distributor
	 * VendorTypeEnum
	 */
	@Column(name = "type")
	private Integer type;

	@Column(name = "account_number")
	private String accountNumber;

	@Column(name = "cust_group")
	private String custGroup;

	@Column(name = "currency")
	private String currency;

	@Column(name = "vat_gst")
	private String vatGst;

	@Column(name = "bank_of_deposit")
	private String bankOfDeposit;

	@Column(name = "billing_info_company_name")
	private String billingInfoCompanyName;

	@Column(name = "billing_info_telephone")
	private String billingInfoTelephone;

	@Column(name = "bank_account_number")
	private String bankAccountNumber;

	@OneToMany(mappedBy = "vendor")
	private List<Product> productList;

	@OneToMany(mappedBy = "vendor")
	private List<VendorReject> vendorRejectList;

	@OneToMany(mappedBy = "vendor")
	private List<FileAttachment> fileAttachmentList;

	@OneToMany(mappedBy = "vendor")
	private List<VendorAdditionalInfo> additionalInfos;

	@OneToMany(mappedBy = "vendor")
	private List<VendorAddress> vendorAddresses;

	@OneToMany(mappedBy = "vendor")
	private List<VendorActionLog> vendorActionLogs;

	@Column(name = "vendor_coordinator")
	private Long vendorCoordinator;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCompanyNamePrimary() {
		return companyNamePrimary;
	}

	public void setCompanyNamePrimary(String companyNamePrimary) {
		this.companyNamePrimary = companyNamePrimary;
	}

	public String getCompanyNameSecondary() {
		return companyNameSecondary;
	}

	public void setCompanyNameSecondary(String companyNameSecondary) {
		this.companyNameSecondary = companyNameSecondary;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPersonIncharge1Name() {
		return personIncharge1Name;
	}

	public void setPersonIncharge1Name(String personIncharge1Name) {
		this.personIncharge1Name = personIncharge1Name;
	}

	public String getPersonIncharge1Phone() {
		return personIncharge1Phone;
	}

	public void setPersonIncharge1Phone(String personIncharge1Phone) {
		this.personIncharge1Phone = personIncharge1Phone;
	}

	public String getPersonIncharge1Email() {
		return personIncharge1Email;
	}

	public void setPersonIncharge1Email(String personIncharge1Email) {
		this.personIncharge1Email = personIncharge1Email;
	}

	public String getPersonIncharge1Wechat() {
		return personIncharge1Wechat;
	}

	public void setPersonIncharge1Wechat(String personIncharge1Wechat) {
		this.personIncharge1Wechat = personIncharge1Wechat;
	}

	public String getPersonIncharge1Qq() {
		return personIncharge1Qq;
	}

	public void setPersonIncharge1Qq(String personIncharge1Qq) {
		this.personIncharge1Qq = personIncharge1Qq;
	}

	public String getPersonIncharge2Name() {
		return personIncharge2Name;
	}

	public void setPersonIncharge2Name(String personIncharge2Name) {
		this.personIncharge2Name = personIncharge2Name;
	}

	public String getPersonIncharge2Phone() {
		return personIncharge2Phone;
	}

	public void setPersonIncharge2Phone(String personIncharge2Phone) {
		this.personIncharge2Phone = personIncharge2Phone;
	}

	public String getPersonIncharge2Email() {
		return personIncharge2Email;
	}

	public void setPersonIncharge2Email(String personIncharge2Email) {
		this.personIncharge2Email = personIncharge2Email;
	}

	public String getPersonIncharge2Wechat() {
		return personIncharge2Wechat;
	}

	public void setPersonIncharge2Wechat(String personIncharge2Wechat) {
		this.personIncharge2Wechat = personIncharge2Wechat;
	}

	public String getPersonIncharge2Qq() {
		return personIncharge2Qq;
	}

	public void setPersonIncharge2Qq(String personIncharge2Qq) {
		this.personIncharge2Qq = personIncharge2Qq;
	}

	public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getCompanyLogoPath() {
		return companyLogoPath;
	}

	public void setCompanyLogoPath(String companyLogoPath) {
		this.companyLogoPath = companyLogoPath;
	}

	public Long getApprovedBy() {
		return approvedBy;
	}

	public void setApprovedBy(Long approvedBy) {
		this.approvedBy = approvedBy;
	}

	public List<Product> getProductList() {
		return productList;
	}

	public void setProductList(List<Product> productList) {
		this.productList = productList;
	}

	public List<VendorReject> getVendorRejectList() {
		return vendorRejectList;
	}

	public void setVendorRejectList(List<VendorReject> vendorRejectList) {
		this.vendorRejectList = vendorRejectList;
	}

	public List<FileAttachment> getFileAttachmentList() {
		return fileAttachmentList;
	}

	public void setFileAttachmentList(List<FileAttachment> fileAttachmentList) {
		this.fileAttachmentList = fileAttachmentList;
	}

	public List<VendorAdditionalInfo> getAdditionalInfos() {
		return additionalInfos;
	}

	public void setAdditionalInfos(List<VendorAdditionalInfo> additionalInfos) {
		this.additionalInfos = additionalInfos;
	}

	public List<VendorAddress> getVendorAddresses() {
		return vendorAddresses;
	}

	public void setVendorAddresses(List<VendorAddress> vendorAddresses) {
		this.vendorAddresses = vendorAddresses;
	}

	// public Integer getLocation() {
	// 	return location;
	// }
	//
	// public void setLocation(Integer location) {
	// 	this.location = location;
	// }

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getAccountNumber() { return accountNumber; }

	public void setAccountNumber(String accountNumber) { this.accountNumber = accountNumber; }

	public String getCustGroup() { return custGroup; }

	public void setCustGroup(String custGroup) { this.custGroup = custGroup; }

	public String getCurrency() { return currency; }

	public void setCurrency(String currency) { this.currency = currency; }

	public String getVatGst() { return vatGst; }

	public void setVatGst(String vatGst) { this.vatGst = vatGst; }

	public List<VendorActionLog> getVendorActionLogs() {
		return vendorActionLogs;
	}

	public void setVendorActionLogs(List<VendorActionLog> vendorActionLogs) {
		this.vendorActionLogs = vendorActionLogs;
	}

	public Long getVendorCoordinator() {
		return vendorCoordinator;
	}

	public void setVendorCoordinator(Long vendorCoordinator) {
		this.vendorCoordinator = vendorCoordinator;
	}

	public String getBankOfDeposit() { return bankOfDeposit; }

	public void setBankOfDeposit(String bankOfDeposit) { this.bankOfDeposit = bankOfDeposit; }

	public String getBillingInfoCompanyName() { return billingInfoCompanyName; }

	public void setBillingInfoCompanyName(String billingInfoCompanyName) { this.billingInfoCompanyName = billingInfoCompanyName; }

	public String getBillingInfoTelephone() { return billingInfoTelephone; }

	public void setBillingInfoTelephone(String billingInfoTelephone) { this.billingInfoTelephone = billingInfoTelephone; }

	public String getBankAccountNumber() { return bankAccountNumber; }

	public void setBankAccountNumber(String bankAccountNumber) { this.bankAccountNumber = bankAccountNumber; }
}

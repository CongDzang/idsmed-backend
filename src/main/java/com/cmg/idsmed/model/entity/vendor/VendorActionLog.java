package com.cmg.idsmed.model.entity.vendor;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

/**
 * This entity is just for log.
 * No need status, createdBy, updatedBy...
 * We don't extend from AbstractEntity
 *
 */

@Entity
@Table(name = "vendor_action_log")
public class VendorActionLog {

	private static final long serialVersionUID = -8369248221195641298L;

	//We prefer sequence generator than identity. If you use identity it may disable JDBC batching
	@Id
	@GeneratedValue(strategy= GenerationType.SEQUENCE, generator = "vendor_action_log_id_seq")
	@SequenceGenerator(name = "vendor_action_log_id_seq",
			sequenceName = "vendor_action_log_id_seq",
			allocationSize = 1)
	@Column(updatable = false)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "vendor_id")
	private Vendor vendor;

	@Column(name = "action_code")
	private Integer actionCode;

	@Column(name = "reason")
	private String reason;

	@Column(name = "detail_describer")
	private String detailDescriber;

	@Column(name = "handler_login_id")
	private String handlerLoginId;

	@Column(name = "approver_login_id")
	private String approverLoginId;

	@Column(name = "ref_code")
	private String refCode;

	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern = "MM/dd/yyyy")
	@Column(name="created_datetime")
	private Date createdDate = new Date();

	public VendorActionLog() {
	}

	public VendorActionLog(Integer actionCode, String reason, String detailDescriber
			, String handlerLoginId, String approverLoginId) {
		this.actionCode = actionCode;
		this.reason = reason;
		this.detailDescriber = detailDescriber;
		this.handlerLoginId = handlerLoginId;
		this.approverLoginId = approverLoginId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getActionCode() {
		return actionCode;
	}

	public void setActionCode(Integer actionCode) {
		this.actionCode = actionCode;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getDetailDescriber() {
		return detailDescriber;
	}

	public void setDetailDescriber(String detailDescriber) {
		this.detailDescriber = detailDescriber;
	}

	public String getHandlerLoginId() {
		return handlerLoginId;
	}

	public void setHandlerLoginId(String handlerLoginId) {
		this.handlerLoginId = handlerLoginId;
	}

	public String getApproverLoginId() {
		return approverLoginId;
	}

	public void setApproverLoginId(String approverLoginId) {
		this.approverLoginId = approverLoginId;
	}

	public String getRefCode() {
		return refCode;
	}

	public void setRefCode(String refCode) {
		this.refCode = refCode;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Vendor getVendor() {
		return vendor;
	}

	public void setVendor(Vendor vendor) {
		this.vendor = vendor;
	}
}

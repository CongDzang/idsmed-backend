package com.cmg.idsmed.model.entity.vendor;

import com.cmg.idsmed.dto.vendor.VendorAdditionalInfoRequest;
import com.cmg.idsmed.model.entity.AbstractEntity;
import com.cmg.idsmed.model.entity.vendor.config.VendorLabelConfig;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Entity
@Table(name = "vendor_additional_info")
public class VendorAdditionalInfo extends AbstractEntity {
	private static final long serialVersionUID = -8369248221195641298L;

	// We prefer sequence generator than identity. If you use identity it may
	// disable JDBC batching
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "vendor_additional_info_id_seq")
	@SequenceGenerator(name = "vendor_additional_info_id_seq", sequenceName = "vendor_additional_info_id_seq", allocationSize = 1)
	@Column(updatable = false)
	private Long id;

	@Column(name = "d_id")
	private Long dId = 0L;

	@Column(name = "field_name")
	@Size(max = 500)
	private String fieldName;

	@Column(name = "field_secondary_name")
	@Size(max = 500)
	private String fieldSecondaryName;
	
	@Column(name = "info_value")
	private String  infoValue;

	@ManyToOne
	@JoinColumn(name = "vendor_id", referencedColumnName = "id")
	private Vendor vendor;

	@ManyToOne
	@JoinColumn(name = "vendor_label_config_id")
	private VendorLabelConfig vendorLabelConfig;

	public void updateVendorAdditionalInfo(VendorAdditionalInfoRequest request) {
		this.setInfoValue(request.getInfoValue());
	}
	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getdId() {
		return dId;
	}

	public void setdId(Long dId) {
		this.dId = dId;
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public String getInfoValue() {
		return infoValue;
	}

	public void setInfoValue(String infoValue) {
		this.infoValue = infoValue;
	}

	public Vendor getVendor() {
		return vendor;
	}

	public void setVendor(Vendor vendor) {
		this.vendor = vendor;
	}

	public VendorLabelConfig getVendorLabelConfig() {
		return vendorLabelConfig;
	}

	public String getFieldSecondaryName() {
		return fieldSecondaryName;
	}

	public void setFieldSecondaryName(String fieldSecondaryName) {
		this.fieldSecondaryName = fieldSecondaryName;
	}

	public void setVendorLabelConfig(VendorLabelConfig vendorLabelConfig) {
		this.vendorLabelConfig = vendorLabelConfig;


	}
}

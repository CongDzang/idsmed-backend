package com.cmg.idsmed.model.entity.vendor;

import com.cmg.idsmed.common.utils.DateTimeHelper;
import com.cmg.idsmed.dto.vendor.VendorAddressRequest;
import com.cmg.idsmed.model.entity.AbstractEntity;
import com.cmg.idsmed.model.entity.masterdata.Country;
import com.cmg.idsmed.model.entity.masterdata.Province;
import org.springframework.util.StringUtils;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * Status of this entity is using EntitySimpleStatusEnum
 *
 */

@Entity
@Table(name = "vendor_address")
public class VendorAddress extends AbstractEntity {

	private static final long serialVersionUID = -8369248221195641298L;

	public LocalDateTime getFromDate() {
		return fromDate;
	}

	public void setFromDate(LocalDateTime fromDate) {
		this.fromDate = fromDate;
	}

	public LocalDateTime getToDate() {
		return toDate;
	}

	public void setToDate(LocalDateTime toDate) {
		this.toDate = toDate;
	}

	public enum VendorAddressTypeEnum {
		ADDRESS(1, "address"),
		BILLING_ADDRESS(2, "billing address");
		private Integer code;
		private String name;
		VendorAddressTypeEnum(Integer code, String name) {
			this.code = code;
			this.name = name;
		}

		public Integer getCode() {
			return code;
		}

		public void setCode(Integer code) {
			this.code = code;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}
	}

	// We prefer sequence generator than identity. If you use identity it may
	// disable JDBC batching
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "vendor_address_id_seq")
	@SequenceGenerator(name = "vendor_address_id_seq", sequenceName = "vendor_address_id_seq", allocationSize = 1)
	@Column(updatable = false)
	private Long id;

	/**
	 *  Using VendorAddressTypeEnum
	 * 1 : address
	 * 2 : billing address
	 */
	@Column(name = "type")
	private Integer type = 1;

	@Column(name = "first_address")
	@Size(max = 1000)
	private String firstAddress;

	@Column(name = "second_address")
	@Size(max = 1000)
	private String secondAddress;

	@Column(name = "third_address")
	@Size(max = 1000)
	private String thirdAddress;

	@Column(name = "postcode")
	@Size(max = 200)
	private String postcode;

	@Column(name = "from_date")
	private LocalDateTime fromDate;

	@Column(name = "to_date")
	private LocalDateTime toDate;

	@ManyToOne
	@JoinColumn(name = "vendor_id")
	private Vendor vendor;

	@ManyToOne
	@JoinColumn(name = "country_id")
	private Country country;

	@ManyToOne
	@JoinColumn(name = "province_id")
	private Province province;

	public VendorAddress() {
	}

	public VendorAddress(VendorAddressRequest request) {
		this.type = request.getType();
		this.firstAddress = request.getFirstAddress();
		this.secondAddress = request.getSecondAddress();
		this.thirdAddress = request.getThirdAddress();
		this.postcode = request.getPostcode();
	}

	public Boolean updateVendorAddress(VendorAddressRequest request) {
		Boolean updated = false;
		if (request != null) {

			if (!StringUtils.isEmpty(request.getFirstAddress()) && !this.firstAddress.equals(request.getFirstAddress())) {
				this.firstAddress = request.getFirstAddress();
				updated = true;
			}

			if (!StringUtils.isEmpty(request.getSecondAddress())) {
				if (StringUtils.isEmpty(this.secondAddress)) {
					this.secondAddress = request.getSecondAddress();
					updated = true;
				} else {
					if (!this.secondAddress.equals(request.getSecondAddress())) {
						this.secondAddress = request.getSecondAddress();
						updated = true;
					}
				}
			} else {
				if (!StringUtils.isEmpty(this.secondAddress)) {
					this.secondAddress = request.getSecondAddress();
					updated = true;
				}
			}

			if (!StringUtils.isEmpty(request.getThirdAddress())) {
				if (StringUtils.isEmpty(this.thirdAddress)) {
					this.thirdAddress = request.getThirdAddress();
					updated = true;
				} else {
					if (!this.thirdAddress.equals(request.getThirdAddress())) {
						this.thirdAddress = request.getThirdAddress();
						updated = true;
					}
				}
			} else {
				if (!StringUtils.isEmpty(this.thirdAddress)) {
					this.thirdAddress = request.getThirdAddress();
					updated = true;
				}
			}

			if (!StringUtils.isEmpty(request.getPostcode())) {
				if (StringUtils.isEmpty(this.postcode)) {
					this.postcode = request.getPostcode();
					updated = true;
				} else {
					if (!this.postcode.equals(request.getPostcode())) {
						this.postcode = request.getPostcode();
						updated = true;
					}
				}
			} else {
				if (!StringUtils.isEmpty(this.postcode)) {
					this.postcode = request.getPostcode();
					updated = true;
				}
			}
		}

		if (updated == true) {
			this.setUpdatedDate(DateTimeHelper.getCurrentTimeUTC());
		}

		return  updated;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getFirstAddress() {
		return firstAddress;
	}

	public void setFirstAddress(String firstAddress) {
		this.firstAddress = firstAddress;
	}

	public String getSecondAddress() {
		return secondAddress;
	}

	public void setSecondAddress(String secondAddress) {
		this.secondAddress = secondAddress;
	}

	public String getThirdAddress() {
		return thirdAddress;
	}

	public void setThirdAddress(String thirdAddress) {
		this.thirdAddress = thirdAddress;
	}

	public String getPostcode() {
		return postcode;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	public Vendor getVendor() {
		return vendor;
	}

	public void setVendor(Vendor vendor) {
		this.vendor = vendor;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	public Province getProvince() {
		return province;
	}

	public void setProvince(Province province) {
		this.province = province;
	}
}

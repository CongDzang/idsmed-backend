package com.cmg.idsmed.model.entity.vendor;

import com.cmg.idsmed.dto.vendor.VendorFileAttachmentReminderRequest;

import javax.persistence.*;

/**
 * Status of this entity using EntitySimpleStatusEnum
 */
@Entity
@Table(name = "vendor_file_attachment_reminder")
public class VendorFileAttachmentReminder {

	private static final long serialVersionUID = -8369248221195641298L;

	// We prefer sequence generator than identity. If you use identity it may
	// disable JDBC batching
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "vendor_file_attachment_reminder_id_seq")
	@SequenceGenerator(name = "vendor_file_attachment_reminder_id_seq", sequenceName = "vendor_file_attachment_reminder_id_seq", allocationSize = 1)
	@Column(updatable = false)
	private Long id;

	@Column(name = "days_before")
	private Integer daysBefore;

	@Column(name = "days_after")
	private Integer daysAfter;

	@Column(name = "in_expiry_date")
	private Boolean inExpiryDate;

	@Column(name = "by_email")
	private Boolean byEmail;

	@Column(name = "by_web")
	private Boolean byWeb;

	@Column(name = "by_sms")
	private Boolean byeSMS;

	@ManyToOne
	@JoinColumn(name = "file_attachment_id")
	private FileAttachment fileAttachment;

	public VendorFileAttachmentReminder() {
	}

	public VendorFileAttachmentReminder(VendorFileAttachmentReminderRequest request) {
		this.daysAfter = request.getDaysAfter();
		this.daysBefore = request.getDaysBefore();
		this.inExpiryDate = request.getInExpiryDate();
	}


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getDaysBefore() {
		return daysBefore;
	}

	public void setDaysBefore(Integer daysBefore) {
		this.daysBefore = daysBefore;
	}

	public Integer getDaysAfter() {
		return daysAfter;
	}

	public void setDaysAfter(Integer daysAfter) {
		this.daysAfter = daysAfter;
	}

	public Boolean getInExpiryDate() {
		return inExpiryDate;
	}

	public void setInExpiryDate(Boolean inExpiryDate) {
		this.inExpiryDate = inExpiryDate;
	}

	public Boolean getByEmail() {
		return byEmail;
	}

	public void setByEmail(Boolean byEmail) {
		this.byEmail = byEmail;
	}

	public Boolean getByWeb() {
		return byWeb;
	}

	public void setByWeb(Boolean byWeb) {
		this.byWeb = byWeb;
	}

	public Boolean getByeSMS() {
		return byeSMS;
	}

	public void setByeSMS(Boolean byeSMS) {
		this.byeSMS = byeSMS;
	}

	public FileAttachment getFileAttachment() {
		return fileAttachment;
	}

	public void setFileAttachment(FileAttachment fileAttachment) {
		this.fileAttachment = fileAttachment;
	}
}

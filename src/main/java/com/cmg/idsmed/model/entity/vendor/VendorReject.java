package com.cmg.idsmed.model.entity.vendor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.cmg.idsmed.model.entity.AbstractEntity;

@Entity
@Table(name = "vendor_reject")
public class VendorReject extends AbstractEntity {

    //We prefer sequence generator than identity. If you use identity it may disable JDBC batching
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "vendor_reject_id_seq")
    @SequenceGenerator(name = "vendor_reject_id_seq",
            sequenceName = "vendor_reject_id_seq",
            allocationSize = 1)
    @Column(updatable = false)
    private Long id;
    
    @ManyToOne
    @JoinColumn(name = "vendor_id")
    private Vendor vendor;

    @Column(name = "remarks")
    @Size(max = 3000)
    private String remarks;

    @Column(name = "country_code")
    @NotNull
    @Size(max = 10)
    private String countryCode;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

	public com.cmg.idsmed.model.entity.vendor.Vendor getVendor() {
		return vendor;
	}

	public void setVendor(com.cmg.idsmed.model.entity.vendor.Vendor vendor) {
		this.vendor = vendor;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
}

package com.cmg.idsmed.model.entity.vendor.config;

import com.cmg.idsmed.dto.vendor.VendorInfoConfigRequest;
import com.cmg.idsmed.model.entity.AbstractEntity;
import com.cmg.idsmed.model.entity.masterdata.Country;
import com.cmg.idsmed.model.entity.masterdata.Province;
import org.springframework.beans.BeanUtils;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "vendor_info_config")
public class VendorInfoConfig extends AbstractEntity {

	public enum VendorInfoConfigStatusEnum {
		ACTIVE(1, "active"),
		INACTIVE(10, "inactive");
		private Integer code;
		private String name;
		VendorInfoConfigStatusEnum(Integer code, String name) {
			this.code = code;
			this.name = name;
		}

		public Integer getCode() {
			return code;
		}

		public void setCode(Integer code) {
			this.code = code;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}
	}
	private static final long serialVersionUID = 5771796917019428678L;

	//We prefer sequence generator than identity. If you use identity it may disable JDBC batching
	@Id
	@GeneratedValue(strategy= GenerationType.SEQUENCE, generator = "vendor_info_config_id_seq")
	@SequenceGenerator(name = "vendor_info_config_id_seq",
			sequenceName = "vendor_info_config_id_seq",
			allocationSize = 1)
	@Column(updatable = false)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "country_id")
	private Country country;

	@ManyToOne
	@JoinColumn(name = "province_id")
	private Province province;

	@OneToMany(mappedBy = "vendorInfoConfig")
	private List<VendorLabelConfig> vendorLabelConfigs;

	/**
	 * 1 - local
	 * 2 - oversea
	 * LocationTypeEnum
	 */
	@Column(name = "location")
	private Integer location;

	/**
	 * VendorTypeEnum
	 */
	@Column(name = "vendor_type")
	private Integer vendorType;

	public VendorInfoConfig() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	public Province getProvince() {
		return province;
	}

	public void setProvince(Province province) {
		this.province = province;
	}

	public List<VendorLabelConfig> getVendorLabelConfigs() {
		return vendorLabelConfigs;
	}

	public void setVendorLabelConfigs(List<VendorLabelConfig> vendorLabelConfigs) {
		this.vendorLabelConfigs = vendorLabelConfigs;
	}

	public Integer getLocation() {
		return location;
	}

	public void setLocation(Integer location) {
		this.location = location;
	}

	public Integer getVendorType() {
		return vendorType;
	}

	public void setVendorType(Integer vendorType) {
		this.vendorType = vendorType;
	}
}

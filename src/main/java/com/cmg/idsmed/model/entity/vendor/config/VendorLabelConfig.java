package com.cmg.idsmed.model.entity.vendor.config;

import com.cmg.idsmed.dto.vendor.VendorLabelConfigRequest;
import com.cmg.idsmed.model.entity.AbstractEntity;
import com.cmg.idsmed.model.entity.product.config.ProductInfoConfig;
import com.cmg.idsmed.model.entity.vendor.FileAttachment;
import com.cmg.idsmed.model.entity.vendor.VendorAdditionalInfo;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * Status of this entity will use VendorInfoConfigStatusEnum
 * pls go to see details.
 */
@Entity
@Table(name = "vendor_label_config")
public class VendorLabelConfig extends AbstractEntity {

	private static final long serialVersionUID = 5771796917019428678L;

	public enum VendorLabelConfigTypeEnum {
		DOCUMENT_ATTACHMENT(1, "document attachment"),
		ADDITIONAL_INFO(2, "additional info");
		private Integer code;
		private String name;
		VendorLabelConfigTypeEnum(Integer code, String name) {
			this.code = code;
			this.name = name;
		}
	}

	//We prefer sequence generator than identity. If you use identity it may disable JDBC batching
	@Id
	@GeneratedValue(strategy= GenerationType.SEQUENCE, generator = "vendor_label_config_id_seq")
	@SequenceGenerator(name = "vendor_label_config_id_seq",
			sequenceName = "vendor_label_config_id_seq",
			allocationSize = 1)
	@Column(updatable = false)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "vendor_info_config_id")
	private VendorInfoConfig vendorInfoConfig;

	@Column(name = "label_primary_name")
	@Size(max = 500)
	private String labelPrimaryName;

	@Column(name = "label_secondary_name")
	@Size(max = 500)
	private String labelSecondaryName;

	@Column(name = "is_mandatory")
	private Boolean isMandatory = false;

	/**
	 * This column determine this label has expiry date or not
	 */
	@Column(name = "is_expired")
	private Boolean isExpired = false;

	/**
	 *VendorLabelConfigTypeEnum
	 */
	@Column(name = "type")
	private Integer type;

	@Column(name = "sequence")
	private Integer sequence;

	@OneToMany(mappedBy = "vendorLabelConfig")
	private List<FileAttachment> fileAttachments;

	@OneToMany(mappedBy = "vendorLabelConfig")
	private List<VendorAdditionalInfo> vendorAdditionalInfos;

	public VendorLabelConfig() {

	}

	public VendorLabelConfig(VendorLabelConfigRequest request) {
		this.labelPrimaryName = request.getLabelPrimaryName();
		this.labelSecondaryName = request.getLabelSecondaryName();
		this.isMandatory = request.getMandatory();
		this.isExpired = request.getExpired();
		this.type = request.getType();
		this.sequence = request.getSequence();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public VendorInfoConfig getVendorInfoConfig() {
		return vendorInfoConfig;
	}

	public void setVendorInfoConfig(VendorInfoConfig vendorInfoConfig) {
		this.vendorInfoConfig = vendorInfoConfig;
	}

	public String getLabelPrimaryName() {
		return labelPrimaryName;
	}

	public void setLabelPrimaryName(String labelPrimaryName) {
		this.labelPrimaryName = labelPrimaryName;
	}

	public String getLabelSecondaryName() {
		return labelSecondaryName;
	}

	public void setLabelSecondaryName(String labelSecondaryName) {
		this.labelSecondaryName = labelSecondaryName;
	}

	public Boolean getMandatory() {
		return isMandatory;
	}

	public void setMandatory(Boolean mandatory) {
		isMandatory = mandatory;
	}

	public Boolean getExpired() {
		return isExpired;
	}

	public void setExpired(Boolean expired) {
		isExpired = expired;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getSequence() {
		return sequence;
	}

	public void setSequence(Integer sequence) {
		this.sequence = sequence;
	}

	public List<FileAttachment> getFileAttachments() {
		return fileAttachments;
	}

	public void setFileAttachments(List<FileAttachment> fileAttachments) {
		this.fileAttachments = fileAttachments;
	}

	public List<VendorAdditionalInfo> getVendorAdditionalInfos() {
		return vendorAdditionalInfos;
	}

	public void setVendorAdditionalInfos(List<VendorAdditionalInfo> vendorAdditionalInfos) {
		this.vendorAdditionalInfos = vendorAdditionalInfos;
	}
}

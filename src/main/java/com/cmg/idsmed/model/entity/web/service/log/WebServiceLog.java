package com.cmg.idsmed.model.entity.web.service.log;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.cmg.idsmed.model.entity.AbstractEntity;
import com.cmg.idsmed.model.entity.thirdparty.Customer;

import java.util.List;

@Entity
@Table(name = "webservice_log")
public class WebServiceLog extends AbstractEntity {

    /**
	 * 
	 */
	private static final long serialVersionUID = -9104754122716248723L;

	//We prefer sequence generator than identity. If you use identity it may disable JDBC batching
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "webservice_log_id_seq")
    @SequenceGenerator(name = "webservice_log_id_seq",
            sequenceName = "webservice_log_id_seq",
            allocationSize = 1)
    @Column(updatable = false)
    private Long id;

    @Column(name = "method")
    @NotNull
    @Size(max = 500)
    private String method;

    @Column(name = "ip_add")
    @Size(max = 16)
    private String ipAdd;
    
    @Column(name = "parameter")
    @Size(max = 1000)
    private String parameter;

    public WebServiceLog(String ipAdd, String method, String parameter, Integer status) {
    	this.ipAdd = ipAdd;
    	this.method = method;
    	this.parameter = parameter;
    	this.setStatus(status);
    }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	public String getMethod() {
		return method;
	}

	public void setTokenString(String method) {
		this.method = method;
	}

	public String getIpAdd() {
		return ipAdd;
	}

	public void setIpAdd(String ipAdd) {
		this.ipAdd = ipAdd;
	}

	public String getParameter() {
		return parameter;
	}

	public void setParameter(String parameter) {
		this.parameter = parameter;
	}
}

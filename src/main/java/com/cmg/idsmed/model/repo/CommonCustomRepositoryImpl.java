package com.cmg.idsmed.model.repo;

import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;

import javax.persistence.EntityManager;
import java.io.Serializable;

public class CommonCustomRepositoryImpl<T, ID extends Serializable> extends SimpleJpaRepository<T, ID>
        implements CommonCustomRepository<T, ID> {

    private EntityManager entityManager;

    public CommonCustomRepositoryImpl(JpaEntityInformation jpaEntity, EntityManager entityManager) {
        super(jpaEntity, entityManager);
        this.entityManager = entityManager;
    }
}

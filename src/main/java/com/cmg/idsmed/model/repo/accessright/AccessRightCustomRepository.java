package com.cmg.idsmed.model.repo.accessright;

import java.util.List;

import org.springframework.data.util.Pair;

import com.cmg.idsmed.model.entity.auth.IdsmedRole;

public interface AccessRightCustomRepository {
	Pair<Integer, List<IdsmedRole>> searchRoleByCustomCondition(String name, Integer status, Integer roleType, Integer pageIndex, Integer pageSize);
}

package com.cmg.idsmed.model.repo.accessright;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import com.cmg.idsmed.common.utils.JPQLHelper;
import com.cmg.idsmed.service.share.IdsmedServiceHelper;
import org.apache.poi.util.SystemOutLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Pair;
import org.springframework.util.StringUtils;

import com.cmg.idsmed.model.entity.auth.IdsmedRole;

public class AccessRightCustomRepositoryImpl implements AccessRightCustomRepository {

	@PersistenceContext
	private EntityManager em;

	@Override
	public Pair<Integer, List<IdsmedRole>> searchRoleByCustomCondition(String name, Integer status, Integer roleType, Integer pageIndex,
			Integer pageSize) {
		// TODO Auto-generated method stub
		StringBuilder jpql = new StringBuilder();

		jpql.append("SELECT r FROM IdsmedRole r");
		
		Map<String, Object> mapCheckExist = new HashMap<String, Object>();

		if (!StringUtils.isEmpty(name)) {
			mapCheckExist.put("name", name);
		}
		if (status != null) {
			mapCheckExist.put("status", status);
		}

		if (!mapCheckExist.isEmpty()) {
			jpql.append(" WHERE ");
		}

		jpql = JPQLHelper.generateJPQLStatement(mapCheckExist, jpql);

		if(roleType != null) {
			if (!mapCheckExist.isEmpty()) {
				jpql.append(" AND r.roleType = :roleType ");
				jpql.append(" ORDER BY r.createdDate DESC");
			}
			else {
				jpql.append(" WHERE r.roleType = :roleType ");
				jpql.append(" ORDER BY r.createdDate DESC");
			}
		}
		jpql.append(" ORDER BY r.createdDate DESC");
		TypedQuery<IdsmedRole> query = em.createQuery(jpql.toString(), IdsmedRole.class);
		if(roleType != null) {
			query.setParameter("roleType", roleType);
		}

		if(status != null){
			query.setParameter("status", status);
		}

		Integer totalCount = query.getResultList().size();

		if (pageIndex != null && pageSize != null) {
			query.setFirstResult(pageIndex*pageSize);
			query.setMaxResults(pageSize);
		}

		Pair<Integer, List<IdsmedRole>> result = Pair.of(totalCount, query.getResultList());
		return result;
	}
}

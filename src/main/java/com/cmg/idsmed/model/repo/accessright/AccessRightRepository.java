package com.cmg.idsmed.model.repo.accessright;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import com.cmg.idsmed.model.entity.auth.IdsmedRole;

public interface AccessRightRepository extends JpaRepository<IdsmedRole, Long>, JpaSpecificationExecutor<IdsmedRole>, AccessRightCustomRepository {
	Optional<IdsmedRole> findByName(String name);
	@Query(value = "SELECT max(r.id) FROM IdsmedRole r")
	Long getMaxId();
	List<IdsmedRole> findByStatus(Integer status);
}

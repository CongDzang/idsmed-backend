package com.cmg.idsmed.model.repo.accessright;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import com.cmg.idsmed.model.entity.auth.IdsmedFunction;

public interface FunctionNameRepository extends JpaRepository<IdsmedFunction, Long>, JpaSpecificationExecutor<IdsmedFunction> {
	@Query(value = "SELECT if.name FROM IdsmedFunction if ORDER BY if.id")
	List<String> findFunctionName();
}

package com.cmg.idsmed.model.repo.account;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.cmg.idsmed.model.entity.auth.IdsmedAccountSettings;

public interface IdsmedAccountSettingsRepository extends JpaRepository<IdsmedAccountSettings, Long>, JpaSpecificationExecutor<IdsmedAccountSettings> {
}

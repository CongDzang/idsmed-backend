package com.cmg.idsmed.model.repo.auth;

import com.cmg.idsmed.model.entity.auth.IdsmedAccount;

import java.util.List;

public interface IdsmedAccountCustomeRepository {
	List<IdsmedAccount> findAccountsByCompanyCode(String companyCode);
	List<IdsmedAccount> findAllAccountByPermissionCode(String permissionCode);

	List<IdsmedAccount> findAccountsByRoleCode(String roleCode);
	List<IdsmedAccount> findAccountsByCompanyCodeAndEmails(String companyCode, List<String> email);
	List<IdsmedAccount> findByVendorCoordinator(Boolean isVendorCoordinator);
}

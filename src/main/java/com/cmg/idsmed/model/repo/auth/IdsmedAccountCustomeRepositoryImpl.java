package com.cmg.idsmed.model.repo.auth;

import com.cmg.idsmed.common.enums.StatusEnum;
import com.cmg.idsmed.model.entity.auth.IdsmedAccount;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

public class IdsmedAccountCustomeRepositoryImpl implements IdsmedAccountCustomeRepository {
	@PersistenceContext
	private EntityManager em;

	public List<IdsmedAccount> findAccountsByCompanyCode(String companyCode) {
		StringBuilder jpql = new StringBuilder();

		jpql.append("SELECT acc from IdsmedAccount acc JOIN acc.idsmedUser u WHERE u.companyCode = :companyCode");

		TypedQuery<IdsmedAccount> query = em.createQuery(jpql.toString(), IdsmedAccount.class);

		query.setParameter("companyCode", companyCode);

		return query.getResultList();
	}

	public List<IdsmedAccount> findAccountsByCompanyCodeAndEmails(String companyCode, List<String> email) {
		StringBuilder jpql = new StringBuilder();

		jpql.append("SELECT acc from IdsmedAccount acc JOIN acc.idsmedUser u WHERE u.companyCode = :companyCode AND u.email IN (:email)");

		TypedQuery<IdsmedAccount> query = em.createQuery(jpql.toString(), IdsmedAccount.class);

		query.setParameter("companyCode", companyCode);
		query.setParameter("email", email);

		return query.getResultList();
	}

	@Override
	public List<IdsmedAccount> findByVendorCoordinator(Boolean isVendorCoordinator) {
		StringBuilder jpql = new StringBuilder();
		Integer approvedStatus = StatusEnum.APPROVED.getCode();
		Integer inactiveStatus = StatusEnum.INACTIVE.getCode();
		jpql.append("SELECT acc from IdsmedAccount acc JOIN acc.idsmedUser u WHERE acc.status in (:activeStatus, :inactiveStatus) AND acc.vendorCoordinator = :isVendorCoordinator");

		TypedQuery<IdsmedAccount> query = em.createQuery(jpql.toString(), IdsmedAccount.class);

		query.setParameter("activeStatus", approvedStatus);
		query.setParameter("inactiveStatus", inactiveStatus);
		query.setParameter("isVendorCoordinator", isVendorCoordinator);

		return query.getResultList();
	}

	public List<IdsmedAccount> findAllAccountByPermissionCode(String permissionCode) {
		StringBuilder jpql = new StringBuilder();

		jpql.append("SELECT acc FROM IdsmedAccount acc WHERE  acc.idsmedUser " +
				"IN (SELECT iu FROM IdsmedUserRole iur JOIN iur.idsmedRole irole JOIN iur.idsmedUser iu WHERE irole " +
				"IN (SELECT ir FROM IdsmedRolePermission irp JOIN irp.idsmedRole ir JOIN irp.idsmedPermission ip WHERE ip.code = :permissionCode))");

		TypedQuery<IdsmedAccount> query = em.createQuery(jpql.toString(), IdsmedAccount.class);

		query.setParameter("permissionCode", permissionCode);

		return query.getResultList();
	}

	public List<IdsmedAccount> findAccountsByRoleCode(String roleCode) {
		StringBuilder jpql = new StringBuilder();

		jpql.append("SELECT acc FROM IdsmedAccount acc WHERE  acc.idsmedUser " +
				"IN (SELECT iu FROM IdsmedUserRole iur JOIN iur.idsmedRole irole JOIN iur.idsmedUser iu WHERE irole.code = :roleCode) ");

		TypedQuery<IdsmedAccount> query = em.createQuery(jpql.toString(), IdsmedAccount.class);

		query.setParameter("roleCode", roleCode);

		return query.getResultList();
	}

}

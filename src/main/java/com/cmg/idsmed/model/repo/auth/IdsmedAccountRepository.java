package com.cmg.idsmed.model.repo.auth;

import com.cmg.idsmed.model.entity.auth.IdsmedAccount;
import com.cmg.idsmed.model.entity.auth.IdsmedUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface IdsmedAccountRepository extends JpaRepository<IdsmedAccount, Long>, JpaSpecificationExecutor<IdsmedAccount>, IdsmedAccountCustomeRepository {
        IdsmedAccount findByLoginId(String loginId);
        IdsmedAccount findByLoginIdAndStatus(String loginId, Integer status);
        List<IdsmedAccount> findAllByIdsmedUser(IdsmedUser user);
        IdsmedAccount findFirstByIdsmedUser(IdsmedUser user);
}

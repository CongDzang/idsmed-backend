package com.cmg.idsmed.model.repo.auth;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.cmg.idsmed.model.entity.auth.IdsmedPermission;

public interface IdsmedFunctionRepository extends JpaRepository<IdsmedPermission, Long>, JpaSpecificationExecutor<IdsmedPermission> {
		
}

package com.cmg.idsmed.model.repo.auth;

import java.util.List;

import com.cmg.idsmed.model.entity.auth.IdsmedPermission;
import com.cmg.idsmed.model.entity.auth.IdsmedRole;
import com.cmg.idsmed.model.entity.auth.IdsmedUser;

public interface IdsmedPermissionCustomRepository {
    List<IdsmedPermission> findAllPermissionsByRole(IdsmedRole role);
    public List<IdsmedPermission> findAllByUser(IdsmedUser user);
}

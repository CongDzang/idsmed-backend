package com.cmg.idsmed.model.repo.auth;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import com.cmg.idsmed.common.enums.StatusEnum;
import com.cmg.idsmed.model.entity.auth.IdsmedPermission;
import com.cmg.idsmed.model.entity.auth.IdsmedRole;
import com.cmg.idsmed.model.entity.auth.IdsmedUser;

public class IdsmedPermissionCustomRepositoryImpl implements IdsmedPermissionCustomRepository {

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<IdsmedPermission> findAllPermissionsByRole(IdsmedRole role) {

        StringBuilder jpql = new StringBuilder();

        jpql.append("SELECT ip FROM IdsmedPermission ip JOIN IdsmedRolePermission irp ON irp.idsmedPermission = ip ");

        if (role != null) {
            jpql.append("WHERE irp.idsmedRole = :role");
        }

        TypedQuery<IdsmedPermission> query = em.createQuery(jpql.toString(), IdsmedPermission.class);

        if (role != null) {
            query.setParameter("role", role);
        }
        return query.getResultList();
    }

    @Override
    public List<IdsmedPermission> findAllByUser(IdsmedUser user) {
        StringBuilder jpql = new StringBuilder();
        Integer approvedStatusCode = StatusEnum.APPROVED.getCode();
        jpql.append("SELECT ip FROM IdsmedRolePermission ipr INNER JOIN ipr.idsmedPermission ip " +
                "WHERE ipr.idsmedRole IN (SELECT iur.idsmedRole FROM IdsmedUserRole iur INNER JOIN iur.IdsmedUser iu WHERE iu =:user AND iur.idsmedRole.status = :approvedStatusCode) AND ip.status = :approvedStatusCode");

        TypedQuery<IdsmedPermission> query = em.createQuery(jpql.toString(), IdsmedPermission.class);
        query.setParameter("user", user);
        return query.getResultList();
    }
}

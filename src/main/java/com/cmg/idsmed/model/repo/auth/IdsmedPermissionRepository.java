package com.cmg.idsmed.model.repo.auth;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.cmg.idsmed.model.entity.auth.IdsmedPermission;

public interface IdsmedPermissionRepository extends JpaRepository<IdsmedPermission, Long>, JpaSpecificationExecutor<IdsmedPermission>, IdsmedPermissionCustomRepository {
    IdsmedPermission findFirstByCode(String code);
		
}

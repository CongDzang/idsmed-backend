package com.cmg.idsmed.model.repo.auth;

import com.cmg.idsmed.model.entity.auth.IdsmedUser;
import com.cmg.idsmed.model.entity.auth.IdsmedRole;
import com.cmg.idsmed.model.entity.subscriber.Subscriber;

import java.util.List;

public interface IdsmedRoleCustomRepository {
    List<IdsmedRole> findAllRoleByUserAndStatus(IdsmedUser user, Integer status);
    List<IdsmedRole> findAllRoleByStatusAndType(Integer roleType, Integer status);
    List<IdsmedRole> findAllRoleBySubscriber(Subscriber subscriber);
}

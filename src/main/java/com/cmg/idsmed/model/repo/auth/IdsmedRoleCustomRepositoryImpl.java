package com.cmg.idsmed.model.repo.auth;

import com.cmg.idsmed.model.entity.auth.IdsmedUser;
import com.cmg.idsmed.model.entity.auth.IdsmedRole;
import com.cmg.idsmed.model.entity.subscriber.Subscriber;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

public class IdsmedRoleCustomRepositoryImpl implements IdsmedRoleCustomRepository {

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<IdsmedRole> findAllRoleByUserAndStatus(IdsmedUser user, Integer status) {

        StringBuilder jpql = new StringBuilder();

        jpql.append("SELECT ir FROM IdsmedRole ir JOIN IdsmedUserRole iur ON iur.idsmedRole = ir ");

        if (user != null) {
            jpql.append("WHERE iur.idsmedUser = :user ");
        }

        if (status != null) {
            jpql.append("AND ir.status = :status");
        }

        TypedQuery<IdsmedRole> query = em.createQuery(jpql.toString(), IdsmedRole.class);

        if (user != null) {
            query.setParameter("user", user);
        }

        if (status != null) {
            query.setParameter("status", status);
        }
        return query.getResultList();
    }

    @Override
    public List<IdsmedRole> findAllRoleBySubscriber(Subscriber subscriber) {
        StringBuilder jpql = new StringBuilder();
        jpql.append("SELECT ir from IdsmedRole ir JOIN IdsmedSubscriberRole isr ON isr.idsmedRole = ir ");
        if (subscriber != null) {
            jpql.append("WHERE isr.subscriber = :subscriber");
        }

        TypedQuery<IdsmedRole> query = em.createQuery(jpql.toString(), IdsmedRole.class);

        if (subscriber != null) {
            query.setParameter("subscriber", subscriber);
        }

        return query.getResultList();
    }

    @Override
    public List<IdsmedRole> findAllRoleByStatusAndType(Integer roleType, Integer status) {
        StringBuilder jpql = new StringBuilder();

        jpql.append("SELECT ir FROM IdsmedRole ir ");

        if (status != null) {
            jpql.append("WHERE ir.status = :status ");
        }

        if (roleType != null) {
            jpql.append("AND ir.roleType = :roleType");
        }

        TypedQuery<IdsmedRole> query = em.createQuery(jpql.toString(), IdsmedRole.class);

        if (status != null) {
            query.setParameter("status", status);
        }

        if (roleType != null) {
            query.setParameter("roleType", roleType);
        }
        return query.getResultList();
    }
}

package com.cmg.idsmed.model.repo.auth;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.cmg.idsmed.model.entity.auth.IdsmedPermission;
import com.cmg.idsmed.model.entity.auth.IdsmedRole;
import com.cmg.idsmed.model.entity.auth.IdsmedRolePermission;

public interface IdsmedRolePermissionRepository extends JpaRepository<IdsmedRolePermission, Long>, JpaSpecificationExecutor<IdsmedRolePermission> {

    List<IdsmedRolePermission> findAllByIdsmedRole(IdsmedRole role);
    List<IdsmedRolePermission> findAllByIdsmedPermission(IdsmedPermission permission);

}

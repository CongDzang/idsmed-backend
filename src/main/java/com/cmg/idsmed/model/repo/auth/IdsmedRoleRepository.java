package com.cmg.idsmed.model.repo.auth;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import com.cmg.idsmed.model.entity.auth.IdsmedRole;

public interface IdsmedRoleRepository extends JpaRepository<IdsmedRole, Long>, JpaSpecificationExecutor<IdsmedRole>, IdsmedRoleCustomRepository {
	@Query("SELECT r FROM IdsmedRole r WHERE r.status='3' AND r.name NOT IN ('We Doctor Role')")
	List<IdsmedRole> findAllByStatus();

	IdsmedRole findByCode(String code);
}

package com.cmg.idsmed.model.repo.auth;

import com.cmg.idsmed.model.entity.auth.IdsmedRole;
import com.cmg.idsmed.model.entity.subscriber.SubscriberIp;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface IdsmedSubscriberIpRepository extends JpaRepository<SubscriberIp, Long>, JpaSpecificationExecutor<SubscriberIp> {
}

package com.cmg.idsmed.model.repo.auth;

import com.cmg.idsmed.model.entity.auth.IdsmedRole;
import com.cmg.idsmed.model.entity.auth.IdsmedUser;
import com.cmg.idsmed.model.entity.auth.IdsmedUserRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface IdsmedUserRoleRepository extends JpaRepository<IdsmedUserRole, Long>, JpaSpecificationExecutor<IdsmedUserRole> {

    List<IdsmedUserRole> findAllByIdsmedUser(IdsmedUser user);
    List<IdsmedUserRole> findAllByIdsmedRole(IdsmedRole role);
}

package com.cmg.idsmed.model.repo.common;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.cmg.idsmed.model.entity.common.MailSendingLog;

public interface MailSendingLogRepository extends JpaRepository<MailSendingLog, Long>, JpaSpecificationExecutor<MailSendingLog> {
}

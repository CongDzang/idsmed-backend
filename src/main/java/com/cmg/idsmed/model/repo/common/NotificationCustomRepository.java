package com.cmg.idsmed.model.repo.common;

import com.cmg.idsmed.model.entity.auth.IdsmedAccount;
import com.cmg.idsmed.model.entity.notification.Notification;

import java.util.List;

public interface NotificationCustomRepository {
	List<Notification> findAllNotArchivedNotificationByRecipient(IdsmedAccount account);
}

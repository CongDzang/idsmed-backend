package com.cmg.idsmed.model.repo.common;

import com.cmg.idsmed.model.entity.auth.IdsmedAccount;
import com.cmg.idsmed.model.entity.notification.Notification;
import com.cmg.idsmed.model.entity.notification.NotificationRecipient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

public class NotificationCustomRepositoryImpl implements NotificationCustomRepository {

	@PersistenceContext
	private EntityManager em;

	@Override
	public List<Notification> findAllNotArchivedNotificationByRecipient(IdsmedAccount account) {
		Integer status = NotificationRecipient.NotificationRecipientStatus.ARCHIVED.getCode();
		StringBuilder jpql = new StringBuilder();
		jpql.append("SELECT n from NotificationRecipient nr JOIN nr.notification n ");
		jpql.append(" WHERE nr.recipient = :account AND nr.status <> :status");
		jpql.append(" ORDER BY n.type, n.createdDate DESC");
		TypedQuery<Notification> query = em.createQuery(jpql.toString(), Notification.class);
		if (!StringUtils.isEmpty(account)) {
			query.setParameter("account", account);
		}

		if (status != null) {
			query.setParameter("status", status);
		}

		return query.getResultList();
	}
}


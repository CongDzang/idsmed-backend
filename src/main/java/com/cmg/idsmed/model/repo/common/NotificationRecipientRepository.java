package com.cmg.idsmed.model.repo.common;

import com.cmg.idsmed.model.entity.auth.IdsmedAccount;
import com.cmg.idsmed.model.entity.notification.Notification;
import com.cmg.idsmed.model.entity.notification.NotificationRecipient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface NotificationRecipientRepository extends JpaRepository<NotificationRecipient, Long>, JpaSpecificationExecutor<NotificationRecipient> {
	@Query("SELECT nr FROM NotificationRecipient nr JOIN nr.notification n WHERE n IN (:notifications) AND nr.recipient = :account AND nr.status <> :archivedStatus")
	List<NotificationRecipient> findAllNotAchivedNotificationRecipientByNotifsAndRecipient(@Param("notifications") List<Notification> notifications
			, @Param("account") IdsmedAccount account, @Param("archivedStatus") Integer archivedStatus);
}


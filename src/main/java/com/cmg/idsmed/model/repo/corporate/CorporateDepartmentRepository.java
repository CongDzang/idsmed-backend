package com.cmg.idsmed.model.repo.corporate;

import com.cmg.idsmed.model.entity.corporate.CorporateDepartment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface CorporateDepartmentRepository extends JpaRepository<CorporateDepartment, Long>, JpaSpecificationExecutor<CorporateDepartment> {
}

package com.cmg.idsmed.model.repo.corporate;

import com.cmg.idsmed.model.entity.corporate.Corporate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface CorporateRepository extends JpaRepository<Corporate, Long>, JpaSpecificationExecutor<Corporate> {
}

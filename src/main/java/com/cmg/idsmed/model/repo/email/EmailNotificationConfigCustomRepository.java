package com.cmg.idsmed.model.repo.email;

import com.cmg.idsmed.model.entity.email.EmailNotificationConfig;

import java.util.List;

public interface EmailNotificationConfigCustomRepository {
    List<EmailNotificationConfig> findAllOrderByNotificationName();
}

package com.cmg.idsmed.model.repo.email;

import com.cmg.idsmed.model.entity.email.EmailNotificationConfig;
import com.cmg.idsmed.model.entity.email.EmailTemplate;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

public class EmailNotificationConfigCustomRepositoryImpl implements EmailNotificationConfigCustomRepository {

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<EmailNotificationConfig> findAllOrderByNotificationName() {

        StringBuilder jpql = new StringBuilder();
        jpql.append("SELECT enc FROM EmailNotificationConfig enc ORDER BY enc.notificationName ASC ");

        TypedQuery<EmailNotificationConfig> query = em.createQuery(jpql.toString(), EmailNotificationConfig.class);

        List<EmailNotificationConfig> result = query.getResultList();

        return result;
    }
}

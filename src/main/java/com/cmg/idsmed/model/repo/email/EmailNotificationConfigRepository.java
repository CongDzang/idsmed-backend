package com.cmg.idsmed.model.repo.email;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.cmg.idsmed.common.enums.EmailTypeEnum;
import com.cmg.idsmed.model.entity.email.EmailNotificationConfig;

public interface EmailNotificationConfigRepository extends JpaRepository<EmailNotificationConfig, Long>, JpaSpecificationExecutor<EmailNotificationConfig>, EmailNotificationConfigCustomRepository {
	Optional<EmailNotificationConfig> findByEmailType(EmailTypeEnum emailType);
	EmailNotificationConfig findByEmailTemplateId(Long templateId);
}


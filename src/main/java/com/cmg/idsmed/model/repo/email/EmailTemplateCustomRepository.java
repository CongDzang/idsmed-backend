package com.cmg.idsmed.model.repo.email;

import com.cmg.idsmed.model.entity.email.EmailTemplate;
import org.springframework.data.util.Pair;

import java.util.List;

public interface EmailTemplateCustomRepository  {
    Pair<Integer, List<EmailTemplate>> searchEmailTemplateByCustomCondition(String templateName, String subject, Integer status, Integer pageIndex, Integer pageSize);
}

package com.cmg.idsmed.model.repo.email;

import com.cmg.idsmed.common.utils.JPQLHelper;
import com.cmg.idsmed.model.entity.email.EmailTemplate;
import com.cmg.idsmed.service.share.IdsmedServiceHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.util.Pair;
import org.springframework.util.StringUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EmailTemplateCustomRepositoryImpl implements EmailTemplateCustomRepository {

    @PersistenceContext
    private EntityManager em;

    @Override
    public Pair<Integer, List<EmailTemplate>> searchEmailTemplateByCustomCondition(
            String templateName, String subject, Integer status,
            Integer pageIndex, Integer pageSize) {

        StringBuilder jpql = new StringBuilder();
        jpql.append("SELECT et FROM EmailTemplate et");

        Map<String, Object> mapCheckExist = new HashMap<String, Object>();

        if (!StringUtils.isEmpty(templateName)) {
            mapCheckExist.put("templateName", templateName);
        }

        if (!StringUtils.isEmpty(subject)) {
            mapCheckExist.put("subject", subject);
        }

        if (status != null) {
            mapCheckExist.put("status", status);
        }

        if (!mapCheckExist.isEmpty()) {
            jpql.append(" WHERE ");
        }

        jpql = JPQLHelper.generateJPQLStatement(mapCheckExist, jpql);

        if (!StringUtils.isEmpty(jpql)) {
            jpql.append(" ORDER BY et.templateName ASC");
        }

        TypedQuery<EmailTemplate> query = em.createQuery(jpql.toString(), EmailTemplate.class);
        Integer totalCount = query.getResultList().size();

        if (pageIndex != null && pageSize != null) {
            query.setFirstResult(pageIndex*pageSize);
            query.setMaxResults(pageSize);
        }

        Pair<Integer, List<EmailTemplate>> result = Pair.of(totalCount, query.getResultList());

        return result;
    }
}

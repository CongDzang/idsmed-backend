package com.cmg.idsmed.model.repo.email;

import com.cmg.idsmed.model.entity.email.EmailTemplate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface EmailTemplateRepository extends JpaRepository<EmailTemplate, Long>, JpaSpecificationExecutor<EmailTemplate>, EmailTemplateCustomRepository {
    List<EmailTemplate> findByStatus(Integer id);
}

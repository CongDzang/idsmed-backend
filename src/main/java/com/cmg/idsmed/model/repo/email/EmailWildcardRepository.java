package com.cmg.idsmed.model.repo.email;

import com.cmg.idsmed.model.entity.email.EmailWildcard;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface EmailWildcardRepository extends JpaRepository<EmailWildcard, Long>, JpaSpecificationExecutor<EmailWildcard>, EmailTemplateCustomRepository  {
    List<EmailWildcard> findAllByOrderByWildcardNameAsc();
}

package com.cmg.idsmed.model.repo.error;

import com.cmg.idsmed.model.entity.error.LogErrorApi;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ApiErrorRepository extends JpaRepository<LogErrorApi, Long>
{
}
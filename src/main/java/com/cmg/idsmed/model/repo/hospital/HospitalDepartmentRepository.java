package com.cmg.idsmed.model.repo.hospital;

import com.cmg.idsmed.model.entity.hospital.Hospital;
import com.cmg.idsmed.model.entity.hospital.HospitalDepartment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface HospitalDepartmentRepository extends JpaRepository<HospitalDepartment, Long>, JpaSpecificationExecutor<HospitalDepartment> {
	List<HospitalDepartment> findAllByHospital(Hospital hospital);
}

package com.cmg.idsmed.model.repo.hospital;

import com.cmg.idsmed.model.entity.hospital.Hospital;
import com.cmg.idsmed.model.entity.product.Product;
import com.cmg.idsmed.model.repo.product.ProductCustomRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface HospitalRepository extends JpaRepository<Hospital, Long>, JpaSpecificationExecutor<Hospital> {
}

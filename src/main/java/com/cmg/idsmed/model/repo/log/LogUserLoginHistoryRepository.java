package com.cmg.idsmed.model.repo.log;

import com.cmg.idsmed.model.entity.log.LogUserLoginHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface LogUserLoginHistoryRepository extends JpaRepository<LogUserLoginHistory, Long>, JpaSpecificationExecutor<LogUserLoginHistory> {
}

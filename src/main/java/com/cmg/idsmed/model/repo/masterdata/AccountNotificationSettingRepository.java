package com.cmg.idsmed.model.repo.masterdata;

import com.cmg.idsmed.model.entity.auth.IdsmedAccount;
import com.cmg.idsmed.model.entity.masterdata.FavouriteProductBrand;
import com.cmg.idsmed.model.entity.notification.AccountNotificationSetting;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface AccountNotificationSettingRepository extends JpaRepository<AccountNotificationSetting, Long>, JpaSpecificationExecutor<AccountNotificationSetting> {
    List<AccountNotificationSetting> findAllByIdsmedAccount(IdsmedAccount idsmedAccount);
}

package com.cmg.idsmed.model.repo.masterdata;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.cmg.idsmed.model.entity.masterdata.CompanyBackgroundInfo;

public interface CompanyBackgroundInfoRepository  extends JpaRepository<CompanyBackgroundInfo, Long>, JpaSpecificationExecutor<CompanyBackgroundInfo> {
	Optional<CompanyBackgroundInfo> findByCompanyCode(String companyCode);
}

package com.cmg.idsmed.model.repo.masterdata;

import com.cmg.idsmed.model.entity.masterdata.Country;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CountryRepository extends JpaRepository<Country, Long>, JpaSpecificationExecutor<Country> {
	@Query("SELECT c FROM Country c WHERE c.status <> :inactiveStatus")
	List<Country> findAllNotInactiveCountry(@Param("inactiveStatus") Integer inactiveStatus );
	Country findByCountryCode(String manufacturerCountryCode);
}

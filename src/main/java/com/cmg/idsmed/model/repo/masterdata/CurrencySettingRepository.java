package com.cmg.idsmed.model.repo.masterdata;

import com.cmg.idsmed.model.entity.masterdata.CurrencySetting;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface CurrencySettingRepository extends JpaRepository<CurrencySetting, Long>, JpaSpecificationExecutor<CurrencySetting>  {
	CurrencySetting findFirstByCountryCodeAndStatus(String countryCode, Integer status);
}

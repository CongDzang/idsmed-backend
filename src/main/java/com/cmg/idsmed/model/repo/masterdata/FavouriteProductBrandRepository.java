package com.cmg.idsmed.model.repo.masterdata;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.cmg.idsmed.model.entity.masterdata.FavouriteProductBrand;

public interface FavouriteProductBrandRepository  extends JpaRepository<FavouriteProductBrand, Long>, JpaSpecificationExecutor<FavouriteProductBrand> {
	List<FavouriteProductBrand> findByUserId(Long userId);
	List<FavouriteProductBrand> findByWedoctorCustomerId(Long wedoctorCustomerId);
	List<FavouriteProductBrand> findByUserThirdParty(String userId);
}

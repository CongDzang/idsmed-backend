package com.cmg.idsmed.model.repo.masterdata;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.cmg.idsmed.model.entity.masterdata.FavouriteProductCareArea;

public interface FavouriteProductCareAreaRepository  extends JpaRepository<FavouriteProductCareArea, Long>, JpaSpecificationExecutor<FavouriteProductCareArea> {
	List<FavouriteProductCareArea> findByUserId(Long userId);
	List<FavouriteProductCareArea> findByWedoctorCustomerId(Long wedoctorCustomerId);
}

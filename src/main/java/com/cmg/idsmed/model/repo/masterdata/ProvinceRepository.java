package com.cmg.idsmed.model.repo.masterdata;

import com.cmg.idsmed.model.entity.masterdata.Province;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface ProvinceRepository extends JpaRepository<Province, Long>, JpaSpecificationExecutor<Province> {
    List<Province> findByProvinceCode(String manufactureProvinceCode);
    Province findByEnglishName(String provinceName);
}

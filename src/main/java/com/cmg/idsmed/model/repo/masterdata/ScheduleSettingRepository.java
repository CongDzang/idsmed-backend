package com.cmg.idsmed.model.repo.masterdata;

import com.cmg.idsmed.model.entity.masterdata.ScheduleSetting;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ScheduleSettingRepository extends JpaRepository<ScheduleSetting, Long>, JpaSpecificationExecutor<ScheduleSetting> {
	ScheduleSetting findFirstByScheduleTaskCode(String scheduleTaskCode);
}

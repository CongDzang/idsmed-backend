package com.cmg.idsmed.model.repo.masterdata;

import com.cmg.idsmed.model.entity.auth.IdsmedAccount;
import com.cmg.idsmed.model.entity.notification.AccountNotificationSetting;
import com.cmg.idsmed.model.entity.vendor.TenderCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface TenderCategoryRepository extends JpaRepository<TenderCategory, Long>, JpaSpecificationExecutor<TenderCategory> {
}

package com.cmg.idsmed.model.repo.masterdata;

import com.cmg.idsmed.model.entity.masterdata.VerifyingWorkFlowSetting;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface VerifyingWorkFlowSettingRepository extends JpaRepository<VerifyingWorkFlowSetting, Long>, JpaSpecificationExecutor<VerifyingWorkFlowSetting> {

	List<VerifyingWorkFlowSetting> findAllByCountryCodeAndStatus(String countryCode, Integer status);

}

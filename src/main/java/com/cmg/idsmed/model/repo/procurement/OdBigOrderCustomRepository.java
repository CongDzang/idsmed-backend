package com.cmg.idsmed.model.repo.procurement;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;

public interface OdBigOrderCustomRepository {
	BigDecimal countTotalSalesByCreatedDateBetweenFirstDayAndLastDay(LocalDateTime firstDayOfMonth, LocalDateTime lastDayOfMonth);

}

package com.cmg.idsmed.model.repo.procurement;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * @author congdang
 */

public class OdBigOrderCustomRepositoryImpl implements OdBigOrderCustomRepository {

	@PersistenceContext
	private EntityManager em;

	@Override
	public BigDecimal countTotalSalesByCreatedDateBetweenFirstDayAndLastDay(LocalDateTime firstDayOfMonth, LocalDateTime lastDayOfMonth) {
		StringBuilder jpql = new StringBuilder();
		jpql.append("SELECT SUM(obo.realPaidAmount)  FROM OdBigOrder obo");
		jpql.append(" WHERE obo.createdDate BETWEEN :firstDayOfMonth AND :lastDayOfMonth");

		Query query = em.createQuery(jpql.toString());
		query.setParameter("firstDayOfMonth", firstDayOfMonth);
		query.setParameter("lastDayOfMonth", lastDayOfMonth);

		return (BigDecimal) query.getSingleResult();
	}
}

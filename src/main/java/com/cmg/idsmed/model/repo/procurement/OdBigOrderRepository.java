package com.cmg.idsmed.model.repo.procurement;

import com.cmg.idsmed.model.entity.procurement.OdBigOrder;
import com.cmg.idsmed.model.entity.product.ProductBrand;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface OdBigOrderRepository extends JpaRepository<OdBigOrder, Long>, JpaSpecificationExecutor<OdBigOrder>, OdBigOrderCustomRepository {
    OdBigOrder findByBigOrderNo(String bigOrderNo);
}

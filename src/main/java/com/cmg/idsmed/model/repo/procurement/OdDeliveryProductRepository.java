package com.cmg.idsmed.model.repo.procurement;

import com.cmg.idsmed.model.entity.procurement.OdDeliveryProduct;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface OdDeliveryProductRepository  extends JpaRepository<OdDeliveryProduct, Long>, JpaSpecificationExecutor<OdDeliveryProduct> {
    List<OdDeliveryProduct> findByOdOrderDeliveryId(Long odOrderDeliveryId);
}

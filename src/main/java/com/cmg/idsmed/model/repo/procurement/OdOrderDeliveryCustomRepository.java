package com.cmg.idsmed.model.repo.procurement;

import java.math.BigDecimal;

public interface OdOrderDeliveryCustomRepository {
    BigDecimal sumOfTotalFreight(String smallOrderNo);
}

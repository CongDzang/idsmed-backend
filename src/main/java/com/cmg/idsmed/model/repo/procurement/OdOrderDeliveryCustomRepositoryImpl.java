package com.cmg.idsmed.model.repo.procurement;

import com.cmg.idsmed.model.entity.procurement.OdOrderDelivery;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigDecimal;

public class OdOrderDeliveryCustomRepositoryImpl implements OdOrderDeliveryCustomRepository {

    @PersistenceContext
    private EntityManager em;

    @Override
    public BigDecimal sumOfTotalFreight(String smallOrderNo) {

        StringBuilder jpql = new StringBuilder();

        jpql.append("SELECT COALESCE(SUM(od.freight), 0) FROM OdOrderDelivery od WHERE od.smallOrderNo = :smallOrderNo");

        Query query = em.createQuery(jpql.toString(), BigDecimal.class);
        if (smallOrderNo != null) {
            query.setParameter("smallOrderNo", smallOrderNo);
        }

        BigDecimal totalCount = (BigDecimal) query.getSingleResult();

        return totalCount;
    }
}

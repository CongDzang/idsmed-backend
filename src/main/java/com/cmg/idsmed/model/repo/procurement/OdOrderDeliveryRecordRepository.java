package com.cmg.idsmed.model.repo.procurement;

import com.cmg.idsmed.model.entity.procurement.OdBigOrder;
import com.cmg.idsmed.model.entity.procurement.OdOrderDeliveryRecord;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface OdOrderDeliveryRecordRepository extends JpaRepository<OdOrderDeliveryRecord, Long>, JpaSpecificationExecutor<OdOrderDeliveryRecord> {
    OdOrderDeliveryRecord findFirstByOdOrderDeliveryId(Long odOrderDeliveryId);
}

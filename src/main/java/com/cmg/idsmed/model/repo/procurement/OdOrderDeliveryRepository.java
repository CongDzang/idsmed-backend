package com.cmg.idsmed.model.repo.procurement;

import com.cmg.idsmed.model.entity.procurement.OdOrderDelivery;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface OdOrderDeliveryRepository extends JpaRepository<OdOrderDelivery, Long>, JpaSpecificationExecutor<OdOrderDelivery>, OdOrderDeliveryCustomRepository {
    List<OdOrderDelivery> findBySmallOrderNo(String smallOrderNo);
    OdOrderDelivery findFirstBySmallOrderNo(String smallOrderNo);
}

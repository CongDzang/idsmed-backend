package com.cmg.idsmed.model.repo.procurement;

import com.cmg.idsmed.model.entity.procurement.OdOrderItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface OdOrderItemRepository extends JpaRepository<OdOrderItem, Long>, JpaSpecificationExecutor<OdOrderItem> {
    List<OdOrderItem> findBySmallOrderNo(String smallOrderNo);
    OdOrderItem findBySmallOrderNoAndGoodsId(String smallOrderNo, Long goodsId);
    List<OdOrderItem> findBySmallOrderNoAndOrderStatus(String smallOrderNo, Integer orderStatus);
}

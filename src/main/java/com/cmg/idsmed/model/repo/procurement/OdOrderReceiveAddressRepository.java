package com.cmg.idsmed.model.repo.procurement;

import com.cmg.idsmed.model.entity.procurement.OdBigOrder;
import com.cmg.idsmed.model.entity.procurement.OdOrderReceiveAddress;
import com.cmg.idsmed.model.entity.procurement.OdSmallOrder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface OdOrderReceiveAddressRepository extends JpaRepository<OdOrderReceiveAddress, Long>, JpaSpecificationExecutor<OdOrderReceiveAddress> {
    OdOrderReceiveAddress findBySmallOrderNo(String smallOrderNo);
}

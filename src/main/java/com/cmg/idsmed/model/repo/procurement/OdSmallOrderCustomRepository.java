package com.cmg.idsmed.model.repo.procurement;

import com.cmg.idsmed.model.entity.masterdata.Province;
import com.cmg.idsmed.model.entity.procurement.OdSmallOrder;
import org.springframework.data.util.Pair;

import java.util.List;

public interface OdSmallOrderCustomRepository {
    // Merchant ID is Vendor ID
    Pair<Integer, List<OdSmallOrder>> findSmallOrderByMerchantId(Integer pageIndex, Integer pageSize, Long vendorId);
    Long countTotalNoOfTransactionFindByProvince(Province province);
    Long countTotalNoOfTransaction();
    Pair<Integer, List<OdSmallOrder>> findSmallOrder(Integer pageIndex, Integer pageSize);
}

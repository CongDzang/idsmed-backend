package com.cmg.idsmed.model.repo.procurement;

import com.cmg.idsmed.model.entity.masterdata.Province;
import com.cmg.idsmed.model.entity.procurement.OdSmallOrder;
import org.springframework.data.util.Pair;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.List;

public class OdSmallOrderCustomRepositoryImpl implements OdSmallOrderCustomRepository {

    @PersistenceContext
    private EntityManager em;

    @Override
    public Pair<Integer, List<OdSmallOrder>> findSmallOrderByMerchantId(Integer pageIndex, Integer pageSize, Long vendorId) {

        StringBuilder jpql = new StringBuilder();

        jpql.append("SELECT sm FROM OdSmallOrder sm ");

        // Merchant ID is Vendor ID
        if (vendorId != null) {
            jpql.append("WHERE sm.merchantId = :vendorId ");
        }

        jpql.append("ORDER BY sm.smallOrderNo ASC ");

        TypedQuery<OdSmallOrder> query = em.createQuery(jpql.toString(), OdSmallOrder.class);

        if (vendorId != null) {
            query.setParameter("vendorId", vendorId);
        }

        Integer totalCount = query.getResultList().size();

        if (pageIndex != null && pageSize != null) {
            query.setFirstResult(pageIndex * pageSize);
            query.setMaxResults(pageSize);
        }

        Pair<Integer, List<OdSmallOrder>> result = Pair.of(totalCount, query.getResultList());
        return result;
    }
     
    @Override
	public Long countTotalNoOfTransactionFindByProvince(Province province) {
		String provinceCode = province.getProvinceCode();

		StringBuilder jpql = new StringBuilder();
		jpql.append("SELECT COUNT(oso) FROM OdSmallOrder oso JOIN oso.odOrderReceiveAddress oora");
		jpql.append(" WHERE oora.provinceCode=:provinceCode");

		Query query = em.createQuery(jpql.toString());
		query.setParameter("provinceCode", provinceCode);

		return (Long) query.getSingleResult();
   }

	@Override
	public Long countTotalNoOfTransaction() {
		StringBuilder jpql = new StringBuilder();
		jpql.append("SELECT COUNT(oso) FROM OdSmallOrder oso ");

		Query query = em.createQuery(jpql.toString());

		return (Long) query.getSingleResult();
	}

    @Override
    public Pair<Integer, List<OdSmallOrder>> findSmallOrder(Integer pageIndex, Integer pageSize) {
        StringBuilder jpql = new StringBuilder();

        jpql.append("SELECT sm FROM OdSmallOrder sm ORDER BY sm.smallOrderNo ASC ");

        TypedQuery<OdSmallOrder> query = em.createQuery(jpql.toString(), OdSmallOrder.class);

        Integer totalCount = query.getResultList().size();

        if (pageIndex != null && pageSize != null) {
            query.setFirstResult(pageIndex * pageSize);
            query.setMaxResults(pageSize);
        }

        Pair<Integer, List<OdSmallOrder>> result = Pair.of(totalCount, query.getResultList());
        return result;
    }

}

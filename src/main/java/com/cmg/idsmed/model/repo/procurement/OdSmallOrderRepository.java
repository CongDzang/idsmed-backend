package com.cmg.idsmed.model.repo.procurement;

import com.cmg.idsmed.model.entity.procurement.OdBigOrder;
import com.cmg.idsmed.model.entity.procurement.OdSmallOrder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface OdSmallOrderRepository extends JpaRepository<OdSmallOrder, Long>, JpaSpecificationExecutor<OdSmallOrder>, OdSmallOrderCustomRepository {
    List<OdSmallOrder> findByBigOrderNo(String bigOrderNo);
    OdSmallOrder findBySmallOrderNo(String smallOrderNo);
    List<OdSmallOrder> findByBigOrderNoAndOrderStatus(String bigOrderNo, Integer orderStatus);
}

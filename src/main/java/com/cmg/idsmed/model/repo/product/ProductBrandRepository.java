package com.cmg.idsmed.model.repo.product;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.cmg.idsmed.model.entity.product.ProductBrand;

public interface ProductBrandRepository extends JpaRepository<ProductBrand, Long>, JpaSpecificationExecutor<ProductBrand> {
	ProductBrand findByBrandName(String brandName);
	ProductBrand findFirstByBrandCode(String brandCode);
	ProductBrand findFirstByBrandName(String brandName);
	Page<ProductBrand> findAllByStatus(Integer status, Pageable pageable);
}

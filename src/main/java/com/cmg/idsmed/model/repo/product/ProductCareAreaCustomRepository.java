package com.cmg.idsmed.model.repo.product;

import java.util.List;

import org.springframework.data.util.Pair;

import com.cmg.idsmed.model.entity.product.ProductCareArea;

public interface ProductCareAreaCustomRepository {
	Pair<Integer, List<ProductCareArea>> findByStatus(Integer status, Integer pageIndex, Integer pageSize);
}

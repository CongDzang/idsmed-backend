package com.cmg.idsmed.model.repo.product;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.data.util.Pair;

import com.cmg.idsmed.model.entity.product.ProductCareArea;

public class ProductCareAreaCustomRepositoryImpl  implements ProductCareAreaCustomRepository {

	@PersistenceContext
    private EntityManager em;
	
	@Override
	public Pair<Integer, List<ProductCareArea>> findByStatus(Integer status, Integer pageIndex, Integer pageSize) {
		StringBuilder jpql = new StringBuilder();

        jpql.append("SELECT pca FROM ProductCareArea pca WHERE pca.status = :status ORDER BY pca.careAreaName ASC ");


        TypedQuery<ProductCareArea> query = em.createQuery(jpql.toString(), ProductCareArea.class);
        if (status != null) {
        	query.setParameter("status", status);
        }

        Integer totalCount = query.getResultList().size();
        
        if (pageIndex != null && pageSize != null) {
        	query.setFirstResult(pageIndex*pageSize);
        	query.setMaxResults(pageSize);
        }
        
        Pair<Integer, List<ProductCareArea>> result = Pair.of(totalCount, query.getResultList());
        return result;
	}
	
}

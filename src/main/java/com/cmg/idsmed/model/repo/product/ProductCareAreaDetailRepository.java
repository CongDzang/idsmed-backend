package com.cmg.idsmed.model.repo.product;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.cmg.idsmed.model.entity.product.ProductCareAreaDetail;

public interface ProductCareAreaDetailRepository  extends JpaRepository<ProductCareAreaDetail, Long>, JpaSpecificationExecutor<ProductCareAreaDetail>{

}

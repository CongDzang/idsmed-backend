package com.cmg.idsmed.model.repo.product;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.cmg.idsmed.model.entity.product.ProductCareArea;

import java.util.List;

public interface ProductCareAreaRepository  extends JpaRepository<ProductCareArea, Long>, JpaSpecificationExecutor<ProductCareArea>, ProductCareAreaCustomRepository {
	ProductCareArea findFirstById(Long id);
	ProductCareArea findFirstByCareAreaName(String name);
	ProductCareArea findFirstByCareAreaNameOrCareAreaNameZhCnOrCareAreaNameZhTwOrCareAreaNameViOrCareAreaNameThOrCareAreaNameId(String englishName
			, String chineseName, String chineseTwName, String vietnameseName, String thaiName, String indonesiaName);
	List<ProductCareArea> findAllByStatusOrderByCareAreaNameAsc(Integer status);
}

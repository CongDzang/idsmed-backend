package com.cmg.idsmed.model.repo.product;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.cmg.idsmed.model.entity.product.ProductCategory;

public interface ProductCategoryRepository extends JpaRepository<ProductCategory, Long>, JpaSpecificationExecutor<ProductCategory> {
	ProductCategory findFirstByCategoryName(String categoryName);
	ProductCategory findFirstByCategoryNameOrCategoryNameZhCnOrCategoryNameZhTwOrCategoryNameViOrCategoryNameThOrCategoryNameId(String categoryName,
																																String categoryChineseName,
																																String categoryChineseTwName,
																																String categoryVietnameseName,
																																String categoryThaiName,
																																String categoryIndonesianName);
	Page<ProductCategory> findAllByStatus(Integer status, Pageable pageable);
}

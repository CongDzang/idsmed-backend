package com.cmg.idsmed.model.repo.product;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.cmg.idsmed.model.entity.auth.IdsmedAccount;
import com.cmg.idsmed.model.entity.masterdata.Province;
import com.cmg.idsmed.model.entity.product.*;
import com.cmg.idsmed.model.entity.product.search.KeywordHistory;
import org.springframework.data.domain.Pageable;
import org.springframework.data.util.Pair;

import com.cmg.idsmed.common.enums.StatusEnum;
import com.cmg.idsmed.model.entity.vendor.Vendor;

public interface ProductCustomRepository {
	Pair<Integer, List<Product>> searchProductByCustomCondition(Optional<Vendor> vendor, String productNamePrimary, Integer status,
			String productCode, String productModel, String companyNamePrimary, Integer vendorStatus, Long vendorCoordinatorId, Integer pageIndex, Integer pageSize);

	Pair<Integer, List<Product>> fuzzySearchProduct(String keyword, ProductCategory category, ProductBrand brand
			, String type, Integer lastNProduct, Pageable pageable, String sortField, String sortOrder);

	Pair<Integer, List<Product>> fuzzySearchAdvanceProductCareAreaSearchField(String keyword, ProductCareArea careArea
			, ProductCategory category, ProductBrand brand, String type, Integer lastNProduct
			, Pageable pageable, String sortField, String sortOrder);

	Pair<Integer, List<Product>> fuzzySearchProductV2(String keyword, List<ProductCategory> categories, List<ProductBrand> brands
			, Integer pageIndex, Integer pageSize, String sortField, String sortOrder, BigDecimal minPrice
			, BigDecimal maxPrice, Integer productType, String manufacturerCountryCode, String manufacturerProvinceCode);

	Pair<Integer, List<Product>> fuzzySearchAdvanceProductCareAreaSearchFieldV2(String keyword, List<ProductCareArea> careAreas, List<ProductCategory> categories, List<ProductBrand> brands
			, Integer pageIndex, Integer pageSize, String sortFieldName, String sortOrder, BigDecimal minPrice, BigDecimal maxPrice
			, Integer productType, String manufacturerCountryCode, String manufacturerProvinceCode
			, List<ProductSecondCategory> secondCategories);

	Pair<Integer, List<Product>> fuzzySearchProductV2ForWedoctor(String keyword, List<ProductCategory> categories, List<ProductBrand> brands
			, Integer pageIndex, Integer pageSize, String sortField, String sortOrder, BigDecimal minPrice, BigDecimal maxPrice);

	Pair<Integer, List<Product>> fuzzySearchAdvanceProductCareAreaSearchFieldV2ForWedoctor(String keyword, List<ProductCareArea> careAreas, List<ProductCategory> categories, List<ProductBrand> brands
			, Integer pageIndex, Integer pageSize, String sortFieldName, String sortOrder, BigDecimal minPrice, BigDecimal maxPrice);

	Long countTotalProductByVendorAndCreatedDate(List<Vendor> vendors, LocalDateTime approvedDate, Integer status);

	List<String> findAllProductModelByVendors(List<Vendor> vendors);

	Long countAllByProductCareArea(ProductCareArea productCareArea, List<StatusEnum> statusEnums);

	Long countAllByProductCategory(ProductCategory category, List<StatusEnum> statusEnums);

	Long countAllByProductSecondCategory(ProductSecondCategory productSecondCategory, List<StatusEnum> statusEnums);

	Pair<Integer, List<Product>> searchByUserFavourite (String keyword, List<Long> careAreaIds, List<Long> productBrandIds, Pageable pageable);
	
	Pair<Integer, List<Product>> findAllProductFilterByApproveProductOrderByApprovedDateDesc(Integer pageIndex, Integer pageSize);
	
	Pair<Integer, List<Product>> findAllProductBasedOnSearchHistory(Long clientId, String userId, Pageable pageable);

	Pair<Integer, List<Product>> findAllProductBasedOnBestSeller(Pageable pageable);

	Pair<Integer, List<ProductRatingDetail>> findProductRatingByCustomCondition(Product product, Double score, Integer pageIndex, Integer pageSize);

	Pair<Integer, List<Product>> findSuggestedProducts(Long customerId, Integer pageIndex, Integer pageSize);

	Long countTotalApprovedProductBetweenFirstDateAndLastDate(LocalDateTime firstDayOfMonth, LocalDateTime lastDayOfMonth);

	Long countTotalApprovedProductByVendorAddressFindByProvince(Province province);

	BigDecimal getMinPriceOfActiveProduct();

	BigDecimal getMaxPriceOfActiveProduct();

	List<Product> getTopFiveProductRating();

	List<Product> getLatestFiveApprovedProducts();

	List<Product> findAllApprovedProductHaveProductWishlist(IdsmedAccount idsmedAccount);
}

package com.cmg.idsmed.model.repo.product;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.*;
import javax.validation.constraints.Size;

import com.cmg.idsmed.common.enums.RatingEnum;
import com.cmg.idsmed.model.entity.auth.IdsmedAccount;
import com.cmg.idsmed.model.entity.masterdata.Province;
import com.cmg.idsmed.model.entity.product.*;
import com.cmg.idsmed.model.entity.product.search.KeywordHistory;
import com.cmg.idsmed.model.entity.vendor.VendorAddress;
import com.cmg.idsmed.service.product.ProductServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;
import org.springframework.data.util.Pair;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.cmg.idsmed.common.enums.StatusEnum;
import com.cmg.idsmed.common.utils.DateTimeHelper;
import com.cmg.idsmed.common.utils.StringProcessUtils;
import com.cmg.idsmed.model.entity.vendor.Vendor;

/**
 * @author congdang
 */

public class ProductCustomRepositoryImpl implements ProductCustomRepository {

	@PersistenceContext
	private EntityManager em;
	
	private static final String NEW_PRODUCT_SEARCH_TYPE = "new";
	private static final Integer DEFAULT_DAYS__OF_NEW_PRODUCT_DEFINITION = 7;
	private static final Integer DEFAULT_SIZE_HISTORY_USER_PRODUCT_BY_SEARCH = 20;
	private static final Integer DEFAULT_SIZE_BEST_SELLER_PRODUCT_GLOBAL = 20;
	private static final Integer NUMBER_OF_BIGGEST_PRODUCT_RATING = 5;
	private static final Integer NUMBER_OF_LATEST_APPROVED_PRODUCT = 5;
	private static final String DESCENDING = "desc";
	private static final String ASCENDING = "asc";
	private static final String NULL_FIRST = "NULLS FIRST";
	private static final String NULL_LAST = "NULLS LAST";
	private static final String BEST_SELLER_SEARCH_TYPE = "hot";

	public enum FuzzySearchProductSortFieldEnum {
		PRODUCT_PRIMARY_NAME(1, "productNamePrimary"),
		PRODUCT_PRICE(2, "price"),
		PRODUCT_APPROVED_DATE(3, "approvedDate"),
		AVERAGE_SCORE(4, "averageScore"),
		BEST_SELLER_PRODUCT_GLOBAL(5, "frequency");
		private Integer code;
		private String fieldName;
		FuzzySearchProductSortFieldEnum(Integer code, String fieldName) {
			this.code = code;
			this.fieldName = fieldName;
		}

		public Integer getCode() {
			return code;
		}

		public String getFieldName() {
			return fieldName;
		}
	}

	@Override
	public Pair<Integer, List<Product>> fuzzySearchProduct(String keyword, ProductCategory category, ProductBrand brand
			, String type, Integer lastNProduct, Pageable pageable, String sortField, String sortOrder) {
		Boolean hasKeyword = keyword == null || keyword.isEmpty() ? false : true;
		
		Boolean isSortParamExisted = StringUtils.isEmpty(sortField) || StringUtils.isEmpty(sortOrder) ? false : true;
		Boolean isValidSortOrder = !StringUtils.isEmpty(sortOrder) && (sortOrder.toUpperCase().equals("ASC") || sortOrder.toUpperCase().equals("DESC"));
		Boolean isValidSortParam = isSortParamExisted && isValidSortOrder;
		
		List<String> searchFields = new ArrayList<>();
		searchFields.add("productNamePrimary");
		searchFields.add("productModel");
		searchFields.add("productCode");
		searchFields.add("productDescription");
		searchFields.add("hashTag");
		Pair<String, String> pair = Pair.of("productBrand", "brandName");
		List<Pair<String, String>> relatedSearchFields = new ArrayList<>();
		relatedSearchFields.add(pair);
		StringBuilder jpql = new StringBuilder(StringProcessUtils.createFullTextSearchQuery("Product", searchFields, relatedSearchFields, hasKeyword));

		if (hasKeyword) {
			jpql.append(" AND e.status = ");
		} else {
			jpql.append(" WHERE e.status = ");
		}

		jpql.append(StatusEnum.APPROVED.getCode());

		if (category != null) {
			jpql.append(" AND e.productCategory = :productCategory ");
		}
		if (brand != null) {
			jpql.append(" AND e.productBrand = :productBrand ");
		}
		
		Boolean isLastNNewProductSearch = !StringUtils.isEmpty(type)
				&& NEW_PRODUCT_SEARCH_TYPE.equalsIgnoreCase(type) && lastNProduct != null ? true : false;
		
		if (isLastNNewProductSearch) {
			jpql.append(" AND e.approvedDate > :sevenDayBefore ORDER BY e.approvedDate DESC ");
		}
		
		if (isValidSortParam) {
			if (isLastNNewProductSearch) {
				//2 sort applied
				jpql.append(" , e." + sortField + " " + sortOrder);
			} else {
				//only applied sort by request param
				jpql.append(" ORDER BY e." + sortField + " " + sortOrder);
			}
		}
		
		TypedQuery<Product> query = em.createQuery(jpql.toString(), Product.class);

		if (!StringUtils.isEmpty(keyword)) {
			query.setParameter("keyword", keyword);
		}

		if (category != null) {
			query.setParameter("productCategory", category);
		}
		if (brand != null) {
			query.setParameter("productBrand", brand);
		}
		
		if (isLastNNewProductSearch) {
			Calendar cal = Calendar.getInstance();
			LocalDateTime currentDate = DateTimeHelper.getCurrentTimeUTC();
			query.setParameter("sevenDayBefore", DateTimeHelper.addTimeInUTC(currentDate, DateTimeHelper.HOUR, -24*DEFAULT_DAYS__OF_NEW_PRODUCT_DEFINITION));
		}
		
		Integer totalCount = query.getResultList().size();

		if (pageable != null) {
			if (!StringUtils.isEmpty(type) && NEW_PRODUCT_SEARCH_TYPE.equalsIgnoreCase(type) && lastNProduct != null) {
				Integer startIdx = pageable.getPageNumber() * pageable.getPageSize();
				startIdx = startIdx < lastNProduct ? startIdx : lastNProduct;
				Integer endIdx = startIdx + pageable.getPageSize();
				Integer maxResult = endIdx > lastNProduct ? lastNProduct - startIdx : pageable.getPageSize();
				query.setFirstResult(startIdx)
				.setMaxResults(maxResult);
				totalCount = totalCount > lastNProduct ? lastNProduct : totalCount;
			} else {
				query.setFirstResult(pageable.getPageNumber() * pageable.getPageSize())
				.setMaxResults(pageable.getPageSize());
			}
		}

		List<Product> products = query.getResultList();

		Pair<Integer, List<Product>> result = Pair.of(totalCount, products);
		return result;

	}

	@Override
	public Pair<Integer, List<Product>> fuzzySearchAdvanceProductCareAreaSearchField(String keyword, ProductCareArea careArea, ProductCategory category, ProductBrand brand, String type, Integer lastNProduct, Pageable pageable, String sortField, String sortOrder) {
		Boolean hasKeyword = keyword == null || keyword.isEmpty() ? false : true;

		Boolean isSortParamExisted = StringUtils.isEmpty(sortField) || StringUtils.isEmpty(sortOrder) ? false : true;
		Boolean isValidSortOrder = !StringUtils.isEmpty(sortOrder) && (sortOrder.toUpperCase().equals("ASC") || sortOrder.toUpperCase().equals("DESC"));
		Boolean isValidSortParam = isSortParamExisted && isValidSortOrder;

		StringBuilder jpql = new StringBuilder();
		jpql.append("SELECT p FROM ProductCareAreaDetail pcad JOIN pcad.product p JOIN pcad.productCareArea pca WHERE pca = :productCareArea ");

		jpql.append(" AND p.status = ");
		jpql.append(StatusEnum.APPROVED.getCode());

		if (hasKeyword) {
			jpql.append(" AND (LOWER(p.productNamePrimary) LIKE CONCAT('%',LOWER(:keyword),'%') ");
			jpql.append(" OR LOWER(p.productBrand.brandName) LIKE CONCAT('%',LOWER(:keyword),'%') ");
			jpql.append(" OR LOWER(p.productModel) LIKE CONCAT('%',LOWER(:keyword),'%') ");
			jpql.append(" OR LOWER(p.productCode) LIKE CONCAT('%',LOWER(:keyword),'%') ");
			jpql.append(" OR LOWER(p.productDescription) LIKE CONCAT('%',LOWER(:keyword),'%') ");
			jpql.append(" OR LOWER(p.hashTag) LIKE CONCAT('%',LOWER(:keyword),'%'))");
		}

		if (category != null) {
			jpql.append(" AND p.productCategory = :productCategory ");
		}
		if (brand != null) {
			jpql.append(" AND p.productBrand = :productBrand ");
		}

		Boolean isLastNNewProductSearch = !StringUtils.isEmpty(type) && NEW_PRODUCT_SEARCH_TYPE.equalsIgnoreCase(type) && lastNProduct != null ? true : false;

		if (isLastNNewProductSearch) {
			jpql.append(" AND p.approvedDate > :sevenDayBefore ORDER BY p.approvedDate DESC ");
		}

		if (isValidSortParam) {
			if (isLastNNewProductSearch) {
				//2 sort applied
				jpql.append(" , p." + sortField + " " + sortOrder);
			} else {
				//only applied sort by request param
				jpql.append(" ORDER BY p." + sortField + " " + sortOrder);
			}
		}

		TypedQuery<Product> query = em.createQuery(jpql.toString(), Product.class);

		if (hasKeyword) {
			query.setParameter("keyword", keyword);
		}

		if (careArea != null) {
			query.setParameter("productCareArea", careArea);
		}

		if (category != null) {
			query.setParameter("productCategory", category);
		}
		if (brand != null) {
			query.setParameter("productBrand", brand);
		}
		if (isLastNNewProductSearch) {
			Calendar cal = Calendar.getInstance();
			LocalDateTime currentDate = DateTimeHelper.getCurrentTimeUTC();
			query.setParameter("sevenDayBefore", DateTimeHelper.addTimeInUTC(currentDate, DateTimeHelper.HOUR, -24*DEFAULT_DAYS__OF_NEW_PRODUCT_DEFINITION));
		}

		Integer totalCount = query.getResultList().size();

		if (pageable != null) {
			if (isLastNNewProductSearch) {
				Integer startIdx = pageable.getPageNumber() * pageable.getPageSize();
				startIdx = startIdx < lastNProduct ? startIdx : lastNProduct;
				Integer endIdx = startIdx + pageable.getPageSize();
				Integer maxResult = endIdx > lastNProduct ? lastNProduct - startIdx : pageable.getPageSize();
				query.setFirstResult(startIdx)
						.setMaxResults(maxResult);
				totalCount = totalCount > lastNProduct ? lastNProduct : totalCount;
			} else {
				query.setFirstResult(pageable.getPageNumber() * pageable.getPageSize())
						.setMaxResults(pageable.getPageSize());
			}
		} else {
			Integer startIdx = 0;
			Integer maxResult = lastNProduct;

			query.setFirstResult(startIdx)
					.setMaxResults(maxResult);
			totalCount = lastNProduct;
		}

		List<Product> products = query.getResultList();

		Pair<Integer, List<Product>> result = Pair.of(totalCount, products);
		return result;
	}

	@Override
	public Pair<Integer, List<Product>> fuzzySearchProductV2(String keyword, List<ProductCategory> categories, List<ProductBrand> brands
			, Integer pageIndex, Integer pageSize, String sortFieldName, String sortOrder, BigDecimal minPrice, BigDecimal maxPrice
			, Integer productType, String manufacturerCountryCode, String manufacturerProvinceCode) {
		Boolean hasKeyword = keyword == null || keyword.isEmpty() ? false : true;

		List<String> searchFields = new ArrayList<>();
		searchFields.add("productNamePrimary");
		searchFields.add("productModel");
		searchFields.add("productCode");
		searchFields.add("productDescription");
		searchFields.add("hashTag");
		Pair<String, String> pair = Pair.of("productBrand", "brandName");
		List<Pair<String, String>> relatedSearchFields = new ArrayList<>();
		relatedSearchFields.add(pair);
		StringBuilder jpql = null;
		StringBuilder jpqlForTotalCount = null;
		if(sortFieldName.equals(FuzzySearchProductSortFieldEnum.BEST_SELLER_PRODUCT_GLOBAL.getFieldName())){
			jpql = new StringBuilder(StringProcessUtils.createFullTextSearchQueryForBestSeller("BestSellerProductGlobal", searchFields, relatedSearchFields, hasKeyword));
			jpqlForTotalCount = new StringBuilder(StringProcessUtils.createFullTextSearchQueryForTotalCountForBestSeller("BestSellerProductGlobal", searchFields, relatedSearchFields, hasKeyword));
		}else if(sortFieldName.equals(FuzzySearchProductSortFieldEnum.AVERAGE_SCORE.getFieldName())){
			jpql = new StringBuilder(StringProcessUtils.createFullTextSearchQueryForProductRating("ProductRating", searchFields, relatedSearchFields, hasKeyword));
			jpqlForTotalCount = new StringBuilder(StringProcessUtils.createFullTextSearchQueryForTotalCountForProductRating("ProductRating", searchFields, relatedSearchFields, hasKeyword));
		}else {
			jpql = new StringBuilder(StringProcessUtils.createFullTextSearchQuery("Product", searchFields, relatedSearchFields, hasKeyword));
			jpqlForTotalCount = new StringBuilder(StringProcessUtils.createFullTextSearchQueryForTotalCount("Product", searchFields, relatedSearchFields, hasKeyword));
		}

		//Conditional clause
		if (hasKeyword) {
			jpql.append(" AND e.status = ");
			jpqlForTotalCount.append(" AND e.status = ");
		} else {
			jpql.append(" WHERE e.status = ");
			jpqlForTotalCount.append(" WHERE e.status = ");
		}

		jpql.append(StatusEnum.APPROVED.getCode());
		jpqlForTotalCount.append(StatusEnum.APPROVED.getCode());
		jpql.append(" ");
		jpqlForTotalCount.append(" ");

		if (!CollectionUtils.isEmpty(categories)) {
			jpql.append(" AND e.productCategory IN (:categories) ");
			jpqlForTotalCount.append(" AND e.productCategory IN (:categories) ");
		}
		if (!CollectionUtils.isEmpty(brands)) {
			jpql.append(" AND e.productBrand IN (:brands) ");
			jpqlForTotalCount.append(" AND e.productBrand IN (:brands) ");
		}

		if(minPrice != null && maxPrice == null){
			jpql.append(" AND e.price >= :minPrice ");
			jpqlForTotalCount.append(" AND e.price >= :minPrice ");
		}

		if(maxPrice != null && minPrice == null){
			jpql.append(" And e.price <= :maxPrice ");
			jpqlForTotalCount.append(" And e.price <= :maxPrice ");
		}

		if(minPrice != null && maxPrice != null){
			jpql.append(" And e.price BETWEEN :minPrice AND :maxPrice ");
			jpqlForTotalCount.append(" And e.price BETWEEN :minPrice AND :maxPrice ");
		}

		if (productType != null) {
			jpql.append( " AND e.productType = :productType ");
			jpqlForTotalCount.append( " AND e.productType = :productType ");
		}

		if (manufacturerCountryCode != null) {
			jpql.append(" AND e.manufacturerCountryCode = :manufacturerCountryCode ");
			jpqlForTotalCount.append(" AND e.manufacturerCountryCode = :manufacturerCountryCode ");
		}

		if (manufacturerProvinceCode != null) {
			jpql.append(" AND e.manufacturerProvinceCode = :manufacturerProvinceCode ");
			jpqlForTotalCount.append(" AND e.manufacturerProvinceCode = :manufacturerProvinceCode ");
		}

		//Order by
		if (!StringUtils.isEmpty(sortFieldName)) {
			if(sortFieldName.equals(FuzzySearchProductSortFieldEnum.BEST_SELLER_PRODUCT_GLOBAL.getFieldName())){
				jpql.append("ORDER BY bspg.");
				jpql.append(sortFieldName);
				if (!StringUtils.isEmpty(sortOrder)) {
					if(sortOrder.equals(DESCENDING)) {
						jpql.append(" ");
						jpql.append(sortOrder + " " + NULL_LAST);
					}else if(sortOrder.equals(ASCENDING)){
						jpql.append(" ");
						jpql.append(sortOrder + " " + NULL_FIRST);
					}
				}
			} else if(sortFieldName.equals(FuzzySearchProductSortFieldEnum.AVERAGE_SCORE.getFieldName())){
				jpql.append("ORDER BY pr.");
				jpql.append(sortFieldName);
				if (!StringUtils.isEmpty(sortOrder)) {
					if(sortOrder.equals(DESCENDING)) {
						jpql.append(" ");
						jpql.append(sortOrder + " " + NULL_LAST);
					}else if(sortOrder.equals(ASCENDING)){
						jpql.append(" ");
						jpql.append(sortOrder + " " + NULL_FIRST);
					}
				}
			}else {
				jpql.append("ORDER BY e.");
				jpql.append(sortFieldName);
				if (!StringUtils.isEmpty(sortOrder)) {
					if (sortOrder.equals(DESCENDING)) {
						jpql.append(" ");
						jpql.append(sortOrder + " " + NULL_LAST);
					} else if (sortOrder.equals(ASCENDING)) {
						jpql.append(" ");
						jpql.append(sortOrder + " " + NULL_FIRST);
					}
				}
			}
		}

		TypedQuery<Product> query = em.createQuery(jpql.toString(), Product.class);
		Query queryForTotalCount = em.createQuery(jpqlForTotalCount.toString());

		if (!StringUtils.isEmpty(keyword)) {
			query.setParameter("keyword", keyword);
			queryForTotalCount.setParameter("keyword", keyword);
		}

		if (!CollectionUtils.isEmpty(categories)) {
			query.setParameter("categories", categories);
			queryForTotalCount.setParameter("categories", categories);
		}

		if (!CollectionUtils.isEmpty(brands)) {
			query.setParameter("brands", brands);
			queryForTotalCount.setParameter("brands", brands);
		}

		//Filter by price with minimum price or maximum price
		if(minPrice != null && maxPrice == null){
			query.setParameter("minPrice", minPrice);
			queryForTotalCount.setParameter("minPrice", minPrice);
		}

		if(maxPrice != null && minPrice == null){
			query.setParameter("maxPrice", maxPrice);
			queryForTotalCount.setParameter("maxPrice", maxPrice);
		}

		if(minPrice != null && maxPrice != null) {
			query.setParameter("minPrice", minPrice);
			queryForTotalCount.setParameter("minPrice", minPrice);
			query.setParameter("maxPrice", maxPrice);
			queryForTotalCount.setParameter("maxPrice", maxPrice);
		}

		if (productType != null) {
			query.setParameter("productType", productType);
			queryForTotalCount.setParameter("productType", productType);
		}

		if (manufacturerCountryCode != null) {
			query.setParameter("manufacturerCountryCode", manufacturerCountryCode);
			queryForTotalCount.setParameter("manufacturerCountryCode", manufacturerCountryCode);
		}

		if (manufacturerProvinceCode != null) {
			query.setParameter("manufacturerProvinceCode", manufacturerProvinceCode);
		}

		if (!CollectionUtils.isEmpty(brands)) {
			query.setParameter("brands", brands);
			queryForTotalCount.setParameter("brands", brands);
		}

		Long totalCount = (Long)queryForTotalCount.getSingleResult();
		Integer totalCountInt = totalCount.intValue();

		if (pageIndex != null && pageSize != null) {
			query.setFirstResult(pageIndex*pageSize);
			query.setMaxResults(pageSize);
		}

		Pair<Integer, List<Product>> result = Pair.of(totalCountInt, query.getResultList());
		return result;

	}


	@Override
	public Pair<Integer, List<Product>> fuzzySearchAdvanceProductCareAreaSearchFieldV2(String keyword, List<ProductCareArea> careAreas, List<ProductCategory> categories, List<ProductBrand> brands
			, Integer pageIndex, Integer pageSize, String sortFieldName, String sortOrder, BigDecimal minPrice, BigDecimal maxPrice, Integer productType, String manufacturerCountryCode, String manufacturerProvinceCode
			, List<ProductSecondCategory> secondCategories) {
		Boolean hasKeyword = keyword == null || keyword.isEmpty() ? false : true;
		StringBuilder jpql = new StringBuilder();
		StringBuilder jpqlForTotalCount = new StringBuilder();

		if (!CollectionUtils.isEmpty(careAreas) && !CollectionUtils.isEmpty(secondCategories)) {
			if(sortFieldName.equals(FuzzySearchProductSortFieldEnum.BEST_SELLER_PRODUCT_GLOBAL.getFieldName())){
				jpql.append("SELECT p FROM BestSellerProductGlobal bspg JOIN bspg.product p ");
				jpqlForTotalCount.append("SELECT count(p) FROM BestSellerProductGlobal bspg JOIN bspg.product p ");
				jpql.append(" WHERE p IN (SELECT p FROM ProductSecondCategoryDetail pscd JOIN pscd.product p JOIN pscd.productSecondCategory psc WHERE psc IN (:secondCategories)) ");
				jpqlForTotalCount.append(" WHERE p IN (SELECT p FROM ProductSecondCategoryDetail pscd JOIN pscd.product p JOIN pscd.productSecondCategory psc WHERE psc IN (:secondCategories)) ");
				jpql.append(" AND p IN (SELECT p FROM ProductCareAreaDetail pcad JOIN pcad.product p JOIN pcad.productCareArea pca WHERE pca IN (:careAreas)) ");
				jpqlForTotalCount.append(" AND p IN (SELECT pd FROM ProductCareAreaDetail pcad JOIN pcad.product pd JOIN pcad.productCareArea pca WHERE pca IN (:careAreas)) ");
			}
			if(sortFieldName.equals(FuzzySearchProductSortFieldEnum.AVERAGE_SCORE.getFieldName())){
				jpql.append("SELECT p FROM ProductRating pr JOIN pr.product p ");
				jpqlForTotalCount.append("SELECT count(p) FROM ProductRating pr JOIN pr.product p ");
				jpql.append(" WHERE p IN (SELECT p FROM ProductSecondCategoryDetail pscd JOIN pscd.product p JOIN pscd.productSecondCategory psc WHERE psc IN (:secondCategories)) ");
				jpqlForTotalCount.append(" WHERE p IN (SELECT p FROM ProductSecondCategoryDetail pscd JOIN pscd.product p JOIN pscd.productSecondCategory psc WHERE psc IN (:secondCategories)) ");
				jpql.append(" AND p IN (SELECT p FROM ProductCareAreaDetail pcad JOIN pcad.product p JOIN pcad.productCareArea pca WHERE pca IN (:careAreas)) ");
				jpqlForTotalCount.append(" AND p IN (SELECT pd FROM ProductCareAreaDetail pcad JOIN pcad.product pd JOIN pcad.productCareArea pca WHERE pca IN (:careAreas)) ");
			}else {
				jpql.append("SELECT p FROM ProductCareAreaDetail pcad JOIN pcad.product p JOIN pcad.productCareArea pca WHERE pca IN (:careAreas) ");
				jpqlForTotalCount.append("SELECT count(p) FROM ProductCareAreaDetail pcad JOIN pcad.product p JOIN pcad.productCareArea pca WHERE pca IN (:careAreas) ");
				jpql.append(" AND p IN (SELECT pr FROM ProductSecondCategoryDetail pscd JOIN pscd.product pr JOIN pscd.productSecondCategory psc WHERE psc IN (:secondCategories)) ");
				jpqlForTotalCount.append(" AND p IN (SELECT pr FROM ProductSecondCategoryDetail pscd JOIN pscd.product pr JOIN pscd.productSecondCategory psc WHERE psc IN (:secondCategories)) ");
			}
		}

		if (!CollectionUtils.isEmpty(careAreas) && CollectionUtils.isEmpty(secondCategories)) {
			if(sortFieldName.equals(FuzzySearchProductSortFieldEnum.BEST_SELLER_PRODUCT_GLOBAL.getFieldName())){
				jpql.append("SELECT p FROM BestSellerProductGlobal bspg JOIN bspg.product p ");
				jpqlForTotalCount.append("SELECT count(p) FROM BestSellerProductGlobal bspg JOIN bspg.product p ");
				jpql.append(" WHERE p IN (SELECT p FROM ProductCareAreaDetail pcad JOIN pcad.product p JOIN pcad.productCareArea pca WHERE pca IN (:careAreas)) ");
				jpqlForTotalCount.append(" WHERE p IN (SELECT p FROM ProductCareAreaDetail pcad JOIN pcad.product p JOIN pcad.productCareArea pca WHERE pca IN (:careAreas)) ");
			}else if(sortFieldName.equals(FuzzySearchProductSortFieldEnum.AVERAGE_SCORE.getFieldName())){
				jpql.append("SELECT p FROM ProductRating pr JOIN pr.product p ");
				jpqlForTotalCount.append("SELECT count(p) FROM ProductRating pr JOIN pr.product p ");
				jpql.append(" WHERE p IN (SELECT p FROM ProductCareAreaDetail pcad JOIN pcad.product p JOIN pcad.productCareArea pca WHERE pca IN (:careAreas)) ");
				jpqlForTotalCount.append(" WHERE p IN (SELECT p FROM ProductCareAreaDetail pcad JOIN pcad.product p JOIN pcad.productCareArea pca WHERE pca IN (:careAreas)) ");
			}else {
				jpql.append("SELECT p FROM ProductCareAreaDetail pcad JOIN pcad.product p JOIN pcad.productCareArea pca WHERE pca IN (:careAreas) ");
				jpqlForTotalCount.append("SELECT count(p) FROM ProductCareAreaDetail pcad JOIN pcad.product p JOIN pcad.productCareArea pca WHERE pca IN (:careAreas) ");
			}
		}

		if (!CollectionUtils.isEmpty(secondCategories) && CollectionUtils.isEmpty(careAreas)) {
			if(sortFieldName.equals(FuzzySearchProductSortFieldEnum.BEST_SELLER_PRODUCT_GLOBAL.getFieldName())) {
				jpql.append("SELECT p FROM BestSellerProductGlobal bspg JOIN bspg.product p ");
				jpqlForTotalCount.append("SELECT count(p) FROM BestSellerProductGlobal bspg JOIN bspg.product p  ");
				jpql.append(" WHERE p IN (SELECT p FROM ProductSecondCategoryDetail pscd JOIN pscd.product p JOIN pscd.productSecondCategory psc WHERE psc IN (:secondCategories)) ");
				jpqlForTotalCount.append(" WHERE p IN (SELECT count(p) FROM ProductSecondCategoryDetail pscd JOIN pscd.product p JOIN pscd.productSecondCategory psc WHERE psc IN (:secondCategories)) ");
			} else if(sortFieldName.equals(FuzzySearchProductSortFieldEnum.AVERAGE_SCORE.getFieldName())) {
				jpql.append("SELECT p FROM ProductRating pr JOIN pr.product p ");
				jpqlForTotalCount.append("SELECT count(p) FROM ProductRating pr JOIN pr.product p  ");
				jpql.append(" WHERE p IN (SELECT p FROM ProductSecondCategoryDetail pscd JOIN pscd.product p JOIN pscd.productSecondCategory psc WHERE psc IN (:secondCategories)) ");
				jpqlForTotalCount.append(" WHERE p IN (SELECT count(p) FROM ProductSecondCategoryDetail pscd JOIN pscd.product p JOIN pscd.productSecondCategory psc WHERE psc IN (:secondCategories)) ");
			}else{
				jpql.append(" SELECT p FROM ProductSecondCategoryDetail pscd JOIN pscd.product p JOIN pscd.productSecondCategory psc WHERE psc IN (:secondCategories) ");
				jpqlForTotalCount.append(" SELECT count(p) FROM ProductSecondCategoryDetail pscd JOIN pscd.product p JOIN pscd.productSecondCategory psc WHERE psc IN (:secondCategories) ");
			}
		}

		jpql.append(" AND p.status = ");
		jpqlForTotalCount.append(" AND p.status = ");
		jpql.append(StatusEnum.APPROVED.getCode());
		jpqlForTotalCount.append(StatusEnum.APPROVED.getCode());
		jpql.append(" ");
		jpqlForTotalCount.append(" ");

		if (hasKeyword) {
			jpql.append(" AND (LOWER(p.productNamePrimary) LIKE CONCAT('%',LOWER(:keyword),'%') ");
			jpqlForTotalCount.append(" AND (LOWER(p.productNamePrimary) LIKE CONCAT('%',LOWER(:keyword),'%') ");
			jpql.append(" OR LOWER(p.productBrand.brandName) LIKE CONCAT('%',LOWER(:keyword),'%') ");
			jpqlForTotalCount.append(" OR LOWER(p.productBrand.brandName) LIKE CONCAT('%',LOWER(:keyword),'%') ");
			jpql.append(" OR LOWER(p.productModel) LIKE CONCAT('%',LOWER(:keyword),'%') ");
			jpqlForTotalCount.append(" OR LOWER(p.productModel) LIKE CONCAT('%',LOWER(:keyword),'%') ");
			jpql.append(" OR LOWER(p.productCode) LIKE CONCAT('%',LOWER(:keyword),'%') ");
			jpqlForTotalCount.append(" OR LOWER(p.productCode) LIKE CONCAT('%',LOWER(:keyword),'%') ");
			jpql.append(" OR LOWER(p.productDescription) LIKE CONCAT('%',LOWER(:keyword),'%') ");
			jpqlForTotalCount.append(" OR LOWER(p.productDescription) LIKE CONCAT('%',LOWER(:keyword),'%') ");
			jpql.append(" OR LOWER(p.hashTag) LIKE CONCAT('%',LOWER(:keyword),'%'))");
			jpqlForTotalCount.append(" OR LOWER(p.hashTag) LIKE CONCAT('%',LOWER(:keyword),'%'))");
		}

		if (!CollectionUtils.isEmpty(categories)) {
			jpql.append(" AND p.productCategory IN (:categories) ");
			jpqlForTotalCount.append(" AND p.productCategory IN (:categories) ");
		}
		if (!CollectionUtils.isEmpty(brands)) {
			jpql.append(" AND p.productBrand IN (:brands) ");
			jpqlForTotalCount.append(" AND p.productBrand IN (:brands) ");
		}

		if(minPrice != null && maxPrice == null){
			jpql.append(" AND p.price >= :minPrice ");
			jpqlForTotalCount.append(" AND p.price >= :minPrice ");
		}

		if(maxPrice != null && minPrice == null){
			jpql.append(" And p.price <= :maxPrice ");
			jpqlForTotalCount.append(" And p.price <= :maxPrice ");
		}

		if(minPrice != null && maxPrice != null){
			jpql.append(" And p.price BETWEEN :minPrice AND :maxPrice ");
			jpqlForTotalCount.append(" And p.price BETWEEN :minPrice AND :maxPrice ");
		}

		if (productType != null) {
			jpql.append( " AND p.productType = :productType ");
			jpqlForTotalCount.append( " AND p.productType = :productType ");
		}

		if (manufacturerCountryCode != null) {
			jpql.append(" AND p.manufacturerCountryCode = :manufacturerCountryCode ");
			jpqlForTotalCount.append(" AND p.manufacturerCountryCode = :manufacturerCountryCode ");
		}

		if (manufacturerProvinceCode != null) {
			jpql.append(" AND p.manufacturerProvinceCode = :manufacturerProvinceCode ");
			jpqlForTotalCount.append(" AND p.manufacturerProvinceCode = :manufacturerProvinceCode ");
		}

		//Order by
		if (!StringUtils.isEmpty(sortFieldName)) {
			if(sortFieldName.equals(FuzzySearchProductSortFieldEnum.BEST_SELLER_PRODUCT_GLOBAL.getFieldName())){
				jpql.append("ORDER BY bspg.");
				jpql.append(sortFieldName);
				if (!StringUtils.isEmpty(sortOrder)) {
					if (sortOrder.equals(DESCENDING)) {
						jpql.append(" ");
						jpql.append(sortOrder + " " + NULL_LAST);
					} else if (sortOrder.equals(ASCENDING)) {
						jpql.append(" ");
						jpql.append(sortOrder + " " + NULL_FIRST);
					}
				}
			} else if(sortFieldName.equals(FuzzySearchProductSortFieldEnum.AVERAGE_SCORE.getFieldName())){
				jpql.append("ORDER BY pr.");
				jpql.append(sortFieldName);
				if (!StringUtils.isEmpty(sortOrder)) {
					if (sortOrder.equals(DESCENDING)) {
						jpql.append(" ");
						jpql.append(sortOrder + " " + NULL_LAST);
					} else if (sortOrder.equals(ASCENDING)) {
						jpql.append(" ");
						jpql.append(sortOrder + " " + NULL_FIRST);
					}
				}
			}else{
				jpql.append("ORDER BY p.");
				jpql.append(sortFieldName);
				if (!StringUtils.isEmpty(sortOrder)) {
					if (sortOrder.equals(DESCENDING)) {
						jpql.append(" ");
						jpql.append(sortOrder + " " + NULL_LAST);
					} else if (sortOrder.equals(ASCENDING)) {
						jpql.append(" ");
						jpql.append(sortOrder + " " + NULL_FIRST);
					}
				}
			}
		}

		TypedQuery<Product> query = em.createQuery(jpql.toString(), Product.class);
		Query queryForTotalCount = em.createQuery(jpqlForTotalCount.toString());
		
		if (!StringUtils.isEmpty(keyword)) {
			query.setParameter("keyword", keyword);
			queryForTotalCount.setParameter("keyword", keyword);
		}

		if (!CollectionUtils.isEmpty(categories)) {
			query.setParameter("categories", categories);
			queryForTotalCount.setParameter("categories", categories);
		}

		if (!CollectionUtils.isEmpty(brands)) {
			query.setParameter("brands", brands);
			queryForTotalCount.setParameter("brands", brands);
		}

		//Filter by price with minimum price or maximum price
		if(minPrice != null && maxPrice == null){
			query.setParameter("minPrice", minPrice);
			queryForTotalCount.setParameter("minPrice", minPrice);
		}

		if(maxPrice != null && minPrice == null){
			query.setParameter("maxPrice", maxPrice);
			queryForTotalCount.setParameter("maxPrice", maxPrice);
		}

		if(minPrice != null && maxPrice != null) {
			query.setParameter("minPrice", minPrice);
			queryForTotalCount.setParameter("minPrice", minPrice);
			query.setParameter("maxPrice", maxPrice);
			queryForTotalCount.setParameter("maxPrice", maxPrice);
		}

		if (productType != null) {
			query.setParameter("productType", productType);
			queryForTotalCount.setParameter("productType", productType);
		}

		if (manufacturerCountryCode != null) {
			query.setParameter("manufacturerCountryCode", manufacturerCountryCode);
			queryForTotalCount.setParameter("manufacturerCountryCode", manufacturerCountryCode);
		}

		if (manufacturerProvinceCode != null) {
			query.setParameter("manufacturerProvinceCode", manufacturerProvinceCode);
			queryForTotalCount.setParameter("manufacturerProvinceCode", manufacturerProvinceCode);
		}

		if (!CollectionUtils.isEmpty(secondCategories)) {
			query.setParameter("secondCategories", secondCategories);
			queryForTotalCount.setParameter("secondCategories", secondCategories);
		}

		if (!CollectionUtils.isEmpty(careAreas)) {
			query.setParameter("careAreas", careAreas);
			queryForTotalCount.setParameter("careAreas", careAreas);
		}


		Long totalCount = (Long)queryForTotalCount.getSingleResult();
		Integer totalCountInt = totalCount.intValue();

		if (pageIndex != null && pageSize != null) {
			query.setFirstResult(pageIndex*pageSize);
			query.setMaxResults(pageSize);
		}

		Pair<Integer, List<Product>> result = Pair.of(totalCountInt, query.getResultList());
		return result;
	}


	@Override
	public Pair<Integer, List<Product>> fuzzySearchProductV2ForWedoctor(String keyword, List<ProductCategory> categories, List<ProductBrand> brands
			, Integer pageIndex, Integer pageSize, String sortFieldName, String sortOrder, BigDecimal minPrice, BigDecimal maxPrice) {
		Boolean hasKeyword = keyword == null || keyword.isEmpty() ? false : true;

		List<String> searchFields = new ArrayList<>();
		searchFields.add("productNamePrimary");
		searchFields.add("productModel");
		searchFields.add("productCode");
		searchFields.add("productDescription");
		searchFields.add("hashTag");
		Pair<String, String> pair = Pair.of("productBrand", "brandName");
		List<Pair<String, String>> relatedSearchFields = new ArrayList<>();
		relatedSearchFields.add(pair);
		StringBuilder jpql = new StringBuilder(StringProcessUtils.createFullTextSearchQuery("Product", searchFields, relatedSearchFields, hasKeyword));

		//Conditional clause
		if (hasKeyword) {
			jpql.append(" AND e.status = ");
		} else {
			jpql.append(" WHERE e.status = ");
		}

		jpql.append(StatusEnum.APPROVED.getCode());

		if (!CollectionUtils.isEmpty(categories)) {
			jpql.append(" AND e.productCategory IN (:categories) ");
		}
		if (!CollectionUtils.isEmpty(brands)) {
			jpql.append(" AND e.productBrand IN (:brands) ");
		}

		if(minPrice != null && maxPrice == null){
			jpql.append(" AND e.price >= :minPrice ");
		}

		if(maxPrice != null && minPrice == null){
			jpql.append(" And e.price <= :maxPrice ");
		}

		if(minPrice != null && maxPrice != null){
			jpql.append(" And e.price BETWEEN :minPrice AND :maxPrice ");
		}

		//Order by
		if (!StringUtils.isEmpty(sortFieldName)) {
			jpql.append("ORDER BY e.");
			jpql.append(sortFieldName);
			if (!StringUtils.isEmpty(sortOrder)) {
				jpql.append(" ");
				jpql.append(sortOrder);
			}
		}

		TypedQuery<Product> query = em.createQuery(jpql.toString(), Product.class);

		if (!StringUtils.isEmpty(keyword)) {
			query.setParameter("keyword", keyword);
		}

		if (!CollectionUtils.isEmpty(categories)) {
			query.setParameter("categories", categories);
		}

		if (!CollectionUtils.isEmpty(brands)) {
			query.setParameter("brands", brands);
		}

		//Filter by price with minimum price or maximum price
		if(minPrice != null && maxPrice == null){
			query.setParameter("minPrice", minPrice);
		}

		if(maxPrice != null && minPrice == null){
			query.setParameter("maxPrice", maxPrice);
		}

		if(minPrice != null && maxPrice != null) {
			query.setParameter("minPrice", minPrice);
			query.setParameter("maxPrice", maxPrice);
		}

		Integer totalCount = query.getResultList().size();

		if (pageIndex != null && pageSize != null) {
			query.setFirstResult(pageIndex*pageSize);
			query.setMaxResults(pageSize);
		}

		Pair<Integer, List<Product>> result = Pair.of(totalCount, query.getResultList());
		return result;

	}

	@Override
	public Pair<Integer, List<Product>> fuzzySearchAdvanceProductCareAreaSearchFieldV2ForWedoctor(String keyword, List<ProductCareArea> careAreas, List<ProductCategory> categories, List<ProductBrand> brands
			, Integer pageIndex, Integer pageSize, String sortFieldName, String sortOrder, BigDecimal minPrice, BigDecimal maxPrice) {
		Boolean hasKeyword = keyword == null || keyword.isEmpty() ? false : true;
		StringBuilder jpql = new StringBuilder();
		jpql.append("SELECT p FROM ProductCareAreaDetail pcad JOIN pcad.product p JOIN pcad.productCareArea pca WHERE pca IN (:careAreas) ");

		jpql.append(" AND p.status = ");
		jpql.append(StatusEnum.APPROVED.getCode());

		if (hasKeyword) {
			jpql.append(" AND (LOWER(p.productNamePrimary) LIKE CONCAT('%',LOWER(:keyword),'%') ");
			jpql.append(" OR LOWER(p.productBrand.brandName) LIKE CONCAT('%',LOWER(:keyword),'%') ");
			jpql.append(" OR LOWER(p.productModel) LIKE CONCAT('%',LOWER(:keyword),'%') ");
			jpql.append(" OR LOWER(p.productCode) LIKE CONCAT('%',LOWER(:keyword),'%') ");
			jpql.append(" OR LOWER(p.productDescription) LIKE CONCAT('%',LOWER(:keyword),'%') ");
			jpql.append(" OR LOWER(p.hashTag) LIKE CONCAT('%',LOWER(:keyword),'%'))");
		}

		if (!CollectionUtils.isEmpty(categories)) {
			jpql.append(" AND p.productCategory IN (:categories) ");
		}
		if (!CollectionUtils.isEmpty(brands)) {
			jpql.append(" AND p.productBrand IN (:brands) ");
		}

		if(minPrice != null && maxPrice == null){
			jpql.append(" AND p.price >= :minPrice ");
		}

		if(maxPrice != null && minPrice == null){
			jpql.append(" AND p.price <= :maxPrice ");
		}

		if(minPrice != null && maxPrice != null){
			jpql.append(" AND p.price BETWEEN :minPrice AND :maxPrice ");
		}

		//Order by
		if (!StringUtils.isEmpty(sortFieldName)) {
			jpql.append("ORDER BY p.");
			jpql.append(sortFieldName);
			if (!StringUtils.isEmpty(sortOrder)) {
				jpql.append(" ");
				jpql.append(sortOrder);
			}
		}

		TypedQuery<Product> query = em.createQuery(jpql.toString(), Product.class);

		query.setParameter("careAreas", careAreas);

		if (!StringUtils.isEmpty(keyword)) {
			query.setParameter("keyword", keyword);
		}

		if (!CollectionUtils.isEmpty(categories)) {
			query.setParameter("categories", categories);
		}

		if (!CollectionUtils.isEmpty(brands)) {
			query.setParameter("brands", brands);
		}

		//Filter by price with minimum price or maximum price
		if(minPrice != null && maxPrice == null){
			query.setParameter("minPrice", minPrice);
		}

		if(maxPrice != null && minPrice == null){
			query.setParameter("maxPrice", maxPrice);
		}

		if(minPrice != null && maxPrice != null) {
			query.setParameter("minPrice", minPrice);
			query.setParameter("maxPrice", maxPrice);
		}

		Integer totalCount = query.getResultList().size();

		if (pageIndex != null && pageSize != null) {
			query.setFirstResult(pageIndex*pageSize);
			query.setMaxResults(pageSize);
		}

		Pair<Integer, List<Product>> result = Pair.of(totalCount, query.getResultList());
		return result;
	}

	@Override
	public Pair<Integer, List<Product>> searchProductByCustomCondition(Optional<Vendor> vendor, String productNamePrimary, Integer status,
			String productCode, String productModel, String companyNamePrimary, Integer vendorStatus, Long vendorCoordinatorId, Integer pageIndex, Integer pageSize) {
		StringBuilder jpql = new StringBuilder();

		jpql.append("SELECT p FROM Product p  JOIN Vendor v ON p.vendor = v ");

		Map<String, Object> mapCheckExist = new HashMap<String, Object>();

		if (!StringUtils.isEmpty(productNamePrimary)) {
			mapCheckExist.put("productNamePrimary", productNamePrimary);
		}
		
		if (status != null) {
			mapCheckExist.put("status", status);
		}
		
		if(!StringUtils.isEmpty(productCode)) {
			mapCheckExist.put("productCode", productCode);
		}
		
		if(!StringUtils.isEmpty(productModel)) {
			mapCheckExist.put("productModel", productModel);
		}
		
		if(!StringUtils.isEmpty(companyNamePrimary)) {
			mapCheckExist.put("companyNamePrimary", companyNamePrimary);
		}

		if(vendorStatus != null){
			mapCheckExist.put("vendor.status", vendorStatus);
		}

		if(vendorCoordinatorId != null){
			mapCheckExist.put("vendor.vendorCoordinator", vendorCoordinatorId);
		}

		if (!mapCheckExist.isEmpty()) {
			jpql.append(" WHERE ");
		}

		if (!CollectionUtils.isEmpty(mapCheckExist)) {
			for (String key : mapCheckExist.keySet()) {
				Object value = mapCheckExist.get(key);

				if (value instanceof String) {
					if(key.equals("companyNamePrimary")) {
						jpql.append("lower(p.vendor." + key + ") LIKE LOWER (CONCAT ('%" + value + "%'))");
					}else {
						jpql.append("lower(p." + key + ") LIKE LOWER (CONCAT ('%" + value + "%'))");
					}
				}
				if (mapCheckExist.get(key) instanceof Integer) {
					if(key.equals("vendor.status")){
						jpql.append("p." + key + " = " + value);
					}else {
						jpql.append("p." + key + " = " + value);
					}
				}
				if (mapCheckExist.get(key) instanceof Long) {
					if(key.equals("vendor.vendorCoordinator")){
						jpql.append("p." + key + " = " + value);
					}
				}
				jpql.append(" AND ");
			}
			
			jpql.replace(0, jpql.toString().length(), jpql.toString().substring(0, jpql.toString().length() - 4));
		}
		
		if (vendor.isPresent()) {
			if (mapCheckExist.isEmpty()) {
				jpql.append(" WHERE v = :vendor ");
			} else {
				jpql.append(" AND v = :vendor ");
			}
		}

		jpql.append(" AND p.status <> :imageProcessingStatus ");

		jpql.append(" ORDER BY p.updatedDate DESC");

		TypedQuery<Product> query = em.createQuery(jpql.toString(), Product.class);
		if (vendor.isPresent()) {
			query.setParameter("vendor", vendor.get());
		}
//
		query.setParameter("imageProcessingStatus", StatusEnum.PRODUCT_IMAGES_PROCESSING.getCode());
		int totalCount = query.getResultList().size();

        if (pageIndex != null && pageSize != null) {
            query.setFirstResult(pageIndex*pageSize);
            query.setMaxResults(pageSize);
        }
        
		Pair<Integer, List<Product>> result = Pair.of(totalCount, query.getResultList());
		return result;
	}

	@Override
	public Long countTotalProductByVendorAndCreatedDate(List<Vendor> vendors, LocalDateTime approvedDate, Integer status) {
		StringBuilder jpql = new StringBuilder();

		jpql.append("SELECT COUNT(p) FROM Product p");
		jpql.append(" WHERE status = :status");

		if (!CollectionUtils.isEmpty(vendors)) {
			jpql.append(" AND p.vendor IN (:vendors)");

		}

		if (approvedDate != null) {
			jpql.append(" AND DATE(p.approvedDate) = DATE(:approvedDate)");
		}

		Query query = em.createQuery(jpql.toString());

		if (status != null) {
			query.setParameter("status", status);
		}

		if (!CollectionUtils.isEmpty(vendors)) {
			query.setParameter("vendors", vendors);
		}

		if (approvedDate != null) {
			query.setParameter("approvedDate", approvedDate);
		}

		return (Long)query.getSingleResult();
	}

	@Override
	public List<String> findAllProductModelByVendors(List<Vendor> vendors) {
		StringBuilder jpql = new StringBuilder();
		Product product;
		jpql.append("SELECT DISTINCT p.productModel FROM Product p ");
		if (!CollectionUtils.isEmpty(vendors)) {
			jpql.append("AND p.vendor IN (:vendors) ");
		}
		jpql.append(" ORDER BY p.productModel ASC");

		Query query = em.createQuery(jpql.toString());

		if (!CollectionUtils.isEmpty(vendors)) {
			query.setParameter("vendors", vendors);
		}

		return query.getResultList();
	}

	@Override
	public Long countAllByProductCareArea(ProductCareArea productCareArea, List<StatusEnum> statusEnums) {
		StringBuilder jpql = new StringBuilder();
		jpql.append("SELECT COUNT(p) FROM ProductCareAreaDetail pcad JOIN pcad.product p JOIN pcad.productCareArea pca ");
		jpql.append(" WHERE p.status in (:status)");
		if (productCareArea != null) {
			jpql.append(" AND pca = :productCareArea");
		}

		Query query = em.createQuery(jpql.toString());
		if (!CollectionUtils.isEmpty(statusEnums)) {
			List<Integer> status = statusEnums.stream().map(s -> s.getCode()).collect(Collectors.toList());
			query.setParameter("status", status);
		}
		if (productCareArea != null) {
			query.setParameter("productCareArea", productCareArea);
		}
		return (Long) query.getSingleResult();
	}

	@Override
	public Long countAllByProductCategory(ProductCategory category, List<StatusEnum> statusEnums) {
		StringBuilder jpql = new StringBuilder();

		jpql.append("SELECT COUNT(p) FROM Product p WHERE p.productCategory = :productCategory");
		jpql.append(" AND p.status in (:status)");

		Query query = em.createQuery(jpql.toString());

		if (!CollectionUtils.isEmpty(statusEnums)) {
			List<Integer> status = statusEnums.stream().map(s -> s.getCode()).collect(Collectors.toList());
			query.setParameter("status", status);
		}

		if (category != null) {
			query.setParameter("productCategory", category);
		}
		return (Long) query.getSingleResult();
	}

	public Long countAllByProductSecondCategory(ProductSecondCategory productSecondCategory, List<StatusEnum> statusEnums) {
		StringBuilder jpql = new StringBuilder();

		jpql.append("SELECT COUNT(p) FROM ProductSecondCategoryDetail pscd JOIN pscd.product p JOIN pscd.productSecondCategory psc");
		jpql.append(" WHERE p.status in (:status) ");
		jpql.append(" AND psc = :productSecondCategory");

		Query query = em.createQuery(jpql.toString());

		if (!CollectionUtils.isEmpty(statusEnums)) {
			List<Integer> status = statusEnums.stream().map(s -> s.getCode()).collect(Collectors.toList());
			query.setParameter("status", status);
		}

		if (productSecondCategory != null) {
			query.setParameter("productSecondCategory", productSecondCategory);
		}

		return (Long) query.getSingleResult();
	}
	
	public Pair<Integer, List<Product>> searchByUserFavourite(String keyword, List<Long> careAreaIds, List<Long> productBrandIds, Pageable pageable) {
		
		Boolean hasKeyword = keyword == null || keyword.isEmpty() ? false : true;
		StringBuilder jpql = new StringBuilder();
		jpql.append("select p.* from product p left join product_care_area_detail pc on p.id = pc.product_id where p.status = 3 ");
		
		if (!CollectionUtils.isEmpty(careAreaIds) && !CollectionUtils.isEmpty(productBrandIds)) {
			jpql.append(" AND (pc.product_care_area_id in (:careAreaIds) or p.product_brand_id in (:productBrandIds)) ");
		} else if (!CollectionUtils.isEmpty(careAreaIds)) {
			jpql.append(" AND pc.product_care_area_id in (:careAreaIds) ");
		} else if (!CollectionUtils.isEmpty(productBrandIds)) {
			jpql.append(" AND p.product_brand_id in (:productBrandIds)) ");
		}

		if (hasKeyword) {
			jpql.append(" AND (LOWER(p.product_name_primary) LIKE CONCAT('%',LOWER(:keyword),'%') ");
			jpql.append(" OR LOWER(p.product_model) LIKE CONCAT('%',LOWER(:keyword),'%') ");
			jpql.append(" OR LOWER(p.product_code) LIKE CONCAT('%',LOWER(:keyword),'%') ");
			jpql.append(" OR LOWER(p.product_description) LIKE CONCAT('%',LOWER(:keyword),'%') ");
			jpql.append(" OR LOWER(p.hash_tag) LIKE CONCAT('%',LOWER(:keyword),'%'))");
		}

		Query query = em.createNativeQuery(jpql.toString(), Product.class);
		
		if (hasKeyword) {
			query.setParameter("keyword", keyword);
		}

		if (!CollectionUtils.isEmpty(careAreaIds)) {
			query.setParameter("careAreaIds", careAreaIds);
		}

		if (!CollectionUtils.isEmpty(productBrandIds)) {
			query.setParameter("productBrandIds", productBrandIds);
		}
		
		Integer totalCount = query.getResultList().size();

		if (pageable != null) {
			query.setFirstResult(pageable.getPageNumber() * pageable.getPageSize())
					.setMaxResults(pageable.getPageSize());
		}

		List<Product> products = query.getResultList();

		Pair<Integer, List<Product>> result = Pair.of(totalCount, products);
		return result;
	}

	@Override
	public Pair<Integer, List<Product>> findAllProductFilterByApproveProductOrderByApprovedDateDesc(Integer pageIndex, Integer pageSize){
		StringBuilder jpql = new StringBuilder();
		Integer status = StatusEnum.APPROVED.getCode();
		jpql.append("SELECT p from Product p WHERE p.status = :status ORDER BY p.approvedDate DESC");
		
		TypedQuery query = em.createQuery(jpql.toString(), Product.class);

		query.setParameter("status", status);

		if (pageIndex != null && pageSize != null) {
			query.setFirstResult(pageIndex*pageSize);
			query.setMaxResults(pageSize);
		}

		Integer totalCount = query.getResultList().size();
		List<Product> products = query.getResultList();
		
		Pair<Integer, List<Product>> result = Pair.of(totalCount, products);
		return result;
	}

	@Override
	public Pair<Integer, List<Product>> findAllProductBasedOnSearchHistory(Long clientId, String userId, Pageable pageable) {
		Integer status = StatusEnum.APPROVED.getCode();
		StringBuilder jpql = new StringBuilder();
		TypedQuery query = null;
		if(!StringUtils.isEmpty(userId)) {
			jpql.append("SELECT p FROM HistoryUserProductBySearch hpbs JOIN hpbs.product p ");
			jpql.append("WHERE p.status = :status AND hpbs.thirdPartyId = :userId ");
			jpql.append("ORDER BY hpbs.createdDate ASC");
			query = em.createQuery(jpql.toString(), Product.class);
			query.setParameter("userId", userId);
			query.setParameter("status", status);
		}
		
		if(clientId != null) {
			jpql.append("SELECT p FROM HistoryUserProductBySearch hpbs JOIN hpbs.product p ");
			jpql.append("WHERE p.status = :status AND hpbs.userId = :clientId ");
			jpql.append("ORDER BY hpbs.createdDate ASC");
			query = em.createQuery(jpql.toString(), Product.class);
			query.setParameter("clientId", clientId);
			query.setParameter("status", status);
		}

		if(pageable != null) {
			query.setFirstResult(pageable.getPageNumber() * pageable.getPageSize())
					.setMaxResults(pageable.getPageSize());
		}else {
			query.setMaxResults(DEFAULT_SIZE_HISTORY_USER_PRODUCT_BY_SEARCH);
		}

		Integer totalCount = query.getResultList().size();
		List<Product> products = query.getResultList();

		Pair<Integer, List<Product>> result = Pair.of(totalCount, products);
		return result;
	}

	@Override
	public Pair<Integer, List<Product>> findAllProductBasedOnBestSeller(Pageable pageable) {
		Integer status = StatusEnum.APPROVED.getCode();
		StringBuilder jpql = new StringBuilder();

		jpql.append("SELECT p FROM BestSellerProductGlobal bspg JOIN bspg.product p ");
		jpql.append("WHERE p.status = :status ");
		jpql.append("ORDER BY bspg.frequency DESC");

		TypedQuery query = em.createQuery(jpql.toString(), Product.class);
		query.setParameter("status", status);
		
		if(pageable != null) {
			query.setFirstResult(pageable.getPageNumber() * pageable.getPageSize())
					.setMaxResults(pageable.getPageSize());
		}else {
			query.setMaxResults(DEFAULT_SIZE_BEST_SELLER_PRODUCT_GLOBAL);
		}

		Integer totalCount = query.getResultList().size();
		List<Product> products = query.getResultList();

		Pair<Integer, List<Product>> result = Pair.of(totalCount, products);
		return result;
	}

	public Pair<Integer, List<ProductRatingDetail>> findProductRatingByCustomCondition(Product product, Double score, Integer pageIndex, Integer pageSize){
		StringBuilder jpql = new StringBuilder();

		jpql.append("SELECT prd FROM ProductRatingDetail prd ");

		if(score != null && !score.equals(RatingEnum.ALLSTAR.getScore())){
			jpql.append("WHERE prd.score = :score ");
			if(product != null){
				jpql.append(" AND prd.product = :product ");
			}
		}else {
			if (product != null) {
				jpql.append(" WHERE prd.product = :product ");
			}
		}

		jpql.append("ORDER BY prd.createdDate DESC");

		TypedQuery query = em.createQuery(jpql.toString(), ProductRatingDetail.class);

		if(score != null && !score.equals(RatingEnum.ALLSTAR.getScore())){
			query.setParameter("score", score);
		}

		if(product != null){
			query.setParameter("product", product);
		}
        Integer totalCount = query.getResultList().size();

        Pageable pageable = null;

        if (pageable != null) {
            query.setFirstResult(pageable.getPageNumber() * pageable.getPageSize())
                    .setMaxResults(pageable.getPageSize());
        }

		List<ProductRatingDetail> productRatingDetails = query.getResultList();

		Pair<Integer, List<ProductRatingDetail>> result = Pair.of(totalCount, productRatingDetails);

		return result;
	}

	@Override
	public Pair<Integer, List<Product>> findSuggestedProducts(Long customerId, Integer pageIndex, Integer pageSize) {
		Integer status = StatusEnum.APPROVED.getCode();
		StringBuilder jpql = new StringBuilder();

		jpql.append("SELECT p FROM HistoryUserProductBySearch hupbs JOIN hupbs.product p ");
		jpql.append("WHERE p.status = :status ");
		jpql.append("AND hupbs.customerId = :customerId ");
		jpql.append("ORDER BY hupbs.frequency DESC");

		TypedQuery query = em.createQuery(jpql.toString(), Product.class);
		query.setParameter("status", status);
		query.setParameter("customerId", customerId);

		Integer totalCount = query.getResultList().size();

		if (pageIndex != null && pageSize != null) {
			query.setFirstResult(pageIndex * pageSize);
			query.setMaxResults(pageSize);
		}

		Pair<Integer, List<Product>> result = Pair.of(totalCount, query.getResultList());

		return result;
	}

	@Override
	public Long countTotalApprovedProductBetweenFirstDateAndLastDate(LocalDateTime firstDayOfMonth, LocalDateTime lastDayOfMonth) {
		Integer status = StatusEnum.APPROVED.getCode();

		StringBuilder jpql = new StringBuilder();
		jpql.append("SELECT COUNT(p) FROM Product p");
		jpql.append(" WHERE p.status = :status");
		jpql.append(" AND p.approvedDate BETWEEN :firstDayOfMonth AND :lastDayOfMonth");

		Query query = em.createQuery(jpql.toString());
		query.setParameter("status", status);
		query.setParameter("firstDayOfMonth", firstDayOfMonth);
		query.setParameter("lastDayOfMonth", lastDayOfMonth);

		return (Long) query.getSingleResult();
	}

	@Override
	public Long countTotalApprovedProductByVendorAddressFindByProvince(Province province) {
		Integer status = StatusEnum.APPROVED.getCode();
		Integer type = VendorAddress.VendorAddressTypeEnum.ADDRESS.getCode();

		StringBuilder jpql = new StringBuilder();
		jpql.append("SELECT COUNT(p) FROM Product p JOIN p.vendor v JOIN v.vendorAddresses va");
		jpql.append(" WHERE va.province=:province AND va.type=:type");
		jpql.append(" AND v.status=:status");
		jpql.append(" AND p.status=:status");

		Query query = em.createQuery(jpql.toString());
		query.setParameter("province", province);
		query.setParameter("type", type);
		query.setParameter("status", status);

		return (Long) query.getSingleResult();
	}

	@Override
	public BigDecimal getMinPriceOfActiveProduct() {
		StringBuilder jpql =  new StringBuilder();
		Integer status = StatusEnum.APPROVED.getCode();
		jpql.append("SELECT MIN(p.price) FROM Product p WHERE p.status = :status");

		Query query = em.createQuery(jpql.toString());
		query.setParameter("status", status);
		return (BigDecimal) query.getSingleResult();
	}

	@Override
	public BigDecimal getMaxPriceOfActiveProduct() {
		StringBuilder jpql =  new StringBuilder();
		Integer status = StatusEnum.APPROVED.getCode();
		jpql.append("SELECT MAX(p.price) FROM Product p WHERE p.status = :status");

		Query query = em.createQuery(jpql.toString());
		query.setParameter("status", status);

		return (BigDecimal) query.getSingleResult();
	}

	@Override
	public List<Product> getTopFiveProductRating() {
		StringBuilder jpql =  new StringBuilder();
		Integer status = StatusEnum.APPROVED.getCode();
		jpql.append("SELECT p FROM Product p INNER JOIN p.productRating pr WHERE p.status = :status AND pr.averageScore is not null ORDER BY pr.averageScore DESC ");

		Query query = em.createQuery(jpql.toString(), Product.class);

		query.setParameter("status", status);

		return query.setMaxResults(NUMBER_OF_BIGGEST_PRODUCT_RATING).getResultList();
	}

	@Override
	public List<Product> getLatestFiveApprovedProducts() {
		StringBuilder jpql =  new StringBuilder();
		Integer status = StatusEnum.APPROVED.getCode();
		jpql.append("SELECT p FROM Product p WHERE p.approvedDate is not null AND p.status = :status  ORDER BY p.approvedDate DESC ");

		Query query = em.createQuery(jpql.toString(), Product.class);
		query.setParameter("status", status);

		return query.setMaxResults(NUMBER_OF_LATEST_APPROVED_PRODUCT).getResultList();
	}

	@Override
	public List<Product> findAllApprovedProductHaveProductWishlist(IdsmedAccount idsmedAccount) {
		Integer approvedStatus = StatusEnum.APPROVED.getCode();
		StringBuilder jpql = new StringBuilder();
		jpql.append("SELECT p FROM ProductWishlistDetail pwd INNER JOIN pwd.productWishlist pw INNER JOIN pwd.product p WHERE pw.status = :approvedStatus AND p.status = :approvedStatus");
		if(idsmedAccount != null){
			jpql.append(" AND pwd.idsmedAccount = :account ");
		}
		TypedQuery query = em.createQuery(jpql.toString(), Product.class);
		if(idsmedAccount != null){
			query.setParameter("account", idsmedAccount);
		}

		query.setParameter("approvedStatus", approvedStatus);

		return query.getResultList();
	}
}

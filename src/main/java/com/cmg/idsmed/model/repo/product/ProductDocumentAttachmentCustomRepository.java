package com.cmg.idsmed.model.repo.product;

import com.cmg.idsmed.model.entity.product.Product;
import com.cmg.idsmed.model.entity.product.ProductDocumentAttachment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

public interface ProductDocumentAttachmentCustomRepository {
    List<Object[]> findRunnableReminder();
}

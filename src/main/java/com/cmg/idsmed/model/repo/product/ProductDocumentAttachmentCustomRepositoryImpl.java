package com.cmg.idsmed.model.repo.product;

import com.cmg.idsmed.common.enums.RatingEnum;
import com.cmg.idsmed.common.enums.StatusEnum;
import com.cmg.idsmed.common.utils.DateTimeHelper;
import com.cmg.idsmed.common.utils.StringProcessUtils;
import com.cmg.idsmed.model.entity.product.*;
import com.cmg.idsmed.model.entity.vendor.Vendor;
import com.cmg.idsmed.service.product.ProductServiceImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.util.Pair;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author congdang
 */

public class ProductDocumentAttachmentCustomRepositoryImpl implements ProductDocumentAttachmentCustomRepository {

	@PersistenceContext
	private EntityManager em;


	@Override
	public List<Object[]> findRunnableReminder() {
		StringBuilder jpql = new StringBuilder();
		jpql.append("SELECT p, pda, pdar FROM ProductDocumentAttachmentReminder pdar ");
		jpql.append("JOIN pdar.productDocumentAttachment pda ");
		jpql.append("JOIN pda.product p WHERE p.status = :status ");

		Query query = em.createQuery(jpql.toString());
		query.setParameter("status", StatusEnum.APPROVED.getCode());
		return query.getResultList();
	}
}

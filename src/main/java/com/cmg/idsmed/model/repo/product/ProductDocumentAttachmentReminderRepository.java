package com.cmg.idsmed.model.repo.product;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.cmg.idsmed.model.entity.product.ProductDocumentAttachment;
import com.cmg.idsmed.model.entity.product.ProductDocumentAttachmentReminder;

public interface ProductDocumentAttachmentReminderRepository extends JpaRepository<ProductDocumentAttachmentReminder, Long>, JpaSpecificationExecutor<ProductDocumentAttachmentReminder> {
	public List<ProductDocumentAttachmentReminder> findByProductDocumentAttachment (ProductDocumentAttachment attachment);
}

package com.cmg.idsmed.model.repo.product;

import com.cmg.idsmed.model.entity.product.Product;
import com.cmg.idsmed.model.entity.product.ProductDocumentAttachment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

public interface ProductDocumentAttachmentRepository extends JpaRepository<ProductDocumentAttachment, Long>, JpaSpecificationExecutor<ProductDocumentAttachment>, ProductDocumentAttachmentCustomRepository {
    @Transactional
    @Modifying
    @Query("UPDATE ProductDocumentAttachment pda SET pda.updatedBy = :updatedBy, pda.updatedDate = :updatedDate, pda.status = :status WHERE pda.product IN (:products)")
    void inactiveProductDocumentAttachment(@Param("products") List<Product> products,
                                           @Param("updatedBy") Long updatedBy,
                                           @Param("updatedDate") Date updatedDate,
                                           @Param("status") Integer status);
}

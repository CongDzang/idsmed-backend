package com.cmg.idsmed.model.repo.product;

import com.cmg.idsmed.model.entity.product.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.cmg.idsmed.model.entity.product.ProductFeatures;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

public interface ProductFeatureRepository extends JpaRepository<ProductFeatures, Long>, JpaSpecificationExecutor<ProductFeatures> {

    @Modifying
    @Query("UPDATE ProductFeatures pf SET pf.updatedBy = :updatedBy, pf.updatedDate = :updatedDate, pf.status = :status WHERE pf.product IN (:products)")
    void updateProductFeatureStatus(@Param("products") List<Product> products,
                                 @Param("updatedBy") Long updatedBy,
                                 @Param("updatedDate") LocalDateTime updatedDate,
                                 @Param("status") Integer status);
}

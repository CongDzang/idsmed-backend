package com.cmg.idsmed.model.repo.product;

import com.cmg.idsmed.model.entity.product.ProductHierarchyLevelFour;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ProductHierarchyRepository extends JpaRepository<ProductHierarchyLevelFour, Long>, JpaSpecificationExecutor<ProductHierarchyLevelFour> {
	ProductHierarchyLevelFour findFirstByName(String name);
	ProductHierarchyLevelFour findByCode(String code);
}

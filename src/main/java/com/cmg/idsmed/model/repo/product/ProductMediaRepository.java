package com.cmg.idsmed.model.repo.product;

import com.cmg.idsmed.model.entity.product.Product;
import com.cmg.idsmed.model.entity.product.ProductMedia;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.ws.rs.QueryParam;

public interface ProductMediaRepository extends JpaRepository<ProductMedia, Long>, JpaSpecificationExecutor<ProductMedia> {
	List<ProductMedia> findByProductId(Long productId);
	List<ProductMedia> findByProductAndDocumentType(Product product, String documentType);

	List<ProductMedia> findAllByProductAndDocumentTypeAndVersion(Product product, String documentType, Integer version);
//	@Query("SELECT pm FROM ProductMedia WHERE pm.product = :product " +
//			"AND pm.documentType = :documentType AND pm.version <> :version")
//	List<ProductMedia> findMediaByProductAndTypeNotIncludeVerion(@Param("product") Product product,
//			@Param("documentType") String documentType, @Param("version") Integer version);


	@Modifying
	@Query("UPDATE ProductMedia pm SET pm.updatedBy = :updatedBy, pm.updatedDate = :updatedDate, pm.status = :status WHERE pm.product IN (:products)")
	void updateProductMediaStatus(@Param("products") List<Product> products,
							  @Param("updatedBy") Long updatedBy,
							  @Param("updatedDate") LocalDateTime updatedDate,
							  @Param("status") Integer status);
}

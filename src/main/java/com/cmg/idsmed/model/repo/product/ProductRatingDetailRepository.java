package com.cmg.idsmed.model.repo.product;

import com.cmg.idsmed.model.entity.product.ProductRating;
import com.cmg.idsmed.model.entity.product.ProductRatingDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ProductRatingDetailRepository extends JpaRepository<ProductRatingDetail, Long>, JpaSpecificationExecutor<ProductRatingDetail> {
}

package com.cmg.idsmed.model.repo.product;

import com.cmg.idsmed.model.entity.product.Product;
import com.cmg.idsmed.model.entity.product.ProductRating;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ProductRatingRepository extends JpaRepository<ProductRating, Long>, JpaSpecificationExecutor<ProductRating> {
    ProductRating findByProduct(Product product);
}

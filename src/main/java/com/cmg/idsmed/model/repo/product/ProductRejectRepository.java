package com.cmg.idsmed.model.repo.product;

import com.cmg.idsmed.model.entity.product.Product;
import com.cmg.idsmed.model.entity.product.ProductReject;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

public interface ProductRejectRepository extends JpaRepository<ProductReject, Long>, JpaSpecificationExecutor<ProductReject> {

    @Modifying
    @Query("UPDATE ProductReject pr SET pr.updatedBy = :updatedBy, pr.updatedDate = :updatedDate, pr.status = :status WHERE pr.product IN (:products)")
    void updateProductRejectStatus(@Param("products") List<Product> products,
                               @Param("updatedBy") Long updatedBy,
                               @Param("updatedDate") Date updatedDate,
                               @Param("status") Integer status);
}

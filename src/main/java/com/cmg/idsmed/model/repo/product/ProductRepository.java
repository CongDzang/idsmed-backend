package com.cmg.idsmed.model.repo.product;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

import com.cmg.idsmed.model.entity.masterdata.Province;
import com.cmg.idsmed.model.entity.product.search.KeywordHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import com.cmg.idsmed.model.entity.product.Product;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.RequestParam;

public interface ProductRepository extends JpaRepository<Product, Long>, JpaSpecificationExecutor<Product>, ProductCustomRepository {
	List<Product> findByStatusOrderByProductNamePrimaryAsc(Integer status);
	List<Product> findByStatusAndApprovedDateGreaterThanOrderByApprovedDateDesc(Integer status, LocalDateTime nDayBefore);
	Product findByIdOrderByProductNamePrimaryAsc(Long id);

	@Query("SELECT p FROM Product p WHERE p.id IN (:productIds) AND p.status = :status")
	List<Product> findByIdsAndStatus(@Param("productIds") List<Long> productIds, @Param("status") Integer status);
}

package com.cmg.idsmed.model.repo.product;

import com.cmg.idsmed.model.entity.product.ProductSecondCategoryDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ProductSecondCategoryDetailRepository extends JpaRepository<ProductSecondCategoryDetail, Long>, JpaSpecificationExecutor<ProductSecondCategoryDetail> {
}

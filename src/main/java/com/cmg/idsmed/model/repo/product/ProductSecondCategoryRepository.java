package com.cmg.idsmed.model.repo.product;

import com.cmg.idsmed.model.entity.product.ProductCategory;
import com.cmg.idsmed.model.entity.product.ProductSecondCategory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ProductSecondCategoryRepository extends JpaRepository<ProductSecondCategory, Long>, JpaSpecificationExecutor<ProductSecondCategory> {

    Page<ProductSecondCategory> findAllByStatus(Integer status, Pageable pageable);
    ProductSecondCategory findFirstById(Long id);
}

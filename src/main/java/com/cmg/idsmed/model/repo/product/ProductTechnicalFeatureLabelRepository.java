package com.cmg.idsmed.model.repo.product;

import com.cmg.idsmed.model.entity.product.ProductTechnicalFeatureLabel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ProductTechnicalFeatureLabelRepository extends JpaRepository<ProductTechnicalFeatureLabel, Long>, JpaSpecificationExecutor<ProductTechnicalFeatureLabel> {
    ProductTechnicalFeatureLabel findFirstByLabelNameAndLabelType(String labelName, Integer type);
    ProductTechnicalFeatureLabel findFirstByLabelNameZhCnAndLabelType(String labelNameZhCn, Integer type);
    ProductTechnicalFeatureLabel findFirstByLabelNameZhTwAndLabelType(String labelNameZhTw, Integer type);
    ProductTechnicalFeatureLabel findFirstByLabelNameViAndLabelType(String labelNameVi, Integer type);
    ProductTechnicalFeatureLabel findFirstByLabelNameThAndLabelType(String labelNameTh, Integer type);
    ProductTechnicalFeatureLabel findFirstByLabelNameIdAndLabelType(String labelNameId, Integer type);

    List<ProductTechnicalFeatureLabel> findByLabelTypeAndForMedicalConsumables(Integer type, Integer forMedicalConsumables);

}

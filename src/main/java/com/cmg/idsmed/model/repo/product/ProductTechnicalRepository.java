package com.cmg.idsmed.model.repo.product;

import com.cmg.idsmed.model.entity.product.Product;
import com.cmg.idsmed.model.entity.product.ProductTechnical;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

public interface ProductTechnicalRepository extends JpaRepository<ProductTechnical, Long>, JpaSpecificationExecutor<ProductTechnical> {

    @Modifying
    @Query("UPDATE ProductTechnical pt SET pt.updatedBy = :updatedBy, pt.updatedDate = :updatedDate, pt.status = :status WHERE pt.product IN (:products)")
    void updateProductTechnicalStatus(@Param("products") List<Product> products,
                                  @Param("updatedBy") Long updatedBy,
                                  @Param("updatedDate") LocalDateTime updatedDate,
                                  @Param("status") Integer status);
}

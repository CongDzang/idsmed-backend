package com.cmg.idsmed.model.repo.product;

import com.cmg.idsmed.model.entity.auth.IdsmedAccount;
import com.cmg.idsmed.model.entity.product.Product;
import com.cmg.idsmed.model.entity.product.ProductWishlist;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ProductWishlistCustomRepository {
    List<ProductWishlist> findProductWishlistByProductAndIdsmedAccount(Product product, IdsmedAccount idsmedAccount);

}

package com.cmg.idsmed.model.repo.product;

import com.cmg.idsmed.common.enums.StatusEnum;
import com.cmg.idsmed.model.entity.auth.IdsmedAccount;
import com.cmg.idsmed.model.entity.product.Product;
import com.cmg.idsmed.model.entity.product.ProductWishlist;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * @author congdang
 */

public class ProductWishlistCustomRepositoryImpl implements ProductWishlistCustomRepository {
	@PersistenceContext
	private EntityManager em;

	@Override
	public List<ProductWishlist> findProductWishlistByProductAndIdsmedAccount(Product product, IdsmedAccount idsmedAccount) {
		Integer approvedStatus = StatusEnum.APPROVED.getCode();
		StringBuilder jpql = new StringBuilder();
		jpql.append("SELECT pw FROM ProductWishlistDetail pwd INNER JOIN pwd.productWishlist pw INNER JOIN pwd.product p WHERE pw.status = :approvedStatus AND p.status = :approvedStatus");
		if(idsmedAccount != null){
			jpql.append(" AND pwd.idsmedAccount = :account ");
		}

		if(product != null) {
			jpql.append(" AND pw.product = :product");
		}

		TypedQuery query = em.createQuery(jpql.toString(), ProductWishlist.class);
		if(idsmedAccount != null){
			query.setParameter("account", idsmedAccount);
		}

		if(product != null) {
			query.setParameter("product", product);
		}

		query.setParameter("approvedStatus", approvedStatus);

		return query.getResultList();
	}
}
package com.cmg.idsmed.model.repo.product;

import com.cmg.idsmed.model.entity.auth.IdsmedAccount;
import com.cmg.idsmed.model.entity.product.Product;
import com.cmg.idsmed.model.entity.product.ProductWishlist;
import com.cmg.idsmed.model.entity.product.ProductWishlistDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface ProductWishlistDetailRepository extends JpaRepository<ProductWishlistDetail, Long>, JpaSpecificationExecutor<ProductWishlistDetail> {


    @Query("SELECT pwd FROM ProductWishlistDetail AS pwd WHERE pwd.product = :product AND pwd.productWishlist = :productWishList AND pwd.createdBy = :createdBy")
    ProductWishlistDetail findWishListDetailByLoginId(@Param("product") Product product, @Param("productWishList")ProductWishlist productWishlist,
                                               @Param("createdBy") Long createdBy);

    @Query("SELECT pwd FROM ProductWishlistDetail AS pwd WHERE pwd.product = :product AND pwd.idsmedAccount = :idsmedAccount")
    ProductWishlistDetail findByProductAndAccount(@Param("product") Product product,
                                               @Param("idsmedAccount") IdsmedAccount idsmedAccount);

    ProductWishlistDetail findFirstByProductAndWedoctorCustomerId(Product product, Long wedoctorCustomerId);

}

package com.cmg.idsmed.model.repo.product.config;

import com.cmg.idsmed.model.entity.masterdata.Country;
import com.cmg.idsmed.model.entity.masterdata.Province;
import com.cmg.idsmed.model.entity.product.config.ProductInfoConfig;
import org.springframework.data.domain.Pageable;
import org.springframework.data.util.Pair;


import java.util.List;

public interface ProductInfoConfigurationCustomRepository {
    Pair<Integer, List<ProductInfoConfig>> findAllProductInfoConfigByCountryAndProvince(Country country, Province province, Integer status, Pageable pageable);
    Pair<Integer, List<ProductInfoConfig>> findAllProductInfoConfig(Integer status, Pageable pageable);
}
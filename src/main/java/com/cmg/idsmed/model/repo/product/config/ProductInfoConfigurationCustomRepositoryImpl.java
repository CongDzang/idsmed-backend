package com.cmg.idsmed.model.repo.product.config;

import com.cmg.idsmed.dto.masterdata.CountryProvincePairRequest;
import com.cmg.idsmed.model.entity.masterdata.Country;
import com.cmg.idsmed.model.entity.masterdata.Province;
import com.cmg.idsmed.model.entity.product.config.ProductInfoConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.util.Pair;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProductInfoConfigurationCustomRepositoryImpl implements ProductInfoConfigurationCustomRepository {
    @PersistenceContext
    private EntityManager em;

    public Pair<Integer, List<ProductInfoConfig>> findAllProductInfoConfig(Integer status, Pageable pageable) {
        StringBuilder jpql = new StringBuilder();
        jpql.append("SELECT pic FROM ProductInfoConfig pic JOIN pic.country c ");
        if(status != null){
            jpql.append("WHERE pic.status = :status ");
        }
        jpql.append("ORDER BY c.englishBriefName");
        TypedQuery<ProductInfoConfig> query = em.createQuery(jpql.toString(), ProductInfoConfig.class);
        if(status != null){
            query.setParameter("status", status);
        }
        if (pageable != null) {
            query.setFirstResult(pageable.getPageNumber() * pageable.getPageSize())
                    .setMaxResults(pageable.getPageSize());
        }
        Integer totalCount = query.getResultList().size();
        List<ProductInfoConfig> productInfoConfigs = query.getResultList();

        Pair<Integer, List<ProductInfoConfig>> result = Pair.of(totalCount, productInfoConfigs);
        return result;
    }
    public Pair<Integer, List<ProductInfoConfig>> findAllProductInfoConfigByCountryAndProvince(Country country, Province province, Integer status, Pageable pageable){
        StringBuilder jpql = new StringBuilder();

        jpql.append("SELECT pic FROM ProductInfoConfig pic ");
        if(status != null && country == null && province == null) {
            jpql.append("WHERE status = :status");
        }

        if(country != null && status == null && province == null) {
            jpql.append("WHERE pic.country = :country");
        }

        if(province != null && status == null && country == null){
            jpql.append("WHERE pic.province = :province");
        }

        if(status != null && country != null && province == null) {
            jpql.append("WHERE pic.status = :status AND pic.country = :country");
        }

        if(country != null && province != null && status == null){
            jpql.append("WHERE pic.country = :country and pic.province = :province");
        }

        if (status != null && country != null && province != null) {
            jpql.append("WHERE pic.status = :status AND pic.country = :country AND pic.province = :province ");
        }

        TypedQuery<ProductInfoConfig> query = em.createQuery(jpql.toString(), ProductInfoConfig.class);
        if(status != null){
            query.setParameter("status", status);
        }

        if(country != null){
            query.setParameter("country", country);
        }

        if(province != null){
            query.setParameter("province", province);
        }
        int totalCount = query.getResultList().size();
        if (pageable != null) {
            query.setFirstResult(pageable.getPageNumber() * pageable.getPageSize())
                    .setMaxResults(pageable.getPageSize());
        }

        if (pageable != null) {
            query.setFirstResult(pageable.getPageNumber() * pageable.getPageSize())
                    .setMaxResults(pageable.getPageSize());
        }

        Pair<Integer, List<ProductInfoConfig>> result = Pair.of(totalCount, query.getResultList());
        return result;
    }
}

package com.cmg.idsmed.model.repo.product.config;

import com.cmg.idsmed.model.entity.masterdata.Country;
import com.cmg.idsmed.model.entity.masterdata.Province;
import com.cmg.idsmed.model.entity.product.config.ProductInfoConfig;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface ProductInfoConfigurationRepository extends JpaRepository<ProductInfoConfig, Long>, JpaSpecificationExecutor<ProductInfoConfig>, ProductInfoConfigurationCustomRepository {
    ProductInfoConfig findFirstByCountryAndProvinceIs(Country country, Province province);
    ProductInfoConfig findFirstByCountryAndProvince(Country country, Province province);
}

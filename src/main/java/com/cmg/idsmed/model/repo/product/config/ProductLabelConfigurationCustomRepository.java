package com.cmg.idsmed.model.repo.product.config;

import com.cmg.idsmed.model.entity.masterdata.Country;
import com.cmg.idsmed.model.entity.masterdata.Province;
import com.cmg.idsmed.model.entity.product.config.ProductInfoConfig;
import com.cmg.idsmed.model.entity.product.config.ProductLabelConfig;
import org.springframework.data.domain.Pageable;
import org.springframework.data.util.Pair;

import java.util.List;

public interface ProductLabelConfigurationCustomRepository {
    List<ProductLabelConfig> findAllProductLabelConfigByProductInfoConfig(ProductInfoConfig productInfoConfig, Integer status);

}
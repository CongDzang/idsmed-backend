package com.cmg.idsmed.model.repo.product.config;

import com.cmg.idsmed.model.entity.masterdata.Country;
import com.cmg.idsmed.model.entity.masterdata.Province;
import com.cmg.idsmed.model.entity.product.config.ProductInfoConfig;
import com.cmg.idsmed.model.entity.product.config.ProductLabelConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.util.Pair;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

public class ProductLabelConfigurationCustomRepositoryImpl implements ProductLabelConfigurationCustomRepository {
    @PersistenceContext
    private EntityManager em;

    @Override
    public List<ProductLabelConfig> findAllProductLabelConfigByProductInfoConfig(ProductInfoConfig productInfoConfig, Integer status) {
        StringBuilder jpql = new StringBuilder();
        jpql.append("SELECT plc FROM ProductLabelConfig plc JOIN plc.productInfoConfig pic ");
        jpql.append("WHERE pic = :productInfoConfig ");

        if(status != null){
            jpql.append("AND plc.status = :status ");
            jpql.append("AND pic.status = :status ");
        }

        jpql.append("ORDER BY plc.sequence");

        TypedQuery<ProductLabelConfig> query = em.createQuery(jpql.toString(), ProductLabelConfig.class);

        query.setParameter("productInfoConfig", productInfoConfig);

        if (status != null) {
            query.setParameter("status", status);
        }

        Integer count = query.getResultList().size();
        return query.getResultList();

    }
}

package com.cmg.idsmed.model.repo.product.config;

import com.cmg.idsmed.model.entity.product.ProductBrand;
import com.cmg.idsmed.model.entity.product.config.ProductInfoConfig;
import com.cmg.idsmed.model.entity.product.config.ProductLabelConfig;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface ProductLabelConfigurationRepository extends JpaRepository<ProductLabelConfig, Long>, JpaSpecificationExecutor<ProductLabelConfig>, ProductLabelConfigurationCustomRepository {
}

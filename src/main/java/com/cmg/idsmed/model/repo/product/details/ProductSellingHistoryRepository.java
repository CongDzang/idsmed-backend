package com.cmg.idsmed.model.repo.product.details;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.cmg.idsmed.model.entity.product.details.ProductSellingHistory;

public interface ProductSellingHistoryRepository extends JpaRepository<ProductSellingHistory, Long>, JpaSpecificationExecutor<ProductSellingHistory> {
	List<ProductSellingHistory> findByUserIdAndProductId(Long userId, Long productId);
	List<ProductSellingHistory> findByWedoctorCustomerIdAndProductId(Long wedoctorCustomerId,  Long productId);
}

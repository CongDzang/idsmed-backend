package com.cmg.idsmed.model.repo.product.details;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import com.cmg.idsmed.model.entity.product.details.ProductSellingRate;

public interface ProductSellingRateRepository extends JpaRepository<ProductSellingRate, Long>, JpaSpecificationExecutor<ProductSellingRate> {
	@Query(nativeQuery=true, value = "SELECT * FROM product_selling_rate psr WHERE psr.product_id = ?1 ORDER BY psr.purchase_feq DESC Limit ?2 OFFSET 0")
	public List<ProductSellingRate> findTopBestSeller(Long product_id, Integer limit);
}

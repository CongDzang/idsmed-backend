package com.cmg.idsmed.model.repo.product.details;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.cmg.idsmed.model.entity.product.details.RelatedProduct;

public interface RelatedProductRepository extends JpaRepository<RelatedProduct, Long>, JpaSpecificationExecutor<RelatedProduct> {
	public List<RelatedProduct> findByProductId(Long productId);
}

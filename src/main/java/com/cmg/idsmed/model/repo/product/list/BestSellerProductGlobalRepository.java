package com.cmg.idsmed.model.repo.product.list;

import java.util.Date;
import java.util.List;

import com.cmg.idsmed.model.entity.product.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.cmg.idsmed.model.entity.product.list.BestSellerProductGlobal;
import com.cmg.idsmed.model.entity.product.list.HistoryUserProductBySearch;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface BestSellerProductGlobalRepository extends JpaRepository<BestSellerProductGlobal, Long>, JpaSpecificationExecutor<HistoryUserProductBySearch> {

//    @Modifying
//    @Query("UPDATE BestSellerProductGlobal bspg SET bspg.updatedBy = :updatedBy, bspg.updatedDate = :updatedDate, bspg.status = :status WHERE bspg.product IN (:products)")
//    void inactiveProduct(@Param("products") List<Product> products,
//                         @Param("updatedBy") Long updatedBy,
//                         @Param("updatedDate") Date updatedDate,
//                         @Param("status") Integer status);


}

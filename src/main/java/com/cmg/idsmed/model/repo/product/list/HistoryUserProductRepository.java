package com.cmg.idsmed.model.repo.product.list;


import com.cmg.idsmed.model.entity.product.Product;
import com.cmg.idsmed.model.entity.product.list.HistoryUserProductBySearch;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface HistoryUserProductRepository extends JpaRepository<HistoryUserProductBySearch, Long>, JpaSpecificationExecutor<HistoryUserProductBySearch> {
    HistoryUserProductBySearch findFirstByCustomerIdAndProductId(Long customerId, Long productId);

    HistoryUserProductBySearch findFirstByProductAndCustomerIdIsNull(Product product);
    HistoryUserProductBySearch findFirstByUserIdAndProductId(Long userId, Long productId);

}

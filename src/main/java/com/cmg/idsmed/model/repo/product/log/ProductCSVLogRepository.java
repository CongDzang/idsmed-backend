package com.cmg.idsmed.model.repo.product.log;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.cmg.idsmed.model.entity.product.log.ProductCSVLog;

public interface ProductCSVLogRepository extends JpaRepository<ProductCSVLog, Long>, JpaSpecificationExecutor<ProductCSVLog> {
}

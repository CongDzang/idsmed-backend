package com.cmg.idsmed.model.repo.product.log;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.cmg.idsmed.model.entity.product.log.ProductCareAreaDetailLog;

public interface ProductCareAreaDetailLogRepository extends JpaRepository<ProductCareAreaDetailLog, Long>, JpaSpecificationExecutor<ProductCareAreaDetailLog> {
}

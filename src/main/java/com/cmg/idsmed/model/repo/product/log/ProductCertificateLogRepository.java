package com.cmg.idsmed.model.repo.product.log;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.cmg.idsmed.model.entity.product.log.ProductCertificateLog;

public interface ProductCertificateLogRepository  extends JpaRepository<ProductCertificateLog, Long>, JpaSpecificationExecutor<ProductCertificateLog> {
}

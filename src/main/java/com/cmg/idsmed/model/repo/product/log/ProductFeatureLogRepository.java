package com.cmg.idsmed.model.repo.product.log;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.cmg.idsmed.model.entity.product.log.ProductFeaturesLog;

public interface ProductFeatureLogRepository extends JpaRepository<ProductFeaturesLog, Long>, JpaSpecificationExecutor<ProductFeaturesLog> {
}

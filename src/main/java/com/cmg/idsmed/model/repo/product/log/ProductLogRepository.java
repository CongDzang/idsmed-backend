package com.cmg.idsmed.model.repo.product.log;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.cmg.idsmed.model.entity.product.log.ProductLog;

public interface ProductLogRepository extends JpaRepository<ProductLog, Long>, JpaSpecificationExecutor<ProductLog>{
}

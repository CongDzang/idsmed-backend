package com.cmg.idsmed.model.repo.product.log;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.cmg.idsmed.model.entity.product.log.ProductTechnicalLog;

public interface ProductTechnicalLogRepository extends JpaRepository<ProductTechnicalLog, Long>, JpaSpecificationExecutor<ProductTechnicalLog> {
}

package com.cmg.idsmed.model.repo.product.search;

import com.cmg.idsmed.dto.product.search.KeywordHistoryResponse;
import com.cmg.idsmed.model.entity.product.search.KeywordHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface KeywordHistoryCustomRepository {
	List<Object> getPopularSearchKeyword(Long numberOfKeyword);
}

package com.cmg.idsmed.model.repo.product.search;

import com.cmg.idsmed.model.entity.product.ProductCareArea;
import com.cmg.idsmed.model.entity.product.search.KeywordHistory;
import com.cmg.idsmed.model.repo.product.ProductCareAreaCustomRepository;
import org.springframework.data.util.Pair;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.List;

public class KeywordHistoryCustomRepositoryImpl implements KeywordHistoryCustomRepository {

	@PersistenceContext
    private EntityManager em;

    @Override
    public List<Object> getPopularSearchKeyword(Long numberOfKeyword) {
        StringBuilder jpql =  new StringBuilder();
        jpql.append("SELECT kh.keyword FROM KeywordHistory kh group by kh.keyword order by count(kh) DESC");

        Query query = em.createQuery(jpql.toString());

        return query.setMaxResults(numberOfKeyword.intValue()).getResultList();
    }
}

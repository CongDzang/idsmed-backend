package com.cmg.idsmed.model.repo.product.search;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import com.cmg.idsmed.model.entity.product.search.KeywordHistory;
import org.springframework.data.repository.query.Param;

public interface KeywordHistoryRepository extends JpaRepository<KeywordHistory, Long>, JpaSpecificationExecutor<KeywordHistory>, KeywordHistoryCustomRepository {
	@Query(nativeQuery=true, value = "SELECT * FROM keyword_history kh WHERE user_id = ?1 ORDER BY kh.created_datetime DESC Limit ?2 OFFSET 0")
	List<KeywordHistory> findLastNKeywordByUserId(Long userId, Integer limit);

	@Query("SELECT kw.keyword, COUNT(kw) FROM KeywordHistory kw GROUP BY kw.keyword ORDER BY COUNT(kw) DESC ")
	List<Object[]> findKeywordDistinctOrderByCount();

	KeywordHistory findFirstByWedoctorCustomerIdAndProductIdsIsNotNullOrderByCreatedDateDesc(Long weDoctorCustomerId);
}

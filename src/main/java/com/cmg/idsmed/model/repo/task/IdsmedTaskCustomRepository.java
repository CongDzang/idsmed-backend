package com.cmg.idsmed.model.repo.task;

import com.cmg.idsmed.model.entity.auth.IdsmedAccount;
import com.cmg.idsmed.model.entity.task.IdsmedTask;
import org.springframework.data.util.Pair;

import java.util.List;

public interface IdsmedTaskCustomRepository {
	Pair<Integer, List<IdsmedTask>> findTasksByLoginIdAnAndStatus(IdsmedAccount account, Integer status, Integer pageIndex, Integer pageSize);
}

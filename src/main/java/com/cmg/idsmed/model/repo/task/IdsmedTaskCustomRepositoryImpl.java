package com.cmg.idsmed.model.repo.task;

import com.cmg.idsmed.model.entity.auth.IdsmedAccount;
import com.cmg.idsmed.model.entity.auth.IdsmedUser;
import com.cmg.idsmed.model.entity.task.IdsmedTask;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.util.Pair;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

public class IdsmedTaskCustomRepositoryImpl implements IdsmedTaskCustomRepository {

	@PersistenceContext
	private EntityManager em;

	@Override
	public Pair<Integer, List<IdsmedTask>> findTasksByLoginIdAnAndStatus(IdsmedAccount account, Integer status, Integer pageIndex, Integer pageSize){
		StringBuilder jpql = new StringBuilder();
		jpql.append("SELECT it FROM IdsmedTaskExecutor ite JOIN ite.idsmedTask it WHERE ite.idsmedAccount = :account ");

		if (status != null) {
			jpql.append(" AND it.status = :status ");
		}

		jpql.append("ORDER BY it.createdDate DESC");

		TypedQuery<IdsmedTask> query = em.createQuery(jpql.toString(), IdsmedTask.class);

		query.setParameter("account", account);
		if (status != null) {
			query.setParameter("status", status);
		}

		Integer totalCount = query.getResultList().size();

		if (pageIndex != null && pageSize != null) {
			query.setFirstResult(pageIndex*pageSize);
			query.setMaxResults(pageSize);
		}

		Pair<Integer, List<IdsmedTask>> result = Pair.of(totalCount, query.getResultList());
		return result;
	}

//	List<IdsmedTask> findTaskByEntityIdAnd

}

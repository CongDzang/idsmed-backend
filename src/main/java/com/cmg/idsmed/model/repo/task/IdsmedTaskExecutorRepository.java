package com.cmg.idsmed.model.repo.task;

import com.cmg.idsmed.model.entity.task.IdsmedTaskExecutor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface IdsmedTaskExecutorRepository extends JpaRepository<IdsmedTaskExecutor, Long>, JpaSpecificationExecutor<IdsmedTaskExecutor> {

}

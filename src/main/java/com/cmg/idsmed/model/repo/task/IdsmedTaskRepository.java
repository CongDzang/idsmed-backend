package com.cmg.idsmed.model.repo.task;

import com.cmg.idsmed.model.entity.task.IdsmedTask;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface IdsmedTaskRepository extends JpaRepository<IdsmedTask, Long>, JpaSpecificationExecutor<IdsmedTask>, IdsmedTaskCustomRepository {
	List<IdsmedTask> findByEntityIdAndTaskTypeCodeAndStatus(Long entityId, Integer taskTypeCode, Integer status);
}

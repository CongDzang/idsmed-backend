package com.cmg.idsmed.model.repo.thirdparty;

import com.cmg.idsmed.common.enums.StatusEnum;
import com.cmg.idsmed.model.entity.masterdata.Province;
import com.cmg.idsmed.model.entity.product.*;
import com.cmg.idsmed.model.entity.vendor.Vendor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.util.Pair;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface CustomerCustomRepository {
	Long countTotalCustomerByCreatedDateBetweenFirstDayAndLastDay(LocalDateTime firstDayOfYear, LocalDateTime lastDayOfYear);
	Long countTotalCustomerByCreatedDateBetweenFirstDayAndLastDayGroupByCompanyName(LocalDateTime firstDayOfYear, LocalDateTime lastDayOfYear);
	Long countTotalCustomerByProvinceCodeAndStatusIsApproved(Province province);
	Long countTotalHospitalByProvinceCodeGroupByCompanyName(Province province);
}

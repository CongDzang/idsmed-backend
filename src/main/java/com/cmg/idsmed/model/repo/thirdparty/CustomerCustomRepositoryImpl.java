package com.cmg.idsmed.model.repo.thirdparty;

import com.cmg.idsmed.common.enums.CustomerStatusEnum;
import com.cmg.idsmed.common.enums.RatingEnum;
import com.cmg.idsmed.common.enums.StatusEnum;
import com.cmg.idsmed.common.utils.DateTimeHelper;
import com.cmg.idsmed.common.utils.StringProcessUtils;
import com.cmg.idsmed.model.entity.masterdata.Province;
import com.cmg.idsmed.model.entity.product.*;
import com.cmg.idsmed.model.entity.vendor.Vendor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.util.Pair;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author congdang
 */

public class CustomerCustomRepositoryImpl implements CustomerCustomRepository {

	@PersistenceContext
	private EntityManager em;

	@Override
	public Long countTotalCustomerByCreatedDateBetweenFirstDayAndLastDay(LocalDateTime firstDayOfMonth, LocalDateTime lastDayOfMonth) {
		StringBuilder jpql = new StringBuilder();
		jpql.append("SELECT COUNT(c) FROM Customer c");
		jpql.append(" WHERE c.createdDate BETWEEN :firstDayOfMonth AND :lastDayOfMonth");

		Query query = em.createQuery(jpql.toString());
		query.setParameter("firstDayOfMonth", firstDayOfMonth);
		query.setParameter("lastDayOfMonth", lastDayOfMonth);

		return (Long) query.getSingleResult();
	}

	@Override
	public Long countTotalCustomerByCreatedDateBetweenFirstDayAndLastDayGroupByCompanyName(LocalDateTime firstDayOfYear, LocalDateTime lastDayOfYear) {
		StringBuilder jpql = new StringBuilder();
		jpql.append("SELECT COUNT(c) FROM Customer c");
		jpql.append(" WHERE c.createdDate BETWEEN :firstDayOfYear AND :lastDayOfYear");
		jpql.append(" GROUP BY c.companyName");

		Query query = em.createQuery(jpql.toString());
		query.setParameter("firstDayOfYear", firstDayOfYear);
		query.setParameter("lastDayOfYear", lastDayOfYear);

		Long totalCustomer = Long.valueOf(query.getResultList().size());

		return totalCustomer;
	}

	@Override
	public Long countTotalCustomerByProvinceCodeAndStatusIsApproved(Province province) {
		String provinceCode = province.getProvinceCode();
		Integer status = CustomerStatusEnum.ACTIVE.getCode();

		StringBuilder jpql = new StringBuilder();
		jpql.append("SELECT COUNT(c) FROM Customer c");
		jpql.append(" WHERE c.provinceCode=:provinceCode AND c.status=:status");

		Query query = em.createQuery(jpql.toString());
		query.setParameter("provinceCode", provinceCode);
		query.setParameter("status", status);

		return (Long) query.getSingleResult();
	}

	@Override
	public Long countTotalHospitalByProvinceCodeGroupByCompanyName(Province province) {
		String provinceCode = province.getProvinceCode();
		Integer status = CustomerStatusEnum.ACTIVE.getCode();
		StringBuilder jpql = new StringBuilder();
		jpql.append("SELECT COUNT(c) FROM Customer c");
		jpql.append(" WHERE c.provinceCode=:provinceCode AND c.status=:status");
		jpql.append(" GROUP BY c.companyName");

		Query query = em.createQuery(jpql.toString());
		query.setParameter("provinceCode", provinceCode);
		query.setParameter("status", status);

		Long totalCustomer = Long.valueOf(query.getResultList().size());

		return totalCustomer;
	}
}

package com.cmg.idsmed.model.repo.user;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

import com.cmg.idsmed.model.entity.auth.IdsmedRole;
import org.springframework.data.repository.query.Param;
import org.springframework.data.util.Pair;

import com.cmg.idsmed.model.entity.auth.IdsmedUser;

public interface IdsmedUserCustomRepository {
    Pair<Integer, List<IdsmedUser>> searchUserByCustomCondition(String loginId
            , String firstname
            , String lastName
            , String companyCode
            , Integer status
            , Integer pageIndex
            , Integer pageSize);

    List<IdsmedUser> findAllUserByPermissionCode(String permissionCode);

    List<IdsmedUser> findAllByCompanyCodeAndPermissionCode(String companyCode, String permissionCode);

    List<IdsmedUser> findAllByPermissionCode(String permissionCode);

    List<IdsmedUser> findVendorSeniorByCompanyCodeAndRole(String companyCode, IdsmedRole role);

    IdsmedUser findByIdsmedUserAndPermissionCode(IdsmedUser idsmedUser, String permissionCode);

    List<IdsmedUser> findFirstSCPAdminByRoleAndPermissionCode(IdsmedRole idsmedRole, String permissionCode);

    IdsmedUser findUserByStatusAndValidUpdatedDate(Long id, Integer status, LocalDateTime compareDate);

    IdsmedUser findUserByLoginIdAndStatus(String loginId, Integer status);
}

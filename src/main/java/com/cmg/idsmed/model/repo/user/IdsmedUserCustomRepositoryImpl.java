package com.cmg.idsmed.model.repo.user;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import com.cmg.idsmed.model.entity.auth.IdsmedRole;
import com.cmg.idsmed.model.entity.auth.IdsmedUserRole;
import org.springframework.data.util.Pair;
import org.springframework.util.StringUtils;

import com.cmg.idsmed.model.entity.auth.IdsmedUser;

public class IdsmedUserCustomRepositoryImpl implements IdsmedUserCustomRepository {

    @PersistenceContext
    private EntityManager em;

    public Pair<Integer, List<IdsmedUser>> searchUserByCustomCondition(String loginIdKeyword, String firstname, String lastname, String companyCode, Integer status, Integer pageIndex, Integer pageSize) {

        StringBuilder jpql = new StringBuilder();

        jpql.append("SELECT iu FROM IdsmedUser iu JOIN IdsmedAccount ia ON ia.idsmedUser = iu ");

        if (!StringUtils.isEmpty(loginIdKeyword)) {
            jpql.append("WHERE LOWER(ia.loginId) LIKE CONCAT('%',LOWER(:loginIdKeyword),'%') ");
        }
        if (!StringUtils.isEmpty(firstname)) {
            jpql.append("AND LOWER(iu.firstName) LIKE CONCAT('%',LOWER(:firstname),'%') ");
        }
        if (!StringUtils.isEmpty(lastname)) {
            jpql.append("AND LOWER(iu.lastName) LIKE CONCAT('%',LOWER(:lastname),'%') ");
        }
        if (status != null) {
            jpql.append("AND iu.status = :status ");
        }

        if (companyCode != null) {
            jpql.append("AND iu.companyCode = :companyCode");
        }

        TypedQuery<IdsmedUser> query = em.createQuery(jpql.toString(), IdsmedUser.class);
        if (!StringUtils.isEmpty(loginIdKeyword)) {
            query.setParameter("loginIdKeyword", loginIdKeyword );
        }
        if (!StringUtils.isEmpty(firstname)) {
            query.setParameter("firstname", firstname);
        }
        if (!StringUtils.isEmpty(lastname)) {
            query.setParameter("lastname", lastname);
        }
        if (status != null) {
            query.setParameter("status", status);
        }

        if (companyCode != null) {
            query.setParameter("companyCode", companyCode);
        }

        Integer totalCount = query.getResultList().size();

        if (pageIndex != null && pageSize != null) {
            query.setFirstResult(pageIndex * pageSize);
            query.setMaxResults(pageSize);
        }

        Pair<Integer, List<IdsmedUser>> result = Pair.of(totalCount, query.getResultList());
        return result;
    }

    @Override
    public List<IdsmedUser> findAllUserByPermissionCode(String permissionCode) {

        StringBuilder jpql = new StringBuilder();

        jpql.append("SELECT iu FROM IdsmedUserRole iur JOIN iur.idsmedRole irole JOIN iur.idsmedUser iu WHERE irole " +
                "IN (SELECT ir FROM IdsmedRolePermission irp JOIN irp.idsmedRole ir JOIN irp.idsmedPermission ip WHERE ip.code = :permissionCode)");

        TypedQuery<IdsmedUser> query = em.createQuery(jpql.toString(), IdsmedUser.class);

        query.setParameter("permissionCode", permissionCode);

        return query.getResultList();
    }

    @Override
    public List<IdsmedUser> findAllByCompanyCodeAndPermissionCode(String companyCode, String permissionCode) {
        StringBuilder jpql = new StringBuilder();

        jpql.append("SELECT iu FROM IdsmedUserRole iur " +
                "JOIN iur.idsmedRole ir " +
                "JOIN iur.idsmedUser iu " +
                "WHERE ir IN (" +
                "SELECT irole FROM IdsmedRolePermission irp " +
                "JOIN irp.idsmedRole irole " +
                "JOIN irp.idsmedPermission ip " +
                "WHERE ip.code = :permissionCode)") ;

        if(!StringUtils.isEmpty(companyCode)){
            jpql.append(" AND iu.companyCode=:companyCode");
        }

        TypedQuery<IdsmedUser> query = em.createQuery(jpql.toString(), IdsmedUser.class);
        if(!StringUtils.isEmpty(companyCode)) {
            query.setParameter("companyCode", companyCode);
        }

        query.setParameter("permissionCode", permissionCode);

        return query.getResultList();
    }

    @Override
    public List<IdsmedUser> findAllByPermissionCode(String permissionCode) {
        StringBuilder jpql = new StringBuilder();

        jpql.append("SELECT iu FROM IdsmedUserRole iur " +
                "JOIN iur.idsmedRole ir " +
                "JOIN iur.idsmedUser iu " +
                "WHERE ir IN (" +
                "SELECT irole FROM IdsmedRolePermission irp " +
                "JOIN irp.idsmedRole irole " +
                "JOIN irp.idsmedPermission ip " +
                "WHERE ip.code = :permissionCode)" +
                "AND iu.companyCode IS NULL");

        TypedQuery<IdsmedUser> query = em.createQuery(jpql.toString(), IdsmedUser.class);
        query.setParameter("permissionCode", permissionCode);

        return query.getResultList();
    }

    @Override
    public List<IdsmedUser> findVendorSeniorByCompanyCodeAndRole(String companyCode, IdsmedRole role) {
        StringBuilder jpql = new StringBuilder();
        jpql.append("SELECT iu FROM IdsmedUserRole iur JOIN iur.idsmedUser iu WHERE iu.companyCode = :companyCode AND iur.idsmedRole = :role ");

        TypedQuery<IdsmedUser> query = em.createQuery(jpql.toString(), IdsmedUser.class);
        query.setParameter("companyCode", companyCode);
        query.setParameter("role", role);

        return query.getResultList();
    }

    @Override
    // Search by idsmeduser  and permission code which is product_approve permission, it will return the user,
    // then it is vendor senior, if get exception, which is mean that this product creator is dun have product_approved permission
    public IdsmedUser findByIdsmedUserAndPermissionCode(IdsmedUser idsmedUser, String permissionCode) {
        StringBuilder jpql = new StringBuilder();

        jpql.append("SELECT iu FROM IdsmedUserRole iur " +
                "JOIN iur.idsmedRole ir " +
                "JOIN iur.idsmedUser iu " +
                "WHERE ir IN (" +
                "SELECT irole FROM IdsmedRolePermission irp " +
                "JOIN irp.idsmedRole irole " +
                "JOIN irp.idsmedPermission ip " +
                "WHERE ip.code = :permissionCode) " +
                "AND iu = :idsmedUser");

        TypedQuery<IdsmedUser> query = em.createQuery(jpql.toString(), IdsmedUser.class);
        query.setParameter("idsmedUser", idsmedUser);
        query.setParameter("permissionCode", permissionCode);

        //
        try {
            return query.getSingleResult();
        } catch(Exception e){
            return null;
        }
    }

    @Override
    public List<IdsmedUser> findFirstSCPAdminByRoleAndPermissionCode(IdsmedRole idsmedRole, String permissionCode) {
        StringBuilder jpql = new StringBuilder();

        jpql.append("SELECT DISTINCT(iu) FROM IdsmedUserRole iur " +
                "JOIN iur.idsmedRole ir " +
                "JOIN iur.idsmedUser iu " +
                "WHERE ir IN (" +
                "SELECT irole FROM IdsmedRolePermission irp " +
                "JOIN irp.idsmedRole irole " +
                "JOIN irp.idsmedPermission ip " +
                "WHERE ip.code = :permissionCode) " +
                "AND ir = :idsmedRole");

        TypedQuery<IdsmedUser> query = em.createQuery(jpql.toString(), IdsmedUser.class);
        query.setParameter("permissionCode", permissionCode);
        query.setParameter("idsmedRole", idsmedRole);

        return query.getResultList();
    }

    @Override
    public IdsmedUser findUserByStatusAndValidUpdatedDate(Long id, Integer status, LocalDateTime compareDate) {
        StringBuilder jpql = new StringBuilder();

        jpql.append("SELECT u FROM IdsmedUser u WHERE u.id = :id AND u.status = :status AND u.updatedDate >= :compareDate");

        TypedQuery<IdsmedUser> query = em.createQuery(jpql.toString(), IdsmedUser.class);

        query.setParameter("id", id);
        query.setParameter("status", status);
        query.setParameter("compareDate", compareDate);

        return query.getSingleResult();
    }

    @Override
    public IdsmedUser findUserByLoginIdAndStatus(String loginId, Integer status) {
        StringBuilder jpql = new StringBuilder();
        jpql.append("SELECT u FROM IdsmedAccount ia INNER JOIN ia.idsmedUser u WHERE ia.loginId = :loginId AND ia.status = :status AND u.status = :status");
        TypedQuery<IdsmedUser> query = em.createQuery(jpql.toString(), IdsmedUser.class);
        query.setParameter("loginId", loginId);
        query.setParameter("status", status);
        return query.getSingleResult();
    }
}

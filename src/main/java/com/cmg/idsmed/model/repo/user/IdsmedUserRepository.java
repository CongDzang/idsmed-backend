package com.cmg.idsmed.model.repo.user;

import com.cmg.idsmed.model.entity.auth.IdsmedAccount;
import com.cmg.idsmed.model.entity.auth.IdsmedUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface IdsmedUserRepository extends JpaRepository<IdsmedUser, Long>, JpaSpecificationExecutor<IdsmedUser>, IdsmedUserCustomRepository {
    List<IdsmedUser> findAllByCompanyCode(String companyCode);

    List<IdsmedUser> findAllByEmail(String email);
    IdsmedUser findIdsmedUserByIdsmedAccounts(IdsmedAccount idsmedAccount);

    @Query("SELECT u FROM IdsmedUserRole iur JOIN iur.idsmedUser u JOIN iur.idsmedRole r WHERE r.code = :code AND u.status = :status")
    List<IdsmedUser> findUserByRoleCode(@Param("code") String code, @Param("status") Integer status);
    List<IdsmedUser> findIdsmedUserByCompanyCodeAndEmail(String companyCode, String email);

    @Query("SELECT u FROM IdsmedAccount ia " +
            "JOIN ia.idsmedUser u " +
            "WHERE ia.loginId = :loginId ")
    IdsmedUser findUserByLoginIdWithoutStatus(@Param("loginId") String loginId);

    IdsmedUser findFirstByUuid(String uuid);
}

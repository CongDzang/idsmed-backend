package com.cmg.idsmed.model.repo.vendor;

import java.util.List;

public interface FileAttachmentCustomRepository {

	List<Object[]> findRunnableReminder();
}

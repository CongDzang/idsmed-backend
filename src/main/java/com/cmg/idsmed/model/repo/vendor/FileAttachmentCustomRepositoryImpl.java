package com.cmg.idsmed.model.repo.vendor;

import com.cmg.idsmed.common.enums.StatusEnum;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

public class FileAttachmentCustomRepositoryImpl implements FileAttachmentCustomRepository{

	@PersistenceContext
	private EntityManager em;

	public List<Object[]> findRunnableReminder() {
		StringBuilder jpql = new StringBuilder();
		jpql.append("SELECT v, fa, vfar FROM VendorFileAttachmentReminder vfar ");
		jpql.append("JOIN vfar.fileAttachment fa ");
		jpql.append("JOIN fa.vendor v WHERE v.status = :status ");

		Query query = em.createQuery(jpql.toString());
		query.setParameter("status", StatusEnum.APPROVED.getCode());
		return query.getResultList();
	}
}

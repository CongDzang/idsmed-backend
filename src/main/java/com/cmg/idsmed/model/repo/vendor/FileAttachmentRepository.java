package com.cmg.idsmed.model.repo.vendor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.cmg.idsmed.model.entity.vendor.FileAttachment;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface FileAttachmentRepository extends JpaRepository<FileAttachment, Long>, JpaSpecificationExecutor<FileAttachment>, FileAttachmentCustomRepository {

//	@Query(nativeQuery = true, value = "SELECT fa, v FROM vendor_file_attachment_reminder vfar " +
//			"INNER JOIN file_attachment fa ON vfar.file_attachment_id = fa.id " +
//			"INNER JOIN vendor v ON fa.vendor_id = v.id " +
//			"WHERE (fa.expiry_date IS NOT NULL AND vfar.days_before IS NOT NULL AND (CURRENT_DATE + INTERVAL '1 days' * vfar.days_before = DATE(fa.expiry_date))) " +
//			"OR (fa.expiry_date IS NOT NULL AND vfar.days_after IS NOT NULL AND (CURRENT_DATE - INTERVAL '1 days' * vfar.days_after = DATE(fa.expiry_date))) " +
//			"OR (vfar.in_expiry_date = TRUE AND fa.expiry_date IS NOT NULL AND (CURRENT_DATE = DATE(fa.expiry_date))) " +
//			"GROUP BY fa, v")
//	List<Object[]> findRunnableReminder();
}

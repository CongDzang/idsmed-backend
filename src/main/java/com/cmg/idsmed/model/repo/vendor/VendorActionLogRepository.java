package com.cmg.idsmed.model.repo.vendor;

import com.cmg.idsmed.model.entity.vendor.VendorActionLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface VendorActionLogRepository extends JpaRepository<VendorActionLog, Long>, JpaSpecificationExecutor<VendorActionLog> {
}

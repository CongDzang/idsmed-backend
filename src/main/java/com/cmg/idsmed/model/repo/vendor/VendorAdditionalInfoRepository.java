package com.cmg.idsmed.model.repo.vendor;

import com.cmg.idsmed.model.entity.vendor.VendorAdditionalInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface VendorAdditionalInfoRepository extends JpaRepository<VendorAdditionalInfo, Long>, JpaSpecificationExecutor<VendorAdditionalInfo> {
}

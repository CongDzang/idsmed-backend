package com.cmg.idsmed.model.repo.vendor;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.cmg.idsmed.model.entity.masterdata.Province;
import com.cmg.idsmed.model.entity.product.*;
import com.cmg.idsmed.model.entity.product.search.KeywordHistory;
import org.springframework.data.domain.Pageable;
import org.springframework.data.util.Pair;

import com.cmg.idsmed.common.enums.StatusEnum;
import com.cmg.idsmed.model.entity.vendor.Vendor;

public interface VendorAddressCustomRepository {
    Long countTotalVendorByVendorAddressFindByProvince(Province province);

}

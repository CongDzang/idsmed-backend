package com.cmg.idsmed.model.repo.vendor;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.*;
import javax.validation.constraints.Size;

import com.cmg.idsmed.common.enums.RatingEnum;
import com.cmg.idsmed.model.entity.masterdata.Province;
import com.cmg.idsmed.model.entity.product.*;
import com.cmg.idsmed.model.entity.product.search.KeywordHistory;
import com.cmg.idsmed.model.entity.vendor.VendorAddress;
import com.cmg.idsmed.model.repo.product.ProductCustomRepository;
import com.cmg.idsmed.service.product.ProductServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;
import org.springframework.data.util.Pair;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.cmg.idsmed.common.enums.StatusEnum;
import com.cmg.idsmed.common.utils.DateTimeHelper;
import com.cmg.idsmed.common.utils.StringProcessUtils;
import com.cmg.idsmed.model.entity.vendor.Vendor;

/**
 * @author congdang
 */

public class VendorAddressCustomRepositoryImpl implements VendorAddressCustomRepository {

	@PersistenceContext
	private EntityManager em;

	@Override
	public Long countTotalVendorByVendorAddressFindByProvince(Province province) {
		StringBuilder jpql = new StringBuilder();
		Integer status = StatusEnum.APPROVED.getCode();
		Integer type = VendorAddress.VendorAddressTypeEnum.ADDRESS.getCode();
		jpql.append("SELECT COUNT(DISTINCT v)  FROM VendorAddress va JOIN va.vendor v");
		jpql.append(" WHERE va.province = :province");
		jpql.append(" AND v.status = :status");

		Query query = em.createQuery(jpql.toString());
		query.setParameter("province", province);
		query.setParameter("status", status);
		return (Long) query.getSingleResult();
	}
}

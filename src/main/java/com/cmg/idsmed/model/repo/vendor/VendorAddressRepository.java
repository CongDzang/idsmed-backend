package com.cmg.idsmed.model.repo.vendor;

import com.cmg.idsmed.model.entity.masterdata.Province;
import com.cmg.idsmed.model.entity.vendor.VendorAddress;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface VendorAddressRepository extends JpaRepository<VendorAddress, Long>, JpaSpecificationExecutor<VendorAddress>, VendorAddressCustomRepository {
}

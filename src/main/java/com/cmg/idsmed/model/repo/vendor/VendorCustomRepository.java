package com.cmg.idsmed.model.repo.vendor;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

import com.cmg.idsmed.model.entity.masterdata.Province;
import org.springframework.data.repository.query.Param;
import org.springframework.data.util.Pair;

import com.cmg.idsmed.model.entity.vendor.Vendor;

public interface VendorCustomRepository {
	Pair<Integer, List<Vendor>> searchVendorByCustomCondition(String companyNamePrimary, Integer status, Integer vendorCoordinator, Integer pageIndex, Integer pageSize);

	List<String> findAllVendorNames();

	Vendor findVendorByStatusAndValidUpdatedDate(Long id
			,Integer status
			,LocalDateTime compareDate);
}

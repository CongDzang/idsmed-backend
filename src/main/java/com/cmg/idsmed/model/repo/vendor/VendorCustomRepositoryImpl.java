package com.cmg.idsmed.model.repo.vendor;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.*;

import com.cmg.idsmed.common.enums.StatusEnum;
import com.cmg.idsmed.common.utils.JPQLHelper;
import com.cmg.idsmed.model.entity.auth.IdsmedUser;
import com.cmg.idsmed.model.entity.masterdata.Province;
import com.cmg.idsmed.service.share.IdsmedServiceHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Pair;
import org.springframework.util.StringUtils;

import com.cmg.idsmed.model.entity.vendor.Vendor;

public class VendorCustomRepositoryImpl implements VendorCustomRepository {

	@PersistenceContext
	private EntityManager em;

	@Override
	public Pair<Integer, List<Vendor>> searchVendorByCustomCondition(String companyNamePrimary,
																	 Integer status,
																	 Integer vendorCoordinator,
																	 Integer pageIndex,
																	 Integer pageSize) {
		StringBuilder jpql = new StringBuilder();

		jpql.append("SELECT v FROM Vendor v");

		Map<String, Object> mapCheckExist = new HashMap<String, Object>();

		if (!StringUtils.isEmpty(companyNamePrimary)) {
			mapCheckExist.put("companyNamePrimary", companyNamePrimary);
		}

		if (status != null) {
			mapCheckExist.put("status", status);
		}

		if (vendorCoordinator != null) {
			mapCheckExist.put("vendorCoordinator", vendorCoordinator);
		}

		if (!mapCheckExist.isEmpty()) {
			jpql.append(" WHERE ");
		}

		jpql = JPQLHelper.generateJPQLStatement(mapCheckExist, jpql);

		TypedQuery<Vendor> query = em.createQuery(jpql.toString(), Vendor.class);

		Integer totalCount = query.getResultList().size();

		if (pageIndex != null && pageSize != null) {
			query.setFirstResult(pageIndex*pageSize);
			query.setMaxResults(pageSize);
		}

		Pair<Integer, List<Vendor>> result = Pair.of(totalCount, query.getResultList());
		return result;
	}

	@Override
	public List<String> findAllVendorNames() {
		StringBuilder jpql = new StringBuilder();
		jpql.append("SELECT DISTINCT v.companyNamePrimary FROM Vendor v ORDER BY v.companyNamePrimary");

		Query query = em.createQuery(jpql.toString());

		return query.getResultList();
	}

	@Override
	public Vendor findVendorByStatusAndValidUpdatedDate(Long id
			,Integer status
			,LocalDateTime compareDate) {
		StringBuilder jpql = new StringBuilder();

		jpql.append("SELECT v FROM Vendor v WHERE v.id = :id AND v.status = :status AND v.updatedDate >= :compareDate");

		TypedQuery<Vendor> query = em.createQuery(jpql.toString(), Vendor.class);

		query.setParameter("id", id);
		query.setParameter("status", status);
		query.setParameter("compareDate", compareDate);

		return query.getSingleResult();
	}
}

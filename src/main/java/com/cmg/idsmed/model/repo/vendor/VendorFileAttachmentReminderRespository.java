package com.cmg.idsmed.model.repo.vendor;

import com.cmg.idsmed.model.entity.vendor.VendorFileAttachmentReminder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface VendorFileAttachmentReminderRespository extends JpaRepository<VendorFileAttachmentReminder, Long>, JpaSpecificationExecutor<VendorFileAttachmentReminder>, VendorFileAttachmentReminderCustomRespository {

}

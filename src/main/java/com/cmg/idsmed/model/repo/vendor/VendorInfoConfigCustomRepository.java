package com.cmg.idsmed.model.repo.vendor;

import com.cmg.idsmed.model.entity.masterdata.Country;
import com.cmg.idsmed.model.entity.masterdata.Province;
import com.cmg.idsmed.model.entity.vendor.config.VendorInfoConfig;
import org.springframework.data.domain.Pageable;
import org.springframework.data.util.Pair;

import java.util.List;

public interface VendorInfoConfigCustomRepository {
	Pair<Integer, List<VendorInfoConfig>> findAllVendorInfoConfigByCountryAndProvince(Country country, Province province
			/*Integer location*/, Integer vendorType, Integer status, Pageable pageable);
	Pair<Integer, List<VendorInfoConfig>> findAllVendorInfoConfig(Integer status, Pageable pageable);
	Pair<Integer, List<VendorInfoConfig>> findAllVendorInfoConfigByVendorType(Integer vendorType, Integer status, Pageable pageable);
}

package com.cmg.idsmed.model.repo.vendor;

import com.cmg.idsmed.model.entity.masterdata.Country;
import com.cmg.idsmed.model.entity.masterdata.Province;
import com.cmg.idsmed.model.entity.vendor.config.VendorInfoConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.util.Pair;
import org.springframework.util.CollectionUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class VendorInfoConfigCustomRepositoryImpl implements VendorInfoConfigCustomRepository {

	@PersistenceContext
	private EntityManager em;


	public Pair<Integer, List<VendorInfoConfig>> findAllVendorInfoConfig(Integer status, Pageable pageable) {
		StringBuilder jpql = new StringBuilder();
		jpql.append("SELECT vic FROM VendorInfoConfig vic JOIN vic.country c ");
		if(status != null){
			jpql.append("WHERE vic.status = :status ");
		}
		jpql.append("ORDER BY c.englishBriefName");

		TypedQuery<VendorInfoConfig> query = em.createQuery(jpql.toString(), VendorInfoConfig.class);
		if(status != null){
			query.setParameter("status", status);
		}
		if (pageable != null) {
			query.setFirstResult(pageable.getPageNumber() * pageable.getPageSize())
					.setMaxResults(pageable.getPageSize());
		}
		Integer totalCount = query.getResultList().size();
		List<VendorInfoConfig> vendorInfoConfigs = query.getResultList();

		Pair<Integer, List<VendorInfoConfig>> result = Pair.of(totalCount, vendorInfoConfigs);
		return result;
	}

	@Override
	public Pair<Integer, List<VendorInfoConfig>> findAllVendorInfoConfigByVendorType(Integer vendorType, Integer status, Pageable pageable) {
		StringBuilder jpql = new StringBuilder();
		jpql.append("SELECT vic FROM VendorInfoConfig vic JOIN vic.country c ");
		if(status != null){
			jpql.append("WHERE vic.status = :status ");
		}
		if(status == null && vendorType != null && vendorType != 0){
			jpql.append("WHERE vic.vendorType = :vendorType ");
		}
		if(status != null && vendorType != null && vendorType != 0){
			jpql.append("AND vic.vendorType = :vendorType");
		}
		jpql.append("ORDER BY c.englishBriefName");

		TypedQuery<VendorInfoConfig> query = em.createQuery(jpql.toString(), VendorInfoConfig.class);
		if(status != null){
			query.setParameter("status", status);
		}
		if(vendorType != null && vendorType != 0){
			query.setParameter("vendorType", vendorType);
		}
		if (pageable != null) {
			query.setFirstResult(pageable.getPageNumber() * pageable.getPageSize())
					.setMaxResults(pageable.getPageSize());
		}
		Integer totalCount = query.getResultList().size();
		List<VendorInfoConfig> vendorInfoConfigs = query.getResultList();

		Pair<Integer, List<VendorInfoConfig>> result = Pair.of(totalCount, vendorInfoConfigs);
		return result;
	}

	public Pair<Integer, List<VendorInfoConfig>> findAllVendorInfoConfigByCountryAndProvince(Country country, Province province
			/*Integer location*/, Integer vendorType, Integer status, Pageable pageable) {
		StringBuilder jpql = new StringBuilder();
		jpql.append("SELECT vic FROM VendorInfoConfig vic ");

		List<String> paramLabels = new ArrayList<>();

		if (country != null) {
			paramLabels.add("country");
		}
		if (province != null) {
			paramLabels.add("province");
		}

		// if (location != null) {
		// 	paramLabels.add("location");
		// }

		if (vendorType != null && vendorType != 0) {
			paramLabels.add("vendorType");
		}

		if (status != null) {
			paramLabels.add("status");
		}

		if (!CollectionUtils.isEmpty(paramLabels)) {
			jpql.append("WHERE vic.");
			jpql.append(paramLabels.get(0));
			jpql.append(" = :");
			jpql.append(paramLabels.get(0));
			for (int i = 1; i < paramLabels.size(); i++) {
				jpql.append(" AND vic.");
				jpql.append(paramLabels.get(i));
				jpql.append(" = :");
				jpql.append(paramLabels.get(i));
			}
		}

		TypedQuery<VendorInfoConfig> query = em.createQuery(jpql.toString(), VendorInfoConfig.class);

		if (country != null) {
			query.setParameter("country", country);
		}
		if (province != null) {
			query.setParameter("province", province);
		}

		// if (location != null) {
		// 	query.setParameter("location", location);
		// }

		if (vendorType != null && vendorType != 0) {
			query.setParameter("vendorType", vendorType);
		}

		if (status != null) {
			query.setParameter("status", status);
		}

		Integer totalCount = query.getResultList().size();

		if (pageable != null) {
			query.setFirstResult(pageable.getPageNumber() * pageable.getPageSize())
					.setMaxResults(pageable.getPageSize());
		}

		List<VendorInfoConfig> vendorInfoConfigs = query.getResultList();

		Pair<Integer, List<VendorInfoConfig>> result = Pair.of(totalCount, vendorInfoConfigs);
		return result;

	}
}

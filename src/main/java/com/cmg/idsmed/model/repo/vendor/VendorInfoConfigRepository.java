package com.cmg.idsmed.model.repo.vendor;

import com.cmg.idsmed.model.entity.masterdata.Country;
import com.cmg.idsmed.model.entity.masterdata.Province;
import com.cmg.idsmed.model.entity.vendor.config.VendorInfoConfig;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface VendorInfoConfigRepository extends JpaRepository<VendorInfoConfig, Long>, JpaSpecificationExecutor<VendorInfoConfig>, VendorInfoConfigCustomRepository {
		VendorInfoConfig findFirstByCountryAndProvinceIsAndLocationAndVendorType(Country country, Province province, Integer location, Integer vendorType);
		VendorInfoConfig findFirstByCountryAndProvinceAndLocationAndVendorType(Country country, Province province, Integer location, Integer vendorType);
}

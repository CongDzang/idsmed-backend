package com.cmg.idsmed.model.repo.vendor;

import com.cmg.idsmed.model.entity.masterdata.Country;
import com.cmg.idsmed.model.entity.masterdata.Province;
import com.cmg.idsmed.model.entity.vendor.config.VendorLabelConfig;

import java.util.List;

public interface VendorLabelConfigCustomRepository {
	List<VendorLabelConfig> findAllVendorLabelConfigByCountryAndProvince(Country country, Province province
			/*, Integer location*/, Integer vendorType, Integer status);
}

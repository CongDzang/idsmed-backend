package com.cmg.idsmed.model.repo.vendor;

import com.cmg.idsmed.model.entity.masterdata.Country;
import com.cmg.idsmed.model.entity.masterdata.Province;
import com.cmg.idsmed.model.entity.vendor.Vendor;
import com.cmg.idsmed.model.entity.vendor.config.VendorInfoConfig;
import com.cmg.idsmed.model.entity.vendor.config.VendorLabelConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.util.Pair;
import org.springframework.util.CollectionUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class VendorLabelConfigCustomRepositoryImpl implements VendorLabelConfigCustomRepository {

	@PersistenceContext
	private EntityManager em;

	public List<VendorLabelConfig> findAllVendorLabelConfigByCountryAndProvince(Country country, Province province
			/*, Integer location*/, Integer vendorType, Integer status) {
		StringBuilder jpql = new StringBuilder();
		jpql.append("SELECT vlc FROM VendorLabelConfig vlc JOIN vlc.vendorInfoConfig vic ");
		jpql.append("JOIN vic.country c ");

        /**
         * Currently, province and vendor type has been add a new option, All (which
         * equivalent to all options available)
         * From line 38 - 50 is commented due to changes in Vendor Configuration
         * If there is any concern, developer may change code(old and new) to suit
         * case required
         */
//		if (province != null) {
//			jpql.append("JOIN vic.province p ");
//		} else {
//			jpql.append("LEFT JOIN vic.province p ");
//		}
//
//		jpql.append("WHERE c = :country ");
//
//		if (province != null) {
//			jpql.append(" AND p = :province ");
//		} else {
//			jpql.append(" AND p IS NULL ");
//		}

        jpql.append("LEFT JOIN vic.province p WHERE c = :country AND (p = :province OR p IS NULL) ");

		// if (location != null) {
		// 	jpql.append(" AND vic.location = :location ");
		// }

		if (vendorType != null) {
			jpql.append("AND vic.vendorType IN (0, :vendorType) ");
		}

		if (status != null) {
			jpql.append("AND vic.status = :status ");
			jpql.append("AND vlc.status = :status ");
		}

		jpql.append("ORDER BY vlc.sequence ASC");

		TypedQuery<VendorLabelConfig> query = em.createQuery(jpql.toString(), VendorLabelConfig.class);

		query.setParameter("country", country);

//		if (province != null) {
			query.setParameter("province", province);
//		}

		if (status != null) {
			query.setParameter("status", status);
		}

		// if (location != null) {
		// 	query.setParameter("location", location);
		// }

		if (vendorType != null) {
			query.setParameter("vendorType", vendorType);
		}

		List<VendorLabelConfig> vendorLabelConfigs = query.getResultList();

		return vendorLabelConfigs;

	}

}

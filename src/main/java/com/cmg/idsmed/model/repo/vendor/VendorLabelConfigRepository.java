package com.cmg.idsmed.model.repo.vendor;

import com.cmg.idsmed.model.entity.vendor.config.VendorInfoConfig;
import com.cmg.idsmed.model.entity.vendor.config.VendorLabelConfig;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface VendorLabelConfigRepository extends JpaRepository<VendorLabelConfig, Long>, JpaSpecificationExecutor<VendorLabelConfig>, VendorLabelConfigCustomRepository {
}

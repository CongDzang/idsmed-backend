package com.cmg.idsmed.model.repo.vendor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.cmg.idsmed.model.entity.vendor.VendorReject;

public interface VendorRejectRepository extends JpaRepository<VendorReject, Long>, JpaSpecificationExecutor<VendorReject>  {
}

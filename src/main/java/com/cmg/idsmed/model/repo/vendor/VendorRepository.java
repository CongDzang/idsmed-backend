package com.cmg.idsmed.model.repo.vendor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.cmg.idsmed.model.entity.vendor.Vendor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

public interface VendorRepository extends JpaRepository<Vendor, Long>, JpaSpecificationExecutor<Vendor>,VendorCustomRepository  {
	Vendor findFirstByCompanyCode(String companyCode);

	@Query("SELECT COUNT(v) FROM Vendor v WHERE v.status = :status")
	Long countTotalVendors(@Param("status") Integer stauts);

	List<Vendor> findByPersonIncharge1Email(String email);

	List<Vendor> findByPersonIncharge2Email(String email);



}

package com.cmg.idsmed.model.repo.web.service.log;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.cmg.idsmed.model.entity.web.service.log.WebServiceLog;

public interface WebServiceLogRepository extends JpaRepository<WebServiceLog, Long>, JpaSpecificationExecutor<WebServiceLog>  {
}
package com.cmg.idsmed.service.accessright;

import com.cmg.idsmed.common.exception.IdsmedBaseException;
import com.cmg.idsmed.dto.accessright.AccessRightListResponse;
import com.cmg.idsmed.dto.accessright.FunctionNameListResponse;
import com.cmg.idsmed.dto.auth.IdsmedRoleRequest;
import com.cmg.idsmed.dto.auth.IdsmedRoleResponse;
import com.cmg.idsmed.dto.user.IdsmedUserResponse;

public interface AccessRightService {

    AccessRightListResponse getAccessRightList(String roleName, Integer status, Integer roleType, Integer pageIndex, Integer pageSize) throws IdsmedBaseException;
    IdsmedRoleResponse getAccessRightById(Long id) throws IdsmedBaseException;
    IdsmedRoleResponse create(IdsmedRoleRequest request) throws IdsmedBaseException;
    IdsmedRoleResponse edit(IdsmedRoleRequest request) throws IdsmedBaseException;
    AccessRightListResponse approve(IdsmedRoleRequest request) throws IdsmedBaseException;
    FunctionNameListResponse getFunctionName() throws IdsmedBaseException;
    IdsmedRoleResponse inactiveRole(IdsmedRoleRequest request) throws IdsmedBaseException;
    IdsmedRoleResponse resumeInactiveRole(IdsmedRoleRequest request) throws IdsmedBaseException;
}

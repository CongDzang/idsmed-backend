package com.cmg.idsmed.service.accessright;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.cmg.idsmed.common.Constant.LoggerConstant;
import com.cmg.idsmed.common.utils.LangHelper;
import com.cmg.idsmed.model.entity.auth.*;
import com.cmg.idsmed.model.repo.auth.IdsmedAccountRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.util.Pair;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.cmg.idsmed.common.enums.CountryCodeEnum;
import com.cmg.idsmed.common.enums.StatusEnum;
import com.cmg.idsmed.common.exception.ErrorInfo;
import com.cmg.idsmed.common.exception.IdsmedBaseException;
import com.cmg.idsmed.dto.accessright.AccessRightListResponse;
import com.cmg.idsmed.dto.accessright.FunctionNameListResponse;
import com.cmg.idsmed.dto.auth.IdsmedRoleRequest;
import com.cmg.idsmed.dto.auth.IdsmedRoleResponse;
import com.cmg.idsmed.model.repo.accessright.AccessRightRepository;
import com.cmg.idsmed.model.repo.accessright.FunctionNameRepository;
import com.cmg.idsmed.model.repo.auth.IdsmedPermissionRepository;
import com.cmg.idsmed.model.repo.auth.IdsmedRolePermissionRepository;
import com.cmg.idsmed.service.share.MessageResourceService;

import javax.transaction.Transactional;

/**
 * @author congdang
 */

@Configuration
@Service
public class AccessRightServiceImpl implements AccessRightService {

	@Autowired
	private AccessRightRepository idsmedRoleRepository;
	
	@Autowired
	private IdsmedPermissionRepository idsmedPermissionRepository;

	@Autowired
	private MessageResourceService messageResourceService;

	@Autowired
	private IdsmedRolePermissionRepository idsmedRolePermissionRepository;

	@Autowired
	private FunctionNameRepository idsmedFunctionRepository;

	@Autowired
	private IdsmedAccountRepository idsmedAccountRepository;

	private static final Logger logger = LoggerFactory.getLogger(AccessRightServiceImpl.class);

	@Value("${logger.start.method.description}")
	String START_METHOD_DESCRIPTION;

	@Value("${logger.end.method.description}")
	String END_METHOD_DESCRIPTION;
	private static final String CREATE_ACCESS_RIGHT_METHOD_NAME = "createAccessRight";
	private static final String GET_ACCESS_RIGHT_LIST_METHOD_NAME = "getAccessRightList";
	private static final String EDIT_ACCESS_RIGHT_METHOD_NAME = "editAccessRight";
	private static final String GET_ACCESS_RIGHT_BY_ID_METHOD_NAME = "getAccessRightById";
	private static final String APPROVE_ACCESS_RIGHT_METHOD_NAME = "approveAccessRight";
	private static final String GET_FUNCTION_NAME_METHOD_NAME = "getFunctionName";
	private static final String INACTIVE_ROLE_METHOD_NAME = "inactiveRole";
	private static final String RESUME_INACTIVE_ROLE_METHOD_NAME = "resumeInactiveRole";

	@Override
	public IdsmedRoleResponse create(IdsmedRoleRequest request) throws IdsmedBaseException {
		// TODO Auto-generated method stub
		loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, CREATE_ACCESS_RIGHT_METHOD_NAME, request.getLoginId());
		Optional<IdsmedRole> existingRole = idsmedRoleRepository.findByName(request.getName());
		List<IdsmedRolePermission> rolePermissionList = new ArrayList<>();
		IdsmedRole role = new IdsmedRole();
		IdsmedRoleResponse response = null;

		if (existingRole.isPresent()) {
			logger.error("The Role Name is existing");
			throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.ROLE_NAME_EXIST_ERROR,
					LangHelper.getLangCode(request.getLangCode())));
		}

		BeanUtils.copyProperties(request, role, "id");
		role.setCountryCode(CountryCodeEnum.CHINA.getCode());
		role.setRoleType(request.getRoleType());
		Long id = idsmedRoleRepository.getMaxId();
		role.setCode(new DecimalFormat("000").format(++id));
		
		role.setStatus(request.getStatus() == null ? StatusEnum.APPROVED.getCode() : request.getStatus());
		IdsmedRole storedRole = idsmedRoleRepository.save(role);

		if (!CollectionUtils.isEmpty(request.getPermissionIdList())) {
			 rolePermissionList = getRolePermissionList(request.getPermissionIdList(), storedRole);
		}
		idsmedRolePermissionRepository.saveAll(rolePermissionList);
		role.setRolePermissions(rolePermissionList);
		
		response = new IdsmedRoleResponse(role);

		loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, CREATE_ACCESS_RIGHT_METHOD_NAME, request.getLoginId());
		return response;
	}

	@Override
	public AccessRightListResponse getAccessRightList(String name, Integer status, Integer roleType, Integer pageIndex,
			Integer pageSize) {
		// TODO Auto-generated method stub
        loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, GET_ACCESS_RIGHT_LIST_METHOD_NAME, null);
 		Pair<Integer, List<IdsmedRole>> roles = idsmedRoleRepository.searchRoleByCustomCondition(name, status, roleType,
				pageIndex, pageSize);

		AccessRightListResponse accessRightListResponse = new AccessRightListResponse(pageIndex, pageSize);
		if(CollectionUtils.isEmpty(roles.getSecond())) {
			return accessRightListResponse;
		}
		int roleCount = roles.getFirst();

		List<IdsmedRoleResponse> idsmedRoleResponses = roles.getSecond().stream().map(v -> new IdsmedRoleResponse(v))
				.collect(Collectors.toList());

		if (pageIndex != null && pageSize != null) {
			accessRightListResponse = new AccessRightListResponse(roleCount, pageIndex, pageSize, idsmedRoleResponses);
		} else {
			accessRightListResponse = new AccessRightListResponse(roleCount, idsmedRoleResponses);
		}
		loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, GET_ACCESS_RIGHT_LIST_METHOD_NAME, null);
		return accessRightListResponse;
	}

	@Override
	public IdsmedRoleResponse edit(IdsmedRoleRequest request) throws IdsmedBaseException {
		// TODO Auto-generated method stub
		loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, EDIT_ACCESS_RIGHT_METHOD_NAME, null);
		Optional<IdsmedRole> role = idsmedRoleRepository.findById(request.getId());
		List<IdsmedRolePermission> rolePermissionList = new ArrayList<>();
		IdsmedRoleResponse response = null;

		if (!role.isPresent()) {
			throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.ROLE_NOT_FOUND_ERROR,
					LangHelper.getLangCode(request.getLangCode())));
		}

		BeanUtils.copyProperties(request, role.get(), "id");
		role.get().setCountryCode(CountryCodeEnum.CHINA.getCode());
		role.get().setRoleType(request.getRoleType());
		
		role.get().setStatus(request.getStatus() == null ? StatusEnum.APPROVED.getCode() : request.getStatus());
		IdsmedRole storedRole = idsmedRoleRepository.save(role.get());

		if (!CollectionUtils.isEmpty(request.getPermissionIdList())) {
			idsmedRolePermissionRepository.deleteAll(storedRole.getRolePermissions());
			rolePermissionList = getRolePermissionList(request.getPermissionIdList(), storedRole);

		}
		idsmedRolePermissionRepository.saveAll(rolePermissionList);
		response = new IdsmedRoleResponse(role.get());
		loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, EDIT_ACCESS_RIGHT_METHOD_NAME, null);

		return response;
	}

	@Override
	public IdsmedRoleResponse getAccessRightById(Long id) throws IdsmedBaseException {
		// TODO Auto-generated method stub
		loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, GET_ACCESS_RIGHT_BY_ID_METHOD_NAME, null);
		Optional<IdsmedRole> idsmedRole = Optional.ofNullable(idsmedRoleRepository.getOne(id));
		logger.info("end getting an access right");
		if (idsmedRole.isPresent()) {
			IdsmedRoleResponse idsmedRoleResponse = new IdsmedRoleResponse(idsmedRole.get());

			return idsmedRoleResponse;
		}
		loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, GET_ACCESS_RIGHT_BY_ID_METHOD_NAME, null);
		return null;
	}

	@Override
	public AccessRightListResponse approve(IdsmedRoleRequest request) throws IdsmedBaseException {
		// TODO Auto-generated method stub
		loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, APPROVE_ACCESS_RIGHT_METHOD_NAME, null);
		Long roleId = request.getId();
		Optional<IdsmedRole> storedRole = idsmedRoleRepository.findById(roleId);

		if (!storedRole.isPresent()) {
			logger.error("The role name not existing");
			throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.ROLE_NOT_FOUND_ERROR,
					LangHelper.getLangCode(request.getLangCode())));
		}
		storedRole.get().setStatus(StatusEnum.APPROVED.getCode());
		idsmedRoleRepository.save(storedRole.get());

		IdsmedRoleResponse accessRightResponses = new IdsmedRoleResponse(storedRole.get());
		AccessRightListResponse accessRightListResponse = new AccessRightListResponse(accessRightResponses);
		loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, APPROVE_ACCESS_RIGHT_METHOD_NAME, null);

		return accessRightListResponse;
	}

	@Override
	public FunctionNameListResponse getFunctionName() throws IdsmedBaseException {
		// TODO Auto-generated method stub
		loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, GET_FUNCTION_NAME_METHOD_NAME, null);
		loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, GET_FUNCTION_NAME_METHOD_NAME, null);
		return new FunctionNameListResponse(idsmedFunctionRepository.findFunctionName());
	}

	public List<IdsmedRolePermission> getRolePermissionList(List<Long> permissionIdList, IdsmedRole storedRole){
		List<IdsmedRolePermission> rolePermissionList = new ArrayList<IdsmedRolePermission>();
		for (Long permissionId : permissionIdList) {
			IdsmedPermission permission = idsmedPermissionRepository.getOne(permissionId);

			IdsmedRolePermission rolePermissions = new IdsmedRolePermission();
			rolePermissions.setIdsmedRole(storedRole);
			rolePermissions.setIdsmedPermission(permission);
			rolePermissions.setCountry_code(CountryCodeEnum.CHINA.getCode());
			rolePermissionList.add(rolePermissions);
		}
		return rolePermissionList;
	}

	@Override
	@PreAuthorize("@PermissionService.isMethodPermited(authentication, {'GT0000005'})")
	@Transactional
	public IdsmedRoleResponse inactiveRole(IdsmedRoleRequest request) throws IdsmedBaseException {
		loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, INACTIVE_ROLE_METHOD_NAME, null);
		IdsmedAccount account = idsmedAccountRepository.findByLoginId(request.getLoginId());
		Optional<IdsmedRole> roleOptional = idsmedRoleRepository.findById(request.getRoleId());
		if (!roleOptional.isPresent()) {
			throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.ROLE_NOT_FOUND_ERROR,
					LangHelper.getLangCode(request.getLangCode())));
		}

		IdsmedRole role = roleOptional.get();

		// Throw error if role is using by any user
		List<IdsmedUserRole> userRoles = new ArrayList<>();
		userRoles = role.getUserRoles();
		if (!CollectionUtils.isEmpty(userRoles)) {
			throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.ROLE_IS_USING_ERROR,
					LangHelper.getLangCode(request.getLangCode())));
		}

		Integer status = StatusEnum.INACTIVE.getCode();

		List<IdsmedRolePermission> rolePermissions = new ArrayList<>();
		rolePermissions = role.getRolePermissions();
		List<IdsmedRolePermission> saveRolePermissions = new ArrayList<>();
		if (!CollectionUtils.isEmpty(rolePermissions)) {
			// Inactive role permissions
			for (IdsmedRolePermission rolePermission : rolePermissions) {
				rolePermission.setUpdatedBy(account.getId());
				rolePermission.setStatus(status);
				saveRolePermissions.add(rolePermission);
			}

			idsmedRolePermissionRepository.saveAll(saveRolePermissions);

			// Inactive role
			role.setUpdatedBy(account.getId());
			role.setStatus(status);
			role.setInactiveReason(request.getInactiveReason());
			idsmedRoleRepository.save(role);
		}

		IdsmedRoleResponse response = new IdsmedRoleResponse(role);
		loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, INACTIVE_ROLE_METHOD_NAME, null);
		return response;
	}

	@Override
	@PreAuthorize("@PermissionService.isMethodPermited(authentication, {'GT0000006'})")
	@Transactional
	public IdsmedRoleResponse resumeInactiveRole(IdsmedRoleRequest request) throws IdsmedBaseException {
		loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, RESUME_INACTIVE_ROLE_METHOD_NAME, null);
		IdsmedAccount account = idsmedAccountRepository.findByLoginId(request.getLoginId());
		Optional<IdsmedRole> roleOptional = idsmedRoleRepository.findById(request.getId());
		if (roleOptional.isPresent()) {
			throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.ROLE_NOT_FOUND_ERROR,
					LangHelper.getLangCode(request.getLangCode())));
		}

		Integer status = StatusEnum.APPROVED.getCode();

		IdsmedRole role = roleOptional.get();
		List<IdsmedRolePermission> rolePermissions = new ArrayList<>();
		rolePermissions = role.getRolePermissions();
		List<IdsmedRolePermission> saveRolePermissions = new ArrayList<>();
		if (!CollectionUtils.isEmpty(rolePermissions)) {
			// Reactive role permissions
			for (IdsmedRolePermission rolePermission : rolePermissions) {
				rolePermission.setUpdatedBy(account.getId());
				rolePermission.setStatus(status);
				saveRolePermissions.add(rolePermission);
			}

			idsmedRolePermissionRepository.saveAll(saveRolePermissions);

			// Reactive role
			role.setUpdatedBy(account.getId());
			role.setStatus(status);
			idsmedRoleRepository.save(role);
		}

		IdsmedRoleResponse response = new IdsmedRoleResponse(role);
		loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, RESUME_INACTIVE_ROLE_METHOD_NAME, null);
		return response;
	}

	private void loggerForAllMethodInThisService(String methodDescription, String methodName, String loginId){
		logger.info(methodDescription, methodName, loginId != null?loginId:null);
	}
}

package com.cmg.idsmed.service.auth;

import com.cmg.idsmed.common.exception.IdsmedBaseException;
import com.cmg.idsmed.dto.auth.*;
import com.cmg.idsmed.model.entity.auth.IdsmedUser;

import java.util.List;

public interface AuthenticationService {

    LoginResponse login(LoginRequest request) throws IdsmedBaseException;

    ApiAuthResponse apiGrantAuth(ApiAuthRequest request, String clientIp) throws IdsmedBaseException ;

    Boolean changePassword(ChangePasswordRequest request) throws IdsmedBaseException;

    List<IdsmedUser> getUsersByPermissionCode(String permissionCode);

    List<IdsmedUser> getUsersByPermissionCodeForSCPAdmin(String permissionCode);

    Boolean resetPassword(String email, String loginId, String langCode) throws IdsmedBaseException;
    void logUserLoginHistory(String loginId, String ip, Integer loginStatusCode);
}

package com.cmg.idsmed.service.auth;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import com.cmg.idsmed.common.Constant.LoggerConstant;
import com.cmg.idsmed.common.Constant.PropertiesLabelConst;
import com.cmg.idsmed.common.enums.*;
import com.cmg.idsmed.common.utils.LangHelper;
import com.cmg.idsmed.common.utils.StringProcessUtils;
import com.cmg.idsmed.config.rabbitmq.IdsmedQueueConfig;
import com.cmg.idsmed.mail.EmailObject;
import com.cmg.idsmed.mail.EmailService;
import com.cmg.idsmed.mail.UserResetPasswordEmailObject;
import com.cmg.idsmed.model.entity.auth.*;
import com.cmg.idsmed.model.entity.log.LogUserLoginHistory;
import com.cmg.idsmed.model.repo.log.LogUserLoginHistoryRepository;
import com.cmg.idsmed.model.repo.user.IdsmedUserRepository;
import org.apache.commons.codec.language.bm.Lang;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.util.Pair;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.cmg.idsmed.common.exception.ErrorInfo;
import com.cmg.idsmed.common.exception.IdsmedBaseException;
import com.cmg.idsmed.common.utils.DateTimeHelper;
import com.cmg.idsmed.common.utils.EncryptionHelper;
import com.cmg.idsmed.dto.auth.ApiAuthRequest;
import com.cmg.idsmed.dto.auth.ApiAuthResponse;
import com.cmg.idsmed.dto.auth.ChangePasswordRequest;
import com.cmg.idsmed.dto.auth.IdsmedPermissionResponse;
import com.cmg.idsmed.dto.auth.IdsmedRoleResponse;
import com.cmg.idsmed.dto.auth.LoginRequest;
import com.cmg.idsmed.dto.auth.LoginResponse;
import com.cmg.idsmed.model.entity.subscriber.Subscriber;
import com.cmg.idsmed.model.entity.subscriber.SubscriberIp;
import com.cmg.idsmed.model.entity.vendor.Vendor;
import com.cmg.idsmed.model.repo.auth.IdsmedAccountRepository;
import com.cmg.idsmed.model.repo.auth.IdsmedPermissionRepository;
import com.cmg.idsmed.model.repo.auth.IdsmedRoleRepository;
import com.cmg.idsmed.model.repo.vendor.VendorRepository;
import com.cmg.idsmed.service.share.MessageResourceService;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

/**
 * @author congdang
 */

@Configuration
@Service
public class AuthenticationServiceImpl implements AuthenticationService {

    private static final Logger logger = LoggerFactory.getLogger(AuthenticationServiceImpl.class);

	@Autowired
	private Environment env;

    @Autowired
    private MessageResourceService messageResourceService;

    @Autowired
    private IdsmedRoleRepository idsmedRoleRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
	private IdsmedUserRepository idsmedUserRepository;

    @Autowired
	private IdsmedAccountRepository idsmedAccountRepository;
    
    @Autowired
    private IdsmedPermissionRepository idsmedPermissionRepository;
    
    @Autowired
    private VendorRepository vendorRepository;

    @Autowired
	private EmailService emailService;

	@Autowired
	private RabbitTemplate rabbitTemplate;

	@Autowired
	private LogUserLoginHistoryRepository logUserLoginHistoryRepository;

	@Value("${logger.start.method.description}")
	String START_METHOD_DESCRIPTION;

	@Value("${logger.end.method.description}")
	String END_METHOD_DESCRIPTION;

	private static final String LOGIN_METHOD_NAME = "login";
	private static final String API_GRANT_AUTH_METHOD_NAME = "apiGrantAuth";
	private static final String CHANGE_PASSWORD_METHOD_NAME = "changePassword";
	private static final String RESET_PASSWORD_METHOD_NAME = "resetPassword";

    @Override
    public LoginResponse login(LoginRequest request) throws IdsmedBaseException {
		loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, LOGIN_METHOD_NAME, request.getLoginId());
		String token = null;
		if(StringProcessUtils.hasWhiteSpace(request.getLoginId())){
			throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.USER_LOGIN_ID_CANNOT_CONTAIN_SPACE, LangHelper.getLangCode(request.getLangCode())));
		}
        String loginId = request.getLoginId().toLowerCase();
        IdsmedAccount account = idsmedAccountRepository.findByLoginId(loginId);
        List<IdsmedRole> idsmedRoles = new ArrayList<>();
        Optional<IdsmedUser>  user = null;
        Optional<Subscriber> subscriber = null;

        if (account == null) {
        	logger.error("Can't find out Account with login ID: {}", request.getLoginId());
        	throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.WRONG_LOGIN_INFO, LangHelper.getLangCode(request.getLangCode())));
        }

        //Check if account is inactive or archive.
		if (!account.getStatus().equals(StatusEnum.APPROVED.getCode())) {
        	throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.ACCOUNT_NOT_ACTIVE_ERROR, LangHelper.getLangCode(request.getLangCode())));
		}

        //Move this checking to here for avoid unnecessary checking type and load role
        if (!passwordEncoder.matches(request.getPassword(), account.getPassword())) {
        	logger.error("Password is not correct");
        	throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.WRONG_LOGIN_INFO, LangHelper.getLangCode(request.getLangCode())));
        }

		user = Optional.ofNullable(account.getIdsmedUser());

		if (account.getType().equals(IdsmedAccountTypeEnum.SUBSCRIBER.getCode())) {
        	subscriber = Optional.ofNullable(account.getSubscriber());
        	if (subscriber.isPresent()) {
				idsmedRoles = idsmedRoleRepository.findAllRoleBySubscriber(subscriber.get());
			}
		} else {
			if (user.isPresent()) {
				idsmedRoles = idsmedRoleRepository.findAllRoleByUserAndStatus(user.get(), StatusEnum.APPROVED.getCode());
			}
		}
		
		List<IdsmedRoleResponse> roleResponses = idsmedRoles.stream().map(ir -> {
			IdsmedRoleResponse irr = new IdsmedRoleResponse(ir);
			return irr;
		}).collect(Collectors.toList());
		
        List<IdsmedPermission> permissions = idsmedRoles.stream().flatMap(ir -> {
        	List<IdsmedPermission> ips = idsmedPermissionRepository.findAllPermissionsByRole(ir);
        	return ips.stream();
        }).collect(Collectors.toList());
        
        List<IdsmedPermissionResponse> permissionReponses = permissions.stream().map(ip -> {
        	IdsmedPermissionResponse ipr = new IdsmedPermissionResponse(ip);
        	ipr.setFunctionName(ip.getFunction().getName());
        	return ipr;
        }).collect(Collectors.toList());

        List<String> permissionIds = permissions.stream().map(ip -> String.valueOf(ip.getId())).collect(Collectors.toList());

        Integer timeout = Integer.valueOf(env.getProperty(PropertiesLabelConst.WEB_SESSION_TIMEOUT_LABEL));
        Date currentDate = Calendar.getInstance().getTime();
        Date expiredDate = DateTimeHelper.addTime(currentDate, DateTimeHelper.MINUTE, timeout == null ? 30 : timeout);
        
        System.out.println("Token generated and will expired 30 second later at : " + DateTimeHelper.convertDateToString(expiredDate, DateTimeHelper.DATE_TIME_PATTERN));
        token = Jwts.builder().setSubject(account.getLoginId())
        		.setExpiration(expiredDate)
                .claim("permissions", permissionIds).setIssuedAt(new Date())
                .signWith(SignatureAlgorithm.HS256, "secretkey").compact();

        if (StringUtils.isEmpty(token)) {
            throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.GENERATE_TOKEN_ERROR, LangHelper.getLangCode(request.getLangCode())));
        }

        LoginResponse response = new LoginResponse();

        if (user.isPresent() && user.get().getTypeOfUser() != null && user.get().getTypeOfUser().equals(UserTypeEnum.VENDOR_USER.getCode())) {
			String companyCode = user.get().getCompanyCode();
			if (!StringUtils.isEmpty(companyCode)) {
				Vendor vendor = vendorRepository.findFirstByCompanyCode(companyCode);
				response.setVendorId(vendor != null ? vendor.getId() : null);
				response.setCompanyCode(companyCode);
			}
		}

		response.setUserType(user.get().getTypeOfUser());
    	response.setUserId(user.get().getId());
    	response.setAccountId(account.getId());
    	response.setCurrentLangCode(user.get().getIdsmedAccounts().get(0).getAccountSettings() == null ? CountryCodeEnum.CHINA.getCode() : user.get().getIdsmedAccounts().get(0).getAccountSettings().getCurrentLangCode());
        response.setToken(token);
        response.setRoles(roleResponses);
        response.setPermissions(permissionReponses);
		loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, LOGIN_METHOD_NAME, request.getLoginId());
		return response;
    }

    @Override
	public ApiAuthResponse apiGrantAuth(ApiAuthRequest request, String clientIp) throws IdsmedBaseException {
		loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, API_GRANT_AUTH_METHOD_NAME, request.getLoginId());

		String token = null;
    	ApiAuthResponse response = new ApiAuthResponse();
    	IdsmedAccount account = idsmedAccountRepository.findByLoginId(request.getClientId());

		if (account == null) {
			logger.error("Can't find out account with login Id: {}", request.getClientId());
			throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.WRONG_LOGIN_INFO, LangHelper.getLangCode(request.getLangCode())));
		}
		
		//Move this checking to here for avoid unnecessary checking type and load role
		if (!passwordEncoder.matches(request.getPassword(), account.getPassword())) {
			logger.error("Password is not correct");
			throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.WRONG_LOGIN_INFO, LangHelper.getLangCode(request.getLangCode())));
		}
		
		@SuppressWarnings("unused")
		List<SubscriberIp> subscriberIps = new ArrayList<>();
		if (account.getType().equals(IdsmedAccountTypeEnum.SUBSCRIBER.getCode()) && account.getSubscriber() != null) {
			subscriberIps = account.getSubscriber().getSubscriberIpList();
		}

		//Check appKey
		if (!checkAppKey(request.getAppKey(), request.getCountryCode(), request.getClientId(), request.getPassword())) {
			throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.APPKEY_NOT_MATCH_ERROR, LangHelper.getLangCode(request.getLangCode())));
		}

		List<IdsmedRole> idsmedRoles = new ArrayList<>();

		if (account.getType().equals(IdsmedAccountTypeEnum.SUBSCRIBER.getCode())) {
        	Optional<Subscriber> subscriber = Optional.ofNullable(account.getSubscriber());
        	if (subscriber.isPresent()) {
				idsmedRoles = idsmedRoleRepository.findAllRoleBySubscriber(subscriber.get());
			}
		}
		
        List<IdsmedPermission> permissions = idsmedRoles.stream().flatMap(ir -> {
        	List<IdsmedPermission> ips = idsmedPermissionRepository.findAllPermissionsByRole(ir);
        	return ips.stream();
        }).collect(Collectors.toList());
        

//		List<String> roleCodes = idsmedRoleResponses.stream().map(rp -> rp.getCode()).collect(Collectors.toList());
		List<String> permissionIds = permissions.stream().map(ip -> String.valueOf(ip.getId())).collect(Collectors.toList());
		
		Integer timeout = Integer.valueOf(env.getProperty(PropertiesLabelConst.API_SESSION_TIMEOUT_LABEL));
        Date currentDate = Calendar.getInstance().getTime();
        Date expiredDate = DateTimeHelper.addTime(currentDate, DateTimeHelper.MINUTE, timeout == null ? 30 : timeout);
        
        System.out.println("Token generated and will expired 30 second later at : " + DateTimeHelper.convertDateToString(expiredDate, DateTimeHelper.DATE_TIME_PATTERN));


		token = Jwts.builder().setSubject(account.getLoginId())
				//.setExpiration(expiredDate)
				.claim("permissions", permissionIds).setIssuedAt(new Date())
				.signWith(SignatureAlgorithm.HS256, "secretkey").compact();

		if (StringUtils.isEmpty(token)) {
			throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.GENERATE_TOKEN_ERROR, LangHelper.getLangCode(request.getLangCode())));
		}

		response.setClientId(request.getClientId());
		response.setToken(token);
		loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, API_GRANT_AUTH_METHOD_NAME, request.getLoginId());
		return response;
	}

	@Override
	@Transactional
	public Boolean changePassword(ChangePasswordRequest request) throws IdsmedBaseException {
		loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, CHANGE_PASSWORD_METHOD_NAME, request.getLoginId());

		IdsmedAccount account = idsmedAccountRepository.findByLoginId(request.getLoginId());
		if (account == null) {
			throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.ACCOUNT_NOT_EXIST_ERROR, request.getLangCode()));
		}

		if (!passwordEncoder.matches(request.getOldPassword(), account.getPassword())) {
			logger.error("Old password is not match");
			throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.WRONG_OLD_PASSWORD_ERROR, request.getLangCode()));
		}

		account.setPassword(passwordEncoder.encode(request.getNewPassword()));

		idsmedAccountRepository.save(account);
		loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, CHANGE_PASSWORD_METHOD_NAME, request.getLoginId());
		return true;
	}

	@Override
	public List<IdsmedUser> getUsersByPermissionCode(String permissionCode) {
		List<IdsmedUser> idsmedUsers = idsmedUserRepository.findAllUserByPermissionCode(permissionCode);
		return idsmedUsers;
	}

	@Override
	public List<IdsmedUser> getUsersByPermissionCodeForSCPAdmin(String permissionCode) {
		List<IdsmedUser> idsmedUsers = idsmedUserRepository.findAllByPermissionCode(permissionCode);
		return idsmedUsers;
	}

	private Boolean checkAppKey(String requestAppKey, String countryCode, String clientId, String password) throws IdsmedBaseException {
		StringBuilder rawString = new StringBuilder();
		if (!StringUtils.isEmpty(countryCode)) {
			rawString.append(countryCode);
			rawString.append(ApiAuthRequest.COUNTRY_CODE_PARAMETER_NAME);
		}
		if (!StringUtils.isEmpty(clientId)) {
			rawString.append(clientId);
			rawString.append(ApiAuthRequest.CLIENT_ID_PARAMETER_NAME);
		}
		if (!StringUtils.isEmpty(password)) {
			rawString.append(password);
			rawString.append(ApiAuthRequest.PASSWORD_PARAMETER_NAME);
		}
		String generationAppKey = "";
		try {
			generationAppKey = EncryptionHelper.md5Encrypt(rawString.toString().toLowerCase());
		} catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
			new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.MD5_ENCRYPTION_ERROR, LangEnum.ENGLISH.getCode()));
		}
		if (!StringUtils.isEmpty(requestAppKey) && requestAppKey.equals(generationAppKey.toUpperCase())) {
			return true;
		}
		return false;
    }

    @Transactional
    public Boolean resetPassword(String email, String loginId, String langCode) throws IdsmedBaseException {
		loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, RESET_PASSWORD_METHOD_NAME, null);

		IdsmedAccount account = idsmedAccountRepository.findByLoginIdAndStatus(loginId, StatusEnum.APPROVED.getCode());
		if (account == null) {
			throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.ACCOUNT_NOT_EXIST_ERROR, LangHelper.getLangCode(langCode)));
		}
		IdsmedUser user = account.getIdsmedUser();
		if (user == null) {
			throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.USER_NOT_FOUND_ERROR, LangHelper.getLangCode(langCode)));
		}

		if (!user.getEmail().equals(email)) {
			throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.EMAIL_NOT_VALID_ERROR, LangHelper.getLangCode(langCode)));
		}

		String defaultRandomPassword  = generateRandomDefaultPassword();
		account.setPassword(passwordEncoder.encode(defaultRandomPassword));

		idsmedAccountRepository.save(account);

		//Prepare and send reset email to queue.
		String firstName = user.getFirstName() != null? user.getFirstName() : "";
		String lastName = user.getLastName() != null? user.getLastName() : "";
		Pair<String, String> resetPasswordEmailPair = emailService.generateUserResetPasswordEmailSubjectAndContent(firstName, lastName, defaultRandomPassword);
		List<String> recipients = new ArrayList<>();
		recipients.add(email);
		String sender = env.getProperty(PropertiesLabelConst.SMTP_SUPPORT_MAIL_LABEL);
		EmailObject resetPasswordEmailObject = new UserResetPasswordEmailObject(recipients, sender, resetPasswordEmailPair);
		rabbitTemplate.convertAndSend(env.getProperty(IdsmedQueueConfig.RABBIT_MAIL_SERVICE_PROCESS_QUEUE), resetPasswordEmailObject);
		loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, RESET_PASSWORD_METHOD_NAME, null);
		return true;
	}

	private String generateRandomDefaultPassword() {
		char[] CHARSET_AZaz_09 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789".toCharArray();
		Random random = new SecureRandom();
		char[] result = new char[7];
		for (int i = 0; i < result.length; i++) {
			int randomCharIndex = random.nextInt(CHARSET_AZaz_09.length);
			result[i] = CHARSET_AZaz_09[randomCharIndex];
		}
		return new String(result);
	}

	public boolean checkPermissionByLoginIdAndPermissionCode(String loginId, String permissionCode) {
		if (StringUtils.isEmpty(loginId) || StringUtils.isEmpty(permissionCode)) {
			return false;
		}
		IdsmedUser user = idsmedUserRepository.findUserByLoginIdAndStatus(loginId, StatusEnum.APPROVED.getCode());
		IdsmedPermission permission = idsmedPermissionRepository.findFirstByCode(permissionCode);
		if (user == null || permission == null) {
			return false;
		}
		List<String> permissonCodes = idsmedPermissionRepository.findAllByUser(user).stream().map(pm -> pm.getCode()).collect(Collectors.toList());
		if (!permissonCodes.contains(permission.getCode())) {
			return false;
		}
		return true;
	}

	public void logUserLoginHistory(String loginId, String ip, Integer loginStatusCode) {
		LogUserLoginHistory logUserLoginHistory = new LogUserLoginHistory();
		logUserLoginHistory.setLoginId(loginId);
		logUserLoginHistory.setIp(ip);
		logUserLoginHistory.setLoginStatus(loginStatusCode);
		logUserLoginHistoryRepository.save(logUserLoginHistory);

	}

	private void loggerForAllMethodInThisService(String methodDescription, String methodName, String loginId){
		logger.info(methodDescription, methodName, loginId != null?loginId:null);
	}
}

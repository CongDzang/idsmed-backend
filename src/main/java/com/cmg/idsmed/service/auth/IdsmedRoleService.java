package com.cmg.idsmed.service.auth;

import java.util.List;

import com.cmg.idsmed.common.exception.IdsmedBaseException;
import com.cmg.idsmed.dto.auth.IdsmedRoleResponse;

public interface IdsmedRoleService {
	List<IdsmedRoleResponse> getRoleList(String loginId) throws IdsmedBaseException;
}

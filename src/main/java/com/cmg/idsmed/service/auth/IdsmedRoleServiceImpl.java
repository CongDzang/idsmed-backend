package com.cmg.idsmed.service.auth;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.cmg.idsmed.common.Constant.LoggerConstant;
import com.cmg.idsmed.common.enums.RoleTypeEnum;
import com.cmg.idsmed.common.enums.StatusEnum;
import com.cmg.idsmed.common.exception.ErrorInfo;
import com.cmg.idsmed.common.exception.IdsmedBaseException;
import com.cmg.idsmed.model.entity.auth.IdsmedUser;
import com.cmg.idsmed.model.repo.user.IdsmedUserRepository;
import com.cmg.idsmed.service.share.MessageResourceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import com.cmg.idsmed.dto.auth.IdsmedRoleResponse;
import com.cmg.idsmed.model.repo.auth.IdsmedRoleRepository;
import org.springframework.util.StringUtils;

@Configuration
@Service
public class IdsmedRoleServiceImpl implements IdsmedRoleService {
	
	private static final Logger logger = LoggerFactory.getLogger(IdsmedRoleServiceImpl.class);
	
	@Autowired
	private IdsmedRoleRepository idsmedRoleRepository;

	@Autowired
	private IdsmedUserRepository userRepository;

	@Autowired
	private MessageResourceService messageResourceService;

	@Autowired
	private Environment env;

	@Value("${logger.start.method.description}")
	String START_METHOD_DESCRIPTION;

	@Value("${logger.end.method.description}")
	String END_METHOD_DESCRIPTION;

	private static final String GET_ROLE_LIST_METHOD_NAME = "getRoleList";
	@Override
	public List<IdsmedRoleResponse> getRoleList(String loginId){
        loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, GET_ROLE_LIST_METHOD_NAME, loginId);
		List<IdsmedRoleResponse> roleListResponse = new ArrayList<>();
		IdsmedUser user = userRepository.findUserByLoginIdWithoutStatus(loginId);
		if (!StringUtils.isEmpty(user.getCompanyCode())) {
			roleListResponse = idsmedRoleRepository
					.findAllRoleByStatusAndType(RoleTypeEnum.EXTERNAL.getCode(), StatusEnum.APPROVED.getCode())
					.stream()
					.map(r -> new IdsmedRoleResponse(r)).collect(Collectors.toList());
		} else {
			roleListResponse = idsmedRoleRepository
					.findAllRoleByStatusAndType(RoleTypeEnum.INTERNAL.getCode(), StatusEnum.APPROVED.getCode())
					.stream()
					.map(r -> new IdsmedRoleResponse(r)).collect(Collectors.toList());
		}
		loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, GET_ROLE_LIST_METHOD_NAME, loginId);
		return roleListResponse;
	}

	private void loggerForAllMethodInThisService(String methodDescription, String methodName, String loginId){
		logger.info(methodDescription, methodName, loginId != null?loginId:null);
	}
}

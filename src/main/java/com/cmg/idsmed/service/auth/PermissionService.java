package com.cmg.idsmed.service.auth;

import org.springframework.security.core.Authentication;

import java.util.List;

public interface PermissionService {
	boolean isMethodPermited (Authentication auth, List<String> permissionCodes);
}

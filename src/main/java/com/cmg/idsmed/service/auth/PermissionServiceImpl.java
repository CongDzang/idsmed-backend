package com.cmg.idsmed.service.auth;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.cmg.idsmed.model.entity.auth.IdsmedPermission;
import com.cmg.idsmed.model.entity.auth.IdsmedUser;
import com.cmg.idsmed.model.repo.auth.IdsmedPermissionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

@Service
@Component("PermissionService")
public class PermissionServiceImpl implements PermissionService {
	@Autowired
	private IdsmedPermissionRepository idsmedPermissionRepository;


	/*
	 * Method to check if one user with list of own permissions can access the API which allowed by a code of permission
	 * Parameter
	 * auth : authentication information of requester (contains list of permission of all the roles he has)
	 * permissionCode: The code of the permission which is set for certain service
	 * @see com.cmg.idsmed.service.auth.PermissionService#isMethodPermited(org.springframework.security.core.Authentication, long[])
	 */
	public boolean isMethodPermited (Authentication auth, List<String> permissionCodes) {
		@SuppressWarnings("unchecked")
		List<SimpleGrantedAuthority> auths = (List<SimpleGrantedAuthority>) auth.getAuthorities();
		List<IdsmedPermission> permissions = new ArrayList<>();
		if (!CollectionUtils.isEmpty(permissionCodes)) {
			for (String permissionCode : permissionCodes) {
				IdsmedPermission permission = idsmedPermissionRepository.findFirstByCode(permissionCode);
				if (permission != null) {
					permissions.add(permission);
				}
			}

		}

		List<Long> ids = permissions.stream().map(p -> p.getId()).collect(Collectors.toList());
		Optional<SimpleGrantedAuthority> isExisted = auths.stream().filter(s -> ids.contains(Long.valueOf(s.getAuthority()))).findAny();

		return isExisted.isPresent();
	}


}

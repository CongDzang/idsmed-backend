package com.cmg.idsmed.service.corporate;

import com.cmg.idsmed.common.exception.IdsmedBaseException;
import com.cmg.idsmed.dto.corporate.CorporateRegistrationRequest;
import com.cmg.idsmed.dto.user.IdsmedCorporateUserResponse;
import org.springframework.web.multipart.MultipartFile;

public interface CorporateService {

	Boolean corporateUserRegister(CorporateRegistrationRequest request, MultipartFile profileImage, MultipartFile frontIdImage, MultipartFile backIdImage) throws IdsmedBaseException;

	Boolean activateCorporatelUser(String uuid, String langCode) throws IdsmedBaseException;
	IdsmedCorporateUserResponse editCorporateUser(CorporateRegistrationRequest request, MultipartFile profileImage, MultipartFile frontIdImage, MultipartFile backIdImage) throws IdsmedBaseException;
}

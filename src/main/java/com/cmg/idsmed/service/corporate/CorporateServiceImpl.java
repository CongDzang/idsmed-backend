package com.cmg.idsmed.service.corporate;

import com.cmg.idsmed.common.Constant.IdsmedRoleCodeConst;
import com.cmg.idsmed.common.Constant.PropertiesLabelConst;
import com.cmg.idsmed.common.enums.IdsmedAccountTypeEnum;
import com.cmg.idsmed.common.enums.StatusEnum;
import com.cmg.idsmed.common.exception.ErrorInfo;
import com.cmg.idsmed.common.exception.IdsmedBaseException;
import com.cmg.idsmed.common.utils.LangHelper;
import com.cmg.idsmed.config.rabbitmq.IdsmedQueueConfig;
import com.cmg.idsmed.dto.corporate.*;
import com.cmg.idsmed.dto.user.IdsmedCorporateUserResponse;
import com.cmg.idsmed.dto.user.IdsmedUserResponse;
import com.cmg.idsmed.dto.user.UserRegistrationRequest;
import com.cmg.idsmed.mail.*;
import com.cmg.idsmed.model.entity.auth.*;
import com.cmg.idsmed.model.entity.corporate.Corporate;
import com.cmg.idsmed.model.entity.corporate.CorporateDepartment;
import com.cmg.idsmed.model.entity.masterdata.Country;
import com.cmg.idsmed.model.entity.masterdata.Province;
import com.cmg.idsmed.model.repo.account.IdsmedAccountSettingsRepository;
import com.cmg.idsmed.model.repo.auth.IdsmedAccountRepository;
import com.cmg.idsmed.model.repo.auth.IdsmedRoleRepository;
import com.cmg.idsmed.model.repo.auth.IdsmedUserRoleRepository;
import com.cmg.idsmed.model.repo.corporate.CorporateDepartmentRepository;
import com.cmg.idsmed.model.repo.corporate.CorporateRepository;
import com.cmg.idsmed.model.repo.masterdata.CountryRepository;
import com.cmg.idsmed.model.repo.masterdata.ProvinceRepository;
import com.cmg.idsmed.model.repo.user.IdsmedUserRepository;
import com.cmg.idsmed.service.share.MessageResourceService;
import com.cmg.idsmed.service.user.IdsmedUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class CorporateServiceImpl implements CorporateService{

	@Autowired
	private CountryRepository countryRepository;

	@Autowired
	private ProvinceRepository provinceRepository;

	@Autowired
	private IdsmedUserRepository idsmedUserRepository;

	@Autowired
	private IdsmedAccountRepository idsmedAccountRepository;

	@Autowired
	private IdsmedUserService idsmedUserService;

	@Autowired
	private IdsmedAccountSettingsRepository idsmedAccountSettingsRepository;

	@Autowired
	private IdsmedRoleRepository idsmedRoleRepository;

	@Autowired
	private IdsmedUserRoleRepository idsmedUserRoleRepository;

	@Autowired
	private EmailService emailService;

	@Autowired
	private Environment env;

	@Autowired
	private RabbitTemplate rabbitTemplate;

	@Autowired
	private MessageResourceService messageResourceService;

	@Autowired
	private CorporateRepository corporateRepository;

	@Autowired
	private CorporateDepartmentRepository corporateDepartmentRepository;

	private static final Logger logger = LoggerFactory.getLogger(CorporateServiceImpl.class);

	public Boolean corporateUserRegister(CorporateRegistrationRequest request, MultipartFile profileImage, MultipartFile frontIdImage, MultipartFile backIdImage) throws IdsmedBaseException {
		if (!validateRequest(request)) {
			throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.USER_REQUEST_INFO_NOT_CORRECT, LangHelper.getLangCode(request.getLangCode())));
		}
		//Generate Corporate
		CorporateRequest corporateRequest = request.getCorporateRequest();
		Corporate corporate = new Corporate(corporateRequest);
		if (corporateRequest.getCountryId() != null) {
			Optional<Country> countryOpt = countryRepository.findById(corporateRequest.getCountryId());
			if (countryOpt.isPresent()) {
				corporate.setCountry(countryOpt.get());
			}
		}

		if (corporateRequest.getProvinceId() != null) {
			Optional<Province> provinceOpt = provinceRepository.findById(corporateRequest.getProvinceId());
			if (provinceOpt.isPresent()) {
				corporate.setProvince(provinceOpt.get());
			}
		}
		corporate.setStatus(StatusEnum.PENDING_FOR_ACTIVATE_BY_REGISTER.getCode());
		String coporateCode = UUID.randomUUID().toString();
		corporate.setCode(coporateCode);
		corporate = corporateRepository.save(corporate);

		//Generate CorporateDepartment
		CorporateDepartment corporateDepartment = new CorporateDepartment(request.getCorporateDepartmentRequest());
		corporateDepartment.setStatus(StatusEnum.PENDING_FOR_ACTIVATE_BY_REGISTER.getCode());
		corporateDepartment.setCorporate(corporate);
		String departmentCode = UUID.randomUUID().toString();
		corporateDepartment.setCode(departmentCode);
		corporateDepartment = corporateDepartmentRepository.save(corporateDepartment);

		//Generate user
		CorporateUserRegistrationRequest userRegistrationRequest = request.getCorporateUserRegistrationRequest();
		if (!idsmedUserService.checkValidNewLoginId(userRegistrationRequest.getEmail())) {
			throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.LOGIN_ID_NOT_VALID_WHEN_REGISTER, LangHelper.getLangCode(request.getLangCode())));
		}
		IdsmedUser corporateUser = new IdsmedUser(userRegistrationRequest);

		corporateUser = idsmedUserService.uploadAndSetImagesForHospitalAndCorporateUser(corporateUser, profileImage, frontIdImage, backIdImage, request.getLangCode());
		corporateUser.setStatus(StatusEnum.PENDING_FOR_ACTIVATE_BY_REGISTER.getCode());
		corporateUser.setCorporate(corporate);
		String uuid = UUID.randomUUID().toString();
		corporateUser.setUuid(uuid);
		corporateUser = idsmedUserRepository.save(corporateUser);

		//Set Role for user
		IdsmedUserRole userRole = new IdsmedUserRole();
		userRole.setIdsmedUser(corporateUser);
		IdsmedRole role =idsmedRoleRepository.findByCode(IdsmedRoleCodeConst.CORPORATE_USER);
		userRole.setIdsmedRole(role);
		userRole.setStatus(StatusEnum.PENDING_FOR_ACTIVATE_BY_REGISTER.getCode());
		userRole = idsmedUserRoleRepository.save(userRole);
		List<IdsmedUserRole> userRoles = new ArrayList<>();
		corporateUser.setUserRoles(userRoles);

		//Generate account
		String loginId = userRegistrationRequest.getEmail();
		IdsmedAccount account = idsmedUserService.generateProperAccount(corporateUser, loginId, userRegistrationRequest.getPassword(), IdsmedAccountTypeEnum.CORPORATE_USER, StatusEnum.PENDING_FOR_ACTIVATE_BY_REGISTER);
		account.setIdsmedUser(corporateUser);

		account = idsmedAccountRepository.save(account);
		List<IdsmedAccount> accounts = new ArrayList<>();
		accounts.add(account);
		corporateUser.setIdsmedAccounts(accounts);

		//Set accountsetting
		IdsmedAccountSettings accSettings = new IdsmedAccountSettings(account, request.getLangCode());
		idsmedAccountSettingsRepository.save(accSettings);

		processSendActivateLinkEmail(corporateUser, request.getLangCode());

		return true;
	}


	@Override
	@Transactional
	public IdsmedCorporateUserResponse editCorporateUser(CorporateRegistrationRequest request, MultipartFile profileImage, MultipartFile frontIdImage, MultipartFile backIdImage) throws IdsmedBaseException {
		String ossBucketDomain = env.getProperty(PropertiesLabelConst.OSS_BUCKET_DOMAIN);
		IdsmedCorporateUserResponse response = new IdsmedCorporateUserResponse();
		CorporateRequest corporateRequest = request.getCorporateRequest();
		UserRegistrationRequest corporateUserRegistrationRequest = request.getCorporateUserRegistrationRequest();
		CorporateDepartmentRequest corporateDepartmentRequest = request.getCorporateDepartmentRequest();

		if (!validateRequest(request)) {
			throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.USER_REQUEST_INFO_NOT_CORRECT, LangHelper.getLangCode(request.getLangCode())));
		}

		if (corporateUserRegistrationRequest.getId() == null) {
			throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.USER_NOT_FOUND_ERROR, LangHelper.getLangCode(request.getLangCode())));
		}
		Optional<IdsmedUser> corporateUserOpt = idsmedUserRepository.findById(corporateUserRegistrationRequest.getId());
		if (!corporateUserOpt.isPresent()) {
			throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.USER_NOT_FOUND_ERROR, LangHelper.getLangCode(request.getLangCode())));
		}

		IdsmedUser corporateUser = corporateUserOpt.get();
		corporateUser = corporateUser.updateRegistrationUser(corporateUserRegistrationRequest);
		corporateUser = idsmedUserService.uploadAndSetImagesForHospitalAndCorporateUser(corporateUser, profileImage, frontIdImage, backIdImage, request.getLangCode());
		corporateUser = idsmedUserRepository.save(corporateUser);

		Country country = countryRepository.getOne(corporateRequest.getCountryId());
		Province province = provinceRepository.getOne(corporateRequest.getProvinceId());

		if (country == null || province == null) {
			throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.COUNTRY_CAN_NOT_FOUND_ERROR, LangHelper.getLangCode(request.getLangCode())));
		}

		Optional<Corporate> corporateOpt = corporateRepository.findById(corporateRequest.getId());
		if (!corporateOpt.isPresent()) {
			throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.CORPORATE_NOT_FOUND, LangHelper.getLangCode(request.getLangCode())));
		}
		Corporate corporate = corporateOpt.get();
		corporate = corporate.update(corporateRequest, country, province);
		corporate = corporateRepository.save(corporate);

		Optional<CorporateDepartment> corporateDepartmentOpt = corporate.getCorporateDepartments().stream().filter(cd -> cd.getId().equals(corporateDepartmentRequest.getId())).findFirst();
		if (!corporateDepartmentOpt.isPresent()) {
			throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.CORPORATE_DEPARMENT_NOT_FOUND, LangHelper.getLangCode(request.getLangCode())));
		}

		CorporateDepartment corporateDepartment = corporateDepartmentOpt.get();
		corporateDepartment = corporateDepartment.update(corporateDepartmentRequest);
		corporateDepartment = corporateDepartmentRepository.save(corporateDepartment);

		IdsmedUserResponse idsmedUserResponse = new IdsmedUserResponse(corporateUser, corporateUser.getUserRoles().stream().map(iur -> iur.getIdsmedRole()).collect(Collectors.toList()), ossBucketDomain);
		if (idsmedUserResponse != null) {
			response.setIdsmedUserResponse(idsmedUserResponse);
		}

		CorporateResponse corporateResponse = new CorporateResponse(corporate);
		response.setCorporateResponse(corporateResponse);
		List<CorporateDepartment> corporateDepartments = corporate.getCorporateDepartments();
		if (!CollectionUtils.isEmpty(corporateDepartments)) {
			response.setCorporateDepartmentResponses(corporateDepartments.stream().map(cd -> new CorporateDepartmentResponse(cd)).collect(Collectors.toList()));
		}
		return response;

	}




	@Override
	@Transactional
	public Boolean activateCorporatelUser(String uuid, String langCode) throws IdsmedBaseException {
		if (StringUtils.isEmpty(uuid)) {
			throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.USER_CAN_NOT_ACTIVATE_ERROR, LangHelper.getLangCode(langCode)));
		}

		IdsmedUser corporateUser = idsmedUserRepository.findFirstByUuid(uuid);
		if (corporateUser == null) {
			throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.USER_CAN_NOT_ACTIVATE_ERROR, LangHelper.getLangCode(langCode)));
		}

		//Activate accounts
		List<IdsmedAccount> accounts = corporateUser.getIdsmedAccounts();
		if (CollectionUtils.isEmpty(accounts)) {
			throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.USER_CAN_NOT_ACTIVATE_ERROR, LangHelper.getLangCode(langCode)));
		}

		accounts.stream().forEach(a -> {
			a.setStatus(StatusEnum.APPROVED.getCode());
		});

		accounts = idsmedAccountRepository.saveAll(accounts);

		//Set account setting
		for (IdsmedAccount account : accounts) {
			IdsmedAccountSettings accountSettings = account.getAccountSettings();
			if (accountSettings != null) {
				accountSettings.setStatus(StatusEnum.APPROVED.getCode());
				idsmedAccountSettingsRepository.save(accountSettings);
			}
		}

		//Activate Corporate
		Corporate corporate = corporateUser!= null? corporateUser.getCorporate() : null;

		if(corporate != null && corporate.getStatus().equals(StatusEnum.PENDING_FOR_ACTIVATE_BY_REGISTER.getCode())) {
			corporate.setStatus(StatusEnum.APPROVED.getCode());
			corporate = corporateRepository.save(corporate);
		}

		//Activate Corporate Departments
		List<CorporateDepartment> corporateDepartments = new ArrayList<>();
		if (corporate != null) {
			corporateDepartments = corporate.getCorporateDepartments();
		}
		if (!CollectionUtils.isEmpty(corporateDepartments)) {
			corporateDepartments.forEach(cd -> {
				if (cd.getStatus().equals(StatusEnum.PENDING_FOR_ACTIVATE_BY_REGISTER.getCode())) {
					cd.setStatus(StatusEnum.APPROVED.getCode());
					corporateDepartmentRepository.save(cd);
				}
			});
		}

		//Activate user
		if(corporateUser.getStatus().equals(StatusEnum.PENDING_FOR_ACTIVATE_BY_REGISTER.getCode())) {
			corporateUser.setStatus(StatusEnum.APPROVED.getCode());
			idsmedUserRepository.save(corporateUser);
		}

		//Activate UserRole
		corporateUser.getUserRoles().forEach(ur -> {
			ur.setStatus(StatusEnum.APPROVED.getCode());
			idsmedUserRoleRepository.save(ur);
		});

		//Send activate success email.
		processSendCorporateUserActivationSuccessEmail(corporateUser);

		return true;
	}

	private void processSendCorporateUserActivationSuccessEmail(IdsmedUser user) throws IdsmedBaseException {
		logger.info("Start sending Corporate User Registration Success email to register",
				user.getFirstName());
		Pair<String, String> emailPair = null;
		emailPair = emailService.generateCorporateUserRegistrationActivationSuccessEmailSubjectAndContent(user);
		List<String> recipients = new ArrayList<>();
		String emailAddress = user.getEmail();
		recipients.add(emailAddress);
		EmailObject emailObject = new CorporateUserRegistrationSuccessEmailObject(recipients, env.getProperty(PropertiesLabelConst.SMTP_SUPPORT_MAIL_LABEL), emailPair);
		rabbitTemplate.convertAndSend(env.getProperty(IdsmedQueueConfig.RABBIT_MAIL_SERVICE_PROCESS_QUEUE), emailObject);

		logger.info("End sending Corporate User Registration Success email to register {}",
				user.getFirstName());
	}

	private void processSendActivateLinkEmail(IdsmedUser user, String langCode) throws IdsmedBaseException {
		// Email Notification Part
		logger.info("Start sending Activate Link Email to register",
				user.getFirstName());

		Pair<String, String> emailPair = null;
		emailPair = emailService.generateCorporateUserRegistrationActivateLinkEmailSubjectAndContent(user, langCode);
		List<String> recipients = new ArrayList<>();
		String emailAddress = user.getEmail();
		recipients.add(emailAddress);
		EmailObject emailObject = new CorporateUserRegistrationActivationLinkEmailObject(recipients, env.getProperty(PropertiesLabelConst.SMTP_SUPPORT_MAIL_LABEL), emailPair);

		rabbitTemplate.convertAndSend(env.getProperty(IdsmedQueueConfig.RABBIT_MAIL_SERVICE_PROCESS_QUEUE), emailObject);

		logger.info("End sending Activate Link Email to register {}",
				user.getFirstName());
	}

	private Boolean validateRequest(CorporateRegistrationRequest request) {
		CorporateRequest corporateRequest = request.getCorporateRequest();
		CorporateUserRegistrationRequest corporateUserRegistrationRequest = request.getCorporateUserRegistrationRequest();
		CorporateDepartmentRequest corporateDepartmentRequest = request.getCorporateDepartmentRequest();
		if (corporateRequest == null || corporateUserRegistrationRequest == null || corporateDepartmentRequest == null) {
			return false;
		}
		//Check Corporate request.
		if (StringUtils.isEmpty(corporateRequest.getNamePrimary())
				|| StringUtils.isEmpty(corporateRequest.getFirstAddress())
				|| corporateRequest.getCountryId() == null
				|| corporateRequest.getProvinceId() == null) {
			return false;
		}

		//Check user request
		if (StringUtils.isEmpty(corporateUserRegistrationRequest.getEmail()) || StringUtils.isEmpty(corporateUserRegistrationRequest.getTitle())
				|| StringUtils.isEmpty(corporateUserRegistrationRequest.getIdCardNumber())
				|| StringUtils.isEmpty(corporateUserRegistrationRequest.getFirstName())
				|| StringUtils.isEmpty(corporateUserRegistrationRequest.getLastName())
				|| StringUtils.isEmpty(corporateUserRegistrationRequest.getMobilePhoneNumber()) || corporateUserRegistrationRequest.getGender() == null) {
			return false;
		}

		//Check Corporate Department
		if (StringUtils.isEmpty(corporateDepartmentRequest.getNamePrimary())) {
			return false;
		}

		return true;
	}
}

package com.cmg.idsmed.service.dashboard;

import com.cmg.idsmed.common.exception.IdsmedBaseException;
import com.cmg.idsmed.dto.dashboard.DashboardMapResponse;
import com.cmg.idsmed.dto.dashboard.DashboardMapResponses;
import com.cmg.idsmed.dto.dashboard.DashboardResponse;
import com.cmg.idsmed.model.entity.vendor.Vendor;

import java.time.ZonedDateTime;
import java.util.Date;
import java.util.List;

public interface DashboardService {
	DashboardResponse getDashboardInfo(List<Long> vendorIds, ZonedDateTime approvedDate) throws IdsmedBaseException;
	DashboardMapResponses getMapInfoByProvinceName() throws IdsmedBaseException;
}

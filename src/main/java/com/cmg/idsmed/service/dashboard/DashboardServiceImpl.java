package com.cmg.idsmed.service.dashboard;

import com.cmg.idsmed.common.Constant.LoggerConstant;
import com.cmg.idsmed.common.enums.StatusEnum;
import com.cmg.idsmed.common.exception.IdsmedBaseException;
import com.cmg.idsmed.common.utils.DateTimeHelper;
import com.cmg.idsmed.dto.dashboard.DashboardBarChartResponse;
import com.cmg.idsmed.dto.dashboard.DashboardMapResponse;
import com.cmg.idsmed.dto.dashboard.DashboardMapResponses;
import com.cmg.idsmed.dto.dashboard.DashboardResponse;
import com.cmg.idsmed.model.entity.masterdata.Province;
import com.cmg.idsmed.model.entity.vendor.Vendor;
import com.cmg.idsmed.model.entity.vendor.VendorAddress;
import com.cmg.idsmed.model.repo.masterdata.ProvinceRepository;
import com.cmg.idsmed.model.repo.procurement.OdBigOrderRepository;
import com.cmg.idsmed.model.repo.procurement.OdSmallOrderRepository;
import com.cmg.idsmed.model.repo.product.ProductRepository;
import com.cmg.idsmed.model.repo.thirdparty.CustomerRepository;
import com.cmg.idsmed.model.repo.vendor.VendorAddressRepository;
import com.cmg.idsmed.model.repo.vendor.VendorRepository;
import com.cmg.idsmed.service.task.IdsmedTaskService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Configuration
@Service
public class DashboardServiceImpl implements DashboardService {
	private static final Logger logger = LoggerFactory.getLogger(DashboardServiceImpl.class);
	@Autowired
	private ProductRepository productRepository;

	@Autowired
	private VendorRepository vendorRepository;

	@Autowired
	private VendorAddressRepository vendorAddressRepository;

	@Autowired
	private IdsmedTaskService idsmedTaskService;

	@Autowired
	private CustomerRepository customerRepository;

	@Autowired
	private OdBigOrderRepository odBigOrderRepository;

	@Autowired
	private ProvinceRepository provinceRepository;

	@Autowired
	private OdSmallOrderRepository odSmallOrderRepository;

	@Autowired
	private Environment env;

	@Value("${logger.start.method.description}")
	String START_METHOD_DESCRIPTION;

	@Value("${logger.end.method.description}")
	String END_METHOD_DESCRIPTION;

	private static final String GET_DASHBOARD_INFO_METHOD_NAME = "getDashboardInfo";
	private static final String GET_MAP_INFO_BY_PROVINCE_NAME_METHOD_NAME = "getMapInfoByProvinceName";

	@Override
	@Transactional
	public DashboardResponse getDashboardInfo(List<Long> vendorIds, ZonedDateTime approvedDate) throws IdsmedBaseException {
		loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, GET_DASHBOARD_INFO_METHOD_NAME, null);

		List<Vendor> vendors = new ArrayList<>();

		if (!CollectionUtils.isEmpty(vendorIds)) {
			vendors = vendorIds.stream().map(id -> vendorRepository.getOne(id)).collect(Collectors.toList());
		}

		LocalDateTime date = approvedDate == null ? DateTimeHelper.getCurrentTimeUTC() : DateTimeHelper.convertZonedDateTimeToUTC(approvedDate);

		Long totalVendor = vendorRepository.countTotalVendors(StatusEnum.APPROVED.getCode()) == null? 0 : vendorRepository.countTotalVendors(StatusEnum.APPROVED.getCode());
		Long totalProduct = productRepository.countTotalProductByVendorAndCreatedDate(vendors, null, StatusEnum.APPROVED.getCode());
		Long totalProductApprovedToday = productRepository.countTotalProductByVendorAndCreatedDate(vendors, date, StatusEnum.APPROVED.getCode());
		List<DashboardBarChartResponse> fiveMonthTotalApprovedProduct = generateListOfTotalApprovedProductForFiveMonth();
		List<DashboardBarChartResponse> fiveMonthTotalCustomer = generateListOfTotalCustomerForFiveMonth();
		List<DashboardBarChartResponse> fiveMonthTotalSales = generateListOfTotalSalesForFiveMonth();
		List<DashboardBarChartResponse> fiveYearTotalRegisteredCustomer = generateListOfTotalRegisteredCustomerForFiveYear();
		List<DashboardBarChartResponse> fiveYearTotalHospital = generateListOfTotalHospitalForFiveYear();
		List<DashboardBarChartResponse> fiveYearTotalSales = generateListOfTotalSalesForFiveYear();


		DashboardResponse dashboardResponse = new DashboardResponse(totalVendor, totalProduct, totalProductApprovedToday,
				fiveMonthTotalApprovedProduct, fiveMonthTotalCustomer, fiveMonthTotalSales, fiveYearTotalRegisteredCustomer,
				fiveYearTotalHospital, fiveYearTotalSales);
		loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, GET_DASHBOARD_INFO_METHOD_NAME, null);
		return dashboardResponse;
	}

	@Override
	public DashboardMapResponses getMapInfoByProvinceName() throws IdsmedBaseException {
		loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, GET_MAP_INFO_BY_PROVINCE_NAME_METHOD_NAME, null);
		List<Vendor> vendors = new ArrayList<>();
		List<Province> provinces = provinceRepository.findAll();
		List<DashboardMapResponse> dashboardMapResponseList = new ArrayList<>();
		Long totalVendor = 0L;
		Long totalProduct = 0L;
		Long totalCustomer = 0L;
		Long totalHospital = 0L;
		Long totalNoOfTransaction = 0L;
		String provinceName = null;
		for(Province province : provinces){
			provinceName = province.getEnglishName();
			// this method is to get count of total vendor by province in vendor address table.
			// logic here is to get total count of distinct vendor id in vendor address table which is match the province given.
			totalVendor = vendorAddressRepository.countTotalVendorByVendorAddressFindByProvince(province);
			// this method is to get count of total product, and use the product relationship with vendor to find the product belong to which vendor
			// After that use vendor relationship with vendor address to find the vendor address which is type 1 to match the province.
			// Product and Vendor status must is approved status.
			totalProduct = productRepository.countTotalApprovedProductByVendorAddressFindByProvince(province);
			totalCustomer = customerRepository.countTotalCustomerByProvinceCodeAndStatusIsApproved(province);
			totalHospital = customerRepository.countTotalHospitalByProvinceCodeGroupByCompanyName(province);
			// This method is to get total number of transaction from od_small_order.
			// get the od_order_receive_address by relationship to find the province match with province given
			totalNoOfTransaction = odSmallOrderRepository.countTotalNoOfTransactionFindByProvince(province);
			DashboardMapResponse dashboardMapResponse = new DashboardMapResponse(provinceName, totalVendor, totalProduct, totalCustomer, totalHospital, totalNoOfTransaction);
			dashboardMapResponseList.add(dashboardMapResponse);
		}

		Long totalNoOfTransactionRegardlessOfProvince = odSmallOrderRepository.countTotalNoOfTransaction();
		DashboardMapResponses dashboardMapResponses = new DashboardMapResponses(dashboardMapResponseList, totalNoOfTransactionRegardlessOfProvince);
		loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, GET_MAP_INFO_BY_PROVINCE_NAME_METHOD_NAME, null);
		return dashboardMapResponses;
	}

	private List<DashboardBarChartResponse> generateListOfTotalApprovedProductForFiveMonth(){
		List<DashboardBarChartResponse> fiveMonthTotalApprovedProduct = new ArrayList<>();
		Calendar calendar = Calendar.getInstance();

		DashboardBarChartResponse dashboardBarChartResponseForCurrentMonth = generateTotalApprovedProductForCurrentMonth(calendar);
        DashboardBarChartResponse dashboardBarChartResponseForPreviousMonth = generateTotalApprovedProductForPreviousMonth(calendar);
        DashboardBarChartResponse dashboardBarChartResponseForPreviousTwoMonth = generateTotalApprovedProductForPreviousMonth(calendar);
        DashboardBarChartResponse dashboardBarChartResponseForPreviousThreeMonth = generateTotalApprovedProductForPreviousMonth(calendar);
        DashboardBarChartResponse dashboardBarChartResponseForPreviousFourMonth = generateTotalApprovedProductForPreviousMonth(calendar);

		fiveMonthTotalApprovedProduct.add(dashboardBarChartResponseForPreviousFourMonth);
		fiveMonthTotalApprovedProduct.add(dashboardBarChartResponseForPreviousThreeMonth);
		fiveMonthTotalApprovedProduct.add(dashboardBarChartResponseForPreviousTwoMonth);
		fiveMonthTotalApprovedProduct.add(dashboardBarChartResponseForPreviousMonth);
		fiveMonthTotalApprovedProduct.add(dashboardBarChartResponseForCurrentMonth);

		return fiveMonthTotalApprovedProduct;
	}

	//generate pair for total Approved Product Month in format example(Mar-19) and totalCount for this month
	private DashboardBarChartResponse generateTotalApprovedProductForCurrentMonth(Calendar calendar){
		Pair<LocalDateTime, LocalDateTime> pairOfFirstDayOfMonthAndLastDayOfMonth = generateFirstDayOfMonthAndLastDayOfMonth(calendar);
		Date dateCurrentMonth = calendar.getTime();
		String dateForCurrentMonth = DateTimeHelper.convertDateToString(dateCurrentMonth, DateTimeHelper.MONTH_YEAR_FORMAT);
		Long totalApprovedProductForCurrentMonth =  productRepository.countTotalApprovedProductBetweenFirstDateAndLastDate(
				pairOfFirstDayOfMonthAndLastDayOfMonth.getFirst(), pairOfFirstDayOfMonthAndLastDayOfMonth.getSecond());
		DashboardBarChartResponse dashboardBarChartResponse = new DashboardBarChartResponse(dateForCurrentMonth,
				totalApprovedProductForCurrentMonth);

		return dashboardBarChartResponse;
	}

	//generate pair totalApprovedProduct and short form for Month in format example(Mar-19) and totalCount for previous 4 month, this method will be call 4 time
	private DashboardBarChartResponse generateTotalApprovedProductForPreviousMonth(Calendar calendar){
		calendar.add(Calendar.MONTH, -1);
		Pair<LocalDateTime, LocalDateTime> pairOfFirstDayOfMonthAndLastDayOfMonth = generateFirstDayOfMonthAndLastDayOfMonth(calendar);
		Date datePreviousMonth = calendar.getTime();
		String dateForPreviousMonth = DateTimeHelper.convertDateToString(datePreviousMonth, DateTimeHelper.MONTH_YEAR_FORMAT);

		Long totalApprovedProductForPreviousMonth =  productRepository.countTotalApprovedProductBetweenFirstDateAndLastDate(
				pairOfFirstDayOfMonthAndLastDayOfMonth.getFirst(), pairOfFirstDayOfMonthAndLastDayOfMonth.getSecond());
		DashboardBarChartResponse dashboardBarChartResponse = new DashboardBarChartResponse(dateForPreviousMonth, totalApprovedProductForPreviousMonth);

		return dashboardBarChartResponse;
	}

	private List<DashboardBarChartResponse> generateListOfTotalCustomerForFiveMonth(){
		List<DashboardBarChartResponse> fiveMonthTotalCustomer = new ArrayList<>();
		Calendar calendar = Calendar.getInstance();

		DashboardBarChartResponse totalCustomerForThisMonth = generatedTotalCustomerForCurrentMonth(calendar);
		DashboardBarChartResponse totalCustomerForPreviousMonth = generateTotalCustomerForPreviousMonth(calendar);
		DashboardBarChartResponse totalCustomerForPreviousTwoMonth = generateTotalCustomerForPreviousMonth(calendar);
		DashboardBarChartResponse totalCustomerForPreviousThreeMonth = generateTotalCustomerForPreviousMonth(calendar);
		DashboardBarChartResponse totalCustomerForPreviousFourMonth = generateTotalCustomerForPreviousMonth(calendar);

		fiveMonthTotalCustomer.add(totalCustomerForPreviousFourMonth);
		fiveMonthTotalCustomer.add(totalCustomerForPreviousThreeMonth);
		fiveMonthTotalCustomer.add(totalCustomerForPreviousTwoMonth);
		fiveMonthTotalCustomer.add(totalCustomerForPreviousMonth);
		fiveMonthTotalCustomer.add(totalCustomerForThisMonth);

		return fiveMonthTotalCustomer;
	}

	//generate pair for total customer for short form for  Month in format example(Mar-19) and totalCount for this month
	private DashboardBarChartResponse generatedTotalCustomerForCurrentMonth(Calendar calendar){
		Pair<LocalDateTime, LocalDateTime> pairOfFirstDayOfMonthAndLastDayOfMonth = generateFirstDayOfMonthAndLastDayOfMonth(calendar);
		Date dateCurrentMonth = calendar.getTime();
		String dateForCurrentMonth = DateTimeHelper.convertDateToString(dateCurrentMonth, DateTimeHelper.MONTH_YEAR_FORMAT);
		Long totalCustomerForCurrentMonth =  customerRepository.countTotalCustomerByCreatedDateBetweenFirstDayAndLastDay(
				pairOfFirstDayOfMonthAndLastDayOfMonth.getFirst(), pairOfFirstDayOfMonthAndLastDayOfMonth.getSecond());
		DashboardBarChartResponse dashboardBarChartResponse = new DashboardBarChartResponse(dateForCurrentMonth, totalCustomerForCurrentMonth);

		return dashboardBarChartResponse;
	}

	//generate pair for total customer Month in format example(Mar-19) and totalCount for previous 4 month, this method will call 4 time
	private DashboardBarChartResponse generateTotalCustomerForPreviousMonth(Calendar calendar){
		calendar.add(Calendar.MONTH, -1);
		Pair<LocalDateTime, LocalDateTime> pairOfFirstDayOfMonthAndLastDayOfMonth = generateFirstDayOfMonthAndLastDayOfMonth(calendar);
		Date datePreviousMonth = calendar.getTime();
		String dateForPreviousMonth = DateTimeHelper.convertDateToString(datePreviousMonth, DateTimeHelper.MONTH_YEAR_FORMAT);

		Long totalCustomerForPreviousMonth =  customerRepository.countTotalCustomerByCreatedDateBetweenFirstDayAndLastDay(
				pairOfFirstDayOfMonthAndLastDayOfMonth.getFirst(), pairOfFirstDayOfMonthAndLastDayOfMonth.getSecond());
		DashboardBarChartResponse dashboardBarChartResponse = new DashboardBarChartResponse(dateForPreviousMonth, totalCustomerForPreviousMonth);

		return dashboardBarChartResponse;
	}

	private List<DashboardBarChartResponse> generateListOfTotalSalesForFiveMonth(){
		List<DashboardBarChartResponse> fiveMonthTotalSales = new ArrayList<>();
		Calendar calendar = Calendar.getInstance();

		DashboardBarChartResponse totalSalesForThisMonth = generatedTotalSalesForCurrentMonth(calendar);
		DashboardBarChartResponse totalSalesForPreviousMonth = generateTotalSalesForPreviousMonth(calendar);
		DashboardBarChartResponse totalSalesForPreviousTwoMonth = generateTotalSalesForPreviousMonth(calendar);
		DashboardBarChartResponse totalSalesForPreviousThreeMonth = generateTotalSalesForPreviousMonth(calendar);
		DashboardBarChartResponse totalSalesForPreviousFourMonth = generateTotalSalesForPreviousMonth(calendar);

		fiveMonthTotalSales.add(totalSalesForPreviousFourMonth);
		fiveMonthTotalSales.add(totalSalesForPreviousThreeMonth);
		fiveMonthTotalSales.add(totalSalesForPreviousTwoMonth);
		fiveMonthTotalSales.add(totalSalesForPreviousMonth);
		fiveMonthTotalSales.add(totalSalesForThisMonth);

		return fiveMonthTotalSales;
	}

	//generate pair for total sales for short form for  Month in format example(Mar-19) and totalCount for this month
	private DashboardBarChartResponse generatedTotalSalesForCurrentMonth(Calendar calendar){
		Pair<LocalDateTime, LocalDateTime> pairOfFirstDayOfMonthAndLastDayOfMonth = generateFirstDayOfMonthAndLastDayOfMonth(calendar);
		Date dateCurrentMonth = calendar.getTime();
		String dateForCurrentMonth = DateTimeHelper.convertDateToString(dateCurrentMonth, DateTimeHelper.MONTH_YEAR_FORMAT);
		BigDecimal totalSalesForCurrentMonth =  odBigOrderRepository.countTotalSalesByCreatedDateBetweenFirstDayAndLastDay(
				pairOfFirstDayOfMonthAndLastDayOfMonth.getFirst(), pairOfFirstDayOfMonthAndLastDayOfMonth.getSecond());
		DashboardBarChartResponse dashboardBarChartResponse = new DashboardBarChartResponse(dateForCurrentMonth, totalSalesForCurrentMonth);

		return dashboardBarChartResponse;
	}

	//generate pair for total sales Month in format example(Mar-19) and totalCount for previous 4 month, this method will call 4 time
	private DashboardBarChartResponse generateTotalSalesForPreviousMonth(Calendar calendar){
		calendar.add(Calendar.MONTH, -1);
		Pair<LocalDateTime, LocalDateTime> pairOfFirstDayOfMonthAndLastDayOfMonth = generateFirstDayOfMonthAndLastDayOfMonth(calendar);
		Date datePreviousMonth = calendar.getTime();
		String dateForPreviousMonth = DateTimeHelper.convertDateToString(datePreviousMonth, DateTimeHelper.MONTH_YEAR_FORMAT);

		BigDecimal totalSalesForPreviousMonth =  odBigOrderRepository.countTotalSalesByCreatedDateBetweenFirstDayAndLastDay(
				pairOfFirstDayOfMonthAndLastDayOfMonth.getFirst(), pairOfFirstDayOfMonthAndLastDayOfMonth.getSecond());
		DashboardBarChartResponse dashboardBarChartResponse = new DashboardBarChartResponse(dateForPreviousMonth, totalSalesForPreviousMonth);

		return dashboardBarChartResponse;
	}

	private List<DashboardBarChartResponse> generateListOfTotalRegisteredCustomerForFiveYear(){
		List<DashboardBarChartResponse> fiveYearTotalRegisteredCustomer = new ArrayList<>();
		Calendar calendar = Calendar.getInstance();
		DashboardBarChartResponse totalCustomerForThisYear = generatedTotalCustomerForCurrentYear(calendar);
		DashboardBarChartResponse totalCustomerForPreviousYear = generateTotalCustomerForPreviousYear(calendar);
		DashboardBarChartResponse totalCustomerForPreviousTwoYear = generateTotalCustomerForPreviousYear(calendar);
		DashboardBarChartResponse totalCustomerForPreviousThreeYear= generateTotalCustomerForPreviousYear(calendar);
		DashboardBarChartResponse totalCustomerForPreviousFourYear = generateTotalCustomerForPreviousYear(calendar);

		fiveYearTotalRegisteredCustomer.add(totalCustomerForPreviousFourYear);
		fiveYearTotalRegisteredCustomer.add(totalCustomerForPreviousThreeYear);
		fiveYearTotalRegisteredCustomer.add(totalCustomerForPreviousTwoYear);
		fiveYearTotalRegisteredCustomer.add(totalCustomerForPreviousYear);
		fiveYearTotalRegisteredCustomer.add(totalCustomerForThisYear);

		return fiveYearTotalRegisteredCustomer;
	}

	//generate total customer  for short form for year in format example(1996) and totalCount for this year
	private DashboardBarChartResponse generatedTotalCustomerForCurrentYear(Calendar calendar){
		Pair<LocalDateTime, LocalDateTime> pairOfFirstDayOfMonthAndLastDayOfYear = generateFirstDayOfYearAndLastDayOfYear(calendar);
		Date dateCurrentYear = calendar.getTime();
		String dateForCurrentYear = DateTimeHelper.convertDateToString(dateCurrentYear, DateTimeHelper.YEAR_FORMAT);
		Long totalCustomerForCurrentYear =  customerRepository.countTotalCustomerByCreatedDateBetweenFirstDayAndLastDay(
				pairOfFirstDayOfMonthAndLastDayOfYear.getFirst(), pairOfFirstDayOfMonthAndLastDayOfYear.getSecond());
		DashboardBarChartResponse dashboardBarChartResponse = new DashboardBarChartResponse(dateForCurrentYear, totalCustomerForCurrentYear);

		return dashboardBarChartResponse;
	}

	//generate total customer in format example(Mar-19) and totalCount for previous 4 month, this method will call 4 time
	private DashboardBarChartResponse generateTotalCustomerForPreviousYear(Calendar calendar){
		calendar.add(Calendar.YEAR, -1);
		Pair<LocalDateTime, LocalDateTime> pairOfFirstDayOfMonthAndLastDayOfYear = generateFirstDayOfYearAndLastDayOfYear(calendar);
		Date datePreviousYear = calendar.getTime();
		String dateForPreviousYear = DateTimeHelper.convertDateToString(datePreviousYear, DateTimeHelper.YEAR_FORMAT);

		Long totalCustomerForCurrentYear =  customerRepository.countTotalCustomerByCreatedDateBetweenFirstDayAndLastDay(
				pairOfFirstDayOfMonthAndLastDayOfYear.getFirst(), pairOfFirstDayOfMonthAndLastDayOfYear.getSecond());
		DashboardBarChartResponse dashboardBarChartResponse = new DashboardBarChartResponse(dateForPreviousYear, totalCustomerForCurrentYear);

		return dashboardBarChartResponse;
	}

	private List<DashboardBarChartResponse> generateListOfTotalHospitalForFiveYear(){
		List<DashboardBarChartResponse> fiveYearTotalHospital = new ArrayList<>();
		Calendar calendar = Calendar.getInstance();
		DashboardBarChartResponse totalHospitalForThisYear = generatedTotalHospitalForCurrentYear(calendar);
		DashboardBarChartResponse totalHospitalForPreviousYear = generateTotalHospitalForPreviousYear(calendar);
		DashboardBarChartResponse totalHospitalForPreviousTwoYear = generateTotalHospitalForPreviousYear(calendar);
		DashboardBarChartResponse totalHospitalForPreviousThreeYear= generateTotalHospitalForPreviousYear(calendar);
		DashboardBarChartResponse totalHospitalForPreviousFourYear = generateTotalHospitalForPreviousYear(calendar);

		fiveYearTotalHospital.add(totalHospitalForPreviousFourYear);
		fiveYearTotalHospital.add(totalHospitalForPreviousThreeYear);
		fiveYearTotalHospital.add(totalHospitalForPreviousTwoYear);
		fiveYearTotalHospital.add(totalHospitalForPreviousYear);
		fiveYearTotalHospital.add(totalHospitalForThisYear);
		return fiveYearTotalHospital;
	}

	//generate total hospital for year in format example(1996) and totalCount for this year
	private DashboardBarChartResponse generatedTotalHospitalForCurrentYear(Calendar calendar){
		Pair<LocalDateTime, LocalDateTime> pairOfFirstDayOfMonthAndLastDayOfYear = generateFirstDayOfYearAndLastDayOfYear(calendar);
		Date dateCurrentYear = calendar.getTime();
		String dateForCurrentYear = DateTimeHelper.convertDateToString(dateCurrentYear, DateTimeHelper.YEAR_FORMAT);
		Long totalHospitalForCurrentYear = customerRepository.countTotalCustomerByCreatedDateBetweenFirstDayAndLastDayGroupByCompanyName(
				pairOfFirstDayOfMonthAndLastDayOfYear.getFirst(), pairOfFirstDayOfMonthAndLastDayOfYear.getSecond());
		DashboardBarChartResponse dashboardBarChartResponse = new DashboardBarChartResponse(dateForCurrentYear, totalHospitalForCurrentYear);

		return dashboardBarChartResponse;
	}

	//generate total hospital in format example(Mar-19) and totalCount for previous 4 month, this method will call 4 time
	private DashboardBarChartResponse generateTotalHospitalForPreviousYear(Calendar calendar){
		calendar.add(Calendar.YEAR, -1);
		Pair<LocalDateTime, LocalDateTime> pairOfFirstDayOfMonthAndLastDayOfYear = generateFirstDayOfYearAndLastDayOfYear(calendar);
		Date datePreviousYear = calendar.getTime();
		String dateForPreviousYear = DateTimeHelper.convertDateToString(datePreviousYear, DateTimeHelper.YEAR_FORMAT);

		Long totalHospitalForCurrentYear = customerRepository.countTotalCustomerByCreatedDateBetweenFirstDayAndLastDayGroupByCompanyName(
				pairOfFirstDayOfMonthAndLastDayOfYear.getFirst(), pairOfFirstDayOfMonthAndLastDayOfYear.getSecond());
		DashboardBarChartResponse dashboardBarChartResponse = new DashboardBarChartResponse(dateForPreviousYear, totalHospitalForCurrentYear);

		return dashboardBarChartResponse;
	}

	private List<DashboardBarChartResponse> generateListOfTotalSalesForFiveYear(){
		List<DashboardBarChartResponse> fiveYearTotalSales = new ArrayList<>();
		Calendar calendar = Calendar.getInstance();
		DashboardBarChartResponse totalSalesForThisYear = generatedTotalSalesForCurrentYear(calendar);
		DashboardBarChartResponse totalSalesForPreviousYear = generateTotalSalesForPreviousYear(calendar);
		DashboardBarChartResponse totalSalesForPreviousTwoYear = generateTotalSalesForPreviousYear(calendar);
		DashboardBarChartResponse totalSalesForPreviousThreeYear= generateTotalSalesForPreviousYear(calendar);
		DashboardBarChartResponse totalSalesForPreviousFourYear = generateTotalSalesForPreviousYear(calendar);

		fiveYearTotalSales.add(totalSalesForPreviousFourYear);
		fiveYearTotalSales.add(totalSalesForPreviousThreeYear);
		fiveYearTotalSales.add(totalSalesForPreviousTwoYear);
		fiveYearTotalSales.add(totalSalesForPreviousYear);
		fiveYearTotalSales.add(totalSalesForThisYear);

		return fiveYearTotalSales;
	}

	//generate total Sales for year in format example(1996) and totalCount for this year
	private DashboardBarChartResponse generatedTotalSalesForCurrentYear(Calendar calendar){
		Pair<LocalDateTime, LocalDateTime> pairOfFirstDayOfMonthAndLastDayOfYear = generateFirstDayOfYearAndLastDayOfYear(calendar);
		Date dateCurrentYear = calendar.getTime();
		String dateForCurrentYear = DateTimeHelper.convertDateToString(dateCurrentYear, DateTimeHelper.YEAR_FORMAT);
		BigDecimal totalSalesForCurrentYear = odBigOrderRepository.countTotalSalesByCreatedDateBetweenFirstDayAndLastDay(
				pairOfFirstDayOfMonthAndLastDayOfYear.getFirst(), pairOfFirstDayOfMonthAndLastDayOfYear.getSecond());
		DashboardBarChartResponse dashboardBarChartResponse = new DashboardBarChartResponse(dateForCurrentYear, totalSalesForCurrentYear);

		return dashboardBarChartResponse;
	}

	//generate total Sales in format example(Mar-19) and totalCount for previous 4 month, this method will call 4 time
	private DashboardBarChartResponse generateTotalSalesForPreviousYear(Calendar calendar){
		calendar.add(Calendar.YEAR, -1);
		Pair<LocalDateTime, LocalDateTime> pairOfFirstDayOfMonthAndLastDayOfYear = generateFirstDayOfYearAndLastDayOfYear(calendar);
		Date datePreviousYear = calendar.getTime();
		String dateForPreviousYear = DateTimeHelper.convertDateToString(datePreviousYear, DateTimeHelper.YEAR_FORMAT);

		BigDecimal totalSalesForCurrentYear = odBigOrderRepository.countTotalSalesByCreatedDateBetweenFirstDayAndLastDay(
				pairOfFirstDayOfMonthAndLastDayOfYear.getFirst(), pairOfFirstDayOfMonthAndLastDayOfYear.getSecond());
		DashboardBarChartResponse dashboardBarChartResponse = new DashboardBarChartResponse(dateForPreviousYear, totalSalesForCurrentYear);

		return dashboardBarChartResponse;
	}

	private Pair<LocalDateTime, LocalDateTime> generateFirstDayOfMonthAndLastDayOfMonth(Calendar calendar){
		Pair<LocalDateTime, LocalDateTime> pairOfFirstDayAndLastDay;
		Calendar startDate = Calendar.getInstance();
		Calendar endDate = Calendar.getInstance();

		startDate.set(Calendar.YEAR, calendar.get(Calendar.YEAR));
		startDate.set(Calendar.MONTH, calendar.get(Calendar.MONTH));
		startDate.set(Calendar.DAY_OF_MONTH, 1);
		startDate.set(Calendar.HOUR_OF_DAY, 0);
		startDate.set(Calendar.MINUTE, 0);
		startDate.set(Calendar.SECOND, 0);
		startDate.set(Calendar.MILLISECOND, 0);

		endDate.set(Calendar.YEAR, calendar.get(Calendar.YEAR));
		endDate.set(Calendar.MONTH, calendar.get(Calendar.MONTH));
		endDate.set(Calendar.DAY_OF_MONTH, endDate.getActualMaximum(Calendar.DAY_OF_MONTH));
		endDate.set(Calendar.HOUR_OF_DAY, 23);
		endDate.set(Calendar.MINUTE, 59);
		endDate.set(Calendar.SECOND, 59);
		endDate.set(Calendar.MILLISECOND, 999);

		LocalDateTime firstDateOfMonth = DateTimeHelper.convertDateToLocalDateTime(startDate.getTime());
		LocalDateTime lastDateOfMonth = DateTimeHelper.convertDateToLocalDateTime(endDate.getTime());

		pairOfFirstDayAndLastDay = Pair.of(firstDateOfMonth, lastDateOfMonth);

		return pairOfFirstDayAndLastDay;
	}

	private Pair<LocalDateTime, LocalDateTime> generateFirstDayOfYearAndLastDayOfYear(Calendar calendar){
		Pair<LocalDateTime, LocalDateTime> pairOfFirstDayAndLastDay;
		Calendar startDate = Calendar.getInstance();
		Calendar endDate = Calendar.getInstance();

		startDate.set(Calendar.YEAR, calendar.get(Calendar.YEAR));
		startDate.set(Calendar.DAY_OF_YEAR, 1);
		startDate.set(Calendar.HOUR_OF_DAY, 0);
		startDate.set(Calendar.MINUTE, 0);
		startDate.set(Calendar.SECOND, 0);
		startDate.set(Calendar.MILLISECOND, 0);

		endDate.set(Calendar.YEAR, calendar.get(Calendar.YEAR));
		endDate.set(Calendar.DAY_OF_YEAR, endDate.getActualMaximum(Calendar.DAY_OF_YEAR));
		endDate.set(Calendar.HOUR_OF_DAY, 23);
		endDate.set(Calendar.MINUTE, 59);
		endDate.set(Calendar.SECOND, 59);
		endDate.set(Calendar.MILLISECOND, 999);

		LocalDateTime firstDateOfMonth = DateTimeHelper.convertDateToLocalDateTime(startDate.getTime());
		LocalDateTime lastDateOfMonth = DateTimeHelper.convertDateToLocalDateTime(endDate.getTime());


		pairOfFirstDayAndLastDay = Pair.of(firstDateOfMonth, lastDateOfMonth);

		return pairOfFirstDayAndLastDay;
	}

	private void loggerForAllMethodInThisService(String methodDescription, String methodName, String loginId){
		logger.info(methodDescription, methodName, loginId != null?loginId:null);
	}
}

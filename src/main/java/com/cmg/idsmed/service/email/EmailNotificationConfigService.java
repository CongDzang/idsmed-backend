package com.cmg.idsmed.service.email;

import com.cmg.idsmed.common.enums.EmailTypeEnum;
import com.cmg.idsmed.common.exception.IdsmedBaseException;
import com.cmg.idsmed.dto.email.EmailNotificationConfigByUserRequest;
import com.cmg.idsmed.dto.email.EmailNotificationConfigListResponse;
import com.cmg.idsmed.dto.email.EmailNotificationConfigResponse;

public interface EmailNotificationConfigService {
	EmailNotificationConfigListResponse getEmailNotificationConfigs(String langCode) throws IdsmedBaseException;
	EmailNotificationConfigResponse getEmailNotificationConfigByEmailType(EmailTypeEnum emailType) throws IdsmedBaseException;
    EmailNotificationConfigListResponse createEmailNotificationConfigByUser(EmailNotificationConfigByUserRequest request) throws IdsmedBaseException;
    EmailNotificationConfigListResponse updateEmailNotificationConfigByUser(EmailNotificationConfigByUserRequest request) throws IdsmedBaseException;
    EmailNotificationConfigListResponse updateEmailNotificationConfigs(EmailNotificationConfigByUserRequest request) throws IdsmedBaseException;
}

package com.cmg.idsmed.service.email;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.cmg.idsmed.common.Constant.LoggerConstant;
import com.cmg.idsmed.common.exception.ErrorInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.cmg.idsmed.common.enums.EmailTypeEnum;
import com.cmg.idsmed.common.exception.IdsmedBaseException;
import com.cmg.idsmed.dto.email.EmailNotificationConfigByUserRequest;
import com.cmg.idsmed.dto.email.EmailNotificationConfigListResponse;
import com.cmg.idsmed.dto.email.EmailNotificationConfigResponse;
import com.cmg.idsmed.model.entity.email.EmailNotificationConfig;
import com.cmg.idsmed.model.entity.email.EmailTemplate;
import com.cmg.idsmed.model.repo.auth.IdsmedAccountRepository;
import com.cmg.idsmed.model.repo.email.EmailNotificationConfigRepository;
import com.cmg.idsmed.model.repo.email.EmailTemplateRepository;
import com.cmg.idsmed.model.repo.user.IdsmedUserRepository;
import com.cmg.idsmed.service.share.MessageResourceService;

@Configuration
@Service
public class EmailNotificationConfigServiceImpl implements EmailNotificationConfigService {

	private static final Logger logger = LoggerFactory.getLogger(EmailNotificationConfigServiceImpl.class);

	@Autowired
	EmailTemplateRepository emailTemplateRepository;
	
	@Autowired
	EmailNotificationConfigRepository emailNotificationConfigRepository;

	@Autowired
	IdsmedAccountRepository idsmedAccountRepository;
	
	@Autowired
	IdsmedUserRepository idsmedUserRepository;

	@Autowired
	MessageResourceService messageResourceService;

	@Autowired
	private Environment env;

    @Value("${logger.start.method.description}")
    String START_METHOD_DESCRIPTION;

    @Value("${logger.end.method.description}")
    String END_METHOD_DESCRIPTION;
    private static final String GET_EMAIL_NOTIFICATION_CONFIGS_METHOD_NAME = "getEmailNotificationConfigs";
    private static final String GET_EMAIL_NOTIFICATION_BY_EMAIL_TYPE_METHOD_NAME = "getEmailNotificationConfigByEmailType";
    private static final String CREATE_EMAIL_NOTIFICATION_CONFIG_BY_USER_METHOD_NAME = "createEmailNotificationConfigByUser";
    private static final String UPDATE_EMAIL_NOTIFICATION_CONFIG_BY_USER_METHOD_NAME = "updateEmailNotificationConfigByUser";
    private static final String UPDATE_EMAIL_NOTIFICATION_CONFIGS_METHOD_NAME = "updateEmailNotificationConfigs";

	@Override
	public EmailNotificationConfigListResponse getEmailNotificationConfigs(String langCode) throws IdsmedBaseException {
        loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, GET_EMAIL_NOTIFICATION_CONFIGS_METHOD_NAME, null);
		List<EmailNotificationConfig> emailConfigs = emailNotificationConfigRepository.findAllOrderByNotificationName();
		if (CollectionUtils.isEmpty(emailConfigs)) {
			throw new IdsmedBaseException(messageResourceService.getErrorInfo(
					ErrorInfo.EMAIL_NOTIFICATION_CONFIGURATION_NOT_FOUND, langCode
			));
		}

		emailConfigs.stream().map(ec -> {
			ec.setNotificationName(messageResourceService.getMessage(ec.getNotificationName(), langCode));
			return ec;
		}).collect(Collectors.toList());

		EmailNotificationConfigListResponse response = new EmailNotificationConfigListResponse(emailConfigs);

        loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, GET_EMAIL_NOTIFICATION_CONFIGS_METHOD_NAME, null);
		return response;
	}
	
	@Override
	public EmailNotificationConfigResponse getEmailNotificationConfigByEmailType(EmailTypeEnum emailType) throws IdsmedBaseException {
        loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, GET_EMAIL_NOTIFICATION_BY_EMAIL_TYPE_METHOD_NAME, null);
		Optional<EmailNotificationConfig> emailConfig = emailNotificationConfigRepository.findByEmailType(emailType);
        loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, GET_EMAIL_NOTIFICATION_BY_EMAIL_TYPE_METHOD_NAME, null);

		return emailConfig.isPresent() ? new EmailNotificationConfigResponse(emailConfig.get()) : new EmailNotificationConfigResponse();
	}

	@Transactional
	@Override
	public EmailNotificationConfigListResponse createEmailNotificationConfigByUser(
			EmailNotificationConfigByUserRequest request) throws IdsmedBaseException {
        loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, CREATE_EMAIL_NOTIFICATION_CONFIG_BY_USER_METHOD_NAME, request.getLoginId());
		List<EmailNotificationConfig> tobeStoredEmailConfigs = new ArrayList<EmailNotificationConfig> ();
		request.getEmailConfigs().stream().forEach(ecr -> {
			EmailTemplate emailTemplate = emailTemplateRepository.getOne(ecr.getTemplateId());
			
			if (emailTemplate != null) {
				EmailNotificationConfig emailConfig = new EmailNotificationConfig();
				
				emailConfig.setEnabled(ecr.getEnabled());
				emailConfig.setRecipientList(ecr.getRecipientList());
				emailConfig.setEmailType(getEmailTypeByCode(ecr.getEmailType()));
				emailConfig.setEmailTemplate(emailTemplate);
				
				tobeStoredEmailConfigs.add(emailConfig);
			}
		});
		
		EmailNotificationConfigListResponse response = new EmailNotificationConfigListResponse(emailNotificationConfigRepository.saveAll(tobeStoredEmailConfigs));

        loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, CREATE_EMAIL_NOTIFICATION_CONFIG_BY_USER_METHOD_NAME, request.getLoginId());
		return response;
	}
	
	private EmailTypeEnum getEmailTypeByCode (Integer code) {
		for (EmailTypeEnum emailType : EmailTypeEnum.values()) {
			if (emailType.getCode() == code) {
				return emailType;
			}
		}
		return null;
	}

	@Transactional
	@Override
	public EmailNotificationConfigListResponse updateEmailNotificationConfigByUser(
			EmailNotificationConfigByUserRequest request) throws IdsmedBaseException {
        loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, UPDATE_EMAIL_NOTIFICATION_CONFIG_BY_USER_METHOD_NAME, request.getLoginId());

		EmailNotificationConfigListResponse response = null;
		
		List<EmailNotificationConfig> tobeStoredEmailConfigs = new ArrayList<EmailNotificationConfig> ();
		
		if (!CollectionUtils.isEmpty(request.getEmailConfigs())) {
			request.getEmailConfigs().stream().forEach(ecr -> {
				Optional<EmailNotificationConfig> existedEmailConfig = emailNotificationConfigRepository.findById(ecr.getId());
				
				if (existedEmailConfig.isPresent()) {
					EmailTemplate emailTemplate = emailTemplateRepository.getOne(ecr.getTemplateId());
					if (emailTemplate != null) {
						
						existedEmailConfig.get().setEnabled(ecr.getEnabled());
						existedEmailConfig.get().setRecipientList(ecr.getRecipientList());
						existedEmailConfig.get().setEmailType(getEmailTypeByCode(ecr.getEmailType()));
						existedEmailConfig.get().setEmailTemplate(emailTemplate);
						
						tobeStoredEmailConfigs.add(existedEmailConfig.get());
					}
				}
			});
			
			response = new EmailNotificationConfigListResponse(emailNotificationConfigRepository.saveAll(tobeStoredEmailConfigs));
		}

        loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, UPDATE_EMAIL_NOTIFICATION_CONFIG_BY_USER_METHOD_NAME, request.getLoginId());

		return response;
	}

	/*
	 * This method is doing 2 task
	 * Create a new record in database if no existing, update existing record if id not null
	 */
	@Transactional
	@Override
	public EmailNotificationConfigListResponse updateEmailNotificationConfigs(
			EmailNotificationConfigByUserRequest request) throws IdsmedBaseException {
        loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, UPDATE_EMAIL_NOTIFICATION_CONFIGS_METHOD_NAME, request.getLoginId());

		List<EmailNotificationConfig> createEmailConfig = new ArrayList<>();
		List<EmailNotificationConfig> updateEmailConfig = new ArrayList<>();

		Long loginId = idsmedAccountRepository.findByLoginId(request.getLoginId()).getId();

		request.getEmailConfigs()
				.stream()
				.forEach(ecr -> {

					EmailTemplate emailTemplate;

					if (ecr.getId() != null) {
						if (ecr.getTemplateId() != null) {
							EmailNotificationConfig existedEmailConfig = emailNotificationConfigRepository
									.findById(ecr.getId()).get();

							if (existedEmailConfig != null) {
								emailTemplate = emailTemplateRepository.getOne(ecr.getTemplateId());
								if (emailTemplate != null) {
									existedEmailConfig.setEmailTemplate(emailTemplate);
									existedEmailConfig.setEmailType(getEmailTypeByCode(ecr.getEmailType()));
									existedEmailConfig.setEnabled(ecr.getEnabled());
									existedEmailConfig.setRecipientList(ecr.getRecipientList());
									existedEmailConfig.setCountryCode(ecr.getCountryCode());
									existedEmailConfig.setUpdatedBy(loginId);

									updateEmailConfig.add(existedEmailConfig);
								}
							}
						}
					} else {
						if (ecr.getEnabled() && ecr.getTemplateId() != null) {
							emailTemplate = emailTemplateRepository.getOne(ecr.getTemplateId());

							EmailNotificationConfig emailConfig = new EmailNotificationConfig(
									ecr.getNotificationName(),
									ecr.getNotificationConfigType(),
									getEmailTypeByCode(ecr.getEmailType()),
									ecr.getRecipientList(),
									ecr.getEnabled(),
									emailTemplate,
									ecr.getCountryCode()
							);

							emailConfig.setCreatedBy(loginId);
							emailConfig.setUpdatedBy(loginId);

							createEmailConfig.add(emailConfig);
						}
					}
				});

		if (!CollectionUtils.isEmpty(createEmailConfig)) {
			emailNotificationConfigRepository.saveAll(createEmailConfig);
		}

		if (!CollectionUtils.isEmpty(updateEmailConfig)) {
			emailNotificationConfigRepository.saveAll(updateEmailConfig);
		}

		EmailNotificationConfigListResponse response = new EmailNotificationConfigListResponse();

        loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, UPDATE_EMAIL_NOTIFICATION_CONFIGS_METHOD_NAME, request.getLoginId());
		return response;
	}
    private void loggerForAllMethodInThisService(String methodDescription, String methodName, String loginId){
        logger.info(methodDescription, methodName, loginId != null?loginId:null);
    }
}

package com.cmg.idsmed.service.email;

import com.cmg.idsmed.common.exception.IdsmedBaseException;
import com.cmg.idsmed.dto.email.EmailTemplateListResponse;
import com.cmg.idsmed.dto.email.EmailTemplateRequest;
import com.cmg.idsmed.dto.email.EmailTemplateResponse;

public interface EmailTemplateService {

    EmailTemplateResponse createEmailTemplate(EmailTemplateRequest request) throws IdsmedBaseException;
    EmailTemplateResponse getTemplateById(Long id, String langCode) throws IdsmedBaseException;
    EmailTemplateListResponse getTemplateList(String templateName, String subject, Integer status, Integer pageIndex, Integer pageSize, String langCode) throws IdsmedBaseException;
    EmailTemplateListResponse getEmailTemplates() throws IdsmedBaseException;
    EmailTemplateResponse editEmailTemplate(EmailTemplateRequest request) throws IdsmedBaseException;
    EmailTemplateResponse inactiveEmailTemplate(EmailTemplateRequest request) throws IdsmedBaseException;

}

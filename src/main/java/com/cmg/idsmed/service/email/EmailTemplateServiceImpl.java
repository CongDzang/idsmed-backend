package com.cmg.idsmed.service.email;

import com.cmg.idsmed.common.Constant.LoggerConstant;
import com.cmg.idsmed.common.exception.ErrorInfo;
import com.cmg.idsmed.common.exception.IdsmedBaseException;
import com.cmg.idsmed.common.utils.LangHelper;
import com.cmg.idsmed.dto.email.EmailTemplateListResponse;
import com.cmg.idsmed.dto.email.EmailTemplateRequest;
import com.cmg.idsmed.dto.email.EmailTemplateResponse;
import com.cmg.idsmed.model.entity.email.EmailNotificationConfig;
import com.cmg.idsmed.model.entity.email.EmailTemplate;
import com.cmg.idsmed.model.repo.auth.IdsmedAccountRepository;
import com.cmg.idsmed.model.repo.email.EmailNotificationConfigRepository;
import com.cmg.idsmed.model.repo.email.EmailTemplateRepository;
import com.cmg.idsmed.service.share.MessageResourceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Configuration
@Service
public class EmailTemplateServiceImpl implements EmailTemplateService {

    private static final Logger logger = LoggerFactory.getLogger(EmailTemplateServiceImpl.class);

    @Autowired
    EmailTemplateRepository emailTemplateRepository;

    @Autowired
    IdsmedAccountRepository idsmedAccountRepository;

    @Autowired
    MessageResourceService messageResourceService;

    @Autowired
    EmailNotificationConfigRepository emailNotificationConfigRepository;

    @Autowired
    private Environment env;

    @Value("${logger.start.method.description}")
    String START_METHOD_DESCRIPTION;

    @Value("${logger.end.method.description}")
    String END_METHOD_DESCRIPTION;
    private static final String CREATE_EMAIL_TEMPLATE_METHOD_NAME = "createEmailTemplate";
    private static final String GET_TEMPLATE_BY_ID_METHOD_NAME = "getTemplateById";
    private static final String GET_TEMPLATE_LIST_METHOD_NAME = "getTemplateList";
    private static final String GET_EMAIL_TEMPLATES_METHOD_NAME = "getEmailTemplates";
    private static final String EDIT_EMAIL_TEMPLATE_METHOD_NAME = "editEmailTemplate";
    private static final String INACTIVE_EMAIL_TEMPLATE_METHOD_NAME = "inactiveEmailTemplate";

    @Transactional
    @Override
    public EmailTemplateResponse createEmailTemplate(EmailTemplateRequest request) throws IdsmedBaseException {
        loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, CREATE_EMAIL_TEMPLATE_METHOD_NAME, request.getLoginId());
        // Retrieve template list for validation with new creation data
        List<EmailTemplate> emailTemplates = emailTemplateRepository.findAll().stream()
                .filter(et -> {
                    if (et.getTemplateName().equalsIgnoreCase(request.getTemplateName()))
                        return true;
                    else
                        return false;
                }).collect(Collectors.toList());

        if (!CollectionUtils.isEmpty(emailTemplates)) {
            throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.EMAIL_TEMPLATE_NAME_EXIST_ERROR,
                    LangHelper.getLangCode(request.getLangCode())));
        }

        // Add new template if no exists
        EmailTemplate emailTemplate = new EmailTemplate();
        String[] ignoreProperties = {"id"};
        BeanUtils.copyProperties(request, emailTemplate, ignoreProperties);

        Long loginId = idsmedAccountRepository.findByLoginId(request.getLoginId()).getId();

        emailTemplate.setTemplateName(request.getTemplateName());
        emailTemplate.setSubject(request.getSubject());
        emailTemplate.setContent(request.getContent());
        emailTemplate.setCreatedBy(loginId);
        emailTemplate.setUpdatedBy(loginId);
        emailTemplate.setStatus(EmailTemplate.EmailTemplateStatusEnum.ACTIVE.getCode());

        emailTemplateRepository.save(emailTemplate);

        EmailTemplateResponse response = new EmailTemplateResponse(emailTemplate);
        loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, CREATE_EMAIL_TEMPLATE_METHOD_NAME, request.getLoginId());
        return response;
    }

    @Override
    public EmailTemplateResponse getTemplateById(Long id, String langCode) throws IdsmedBaseException {
        loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, GET_TEMPLATE_BY_ID_METHOD_NAME, null);

        EmailTemplate template = emailTemplateRepository.getOne(id);
        if (template == null) {
            throw new IdsmedBaseException(messageResourceService
                    .getErrorInfo(ErrorInfo.EMAIL_TEMPLATE_ID_NOT_FOUND_ERROR, LangHelper.getLangCode(langCode)));
        }

        loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, GET_TEMPLATE_BY_ID_METHOD_NAME, null);
        return new EmailTemplateResponse(template);
    }

    @Override
    public EmailTemplateListResponse getTemplateList(String templateName,
                                                     String subject,
                                                     Integer status,
                                                     Integer pageIndex,
                                                     Integer pageSize,
                                                     String langCode) throws IdsmedBaseException {
        loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, GET_TEMPLATE_LIST_METHOD_NAME, null);

        EmailTemplateListResponse emailTemplateListResponse = new EmailTemplateListResponse(pageIndex, pageSize);

        Pair<Integer, List<EmailTemplate>> emailTemplates = emailTemplateRepository
                .searchEmailTemplateByCustomCondition(templateName, subject, status, pageIndex, pageSize);

        if (CollectionUtils.isEmpty(emailTemplates.getSecond())) {
            return emailTemplateListResponse;
        }

        int emailTemplateCount = emailTemplates.getFirst();

        List<EmailTemplateResponse> emailTemplateResponses = emailTemplates.getSecond()
                .stream()
                .map(et -> new EmailTemplateResponse(et))
                .collect(Collectors.toList());

        if (pageIndex != null && pageSize != null) {
            emailTemplateListResponse = new EmailTemplateListResponse(emailTemplateCount, pageIndex, pageSize, emailTemplateResponses);
        } else {
            emailTemplateListResponse = new EmailTemplateListResponse(emailTemplateCount, emailTemplateResponses);
        }

        loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, GET_TEMPLATE_LIST_METHOD_NAME, null);

        return emailTemplateListResponse;

    }

    @Override
    public EmailTemplateListResponse getEmailTemplates() throws IdsmedBaseException {
        loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, GET_EMAIL_TEMPLATES_METHOD_NAME, null);

        List<EmailTemplate> emailTemplateList = emailTemplateRepository.findByStatus(EmailTemplate.EmailTemplateStatusEnum.ACTIVE.getCode());

        List<EmailTemplateResponse> emailTemplateResponseList = null;
        if (!CollectionUtils.isEmpty(emailTemplateList)) {
            emailTemplateResponseList = emailTemplateList
                    .stream()
                    .filter(tl -> {
                        if (!CollectionUtils.isEmpty(emailTemplateList)) return true;
                        return false;
                    })
                    .map(tl -> {
                        EmailTemplateResponse emailTemplateResponse = new EmailTemplateResponse(tl);

                        return emailTemplateResponse;
                    }).collect(Collectors.toList());
        }

        EmailTemplateListResponse response = new EmailTemplateListResponse(emailTemplateResponseList);

        loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, GET_EMAIL_TEMPLATES_METHOD_NAME, null);

        return response;
    }

    @Override
    public EmailTemplateResponse editEmailTemplate(EmailTemplateRequest request) throws IdsmedBaseException {
        loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, EDIT_EMAIL_TEMPLATE_METHOD_NAME, request.getLoginId());

        EmailTemplate emailTemplate = emailTemplateRepository.getOne(request.getId());
        if (emailTemplate == null) {
            logger.error("Email template not exists");
            throw new IdsmedBaseException(messageResourceService.getErrorInfo(
                    ErrorInfo.EMAIL_TEMPLATE_ID_NOT_FOUND_ERROR, LangHelper.getLangCode(request.getLangCode())));
        }

        Long loginId = idsmedAccountRepository.findByLoginId(request.getLoginId()).getId();

        emailTemplate.setTemplateName(request.getTemplateName());
        emailTemplate.setSubject(request.getSubject());
        emailTemplate.setContent(request.getContent());
        emailTemplate.setUpdatedBy(loginId);
        emailTemplateRepository.save(emailTemplate);

        EmailTemplateResponse response = new EmailTemplateResponse(emailTemplate);

        loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, EDIT_EMAIL_TEMPLATE_METHOD_NAME, request.getLoginId());

        return response;
    }


    @Override
    public EmailTemplateResponse inactiveEmailTemplate(EmailTemplateRequest request) throws IdsmedBaseException {
        loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, INACTIVE_EMAIL_TEMPLATE_METHOD_NAME, request.getLoginId());

        // To check is email template exists
        Optional<EmailTemplate> emailTemplate = emailTemplateRepository.findById(request.getId());
        if (!emailTemplate.isPresent()) {
            logger.error("Email template not exists");
            throw new IdsmedBaseException(messageResourceService.getErrorInfo(
                    ErrorInfo.EMAIL_TEMPLATE_ID_NOT_FOUND_ERROR,
                    LangHelper.getLangCode(request.getLangCode())));
        }

        // To check is email template using by any email notification
        EmailNotificationConfig occupiedEmailTemplate = emailNotificationConfigRepository
                .findByEmailTemplateId(request.getId());

        if (occupiedEmailTemplate != null && occupiedEmailTemplate.getEnabled()) {
            logger.error("Email template attached to active notification. Inactive email template is not available");
            throw new IdsmedBaseException(messageResourceService.getErrorInfo(
                    ErrorInfo.EMAIL_TEMPLATE_ATTACHED_TO_ACTIVE_NOTIFICATION,
                    LangHelper.getLangCode(request.getLangCode())));
        }

        // get login user id
        Long loginId = idsmedAccountRepository.findByLoginId(request.getLoginId()).getId();

        EmailTemplate template = emailTemplate.get();
        template.setUpdatedBy(loginId);
        template.setStatus(EmailTemplate.EmailTemplateStatusEnum.INACTIVE.getCode());

        emailTemplateRepository.save(template);

        EmailTemplateResponse response = new EmailTemplateResponse(template);

        loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, INACTIVE_EMAIL_TEMPLATE_METHOD_NAME, request.getLoginId());

        return response;
    }
    private void loggerForAllMethodInThisService(String methodDescription, String methodName, String loginId){
        logger.info(methodDescription, methodName, loginId != null?loginId:null);
    }
}

package com.cmg.idsmed.service.email;

import com.cmg.idsmed.common.exception.IdsmedBaseException;
import com.cmg.idsmed.dto.email.EmailWildcardListResponse;

public interface EmailWildcardService {

    EmailWildcardListResponse getEmailWildcardList(String langCode) throws IdsmedBaseException;

}

package com.cmg.idsmed.service.email;

import com.cmg.idsmed.common.Constant.LoggerConstant;
import com.cmg.idsmed.common.exception.ErrorInfo;
import com.cmg.idsmed.common.exception.IdsmedBaseException;
import com.cmg.idsmed.common.utils.LangHelper;
import com.cmg.idsmed.dto.email.EmailWildcardListResponse;
import com.cmg.idsmed.dto.email.EmailWildcardResponse;
import com.cmg.idsmed.model.entity.email.EmailWildcard;
import com.cmg.idsmed.model.repo.email.EmailWildcardRepository;
import com.cmg.idsmed.service.share.MessageResourceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.stream.Collectors;

@Configuration
@Service
public class EmailWildcardServiceImpl implements EmailWildcardService {

    private static final Logger logger = LoggerFactory.getLogger(EmailWildcardServiceImpl.class);

    @Autowired
    EmailWildcardRepository emailWildcardRepository;

    @Autowired
    MessageResourceService messageResourceService;

    @Autowired
    private Environment env;

    @Value("${logger.start.method.description}")
    String START_METHOD_DESCRIPTION;

    @Value("${logger.end.method.description}")
    String END_METHOD_DESCRIPTION;
    private static final String GET_EMAIL_WILDCARD_LIST_METHOD_NAME = "getEmailWildcardList";

    @Transactional
    @Override
    public EmailWildcardListResponse getEmailWildcardList(String langCode) throws IdsmedBaseException {
        loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, GET_EMAIL_WILDCARD_LIST_METHOD_NAME, null);

        List<EmailWildcard> emailWildcardList = emailWildcardRepository.findAllByOrderByWildcardNameAsc();

        if (CollectionUtils.isEmpty(emailWildcardList)) {
            throw new IdsmedBaseException(messageResourceService.getErrorInfo(
                    ErrorInfo.EMAIL_WILDCARD_NOT_FOUND_ERROR,
                    LangHelper.getLangCode(langCode)));
        }

        List<EmailWildcardResponse> emailWildcardResponseList = emailWildcardList
                .stream()
                .map(ew -> {
                    EmailWildcardResponse emailWildcardResponse = new EmailWildcardResponse(
                            ew.getId(),
                            ew.getWildcardName(),
                            ew.getWildcardValue()
                    );

                    return emailWildcardResponse;

                }).collect(Collectors.toList());

        EmailWildcardListResponse response = new EmailWildcardListResponse(emailWildcardResponseList);

        loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, GET_EMAIL_WILDCARD_LIST_METHOD_NAME, null);
        return response;
    }
    private void loggerForAllMethodInThisService(String methodDescription, String methodName, String loginId){
        logger.info(methodDescription, methodName, loginId != null?loginId:null);
    }
}

package com.cmg.idsmed.service.error;

import com.cmg.idsmed.common.exception.IdsmedBaseException;

public interface LogErrorApiService
{
    public void saveLogError(String loginId, String errorCode, String description, String url, String ip, String browser, String os) throws IdsmedBaseException;
}
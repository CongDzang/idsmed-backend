package com.cmg.idsmed.service.error;

import com.cmg.idsmed.common.Constant.LoggerConstant;
import com.cmg.idsmed.common.exception.IdsmedBaseException;
import com.cmg.idsmed.model.entity.error.LogErrorApi;
import com.cmg.idsmed.model.repo.error.ApiErrorRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

@Configuration
@Service
public class LogErrorApiServiceImpl implements LogErrorApiService
{
    private static final Logger logger = LoggerFactory.getLogger(LogErrorApiServiceImpl.class);

    @Autowired
    private ApiErrorRepository apiErrorRepository;

    @Autowired
    private Environment env;

    @Value("${logger.start.method.description}")
    String START_METHOD_DESCRIPTION;

    @Value("${logger.end.method.description}")
    String END_METHOD_DESCRIPTION;
    private static final String SAVE_LOG_ERROR_METHOD_NAME = "saveLogError";

    @Override
    public void saveLogError(String loginId, String errorCode, String description, String url,
                             String ip, String browser, String os) throws IdsmedBaseException
    {
        loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, SAVE_LOG_ERROR_METHOD_NAME, loginId);
        logger.debug(String.format( "LogErrorApiServiceImpl.saveApiError\n" + "%s, %s, %s, %s, %s, %s, %s",
                                    loginId, errorCode, description, url, ip, browser, os));
        LogErrorApi apiError = new LogErrorApi();

        apiError.setLoginId(loginId);
        apiError.setErrorCode(errorCode);
        apiError.setDescription(description);
        apiError.setUrl(url);
        apiError.setIp(ip);
        apiError.setBrowser(browser);
        apiError.setOs(os);
        apiErrorRepository.save(apiError);
        loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, SAVE_LOG_ERROR_METHOD_NAME, loginId);
    }
    private void loggerForAllMethodInThisService(String methodDescription, String methodName, String loginId){
        logger.info(methodDescription, methodName, loginId != null?loginId:null);
    }
}
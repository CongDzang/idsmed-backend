package com.cmg.idsmed.service.hospital;

import com.cmg.idsmed.common.exception.IdsmedBaseException;
import com.cmg.idsmed.dto.hospital.HospitalRegistrationRequest;
import com.cmg.idsmed.dto.user.IdsmedHospitalUserResponse;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import java.util.Optional;

public interface HospitalService {
	Boolean hospitalRegister(HospitalRegistrationRequest request, MultipartFile profileImage, MultipartFile frontIdImage, MultipartFile backIdImage) throws IdsmedBaseException;

	IdsmedHospitalUserResponse editHospitalUser(HospitalRegistrationRequest request, MultipartFile profileImage, MultipartFile frontIdImage, MultipartFile backIdImage) throws IdsmedBaseException;

	Boolean activateHospitalUser(String uuid, String langCode) throws IdsmedBaseException;
}

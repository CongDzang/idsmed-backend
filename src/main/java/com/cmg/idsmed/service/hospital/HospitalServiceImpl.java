package com.cmg.idsmed.service.hospital;

import com.cmg.idsmed.common.Constant.IdsmedRoleCodeConst;
import com.cmg.idsmed.common.Constant.PropertiesLabelConst;
import com.cmg.idsmed.common.enums.IdsmedAccountTypeEnum;
import com.cmg.idsmed.common.enums.StatusEnum;
import com.cmg.idsmed.common.exception.ErrorInfo;
import com.cmg.idsmed.common.exception.IdsmedBaseException;
import com.cmg.idsmed.common.utils.LangHelper;
import com.cmg.idsmed.config.rabbitmq.IdsmedQueueConfig;
import com.cmg.idsmed.dto.hospital.*;
import com.cmg.idsmed.dto.user.IdsmedHospitalUserResponse;
import com.cmg.idsmed.dto.user.IdsmedUserResponse;
import com.cmg.idsmed.dto.user.UserRegistrationRequest;
import com.cmg.idsmed.mail.EmailObject;
import com.cmg.idsmed.mail.EmailService;
import com.cmg.idsmed.mail.HospitalUserRegistrationActivationLinkEmailObject;
import com.cmg.idsmed.mail.HospitalUserRegistrationSuccessEmailObject;
import com.cmg.idsmed.model.entity.auth.*;
import com.cmg.idsmed.model.entity.hospital.Hospital;
import com.cmg.idsmed.model.entity.hospital.HospitalDepartment;
import com.cmg.idsmed.model.entity.masterdata.Country;
import com.cmg.idsmed.model.entity.masterdata.Province;
import com.cmg.idsmed.model.repo.account.IdsmedAccountSettingsRepository;
import com.cmg.idsmed.model.repo.auth.IdsmedAccountRepository;
import com.cmg.idsmed.model.repo.auth.IdsmedRoleRepository;
import com.cmg.idsmed.model.repo.auth.IdsmedUserRoleRepository;
import com.cmg.idsmed.model.repo.hospital.HospitalDepartmentRepository;
import com.cmg.idsmed.model.repo.hospital.HospitalRepository;
import com.cmg.idsmed.model.repo.masterdata.CountryRepository;
import com.cmg.idsmed.model.repo.masterdata.ProvinceRepository;
import com.cmg.idsmed.model.repo.user.IdsmedUserRepository;
import com.cmg.idsmed.service.share.MessageResourceService;
import com.cmg.idsmed.service.user.IdsmedUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.util.*;
import java.util.stream.Collectors;

@Configuration
@Service
public class HospitalServiceImpl implements HospitalService{
	@Autowired
	private CountryRepository countryRepository;

	@Autowired
	private ProvinceRepository provinceRepository;

	@Autowired
	private IdsmedUserRepository idsmedUserRepository;

	@Autowired
	private HospitalRepository hospitalRepository;

	@Autowired
	private IdsmedAccountRepository idsmedAccountRepository;

	@Autowired
	private IdsmedUserService idsmedUserService;

	@Autowired
	private IdsmedAccountSettingsRepository idsmedAccountSettingsRepository;

	@Autowired
	private IdsmedRoleRepository idsmedRoleRepository;

	@Autowired
	private IdsmedUserRoleRepository idsmedUserRoleRepository;

	@Autowired
	private EmailService emailService;

	@Autowired
	private Environment env;

	@Autowired
	private RabbitTemplate rabbitTemplate;

	@Autowired
	private MessageResourceService messageResourceService;

	@Autowired
	private HospitalDepartmentRepository hospitalDepartmentRepository;

	private static final Logger logger = LoggerFactory.getLogger(HospitalServiceImpl.class);

	@Value("${logger.start.method.description}")
	String START_METHOD_DESCRIPTION;

	@Value("${logger.end.method.description}")
	String END_METHOD_DESCRIPTION;
	private static final String HOSPITAL_REGISTER_METHOD_NAME = "hospitalRegister";
	private static final String ACTIVATE_HOSPITAL_USER_METHOD_NAME = "activateHospitalUser";

	@Override
	@Transactional
	public Boolean hospitalRegister(HospitalRegistrationRequest request
			, MultipartFile profileImage, MultipartFile frontIdImage, MultipartFile backIdImage) throws IdsmedBaseException {
		if (!validateRequest(request)) {
			throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.USER_REQUEST_INFO_NOT_CORRECT, LangHelper.getLangCode(request.getLangCode())));
		}
		//Generate hospital
		loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, HOSPITAL_REGISTER_METHOD_NAME, request.getLoginId());
		HospitalRequest hospitalRequest = request.getHospitalRequest();
		Hospital hospital = new Hospital(hospitalRequest);
		if (hospitalRequest.getCountryId() != null) {
			Optional<Country> countryOpt = countryRepository.findById(hospitalRequest.getCountryId());
			if (countryOpt.isPresent()) {
				hospital.setCountry(countryOpt.get());
			}
		}

		if (hospitalRequest.getProvinceId() != null) {
			Optional<Province> provinceOpt = provinceRepository.findById(hospitalRequest.getProvinceId());
			if (provinceOpt.isPresent()) {
				hospital.setProvince(provinceOpt.get());
			}
		}
		hospital.setStatus(StatusEnum.PENDING_FOR_ACTIVATE_BY_REGISTER.getCode());
		String hospitalCode = UUID.randomUUID().toString();
		hospital.setCode(hospitalCode);
		hospital = hospitalRepository.save(hospital);

		//Generate HospitalDepartment
		HospitalDepartment hospitalDepartment = new HospitalDepartment(request.getHospitalDepartmentRequest());
		hospitalDepartment.setStatus(StatusEnum.PENDING_FOR_ACTIVATE_BY_REGISTER.getCode());
		hospitalDepartment.setHospital(hospital);
		String departmentCode = UUID.randomUUID().toString();
		hospitalDepartment.setCode(departmentCode);
		hospitalDepartment = hospitalDepartmentRepository.save(hospitalDepartment);

		//Generate user

		HospitalUserRegistrationRequest userRegistrationRequest = request.getHospitalUserRegistrationRequest();
		if (!idsmedUserService.checkValidNewLoginId(userRegistrationRequest.getEmail())) {
			throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.LOGIN_ID_NOT_VALID_WHEN_REGISTER, LangHelper.getLangCode(request.getLangCode())));
		}
		IdsmedUser hospitalUser = new IdsmedUser(userRegistrationRequest);

		hospitalUser = idsmedUserService.uploadAndSetImagesForHospitalAndCorporateUser(hospitalUser, profileImage, frontIdImage, backIdImage, request.getLangCode());
		hospitalUser.setStatus(StatusEnum.PENDING_FOR_ACTIVATE_BY_REGISTER.getCode());
		hospitalUser.setHospital(hospital);
		String uuid = UUID.randomUUID().toString();
		hospitalUser.setUuid(uuid);
		hospitalUser = idsmedUserRepository.save(hospitalUser);

		//Set Role for user
		IdsmedUserRole userRole = new IdsmedUserRole();
		userRole.setIdsmedUser(hospitalUser);
		IdsmedRole role =idsmedRoleRepository.findByCode(IdsmedRoleCodeConst.HOSPITAL_USER);
		userRole.setIdsmedRole(role);
		userRole.setStatus(StatusEnum.PENDING_FOR_ACTIVATE_BY_REGISTER.getCode());
		userRole = idsmedUserRoleRepository.save(userRole);
		List<IdsmedUserRole> userRoles = new ArrayList<>();
		hospitalUser.setUserRoles(userRoles);

		//Generate account
		String loginId = userRegistrationRequest.getEmail();
		IdsmedAccount account = idsmedUserService.generateProperAccount(hospitalUser, loginId, userRegistrationRequest.getPassword(), IdsmedAccountTypeEnum.HOSPITAL_USER, StatusEnum.PENDING_FOR_ACTIVATE_BY_REGISTER);
		account.setIdsmedUser(hospitalUser);

		account = idsmedAccountRepository.save(account);
		List<IdsmedAccount> accounts = new ArrayList<>();
		accounts.add(account);
		hospitalUser.setIdsmedAccounts(accounts);

		//Set accountsetting
		IdsmedAccountSettings accSettings = new IdsmedAccountSettings(account, request.getLangCode());
		idsmedAccountSettingsRepository.save(accSettings);

		processSendActivateLinkEmail(hospitalUser, request.getLangCode());

		loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, HOSPITAL_REGISTER_METHOD_NAME, request.getLoginId());
		return true;
	}

	@Override
	@Transactional
	public IdsmedHospitalUserResponse editHospitalUser(HospitalRegistrationRequest request, MultipartFile profileImage, MultipartFile frontIdImage, MultipartFile backIdImage) throws IdsmedBaseException {
		String ossBucketDomain = env.getProperty(PropertiesLabelConst.OSS_BUCKET_DOMAIN);
		IdsmedHospitalUserResponse response = new IdsmedHospitalUserResponse();
		HospitalRequest hospitalRequest = request.getHospitalRequest();
		UserRegistrationRequest hospitalUserRegistrationRequest = request.getHospitalUserRegistrationRequest();
		HospitalDepartmentRequest hospitalDepartmentRequest = request.getHospitalDepartmentRequest();

		if (!validateRequest(request)) {
			throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.USER_REQUEST_INFO_NOT_CORRECT, LangHelper.getLangCode(request.getLangCode())));
		}

		if (hospitalUserRegistrationRequest.getId() == null) {
			throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.USER_NOT_FOUND_ERROR, LangHelper.getLangCode(request.getLangCode())));
		}
		Optional<IdsmedUser> hospitalUserOpt = idsmedUserRepository.findById(hospitalUserRegistrationRequest.getId());
		if (!hospitalUserOpt.isPresent()) {
			throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.USER_NOT_FOUND_ERROR, LangHelper.getLangCode(request.getLangCode())));
		}

		IdsmedUser hospitalUser = hospitalUserOpt.get();
		hospitalUser = hospitalUser.updateRegistrationUser(hospitalUserRegistrationRequest);
		hospitalUser = idsmedUserService.uploadAndSetImagesForHospitalAndCorporateUser(hospitalUser, profileImage, frontIdImage, backIdImage, request.getLangCode());
		hospitalUser = idsmedUserRepository.save(hospitalUser);

		Country country = countryRepository.getOne(hospitalRequest.getCountryId());
		Province province = provinceRepository.getOne(hospitalRequest.getProvinceId());

		if (country == null || province == null) {
			throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.COUNTRY_CAN_NOT_FOUND_ERROR, LangHelper.getLangCode(request.getLangCode())));
		}

		Optional<Hospital> hospitalOpt = hospitalRepository.findById(hospitalRequest.getId());
		if (!hospitalOpt.isPresent()) {
			throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.HOSPITAL_NOT_FOUND, LangHelper.getLangCode(request.getLangCode())));
		}
		Hospital hospital = hospitalOpt.get();
		hospital = hospital.update(hospitalRequest, country, province);
		hospital = hospitalRepository.save(hospital);

		Optional<HospitalDepartment> hospitalDepartmentOpt = hospital.getHospitalDepartments().stream().filter(h -> h.getId().equals(hospitalDepartmentRequest.getId())).findFirst();
		if (!hospitalDepartmentOpt.isPresent()) {
			throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.HOSPITAL_DEPARMENT_NOT_FOUND, LangHelper.getLangCode(request.getLangCode())));
		}

		HospitalDepartment hospitalDepartment = hospitalDepartmentOpt.get();
		hospitalDepartment = hospitalDepartment.update(hospitalDepartmentRequest);
		hospitalDepartment = hospitalDepartmentRepository.save(hospitalDepartment);

		IdsmedUserResponse idsmedUserResponse = new IdsmedUserResponse(hospitalUser, hospitalUser.getUserRoles().stream().map(iur -> iur.getIdsmedRole()).collect(Collectors.toList()), ossBucketDomain);
		if (idsmedUserResponse != null) {
			response.setIdsmedUserResponse(idsmedUserResponse);
		}

		HospitalResponse hospitalResponse = new HospitalResponse(hospital);
		response.setHospitalResponse(hospitalResponse);
		List<HospitalDepartment> hospitalDepartments = hospital.getHospitalDepartments();
		if (!CollectionUtils.isEmpty(hospitalDepartments)) {
			response.setHospitalDepartmentResponse(hospitalDepartments.stream().map(hd -> new HospitalDepartmentResponse(hd)).collect(Collectors.toList()));
		}
	return response;

	}

	private Boolean validateRequest(HospitalRegistrationRequest request) {
		HospitalRequest hospitalRequest = request.getHospitalRequest();
		HospitalUserRegistrationRequest hospitalUserRegistrationRequest = request.getHospitalUserRegistrationRequest();
		HospitalDepartmentRequest hospitalDepartmentRequest = request.getHospitalDepartmentRequest();
		if (hospitalRequest == null || hospitalUserRegistrationRequest == null || hospitalDepartmentRequest == null) {
			return false;
		}
		//Check hospital request.
		if (StringUtils.isEmpty(hospitalRequest.getNamePrimary())
				|| StringUtils.isEmpty(hospitalRequest.getFirstAddress())
				|| hospitalRequest.getCountryId() == null
				|| hospitalRequest.getProvinceId() == null) {
			return false;
		}

		//Check user request
		if (StringUtils.isEmpty(hospitalUserRegistrationRequest.getEmail()) || StringUtils.isEmpty(hospitalUserRegistrationRequest.getTitle())
				|| StringUtils.isEmpty(hospitalUserRegistrationRequest.getIdCardNumber())
				|| StringUtils.isEmpty(hospitalUserRegistrationRequest.getFirstName())
				|| StringUtils.isEmpty(hospitalUserRegistrationRequest.getLastName())
				|| StringUtils.isEmpty(hospitalUserRegistrationRequest.getMobilePhoneNumber()) || hospitalUserRegistrationRequest.getGender() == null) {
			return false;
		}

		//Check Hospital Department
		if (StringUtils.isEmpty(hospitalDepartmentRequest.getNamePrimary())) {
			return false;
		}

		return true;
	}

	@Transactional
	public Boolean activateHospitalUser(String uuid, String langCode) throws IdsmedBaseException {
		loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, ACTIVATE_HOSPITAL_USER_METHOD_NAME, null);
		if (StringUtils.isEmpty(uuid)) {
			throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.USER_CAN_NOT_ACTIVATE_ERROR, LangHelper.getLangCode(langCode)));
		}

		IdsmedUser hospitalUser = idsmedUserRepository.findFirstByUuid(uuid);
		if (hospitalUser == null) {
			throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.USER_CAN_NOT_ACTIVATE_ERROR, LangHelper.getLangCode(langCode)));
		}

		//Activate accounts
		List<IdsmedAccount> accounts = hospitalUser.getIdsmedAccounts();
		if (CollectionUtils.isEmpty(accounts)) {
			throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.USER_CAN_NOT_ACTIVATE_ERROR, LangHelper.getLangCode(langCode)));
		}

		accounts.stream().forEach(a -> {
			a.setStatus(StatusEnum.APPROVED.getCode());
		});

		accounts = idsmedAccountRepository.saveAll(accounts);

		//Set account setting
		for (IdsmedAccount account : accounts) {
			IdsmedAccountSettings accountSettings = account.getAccountSettings();
			if (accountSettings != null) {
				accountSettings.setStatus(StatusEnum.APPROVED.getCode());
				idsmedAccountSettingsRepository.save(accountSettings);
			}
		}

		//Activate Hospital
		Hospital hospital = hospitalUser!= null? hospitalUser.getHospital() : null;

		if(hospital != null && hospital.getStatus().equals(StatusEnum.PENDING_FOR_ACTIVATE_BY_REGISTER.getCode())) {
			hospital.setStatus(StatusEnum.APPROVED.getCode());
			hospital = hospitalRepository.save(hospital);
		}

		//Activate Hospital Departments
		List<HospitalDepartment> hospitalDepartments = new ArrayList<>();
		if (hospital != null) {
			hospitalDepartments = hospital.getHospitalDepartments();
		}
		if (!CollectionUtils.isEmpty(hospitalDepartments)) {
			hospitalDepartments.forEach(hd -> {
				if (hd.getStatus().equals(StatusEnum.PENDING_FOR_ACTIVATE_BY_REGISTER.getCode())) {
					hd.setStatus(StatusEnum.APPROVED.getCode());
					hospitalDepartmentRepository.save(hd);
				}
			});
		}

		//Activate user
		if(hospitalUser.getStatus().equals(StatusEnum.PENDING_FOR_ACTIVATE_BY_REGISTER.getCode())) {
			hospitalUser.setStatus(StatusEnum.APPROVED.getCode());
			idsmedUserRepository.save(hospitalUser);
		}

		//Activate UserRole
		hospitalUser.getUserRoles().forEach(ur -> {
			ur.setStatus(StatusEnum.APPROVED.getCode());
			idsmedUserRoleRepository.save(ur);
		});

		//Send activate success email.
		processSendHospitalUserActivationSuccessEmail(hospitalUser);
		loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, ACTIVATE_HOSPITAL_USER_METHOD_NAME, null);

		return true;
	}

	private void processSendActivateLinkEmail(IdsmedUser user, String langCode) throws IdsmedBaseException {
		// Email Notification Part
		logger.info("Start sending Activate Link Email to register",
				user.getFirstName());

		Pair<String, String> emailPair = null;
		emailPair = emailService.generateHospitalUserRegistrationActivateLinkEmailSubjectAndContent(user, langCode);
		List<String> recipients = new ArrayList<>();
		String emailAddress = user.getEmail();
		recipients.add(emailAddress);
		EmailObject emailObject = new HospitalUserRegistrationActivationLinkEmailObject(recipients, env.getProperty(PropertiesLabelConst.SMTP_SUPPORT_MAIL_LABEL), emailPair);

		rabbitTemplate.convertAndSend(env.getProperty(IdsmedQueueConfig.RABBIT_MAIL_SERVICE_PROCESS_QUEUE), emailObject);

		logger.info("End sending Activate Link Email to register {}",
				user.getFirstName());
	}

	private void processSendHospitalUserActivationSuccessEmail(IdsmedUser user) throws IdsmedBaseException {
		logger.info("Start sending Hospital User Registration Success email to register",
				user.getFirstName());
		Pair<String, String> emailPair = null;
		emailPair = emailService.generateHospitalUserRegistrationActivationSuccessEmailSubjectAndContent(user);
		List<String> recipients = new ArrayList<>();
		String emailAddress = user.getEmail();
		recipients.add(emailAddress);
		EmailObject emailObject = new HospitalUserRegistrationSuccessEmailObject(recipients, env.getProperty(PropertiesLabelConst.SMTP_SUPPORT_MAIL_LABEL), emailPair);
		rabbitTemplate.convertAndSend(env.getProperty(IdsmedQueueConfig.RABBIT_MAIL_SERVICE_PROCESS_QUEUE), emailObject);

		logger.info("End sending Hospital User Registration Success email to register {}",
				user.getFirstName());
	}
	private void loggerForAllMethodInThisService(String methodDescription, String methodName, String loginId){
		logger.info(methodDescription, methodName, loginId != null?loginId:null);
	}
}

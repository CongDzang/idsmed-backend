package com.cmg.idsmed.service.masterdata;

import com.cmg.idsmed.common.exception.IdsmedBaseException;
import com.cmg.idsmed.dto.masterdata.AccountNotificationSettingResponse;

import java.util.List;
import java.util.Map;

public interface AccountNotificationSettingService {
    public List<AccountNotificationSettingResponse> getAccountNotificationSettingConstantMap(Integer type, String loginId, String langCode) throws IdsmedBaseException;
}

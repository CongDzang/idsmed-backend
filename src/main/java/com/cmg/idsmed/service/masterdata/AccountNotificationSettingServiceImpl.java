package com.cmg.idsmed.service.masterdata;

import com.cmg.idsmed.common.enums.EmailTypeEnum;
import com.cmg.idsmed.common.enums.SCPUserAccountNotificationSettingEnum;
import com.cmg.idsmed.common.enums.TypeOfUserEnum;
import com.cmg.idsmed.common.enums.VendorUserAccountNotificationSettingEnum;
import com.cmg.idsmed.common.exception.IdsmedBaseException;
import com.cmg.idsmed.dto.masterdata.AccountNotificationSettingResponse;
import com.cmg.idsmed.model.entity.auth.IdsmedAccount;
import com.cmg.idsmed.model.entity.auth.IdsmedUser;
import com.cmg.idsmed.model.entity.email.EmailNotificationConfig;
import com.cmg.idsmed.model.repo.auth.IdsmedAccountRepository;
import com.cmg.idsmed.model.repo.email.EmailNotificationConfigRepository;
import com.cmg.idsmed.model.repo.user.IdsmedUserRepository;
import com.cmg.idsmed.service.share.MessageResourceService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.util.*;

@Configuration
@Service
public class AccountNotificationSettingServiceImpl implements AccountNotificationSettingService {
    private static final Logger logger = LoggerFactory.getLogger(AccountNotificationSettingServiceImpl.class);

    @Autowired
    IdsmedAccountRepository idsmedAccountRepository;

    @Autowired
    IdsmedUserRepository idsmedUserRepository;

    @Autowired
    private Environment env;

    @Autowired
    private EmailNotificationConfigRepository emailNotificationConfigRepository;

    @Autowired
    private MessageResourceService messageResourceService;

    @Value("${logger.start.method.description}")
    String START_METHOD_DESCRIPTION;

    @Value("${logger.end.method.description}")
    String END_METHOD_DESCRIPTION;
    private static final String GET_ACCOUNT_NOTIFICATION_SETTING_CONSTANT_MAP_METHOD_NAME = "getAccountNotificationSettingConstantMap";

    // if login id is null, then it will get base on the type of user which match typeofuserenum:
    // for example: 1: SCP user will return scpUserAccountSetting constants map.
    // 2: Vendor User will return vendorUserAccountSetting constants map
    // if login id is not null, then it will get base on idsmed_user which is get from idsmed_account
    // criteria is when the idsmed_user have company code then return vendorUserAccountSettingconstantsmap.
    // When the idsmed_user don't have company code then return SCPUserAccountSettingConstantsMap
    // default will return null.
    @Override
    public List<AccountNotificationSettingResponse> getAccountNotificationSettingConstantMap(Integer type, String loginId, String langCode) throws IdsmedBaseException {
        loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, GET_ACCOUNT_NOTIFICATION_SETTING_CONSTANT_MAP_METHOD_NAME, loginId);
        List<AccountNotificationSettingResponse> accountNotificationSettingResponseList = new ArrayList<>();
        if(StringUtils.isEmpty(loginId)){
            if(type.equals(TypeOfUserEnum.SCP_USER.getCode())){
                accountNotificationSettingResponseList = generateAccountNotificationSettingResponseListForSCPUser(langCode);
                return accountNotificationSettingResponseList;
            }
            if(type.equals(TypeOfUserEnum.VENDOR_USER.getCode())) {
                accountNotificationSettingResponseList = generateAccountNotificationSettingResponseListForVendorUser(langCode);
                return accountNotificationSettingResponseList;
            }
        }
        if(!StringUtils.isEmpty(loginId)){
            IdsmedAccount idsmedAccount = idsmedAccountRepository.findByLoginId(loginId);
            IdsmedUser idsmedUser = idsmedAccount.getIdsmedUser();
            if(StringUtils.isEmpty(idsmedUser.getCompanyCode())){
               accountNotificationSettingResponseList = generateAccountNotificationSettingResponseListForSCPUser(langCode);
               return accountNotificationSettingResponseList;
            }
            if(!StringUtils.isEmpty(idsmedUser.getCompanyCode())){
                accountNotificationSettingResponseList = generateAccountNotificationSettingResponseListForVendorUser(langCode);
                return accountNotificationSettingResponseList;
            }
        }
        loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, GET_ACCOUNT_NOTIFICATION_SETTING_CONSTANT_MAP_METHOD_NAME, loginId);

        return null;
    }

    private List<AccountNotificationSettingResponse> generateAccountNotificationSettingResponseListForSCPUser(String langCode){
        Map<Integer, String> scpUserNotificationMap = generateAccountNotificationHashMapForSCPUser(langCode);
        List<AccountNotificationSettingResponse> accountNotificationSettingResponseList = new ArrayList<>();
        for (Map.Entry<Integer, String> entry : scpUserNotificationMap.entrySet()) {
            AccountNotificationSettingResponse accountNotificationSettingResponse =
                    new AccountNotificationSettingResponse(entry.getKey(), entry.getValue());
            accountNotificationSettingResponseList.add(accountNotificationSettingResponse);
        }
        return accountNotificationSettingResponseList;
    }

    private Map<Integer, String> generateAccountNotificationHashMapForSCPUser(String langCode){
        Map<Integer, String> scpUserNotificationMap = new HashMap<>();
        for(SCPUserAccountNotificationSettingEnum suanse : SCPUserAccountNotificationSettingEnum.values()){
            for (EmailTypeEnum ete : EmailTypeEnum.values()) {
                if (ete.getCode() == suanse.getCode()) {
                    Optional<EmailNotificationConfig> emailNotificationConfig = emailNotificationConfigRepository.findByEmailType(ete);
                    EmailNotificationConfig enc = emailNotificationConfig.get();
                    scpUserNotificationMap.put(suanse.getCode(), messageResourceService.getMessage(enc.getNotificationName(), langCode));
                }
            }

//            if(langCode.equalsIgnoreCase(LangEnum.ENGLISH.getCode())){
//                scpUserNotificationMap.put(suanse.getCode(), suanse.getName());
//            }
//
//            if(langCode.equalsIgnoreCase(LangEnum.CHINA.getCode())){
//                scpUserNotificationMap.put(suanse.getCode(), suanse.getName_zh_cn());
//            }
//
//            if(langCode.equalsIgnoreCase(LangEnum.CHINA_TAIWAN.getCode())){
//                scpUserNotificationMap.put(suanse.getCode(), suanse.getName_zh_tw());
//            }
//
//            if(langCode.equalsIgnoreCase(LangEnum.THAI.getCode())){
//                scpUserNotificationMap.put(suanse.getCode(), suanse.getName_th());
//            }
//
//            if(langCode.equalsIgnoreCase(LangEnum.INDONESIAN.getCode())){
//                scpUserNotificationMap.put(suanse.getCode(), suanse.getName_id());
//            }
//
//            if(langCode.equalsIgnoreCase(LangEnum.VIETNAMESE.getCode())){
//                scpUserNotificationMap.put(suanse.getCode(), suanse.getName_vi());
//            }
        }
        return scpUserNotificationMap;
    }

    private List<AccountNotificationSettingResponse> generateAccountNotificationSettingResponseListForVendorUser(String langCode){
        Map<Integer, String> vendorUserNotificationMap = generateAccountNotificationHashMapForVendorUser(langCode);
        List<AccountNotificationSettingResponse> accountNotificationSettingResponseList = new ArrayList<>();
        for (Map.Entry<Integer, String> entry : vendorUserNotificationMap.entrySet()) {
            AccountNotificationSettingResponse accountNotificationSettingResponse =
                    new AccountNotificationSettingResponse(entry.getKey(), entry.getValue());
            accountNotificationSettingResponseList.add(accountNotificationSettingResponse);
        }
        return accountNotificationSettingResponseList;
    }

    private Map<Integer, String> generateAccountNotificationHashMapForVendorUser(String langCode){
        Map<Integer, String> vendorUserNotificationMap = new HashMap<>();
        for(VendorUserAccountNotificationSettingEnum suanse : VendorUserAccountNotificationSettingEnum.values()){
            for (EmailTypeEnum ete : EmailTypeEnum.values()) {
                if (ete.getCode() == suanse.getCode()) {
                    Optional<EmailNotificationConfig> emailNotificationConfig = emailNotificationConfigRepository.findByEmailType(ete);
                    EmailNotificationConfig enc = emailNotificationConfig.get();
                    vendorUserNotificationMap.put(suanse.getCode(), messageResourceService.getMessage(enc.getNotificationName(), langCode));
                }
            }

//            if(langCode.equalsIgnoreCase(LangEnum.ENGLISH.getCode())){
//                vendorUserNotificationMap.put(suanse.getCode(), suanse.getName());
//            }
//
//            if(langCode.equalsIgnoreCase(LangEnum.CHINA.getCode())){
//                vendorUserNotificationMap.put(suanse.getCode(), suanse.getName_zh_cn());
//            }
//
//            if(langCode.equalsIgnoreCase(LangEnum.CHINA_TAIWAN.getCode())){
//                vendorUserNotificationMap.put(suanse.getCode(), suanse.getName_zh_tw());
//            }
//
//            if(langCode.equalsIgnoreCase(LangEnum.THAI.getCode())){
//                vendorUserNotificationMap.put(suanse.getCode(), suanse.getName_th());
//            }
//
//            if(langCode.equalsIgnoreCase(LangEnum.INDONESIAN.getCode())){
//                vendorUserNotificationMap.put(suanse.getCode(), suanse.getName_id());
//            }
//
//            if(langCode.equalsIgnoreCase(LangEnum.VIETNAMESE.getCode())){
//                vendorUserNotificationMap.put(suanse.getCode(), suanse.getName_vi());
//            }
        }
        return vendorUserNotificationMap;
    }
    private void loggerForAllMethodInThisService(String methodDescription, String methodName, String loginId){
        logger.info(methodDescription, methodName, loginId != null?loginId:null);
    }
}

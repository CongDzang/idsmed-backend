package com.cmg.idsmed.service.masterdata;

import java.util.List;

import com.cmg.idsmed.common.exception.IdsmedBaseException;
import com.cmg.idsmed.dto.product.ProductCareAreaListResponse;
import com.cmg.idsmed.dto.product.ProductCareAreaResponse;

public interface CareAreaService {
	ProductCareAreaListResponse getCareAreaList(Integer pageIndex, Integer pageSize, String langCode) throws IdsmedBaseException;
	List<ProductCareAreaResponse> getFavouriteCareAreaList(Long userId, String langCode) throws IdsmedBaseException;
	List<ProductCareAreaResponse> getCareAreaList(String langCode) throws IdsmedBaseException;
}

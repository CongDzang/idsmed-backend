package com.cmg.idsmed.service.masterdata;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cmg.idsmed.common.Constant.LoggerConstant;
import com.cmg.idsmed.common.Constant.PropertiesLabelConst;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.util.Pair;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import com.cmg.idsmed.common.Constant.ProductParameterConstant;
import com.cmg.idsmed.common.enums.HTTPServletReturnStatusEnum;
import com.cmg.idsmed.common.enums.MethodNameEnum;
import com.cmg.idsmed.common.enums.StatusEnum;
import com.cmg.idsmed.common.exception.IdsmedBaseException;
import com.cmg.idsmed.dto.product.ProductCareAreaListResponse;
import com.cmg.idsmed.dto.product.ProductCareAreaResponse;
import com.cmg.idsmed.model.entity.masterdata.FavouriteProductCareArea;
import com.cmg.idsmed.model.entity.product.ProductCareArea;
import com.cmg.idsmed.model.repo.masterdata.FavouriteProductCareAreaRepository;
import com.cmg.idsmed.model.repo.product.ProductCareAreaRepository;
import com.cmg.idsmed.model.repo.product.ProductRepository;
import com.cmg.idsmed.service.product.ProductServiceImpl;
import com.cmg.idsmed.service.share.WebServiceLogService;

@Configuration
@Service
public class CareAreaServiceImpl implements CareAreaService {
    private static final Logger logger = LoggerFactory.getLogger(CareAreaServiceImpl.class);
    
    @Autowired
    private ProductRepository productRepository;
    
    @Autowired
    private ProductCareAreaRepository productCareAreaRepository;
    
    @Autowired
    private FavouriteProductCareAreaRepository favouriteProductCareAreaRepository;
    
    @Autowired
    private HttpServletRequest httpServletRequest;
    
    @Autowired
    private WebServiceLogService webServiceLogService;

    @Autowired
	private Environment env;

	@Value("${logger.start.method.description}")
	String START_METHOD_DESCRIPTION;

	@Value("${logger.end.method.description}")
	String END_METHOD_DESCRIPTION;
	private static final String GET_CARE_AREA_LIST_METHOD_NAME = "getCareAreaList";
	private static final String GET_FAVOURITE_CARE_AREA_LIST_METHOD_NAME = "getFavouriteCareAreaList";

	@Override
	@PreAuthorize("@PermissionService.isMethodPermited(authentication,  {'API0000004'})")
	public ProductCareAreaListResponse getCareAreaList(Integer pageIndex, Integer pageSize, String langCode) throws IdsmedBaseException {
		loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, GET_CARE_AREA_LIST_METHOD_NAME, null);
		String ossBucketDomain = env.getProperty(PropertiesLabelConst.OSS_BUCKET_DOMAIN);
		ProductCareAreaListResponse productCareAreaListResponse = null;
		logger.info("Start fetching care area list");
		pageIndex = pageIndex == null ? 0 : pageIndex-1;
		pageSize = pageSize == null ? 10 : pageSize;
		
		Pair<Integer, List<ProductCareArea>> careAreas = productCareAreaRepository.findByStatus(StatusEnum.APPROVED.getCode(), pageIndex, pageSize);
		int totalCount = careAreas.getFirst();
		
		if (totalCount == 0) {
			logger.info("Start log into web api log");
			webServiceLogService.StoreInWebServiceLogDatabase(httpServletRequest, MethodNameEnum.GETCAREAREALISTAPI.getName(), HTTPServletReturnStatusEnum.AUTHORIZED.getCode());
			logger.info("End log into web api log");
			return new ProductCareAreaListResponse(totalCount, new ArrayList<ProductCareAreaResponse> ());
		}
		
		List<ProductCareAreaResponse> careAreaResponses = careAreas.getSecond().stream().map(ca -> {
			ProductCareAreaResponse car = new ProductCareAreaResponse(ca, langCode, ossBucketDomain);
			Long totalProducts = productRepository.countAllByProductCareArea(ca, Arrays.asList(StatusEnum.APPROVED));
			if (totalProducts != null) {
				car.setTotalProducts(totalProducts);
			}
			return car;
		}).collect(Collectors.toList());
		
		if (pageIndex != null && pageSize != null) {
			productCareAreaListResponse = new ProductCareAreaListResponse(totalCount, pageIndex, pageSize, careAreaResponses);
		} else {
			productCareAreaListResponse = new ProductCareAreaListResponse(totalCount, careAreaResponses);
		}
		loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, GET_CARE_AREA_LIST_METHOD_NAME, null);

		logger.info("Start log into web api log");
		webServiceLogService.StoreInWebServiceLogDatabase(httpServletRequest, MethodNameEnum.GETCAREAREALISTAPI.getName(), HTTPServletReturnStatusEnum.AUTHORIZED.getCode());
		logger.info("End log into web api log");
		
		return productCareAreaListResponse;
	}
	
	@Override
	public List<ProductCareAreaResponse> getFavouriteCareAreaList(Long userId, String langCode) throws IdsmedBaseException {
		List<ProductCareAreaResponse> favouriteCareAreaListResponse = new ArrayList<ProductCareAreaResponse> ();
		loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, GET_FAVOURITE_CARE_AREA_LIST_METHOD_NAME, null);
		String ossBucketDomain = env.getProperty(PropertiesLabelConst.OSS_BUCKET_DOMAIN);
		List<FavouriteProductCareArea> careAreas = favouriteProductCareAreaRepository.findByUserId(userId);
		
		if (!CollectionUtils.isEmpty(careAreas)) {
			favouriteCareAreaListResponse = careAreas.stream().filter(ca -> productCareAreaRepository.findById(ca.getCareAreaId()).isPresent()).map(ca -> {
				ProductCareArea pca = productCareAreaRepository.getOne(ca.getCareAreaId());
				
				ProductCareAreaResponse car = new ProductCareAreaResponse(pca, langCode, ossBucketDomain);
				Long totalProducts = productRepository.countAllByProductCareArea(pca, Arrays.asList(StatusEnum.APPROVED));
				if (totalProducts != null) {
					car.setTotalProducts(totalProducts);
				}
				return car;
			}).collect(Collectors.toList());
		}

		loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, GET_FAVOURITE_CARE_AREA_LIST_METHOD_NAME, null);
		return favouriteCareAreaListResponse;
	}

	@Override
	public List<ProductCareAreaResponse> getCareAreaList(String langCode) throws IdsmedBaseException {
		loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, GET_CARE_AREA_LIST_METHOD_NAME, null);
		String ossBucketDomain = env.getProperty(PropertiesLabelConst.OSS_BUCKET_DOMAIN);
		Integer status = StatusEnum.APPROVED.getCode();
		List<ProductCareArea> productCareAreas = productCareAreaRepository.findAllByStatusOrderByCareAreaNameAsc(status);
		List<ProductCareAreaResponse> productCareAreaList = new ArrayList<>();

		productCareAreaList = productCareAreas.stream().map(pca -> {
			ProductCareAreaResponse productCareAreaResponse = new ProductCareAreaResponse(pca, langCode, ossBucketDomain);
			return productCareAreaResponse;
		}).collect(Collectors.toList());
		loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, GET_CARE_AREA_LIST_METHOD_NAME, null);
		return productCareAreaList;
	}
	private void loggerForAllMethodInThisService(String methodDescription, String methodName, String loginId){
		logger.info(methodDescription, methodName, loginId != null?loginId:null);
	}
}

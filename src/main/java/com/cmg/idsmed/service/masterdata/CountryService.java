package com.cmg.idsmed.service.masterdata;

import com.cmg.idsmed.common.exception.IdsmedBaseException;
import com.cmg.idsmed.dto.masterdata.CountryResponse;

import java.util.List;

public interface CountryService {
	List<CountryResponse> getAllNotInactiveCountry() throws IdsmedBaseException;
}

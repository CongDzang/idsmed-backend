package com.cmg.idsmed.service.masterdata;

import com.cmg.idsmed.common.Constant.LoggerConstant;
import com.cmg.idsmed.common.enums.EntitySimpleStatusEnum;
import com.cmg.idsmed.common.exception.IdsmedBaseException;
import com.cmg.idsmed.dto.masterdata.CountryResponse;
import com.cmg.idsmed.model.entity.masterdata.Country;
import com.cmg.idsmed.model.repo.masterdata.CountryRepository;
import com.cmg.idsmed.service.email.EmailNotificationConfigServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Configuration
@Service
public class CountryServiceImpl implements CountryService {
	private static final Logger logger = LoggerFactory.getLogger(CountryServiceImpl.class);

	@Autowired
	private CountryRepository countryRepository;

	@Autowired
	private Environment env;

	@Value("${logger.start.method.description}")
	String START_METHOD_DESCRIPTION;

	@Value("${logger.end.method.description}")
	String END_METHOD_DESCRIPTION;
	private static final String GET_ALL_NOT_INACTIVE_COUNTRT_METHOD_NAME = "getAllNotInactiveCountry";

	public List<CountryResponse> getAllNotInactiveCountry() throws IdsmedBaseException {
		loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, GET_ALL_NOT_INACTIVE_COUNTRT_METHOD_NAME, null);
		List<Country> countries = countryRepository.findAllNotInactiveCountry(EntitySimpleStatusEnum.INACTIVE.getCode());
		loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, GET_ALL_NOT_INACTIVE_COUNTRT_METHOD_NAME, null);
		return countries.stream().map(c -> new CountryResponse(c)).collect(Collectors.toList());
	}
	private void loggerForAllMethodInThisService(String methodDescription, String methodName, String loginId){
		logger.info(methodDescription, methodName, loginId != null?loginId:null);
	}
}

package com.cmg.idsmed.service.masterdata;

import com.cmg.idsmed.common.exception.IdsmedBaseException;
import com.cmg.idsmed.dto.masterdata.EmailProviderRequest;
import com.cmg.idsmed.dto.masterdata.EmailProviderResponse;
import com.cmg.idsmed.dto.masterdata.VerifyingWorkFlowSettingResponse;

import java.util.List;

public interface MasterDataService {
	List<VerifyingWorkFlowSettingResponse> getVerifyingWorkFlowSettingByCountryCode(String countryCode, String langCode) throws IdsmedBaseException;
	String getCurrencySettingByCountryCode(String countryCode, String langCode) throws IdsmedBaseException;
	Boolean changeEmailProvider(EmailProviderRequest request) throws IdsmedBaseException;
	List<EmailProviderResponse> getEmailProvider(String langCode) throws IdsmedBaseException;
}

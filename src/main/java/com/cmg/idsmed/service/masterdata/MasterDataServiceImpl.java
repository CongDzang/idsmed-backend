package com.cmg.idsmed.service.masterdata;

import com.cmg.idsmed.common.Constant.LoggerConstant;
import com.cmg.idsmed.common.Constant.PropertiesLabelConst;
import com.cmg.idsmed.common.enums.EmailProviderEnum;
import com.cmg.idsmed.common.enums.EntitySimpleStatusEnum;
import com.cmg.idsmed.common.exception.ErrorInfo;
import com.cmg.idsmed.common.exception.IdsmedBaseException;
import com.cmg.idsmed.common.utils.LangHelper;
import com.cmg.idsmed.dto.masterdata.EmailProviderRequest;
import com.cmg.idsmed.dto.masterdata.EmailProviderResponse;
import com.cmg.idsmed.dto.masterdata.VerifyingWorkFlowSettingResponse;
import com.cmg.idsmed.model.entity.masterdata.CurrencySetting;
import com.cmg.idsmed.model.entity.masterdata.VerifyingWorkFlowSetting;
import com.cmg.idsmed.model.repo.masterdata.CurrencySettingRepository;
import com.cmg.idsmed.model.repo.masterdata.VerifyingWorkFlowSettingRepository;
import com.cmg.idsmed.service.share.FileService;
import com.cmg.idsmed.service.share.MessageResourceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Configuration
@Service
public class MasterDataServiceImpl implements MasterDataService {
	private static final Logger logger = LoggerFactory.getLogger(MasterDataServiceImpl.class);

	@Autowired
	private Environment env;

	@Autowired
	private VerifyingWorkFlowSettingRepository verifyingWorkFlowSettingRepository;

	@Autowired
	private CurrencySettingRepository currencySettingRepository;

	@Autowired
	private FileService fileService;

	@Autowired
	private MessageResourceService messageResourceService;

	@Value("${logger.start.method.description}")
	String START_METHOD_DESCRIPTION;

	@Value("${logger.end.method.description}")
	String END_METHOD_DESCRIPTION;
	private static final String GET_VERIFYING_WORKFLOW_SETTING_BY_COUNTRY_CODE = "getVerifyingWorkFlowSettingByCountryCode";
	private static final String GET_CURRENCY_SETTING_BY_COUNTRY_CODE_METHOD_NAME = "getCurrencySettingByCountryCode";
	private static final String CHANGE_EMAIL_PROVIDER_METHOD_NAME = "changeEmailProvider";
	private static final String GET_EMAIL_PROVIDER_METHOD_NAME = "getEmailProvider";

	@Override
	public List<VerifyingWorkFlowSettingResponse> getVerifyingWorkFlowSettingByCountryCode(String countryCode, String langCode) throws IdsmedBaseException {
		loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, GET_VERIFYING_WORKFLOW_SETTING_BY_COUNTRY_CODE, null);
		List<VerifyingWorkFlowSettingResponse> responses = new ArrayList<>();
		if (StringUtils.isEmpty(countryCode)) {
			//TODO throw exception here;
		}
		List<VerifyingWorkFlowSetting> settings = verifyingWorkFlowSettingRepository.findAllByCountryCodeAndStatus(countryCode, EntitySimpleStatusEnum.ACTIVE.getCode());
		if (!CollectionUtils.isEmpty(settings)) {
			responses = settings.stream().map(s -> new VerifyingWorkFlowSettingResponse(s)).collect(Collectors.toList());
		}
		loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, GET_VERIFYING_WORKFLOW_SETTING_BY_COUNTRY_CODE, null);
		return responses;
	}

	@Override
	public String getCurrencySettingByCountryCode(String countryCode, String langCode) throws IdsmedBaseException {
		loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, GET_CURRENCY_SETTING_BY_COUNTRY_CODE_METHOD_NAME, null);
		if (StringUtils.isEmpty(countryCode)) {
			//TODO throw exception here;
		}
		CurrencySetting setting = currencySettingRepository.findFirstByCountryCodeAndStatus(countryCode, EntitySimpleStatusEnum.ACTIVE.getCode());
		String currency = setting != null ? setting.getPrimaryCurrencyCode() : "";
		loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, GET_CURRENCY_SETTING_BY_COUNTRY_CODE_METHOD_NAME, null);
		return currency;
	}

	@Override
	public Boolean changeEmailProvider(EmailProviderRequest request) throws IdsmedBaseException {
		loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, CHANGE_EMAIL_PROVIDER_METHOD_NAME, request.getLoginId());
		if (!validEmailProvider(request.getEmailProviderCode())) {
			throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.CHANGE_EMAIL_PROVIDER_ERROR,
					LangHelper.getLangCode(request.getLangCode())));
		}
		String pathToFile = env.getProperty(PropertiesLabelConst.EMAIL_PROVIDER_CONFIG_FILE_LABEL);
		loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, CHANGE_EMAIL_PROVIDER_METHOD_NAME, request.getLoginId());
		return fileService.replaceSingleLineOfTextFile(pathToFile, request.getEmailProviderCode());
	}

	private Boolean validEmailProvider(String emailType) {
		if (StringUtils.isEmpty(emailType)) {
			return false;
		}
		if (!emailType.equalsIgnoreCase(EmailProviderEnum.SENDGRID.getCode()) && !emailType.equalsIgnoreCase(EmailProviderEnum.SMTP.getCode())) {
			return false;
		}
		return true;
	}

	public List<EmailProviderResponse> getEmailProvider(String langCode) throws IdsmedBaseException {
		loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, GET_EMAIL_PROVIDER_METHOD_NAME, null);
		String currentEmailProvider = null;
		String pathToFile = env.getProperty(PropertiesLabelConst.EMAIL_PROVIDER_CONFIG_FILE_LABEL);
		List<String> textsFromFile = fileService.readTextFile(pathToFile);
		if (!CollectionUtils.isEmpty(textsFromFile)) {
			logger.info("Current Email Provider get from text file is: {}", textsFromFile.get(0));
		}
		if (!CollectionUtils.isEmpty(textsFromFile)) {
			currentEmailProvider = textsFromFile.get(0);
		}
		List<EmailProviderResponse> responses = new ArrayList<>();
		for (EmailProviderEnum emailProviderEnum : EmailProviderEnum.values()) {
			String code = emailProviderEnum.getCode();
			String description = emailProviderEnum.getDescription();
			EmailProviderResponse response = new EmailProviderResponse(code, description);
			if(code.equalsIgnoreCase(currentEmailProvider)) {
				response.setCurrent(true);
			}
			responses.add(response);
		}
		loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, GET_EMAIL_PROVIDER_METHOD_NAME, null);
		return responses;
	}
	private void loggerForAllMethodInThisService(String methodDescription, String methodName, String loginId){
		logger.info(methodDescription, methodName, loginId != null?loginId:null);
	}
}

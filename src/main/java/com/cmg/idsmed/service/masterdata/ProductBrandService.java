package com.cmg.idsmed.service.masterdata;

import java.util.List;

import com.cmg.idsmed.common.exception.IdsmedBaseException;
import com.cmg.idsmed.dto.product.ProductBrandListResponse;
import com.cmg.idsmed.dto.product.ProductBrandResponse;
import com.cmg.idsmed.model.entity.product.ProductBrand;

public interface ProductBrandService {
	List<ProductBrandResponse> getFavouriteProductBrandList(Long userId) throws IdsmedBaseException;
	List<ProductBrandResponse> getFavouriteBrand(String userId) throws IdsmedBaseException;
	ProductBrandListResponse getProductBrandList(Integer pageIndex, Integer pageSize) throws IdsmedBaseException;
	
	ProductBrand createNewProductBrand(String brandName) throws IdsmedBaseException;

	// Third Party
	List<ProductBrandResponse> getThirdPartyFavouriteBrand(Long wedoctorCustomerId) throws IdsmedBaseException;
}

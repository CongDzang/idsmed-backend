package com.cmg.idsmed.service.masterdata;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import com.cmg.idsmed.common.Constant.LoggerConstant;
import com.cmg.idsmed.common.Constant.PropertiesLabelConst;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import com.cmg.idsmed.common.enums.HTTPServletReturnStatusEnum;
import com.cmg.idsmed.common.enums.LangEnum;
import com.cmg.idsmed.common.enums.MethodNameEnum;
import com.cmg.idsmed.common.enums.StatusEnum;
import com.cmg.idsmed.common.exception.IdsmedBaseException;
import com.cmg.idsmed.dto.product.ProductBrandListResponse;
import com.cmg.idsmed.dto.product.ProductBrandResponse;
import com.cmg.idsmed.model.entity.masterdata.FavouriteProductBrand;
import com.cmg.idsmed.model.entity.product.ProductBrand;
import com.cmg.idsmed.model.repo.masterdata.FavouriteProductBrandRepository;
import com.cmg.idsmed.model.repo.product.ProductBrandRepository;
import com.cmg.idsmed.service.product.ProductServiceImpl;
import com.cmg.idsmed.service.share.IdsmedServiceHelper;
import com.cmg.idsmed.service.share.WebServiceLogService;

@Configuration
@Service
public class ProductBrandServiceImpl implements ProductBrandService {
    private static final Logger logger = LoggerFactory.getLogger(ProductBrandServiceImpl.class);
    
	private static final Integer LENGTH_OF_DIGIT = 6;
	private static final Integer LENGTH_OF_ALPHABET = 3;

	@Autowired
	private Environment env;

    @Autowired
    private ProductBrandRepository productBrandRepository;
    
    @Autowired
    private FavouriteProductBrandRepository favouriteProductBrandRepository;
    
    @Autowired
    private HttpServletRequest httpServletRequest;
    
    @Autowired
    private WebServiceLogService webServiceLogService;

    @Autowired
	private IdsmedServiceHelper idsmedServiceHelper;

	@Value("${logger.start.method.description}")
	String START_METHOD_DESCRIPTION;

	@Value("${logger.end.method.description}")
	String END_METHOD_DESCRIPTION;
	private static final String GET_FAVOURITE_PRODUCT_BRAND_LIST_METHOD_NAME = "getFavouriteProductBrandList";
	private static final String GET_FAVOURITE_BRAND_METHOD_NAME = "getFavouriteBrand";
	private static final String GET_PRODUCT_BRAND_LIST_METHOD_NAME = "getProductBrandList";
	private static final String GET_THIRD_PARTY_FAVOURITE_BRAND = "getThirdPartyFavouriteBrand";

	@Override
	public List<ProductBrandResponse> getFavouriteProductBrandList(Long userId) throws IdsmedBaseException {
		loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, GET_FAVOURITE_PRODUCT_BRAND_LIST_METHOD_NAME, null);
		List<ProductBrandResponse> favouriteProductBrandsResponse = new ArrayList<ProductBrandResponse> ();
		
		List<FavouriteProductBrand> favouriteBrands = favouriteProductBrandRepository.findByUserId(userId);

		String ossBucketDomain = env.getProperty(PropertiesLabelConst.OSS_BUCKET_DOMAIN);
		
		if (!CollectionUtils.isEmpty(favouriteBrands)) {
			favouriteProductBrandsResponse = favouriteBrands.stream().filter(pb -> productBrandRepository.findById(pb.getBrandId()).isPresent())
					.map(pb -> new ProductBrandResponse(productBrandRepository.getOne(pb.getBrandId()), ossBucketDomain)).collect(Collectors.toList());
		}

		loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, GET_FAVOURITE_PRODUCT_BRAND_LIST_METHOD_NAME, null);
		return favouriteProductBrandsResponse;
	}

	@Override
	@PreAuthorize("@PermissionService.isMethodPermited(authentication,  {'API0000010'})")
	public List<ProductBrandResponse> getFavouriteBrand(String userId) throws IdsmedBaseException {
		// TODO Auto-generated method stub
		List<ProductBrandResponse> favouriteProductBrandsResponse = new ArrayList<ProductBrandResponse> ();
		loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, GET_FAVOURITE_BRAND_METHOD_NAME, null);

		List<FavouriteProductBrand> favouriteBrands = favouriteProductBrandRepository.findByUserThirdParty(userId);

		String ossBucketDomain = env.getProperty(PropertiesLabelConst.OSS_BUCKET_DOMAIN);
		
		if (!CollectionUtils.isEmpty(favouriteBrands)) {
			favouriteProductBrandsResponse = favouriteBrands.stream().filter(pb -> productBrandRepository.findById(pb.getBrandId()).isPresent())
					.map(pb -> new ProductBrandResponse(productBrandRepository.getOne(pb.getBrandId()), ossBucketDomain)).collect(Collectors.toList());
		}

		loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, GET_FAVOURITE_BRAND_METHOD_NAME, null);
		
		logger.info("Start log into web api log");
		webServiceLogService.StoreInWebServiceLogDatabase(httpServletRequest, MethodNameEnum.GETFAVOURITEBRAND.getName(), HTTPServletReturnStatusEnum.AUTHORIZED.getCode());
		logger.info("End log into web api log");
		return favouriteProductBrandsResponse;
	}
	
	@Override
	@PreAuthorize("@PermissionService.isMethodPermited(authentication, {'API0000006'})")
	public ProductBrandListResponse getProductBrandList(Integer pageIndex, Integer pageSize) throws IdsmedBaseException {
		loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, GET_PRODUCT_BRAND_LIST_METHOD_NAME, null);
		Page<ProductBrand> productBrands = null;
		Pageable pageable = PageRequest.of(pageIndex, pageSize, Sort.by("brandName").ascending());
		
		productBrands = productBrandRepository.findAllByStatus(StatusEnum.APPROVED.getCode(), pageable);
		
		if (productBrands == null || CollectionUtils.isEmpty(productBrands.getContent())) {
			logger.info("Start log into web api log");
			webServiceLogService.StoreInWebServiceLogDatabase(httpServletRequest, MethodNameEnum.GETBRANDLISTAPI.getName(), HTTPServletReturnStatusEnum.AUTHORIZED.getCode());
			logger.info("End log into web api log");
			return new ProductBrandListResponse(0, new ArrayList<ProductBrandResponse> ());
		}
		Integer totalCount = Integer.valueOf(Long.valueOf(productBrands.getTotalElements()).intValue());

		String ossBucketDomain = env.getProperty(PropertiesLabelConst.OSS_BUCKET_DOMAIN);
		
		List<ProductBrandResponse> productBrandResponses = productBrands.getContent()
				.stream()
				.map(p -> new ProductBrandResponse(p, ossBucketDomain)).collect(Collectors.toList());

		ProductBrandListResponse productBrandListResponse = null;
		if (pageIndex != null && pageSize != null) {
			productBrandListResponse = new ProductBrandListResponse(totalCount, pageIndex, pageSize, productBrandResponses);
		} else {
			productBrandListResponse = new ProductBrandListResponse(totalCount, productBrandResponses);
		}
		loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, GET_PRODUCT_BRAND_LIST_METHOD_NAME, null);

		logger.info("Start log into web api log");
		webServiceLogService.StoreInWebServiceLogDatabase(httpServletRequest, MethodNameEnum.GETBRANDLISTAPI.getName(), HTTPServletReturnStatusEnum.AUTHORIZED.getCode());
		logger.info("End log into web api log");
		
		return productBrandListResponse;
	}
	
	public ProductBrand createNewProductBrand(String brandName) {
		ProductBrand newProductBrand = new ProductBrand();
		newProductBrand.setBrandName(brandName);
		String brandCode = idsmedServiceHelper.generateBrandCode();
		newProductBrand.setBrandCode(brandCode);
		newProductBrand.setCountryCode(LangEnum.CHINA.getCode());
		return newProductBrand;
	}

	@Override
	@PreAuthorize("@PermissionService.isMethodPermited(authentication,  {'API0000010'})")
	public List<ProductBrandResponse> getThirdPartyFavouriteBrand(Long wedoctorCustomerId) throws IdsmedBaseException {
		// TODO Auto-generated method stub
		loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, GET_THIRD_PARTY_FAVOURITE_BRAND, null);
		List<ProductBrandResponse> favouriteProductBrandsResponse = new ArrayList<ProductBrandResponse> ();

		List<FavouriteProductBrand> favouriteBrands = favouriteProductBrandRepository.findByWedoctorCustomerId(wedoctorCustomerId);

		String ossBucketDomain = env.getProperty(PropertiesLabelConst.OSS_BUCKET_DOMAIN);

		if (!CollectionUtils.isEmpty(favouriteBrands)) {
			favouriteProductBrandsResponse = favouriteBrands.stream().filter(pb -> productBrandRepository.findById(pb.getBrandId()).isPresent())
					.map(pb -> new ProductBrandResponse(productBrandRepository.getOne(pb.getBrandId()), ossBucketDomain)).collect(Collectors.toList());
		}

		loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, GET_THIRD_PARTY_FAVOURITE_BRAND, null);

		logger.info("Start log into web api log");
		webServiceLogService.StoreInWebServiceLogDatabase(httpServletRequest, MethodNameEnum.GETFAVOURITEBRAND.getName(), HTTPServletReturnStatusEnum.AUTHORIZED.getCode());
		logger.info("End log into web api log");
		return favouriteProductBrandsResponse;
	}
	private void loggerForAllMethodInThisService(String methodDescription, String methodName, String loginId){
		logger.info(methodDescription, methodName, loginId != null?loginId:null);
	}
}

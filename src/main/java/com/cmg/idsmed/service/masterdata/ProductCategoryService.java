package com.cmg.idsmed.service.masterdata;

import com.cmg.idsmed.common.exception.IdsmedBaseException;
import com.cmg.idsmed.dto.product.ProductCategoryListResponse;

public interface ProductCategoryService {
	ProductCategoryListResponse getProductCategoryList(Integer pageIndex, Integer pageSize) throws IdsmedBaseException;
}

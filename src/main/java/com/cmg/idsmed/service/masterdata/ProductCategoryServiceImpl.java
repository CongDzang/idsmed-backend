package com.cmg.idsmed.service.masterdata;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cmg.idsmed.common.Constant.LoggerConstant;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import com.cmg.idsmed.common.Constant.ProductParameterConstant;
import com.cmg.idsmed.common.enums.HTTPServletReturnStatusEnum;
import com.cmg.idsmed.common.enums.MethodNameEnum;
import com.cmg.idsmed.common.enums.StatusEnum;
import com.cmg.idsmed.common.exception.IdsmedBaseException;
import com.cmg.idsmed.dto.product.ProductCategoryListResponse;
import com.cmg.idsmed.dto.product.ProductCategoryResponse;
import com.cmg.idsmed.model.entity.product.ProductCategory;
import com.cmg.idsmed.model.repo.product.ProductCategoryRepository;
import com.cmg.idsmed.model.repo.product.ProductRepository;
import com.cmg.idsmed.service.product.ProductServiceImpl;
import com.cmg.idsmed.service.share.WebServiceLogService;

@Configuration
@Service
public class ProductCategoryServiceImpl implements ProductCategoryService {
    private static final Logger logger = LoggerFactory.getLogger(ProductCategoryServiceImpl.class);
    
    @Autowired
    private ProductRepository productRepository;
    
    @Autowired
    private ProductCategoryRepository productCategoryRepository;
    
    @Autowired
    private HttpServletRequest httpServletRequest;
    
    @Autowired
    private WebServiceLogService webServiceLogService;

    @Autowired
	private Environment env;

	@Value("${logger.start.method.description}")
	String START_METHOD_DESCRIPTION;

	@Value("${logger.end.method.description}")
	String END_METHOD_DESCRIPTION;
	private static final String GET_PRODUCT_CATEGORY_LIST_METHOD_NAME = "getProductCategoryList";
    
	@Override
	@PreAuthorize("@PermissionService.isMethodPermited(authentication, 'API0000007')")
	public ProductCategoryListResponse getProductCategoryList(Integer pageIndex, Integer pageSize) throws IdsmedBaseException {
		loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, GET_PRODUCT_CATEGORY_LIST_METHOD_NAME, null);
		Page<ProductCategory> productCategories = null;
		Pageable pageable = PageRequest.of(pageIndex, pageSize, Sort.by("categoryName").ascending());
		
		productCategories = productCategoryRepository.findAllByStatus(StatusEnum.APPROVED.getCode(), pageable);
		
		if (productCategories == null || CollectionUtils.isEmpty(productCategories.getContent())) {
			logger.info("Start log into web api log");
			webServiceLogService.StoreInWebServiceLogDatabase(httpServletRequest, MethodNameEnum.GETCATEGORYLISTAPI.getName(),HTTPServletReturnStatusEnum.AUTHORIZED.getCode());
			logger.info("End log into web api log");
			return new ProductCategoryListResponse(0, new ArrayList<ProductCategoryResponse> ());
		}
		Integer totalCount = Integer.valueOf(Long.valueOf(productCategories.getTotalElements()).intValue());
		
		List<ProductCategoryResponse> productCategoryList = productCategories.getContent().stream().map(p -> {
			ProductCategoryResponse pcr = new ProductCategoryResponse(p);
			Long totalProducts = productRepository.countAllByProductCategory(p, Arrays.asList(StatusEnum.APPROVED));
			if (totalProducts != null) {
				pcr.setTotalProducts(totalProducts);
			}
			return pcr;
		}).collect(Collectors.toList());

		ProductCategoryListResponse productCategoryListResponse = null;
		if (pageIndex != null && pageSize != null) {
			productCategoryListResponse = new ProductCategoryListResponse(totalCount, pageIndex, pageSize, productCategoryList);
		} else {
			productCategoryListResponse = new ProductCategoryListResponse(totalCount, productCategoryList);
		}

		loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, GET_PRODUCT_CATEGORY_LIST_METHOD_NAME, null);

		logger.info("Start log into web api log");
		webServiceLogService.StoreInWebServiceLogDatabase(httpServletRequest, MethodNameEnum.GETCATEGORYLISTAPI.getName(),HTTPServletReturnStatusEnum.AUTHORIZED.getCode());
		logger.info("End log into web api log");
		return productCategoryListResponse;
	}
	private void loggerForAllMethodInThisService(String methodDescription, String methodName, String loginId){
		logger.info(methodDescription, methodName, loginId != null?loginId:null);
	}
}

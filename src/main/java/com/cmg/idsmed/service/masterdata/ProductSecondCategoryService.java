package com.cmg.idsmed.service.masterdata;

import com.cmg.idsmed.common.exception.IdsmedBaseException;
import com.cmg.idsmed.dto.product.ProductCategoryListResponse;
import com.cmg.idsmed.dto.product.ProductSecondCategoryListResponse;

public interface ProductSecondCategoryService {
	ProductSecondCategoryListResponse getProductSecondCategoryList(Integer pageIndex, Integer pageSize) throws IdsmedBaseException;
}

package com.cmg.idsmed.service.masterdata;

import com.cmg.idsmed.common.Constant.LoggerConstant;
import com.cmg.idsmed.common.enums.HTTPServletReturnStatusEnum;
import com.cmg.idsmed.common.enums.MethodNameEnum;
import com.cmg.idsmed.common.enums.StatusEnum;
import com.cmg.idsmed.common.exception.IdsmedBaseException;
import com.cmg.idsmed.dto.product.ProductCategoryListResponse;
import com.cmg.idsmed.dto.product.ProductCategoryResponse;
import com.cmg.idsmed.dto.product.ProductSecondCategoryListResponse;
import com.cmg.idsmed.dto.product.ProductSecondCategoryResponse;
import com.cmg.idsmed.model.entity.product.ProductCategory;
import com.cmg.idsmed.model.entity.product.ProductSecondCategory;
import com.cmg.idsmed.model.repo.product.ProductRepository;
import com.cmg.idsmed.model.repo.product.ProductSecondCategoryRepository;
import com.cmg.idsmed.service.share.WebServiceLogService;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Configuration
@Service
public class ProductSecondCategoryServiceImpl implements ProductSecondCategoryService {
    private static final Logger logger = LoggerFactory.getLogger(ProductSecondCategoryServiceImpl.class);

	@Autowired
    private ProductRepository productRepository;
    
    @Autowired
    private ProductSecondCategoryRepository productSecondCategoryRepository;

    @Autowired
	private Environment env;

	@Value("${logger.start.method.description}")
	String START_METHOD_DESCRIPTION;

	@Value("${logger.end.method.description}")
	String END_METHOD_DESCRIPTION;
	private static final String GET_PRODUCT_SECOND_CATEGORY_LIST_METHOD_NAME = "getProductSecondCategoryList";

	@Override
	public ProductSecondCategoryListResponse getProductSecondCategoryList(Integer pageIndex, Integer pageSize) throws IdsmedBaseException {
		loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, GET_PRODUCT_SECOND_CATEGORY_LIST_METHOD_NAME, null);

		Page<ProductSecondCategory> productSecondCategories = null;
		Pageable pageable = PageRequest.of(pageIndex, pageSize, Sort.by("categoryName").ascending());

		productSecondCategories = productSecondCategoryRepository.findAllByStatus(StatusEnum.APPROVED.getCode(), pageable);

		if (productSecondCategories == null || CollectionUtils.isEmpty(productSecondCategories.getContent())) {
			return new ProductSecondCategoryListResponse(0, new ArrayList<ProductSecondCategoryResponse> ());
		}

		Integer totalCount = Integer.valueOf(Long.valueOf(productSecondCategories.getTotalElements()).intValue());

		List<ProductSecondCategoryResponse> productSecondCategoryList = productSecondCategories.stream().map(p -> {
			ProductSecondCategoryResponse pscr = new ProductSecondCategoryResponse(p);
			Long totalProducts = productRepository.countAllByProductSecondCategory(p, Arrays.asList(StatusEnum.APPROVED));
			if (totalProducts != null) {
				pscr.setTotalProducts(totalProducts);
			}
			return pscr;
		}).collect(Collectors.toList());

		ProductSecondCategoryListResponse productSecondCategoryListResponse = null;
		if (pageIndex != null && pageSize != null) {
			productSecondCategoryListResponse = new ProductSecondCategoryListResponse(totalCount, pageIndex, pageSize, productSecondCategoryList);
		} else {
			productSecondCategoryListResponse = new ProductSecondCategoryListResponse(totalCount, productSecondCategoryList);
		}

		loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, GET_PRODUCT_SECOND_CATEGORY_LIST_METHOD_NAME, null);

		return productSecondCategoryListResponse;
	}
	private void loggerForAllMethodInThisService(String methodDescription, String methodName, String loginId){
		logger.info(methodDescription, methodName, loginId != null?loginId:null);
	}
}

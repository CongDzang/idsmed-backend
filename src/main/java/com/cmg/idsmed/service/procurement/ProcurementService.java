package com.cmg.idsmed.service.procurement;

import com.cmg.idsmed.common.exception.IdsmedBaseException;
import com.cmg.idsmed.dto.procurement.SmallOrderListResponse;
import com.cmg.idsmed.dto.procurement.SmallOrderResponse;

public interface ProcurementService {
    SmallOrderListResponse getSmallOrderList(Integer pageIndex, Integer pageSize, String loginId, String langCode) throws IdsmedBaseException;
    SmallOrderResponse getSmallOrderById(Long id, String langCode) throws IdsmedBaseException;

}

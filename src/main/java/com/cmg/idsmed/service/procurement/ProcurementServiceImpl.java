package com.cmg.idsmed.service.procurement;

import com.cmg.idsmed.common.Constant.LoggerConstant;
import com.cmg.idsmed.common.exception.ErrorInfo;
import com.cmg.idsmed.common.exception.IdsmedBaseException;
import com.cmg.idsmed.common.utils.LangHelper;
import com.cmg.idsmed.dto.procurement.SmallOrderListResponse;
import com.cmg.idsmed.dto.procurement.SmallOrderResponse;
import com.cmg.idsmed.model.entity.auth.IdsmedAccount;
import com.cmg.idsmed.model.entity.auth.IdsmedUser;
import com.cmg.idsmed.model.entity.procurement.OdSmallOrder;
import com.cmg.idsmed.model.entity.thirdparty.Customer;
import com.cmg.idsmed.model.entity.vendor.Vendor;
import com.cmg.idsmed.model.repo.auth.IdsmedAccountRepository;
import com.cmg.idsmed.model.repo.procurement.OdSmallOrderRepository;
import com.cmg.idsmed.model.repo.thirdparty.CustomerRepository;
import com.cmg.idsmed.model.repo.user.IdsmedUserRepository;
import com.cmg.idsmed.model.repo.vendor.VendorRepository;
import com.cmg.idsmed.service.share.MessageResourceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Configuration
@Service
public class ProcurementServiceImpl implements ProcurementService {

    private static final Logger logger = LoggerFactory.getLogger(ProcurementServiceImpl.class);

    @Autowired
    IdsmedAccountRepository idsmedAccountRepository;

    @Autowired
    IdsmedUserRepository idsmedUserRepository;

    @Autowired
    VendorRepository vendorRepository;

    @Autowired
    OdSmallOrderRepository odSmallOrderRepository;

    @Autowired
    MessageResourceService messageResourceService;

    @Autowired
    CustomerRepository customerRepository;

    @Autowired
    private Environment env;

    @Value("${logger.start.method.description}")
    String START_METHOD_DESCRIPTION;

    @Value("${logger.end.method.description}")
    String END_METHOD_DESCRIPTION;
    private static final String GET_SMALL_ORDER_LIST_METHOD_NAME = "getSmallOrderList";
    private static final String GET_SMALL_ORDER_BY_ID_METHOD_NAME = "getSmallOrderById";

    @Override
    public SmallOrderListResponse getSmallOrderList(Integer pageIndex, Integer pageSize, String loginId, String langCode) throws IdsmedBaseException {
        loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, GET_SMALL_ORDER_LIST_METHOD_NAME, loginId);

        Pair<Integer, List<OdSmallOrder>> pair = null;

        IdsmedAccount account = idsmedAccountRepository.findByLoginId(loginId);
        if (account != null) {
            IdsmedUser user = idsmedUserRepository.findIdsmedUserByIdsmedAccounts(account);
            if (user != null) {
                String companyCode = user.getCompanyCode();

                /**
                 * If user with company code, means is vendor else SCP user
                 */
                if (!StringUtils.isEmpty(companyCode)) {
                    Vendor vendor = vendorRepository.findFirstByCompanyCode(user.getCompanyCode());
                    Long vendorId = vendor.getId();

                    pair = odSmallOrderRepository.findSmallOrderByMerchantId(pageIndex, pageSize, vendorId);
                } else {
                    pair = odSmallOrderRepository.findSmallOrder(pageIndex, pageSize);
                }
            }
        }

        SmallOrderListResponse smallOrderListResponse = new SmallOrderListResponse(pageIndex, pageSize);

        if (CollectionUtils.isEmpty(pair.getSecond())) {
            return smallOrderListResponse;
        }

        List<SmallOrderResponse> smallOrderResponses = generateSmallOrderResponseList(pair.getSecond());

        if (pageIndex != null && pageSize != null) {
            smallOrderListResponse = new SmallOrderListResponse(pair.getFirst(), pageIndex, pageSize, smallOrderResponses);
        } else {
            smallOrderListResponse = new SmallOrderListResponse(pair.getFirst(), smallOrderResponses);
        }

        loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, GET_SMALL_ORDER_LIST_METHOD_NAME, loginId);
        return smallOrderListResponse;
    }

    private List<SmallOrderResponse> generateSmallOrderResponseList(List<OdSmallOrder> smallOrders) {

        List<SmallOrderResponse> smallOrderResponses = smallOrders
                .stream()
                .map(so -> {
                    Long customerId = so.getUserId().longValue();
                    Customer customer = customerRepository.findByCustomerId(customerId);
                    SmallOrderResponse response = new SmallOrderResponse(so);
                    if (customer != null) {
                        response.setOrderedBy(customer.getCompanyName());
                    }

                    return response;
                }).collect(Collectors.toList());

        return smallOrderResponses;
    }

    @Override
    public SmallOrderResponse getSmallOrderById(Long id, String langCode) throws IdsmedBaseException {
        loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, GET_SMALL_ORDER_BY_ID_METHOD_NAME, null);

        Optional<OdSmallOrder> odSmallOrderOptional = odSmallOrderRepository.findById(id);

        if (!odSmallOrderOptional.isPresent()) {
            throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.SMALL_ORDER_NOT_FOUND,
                    LangHelper.getLangCode(langCode)));
        }

        OdSmallOrder odSmallOrder = odSmallOrderOptional.get();

        SmallOrderResponse response = new SmallOrderResponse(odSmallOrder);

        loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, GET_SMALL_ORDER_BY_ID_METHOD_NAME, null);
        return response;
    }
    private void loggerForAllMethodInThisService(String methodDescription, String methodName, String loginId){
        logger.info(methodDescription, methodName, loginId != null?loginId:null);
    }
}

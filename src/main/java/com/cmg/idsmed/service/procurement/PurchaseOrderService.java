package com.cmg.idsmed.service.procurement;

import com.cmg.idsmed.common.exception.IdsmedBaseException;
import com.cmg.idsmed.dto.procurement.*;

public interface PurchaseOrderService {
	PurchaseOrderResponse createPurchaseOrder(PurchaseOrderRequest request) throws IdsmedBaseException;
	OrderStatusResponse updateOrderStatus(OrderStatusRequest request) throws IdsmedBaseException;
	CancelOrderResponse cancelOrderByProductId(CancelOrderRequest request) throws IdsmedBaseException;
	SyncLogisticsResponse syncOrderLogisticsInfo(SyncLogisticsRequest request) throws IdsmedBaseException;
}

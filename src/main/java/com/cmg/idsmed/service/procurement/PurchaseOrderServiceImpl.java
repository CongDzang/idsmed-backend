package com.cmg.idsmed.service.procurement;

import com.cmg.idsmed.common.Constant.IdsmedMessageKeyConst;
import com.cmg.idsmed.common.Constant.LoggerConstant;
import com.cmg.idsmed.common.enums.LangEnum;
import com.cmg.idsmed.common.enums.ProcurementOrderStatusEnum;
import com.cmg.idsmed.common.exception.ErrorInfo;
import com.cmg.idsmed.common.exception.IdsmedBaseException;
import com.cmg.idsmed.common.utils.LangHelper;
import com.cmg.idsmed.dto.procurement.*;
import com.cmg.idsmed.model.entity.procurement.*;
import com.cmg.idsmed.model.repo.procurement.*;
import com.cmg.idsmed.service.share.MessageResourceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Configuration
@Service
public class PurchaseOrderServiceImpl implements PurchaseOrderService {

    private static final Logger logger = LoggerFactory.getLogger(PurchaseOrderServiceImpl.class);

    @Autowired
    private OdBigOrderRepository odBigOrderRepository;

    @Autowired
    private OdSmallOrderRepository odSmallOrderRepository;

    @Autowired
    private OdOrderItemRepository odOrderItemRepository;

    @Autowired
    private OdOrderDeliveryRepository odOrderDeliveryRepository;

    @Autowired
    private OdOrderReceiveAddressRepository odOrderReceiveAddressRepository;

    @Autowired
    private OdDeliveryProductRepository odDeliveryProductRepository;

    @Autowired
    private OdOrderDeliveryRecordRepository odOrderDeliveryRecordRepository;

    @Autowired
    private MessageResourceService messageResourceService;

    @Autowired
    private Environment env;

    @Value("${logger.start.method.description}")
    String START_METHOD_DESCRIPTION;

    @Value("${logger.end.method.description}")
    String END_METHOD_DESCRIPTION;
    private static final String CREATE_PURCHASE_ORDER_METHOD_NAME = "createPurchaseOrder";
    private static final String UPDATE_ORDER_STATUS_METHOD_NAME = "updateOrderStatus";
    private static final String SYNC_ORDER_LOGISTICS_INFO_METHOD_NAME = "syncOrderLogisticsInfo";
    private static final String CANCEL_ORDER_BY_PRODUCT_ID_METHOD_NAME = "cancelOrderByProductId";

    @Override
    @Transactional
    public PurchaseOrderResponse createPurchaseOrder(PurchaseOrderRequest request) throws IdsmedBaseException {
        loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, CREATE_PURCHASE_ORDER_METHOD_NAME, null);

        OdBigOrder bigOrder = new OdBigOrder(request);
        OdOrderDelivery odOrderDelivery = null;
        OdOrderReceiveAddress odOrderReceiveAddress = null;
        bigOrder = odBigOrderRepository.save(bigOrder);

        List<POList> poLists = new ArrayList<>();

        List<OdSmallOrder> smallOrders = new ArrayList<>();
        List<SmallOrderRequest> smallOrderRequests = request.getSmallOrders();

        if (!CollectionUtils.isEmpty(smallOrderRequests)) {
            for (SmallOrderRequest rq : smallOrderRequests) {
                OdSmallOrder smallOrder = new OdSmallOrder(rq, bigOrder);
                String epoUUID = UUID.randomUUID().toString();
                String epo = "";
                if (epoUUID.length() > 30) {
                    epo = epoUUID.substring(0, 30);
                } else {
                    epo = epoUUID;
                }
                smallOrder.setEpo(epo);
                smallOrder.setOdBigOrder(bigOrder);
                smallOrder = odSmallOrderRepository.save(smallOrder);
                smallOrders.add(smallOrder);

                POList poList = new POList(smallOrder.getSmallOrderNo(), epo);
                poLists.add(poList);

                List<OdOrderItem> odOrderItems = new ArrayList<>();
                List<ProductOrderRequest> productOrderRequests = rq.getProductList();
                if (!CollectionUtils.isEmpty(productOrderRequests)) {
                    for (ProductOrderRequest por : productOrderRequests) {
                        OdOrderItem odOrderItem = new OdOrderItem(por);
                        odOrderItem.setOdSmallOrder(smallOrder);
                        odOrderItem.setOrderStatus(rq.getOrderStatus());
                        odOrderItems.add(odOrderItem);
                    }

                    if (!CollectionUtils.isEmpty(odOrderItems)) {
                        odOrderItems = odOrderItemRepository.saveAll(odOrderItems);
                    }
                }

                OrderReceiveInfo orderReceiveInfo = rq.getOrderReceiveInfo();
                if (orderReceiveInfo != null) {
                    odOrderDelivery = new OdOrderDelivery(orderReceiveInfo);
                    odOrderDelivery.setOdSmallOrder(smallOrder);
                    odOrderDelivery.setSmallOrderNo(smallOrder.getSmallOrderNo());
                    odOrderDelivery.setMemberId(123L);
                    odOrderDelivery.setCreateTime(smallOrder.getCreateTime());
                    odOrderDelivery.setFreight(smallOrder.getTotalFreight());
                    odOrderDelivery = odOrderDeliveryRepository.save(odOrderDelivery);

                    OdOrderDeliveryRecord odOrderDeliveryRecord = new OdOrderDeliveryRecord(odOrderDelivery);
                    odOrderDeliveryRecordRepository.save(odOrderDeliveryRecord);

                    odOrderReceiveAddress = new OdOrderReceiveAddress(orderReceiveInfo);
                    odOrderReceiveAddress.setSmallOrderNo(smallOrder.getSmallOrderNo());
                    odOrderReceiveAddress.setMobile(orderReceiveInfo.getMobile());
                    odOrderReceiveAddress.setCreateTime(new Date());
                    odOrderReceiveAddress.setOdSmallOrder(smallOrder);
                    odOrderReceiveAddressRepository.save(odOrderReceiveAddress);
                }

                if (!CollectionUtils.isEmpty(odOrderItems)) {
                    for (OdOrderItem odi : odOrderItems) {
                        OdDeliveryProduct odDeliveryProduct = new OdDeliveryProduct(odi);
                        odDeliveryProduct.setOdOrderDelivery(odOrderDelivery);
                        odDeliveryProductRepository.save(odDeliveryProduct);
                    }
                }
            }
        }

        loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, CREATE_PURCHASE_ORDER_METHOD_NAME, null);
        return new PurchaseOrderResponse(0, poLists,
                messageResourceService.getMessage(IdsmedMessageKeyConst.SUCCESS_MESSAGE_TO_WEDOCTOR,
                        LangHelper.getLangCode(LangEnum.CHINA.getCode())));
    }

    @Override
    @Transactional
    public OrderStatusResponse updateOrderStatus(OrderStatusRequest request) throws IdsmedBaseException {
        loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, UPDATE_ORDER_STATUS_METHOD_NAME, null);
        String bigOrderNo = request.getBigOrderNo();
        Integer orderStatus = request.getOrderStatus();

        OdBigOrder odBigOrder = odBigOrderRepository.findByBigOrderNo(bigOrderNo);
        if (odBigOrder == null) {
            throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.BIG_ORDER_NOT_FOUND,
                    LangHelper.getLangCode(request.getLangCode())));
        }

        List<OdSmallOrder> odSmallOrders = odSmallOrderRepository.findByBigOrderNo(odBigOrder.getBigOrderNo());

        if (CollectionUtils.isEmpty(odSmallOrders)) {
            throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.SMALL_ORDER_NOT_FOUND,
                    LangHelper.getLangCode(request.getLangCode())));
        }

        for (OdSmallOrder odSmallOrder : odSmallOrders) {
            processUpdateOrderItemOrderStatusBySmallOrderNo(odSmallOrder, orderStatus);
            odSmallOrder.setOrderStatus(orderStatus);
            odSmallOrderRepository.save(odSmallOrder);
        }

        odBigOrder.setOrderStatus(orderStatus);
        odBigOrderRepository.save(odBigOrder);

        loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, UPDATE_ORDER_STATUS_METHOD_NAME, null);
        return new OrderStatusResponse(0,
                messageResourceService.getMessage(IdsmedMessageKeyConst.SUCCESS_MESSAGE_TO_WEDOCTOR,
                        LangHelper.getLangCode(LangEnum.CHINA.getCode())));
    }

    @Transactional
    private void processUpdateOrderItemOrderStatusBySmallOrderNo(OdSmallOrder odSmallOrder, Integer orderStatus) {
        List<OdOrderItem> odOrderItems = odOrderItemRepository.findBySmallOrderNo(odSmallOrder.getSmallOrderNo());

        if (!CollectionUtils.isEmpty(odOrderItems)) {
            for (OdOrderItem odOrderItem : odOrderItems) {
                odOrderItem.setOrderStatus(orderStatus);
                odOrderItemRepository.save(odOrderItem);
            }
        }

    }

    @Override
    @Transactional
    public SyncLogisticsResponse syncOrderLogisticsInfo(SyncLogisticsRequest request) throws IdsmedBaseException {
        loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, SYNC_ORDER_LOGISTICS_INFO_METHOD_NAME, null);
        String smallOrderNo = request.getSmallOrderNo();

        // From third-party document, they only send one small order no.
        OdOrderDelivery orderDelivery = odOrderDeliveryRepository.findFirstBySmallOrderNo(smallOrderNo);
        if (orderDelivery == null) {
            throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.ORDER_DELIVERY_NOT_FOUND,
                    LangHelper.getLangCode(request.getLangCode())));
        }

        /**
         * Due to incorrect design, so we expect Delivery Record only comes with 1 record
         * Refer to LucidChart
         */
        List<DeliveryRecordRequest> deliveryRecords = request.getDeliveryRecords();
        if (!CollectionUtils.isEmpty(deliveryRecords)) {
            processSyncOrderLogistics(request, deliveryRecords, orderDelivery);
        }

        processUpdateTotalFreightInSmallOrder(smallOrderNo);
        loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, SYNC_ORDER_LOGISTICS_INFO_METHOD_NAME, null);
        return new SyncLogisticsResponse(0,
                messageResourceService.getMessage(IdsmedMessageKeyConst.SUCCESS_MESSAGE_TO_WEDOCTOR,
                        LangHelper.getLangCode(LangEnum.CHINA.getCode())));
    }

    /**
     * Due to stupid design, delivery status from small order listing at SCP side
     * will save in mail status field in order delivery when sync logistic info in
     * small order details page at SCP side
     * @param request
     * @param deliveryRecords
     * @param orderDelivery
     */
    @Transactional
    private void processSyncOrderLogistics(SyncLogisticsRequest request,
                                           List<DeliveryRecordRequest> deliveryRecords,
                                           OdOrderDelivery orderDelivery) {
        Long orderDeliveryId = orderDelivery.getId();

        deliveryRecords.forEach(dr -> {
            OdOrderDeliveryRecord odOrderDeliveryRecord = odOrderDeliveryRecordRepository.findFirstByOdOrderDeliveryId(orderDeliveryId);
            odOrderDeliveryRecord.setDeliveryNo(request.getDeliveryNo());
            odOrderDeliveryRecord.setDeliveryTime(dr.getDeliveryTime());
            odOrderDeliveryRecord.setDeliveryInfo(dr.getDeliveryInfo());
            odOrderDeliveryRecordRepository.save(odOrderDeliveryRecord);

            orderDelivery.setFreight(request.getFreight());
            orderDelivery.setShipType(request.getShipType());
            orderDelivery.setLogisCompId(request.getLogisCompId());
            orderDelivery.setLogisCompCode(request.getLogisCompCode());
            orderDelivery.setLogisCompName(request.getLogisCompName());
            orderDelivery.setDeliveryNo(request.getDeliveryNo());
            orderDelivery.setMobile(request.getMobile());
            orderDelivery.setRemark(request.getRemark());
            orderDelivery.setMailStatus(request.getMailStatus());
            orderDelivery.setSupplierDeliveryNo(dr.geteTrackingNo());
            odOrderDeliveryRepository.save(orderDelivery);
        });

    }

    @Transactional
    private void processUpdateTotalFreightInSmallOrder(String smallOrderNo) {
        BigDecimal totalFreightCount = odOrderDeliveryRepository.sumOfTotalFreight(smallOrderNo);
        if (totalFreightCount != null) {
            OdSmallOrder odSmallOrder = odSmallOrderRepository.findBySmallOrderNo(smallOrderNo);
            if (odSmallOrder != null) {
                odSmallOrder.setTotalFreight(totalFreightCount);
                odSmallOrderRepository.save(odSmallOrder);
            }
        }

    }

    /**
     * We don't delete product, we just change status to cancel
     */
    @Override
    @Transactional
    public CancelOrderResponse cancelOrderByProductId(CancelOrderRequest request) throws IdsmedBaseException {
        loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, CANCEL_ORDER_BY_PRODUCT_ID_METHOD_NAME, null);

        String bigOrderNo = request.getBigOrderNo();

        OdBigOrder odBigOrder = odBigOrderRepository.findByBigOrderNo(bigOrderNo);
        if (odBigOrder == null) {
            throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.BIG_ORDER_NOT_FOUND,
                    LangHelper.getLangCode(request.getLangCode())));
        }

        List<CancelSmallOrderRequest> cancelSmallOrderRequests = request.getSmallOrders();

        if (!CollectionUtils.isEmpty(cancelSmallOrderRequests)) {
            for (CancelSmallOrderRequest cancelSmallOrderRequest : cancelSmallOrderRequests) {
                List<CancelProductOrderRequest> cancelProductOrderRequests = cancelSmallOrderRequest.getProductList();
                if (!CollectionUtils.isEmpty(cancelProductOrderRequests)) {
                    processCancelOrderItem(cancelSmallOrderRequest.getSmallOrderNo(), cancelProductOrderRequests);
                }

                String smallOrderNo = cancelSmallOrderRequest.getSmallOrderNo();
                if (!StringUtils.isEmpty(smallOrderNo)) {
                    if (checkIsAllProductCancelled(smallOrderNo)) {
                        OdSmallOrder odSmallOrder = odSmallOrderRepository.findBySmallOrderNo(smallOrderNo);
                        odSmallOrder.setOrderStatus(ProcurementOrderStatusEnum.CANCELLED.getCode());
                        odSmallOrderRepository.save(odSmallOrder);
                    }
                }
            }
        }

        if (checkIsAllSmallOrderCancelled(bigOrderNo)) {
            odBigOrder.setOrderStatus(ProcurementOrderStatusEnum.CANCELLED.getCode());
            odBigOrderRepository.save(odBigOrder);
        }
        loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, CANCEL_ORDER_BY_PRODUCT_ID_METHOD_NAME, null);
        return new CancelOrderResponse(0,
                messageResourceService.getMessage(IdsmedMessageKeyConst.SUCCESS_MESSAGE_TO_WEDOCTOR,
                        LangHelper.getLangCode(LangEnum.CHINA.getCode())));
    }

    /**
     * Set order item (product) status to cancel
     */
    @Transactional
    private void processCancelOrderItem(String smallOrderNo, List<CancelProductOrderRequest> cancelProductOrderRequests) {
        for (CancelProductOrderRequest cancelProductOrderRequest : cancelProductOrderRequests) {
            OdOrderItem odOrderItem = odOrderItemRepository.findBySmallOrderNoAndGoodsId(smallOrderNo, cancelProductOrderRequest.getProductId());
            odOrderItem.setOrderStatus(ProcurementOrderStatusEnum.CANCELLED.getCode());
            odOrderItem.setCancelReason(cancelProductOrderRequest.getCancelReason());
            odOrderItemRepository.save(odOrderItem);
        }
    }

    /**
     * Check is all product with same small order number are cancelled
     * If total product count and cancelled product count are same,
     * means all product with the same small order number has been cancelled
     * @param smallOrderNo
     * @return
     */
    @Transactional
    private Boolean checkIsAllProductCancelled(String smallOrderNo) {
        List<OdOrderItem> totalProduct = odOrderItemRepository.findBySmallOrderNo(smallOrderNo);
        List<OdOrderItem> cancelledProduct = odOrderItemRepository
                .findBySmallOrderNoAndOrderStatus(smallOrderNo, ProcurementOrderStatusEnum.CANCELLED.getCode());

        if (!CollectionUtils.isEmpty(totalProduct)) {
            if (!CollectionUtils.isEmpty(cancelledProduct)) {
                if (totalProduct.size() == cancelledProduct.size()) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Check is all small order with same big order number are cancelled
     * If total small order count and cancelled small order count are same,
     * means all small order with same big order number has been canceled
     * @param bigOrderNo
     * @return
     */
    @Transactional
    private Boolean checkIsAllSmallOrderCancelled(String bigOrderNo) {
        List<OdSmallOrder> totalSmallOrder = odSmallOrderRepository.findByBigOrderNo(bigOrderNo);
        List<OdSmallOrder> cancelledSmallOrder = odSmallOrderRepository
                .findByBigOrderNoAndOrderStatus(bigOrderNo, ProcurementOrderStatusEnum.CANCELLED.getCode());

        if (!CollectionUtils.isEmpty(totalSmallOrder)) {
            if (!CollectionUtils.isEmpty(cancelledSmallOrder)) {
                if (totalSmallOrder.size() == cancelledSmallOrder.size()) {
                    return true;
                }
            }
        }

        return false;
    }
    private void loggerForAllMethodInThisService(String methodDescription, String methodName, String loginId){
        logger.info(methodDescription, methodName, loginId != null?loginId:null);
    }
}

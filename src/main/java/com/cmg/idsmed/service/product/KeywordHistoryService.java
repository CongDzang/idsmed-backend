package com.cmg.idsmed.service.product;

import com.cmg.idsmed.common.exception.IdsmedBaseException;
import com.cmg.idsmed.dto.product.search.KeywordHistoryResponse;

import java.util.List;

public interface KeywordHistoryService {
	List<KeywordHistoryResponse> getAllKeywordOrderByCount() throws IdsmedBaseException;
}

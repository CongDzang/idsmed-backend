package com.cmg.idsmed.service.product;

import com.cmg.idsmed.common.Constant.LoggerConstant;
import com.cmg.idsmed.common.exception.IdsmedBaseException;
import com.cmg.idsmed.dto.product.search.KeywordHistoryResponse;
import com.cmg.idsmed.model.repo.product.search.KeywordHistoryRepository;
import com.cmg.idsmed.service.accessright.AccessRightServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

@Configuration
@Service
public class KeywordHistoryServiceImpl implements KeywordHistoryService {
	private static final Logger logger = LoggerFactory.getLogger(KeywordHistoryServiceImpl.class);	@Autowired
	private KeywordHistoryRepository keywordHistoryRepository;

	@Autowired
	private Environment env;

	@Value("${logger.start.method.description}")
	String START_METHOD_DESCRIPTION;

	@Value("${logger.end.method.description}")
	String END_METHOD_DESCRIPTION;
	private static final String GET_ALL_KEYWORD_ORDER_BY_COUNT_METHOD_NAME = "getAllKeywordOrderByCount";

	@Override
	public List<KeywordHistoryResponse> getAllKeywordOrderByCount() throws IdsmedBaseException {
		loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, GET_ALL_KEYWORD_ORDER_BY_COUNT_METHOD_NAME, null);

		List<KeywordHistoryResponse> responses = new ArrayList<>();
		List<Object[]> keywordList = keywordHistoryRepository.findKeywordDistinctOrderByCount();
		if (!CollectionUtils.isEmpty(keywordList)) {
			for (Object[] obj : keywordList) {
				String keyword = (String)obj[0];
				Long count = (Long)obj[1];
				KeywordHistoryResponse response = new KeywordHistoryResponse(keyword, count);
				responses.add(response);
			}
		}
		loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, GET_ALL_KEYWORD_ORDER_BY_COUNT_METHOD_NAME, null);
		return responses;
	}
	private void loggerForAllMethodInThisService(String methodDescription, String methodName, String loginId){
		logger.info(methodDescription, methodName, loginId != null?loginId:null);
	}
}

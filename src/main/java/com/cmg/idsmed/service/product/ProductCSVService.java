package com.cmg.idsmed.service.product;

import com.cmg.idsmed.common.exception.IdsmedBaseException;
import com.cmg.idsmed.dto.product.ProductCSVUploadRequest;
import com.cmg.idsmed.dto.product.ProductCSVUploadedQueueMessage;
import com.cmg.idsmed.model.entity.product.Product;
import com.cmg.idsmed.model.entity.product.ProductMedia;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.List;

public interface ProductCSVService {
	Boolean uploadProductCSVIntoOss(ProductCSVUploadRequest request, MultipartFile file) throws IdsmedBaseException;
	void processProductCSV(ProductCSVUploadedQueueMessage message) throws IdsmedBaseException, IOException, Exception;
	List<ProductMedia> pullMediaFromCustomerSourceAndPushToOss(List<ProductMedia> medias) throws IOException, IdsmedBaseException;
	List<ProductMedia> processProductImages(Product product) throws IOException;
	Boolean validateProductOriginalImage(File file);
}

package com.cmg.idsmed.service.product;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import com.cmg.idsmed.common.Constant.LoggerConstant;
import com.cmg.idsmed.common.Constant.PropertiesLabelConst;
import com.cmg.idsmed.common.enums.*;
import com.cmg.idsmed.model.entity.product.*;
import com.cmg.idsmed.model.repo.product.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.AmqpException;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.cmg.idsmed.common.Constant.ProductCSVColumnIndex;
import com.cmg.idsmed.common.Constant.ProductFieldsLength;
import com.cmg.idsmed.common.exception.ErrorInfo;
import com.cmg.idsmed.common.exception.IdsmedBaseException;
import com.cmg.idsmed.common.utils.OssUtils;
import com.cmg.idsmed.config.rabbitmq.IdsmedQueueConfig;
import com.cmg.idsmed.dto.product.ProductCSVProcessStatus;
import com.cmg.idsmed.dto.product.ProductCSVUploadRequest;
import com.cmg.idsmed.dto.product.ProductCSVUploadedQueueMessage;
import com.cmg.idsmed.mail.EmailObject;
import com.cmg.idsmed.mail.EmailService;
import com.cmg.idsmed.mail.ProductCSVProcessReportEmailObject;
import com.cmg.idsmed.model.entity.auth.IdsmedAccount;
import com.cmg.idsmed.model.entity.auth.IdsmedUser;
import com.cmg.idsmed.model.entity.product.log.ProductCSVLog;
import com.cmg.idsmed.model.entity.vendor.Vendor;
import com.cmg.idsmed.model.repo.auth.IdsmedAccountRepository;
import com.cmg.idsmed.model.repo.product.log.ProductCSVLogRepository;
import com.cmg.idsmed.model.repo.user.IdsmedUserRepository;
import com.cmg.idsmed.model.repo.vendor.VendorRepository;
import com.cmg.idsmed.service.auth.AuthenticationService;
import com.cmg.idsmed.service.share.FileDownloadService;
import com.cmg.idsmed.service.share.FileUploadService;
import com.cmg.idsmed.service.share.ImageProcessService;
import com.cmg.idsmed.service.share.MessageResourceService;
import com.cmg.idsmed.service.share.NotificationService;
import com.cmg.idsmed.service.share.SecurityContextHelper;
import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;

import javax.imageio.ImageIO;

@Configuration
@Service
public class ProductCSVServiceImpl implements ProductCSVService {
	@Autowired
	private FileUploadService fileUploadService;

	@Autowired
	private FileDownloadService fileDownloadService;

	@Autowired
	private MessageResourceService messageResourceService;

	@Autowired
	private ProductCareAreaRepository productCareAreaRepository;

	@Autowired
	private ProductRepository productRepository;

	@Autowired
	private IdsmedUserRepository idsmedUserRepository;

	@Autowired
	private IdsmedAccountRepository idsmedAccountRepository;

	@Autowired
	private ProductCareAreaDetailRepository careAreaDetailRepository;

	@Autowired
	private ProductCategoryRepository productCategoryRepository;

	@Autowired
	private ProductBrandRepository productBrandRepository;

	@Autowired
	private VendorRepository vendorRepository;

	@Autowired
	private ProductMediaRepository mediaRepository;

	@Autowired
	private ProductTechnicalRepository technicalRepository;

	@Autowired
	private ProductFeatureRepository featureRepository;

	@Autowired
	private OssUtils ossUtils;

	@Autowired
	private ImageProcessService imageProcessService;

	@Autowired
	private NotificationService notificationService;

	@Autowired
	private AuthenticationService authenticationService;

	@Autowired
	private EmailService emailService;

	@Autowired
	private RabbitTemplate rabbitTemplate;

	@Autowired
	private Environment env;

	@Autowired
	private ProductRatingRepository productRatingRepository;

	@Autowired
	private ProductWishlistRepository productWishlistRepository;

	private final static Integer PRODUCT_PRICE_DECIMAL_POINT = 2;

	@Value("${logger.start.method.description}")
	String START_METHOD_DESCRIPTION;

	@Value("${logger.end.method.description}")
	String END_METHOD_DESCRIPTION;
	private static final String UPLOAD_PRODUCT_CSV_INTO_OSS_METHOD_NAME = "uploadProductCSVIntoOss";

	@Autowired
	private ProductCSVLogRepository csvLogRepository;

	private static final Map<Long, ProductCSVProcessStatus> PRODUCT_CSV_PROCESS_STATUS = new ConcurrentHashMap<>();

	private static final String PRODUCT_PREVIEW_PERMISSION_CODE = "PP0000004";

	private static final Logger logger = LoggerFactory.getLogger(ProductCSVServiceImpl.class);

	public Boolean uploadProductCSVIntoOss(ProductCSVUploadRequest request, MultipartFile file)
			throws IdsmedBaseException {
        loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, UPLOAD_PRODUCT_CSV_INTO_OSS_METHOD_NAME, request.getUploadBy());
		fileUploadService.uploadProductCSV(request, file);
		loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, UPLOAD_PRODUCT_CSV_INTO_OSS_METHOD_NAME, request.getUploadBy());
		return true;
	}

	@Override
	public void processProductCSV(ProductCSVUploadedQueueMessage message) throws Exception {
		logger.info("Start process csv file: {}", message.getProductCSVOssUrl());

		if (message.getProductCSVOssUrl() == null) {
			throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.DOWNLOAD_FILE_FROM_OSS_ERROR,
					LangEnum.ENGLISH.getCode()));
		}

		// Clear previous product csv process status then create new one for this
		// process.
		PRODUCT_CSV_PROCESS_STATUS.remove(message.getVendorId());
		PRODUCT_CSV_PROCESS_STATUS.computeIfAbsent(message.getVendorId(), k -> new ProductCSVProcessStatus());

		File file = fileDownloadService.downloadProductCSVOrExcelFileFromOSS(message.getProductCSVOssUrl());
		List<Product> products = new ArrayList<>();
		if (!file.exists()) {
			throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.DOWNLOAD_FILE_FROM_OSS_ERROR,
					LangEnum.ENGLISH.getCode()));
		}
		try {
			products = processCSVFile(file, message.getVendorId(), message.getUploadBy());
		} catch (Exception e) {
			logger.error("Error while process CSV file", e.getMessage());
			throw e;
		}

		//Save log here but send report email at the end of this method
		List<Product> finalProducts = products;

		//TODO: Variable for support email. Need to refactor later on
		String vendorEmail = "";
		String companyCode = "";
		String originalFileName = "";
		Integer totalCreatedProduct = 0;
		String errorRowInfo = "";

		Optional<ProductCSVLog> logOp = csvLogRepository.findById(message.getProductCsvLogId());
		if (logOp.isPresent()) {
			ProductCSVLog log = logOp.get();
			log.setStatus(ProductCSVStatusEnum.PROCESS_DONE.getCode());
			ProductCSVProcessStatus csvProcessStatus = PRODUCT_CSV_PROCESS_STATUS.get(message.getVendorId());
			if (csvProcessStatus != null) {
				if (csvProcessStatus.getCSVErrorRowsInfo() != null)
					log.setErrorRowInfo(csvProcessStatus.getCSVErrorRowsInfo());
			}
			if (!CollectionUtils.isEmpty(finalProducts)) {
				List<Long> createdProductIds = finalProducts.stream().map(p -> p.getId()).collect(Collectors.toList());
				log.setProductCreatedInfoByList(createdProductIds);
				log.setTotalProductCreated(finalProducts.size());
			}
			originalFileName = log.getOriginalFileName();
			totalCreatedProduct = log.getTotalProductCreated();
			errorRowInfo = log.getErrorRowInfo();
			vendorEmail = log.getVendorEmail();
			companyCode = log.getCompanyCode();
			csvLogRepository.save(log);

		}

		file.delete();

		for (Product product : products) {
			// Download brochure from customer url and upload to OSS
			productBrochureProcess(product);

			// Download video from customer url and upload to OSS
			productVideoProcess(product);

			// Download images from customer url and upload to OSS
			productOriginalImagesProcess(product);
		}

		// Implement Resize images
		for (Product product : products) {
			processProductImages(product);
		}
		//Send report email to queue
		String sendFrom = env.getProperty(PropertiesLabelConst.SMTP_SUPPORT_MAIL_LABEL);

		String vendorName = "";
		Optional<Vendor> vendor = vendorRepository.findById(message.getVendorId());
		if (vendor.isPresent()) {
			vendorName = vendor.get().getCompanyNamePrimary();
		}

		if (!CollectionUtils.isEmpty(products)) {
			products.forEach(p -> {
				p.setStatus(StatusEnum.DRAFT.getCode());
			});
		}

		String loginId = message.getUploadBy();
		IdsmedAccount uploaderAccount = idsmedAccountRepository.findByLoginId(loginId);
		IdsmedUser uploaderUser = uploaderAccount.getIdsmedUser();
		String userEmail = uploaderUser.getEmail();
		String userName = uploaderUser.getFirstName() + " " + uploaderUser.getLastName();

		productRepository.saveAll(products);
		//Send email
		logger.info("Start send product batch upload notification email");
		errorRowInfo = StringUtils.isEmpty(errorRowInfo) ? "" : errorRowInfo;
		Pair<String, String> emailPair = sendProductCSVProcessReport(userName, vendorName, userEmail, sendFrom, originalFileName, totalCreatedProduct, errorRowInfo, uploaderUser);
		logger.info("end send product batch upload notification email");

		//Send web notification
		logger.info("start send product batch upload notification");

		List<IdsmedAccount> accountList = new ArrayList<>();
		List<IdsmedUser> vendorUsers = idsmedUserRepository.findAllByCompanyCode(vendor.get().getCompanyCode());

		for (IdsmedUser idsmedUser : vendorUsers) {
			List<IdsmedAccount> accounts = idsmedUser.getIdsmedAccounts().isEmpty() ? null : idsmedUser.getIdsmedAccounts();
			accountList.addAll(accounts);
		}

		//TODO : temporary for avoid SecurityContextHelper.getCurrentAccount() can not execute
		IdsmedAccount senderAccount = idsmedAccountRepository.findByLoginId(message.getUploadBy());
		if (senderAccount != null) {
			notificationService.sendProductApprovedNotification(senderAccount, accountList, emailPair);
		}

		logger.info("end send product batch upload notification");

		logger.info(env.getProperty(LoggerConstant.LOGGER_START_METHOD_DESCRIPTION) + "file: {} and a report email sent to queue", message.getProductCSVOssUrl());
}

	@Override
	public List<ProductMedia> processProductImages(Product product) throws IOException {
		logger.info("Start process resize image of product with id {}", product.getId());
		List<ProductMedia> originalImages = mediaRepository.findAllByProductAndDocumentTypeAndVersion(product,
				ProductMedia.MEDIA_TYPE.IMAGE.getType(), ImageVersionEnum.VERSION1.getVersion());
		List<ProductMedia> resizedImages = new ArrayList<>();
		if (CollectionUtils.isEmpty(originalImages)) {
			logger.info("Product with id {} does't has image", product.getId());
			return null;
		}
		for (ProductMedia originalImg : originalImages) {
			if (StringUtils.isEmpty(originalImg.getDocumentUrl())) {
				continue;
			}
			List<Pair<File, Integer>> resizedFiles = new ArrayList<>();
			for (ImageVersionEnum imgEnum : ImageVersionEnum.values()) {
				if (imgEnum.equals(ImageVersionEnum.VERSION1))
					continue;
				Pair<File, Integer> resignPair = imageProcessService.resizeProductImage(originalImg.getDocumentUrl(),
						imgEnum);
				if (resignPair != null)
					resizedFiles.add(resignPair);
			}

			//TODO update resized images false to PRODUCT_CSV_PROCESS_STATUS;
//			Pair<Integer, List<String>> errorResizeImage = Pair.of()

			if (!CollectionUtils.isEmpty(resizedFiles)) {
				for (Pair<File, Integer> resignFilePair : resizedFiles) {
					String uploadFilePath = env.getProperty(OssUtils.IDSMED_PRODUCT_OSS_UPLOAD_PATH);
					String originalFileName = originalImg.getCustomerUrl().substring(
							originalImg.getCustomerUrl().lastIndexOf("/") + 1,
							originalImg.getCustomerUrl().length());
					String fileName = resignFilePair.getFirst().getName();
					String documentUrl = ossUtils.uploadFile(uploadFilePath, fileName,
							resignFilePair.getFirst().getPath());

					if (!StringUtils.isEmpty(documentUrl)) {
						ProductMedia resizedImg = new ProductMedia();
						resizedImg.setProduct(product);
						resizedImg.setDocumentUrl(documentUrl);
						resizedImg.setDocumentSeq(originalImg.getDocumentSeq());
						resizedImg.setDocumentName(originalFileName);
						resizedImg.setVersion(resignFilePair.getSecond());
						resizedImg.setDocumentType(ProductMedia.MEDIA_TYPE.IMAGE.getType());
						resizedImg.setCreatedBy(product.getCreatedBy());
						resizedImg.setUpdatedBy(product.getCreatedBy());
						URL url = new URL(documentUrl);
						resizedImg.setGid(originalImg.getGid());
						//Set this ProductMedia to approved. Need to implement this logic in approved product later
						resizedImg.setStatus(StatusEnum.APPROVED.getCode());
						resizedImages.add(resizedImg);
					}
					resignFilePair.getFirst().delete();
				}
			}

		}
		resizedImages = mediaRepository.saveAll(resizedImages);
		logger.info("Start process resize image of product with id {}", product.getId());
		return resizedImages;
	}

	@Transactional
	public List<Product> processCSVFile(File file, Long vendorId, String uploadBy) throws IOException, IdsmedBaseException, Exception {
		logger.info("Start process Product CSV File");
		List<Product> products = new ArrayList<>();

		// Iterate csv file and fetch raw data
		List<ProductRawData> productRawDatas = new ArrayList<>();
		String[] productCSVLineSplitBySeparator = null;
		CSVParser parser = new CSVParserBuilder().withSeparator('|').build();
		try (BufferedReader br = Files.newBufferedReader(Paths.get(file.toURI()), StandardCharsets.UTF_8);
			 CSVReader reader = new CSVReaderBuilder(br).withCSVParser(parser).build()) {
			// Ignore header row
			reader.readNext();
			// Iterate data rows
			Integer index = 1;
			while ((productCSVLineSplitBySeparator = reader.readNext()) != null) {
				ProductRawData productRawData;
				try {
					productRawData = getProductRawData(productCSVLineSplitBySeparator);
				} catch (Exception e) {
					logger.error("Error when get raw data from csv file:", e.getMessage());
					throw e;
				}

				if (productRawData == null) continue;
				productRawData.setCsvRowNum(index);
				index++;
				productRawDatas.add(productRawData);
			}
		}

		//This list to record error CSV rows.
		List<Integer> productCSVErrorRows = new ArrayList<>();

		List<ProductRawData> productRawDataValidList = productRawDatas
				.stream()
				.filter(pr -> {
					Boolean isValid = validateProductCSVData(pr);
					if (!isValid) productCSVErrorRows.add(pr.getCsvRowNum());
					return isValid;
				}).collect(Collectors.toList());

		//Update the CSV file process
		updateProductCSVProcessStatus(vendorId, productCSVErrorRows);

		Map<Product, List<ProductCareArea>> productCareAreaMap = new HashMap<>();
		// Create product set meta data and save it.
		if (!CollectionUtils.isEmpty(productRawDataValidList)) {
			productRawDataValidList.forEach(prd -> {
				Product product = new Product(prd);

				String currentLoginId = uploadBy;
				Long currentAccountId = idsmedAccountRepository.findByLoginId(currentLoginId).getId();
				if (currentAccountId != null) {
					product.setCreatedBy(currentAccountId);
					product.setUpdatedBy(currentAccountId);
				}
				productRepository.save(product);

				// Set product care area.
				ProductCareAreaDetail productCareAreaDetail = null;
				String careAreaName = prd.getCareArea();
				if (!StringUtils.isEmpty(careAreaName)) {
					ProductCareArea careArea = productCareAreaRepository.findFirstByCareAreaNameOrCareAreaNameZhCnOrCareAreaNameZhTwOrCareAreaNameViOrCareAreaNameThOrCareAreaNameId(careAreaName
							, careAreaName, careAreaName, careAreaName, careAreaName, careAreaName);
					if (careArea != null) {
						productCareAreaDetail = new ProductCareAreaDetail(careArea, product);
					} else {
						careArea = productCareAreaRepository
								.findFirstByCareAreaName(ProductCareArea.OTHER_PRODUCT_CARE_AREA_NAME);
						productCareAreaDetail = new ProductCareAreaDetail(careArea, product);
					}
				} else {
					ProductCareArea careArea = productCareAreaRepository
							.findFirstByCareAreaName(ProductCareArea.OTHER_PRODUCT_CARE_AREA_NAME);
					productCareAreaDetail = new ProductCareAreaDetail(careArea, product);
					if (currentAccountId != null) {
						productCareAreaDetail.setCreatedBy(currentAccountId);
						productCareAreaDetail.setUpdatedBy(currentAccountId);
					}
				}

				careAreaDetailRepository.save(productCareAreaDetail);

				// Set product category, If customer doesn't pass category name. set it to
				// "OTHER"
				product.setProductCategory(productCategoryRepository
						.findFirstByCategoryName(ProductCategory.OTHERS_PRODUCT_CATEGORY_NAME));
				String categoryName = prd.getCategory();
				if (!StringUtils.isEmpty(categoryName)) {
					ProductCategory category = productCategoryRepository.findFirstByCategoryNameOrCategoryNameZhCnOrCategoryNameZhTwOrCategoryNameViOrCategoryNameThOrCategoryNameId(categoryName
							, categoryName, categoryName, categoryName, categoryName, categoryName);
					if (category != null) {
						product.setProductCategory(category);
					}
				}

				//Set product brand.
				ProductBrand brand = null;
				if (StringUtils.isEmpty(prd.getBrand())) {
					brand = productBrandRepository.findFirstByBrandName(ProductBrand.OTHERS_PRODUCT_BRAND_NAME);
				} else {
					brand = productBrandRepository.findFirstByBrandName(prd.getBrand());
					if (brand == null) {
						brand = new ProductBrand();
						brand.setBrandName(prd.getBrand());
						brand.setBrandCode(prd.getBrand());
						brand.setStatus(StatusEnum.APPROVED.getCode());
						if (currentAccountId != null) {
							brand.setCreatedBy(currentAccountId);
							brand.setUpdatedBy(currentAccountId);
						}
						brand = productBrandRepository.save(brand);
					}
				}

				product.setProductBrand(brand);

				// Set Vendor
				Vendor vendor = vendorRepository.getOne(vendorId);
				if (vendor != null) {
					product.setVendor(vendor);
				}

				// Set ProductMediaList raw data which includes Brochure, Video, Images
				List<ProductMedia> mediaList = new ArrayList<>();
				// Set Brochure raw data.
				if (!StringUtils.isEmpty(prd.getBrochureAttachmentUrl())) {
					ProductMedia productMedia = new ProductMedia();
					productMedia.setDocumentType(ProductMedia.MEDIA_TYPE.BROCHURE.getType());
					productMedia.setCustomerUrl(prd.getBrochureAttachmentUrl());
					if (currentAccountId != null) {
						productMedia.setCreatedBy(currentAccountId);
						productMedia.setUpdatedBy(currentAccountId);
					}
					productMedia.setProduct(product);
					mediaList.add(productMedia);
				}

				// Set Video raw data
				if (!StringUtils.isEmpty(prd.getVideoAttachmentUrl())) {
					ProductMedia productMedia = new ProductMedia();
					productMedia.setDocumentType(ProductMedia.MEDIA_TYPE.VIDEO.getType());
					productMedia.setCustomerUrl(prd.getVideoAttachmentUrl());
					if (currentAccountId != null) {
						productMedia.setCreatedBy(currentAccountId);
						productMedia.setUpdatedBy(currentAccountId);
					}
					productMedia.setProduct(product);
					mediaList.add(productMedia);
				}

				// Set Product images
				if (!CollectionUtils.isEmpty(prd.getImagesUrl())) {
					Integer index = 1;
					for (String im : prd.getImagesUrl()) {
						ProductMedia productMedia = new ProductMedia();
						productMedia.setDocumentType(ProductMedia.MEDIA_TYPE.IMAGE.getType());
						productMedia.setCustomerUrl(im);
						productMedia.setDocumentSeq(index);
						if (currentAccountId != null) {
							productMedia.setCreatedBy(currentAccountId);
							productMedia.setUpdatedBy(currentAccountId);
						}
						index++;
						productMedia.setProduct(product);
						mediaList.add(productMedia);
					}

				}

				if (!CollectionUtils.isEmpty(mediaList)) {
					mediaList = mediaRepository.saveAll(mediaList);
					product.setProductMediaList(mediaList);
				}

				// Set Product technical features raw data.
				List<ProductTechnical> productTechnicals = new ArrayList<>();
				if (!CollectionUtils.isEmpty(prd.getProductTechnicals())) {
					Integer index = 1;
					for (Pair<String, String> p : prd.getProductTechnicals()) {
						ProductTechnical productTechnical = new ProductTechnical();
						productTechnical.setProductLabel(p.getFirst());
						productTechnical.setProductValue(p.getSecond());
						productTechnical.setProductLabelSequence(index);
						if (currentAccountId != null) {
							productTechnical.setCreatedBy(currentAccountId);
							productTechnical.setUpdatedBy(currentAccountId);
						}
						index++;
						productTechnical.setProduct(product);
						productTechnicals.add(productTechnical);
					}

				}

				if (!CollectionUtils.isEmpty(productTechnicals)) {
					productTechnicals = technicalRepository.saveAll(productTechnicals);
					product.setProductTechnicalList(productTechnicals);
				}

				// Set product features raw data
				List<ProductFeatures> productFeaturesList = new ArrayList<>();
				if (!CollectionUtils.isEmpty(prd.getProductFeatures())) {
					Integer index = 1;
					for (Pair<String, String> p : prd.getProductFeatures()) {
						ProductFeatures feature = new ProductFeatures();
						feature.setProductLabel(p.getFirst());
						feature.setProductValue(p.getSecond());
						feature.setProductLabelSequence(index);
						if (currentAccountId != null) {
							feature.setCreatedBy(currentAccountId);
							feature.setUpdatedBy(currentAccountId);
						}
						index++;
						feature.setProduct(product);
						productFeaturesList.add(feature);
					}
				}
				if (!CollectionUtils.isEmpty(productFeaturesList)) {
					productFeaturesList = featureRepository.saveAll(productFeaturesList);
					product.setProductFeaturesList(productFeaturesList);
				}

				//initialize product rating when create new product, new product need to add into product rating
				ProductRating productRating = new ProductRating(product, currentAccountId);
				productRatingRepository.save(productRating);

				//Initialized product wishlist when create new product, new product need to add into product wishlist
				ProductWishlist productWishlist = new ProductWishlist(product, 0L, currentAccountId);
				productWishlistRepository.save(productWishlist);

				product.setStatus(StatusEnum.PRODUCT_IMAGES_PROCESSING.getCode());
				product = productRepository.save(product);
				products.add(product);
			});
		}

		return products;
	}

	@Transactional
	public Product productBrochureProcess(Product product) throws IOException, IdsmedBaseException {
		logger.info("Start process brochure of product with id is {}", product.getId());
		List<ProductMedia> bochures = mediaRepository.findByProductAndDocumentType(product,
				ProductMedia.MEDIA_TYPE.BROCHURE.getType());
		pullMediaFromCustomerSourceAndPushToOss(bochures);
		return product;
	}

	@Transactional
	public Product productVideoProcess(Product product) throws IOException, IdsmedBaseException {
		logger.info("Start process video of product with id is {}", product.getId());
		List<ProductMedia> videos = mediaRepository.findByProductAndDocumentType(product,
				ProductMedia.MEDIA_TYPE.VIDEO.getType());

		pullMediaFromCustomerSourceAndPushToOss(videos);

		return product;
	}

	/**
	 * @param product
	 * @return product This method implement logic:
	 */
	private Product productOriginalImagesProcess(Product product) {
		logger.info("Start process original images of product with id is {}", product.getId());
		List<ProductMedia> images = mediaRepository.findByProductAndDocumentType(product,
				ProductMedia.MEDIA_TYPE.IMAGE.getType());
		if (!CollectionUtils.isEmpty(images)) {
			for (ProductMedia pm : images) {
				if (!StringUtils.isEmpty(pm.getDocumentUrl()))
					continue;
				pm = processOriginalImage(pm);
				//If can't download the image from customer_url and upload to OSS we need to delete ProductMedia
				if (pm != null && StringUtils.isEmpty(pm.getDocumentUrl())) {
					mediaRepository.delete(pm);
				}
			}
		}
		return product;
	}

	private ProductMedia processOriginalImage(ProductMedia image) {
		List<ProductMedia> media = new ArrayList<>();
		if (image != null) {
			media.add(image);
		}
		if (!CollectionUtils.isEmpty(media)) {
			media = pullOriginalImageFromCustomerAndPushToOss(media);
		}
		if (!CollectionUtils.isEmpty(media)) {
			return media.get(0);
		}
		return null;
	}

	@Transactional
	@Modifying
	public List<ProductMedia> pullOriginalImageFromCustomerAndPushToOss(List<ProductMedia> medias) {
		if (!CollectionUtils.isEmpty(medias)) {
			ListIterator<ProductMedia> iterator = medias.listIterator();
			while (iterator.hasNext()) {
				ProductMedia media = iterator.next();
				File file = fileDownloadService.downloadFileFromUrl(media.getCustomerUrl());
				if (file == null || !validateProductOriginalImage(file)) {
					mediaRepository.delete(media);
					iterator.remove();
					continue;
				}

				String uploadFilePath = "idsmedmedia/product";
				String originalFileName = media.getCustomerUrl()
						.substring(media.getCustomerUrl().lastIndexOf("/") + 1, media.getCustomerUrl().length());
				String fileName = file.getName();

				String documentUrl = ossUtils.uploadFile(uploadFilePath, fileName, file.getPath());

				if (StringUtils.isEmpty(documentUrl)) {
					logger.info("Can't upload media file to OSS: {}", file.getName());
					mediaRepository.delete(media);
					iterator.remove();
					continue;
				}

				media.setDocumentUrl(documentUrl);
				media.setDocumentName(originalFileName);
				media.setGid(fileName);
				//Set this ProductMedia to approved. Need to implement this logic in approved product later
				media.setStatus(StatusEnum.APPROVED.getCode());
				mediaRepository.save(media);
				file.delete();
			}
		}
		return medias;
	}

	public Boolean validateProductOriginalImage(File file) {
		//TODO implement logic check original image
		try {
			BufferedImage bufferedImage = ImageIO.read(file);
			Integer width = bufferedImage.getWidth();
			Integer height = bufferedImage.getHeight();
			if (width < 800 || width > 1200 || height < 600 || height > 1200) {
				return false;
			}
		} catch (IOException ioe) {
			return false;
		}
		return true;
	}

	@Override
	@Transactional
	@Modifying
	public List<ProductMedia> pullMediaFromCustomerSourceAndPushToOss(List<ProductMedia> medias) {
		if (!CollectionUtils.isEmpty(medias)) {
			ListIterator<ProductMedia> iterator = medias.listIterator();
			while (iterator.hasNext()) {
				ProductMedia media = iterator.next();
				File file = fileDownloadService.downloadFileFromUrl(media.getCustomerUrl());

				if (file == null) {
					mediaRepository.delete(media);
					iterator.remove();
					continue;
				}

				String uploadFilePath = "idsmedmedia/product";
				String originalFileName = media.getCustomerUrl()
						.substring(media.getCustomerUrl().lastIndexOf("/") + 1, media.getCustomerUrl().length());
				String fileName = file.getName();

				String documentUrl = ossUtils.uploadFile(uploadFilePath, fileName, file.getPath());

				if (StringUtils.isEmpty(documentUrl)) {
					logger.info("Can't upload media file to OSS: {}", file.getName());
					mediaRepository.delete(media);
					iterator.remove();
					continue;
				}

				media.setDocumentUrl(documentUrl);
				media.setDocumentName(originalFileName);
				media.setGid(fileName);
				//Set this ProductMedia to approved. Need to implement this logic in approved product later
				media.setStatus(StatusEnum.APPROVED.getCode());
				mediaRepository.save(media);
				file.delete();
			}
		}
		return medias;
	}

	private ProductRawData getProductRawData(String[] data) throws Exception {
		ProductRawData productRawData = new ProductRawData();
		productRawData.setCareArea(data[ProductCSVColumnIndex.CARE_AREA_COLUMN_INDEX]);
		productRawData.setCategory(data[ProductCSVColumnIndex.CATEGORY_COLUMN_INDEX]);
		productRawData.setBrand(data[ProductCSVColumnIndex.BRAND_COLUMN_INDEX]);
		productRawData.setProductNamePrimary(data[ProductCSVColumnIndex.PRODUCT_NAME_PRIMARY_COLUMN_INDEX]);
		productRawData.setProductNameSecondary(data[ProductCSVColumnIndex.PRODUCT_NAME_SECONDARY_COLUMN_INDEX]);
		productRawData.setProductCode(data[ProductCSVColumnIndex.PRODUCT_CODE_COLUMN_INDEX]);
		productRawData.setProductModel(data[ProductCSVColumnIndex.PRODUCT_MODEL_COLUMN_INDEX]);
		productRawData.setProductRegNo(data[ProductCSVColumnIndex.PRODUCT_REG_NO_COLUMN_INDEX]);
		productRawData.setProductDes(data[ProductCSVColumnIndex.PRODUCT_DESCRIPTION_COLUMN_INDEX]);
		productRawData.setHashTag(data[ProductCSVColumnIndex.PRODUCT_HASHTAG_COLUMN_INDEX]);
		productRawData.setUnit(data[ProductCSVColumnIndex.PRODUCT_UNIT_COLUMN_INDEX]);

		if (!StringUtils.isEmpty(data[ProductCSVColumnIndex.PRODUCT_PRICE_COLUMN_INDEX])) {
			BigDecimal rawPrice = new BigDecimal(data[ProductCSVColumnIndex.PRODUCT_PRICE_COLUMN_INDEX]);
			rawPrice = rawPrice.setScale(PRODUCT_PRICE_DECIMAL_POINT, RoundingMode.HALF_UP);
			productRawData.setPrice(rawPrice);
		}

		productRawData.setPackaging(data[ProductCSVColumnIndex.PRODUCT_PACKAGING_COLUMN_INDEX]);

		if (!StringUtils.isEmpty(data[ProductCSVColumnIndex.PRODUCT_MIN_OF_ORDER_COLUMN_INDEX])) {
			if (!checkNumeric(data[ProductCSVColumnIndex.PRODUCT_MIN_OF_ORDER_COLUMN_INDEX])) return null;
			productRawData
					.setMinOfOrder(Integer.parseInt(data[ProductCSVColumnIndex.PRODUCT_MIN_OF_ORDER_COLUMN_INDEX]));

		}

		productRawData
				.setBrochureAttachmentUrl(data[ProductCSVColumnIndex.PRODUCT_BROCHURE_ATTACHMENT_URL_COLUMN_INDEX]);

		productRawData.setVideoAttachmentUrl(data[ProductCSVColumnIndex.PRODUCT_VIDEO_ATTACHMENT_URL_COLUMN_INDEX]);

		// Process image url raw data
		List<String> imageURLs = new ArrayList<>();

		if (!StringUtils.isEmpty(data[ProductCSVColumnIndex.PRODUCT_IMAGE_1_COLUMN_INDEX])) {
			imageURLs.add(data[ProductCSVColumnIndex.PRODUCT_IMAGE_1_COLUMN_INDEX]);
		}

		if (!StringUtils.isEmpty(data[ProductCSVColumnIndex.PRODUCT_IMAGE_2_COLUMN_INDEX])) {
			imageURLs.add(data[ProductCSVColumnIndex.PRODUCT_IMAGE_2_COLUMN_INDEX]);
		}

		if (!StringUtils.isEmpty(data[ProductCSVColumnIndex.PRODUCT_IMAGE_3_COLUMN_INDEX])) {
			imageURLs.add(data[ProductCSVColumnIndex.PRODUCT_IMAGE_3_COLUMN_INDEX]);
		}

		if (!StringUtils.isEmpty(data[ProductCSVColumnIndex.PRODUCT_IMAGE_4_COLUMN_INDEX])) {
			imageURLs.add(data[ProductCSVColumnIndex.PRODUCT_IMAGE_4_COLUMN_INDEX]);
		}

		if (!StringUtils.isEmpty(data[ProductCSVColumnIndex.PRODUCT_IMAGE_5_COLUMN_INDEX])) {
			imageURLs.add(data[ProductCSVColumnIndex.PRODUCT_IMAGE_5_COLUMN_INDEX]);
		}

		if (!StringUtils.isEmpty(data[ProductCSVColumnIndex.PRODUCT_IMAGE_6_COLUMN_INDEX])) {
			imageURLs.add(data[ProductCSVColumnIndex.PRODUCT_IMAGE_6_COLUMN_INDEX]);
		}

		productRawData.setImagesUrl(imageURLs);

		// Process technical raw data
		List<Pair<String, String>> productTechnicals = new ArrayList<>();

		if (!StringUtils.isEmpty(data[ProductCSVColumnIndex.PRODUCT_TECH_LABEL_1_COLUMN_INDEX])
				&& !StringUtils.isEmpty(data[ProductCSVColumnIndex.PRODUCT_TECH_VALUE_1_COLUMN_INDEX])) {
			Pair<String, String> productTechnical = Pair.of(
					data[ProductCSVColumnIndex.PRODUCT_TECH_LABEL_1_COLUMN_INDEX],
					data[ProductCSVColumnIndex.PRODUCT_TECH_VALUE_1_COLUMN_INDEX]);
			productTechnicals.add(productTechnical);
		}

		if (!StringUtils.isEmpty(data[ProductCSVColumnIndex.PRODUCT_TECH_LABEL_2_COLUMN_INDEX])
				&& !StringUtils.isEmpty(data[ProductCSVColumnIndex.PRODUCT_TECH_VALUE_2_COLUMN_INDEX])) {
			Pair<String, String> productTechnical = Pair.of(
					data[ProductCSVColumnIndex.PRODUCT_TECH_LABEL_2_COLUMN_INDEX],
					data[ProductCSVColumnIndex.PRODUCT_TECH_VALUE_2_COLUMN_INDEX]);
			productTechnicals.add(productTechnical);
		}

		if (!StringUtils.isEmpty(data[ProductCSVColumnIndex.PRODUCT_TECH_LABEL_3_COLUMN_INDEX])
				&& !StringUtils.isEmpty(data[ProductCSVColumnIndex.PRODUCT_TECH_VALUE_3_COLUMN_INDEX])) {
			Pair<String, String> productTechnical = Pair.of(
					data[ProductCSVColumnIndex.PRODUCT_TECH_LABEL_3_COLUMN_INDEX],
					data[ProductCSVColumnIndex.PRODUCT_TECH_VALUE_3_COLUMN_INDEX]);
			productTechnicals.add(productTechnical);
		}

		if (!StringUtils.isEmpty(data[ProductCSVColumnIndex.PRODUCT_TECH_LABEL_4_COLUMN_INDEX])
				&& !StringUtils.isEmpty(data[ProductCSVColumnIndex.PRODUCT_TECH_VALUE_4_COLUMN_INDEX])) {
			Pair<String, String> productTechnical = Pair.of(
					data[ProductCSVColumnIndex.PRODUCT_TECH_LABEL_4_COLUMN_INDEX],
					data[ProductCSVColumnIndex.PRODUCT_TECH_VALUE_4_COLUMN_INDEX]);
			productTechnicals.add(productTechnical);
		}

		if (!StringUtils.isEmpty(data[ProductCSVColumnIndex.PRODUCT_TECH_LABEL_5_COLUMN_INDEX])
				&& !StringUtils.isEmpty(data[ProductCSVColumnIndex.PRODUCT_TECH_VALUE_5_COLUMN_INDEX])) {
			Pair<String, String> productTechnical = Pair.of(
					data[ProductCSVColumnIndex.PRODUCT_TECH_LABEL_5_COLUMN_INDEX],
					data[ProductCSVColumnIndex.PRODUCT_TECH_VALUE_5_COLUMN_INDEX]);
			productTechnicals.add(productTechnical);
		}

		if (!StringUtils.isEmpty(data[ProductCSVColumnIndex.PRODUCT_TECH_LABEL_6_COLUMN_INDEX])
				&& !StringUtils.isEmpty(data[ProductCSVColumnIndex.PRODUCT_TECH_VALUE_6_COLUMN_INDEX])) {
			Pair<String, String> productTechnical = Pair.of(
					data[ProductCSVColumnIndex.PRODUCT_TECH_LABEL_6_COLUMN_INDEX],
					data[ProductCSVColumnIndex.PRODUCT_TECH_VALUE_6_COLUMN_INDEX]);
			productTechnicals.add(productTechnical);
		}

		productRawData.setProductTechnicals(productTechnicals);

		// Process product feature raw data
		List<Pair<String, String>> productFeatures = new ArrayList<>();

		if (!StringUtils.isEmpty(data[ProductCSVColumnIndex.PRODUCT_FEATURE_LABEL_1_COLUMN_INDEX])
				&& !StringUtils.isEmpty(data[ProductCSVColumnIndex.PRODUCT_FEATURE_VALUE_1_COLUMN_INDEX])) {
			Pair<String, String> productFeature = Pair.of(
					data[ProductCSVColumnIndex.PRODUCT_FEATURE_LABEL_1_COLUMN_INDEX],
					data[ProductCSVColumnIndex.PRODUCT_FEATURE_VALUE_1_COLUMN_INDEX]);
			productFeatures.add(productFeature);
		}

		if (!StringUtils.isEmpty(data[ProductCSVColumnIndex.PRODUCT_FEATURE_LABEL_2_COLUMN_INDEX])
				&& !StringUtils.isEmpty(data[ProductCSVColumnIndex.PRODUCT_FEATURE_VALUE_2_COLUMN_INDEX])) {
			Pair<String, String> productFeature = Pair.of(
					data[ProductCSVColumnIndex.PRODUCT_FEATURE_LABEL_2_COLUMN_INDEX],
					data[ProductCSVColumnIndex.PRODUCT_FEATURE_VALUE_2_COLUMN_INDEX]);
			productFeatures.add(productFeature);
		}

		if (!StringUtils.isEmpty(data[ProductCSVColumnIndex.PRODUCT_FEATURE_LABEL_3_COLUMN_INDEX])
				&& !StringUtils.isEmpty(data[ProductCSVColumnIndex.PRODUCT_FEATURE_VALUE_3_COLUMN_INDEX])) {
			Pair<String, String> productFeature = Pair.of(
					data[ProductCSVColumnIndex.PRODUCT_FEATURE_LABEL_3_COLUMN_INDEX],
					data[ProductCSVColumnIndex.PRODUCT_FEATURE_VALUE_3_COLUMN_INDEX]);
			productFeatures.add(productFeature);
		}

		if (!StringUtils.isEmpty(data[ProductCSVColumnIndex.PRODUCT_FEATURE_LABEL_4_COLUMN_INDEX])
				&& !StringUtils.isEmpty(data[ProductCSVColumnIndex.PRODUCT_FEATURE_VALUE_4_COLUMN_INDEX])) {
			Pair<String, String> productFeature = Pair.of(
					data[ProductCSVColumnIndex.PRODUCT_FEATURE_LABEL_4_COLUMN_INDEX],
					data[ProductCSVColumnIndex.PRODUCT_FEATURE_VALUE_4_COLUMN_INDEX]);
			productFeatures.add(productFeature);
		}

		if (!StringUtils.isEmpty(data[ProductCSVColumnIndex.PRODUCT_FEATURE_LABEL_5_COLUMN_INDEX])
				&& !StringUtils.isEmpty(data[ProductCSVColumnIndex.PRODUCT_FEATURE_VALUE_5_COLUMN_INDEX])) {
			Pair<String, String> productFeature = Pair.of(
					data[ProductCSVColumnIndex.PRODUCT_FEATURE_LABEL_5_COLUMN_INDEX],
					data[ProductCSVColumnIndex.PRODUCT_FEATURE_VALUE_5_COLUMN_INDEX]);
			productFeatures.add(productFeature);
		}

		if (!StringUtils.isEmpty(data[ProductCSVColumnIndex.PRODUCT_FEATURE_LABEL_6_COLUMN_INDEX])
				&& !StringUtils.isEmpty(data[ProductCSVColumnIndex.PRODUCT_FEATURE_VALUE_6_COLUMN_INDEX])) {
			Pair<String, String> productFeature = Pair.of(
					data[ProductCSVColumnIndex.PRODUCT_FEATURE_LABEL_6_COLUMN_INDEX],
					data[ProductCSVColumnIndex.PRODUCT_FEATURE_VALUE_6_COLUMN_INDEX]);
			productFeatures.add(productFeature);
		}

		productRawData.setProductFeatures(productFeatures);

		//Product Batch Upload add new column which is Product_Procurement
		if (!StringUtils.isEmpty(data[ProductCSVColumnIndex.PRODUCT_CAN_PROCURE_COLUMN_INDEX])) {
			Boolean canProcure = data[ProductCSVColumnIndex.PRODUCT_CAN_PROCURE_COLUMN_INDEX].equalsIgnoreCase(ProcurementEnum.TRUE.getCanProcure())
					? ProcurementEnum.TRUE.getConvertValueOfCanProcureToBoolean() : ProcurementEnum.FALSE.getConvertValueOfCanProcureToBoolean();

			productRawData.setCanProcure(canProcure);
		}

		/*Product Batch Upload add new column which is Product_Device_Category
		 * 1.check condition if empty for device category in batch upload, it will do nothing.
		 * 2. check condition if  device category is not 1, 2, 3, then do nothing
		 * 3. check condition if not numeric then it will return null.
		 * 4. set the device category in product.
		 */
		if (!StringUtils.isEmpty(data[ProductCSVColumnIndex.PRODUCT_DEVICE_CATEGORY_COLUMN_INDEX])) {
			if (data[ProductCSVColumnIndex.PRODUCT_DEVICE_CATEGORY_COLUMN_INDEX].equals(DeviceCategoryEnum.ONE.getName()) ||
					data[ProductCSVColumnIndex.PRODUCT_DEVICE_CATEGORY_COLUMN_INDEX].equals(DeviceCategoryEnum.TWO.getName()) ||
					data[ProductCSVColumnIndex.PRODUCT_DEVICE_CATEGORY_COLUMN_INDEX].equals((DeviceCategoryEnum.THREE.getName()))) {
				if (!checkNumeric(data[ProductCSVColumnIndex.PRODUCT_DEVICE_CATEGORY_COLUMN_INDEX])) return null;
				productRawData.setDeviceCategory(Long.parseLong(data[ProductCSVColumnIndex.PRODUCT_DEVICE_CATEGORY_COLUMN_INDEX]));
			}
		}

		return productRawData;
	}

	private Boolean validateProductCSVData(ProductRawData productRawData) {
		if (productRawData == null) return false;
		if (!checkEmptyOrNullString(productRawData)) return false;
		if (!checkLength(productRawData)) return false;
		if (!checkNumericWithDecimal(productRawData.getPrice().toString())) return false;
		return true;
	}

	private Boolean checkEmptyOrNullString(ProductRawData productRawData) {

		if (StringUtils.isEmpty(productRawData.getProductNamePrimary())) {
			return false;
		}

		if (StringUtils.isEmpty(productRawData.getPackaging())) {
			return false;
		}

		if (StringUtils.isEmpty(productRawData.getProductCode())) {
			return false;
		}

		if (productRawData.getPrice() == null) {
			return false;
		}

		if (StringUtils.isEmpty(productRawData.getUnit())) {
			return false;
		}

		if (CollectionUtils.isEmpty(productRawData.getImagesUrl())) {
			return false;
		}
		return true;
	}

	public Boolean checkLength(ProductRawData productRawData) {

		if (!StringUtils.isEmpty(productRawData.getBrand())) {
			if (productRawData.getBrand().length() > ProductFieldsLength.PRODUCT_BRAND_NAME_LENGTH) {
				return false;
			}
		}

		if (!StringUtils.isEmpty(productRawData.getCareArea())) {
			if (productRawData.getCareArea().length() > ProductFieldsLength.PRODUCT_CARE_AREA_NAME_LNEGTH) {
				return false;
			}
		}

		if (!StringUtils.isEmpty(productRawData.getCategory())) {
			if (productRawData.getCategory().length() > ProductFieldsLength.PRODUCT_CATEGORY_NAME_LENGTH) {
				return false;
			}
		}

		if (productRawData.getProductNamePrimary().length() > ProductFieldsLength.PRODUCT_NAME_PRIMARY_LENGTH) {
			return false;
		}

		if (!StringUtils.isEmpty(productRawData.getProductNameSecondary())) {
			if (productRawData.getProductNameSecondary()
					.length() > ProductFieldsLength.PRODUCT_NAME_SECONDARY_LENGTH) {
				return false;
			}
		}

		if (productRawData.getProductCode().length() > ProductFieldsLength.PRODUCT_CODE_LENGTH) {
			return false;
		}

		if (productRawData.getProductModel().length() > ProductFieldsLength.PRODUCT_MODEL_LENGTH) {
			return false;
		}

		if (productRawData.getProductRegNo().length() > ProductFieldsLength.PRODUCT_REG_NO_LENGTH) {
			return false;
		}

		if (productRawData.getProductDes().length() > ProductFieldsLength.PRODUCT_DESCRIPTION_LENGTH) {
			return false;
		}

		if (productRawData.getHashTag().length() > ProductFieldsLength.PRODUCT_HASHTAG_LENGTH) {
			return false;
		}

		if (productRawData.getUnit().length() > ProductFieldsLength.PRODUCT_UNIT_LENGTH) {
			return false;
		}

		String[] splitter = productRawData.getPrice().toString().split("\\.");
		if (splitter[0].length() > ProductFieldsLength.PRODUCT_PRICE_PRECISION_LENGTH) {
			return false;
		}

		if (productRawData.getPackaging().length() > ProductFieldsLength.PRODUCT_PACKAGING_LENGTH) {
			return false;
		}

		if (!StringUtils.isEmpty(productRawData.getBrochureAttachmentUrl())) {
			if (productRawData.getBrochureAttachmentUrl()
					.length() > ProductFieldsLength.PRODUCT_BROCHURE_ATTACHMENT_URL_LENGTH) {
				return false;
			}
		}

		if (!StringUtils.isEmpty(productRawData.getVideoAttachmentUrl())) {
			if (productRawData.getVideoAttachmentUrl()
					.length() > ProductFieldsLength.PRODUCT_VIDEO_ATTACHMENT_URL_LENGTH) {
				return false;
			}
		}

		for (String imageUrl : productRawData.getImagesUrl()) {
			if (imageUrl.length() > ProductFieldsLength.PRODUCT_IMAGE_URL_LENGTH) {
				return false;
			}
		}

		if (!CollectionUtils.isEmpty(productRawData.getProductTechnicals())) {
			for (Pair<String, String> productTechnical : productRawData.getProductTechnicals()) {
				if (productTechnical.getFirst().length() > ProductFieldsLength.PRODUCT_TECH_LABEL_LENGTH) {
					return false;
				}

				if (productTechnical.getSecond().length() > ProductFieldsLength.PRODUCT_TECH_VALUE_LENGTH) {
					return false;
				}
			}
		}

		if (!CollectionUtils.isEmpty(productRawData.getProductFeatures())) {
			for (Pair<String, String> productFeature : productRawData.getProductFeatures()) {
				if (productFeature.getFirst().length() > ProductFieldsLength.PRODUCT_FEATURE_LABEL_LENGTH) {
					return false;
				}

				if (productFeature.getSecond().length() > ProductFieldsLength.PRODUCT_FEATURE_VALUE_LENGTH) {
					return false;
				}
			}
		}
		return true;
	}

	private Boolean checkNumeric(String fieldValue) {
		if (!fieldValue.toString().matches("[0-9]+")) {
			return false;
		}
		return true;
	}

	private Boolean checkNumericWithDecimal(String fieldValue) {
		if (!fieldValue.toString().matches("[0-9]+\\.[0-9]+")) {
			return false;
		}

		return true;
	}

	private void updateProductCSVProcessStatus(Long vendorId, List<Integer> errorCSVRowNum) {
		ProductCSVProcessStatus productCSVProcessStatus = PRODUCT_CSV_PROCESS_STATUS.computeIfAbsent(vendorId, k -> new ProductCSVProcessStatus());
		productCSVProcessStatus.setErrorCSVRowNum(errorCSVRowNum);
	}


	private Pair<String, String> sendProductCSVProcessReport(String username, String vendorName, String userEmail, String sendFrom, String originalFileName, Integer totalCreatedProduct, String errorRowInfo, IdsmedUser uploaderUser) throws IdsmedBaseException {
		List<String> recipients = new ArrayList<>();
		Boolean isSendingEmail = emailService.checkSendEmail(uploaderUser.getIdsmedAccounts(), EmailTypeEnum.PRODUCT_BATCH_UPLOAD_RESULT_NOTIFICATION);
		if (isSendingEmail) {
			recipients.add(userEmail);
		}

		if (!CollectionUtils.isEmpty(recipients)) {
			Pair<String, String> emailPair = emailService.generatedProductBatchUploadReportEmail(username, vendorName, originalFileName, totalCreatedProduct, errorRowInfo);

			EmailObject emailObject = new ProductCSVProcessReportEmailObject(recipients, sendFrom, emailPair);
			try {
				rabbitTemplate.convertAndSend(env.getProperty(IdsmedQueueConfig.RABBIT_MAIL_SERVICE_PROCESS_QUEUE), emailObject);
				return emailPair;
			} catch (AmqpException e) {
				logger.error("Send email object to queue error", e.getMessage());
				return null;
			}
		}
		return null;
	}
	private void loggerForAllMethodInThisService(String methodDescription, String methodName, String loginId){
		logger.info(methodDescription, methodName, loginId != null?loginId:null);
	}
}

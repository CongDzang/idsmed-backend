package com.cmg.idsmed.service.product;

import java.io.File;
import java.io.IOException;
import java.util.List;

import com.cmg.idsmed.dto.product.ProductCSVUploadedQueueMessage;
import com.cmg.idsmed.model.entity.product.Product;
import com.cmg.idsmed.model.entity.product.ProductMedia;
import org.springframework.web.multipart.MultipartFile;

import com.cmg.idsmed.common.exception.IdsmedBaseException;
import com.cmg.idsmed.dto.product.ProductExcelUploadRequest;
import com.cmg.idsmed.dto.product.ProductExcelUploadedQueueMessage;

public interface ProductExcelService {
	boolean uploadProductExcelIntoOss(ProductExcelUploadRequest request, MultipartFile file)throws IdsmedBaseException;
	void processProductExcel(ProductExcelUploadedQueueMessage message) throws IdsmedBaseException, IOException, Exception;
	List<ProductMedia> processProductImages(Product product) throws Exception;
	List<ProductMedia> pullMediaFromCustomerSourceAndPushToOss(List<ProductMedia> medias, Product product) throws IOException, IdsmedBaseException;
	Boolean validateProductOriginalImage(File file);
}

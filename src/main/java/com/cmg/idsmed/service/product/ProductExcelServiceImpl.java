package com.cmg.idsmed.service.product;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URL;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import com.cmg.idsmed.common.Constant.*;
import com.cmg.idsmed.common.enums.*;
import com.cmg.idsmed.common.utils.LangHelper;
import com.cmg.idsmed.common.utils.OssUtils;
import com.cmg.idsmed.config.rabbitmq.IdsmedQueueConfig;
import com.cmg.idsmed.mail.EmailObject;
import com.cmg.idsmed.mail.EmailService;
import com.cmg.idsmed.mail.ProductCSVProcessReportEmailObject;
import com.cmg.idsmed.model.entity.auth.IdsmedAccount;
import com.cmg.idsmed.model.entity.auth.IdsmedUser;
import com.cmg.idsmed.model.entity.product.*;
import com.cmg.idsmed.model.entity.vendor.Vendor;
import com.cmg.idsmed.model.repo.auth.IdsmedAccountRepository;
import com.cmg.idsmed.model.repo.product.*;
import com.cmg.idsmed.model.repo.user.IdsmedUserRepository;
import com.cmg.idsmed.model.repo.vendor.VendorRepository;
import com.cmg.idsmed.service.share.*;
import io.swagger.models.auth.In;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.AmqpException;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.cmg.idsmed.common.exception.ErrorInfo;
import com.cmg.idsmed.common.exception.IdsmedBaseException;
import com.cmg.idsmed.dto.product.ProductCSVProcessStatus;
import com.cmg.idsmed.dto.product.ProductExcelUploadRequest;
import com.cmg.idsmed.dto.product.ProductExcelUploadedQueueMessage;
import com.cmg.idsmed.model.entity.product.log.ProductCSVLog;
import com.cmg.idsmed.model.repo.product.log.ProductCSVLogRepository;

import javax.imageio.ImageIO;

@Configuration
@Service
public class ProductExcelServiceImpl implements ProductExcelService {
	@Autowired
	private FileUploadService fileUploadService;

	@Autowired
	private FileDownloadService fileDownloadService;

	@Autowired
	private MessageResourceService messageResourceService;

	@Autowired
	private ProductRepository productRepository;

	@Autowired
	private ProductMediaRepository mediaRepository;

	@Autowired
	private ProductCareAreaRepository careAreaRepository;

	@Autowired
	private ProductCareAreaDetailRepository careAreaDetailRepository;

	@Autowired
	private ProductCategoryRepository categoryRepository;

	@Autowired
	private ProductBrandRepository brandRepository;

	@Autowired
	private IdsmedUserRepository idsmedUserRepository;

	@Autowired
	private NotificationService notificationService;

	@Autowired
	private ProductCSVLogRepository csvLogRepository;

	@Autowired
	private OssUtils ossUtils;

	@Autowired
	private ImageProcessService imageProcessService;

	@Autowired
	private VendorRepository vendorRepository;

	@Autowired
	private IdsmedAccountRepository idsmedAccountRepository;

	@Autowired
	private EmailService emailService;

	@Autowired
	private ProductCareAreaRepository productCareAreaRepository;

	@Autowired
	private ProductCategoryRepository productCategoryRepository;

	@Autowired
	private ProductBrandRepository productBrandRepository;

	@Autowired
	private ProductTechnicalRepository technicalRepository;

	@Autowired
	private ProductFeatureRepository featureRepository;

	@Autowired
	private ProductRatingRepository productRatingRepository;

	@Autowired
	private ProductWishlistRepository productWishlistRepository;

	@Autowired
	private ProductHierarchyRepository productHierarchyRepository;

	private static final Logger logger = LoggerFactory.getLogger(ProductExcelServiceImpl.class);

	public static final Integer FIRST_SHEET_NUM = 0;

	private final static Integer PRODUCT_PRICE_DECIMAL_POINT = 2;

	private static final Map<Long, ProductCSVProcessStatus> PRODUCT_EXCEL_PROCESS_STATUS = new ConcurrentHashMap<>();

	@Autowired
	private Environment env;

	@Autowired
	private RabbitTemplate rabbitTemplate;

	@Value("${logger.start.method.description}")
	String START_METHOD_DESCRIPTION;

	@Value("${logger.end.method.description}")
	String END_METHOD_DESCRIPTION;
	private static final String UPLOAD_PRODUCT_EXCEL_INTO_OSS_METHOD_NAME = "uploadProductExcelIntoOss";

	@Override
	public boolean uploadProductExcelIntoOss(ProductExcelUploadRequest request, MultipartFile file)
			throws IdsmedBaseException {
		// TODO Auto-generated method stub
		loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, UPLOAD_PRODUCT_EXCEL_INTO_OSS_METHOD_NAME, request.getUploadBy());
		String fileExtension = FilenameUtils.getExtension(file.getOriginalFilename());
		if(!fileExtension.equals("xls") && !fileExtension.equals("xlsx")){
			throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.PRODUCT_EXCEL_FORMAT_ERROR,
					LangHelper.getLangCode(request.getLangCode())));
		}
		fileUploadService.uploadProductExcel(request, file);
		loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, UPLOAD_PRODUCT_EXCEL_INTO_OSS_METHOD_NAME, request.getUploadBy());
		return true;
	}

	@Override
	@Transactional
	public void processProductExcel(ProductExcelUploadedQueueMessage message) throws Exception {
		logger.info("Start process excel file: {}", message.getProductExcelOssUrl());
		if (message.getProductExcelOssUrl() == null) {
			throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.DOWNLOAD_FILE_FROM_OSS_ERROR,
					LangEnum.ENGLISH.getCode()));
		}

		// Clear previous product csv process status then create new one for this
		// process.
		PRODUCT_EXCEL_PROCESS_STATUS.remove(message.getVendorId());
		PRODUCT_EXCEL_PROCESS_STATUS.computeIfAbsent(message.getVendorId(), k -> new ProductCSVProcessStatus());

		File file = fileDownloadService.downloadProductCSVOrExcelFileFromOSS(message.getProductExcelOssUrl());
		List<Product> products = new ArrayList<>();
		String fileExtension = FilenameUtils.getExtension(file.getPath());
		if (!file.exists()) {
			throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.DOWNLOAD_FILE_FROM_OSS_ERROR,
					LangEnum.ENGLISH.getCode()));
		}

		try {
			products = processExcelFile(file, message.getVendorId(), fileExtension, message.getUploadBy());
		}catch(Exception e){
			logger.error("Error while process excel file", e.getMessage());
			throw e;
		}

		//Save log here but send report email at the end of this method
		List<Product> finalProducts = products;

		//TODO: Variable for support email. Need to refactor later on
		String vendorEmail="";
		String companyCode="";
		String originalFileName = "";
		Integer totalCreatedProduct = 0;
		String errorRowInfo = "";

		Optional<ProductCSVLog> logOp = csvLogRepository.findById(message.getProductExcelLogId());
		if (logOp.isPresent()) {
			ProductCSVLog log = logOp.get();
			log.setStatus(ProductCSVStatusEnum.PROCESS_DONE.getCode());
			ProductCSVProcessStatus excelProcessStatus = PRODUCT_EXCEL_PROCESS_STATUS.get(message.getVendorId());
			if (excelProcessStatus != null) {
				if (excelProcessStatus.getCSVErrorRowsInfo() != null)
					log.setErrorRowInfo(excelProcessStatus.getCSVErrorRowsInfo());
			}

			if (!CollectionUtils.isEmpty(finalProducts)) {
				List<Long> createdProductIds = finalProducts.stream().map(p -> p.getId()).collect(Collectors.toList());
				log.setProductCreatedInfoByList(createdProductIds);
				log.setTotalProductCreated(finalProducts.size());
			}
			originalFileName = log.getOriginalFileName();
			totalCreatedProduct = log.getTotalProductCreated();
			errorRowInfo = log.getErrorRowInfo();
			vendorEmail = log.getVendorEmail();
			companyCode = log.getCompanyCode();
			csvLogRepository.save(log);
		}

		file.delete();

		for (Product product : products) {
			// Download brochure from customer url and upload to OSS
			productBrochureProcess(product);

			// Download video from customer url and upload to OSS
			productVideoProcess(product);

			// Download images from customer url and upload to OSS
			productOriginalImagesProcess(product);
		}

		// Implement Resize images
		for (Product product : products) {
			processProductImages(product);
		}
		//Send report email to queue
		String sendFrom = env.getProperty(PropertiesLabelConst.SMTP_SUPPORT_MAIL_LABEL);
		String vendorName = "";
		Optional<Vendor> vendor = vendorRepository.findById(message.getVendorId());
		if (vendor.isPresent()) {
			vendorName = vendor.get().getCompanyNamePrimary();
		}

		if (!CollectionUtils.isEmpty(products)) {
			products.forEach(p -> {
				p.setStatus(StatusEnum.DRAFT.getCode());
			});
		}

		String loginId = message.getUploadBy();
		IdsmedAccount uploaderAccount = idsmedAccountRepository.findByLoginId(loginId);
		IdsmedUser uploaderUser = uploaderAccount.getIdsmedUser();
		String userEmail = uploaderUser.getEmail();
		String userName = uploaderUser.getFirstName() + " " + uploaderUser.getLastName();

		productRepository.saveAll(products);

		//Send email
		logger.info("Start send product batch upload notification email");
		errorRowInfo = StringUtils.isEmpty(errorRowInfo) ? "" : errorRowInfo;
		Pair<String, String> emailPair = sendProductExcelProcessReport(userName, vendorName, userEmail, sendFrom, originalFileName, totalCreatedProduct, errorRowInfo, uploaderAccount);
		logger.info("end send product batch upload notification email");

		//Send web notification
		logger.info("start send product batch upload notification");

		List<IdsmedAccount> accountList = new ArrayList<>();
		List<IdsmedUser> vendorUsers = idsmedUserRepository.findAllByCompanyCode(vendor.get().getCompanyCode());

		for (IdsmedUser idsmedUser : vendorUsers) {
			List<IdsmedAccount> accounts = idsmedUser.getIdsmedAccounts().isEmpty() ? null : idsmedUser.getIdsmedAccounts();
			accountList.addAll(accounts);
		}

		//TODO : temporary for avoid SecurityContextHelper.getCurrentAccount() can not execute
		IdsmedAccount senderAccount = idsmedAccountRepository.findByLoginId(message.getUploadBy());
		if (senderAccount != null) {
			notificationService.sendProductApprovedNotification(senderAccount, accountList, emailPair);
		}

		logger.info("end send product batch upload notification");

		logger.info(env.getProperty(LoggerConstant.LOGGER_END_METHOD_DESCRIPTION) + "with a report email sent to queue", message.getProductExcelOssUrl());
	}

	@Override
	public List<ProductMedia> processProductImages(Product product) throws IOException {
		logger.info("Start process resize image of product with id {}", product.getId());
		List<ProductMedia> originalImages = mediaRepository.findAllByProductAndDocumentTypeAndVersion(product,
				ProductMedia.MEDIA_TYPE.IMAGE.getType(), ImageVersionEnum.VERSION1.getVersion());
		List<ProductMedia> resizedImages = new ArrayList<>();
		if (CollectionUtils.isEmpty(originalImages)) {
			logger.info("Product with id {} does't has image", product.getId());
			return null;
		}
		for (ProductMedia originalImg : originalImages) {
			if (StringUtils.isEmpty(originalImg.getDocumentUrl())) {
				continue;
			}
			List<Pair<File, Integer>> resizedFiles = new ArrayList<>();
			for (ImageVersionEnum imgEnum : ImageVersionEnum.values()) {
				if (imgEnum.equals(ImageVersionEnum.VERSION1))
					continue;
				Pair<File, Integer> resignPair = imageProcessService.resizeProductImage(originalImg.getDocumentUrl(),
						imgEnum);
				if (resignPair != null)
					resizedFiles.add(resignPair);
			}

			//TODO update resized images false to PRODUCT_CSV_PROCESS_STATUS;
//			Pair<Integer, List<String>> errorResizeImage = Pair.of()

			if (!CollectionUtils.isEmpty(resizedFiles)) {
				for (Pair<File, Integer> resignFilePair : resizedFiles) {
					String uploadFilePath = env.getProperty(OssUtils.IDSMED_PRODUCT_OSS_UPLOAD_PATH);
					String originalFileName = originalImg.getCustomerUrl().substring(
							originalImg.getCustomerUrl().lastIndexOf("/") + 1,
							originalImg.getCustomerUrl().length());
					String fileName = resignFilePair.getFirst().getName();
					String documentUrl = ossUtils.uploadFile(uploadFilePath, fileName,
							resignFilePair.getFirst().getPath());
					if (!StringUtils.isEmpty(documentUrl)) {
						ProductMedia resizedImg = new ProductMedia();
						resizedImg.setProduct(product);
						resizedImg.setDocumentUrl(documentUrl);
						resizedImg.setDocumentSeq(originalImg.getDocumentSeq());
						resizedImg.setDocumentName(originalFileName);
						resizedImg.setVersion(resignFilePair.getSecond());
						resizedImg.setDocumentType(ProductMedia.MEDIA_TYPE.IMAGE.getType());
						resizedImg.setCreatedBy(product.getCreatedBy());
						resizedImg.setUpdatedBy(product.getCreatedBy());
						URL url = new URL(documentUrl);
						resizedImg.setGid(originalImg.getGid());
						//Set this ProductMedia to approved. Need to implement this logic in approved product later
						resizedImg.setStatus(StatusEnum.APPROVED.getCode());
						resizedImages.add(resizedImg);
					}
					resignFilePair.getFirst().delete();
				}
			}

		}
		resizedImages = mediaRepository.saveAll(resizedImages);
		logger.info("Start process resize image of product with id {}", product.getId());
		return resizedImages;
	}

	// this method is to get last row with data for the excel file.
	public int getLastRowWithData(Sheet firstSheet) {
		int rowCount = 0;
		Iterator<Row> iter = firstSheet.rowIterator();

		while (iter.hasNext()) {
			Row r = iter.next();
			if (!this.isRowBlank(r)) {
				rowCount = r.getRowNum();
			}
		}

		return rowCount;
	}

	public boolean isRowBlank(Row r) {
		boolean ret = true;

		/*
		 * If a row is null, it must be blank.
		 */
		if (r != null) {
			Iterator<Cell> cellIter = r.cellIterator();
			/*
			 * Iterate through all cells in a row.
			 */
			while (cellIter.hasNext()) {
				/*
				 * If one of the cells in given row contains data, the row is
				 * considered not blank.
				 */
				if (!this.isCellBlank(cellIter.next())) {
					ret = false;
					break;
				}
			}
		}

		return ret;
	}

	// if the type of cell is blank, then it is consider not data also
	public boolean isCellBlank(Cell c) {
		return (c == null || c.getCellType() == CellType.BLANK);
	}

	public List<Product> processExcelFile(File file, Long vendorId, String fileExtension, String uploadBy)
			throws IOException, IdsmedBaseException, Exception {
		logger.info("Start process Product Excel File");
		List<Product> products = new ArrayList<>();
		List<ProductExcelRawData> productExcelRawDatas = new ArrayList<>();

		// Iterate excel file and fetch raw data
		Workbook workbook = null;

		try {
			FileInputStream fileInputStream = new FileInputStream(file);

			if (fileExtension.equals(FileExtensionEnum.XLSX.getName())) {
				workbook = new XSSFWorkbook(fileInputStream);
			}

			if (fileExtension.equals(FileExtensionEnum.XLS.getName())) {
				workbook = new HSSFWorkbook(fileInputStream);
			}

			Sheet firstSheet = workbook.getSheetAt(FIRST_SHEET_NUM);
			//get the number of row with data
			Integer numberOfRowNumWithData = getLastRowWithData(firstSheet);
			// get last row number from first row to get plus the number of row get from method above and minus 1.
			Integer lastRowNumber = ProductExcelColumnIndex.FIRST_ROW_TO_GET + numberOfRowNumWithData - 1;

			// Reate data from excel rows and set to ProductExcelRawData instance
			for (Integer rowNum = ProductExcelColumnIndex.FIRST_ROW_TO_GET; rowNum < lastRowNumber; rowNum++) {
				// Process image url raw data
				List<String> imageURLs = new ArrayList<>();
				ProductExcelRawData productExcelRawData = new ProductExcelRawData();
				Row nextRow = firstSheet.getRow(rowNum);
				Iterator<Cell> cellIterator = nextRow.cellIterator();
				productExcelRawData.setExcelRowNum(rowNum);
				while (cellIterator.hasNext()) {
					Cell nextCell = cellIterator.next();
					int columnIndex = nextCell.getColumnIndex();
					try {
						productExcelRawData = getProductExcelRawData(nextCell, columnIndex, productExcelRawData, imageURLs, rowNum);
					} catch (Exception e) {
						logger.error("Error when get raw data from excel file:", e.getMessage());
						throw e;
					}
				}
                if (productExcelRawData == null) continue;
                productExcelRawDatas.add(productExcelRawData);
			}

			//This list to record error Excel rows.
			List<Integer> productExcelErrorRows = new ArrayList<>();
			List<ProductExcelRawData> productExcelRawDataValidList = productExcelRawDatas
					.stream()
					.filter(pr -> {
						Boolean isValid = validateProductExcelData(pr);
						if (!isValid) productExcelErrorRows.add(pr.getExcelRowNum());
						return isValid;
					}).collect(Collectors.toList());

			//Update the Excel file process
			updateProductCSVProcessStatus(vendorId, productExcelErrorRows);

			Map<Product, List<ProductCareArea>> productCareAreaMap = new HashMap<>();
			// Create product set meta data and save it.
			if (!CollectionUtils.isEmpty(productExcelRawDataValidList)) {
				productExcelRawDataValidList.forEach(perd -> {
					Product product = new Product(perd);

					String currentLoginId = uploadBy;
					Long currentAccountId = idsmedAccountRepository.findByLoginId(currentLoginId).getId();
					if (currentAccountId != null) {
						product.setCreatedBy(currentAccountId);
						product.setUpdatedBy(currentAccountId);
					}
					productRepository.save(product);

					// Set product care area.
					ProductCareAreaDetail productCareAreaDetail = null;
					String careAreaName = perd.getCareArea();
					if (!StringUtils.isEmpty(careAreaName)) {
						ProductCareArea careArea = productCareAreaRepository.findFirstByCareAreaNameOrCareAreaNameZhCnOrCareAreaNameZhTwOrCareAreaNameViOrCareAreaNameThOrCareAreaNameId(careAreaName
								, careAreaName, careAreaName, careAreaName, careAreaName, careAreaName);
						if (careArea != null) {
							productCareAreaDetail = new ProductCareAreaDetail(careArea, product);
						} else {
							careArea = productCareAreaRepository
									.findFirstByCareAreaName(ProductCareArea.OTHER_PRODUCT_CARE_AREA_NAME);
							productCareAreaDetail = new ProductCareAreaDetail(careArea, product);
						}
					} else {
						ProductCareArea careArea = productCareAreaRepository
								.findFirstByCareAreaName(ProductCareArea.OTHER_PRODUCT_CARE_AREA_NAME);
						productCareAreaDetail = new ProductCareAreaDetail(careArea, product);
						if (currentAccountId != null) {
							productCareAreaDetail.setCreatedBy(currentAccountId);
							productCareAreaDetail.setUpdatedBy(currentAccountId);
						}
					}

					careAreaDetailRepository.save(productCareAreaDetail);

					// Set product category, If customer doesn't pass category name. set it to
					// "OTHER"
					product.setProductCategory(productCategoryRepository
							.findFirstByCategoryName(ProductCategory.OTHERS_PRODUCT_CATEGORY_NAME));
					String categoryName = perd.getCategory();
					if (!StringUtils.isEmpty(categoryName)) {
						ProductCategory category = productCategoryRepository.findFirstByCategoryNameOrCategoryNameZhCnOrCategoryNameZhTwOrCategoryNameViOrCategoryNameThOrCategoryNameId(categoryName
								, categoryName, categoryName, categoryName, categoryName, categoryName);
						if (category != null) {
							product.setProductCategory(category);
						}
					}

					//Set product brand.
					ProductBrand brand = null;
					if (StringUtils.isEmpty(perd.getBrand())) {
						brand = productBrandRepository.findFirstByBrandName(ProductBrand.OTHERS_PRODUCT_BRAND_NAME);
					} else {
						brand = productBrandRepository.findFirstByBrandName(perd.getBrand());
						if (brand == null) {
							brand = new ProductBrand();
							brand.setBrandName(perd.getBrand());
							brand.setBrandCode(perd.getBrand());
							brand.setStatus(StatusEnum.APPROVED.getCode());
							if (currentAccountId != null) {
								brand.setCreatedBy(currentAccountId);
								brand.setUpdatedBy(currentAccountId);
							}
							brand = productBrandRepository.save(brand);
						}
					}

					product.setProductBrand(brand);

					// Set Vendor
					Optional<Vendor> vendorOpt = vendorRepository.findById(vendorId);
					Vendor vendor = vendorOpt.get();
					if (vendor != null) {
						product.setVendor(vendor);
					}

					// Set ProductMediaList raw data which includes Brochure, Video, Images
					List<ProductMedia> mediaList = new ArrayList<>();
					// Set Brochure raw data.
					if (!StringUtils.isEmpty(perd.getBrochureAttachmentUrl())) {
						ProductMedia productMedia = new ProductMedia();
						productMedia.setDocumentType(ProductMedia.MEDIA_TYPE.BROCHURE.getType());
						productMedia.setCustomerUrl(perd.getBrochureAttachmentUrl());
						if (currentAccountId != null) {
							productMedia.setCreatedBy(currentAccountId);
							productMedia.setUpdatedBy(currentAccountId);
						}
						productMedia.setProduct(product);
						mediaList.add(productMedia);
					}

					// Set Video raw data
					if (!StringUtils.isEmpty(perd.getVideoAttachmentUrl())) {
						ProductMedia productMedia = new ProductMedia();
						productMedia.setDocumentType(ProductMedia.MEDIA_TYPE.VIDEO.getType());
						productMedia.setCustomerUrl(perd.getVideoAttachmentUrl());
						if (currentAccountId != null) {
							productMedia.setCreatedBy(currentAccountId);
							productMedia.setUpdatedBy(currentAccountId);
						}
						productMedia.setProduct(product);
						mediaList.add(productMedia);
					}

					// Set Product images
					if (!CollectionUtils.isEmpty(perd.getImagesUrl())) {
						Integer index = 1;
						for (String im : perd.getImagesUrl()) {
							ProductMedia productMedia = new ProductMedia();
							productMedia.setDocumentType(ProductMedia.MEDIA_TYPE.IMAGE.getType());
							productMedia.setCustomerUrl(im);
							productMedia.setDocumentSeq(index);
							if (currentAccountId != null) {
								productMedia.setCreatedBy(currentAccountId);
								productMedia.setUpdatedBy(currentAccountId);
							}
							index++;
							productMedia.setProduct(product);
							mediaList.add(productMedia);
						}
					}

					if (!CollectionUtils.isEmpty(mediaList)) {
						mediaList = mediaRepository.saveAll(mediaList);
						product.setProductMediaList(mediaList);
					}

					// Set Product technical features raw data.
					List<ProductTechnical> productTechnicals = new ArrayList<>();
					if (!CollectionUtils.isEmpty(perd.getProductTechnicals())) {
						Integer index = 1;
						for (Pair<String, String> p : perd.getProductTechnicals()) {
							ProductTechnical productTechnical = new ProductTechnical();
							productTechnical.setProductLabel(p.getFirst());
							productTechnical.setProductValue(p.getSecond());
							productTechnical.setProductLabelSequence(index);
							if (currentAccountId != null) {
								productTechnical.setCreatedBy(currentAccountId);
								productTechnical.setUpdatedBy(currentAccountId);
							}
							index++;
							productTechnical.setProduct(product);
							productTechnicals.add(productTechnical);
						}
					}

					if (!CollectionUtils.isEmpty(productTechnicals)) {
						productTechnicals = technicalRepository.saveAll(productTechnicals);
						product.setProductTechnicalList(productTechnicals);
					}

					// Set product features raw data
					List<ProductFeatures> productFeaturesList = new ArrayList<>();
					if (!CollectionUtils.isEmpty(perd.getProductFeatures())) {
						Integer index = 1;
						for (Pair<String, String> p : perd.getProductFeatures()) {
							ProductFeatures feature = new ProductFeatures();
							feature.setProductLabel(p.getFirst());
							feature.setProductValue(p.getSecond());
							feature.setProductLabelSequence(index);
							if (currentAccountId != null) {
								feature.setCreatedBy(currentAccountId);
								feature.setUpdatedBy(currentAccountId);
							}
							index++;
							feature.setProduct(product);
							productFeaturesList.add(feature);
						}
					}
					if (!CollectionUtils.isEmpty(productFeaturesList)) {
						productFeaturesList = featureRepository.saveAll(productFeaturesList);
						product.setProductFeaturesList(productFeaturesList);
					}

					//initialize product rating when create new product, new product need to add into product rating
					ProductRating productRating = new ProductRating(product, currentAccountId);
					productRatingRepository.save(productRating);

					//Initialized product wishlist when create new product, new product need to add into product wishlist
					ProductWishlist productWishlist = new ProductWishlist(product, 0L, currentAccountId);
					productWishlistRepository.save(productWishlist);
					

					ProductHierarchyLevelFour productHierarchyLevelFour = new ProductHierarchyLevelFour();

					if(perd.getProductHierarchyCode() != null){
						productHierarchyLevelFour = productHierarchyRepository.findByCode(perd.getProductHierarchyCode());
					}

					product.setProductHierarchyLevelFour(productHierarchyLevelFour);
					product.setStatus(StatusEnum.PRODUCT_IMAGES_PROCESSING.getCode());
					productRepository.save(product);
                    products.add(product);
				});
			}
			workbook.close();
			fileInputStream.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			throw new IdsmedBaseException(
					messageResourceService.getErrorInfo(ErrorInfo.FILE_NOT_FOUND_ERROR, LangEnum.CHINA.getCode()));
		}

		return products;
	}

	@Transactional
	public Product productBrochureProcess(Product product) throws IOException, IdsmedBaseException {
		logger.info("Start process brochure of product with id is {}", product.getId());
		List<ProductMedia> bochures = mediaRepository.findByProductAndDocumentType(product,
				ProductMedia.MEDIA_TYPE.BROCHURE.getType());
		pullMediaFromCustomerSourceAndPushToOss(bochures, product);
		return product;
	}

	@Transactional
	public Product productVideoProcess(Product product) throws IOException, IdsmedBaseException {
		logger.info("Start process video of product with id is {}", product.getId());
		List<ProductMedia> videos = mediaRepository.findByProductAndDocumentType(product,
				ProductMedia.MEDIA_TYPE.VIDEO.getType());

		pullMediaFromCustomerSourceAndPushToOss(videos, product);

		return product;
	}

	@Transactional
	public Product productOriginalImagesProcess(Product product) throws IOException, IdsmedBaseException {
		logger.info("Start process original images of product with id is {}", product.getId());
		List<ProductMedia> images = mediaRepository.findByProductAndDocumentType(product,
				ProductMedia.MEDIA_TYPE.IMAGE.getType());
		if (!CollectionUtils.isEmpty(images)) {
			for (ProductMedia pm : images) {
				if (!StringUtils.isEmpty(pm.getDocumentUrl()))
					continue;
				pm = processOriginalImage(pm, product);
                //If can't download the image from customer_url and upload to OSS we need to delete ProductMedia
                if (pm != null && StringUtils.isEmpty(pm.getDocumentUrl())) {
                    mediaRepository.delete(pm);
                }
			}
		}
		return product;
	}

	private ProductMedia processOriginalImage(ProductMedia image, Product product) throws IOException, IdsmedBaseException {
		List<ProductMedia> media = new ArrayList<>();
		if (image != null) {
			media.add(image);
		}
		if (!CollectionUtils.isEmpty(media)) {
			media = pullOriginalImageFromCustomerAndPushToOss(media, product);
		}

        if (!CollectionUtils.isEmpty(media)) {
            return media.get(0);
        }
        return null;
	}

	@Transactional
	@Modifying
	public List<ProductMedia> pullOriginalImageFromCustomerAndPushToOss(List<ProductMedia> medias, Product product) {
		if (!CollectionUtils.isEmpty(medias)) {
			ListIterator<ProductMedia> iterator = medias.listIterator();
			while (iterator.hasNext()) {
				ProductMedia media = iterator.next();
				File file = fileDownloadService.downloadFileFromUrl(media.getCustomerUrl());
				if (file == null /* || !validateProductOriginalImage(file)*/) {
					mediaRepository.delete(media);
					iterator.remove();
					continue;
				}

				String uploadFilePath = "idsmedmedia/product";
				String originalFileName = media.getCustomerUrl()
						.substring(media.getCustomerUrl().lastIndexOf("/") + 1, media.getCustomerUrl().length());
				String fileName = file.getName();

				String documentUrl = ossUtils.uploadFile(uploadFilePath, fileName, file.getPath());

				if (StringUtils.isEmpty(documentUrl)) {
					logger.info("Can't upload media file to OSS: {}", file.getName());
					mediaRepository.delete(media);
					iterator.remove();
					continue;
				}

				media.setDocumentUrl(documentUrl);
				media.setDocumentName(originalFileName);
				media.setGid(fileName);
				media.setProduct(product);
				//Set this ProductMedia to approved. Need to implement this logic in approved product later
				media.setStatus(StatusEnum.APPROVED.getCode());
				mediaRepository.save(media);
				file.delete();
			}
		}
		return medias;
	}

	public Boolean validateProductOriginalImage(File file) {
		//TODO implement logic check original image
		try {
			BufferedImage bufferedImage = ImageIO.read(file);
			if(bufferedImage == null){
				return false;
			}
			Integer width = bufferedImage.getWidth();
			Integer height = bufferedImage.getHeight();
			if (width < 800 || width > 1200 || height < 600 || height > 1200) {
				return false;
			}
		} catch (IOException ioe) {
			return false;
		}
		return true;
	}

	@Override
	@Transactional
	@Modifying
	public List<ProductMedia> pullMediaFromCustomerSourceAndPushToOss(List<ProductMedia> medias, Product product) {
		if (!CollectionUtils.isEmpty(medias)) {
			ListIterator<ProductMedia> iterator = medias.listIterator();
			while (iterator.hasNext()) {
				ProductMedia media = iterator.next();
				File file = fileDownloadService.downloadFileFromUrl(media.getCustomerUrl());

				if (file == null) {
					mediaRepository.delete(media);
					iterator.remove();
					continue;
				}

				String uploadFilePath = "idsmedmedia/product";
				String originalFileName = media.getCustomerUrl()
						.substring(media.getCustomerUrl().lastIndexOf("/") + 1, media.getCustomerUrl().length());
				String fileName = file.getName();

				String documentUrl = ossUtils.uploadFile(uploadFilePath, fileName, file.getPath());

				if (StringUtils.isEmpty(documentUrl)) {
					logger.info("Can't upload media file to OSS: {}", file.getName());
					mediaRepository.delete(media);
					iterator.remove();
					continue;
				}

                media.setProduct(product);
				media.setDocumentUrl(documentUrl);
				media.setDocumentName(originalFileName);
				media.setGid(fileName);
				//Set this ProductMedia to approved. Need to implement this logic in approved product later
				media.setStatus(StatusEnum.APPROVED.getCode());
				mediaRepository.save(media);
				file.delete();
			}
		}
		return medias;
	}

	private ProductExcelRawData getProductExcelRawData(Cell nextCell, int columnIndex, ProductExcelRawData productExcelRawData, List<String> imageURLs, Integer rowNumber) {
		if (columnIndex == ProductExcelColumnIndex.CARE_AREA_COLUMN_INDEX) {
			Integer cellType = checkCellType(nextCell);
			if(cellType == CellTypeEnum.NUMERIC_CELL_TYPE.getCode()){
				Double careAreaDoubleValue = nextCell.getNumericCellValue();
				if(careAreaDoubleValue != null){
					Integer careAreaIntegerValue = careAreaDoubleValue.intValue();
					productExcelRawData.setCareArea(careAreaIntegerValue.toString());
				}
			}

			if(cellType == CellTypeEnum.STRING_CELL_TYPE.getCode()) {
				if (!StringUtils.isEmpty(nextCell.getStringCellValue())) {
					productExcelRawData.setCareArea(nextCell.getStringCellValue());
				}
			}
		}

		if (columnIndex == ProductExcelColumnIndex.CATEGORY_COLUMN_INDEX) {
			Integer cellType = checkCellType(nextCell);
			if(cellType == CellTypeEnum.NUMERIC_CELL_TYPE.getCode()) {
				Double categoryDoubleValue = nextCell.getNumericCellValue();
				if(categoryDoubleValue != null){
					Integer categoryIntegerValue = categoryDoubleValue.intValue();
					productExcelRawData.setCategory(categoryIntegerValue.toString());
				}
			}

			if(cellType == CellTypeEnum.STRING_CELL_TYPE.getCode()) {
				if (!StringUtils.isEmpty(nextCell.getStringCellValue())) {
					productExcelRawData.setCategory(nextCell.getStringCellValue());
				}
			}

		}

		if (columnIndex == ProductExcelColumnIndex.BRAND_COLUMN_INDEX) {
			Integer cellType = checkCellType(nextCell);
			if(cellType == CellTypeEnum.NUMERIC_CELL_TYPE.getCode()) {
				Double brandDoubleValue = nextCell.getNumericCellValue();
				if(brandDoubleValue != null){
					Integer brandIntegerValue = brandDoubleValue.intValue();
					productExcelRawData.setBrand(brandIntegerValue.toString());
				}
			}

			if(cellType == CellTypeEnum.STRING_CELL_TYPE.getCode()) {
				if (!StringUtils.isEmpty(nextCell.getStringCellValue())) {
					productExcelRawData.setBrand(nextCell.getStringCellValue());
				}
			}

		}

		if (columnIndex == ProductExcelColumnIndex.PRODUCT_NAME_PRIMARY_COLUMN_INDEX) {
			Integer cellType = checkCellType(nextCell);
			if(cellType == CellTypeEnum.NUMERIC_CELL_TYPE.getCode()) {
				Double productNamePrimaryDoubleValue = nextCell.getNumericCellValue();
				if(productNamePrimaryDoubleValue != null){
					Integer productNamePrimaryIntegerValue = productNamePrimaryDoubleValue.intValue();
					productExcelRawData.setProductNamePrimary(productNamePrimaryIntegerValue.toString());
				}
			}

			if(cellType == CellTypeEnum.STRING_CELL_TYPE.getCode()) {
				if (!StringUtils.isEmpty(nextCell.getStringCellValue())) {
					productExcelRawData.setProductNamePrimary(nextCell.getStringCellValue());
				}
			}

		}
		if (columnIndex == ProductExcelColumnIndex.PRODUCT_NAME_SECONDARY_COLUMN_INDEX) {
			Integer cellType = checkCellType(nextCell);
			if(cellType == CellTypeEnum.NUMERIC_CELL_TYPE.getCode()) {
				Double productNameSecondaryDoubleValue = nextCell.getNumericCellValue();
				if(productNameSecondaryDoubleValue != null){
					Integer productNameSecondaryIntegerValue = productNameSecondaryDoubleValue.intValue();
					productExcelRawData.setProductNameSecondary(productNameSecondaryIntegerValue.toString());
				}
			}

			if(cellType == CellTypeEnum.STRING_CELL_TYPE.getCode()) {
				if (!StringUtils.isEmpty(nextCell.getStringCellValue())) {
					productExcelRawData.setProductNameSecondary(nextCell.getStringCellValue());
				}
			}
		}

		if (columnIndex == ProductExcelColumnIndex.PRODUCT_CODE_COLUMN_INDEX) {
			Integer cellType = checkCellType(nextCell);
			if(cellType == CellTypeEnum.NUMERIC_CELL_TYPE.getCode()) {
				Double productCodeDoubleValue = nextCell.getNumericCellValue();
				if(productCodeDoubleValue != null){
					Integer productCodeIntegerValue = productCodeDoubleValue.intValue();
					productExcelRawData.setProductCode(productCodeIntegerValue.toString());
				}
			}

			if(cellType == CellTypeEnum.STRING_CELL_TYPE.getCode()) {
				if (!StringUtils.isEmpty(nextCell.getStringCellValue())) {
					productExcelRawData.setProductCode(nextCell.getStringCellValue());
				}
			}
		}

		if (columnIndex == ProductExcelColumnIndex.PRODUCT_MODEL_COLUMN_INDEX) {
			Integer cellType = checkCellType(nextCell);
			if(cellType == CellTypeEnum.NUMERIC_CELL_TYPE.getCode()) {
				Double productModelDoubleValue = nextCell.getNumericCellValue();
				if(productModelDoubleValue != null){
					Integer productModelIntegerValue = productModelDoubleValue.intValue();
					productExcelRawData.setProductModel(productModelIntegerValue.toString());
				}
			}

			if(cellType == CellTypeEnum.STRING_CELL_TYPE.getCode()) {
				if (!StringUtils.isEmpty(nextCell.getStringCellValue())) {
					productExcelRawData.setProductModel(nextCell.getStringCellValue());
				}
			}
		}

		if (columnIndex == ProductExcelColumnIndex.PRODUCT_REG_NO_COLUMN_INDEX) {
			Integer cellType = checkCellType(nextCell);
			if(cellType == CellTypeEnum.NUMERIC_CELL_TYPE.getCode()) {
				Double productRegNoDoubleValue = nextCell.getNumericCellValue();
				if(productRegNoDoubleValue != null){
					Integer productRegNoIntegerValue = productRegNoDoubleValue.intValue();
					productExcelRawData.setProductRegNo(productRegNoIntegerValue.toString());
				}
			}

			if(cellType == CellTypeEnum.STRING_CELL_TYPE.getCode()) {
				if (!StringUtils.isEmpty(nextCell.getStringCellValue())) {
					productExcelRawData.setProductRegNo(nextCell.getStringCellValue());
				}
			}
		}

		if (columnIndex == ProductExcelColumnIndex.PRODUCT_DESCRIPTION_COLUMN_INDEX) {
			Integer cellType = checkCellType(nextCell);
			if(cellType == CellTypeEnum.NUMERIC_CELL_TYPE.getCode()) {
				Double productDescriptionDoubleValue = nextCell.getNumericCellValue();
				if(productDescriptionDoubleValue != null){
					Integer productDescriptionIntegerValue = productDescriptionDoubleValue.intValue();
					productExcelRawData.setProductDes(productDescriptionIntegerValue.toString());
				}
			}

			if(cellType == CellTypeEnum.STRING_CELL_TYPE.getCode()) {
				if (!StringUtils.isEmpty(nextCell.getStringCellValue())) {
					productExcelRawData.setProductDes(nextCell.getStringCellValue());
				}
			}
		}

		if (columnIndex == ProductExcelColumnIndex.PRODUCT_HASHTAG_COLUMN_INDEX) {
			Integer cellType = checkCellType(nextCell);
			if(cellType == CellTypeEnum.NUMERIC_CELL_TYPE.getCode()) {
				Double productHashtagDoubleValue = nextCell.getNumericCellValue();
				if(productHashtagDoubleValue != null){
					Integer productHashtagIntegerValue = productHashtagDoubleValue.intValue();
					productExcelRawData.setHashTag(productHashtagIntegerValue.toString());
				}
			}

			if(cellType == CellTypeEnum.STRING_CELL_TYPE.getCode()) {
				if (!StringUtils.isEmpty(nextCell.getStringCellValue())) {
					productExcelRawData.setHashTag(nextCell.getStringCellValue());
				}
			}
		}

		if (columnIndex == ProductExcelColumnIndex.PRODUCT_UNIT_COLUMN_INDEX) {
			Integer cellType = checkCellType(nextCell);
			if(cellType == CellTypeEnum.NUMERIC_CELL_TYPE.getCode()) {
				Double productUnitDoubleValue = nextCell.getNumericCellValue();
				if(productUnitDoubleValue != null){
					Integer producUnitIntegerValue = productUnitDoubleValue.intValue();
					productExcelRawData.setUnit(producUnitIntegerValue.toString());
				}
			}

			if(cellType == CellTypeEnum.STRING_CELL_TYPE.getCode()) {
				if (!StringUtils.isEmpty(nextCell.getStringCellValue())) {
					productExcelRawData.setUnit(nextCell.getStringCellValue());
				}
			}
		}

		if (columnIndex == ProductExcelColumnIndex.PRODUCT_PRICE_COLUMN_INDEX) {
			try {
				if (!StringUtils.isEmpty(String.valueOf(nextCell.getNumericCellValue()))) {
					BigDecimal rawPrice = new BigDecimal(nextCell.getNumericCellValue());
					rawPrice = rawPrice.setScale(PRODUCT_PRICE_DECIMAL_POINT, RoundingMode.HALF_UP);
					productExcelRawData.setPrice(rawPrice);
				}
			} catch (Exception e) {
				logger.error("Row number is : {}, Product Price is not valid, please insert numeric field only", rowNumber);
			}
		}

		if (columnIndex == ProductExcelColumnIndex.PRODUCT_PACKAGING_COLUMN_INDEX) {
			Integer cellType = checkCellType(nextCell);
			if(cellType == CellTypeEnum.NUMERIC_CELL_TYPE.getCode()) {
				Double productPackagingDoubleValue = nextCell.getNumericCellValue();
				if(productPackagingDoubleValue != null){
					Integer producPackagingIntegerValue = productPackagingDoubleValue.intValue();
					productExcelRawData.setPackaging(producPackagingIntegerValue.toString());
				}
			}

			if(cellType == CellTypeEnum.STRING_CELL_TYPE.getCode()) {
				if (!StringUtils.isEmpty(nextCell.getStringCellValue())) {
					productExcelRawData.setPackaging(nextCell.getStringCellValue());
				}
			}
		}

		if (columnIndex == ProductExcelColumnIndex.PRODUCT_MIN_OF_ORDER_COLUMN_INDEX) {
			try {
				if (!StringUtils.isEmpty(String.valueOf(nextCell.getNumericCellValue()))) {

					Double minOfOrder = nextCell.getNumericCellValue();
					if (!checkNumeric(String.valueOf(minOfOrder.intValue())))
						return null;
					productExcelRawData.setMinOfOrder(minOfOrder.intValue());
				}
			} catch (Exception e) {
				logger.error("Row Number is : {}, Product min of order is invalid, Please enter numeric value only", rowNumber);
			}
		}

		if (columnIndex == ProductExcelColumnIndex.PRODUCT_BROCHURE_ATTACHMENT_URL_COLUMN_INDEX) {
			Integer cellType = checkCellType(nextCell);
			if(cellType == CellTypeEnum.NUMERIC_CELL_TYPE.getCode()) {
				Double productBrochureAttachmentUrlDoubleValue = nextCell.getNumericCellValue();
				if(productBrochureAttachmentUrlDoubleValue != null){
					Integer producBrochureAttachmentUrlIntegerValue = productBrochureAttachmentUrlDoubleValue.intValue();
					productExcelRawData.setBrochureAttachmentUrl(producBrochureAttachmentUrlIntegerValue.toString());
				}
			}

			if(cellType == CellTypeEnum.STRING_CELL_TYPE.getCode()) {
				if (!StringUtils.isEmpty(nextCell.getStringCellValue())) {
					productExcelRawData.setBrochureAttachmentUrl(nextCell.getStringCellValue());
				}
			}
		}

		if (columnIndex == ProductExcelColumnIndex.PRODUCT_VIDEO_ATTACHMENT_URL_COLUMN_INDEX) {
			Integer cellType = checkCellType(nextCell);
			if(cellType == CellTypeEnum.NUMERIC_CELL_TYPE.getCode()) {
				Double productVideoAttachmentUrlDoubleValue = nextCell.getNumericCellValue();
				if(productVideoAttachmentUrlDoubleValue != null){
					Integer producVideoAttachmentUrlIntegerValue = productVideoAttachmentUrlDoubleValue.intValue();
					productExcelRawData.setVideoAttachmentUrl(producVideoAttachmentUrlIntegerValue.toString());
				}
			}
			if(cellType == CellTypeEnum.STRING_CELL_TYPE.getCode()) {
				if (!StringUtils.isEmpty(nextCell.getStringCellValue())) {
					productExcelRawData.setVideoAttachmentUrl(nextCell.getStringCellValue());
				}
			}
		}

		if (columnIndex == ProductExcelColumnIndex.PRODUCT_IMAGE_1_COLUMN_INDEX) {
			Integer cellType = checkCellType(nextCell);
			if(cellType == CellTypeEnum.NUMERIC_CELL_TYPE.getCode()) {
				Double productImageAttachmentUrlDoubleValue = nextCell.getNumericCellValue();
				if(productImageAttachmentUrlDoubleValue != null){
					Integer productImageAttachmentUrlIntegerValue = productImageAttachmentUrlDoubleValue.intValue();
					imageURLs.add(productImageAttachmentUrlIntegerValue.toString());
				}
			}

			if(cellType == CellTypeEnum.STRING_CELL_TYPE.getCode()) {
				if (!StringUtils.isEmpty(nextCell.getStringCellValue())) {
					imageURLs.add(nextCell.getStringCellValue());
				}
			}
		}

		if (columnIndex == ProductExcelColumnIndex.PRODUCT_IMAGE_2_COLUMN_INDEX) {
			Integer cellType = checkCellType(nextCell);
			if(cellType == CellTypeEnum.NUMERIC_CELL_TYPE.getCode()) {
				Double productImageAttachmentUrlDoubleValue = nextCell.getNumericCellValue();
				if(productImageAttachmentUrlDoubleValue != null){
					Integer productImageAttachmentUrlIntegerValue = productImageAttachmentUrlDoubleValue.intValue();
					imageURLs.add(productImageAttachmentUrlIntegerValue.toString());
				}
			}

			if(cellType == CellTypeEnum.STRING_CELL_TYPE.getCode()) {
				if (!StringUtils.isEmpty(nextCell.getStringCellValue())) {
					imageURLs.add(nextCell.getStringCellValue());
				}
			}
		}

		if (columnIndex == ProductExcelColumnIndex.PRODUCT_IMAGE_3_COLUMN_INDEX) {
			Integer cellType = checkCellType(nextCell);
			if(cellType == CellTypeEnum.NUMERIC_CELL_TYPE.getCode()) {
				Double productImageAttachmentUrlDoubleValue = nextCell.getNumericCellValue();
				if(productImageAttachmentUrlDoubleValue != null){
					Integer productImageAttachmentUrlIntegerValue = productImageAttachmentUrlDoubleValue.intValue();
					imageURLs.add(productImageAttachmentUrlIntegerValue.toString());
				}
			}

			if(cellType == CellTypeEnum.STRING_CELL_TYPE.getCode()) {
				if (!StringUtils.isEmpty(nextCell.getStringCellValue())) {
					imageURLs.add(nextCell.getStringCellValue());
				}
			}
		}

		if (columnIndex == ProductExcelColumnIndex.PRODUCT_IMAGE_4_COLUMN_INDEX) {
			Integer cellType = checkCellType(nextCell);
			if(cellType == CellTypeEnum.NUMERIC_CELL_TYPE.getCode()) {
				Double productImageAttachmentUrlDoubleValue = nextCell.getNumericCellValue();
				if(productImageAttachmentUrlDoubleValue != null){
					Integer productImageAttachmentUrlIntegerValue = productImageAttachmentUrlDoubleValue.intValue();
					imageURLs.add(productImageAttachmentUrlIntegerValue.toString());
				}
			}

			if(cellType == CellTypeEnum.STRING_CELL_TYPE.getCode()) {
				if (!StringUtils.isEmpty(nextCell.getStringCellValue())) {
					imageURLs.add(nextCell.getStringCellValue());
				}
			}
		}

		if (columnIndex == ProductExcelColumnIndex.PRODUCT_IMAGE_5_COLUMN_INDEX) {
			Integer cellType = checkCellType(nextCell);
			if(cellType == CellTypeEnum.NUMERIC_CELL_TYPE.getCode()) {
				Double productImageAttachmentUrlDoubleValue = nextCell.getNumericCellValue();
				if(productImageAttachmentUrlDoubleValue != null){
					Integer productImageAttachmentUrlIntegerValue = productImageAttachmentUrlDoubleValue.intValue();
					imageURLs.add(productImageAttachmentUrlIntegerValue.toString());
				}
			}

			if(cellType == CellTypeEnum.STRING_CELL_TYPE.getCode()) {
				if (!StringUtils.isEmpty(nextCell.getStringCellValue())) {
					imageURLs.add(nextCell.getStringCellValue());
				}
			}
		}

		if (columnIndex == ProductExcelColumnIndex.PRODUCT_IMAGE_6_COLUMN_INDEX) {
			Integer cellType = checkCellType(nextCell);
			if(cellType == CellTypeEnum.NUMERIC_CELL_TYPE.getCode()) {
				Double productImageAttachmentUrlDoubleValue = nextCell.getNumericCellValue();
				if(productImageAttachmentUrlDoubleValue != null){
					Integer productImageAttachmentUrlIntegerValue = productImageAttachmentUrlDoubleValue.intValue();
					imageURLs.add(productImageAttachmentUrlIntegerValue.toString());
				}
			}

			if(cellType == CellTypeEnum.STRING_CELL_TYPE.getCode()) {
				if (!StringUtils.isEmpty(nextCell.getStringCellValue())) {
					imageURLs.add(nextCell.getStringCellValue());
				}
			}
		}

		if (!CollectionUtils.isEmpty(imageURLs)) {
			productExcelRawData.setImagesUrl(imageURLs);
		}

		// Process technical raw data
		List<Pair<String, String>> productTechnicals = new ArrayList<>();

		String productTechnicalLabel = null;
		String productTechnicalValue = null;
		if (columnIndex == ProductExcelColumnIndex.PRODUCT_TECH_LABEL_1_COLUMN_INDEX) {
			Integer cellType = checkCellType(nextCell);
			if(cellType == CellTypeEnum.NUMERIC_CELL_TYPE.getCode()) {
				Double productTechnicalLabelDoubleValue = nextCell.getNumericCellValue();
				if(productTechnicalLabelDoubleValue != null){
					Integer productTechnicalLabelIntegerValue = productTechnicalLabelDoubleValue.intValue();
					productExcelRawData.setProductTechnicalLabel(productTechnicalLabelIntegerValue.toString());
				}
			}

			if(cellType == CellTypeEnum.STRING_CELL_TYPE.getCode()) {
				if (!StringUtils.isEmpty(nextCell.getStringCellValue())) {
					productTechnicalLabel = nextCell.getStringCellValue();
					productExcelRawData.setProductTechnicalLabel(productTechnicalLabel);
				}
			}
		}

		if (columnIndex == ProductExcelColumnIndex.PRODUCT_TECH_VALUE_1_COLUMN_INDEX) {
			Integer cellType = checkCellType(nextCell);
			if(cellType == CellTypeEnum.NUMERIC_CELL_TYPE.getCode()) {
				Double productTechnicalValueDoubleValue = nextCell.getNumericCellValue();
				if(productTechnicalValueDoubleValue != null){
					Integer productTechnicalValueIntegerValue = productTechnicalValueDoubleValue.intValue();
					productExcelRawData.setProductTechnicalValue(productTechnicalValueIntegerValue.toString());
				}
			}

			if(cellType == CellTypeEnum.STRING_CELL_TYPE.getCode()) {
				if (!StringUtils.isEmpty(nextCell.getStringCellValue())) {
					productTechnicalValue = nextCell.getStringCellValue();
					productExcelRawData.setProductTechnicalValue(productTechnicalValue);
				}
			}
		}

		if (!StringUtils.isEmpty(productExcelRawData.getProductTechnicalLabel())
				&& !StringUtils.isEmpty(productExcelRawData.getProductTechnicalValue())) {
			Pair<String, String> productTechnical = Pair.of(productExcelRawData.getProductTechnicalLabel(),
					productExcelRawData.getProductTechnicalValue());
			productTechnicals.add(productTechnical);
		}

		String productTechnicalLabelTwo = null;
		String productTechnicalValueTwo = null;
		if (columnIndex == ProductExcelColumnIndex.PRODUCT_TECH_LABEL_2_COLUMN_INDEX) {
			Integer cellType = checkCellType(nextCell);
			if(cellType == CellTypeEnum.NUMERIC_CELL_TYPE.getCode()) {
				Double productTechnicalLabelTwoDoubleValue = nextCell.getNumericCellValue();
				if(productTechnicalLabelTwoDoubleValue != null){
					Integer productTechnicalLabelTwoIntegerValue = productTechnicalLabelTwoDoubleValue.intValue();
					productExcelRawData.setProductTechnicalLabelTwo(productTechnicalLabelTwoIntegerValue.toString());
				}
			}

			if(cellType == CellTypeEnum.STRING_CELL_TYPE.getCode()) {
				if (!StringUtils.isEmpty(nextCell.getStringCellValue())) {
					productTechnicalLabelTwo = nextCell.getStringCellValue();
					productExcelRawData.setProductTechnicalLabelTwo(productTechnicalLabelTwo);
				}
			}
		}

		if (columnIndex == ProductExcelColumnIndex.PRODUCT_TECH_VALUE_2_COLUMN_INDEX) {
			Integer cellType = checkCellType(nextCell);
			if(cellType == CellTypeEnum.NUMERIC_CELL_TYPE.getCode()) {
				Double productTechnicalValueTwoDoubleValue = nextCell.getNumericCellValue();
				if(productTechnicalValueTwoDoubleValue != null){
					Integer productTechnicalValueTwoIntegerValue = productTechnicalValueTwoDoubleValue.intValue();
					productExcelRawData.setProductTechnicalValueTwo(productTechnicalValueTwoIntegerValue.toString());
				}
			}

			if(cellType == CellTypeEnum.STRING_CELL_TYPE.getCode()) {
				if (!StringUtils.isEmpty(nextCell.getStringCellValue())) {
					productTechnicalValueTwo = nextCell.getStringCellValue();
					productExcelRawData.setProductTechnicalValueTwo(productTechnicalValueTwo);
				}
			}
		}

		if (!StringUtils.isEmpty(productExcelRawData.getProductTechnicalLabelTwo()) &&
				!StringUtils.isEmpty(productExcelRawData.getProductTechnicalValueTwo())) {
			Pair<String, String> productTechnicalTwo = Pair.of(
					productExcelRawData.getProductTechnicalLabelTwo(),
					productExcelRawData.getProductTechnicalValueTwo());
			productTechnicals.add(productTechnicalTwo);
		}

		String productTechnicalLabelThree = null;
		String productTechnicalValueThree = null;
		if (columnIndex == ProductExcelColumnIndex.PRODUCT_TECH_LABEL_3_COLUMN_INDEX) {
			Integer cellType = checkCellType(nextCell);
			if(cellType == CellTypeEnum.NUMERIC_CELL_TYPE.getCode()) {
				Double productTechnicalLabelThreeDoubleValue = nextCell.getNumericCellValue();
				if(productTechnicalLabelThreeDoubleValue != null){
					Integer productTechnicalLabelThreeIntegerValue = productTechnicalLabelThreeDoubleValue.intValue();
					productExcelRawData.setProductTechnicalLabelThree(productTechnicalLabelThreeIntegerValue.toString());
				}
			}

			if(cellType == CellTypeEnum.STRING_CELL_TYPE.getCode()) {
				if (!StringUtils.isEmpty(nextCell.getStringCellValue())) {
					productTechnicalLabelThree = nextCell.getStringCellValue();
					productExcelRawData.setProductTechnicalLabelThree(productTechnicalLabelThree);
				}
			}
		}

		if (columnIndex == ProductExcelColumnIndex.PRODUCT_TECH_VALUE_3_COLUMN_INDEX) {
			Integer cellType = checkCellType(nextCell);
			if(cellType == CellTypeEnum.NUMERIC_CELL_TYPE.getCode()) {
				Double productTechnicalValueThreeDoubleValue = nextCell.getNumericCellValue();
				if(productTechnicalValueThreeDoubleValue != null){
					Integer productTechnicalValueThreeIntegerValue = productTechnicalValueThreeDoubleValue.intValue();
					productExcelRawData.setProductTechnicalValueThree(productTechnicalValueThreeIntegerValue.toString());
				}
			}

			if(cellType == CellTypeEnum.STRING_CELL_TYPE.getCode()) {
				if (!StringUtils.isEmpty(nextCell.getStringCellValue())) {
					productTechnicalValueThree = nextCell.getStringCellValue();
					productExcelRawData.setProductTechnicalValueThree(productTechnicalValueThree);

				}
			}
		}

		if (!StringUtils.isEmpty(productExcelRawData.getProductTechnicalLabelThree()) &&
				!StringUtils.isEmpty(productExcelRawData.getProductTechnicalValueThree())) {
			Pair<String, String> productTechnicalThree = Pair.of(
					productExcelRawData.getProductTechnicalLabelThree(),
					productExcelRawData.getProductTechnicalValueThree());
			productTechnicals.add(productTechnicalThree);
		}

		String productTechnicalLabelFour = null;
		String productTechnicalValueFour = null;
		if (columnIndex == ProductExcelColumnIndex.PRODUCT_TECH_LABEL_4_COLUMN_INDEX) {
			Integer cellType = checkCellType(nextCell);
			if(cellType == CellTypeEnum.NUMERIC_CELL_TYPE.getCode()) {
				Double productTechnicalLabelFourDoubleValue = nextCell.getNumericCellValue();
				if(productTechnicalLabelFourDoubleValue != null){
					Integer productTechnicalLabelFourIntegerValue = productTechnicalLabelFourDoubleValue.intValue();
					productExcelRawData.setProductTechnicalLabelFour(productTechnicalLabelFourIntegerValue.toString());
				}
			}

			if(cellType == CellTypeEnum.STRING_CELL_TYPE.getCode()) {
				if (!StringUtils.isEmpty(nextCell.getStringCellValue())) {
					productTechnicalLabelFour = nextCell.getStringCellValue();
					productExcelRawData.setProductTechnicalLabelFour(productTechnicalLabelFour);
				}
			}
		}

		if (columnIndex == ProductExcelColumnIndex.PRODUCT_TECH_VALUE_4_COLUMN_INDEX) {
			Integer cellType = checkCellType(nextCell);
			if(cellType == CellTypeEnum.NUMERIC_CELL_TYPE.getCode()) {
				Double productTechnicalValueFourDoubleValue = nextCell.getNumericCellValue();
				if(productTechnicalValueFourDoubleValue != null){
					Integer productTechnicalValueFourIntegerValue = productTechnicalValueFourDoubleValue.intValue();
					productExcelRawData.setProductTechnicalValueFour(productTechnicalValueFourIntegerValue.toString());
				}
			}

			if(cellType == CellTypeEnum.STRING_CELL_TYPE.getCode()) {
				if (!StringUtils.isEmpty(nextCell.getStringCellValue())) {
					productTechnicalValueFour = nextCell.getStringCellValue();
					productExcelRawData.setProductTechnicalValueFour(productTechnicalValueFour);
				}
			}
		}

		if (!StringUtils.isEmpty(productExcelRawData.getProductTechnicalLabelFour()) &&
				!StringUtils.isEmpty(productExcelRawData.getProductTechnicalValueFour())) {
			Pair<String, String> productTechnicalFour = Pair.of(
					productExcelRawData.getProductTechnicalLabelFour(),
					productExcelRawData.getProductTechnicalValueFour());
			productTechnicals.add(productTechnicalFour);
		}

		String productTechnicalLabelFive = null;
		String productTechnicalValueFive = null;
		if (columnIndex == ProductExcelColumnIndex.PRODUCT_TECH_LABEL_5_COLUMN_INDEX) {
			Integer cellType = checkCellType(nextCell);
			if(cellType == CellTypeEnum.NUMERIC_CELL_TYPE.getCode()) {
				Double productTechnicalLabelFiveDoubleValue = nextCell.getNumericCellValue();
				if(productTechnicalLabelFiveDoubleValue != null){
					Integer productTechnicalLabelFiveIntegerValue = productTechnicalLabelFiveDoubleValue.intValue();
					productExcelRawData.setProductTechnicalLabelFive(productTechnicalLabelFiveIntegerValue.toString());
				}
			}

			if(cellType == CellTypeEnum.STRING_CELL_TYPE.getCode()) {
				if (!StringUtils.isEmpty(nextCell.getStringCellValue())) {
					productTechnicalLabelFive = nextCell.getStringCellValue();
					productExcelRawData.setProductTechnicalLabelFive(productTechnicalLabelFive);
				}
			}
		}

		if (columnIndex == ProductExcelColumnIndex.PRODUCT_TECH_VALUE_5_COLUMN_INDEX) {
			Integer cellType = checkCellType(nextCell);
			if(cellType == CellTypeEnum.NUMERIC_CELL_TYPE.getCode()) {
				Double productTechnicalValueFiveDoubleValue = nextCell.getNumericCellValue();
				if(productTechnicalValueFiveDoubleValue != null){
					Integer productTechnicalValueFiveIntegerValue = productTechnicalValueFiveDoubleValue.intValue();
					productExcelRawData.setProductTechnicalValueFive(productTechnicalValueFiveIntegerValue.toString());
				}
			}

			if(cellType == CellTypeEnum.STRING_CELL_TYPE.getCode()) {
				if (!StringUtils.isEmpty(nextCell.getStringCellValue())) {
					productTechnicalValueFive = nextCell.getStringCellValue();
					productExcelRawData.setProductTechnicalValueFive(productTechnicalValueFive);
				}
			}
		}

		if (!StringUtils.isEmpty(productExcelRawData.getProductTechnicalLabelFive()) &&
				!StringUtils.isEmpty(productExcelRawData.getProductTechnicalValueFive())) {
			Pair<String, String> productTechnicalFive = Pair.of(
					productExcelRawData.getProductTechnicalLabelFive(),
					productExcelRawData.getProductTechnicalValueFive());
			productTechnicals.add(productTechnicalFive);
		}

		String productTechnicalLabelSix = null;
		String productTechnicalValueSix = null;
		if (columnIndex == ProductExcelColumnIndex.PRODUCT_TECH_LABEL_6_COLUMN_INDEX) {
			Integer cellType = checkCellType(nextCell);
			if(cellType == CellTypeEnum.NUMERIC_CELL_TYPE.getCode()) {
				Double productTechnicalLabelSixDoubleValue = nextCell.getNumericCellValue();
				if(productTechnicalLabelSixDoubleValue != null){
					Integer productTechnicalLabelSixIntegerValue = productTechnicalLabelSixDoubleValue.intValue();
					productExcelRawData.setProductTechnicalLabelSix(productTechnicalLabelSixIntegerValue.toString());
				}
			}

			if(cellType == CellTypeEnum.STRING_CELL_TYPE.getCode()) {
				if (!StringUtils.isEmpty(nextCell.getStringCellValue())) {
					productTechnicalLabelSix = nextCell.getStringCellValue();
					productExcelRawData.setProductTechnicalLabelSix(productTechnicalLabelSix);
				}
			}
		}

		if (columnIndex == ProductExcelColumnIndex.PRODUCT_TECH_VALUE_6_COLUMN_INDEX) {
			Integer cellType = checkCellType(nextCell);
			if(cellType == CellTypeEnum.NUMERIC_CELL_TYPE.getCode()) {
				Double productTechnicalValueSixDoubleValue = nextCell.getNumericCellValue();
				if(productTechnicalValueSixDoubleValue != null){
					Integer productTechnicalValueSixIntegerValue = productTechnicalValueSixDoubleValue.intValue();
					productExcelRawData.setProductTechnicalValueSix(productTechnicalValueSixIntegerValue.toString());
				}
			}

			if(cellType == CellTypeEnum.STRING_CELL_TYPE.getCode()) {
				if (!StringUtils.isEmpty(nextCell.getStringCellValue())) {
					productTechnicalValueSix = nextCell.getStringCellValue();
					productExcelRawData.setProductTechnicalValueSix(productTechnicalValueSix);
				}
			}
		}

		if (!StringUtils.isEmpty(productExcelRawData.getProductTechnicalLabelSix()) &&
				!StringUtils.isEmpty(productExcelRawData.getProductTechnicalValueSix())) {
			Pair<String, String> productTechnicalSix = Pair.of(
					productExcelRawData.getProductTechnicalLabelSix(),
					productExcelRawData.getProductTechnicalValueSix());
			productTechnicals.add(productTechnicalSix);
		}

		productExcelRawData.setProductTechnicals(productTechnicals);


		// Process product feature raw data
		List<Pair<String, String>> productFeatures = new ArrayList<>();

		String productFeatureLabel = null;
		String productFeatureValue = null;
		if (columnIndex == ProductExcelColumnIndex.PRODUCT_FEATURE_LABEL_1_COLUMN_INDEX) {
			Integer cellType = checkCellType(nextCell);
			if(cellType == CellTypeEnum.NUMERIC_CELL_TYPE.getCode()) {
				Double productFeatureLabelDoubleValue = nextCell.getNumericCellValue();
				if(productFeatureLabelDoubleValue != null){
					Integer productFeatureLabelIntegerValue = productFeatureLabelDoubleValue.intValue();
					productExcelRawData.setProductFeatureLabel(productFeatureLabelIntegerValue.toString());
				}
			}

			if(cellType == CellTypeEnum.STRING_CELL_TYPE.getCode()) {
				if (!StringUtils.isEmpty(nextCell.getStringCellValue())) {
					productFeatureLabel = nextCell.getStringCellValue();
					productExcelRawData.setProductFeatureLabel(productFeatureLabel);
				}
			}
		}

		if (columnIndex == ProductExcelColumnIndex.PRODUCT_FEATURE_VALUE_1_COLUMN_INDEX) {
			Integer cellType = checkCellType(nextCell);
			if(cellType == CellTypeEnum.NUMERIC_CELL_TYPE.getCode()) {
				Double productFeatureValueDoubleValue = nextCell.getNumericCellValue();
				if(productFeatureValueDoubleValue != null){
					Integer productFeatureValueIntegerValue = productFeatureValueDoubleValue.intValue();
					productExcelRawData.setProductFeatureValue(productFeatureValueIntegerValue.toString());
				}
			}

			if(cellType == CellTypeEnum.STRING_CELL_TYPE.getCode()) {
				if (!StringUtils.isEmpty(nextCell.getStringCellValue())) {
					productFeatureValue = nextCell.getStringCellValue();
					productExcelRawData.setProductFeatureValue(productFeatureValue);
				}
			}
		}

		if (!StringUtils.isEmpty(productExcelRawData.getProductFeatureLabel()) &&
				!StringUtils.isEmpty(productExcelRawData.getProductFeatureValue())) {
			Pair<String, String> productFeature = Pair.of(
					productExcelRawData.getProductFeatureLabel(),
					productExcelRawData.getProductFeatureValue());
			productFeatures.add(productFeature);
		}

		String productFeatureLabelTwo = null;
		String productFeatureValueTwo = null;
		if (columnIndex == ProductExcelColumnIndex.PRODUCT_FEATURE_LABEL_2_COLUMN_INDEX) {
			Integer cellType = checkCellType(nextCell);
			if(cellType == CellTypeEnum.NUMERIC_CELL_TYPE.getCode()) {
				Double productFeatureLabelTwoDoubleValue = nextCell.getNumericCellValue();
				if(productFeatureLabelTwoDoubleValue != null){
					Integer productFeatureLabelTwoIntegerValue = productFeatureLabelTwoDoubleValue.intValue();
					productExcelRawData.setProductFeatureLabelTwo(productFeatureLabelTwoIntegerValue.toString());
				}
			}

			if(cellType == CellTypeEnum.STRING_CELL_TYPE.getCode()) {
				if (!StringUtils.isEmpty(nextCell.getStringCellValue())) {
					productFeatureLabelTwo = nextCell.getStringCellValue();
					productExcelRawData.setProductFeatureLabelTwo(productFeatureLabelTwo);
				}
			}
		}

		if (columnIndex == ProductExcelColumnIndex.PRODUCT_FEATURE_VALUE_2_COLUMN_INDEX) {
			Integer cellType = checkCellType(nextCell);
			if(cellType == CellTypeEnum.NUMERIC_CELL_TYPE.getCode()) {
				Double productFeatureValueTwoDoubleValue = nextCell.getNumericCellValue();
				if(productFeatureValueTwoDoubleValue != null){
					Integer productFeatureValueTwoIntegerValue = productFeatureValueTwoDoubleValue.intValue();
					productExcelRawData.setProductFeatureValueTwo(productFeatureValueTwoIntegerValue.toString());
				}
			}

			if(cellType == CellTypeEnum.STRING_CELL_TYPE.getCode()) {
				if (!StringUtils.isEmpty(nextCell.getStringCellValue())) {
					productFeatureValueTwo = nextCell.getStringCellValue();
					productExcelRawData.setProductFeatureValueTwo(productFeatureValueTwo);
				}
			}
		}

		if (!StringUtils.isEmpty(productExcelRawData.getProductFeatureLabelTwo()) &&
				!StringUtils.isEmpty(productExcelRawData.getProductFeatureValueTwo())){
			Pair<String, String> productFeatureTwo = Pair.of(
					productExcelRawData.getProductFeatureLabelTwo(),
					productExcelRawData.getProductFeatureValueTwo());
			productFeatures.add(productFeatureTwo);
		}

		String productFeatureLabelThree = null;
		String productFeatureValueThree = null;
		if (columnIndex == ProductExcelColumnIndex.PRODUCT_FEATURE_LABEL_3_COLUMN_INDEX) {
			Integer cellType = checkCellType(nextCell);
			if(cellType == CellTypeEnum.NUMERIC_CELL_TYPE.getCode()) {
				Double productFeatureLabelThreeDoubleValue = nextCell.getNumericCellValue();
				if(productFeatureLabelThreeDoubleValue != null){
					Integer productFeatureLabelThreeIntegerValue = productFeatureLabelThreeDoubleValue.intValue();
					productExcelRawData.setProductFeatureLabelThree(productFeatureLabelThreeIntegerValue.toString());
				}
			}

			if(cellType == CellTypeEnum.STRING_CELL_TYPE.getCode()) {
				if (!StringUtils.isEmpty(nextCell.getStringCellValue())) {
					productFeatureLabelThree = nextCell.getStringCellValue();
					productExcelRawData.setProductFeatureLabelThree(productFeatureLabelThree);
				}
			}
		}

		if(columnIndex == ProductExcelColumnIndex.PRODUCT_FEATURE_VALUE_3_COLUMN_INDEX) {
			Integer cellType = checkCellType(nextCell);
			if(cellType == CellTypeEnum.NUMERIC_CELL_TYPE.getCode()) {
				Double productFeatureValueThreeDoubleValue = nextCell.getNumericCellValue();
				if(productFeatureValueThreeDoubleValue != null){
					Integer productFeatureValueThreeIntegerValue = productFeatureValueThreeDoubleValue.intValue();
					productExcelRawData.setProductFeatureValueThree(productFeatureValueThreeIntegerValue.toString());
				}
			}

			if(cellType == CellTypeEnum.STRING_CELL_TYPE.getCode()) {
				if (!StringUtils.isEmpty(nextCell.getStringCellValue())) {
					productFeatureValueThree = nextCell.getStringCellValue();
					productExcelRawData.setProductFeatureValueThree(productFeatureValueThree);
				}
			}
		}

		if(!StringUtils.isEmpty(productExcelRawData.getProductFeatureLabelThree()) &&
				!StringUtils.isEmpty(productExcelRawData.getProductFeatureValueThree())) {
			Pair<String, String> productFeatureThree = Pair.of(
					productExcelRawData.getProductFeatureLabelThree(),
					productExcelRawData.getProductFeatureValueThree());
			productFeatures.add(productFeatureThree);
		}

		String productFeatureLabelFour = null;
		String productFeatureValueFour = null;
		if (columnIndex == ProductExcelColumnIndex.PRODUCT_FEATURE_LABEL_4_COLUMN_INDEX) {
			Integer cellType = checkCellType(nextCell);
			if(cellType == CellTypeEnum.NUMERIC_CELL_TYPE.getCode()) {
				Double productFeatureLabelFourDoubleValue = nextCell.getNumericCellValue();
				if(productFeatureLabelFourDoubleValue != null){
					Integer productFeatureLabelFourIntegerValue = productFeatureLabelFourDoubleValue.intValue();
					productExcelRawData.setProductFeatureLabelFour(productFeatureLabelFourIntegerValue.toString());
				}
			}

			if(cellType == CellTypeEnum.STRING_CELL_TYPE.getCode()) {
				if (!StringUtils.isEmpty(nextCell.getStringCellValue())) {
					productFeatureLabelFour = nextCell.getStringCellValue();
					productExcelRawData.setProductFeatureLabelFour(productFeatureLabelFour);
				}
			}
		}

		if(columnIndex == ProductExcelColumnIndex.PRODUCT_FEATURE_VALUE_4_COLUMN_INDEX) {
			Integer cellType = checkCellType(nextCell);
			if(cellType == CellTypeEnum.NUMERIC_CELL_TYPE.getCode()) {
				Double productFeatureValueFourDoubleValue = nextCell.getNumericCellValue();
				if(productFeatureValueFourDoubleValue != null){
					Integer productFeatureValueFourIntegerValue = productFeatureValueFourDoubleValue.intValue();
					productExcelRawData.setProductFeatureValueFour(productFeatureValueFourIntegerValue.toString());
				}
			}

			if(cellType == CellTypeEnum.STRING_CELL_TYPE.getCode()) {
				if (!StringUtils.isEmpty(nextCell.getStringCellValue())) {
					productFeatureValueFour = nextCell.getStringCellValue();
					productExcelRawData.setProductFeatureValueFour(productFeatureValueFour);
				}
			}
		}

		if(!StringUtils.isEmpty(productExcelRawData.getProductFeatureLabelFour()) &&
				!StringUtils.isEmpty(productExcelRawData.getProductFeatureValueFour())) {
			Pair<String, String> productFeatureFour = Pair.of(
					productExcelRawData.getProductFeatureLabelFour(),
					productExcelRawData.getProductFeatureValueFour());
			productFeatures.add(productFeatureFour);
		}

		String productFeatureLabelFive = null;
		String productFeatureValueFive = null;
		if (columnIndex == ProductExcelColumnIndex.PRODUCT_FEATURE_LABEL_5_COLUMN_INDEX) {
			Integer cellType = checkCellType(nextCell);
			if(cellType == CellTypeEnum.NUMERIC_CELL_TYPE.getCode()) {
				Double productFeatureLabelFiveDoubleValue = nextCell.getNumericCellValue();
				if(productFeatureLabelFiveDoubleValue != null){
					Integer productFeatureLabelFiveIntegerValue = productFeatureLabelFiveDoubleValue.intValue();
					productExcelRawData.setProductFeatureLabelFive(productFeatureLabelFiveIntegerValue.toString());
				}
			}

			if(cellType == CellTypeEnum.STRING_CELL_TYPE.getCode()) {
				if (!StringUtils.isEmpty(nextCell.getStringCellValue())) {
					productFeatureLabelFive = nextCell.getStringCellValue();
					productExcelRawData.setProductFeatureLabelFive(productFeatureLabelFive);
				}
			}
		}

		if(columnIndex == ProductExcelColumnIndex.PRODUCT_FEATURE_VALUE_5_COLUMN_INDEX) {
			Integer cellType = checkCellType(nextCell);
			if(cellType == CellTypeEnum.NUMERIC_CELL_TYPE.getCode()) {
				Double productFeatureValueFiveDoubleValue = nextCell.getNumericCellValue();
				if(productFeatureValueFiveDoubleValue != null){
					Integer productFeatureValueFiveIntegerValue = productFeatureValueFiveDoubleValue.intValue();
					productExcelRawData.setProductFeatureValueFive(productFeatureValueFiveIntegerValue.toString());
				}
			}

			if(cellType == CellTypeEnum.STRING_CELL_TYPE.getCode()) {
				if (!StringUtils.isEmpty(nextCell.getStringCellValue())) {
					productFeatureValueFive = nextCell.getStringCellValue();
					productExcelRawData.setProductFeatureValueFive(productFeatureValueFive);
				}
			}
		}

		if(!StringUtils.isEmpty(productExcelRawData.getProductFeatureLabelFive()) &&
				!StringUtils.isEmpty(productExcelRawData.getProductFeatureValueFive())) {
			Pair<String, String> productFeatureFive = Pair.of(
					productExcelRawData.getProductFeatureLabelFive(), productExcelRawData.getProductFeatureValueFive());
			productFeatures.add(productFeatureFive);
		}

		String productFeatureLabelSix = null;
		String productFeatureValueSix = null;
		if (columnIndex == ProductExcelColumnIndex.PRODUCT_FEATURE_LABEL_6_COLUMN_INDEX) {
			Integer cellType = checkCellType(nextCell);
			if(cellType == CellTypeEnum.NUMERIC_CELL_TYPE.getCode()) {
				Double productFeatureLabelSixDoubleValue = nextCell.getNumericCellValue();
				if(productFeatureLabelSixDoubleValue != null){
					Integer productFeatureLabelSixIntegerValue = productFeatureLabelSixDoubleValue.intValue();
					productExcelRawData.setProductFeatureLabelSix(productFeatureLabelSixIntegerValue.toString());
				}
			}

			if(cellType == CellTypeEnum.STRING_CELL_TYPE.getCode()) {
				if (!StringUtils.isEmpty(nextCell.getStringCellValue())) {
					productFeatureLabelSix = nextCell.getStringCellValue();
					productExcelRawData.setProductFeatureLabelSix(productFeatureLabelSix);
				}
			}
		}

		if(columnIndex == ProductExcelColumnIndex.PRODUCT_FEATURE_VALUE_6_COLUMN_INDEX) {
			Integer cellType = checkCellType(nextCell);
			if(cellType == CellTypeEnum.NUMERIC_CELL_TYPE.getCode()) {
				Double productFeatureValueSixDoubleValue = nextCell.getNumericCellValue();
				if(productFeatureValueSixDoubleValue != null){
					Integer productFeatureValueSixIntegerValue = productFeatureValueSixDoubleValue.intValue();
					productExcelRawData.setProductFeatureValueSix(productFeatureValueSixIntegerValue.toString());
				}
			}

			if(cellType == CellTypeEnum.STRING_CELL_TYPE.getCode()) {
				if (!StringUtils.isEmpty(nextCell.getStringCellValue())) {
					productFeatureValueSix = nextCell.getStringCellValue();
					productExcelRawData.setProductFeatureValueSix(productFeatureValueSix);
				}
			}
		}

		if(!StringUtils.isEmpty(productExcelRawData.getProductFeatureLabelSix()) &&
				!StringUtils.isEmpty(productExcelRawData.getProductFeatureValueSix())){
			Pair<String, String> productFeatureSix = Pair.of(
					productExcelRawData.getProductFeatureLabelSix(),
					productExcelRawData.getProductFeatureValueSix());
			productFeatures.add(productFeatureSix);
		}

		productExcelRawData.setProductFeatures(productFeatures);

		//Product Batch Upload add new column which is Product_Procurement
		if (columnIndex == ProductExcelColumnIndex.PRODUCT_CAN_PROCURE_COLUMN_INDEX) {
			Integer cellType = checkCellType(nextCell);
			if(cellType == CellTypeEnum.NUMERIC_CELL_TYPE.getCode()){
				logger.error("Row Number is : {}, Please only use Y or N for can procure field", rowNumber);
			}

			if (!StringUtils.isEmpty(nextCell.getStringCellValue()))
			{
				Boolean canProcure = nextCell.getStringCellValue().equals(ProcurementEnum.TRUE.getCanProcure())
						? ProcurementEnum.TRUE.getConvertValueOfCanProcureToBoolean() : ProcurementEnum.FALSE.getConvertValueOfCanProcureToBoolean();
				productExcelRawData.setCanProcure(canProcure);
			}
		}

		/*Product Batch Upload add new column which is Product_Device_Category
		 * 1.check condition if empty for device category in batch upload, it will do nothing.
		 * 2. check condition if  device category is not 1, 2, 3, then do nothing
		 * 3. check condition if not numeric then it will return null.
		 * 4. set the device category in product.
		 */
		if (columnIndex == ProductExcelColumnIndex.PRODUCT_DEVICE_CATEGORY_COLUMN_INDEX) {
			Integer cellType = checkCellType(nextCell);
			if(cellType == CellTypeEnum.STRING_CELL_TYPE.getCode()){
				logger.error("Row Number : {}, Please Only use 1, 2 and 3, for product device category", rowNumber);
			}
			Double productDeviceCategory = nextCell.getNumericCellValue();
			if (productDeviceCategory != null)
			{
				if(productDeviceCategory.equals(DeviceCategoryEnum.ONE.getCode()) ||
						productDeviceCategory.equals(DeviceCategoryEnum.TWO.getCode()) ||
						productDeviceCategory.equals(DeviceCategoryEnum.THREE.getCode())){
					productExcelRawData.setDeviceCategory(productDeviceCategory.longValue());
				}
			}
		}

		/*
		* Add equipment type in batch upload.
		* this will determine whether the product device category is mandatory or not.
		 */
		if(columnIndex == ProductExcelColumnIndex.PRODUCT_EQUIPMENT_TYPE_COLUMN_INDEX){
			Integer cellType = checkCellType(nextCell);
			if(cellType == CellTypeEnum.NUMERIC_CELL_TYPE.getCode()){
				logger.error("Row Number is {}, Please only use Y and N for product equiment type.", rowNumber);
			}
			if(!StringUtils.isEmpty(nextCell.getStringCellValue())) {
				Integer productEquipmentType = nextCell.getStringCellValue().equals(EquipmentMappingEnum.MEDICAL_EQUIPMENT.getName())
						? EquipmentMappingEnum.MEDICAL_EQUIPMENT.getCode() : EquipmentMappingEnum.NON_MEDICAL_EQUIPMENT.getCode();
				productExcelRawData.setEquipmentType(productEquipmentType);
			}
		}

		if(columnIndex == ProductExcelColumnIndex.PRODUCT_HIERARCHY_COLUMN_INDEX){
			Integer cellType = checkCellType(nextCell);
			if(cellType == CellTypeEnum.NUMERIC_CELL_TYPE.getCode()){
				logger.error("Row Number : {}, Please only use like 01010101 - 软组织超声手术仪", rowNumber);
			}
			if(!StringUtils.isEmpty(nextCell.getStringCellValue())){
				String[]  productHierarchy = nextCell.getStringCellValue().split("-");
				if (!checkNumeric(productHierarchy[0].trim()))
					return null;
				productExcelRawData.setProductHierarchyCode(productHierarchy[0].trim());
			}
		}

		if(columnIndex == ProductExcelColumnIndex.PRODUCT_SKU_NUMBER_COLUMN_INDEX){
			Integer cellType = checkCellType(nextCell);
			if(cellType == CellTypeEnum.STRING_CELL_TYPE.getCode()){
				if(!StringUtils.isEmpty(nextCell.getStringCellValue())){
					productExcelRawData.setSkuNumber(nextCell.getStringCellValue());
				}
			}

			if(cellType == CellTypeEnum.NUMERIC_CELL_TYPE.getCode()){
				//convert sku number to integer, because if is double value will have .0 behind, sku number cannot have .0 behind.
				Double numericProductSKUNumber = nextCell.getNumericCellValue();
				Integer convertProductSKUNumberToInteger = numericProductSKUNumber.intValue();
				if(convertProductSKUNumberToInteger != null){
					productExcelRawData.setSkuNumber(convertProductSKUNumberToInteger.toString());
				}
			}

		}

		if(columnIndex == ProductExcelColumnIndex.PRODUCT_PATENT_COLUMN_INDEX){
			Integer cellType = checkCellType(nextCell);
			if(cellType == CellTypeEnum.NUMERIC_CELL_TYPE.getCode()) {
				Double productPatentDoubleValue = nextCell.getNumericCellValue();
				if(productPatentDoubleValue != null){
					Integer productPatentIntegerValue = productPatentDoubleValue.intValue();
					productExcelRawData.setPatent(productPatentIntegerValue.toString());
				}
			}

			if(cellType == CellTypeEnum.STRING_CELL_TYPE.getCode()) {
				if (!StringUtils.isEmpty(nextCell.getStringCellValue())) {
					productExcelRawData.setPatent(nextCell.getStringCellValue());
				}
			}
		}

		if (columnIndex == ProductExcelColumnIndex.PRODUCT_VENDOR_UNIT_PRICE_COLUMN_INDEX) {
			try {
				if (!StringUtils.isEmpty(String.valueOf(nextCell.getNumericCellValue()))) {
					BigDecimal rawPrice = new BigDecimal(nextCell.getNumericCellValue());
					rawPrice = rawPrice.setScale(PRODUCT_PRICE_DECIMAL_POINT, RoundingMode.HALF_UP);
					productExcelRawData.setVendorUnitPrice(rawPrice);
				}
			}catch(Exception e){
				logger.info("Row Number is : {}, Product Vendor unit price is not valid, please insert numeric field only", rowNumber);
			}
		}

		if (columnIndex == ProductExcelColumnIndex.PRODUCT_VENDOR_PRICE_COLUMN_INDEX) {
			try {
				if (!StringUtils.isEmpty(String.valueOf(nextCell.getNumericCellValue()))) {
					BigDecimal rawPrice = new BigDecimal(nextCell.getNumericCellValue());
					rawPrice = rawPrice.setScale(PRODUCT_PRICE_DECIMAL_POINT, RoundingMode.HALF_UP);
					productExcelRawData.setVendorPrice(rawPrice);
				}
			}catch(Exception e){
				logger.info("Row Number is {}, Product Vendor price is not valid, please insert numeric field only", rowNumber);
			}
		}

		if(columnIndex == ProductExcelColumnIndex.PRODUCT_DISCLAIMER_COLUMN_INDEX){
			Integer cellType = checkCellType(nextCell);
			if(cellType == CellTypeEnum.NUMERIC_CELL_TYPE.getCode()) {
				Double productDisclaimerDoubleValue = nextCell.getNumericCellValue();
				if(productDisclaimerDoubleValue != null){
					Integer productDisclaimerIntegerValue = productDisclaimerDoubleValue.intValue();
					productExcelRawData.setDisclaimer(productDisclaimerIntegerValue.toString());
				}
			}

			if(cellType == CellTypeEnum.STRING_CELL_TYPE.getCode()) {
				if (!StringUtils.isEmpty(nextCell.getStringCellValue())) {
					productExcelRawData.setDisclaimer(nextCell.getStringCellValue());
				}
			}
		}

		if(columnIndex == ProductExcelColumnIndex.PRODUCT_INSTRUCTION_TO_USE_COLUMN_INDEX){
			Integer cellType = checkCellType(nextCell);
			if(cellType == CellTypeEnum.NUMERIC_CELL_TYPE.getCode()) {
				Double productInstructionForUseDoubleValue = nextCell.getNumericCellValue();
				if(productInstructionForUseDoubleValue != null){
					Integer productInstructionForUseIntegerValue = productInstructionForUseDoubleValue.intValue();
					productExcelRawData.setInstructionsForUse(productInstructionForUseIntegerValue.toString());
				}
			}

			if(cellType == CellTypeEnum.STRING_CELL_TYPE.getCode()) {
				if (!StringUtils.isEmpty(nextCell.getStringCellValue())) {
					productExcelRawData.setInstructionsForUse(nextCell.getStringCellValue());
				}
			}
		}

		if(columnIndex == ProductExcelColumnIndex.PRODUCT_WEBSITE_COLUMN_INDEX){
			Integer cellType = checkCellType(nextCell);
			if(cellType == CellTypeEnum.NUMERIC_CELL_TYPE.getCode()) {
				Double productWebsiteDoubleValue = nextCell.getNumericCellValue();
				if(productWebsiteDoubleValue != null){
					Integer productWebsiteIntegerValue = productWebsiteDoubleValue.intValue();
					productExcelRawData.setProductWebsite(productWebsiteIntegerValue.toString());
				}
			}

			if(cellType == CellTypeEnum.STRING_CELL_TYPE.getCode()) {
				if (!StringUtils.isEmpty(nextCell.getStringCellValue())) {
					productExcelRawData.setProductWebsite(nextCell.getStringCellValue());
				}
			}
		}

		if(columnIndex == ProductExcelColumnIndex.PRODUCT_NOTE_COLUMN_INDEX){
			Integer cellType = checkCellType(nextCell);
			if(cellType == CellTypeEnum.NUMERIC_CELL_TYPE.getCode()) {
				Double productnoteDoubleValue = nextCell.getNumericCellValue();
				if(productnoteDoubleValue != null){
					Integer productNoteIntegerValue = productnoteDoubleValue.intValue();
					productExcelRawData.setNote(productNoteIntegerValue.toString());
				}
			}

			if(cellType == CellTypeEnum.STRING_CELL_TYPE.getCode()) {
				if (!StringUtils.isEmpty(nextCell.getStringCellValue())) {
					productExcelRawData.setNote(nextCell.getStringCellValue());
				}
			}
		}
		return productExcelRawData;
	}

	private Boolean validateProductExcelData(ProductExcelRawData productExcelRawData) {
		if (productExcelRawData == null) return false;
		if (!checkEmptyOrNullString(productExcelRawData))return false;
		if (!checkLength(productExcelRawData))return false;
		if (!checkNumeric(productExcelRawData))return false;

		return true;
	}

	private Boolean checkEmptyOrNullString(ProductExcelRawData productExcelRawData) {
		if (StringUtils.isEmpty(productExcelRawData.getProductNamePrimary())) {
			return false;
		}

		if (StringUtils.isEmpty(productExcelRawData.getPackaging())) {
			return false;
		}

		if (StringUtils.isEmpty(productExcelRawData.getProductCode())) {
			return false;
		}

		if (productExcelRawData.getPrice() == null) {
			return false;
		}

		if (StringUtils.isEmpty(productExcelRawData.getUnit())) {
			return false;
		}

		if (CollectionUtils.isEmpty(productExcelRawData.getImagesUrl())) {
			return false;
		}

		if(productExcelRawData.getEquipmentType().equals(EquipmentMappingEnum.MEDICAL_EQUIPMENT.getCode())){
			if(productExcelRawData.getDeviceCategory() == null){
				return false;
			}
		}

		return true;
	}

	public Boolean checkLength(ProductExcelRawData productExcelRawData) {

		if (!StringUtils.isEmpty(productExcelRawData.getBrand())) {
			if (productExcelRawData.getBrand().length() > ProductFieldsLength.PRODUCT_BRAND_NAME_LENGTH) {
				return false;
			}
		}

		if (!StringUtils.isEmpty(productExcelRawData.getCareArea())) {
			if (productExcelRawData.getCareArea().length() > ProductFieldsLength.PRODUCT_CARE_AREA_NAME_LNEGTH) {
				return false;
			}
		}

		if (!StringUtils.isEmpty(productExcelRawData.getCategory())) {
			if (productExcelRawData.getCategory().length() > ProductFieldsLength.PRODUCT_CATEGORY_NAME_LENGTH) {
				return false;
			}
		}

		if (productExcelRawData.getProductNamePrimary().length() > ProductFieldsLength.PRODUCT_NAME_PRIMARY_LENGTH) {
			return false;
		}

		if (!StringUtils.isEmpty(productExcelRawData.getProductNameSecondary())) {
			if (productExcelRawData.getProductNameSecondary()
					.length() > ProductFieldsLength.PRODUCT_NAME_SECONDARY_LENGTH) {
				return false;
			}
		}

		if(!StringUtils.isEmpty(productExcelRawData.getProductCode())) {
            if (productExcelRawData.getProductCode().length() > ProductFieldsLength.PRODUCT_CODE_LENGTH) {
                return false;
            }
        }

        if(!StringUtils.isEmpty(productExcelRawData.getProductModel())) {
            if (productExcelRawData.getProductModel().length() > ProductFieldsLength.PRODUCT_MODEL_LENGTH) {
                return false;
            }
        }

        if(!StringUtils.isEmpty(productExcelRawData.getProductRegNo())) {
            if (productExcelRawData.getProductRegNo().length() > ProductFieldsLength.PRODUCT_REG_NO_LENGTH) {
                return false;
            }
        }

        if(!StringUtils.isEmpty(productExcelRawData.getProductDes())) {
            if (productExcelRawData.getProductDes().length() > ProductFieldsLength.PRODUCT_DESCRIPTION_LENGTH) {
                return false;
            }
        }

        if(!StringUtils.isEmpty(productExcelRawData.getHashTag())) {
            if (productExcelRawData.getHashTag().length() > ProductFieldsLength.PRODUCT_HASHTAG_LENGTH) {
                return false;
            }
        }

        if(!StringUtils.isEmpty(productExcelRawData.getUnit())) {
            if (productExcelRawData.getUnit().length() > ProductFieldsLength.PRODUCT_UNIT_LENGTH) {
                return false;
            }
        }

        if(productExcelRawData.getPrice() != null) {
            String[] splitter = productExcelRawData.getPrice().toString().split("\\.");
            if (splitter[0].length() > ProductFieldsLength.PRODUCT_PRICE_PRECISION_LENGTH) {
                return false;
            }
        }

        if(!StringUtils.isEmpty(productExcelRawData.getPackaging())) {
            if (productExcelRawData.getPackaging().length() > ProductFieldsLength.PRODUCT_PACKAGING_LENGTH) {
                return false;
            }
        }

		if (!StringUtils.isEmpty(productExcelRawData.getBrochureAttachmentUrl())) {
			if (productExcelRawData.getBrochureAttachmentUrl()
					.length() > ProductFieldsLength.PRODUCT_BROCHURE_ATTACHMENT_URL_LENGTH) {
				return false;
			}
		}

		if (!StringUtils.isEmpty(productExcelRawData.getVideoAttachmentUrl())) {
			if (productExcelRawData.getVideoAttachmentUrl()
					.length() > ProductFieldsLength.PRODUCT_VIDEO_ATTACHMENT_URL_LENGTH) {
				return false;
			}
		}

		for (String imageUrl : productExcelRawData.getImagesUrl()) {
		    if(!StringUtils.isEmpty(imageUrl)) {
                if (imageUrl.length() > ProductFieldsLength.PRODUCT_IMAGE_URL_LENGTH) {
                    return false;
                }
            }
		}

		if (!CollectionUtils.isEmpty(productExcelRawData.getProductTechnicals())) {
			for (Pair<String, String> productTechnical : productExcelRawData.getProductTechnicals()) {
			    if(!StringUtils.isEmpty(productTechnical.getFirst())) {
                    if (productTechnical.getFirst().length() > ProductFieldsLength.PRODUCT_TECH_LABEL_LENGTH) {
                        return false;
                    }
                }

                if(!StringUtils.isEmpty(productTechnical.getSecond())) {
                    if (productTechnical.getSecond().length() > ProductFieldsLength.PRODUCT_TECH_VALUE_LENGTH) {
                        return false;
                    }
                }
			}
		}

		if (!CollectionUtils.isEmpty(productExcelRawData.getProductFeatures())) {
			for (Pair<String, String> productFeature : productExcelRawData.getProductFeatures()) {
                if(!StringUtils.isEmpty(productFeature.getFirst())) {
                    if (productFeature.getFirst().length() > ProductFieldsLength.PRODUCT_FEATURE_LABEL_LENGTH) {
                        return false;
                    }
                }

                if(!StringUtils.isEmpty(productFeature.getSecond())) {
                    if (productFeature.getSecond().length() > ProductFieldsLength.PRODUCT_FEATURE_VALUE_LENGTH) {
                        return false;
                    }
                }
			}
		}

		return true;
	}

	private Boolean checkNumeric(String fieldValue) {
		if(fieldValue != null) {
			if (!fieldValue.matches("[0-9]+")) {
			    logger.info("Only enter number for minimum order");
				return false;
			}
		}
		return true;
	}

	private Boolean checkNumericWithDecimal(String fieldValue) {
	    if(fieldValue != null) {
            if (!fieldValue.toString().matches("[0-9]+\\.[0-9]")) {
                logger.info("Only Entry numeric with decimal for price");
                return false;
            }
        }
		return true;
	}

	private Boolean checkNumeric(ProductExcelRawData productExcelRawData) {
	    if(productExcelRawData.getPrice() != null) {
            if (!productExcelRawData.getPrice().toString().matches("[0-9]+\\.[0-9]+")) {
                return false;
            }
        }
		return true;
	}

	private Pair<String, String> sendProductExcelProcessReport(String userName, String vendorName, String userEmail,
												  String sendFrom, String originalFileName, Integer totalCreatedProduct,
												  String errorRowInfo, IdsmedAccount uploaderAccount) throws IdsmedBaseException{
		List<String> recipients = new ArrayList<>();
		Boolean isSendingEmail = emailService.checkSendEmail(Arrays.asList(uploaderAccount), EmailTypeEnum.PRODUCT_BATCH_UPLOAD_RESULT_NOTIFICATION);
		if (isSendingEmail) {
			recipients.add(userEmail);
		}

		if (!CollectionUtils.isEmpty(recipients)) {
			Pair<String, String> emailPair = emailService.generatedProductBatchUploadReportEmail(userName, vendorName, originalFileName, totalCreatedProduct, errorRowInfo);
			EmailObject emailObject = new ProductCSVProcessReportEmailObject(recipients, sendFrom, emailPair);
			try {
				rabbitTemplate.convertAndSend(env.getProperty(IdsmedQueueConfig.RABBIT_MAIL_SERVICE_PROCESS_QUEUE), emailObject);
				return emailPair;
			} catch (AmqpException e) {
				logger.error("Send email object to queue error", e.getMessage());
				return null;
			}
		}

		return null;
	}

	private void updateProductCSVProcessStatus(Long vendorId, List<Integer> errorCSVRowNum) {
		ProductCSVProcessStatus productCSVProcessStatus = PRODUCT_EXCEL_PROCESS_STATUS.computeIfAbsent(vendorId, k -> new ProductCSVProcessStatus());
		productCSVProcessStatus.setErrorCSVRowNum(errorCSVRowNum);
	}

	private Integer checkCellType(Cell nextCell){
	    CellType cellType = nextCell.getCellType();
	    if(cellType == CellType.NUMERIC){
	        return 1;
        }

	    if(cellType == CellType.STRING){
	        return 2;
        }
	    return 0;
    }
	private void loggerForAllMethodInThisService(String methodDescription, String methodName, String loginId){
		logger.info(methodDescription, methodName, loginId != null?loginId:null);
	}
}

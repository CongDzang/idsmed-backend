package com.cmg.idsmed.service.product;

import java.math.BigDecimal;
import java.util.List;

import com.cmg.idsmed.dto.product.*;
import com.cmg.idsmed.dto.product.comparison.ProductComparisonResponse;
import com.cmg.idsmed.dto.product.wedoctor.ProductListWedoctorResponse;
import com.cmg.idsmed.dto.product.wedoctor.ProductWedoctorResponse;
import com.cmg.idsmed.model.entity.product.ProductHierarchyLevelThree;
import com.cmg.idsmed.model.entity.product.search.KeywordHistory;
import org.springframework.web.multipart.MultipartFile;

import com.cmg.idsmed.common.exception.IdsmedBaseException;
import com.cmg.idsmed.dto.product.search.KeywordHistoryResponse;

public interface ProductService {

	ProductListWedoctorResponse getProductListForWedoctor(Integer pageIndex, Integer pageSize, String langCode) throws IdsmedBaseException;

	ProductListResponse getProductListOfNewRecommend(Integer duration, Integer pageIndex, Integer pageSize, String langCode) throws IdsmedBaseException;


	ProductListResponse getProductListForVendor(Long vendorId, String productNamePrimary, Integer status, String productCode,
												String productModel, String companyNamePrimary, Integer vendorStatus, Long vendorCoordinatorId, Integer pageIndex, Integer pageSize) throws IdsmedBaseException;

	ProductResponse getProductById(Long id, String encodedLoginId, String langCode) throws IdsmedBaseException;

	ProductResponse getProductByIdForAnonymous(Long id, String langCode) throws IdsmedBaseException;

	SimpleProductResponse getSimpleProductDetailById(Long id, Long customerId, String langCode) throws IdsmedBaseException;

	ProductListResponse searchProductByKeyword(String keyword, List<Long> careAreaIds, List<Long> categoryIds
			, List<Long> brandIds, String type, Integer pageIndex, Integer pageSize
			, Integer sortFieldCode, String sortOrder, String encodedLoginId, BigDecimal minPrice, BigDecimal maxPrice
			, Integer productType, String manufacturerCountryCode, String manufacturerProvinceCode, List<Long> secondCategoryIds, String langCode) throws IdsmedBaseException;

	ProductListResponse searchProductByKeywordForAnonymous(String keyword, List<Long> careAreaIds, List<Long> categoryIds
			, List<Long> brandIds, String type, Integer pageIndex, Integer pageSize
			, Integer sortFieldCode, String sortOrder, BigDecimal minPrice, BigDecimal maxPrice
			, Integer productType, String manufacturerCountryCode, String manufacturerProvinceCode, List<Long> secondCategoryIds, String langCode) throws IdsmedBaseException;

	SimpleProductListResponse searchSimpleProductByKey(String keyword, Long wedoctorCustomerId, Long careArea
			, Long category, Long brand, Integer pageIndex, Integer pageSize)
			throws IdsmedBaseException;

	ProductAddInfoDetailResponse getProductAddInfoDetail(String langCode) throws IdsmedBaseException;

	ProductResponse create(ProductRequest request, MultipartFile video, MultipartFile brochure,
						   List<MultipartFile> imageFiles, List<MultipartFile> productDocumentAttachments) throws IdsmedBaseException;

	ProductResponse updateDraftOfPendingStatus(ProductRequest request, MultipartFile brochureFile, MultipartFile videoFile,
											   List<MultipartFile> imageFiles, List<MultipartFile> productDocumentAttachments) throws IdsmedBaseException;

	ProductResponse updatePendingApproveStatus(ProductRequest request) throws IdsmedBaseException;

	ProductResponse updateApproveStatus(ProductRequest request) throws IdsmedBaseException;

	ProductResponse updateRejectStatus(ProductRequest requests) throws IdsmedBaseException;

	ProductListSearchFieldsResponse getProductListSearchFields(List<Long> vendorIds) throws IdsmedBaseException;

	List<KeywordHistoryResponse> getLastProductSearchKeyword(Long userId, Integer number) throws IdsmedBaseException;

	ProductListResponse searchProductByUserFavourite(String keyword, Long wedoctorCustomerId, Integer pageIndex, Integer pageSize) throws IdsmedBaseException;

	ProductApiListResponse getProductListForNew(String userId, Integer pageIndex, Integer pageSize) throws IdsmedBaseException;

	ProductApiListResponse getProductListForHistory(Long clientId, String userId, Integer pageIndex, Integer pageSize) throws IdsmedBaseException;

	ProductApiListResponse getProductListForBest(String userId, Integer pageIndex, Integer pageSize) throws IdsmedBaseException;

	ProductApiListResponse getProductListFilter(Long clientId, String userId, String type, Integer pageIndex, Integer pageSize) throws IdsmedBaseException;

	ProductApiListResponse getProductSearchByStr(Long wedoctorCustomerId, String keyword, String brand, Integer pageIndex, Integer pageSize, String langCode) throws IdsmedBaseException;

	List<ProductDocumentAttachmentResponse> editProductDocumentAttachment(ProductDocumentAttachmentEditRequest productDocumentAttachmentEditRequest, List<MultipartFile> documentAttachments) throws IdsmedBaseException;

	ProductListResponse inactiveProduct(ProductInactiveRequest request) throws IdsmedBaseException;

	ProductGeneralInfoRateResponse productRating(ProductRatingRequest productRatingRequest) throws IdsmedBaseException;

	ProductRatingDetailListResponse getProductRatingCommentList(Long productId, Double score, Integer pageIndex, Integer pageSize, String langCode) throws IdsmedBaseException;

	ProductWishlistSimpleResponse updateWishlistInProductDetail(ProductWishlistRequest request) throws IdsmedBaseException;

	ProductWedoctorResponse getProductByIdForWedoctor(Long id, Long weDoctorCustomerId, String langCode) throws IdsmedBaseException;

	ProductWishlistSimpleResponse getFavouriteProductById(Long id, String langCode, String encodedLoginId) throws IdsmedBaseException;

	ProductTechFeatureResponse getListOfProductTechnicalAndFeature(String langCode) throws IdsmedBaseException;

	ProductWishlistSimpleResponse updateWishlistInProductDetailForWedoctor(ProductWishlistForWedoctorRequest request) throws IdsmedBaseException;

	ProductApiListResponse getSuggestedProducts(Long wedoctorCustomerId, Integer pageIndex, Integer pageSize, String langCode) throws IdsmedBaseException;

	ProductApiListResponse getRecentlySearchProduct(String langCode, Long wedoctorCustomerId, Integer pageIndex, Integer pageSize)throws IdsmedBaseException;

	ProductListResponse searchProductByKeywordForWedoctor(String keyword
			, List<Long> careAreaIds
			, List<Long> categoryIds
			, List<Long> brandIds
			, String type
			, Integer pageIndex
			, Integer pageSize
			, Integer sortFieldCode
			, String sortOrder
			, Long wedoctorCustomerId
	        , BigDecimal minPrice
	        , BigDecimal maxPrice) throws IdsmedBaseException;

	List<ProductComparisonResponse> compareProducts(List<Long> productIds, String langCode) throws IdsmedBaseException;

	List<ProductComparisonResponse> compareProductsV2(List<Long> productIds, String langCode) throws IdsmedBaseException;

	ProductTechFeatureResponse getListOfMedicalConsumablesLabel(String langCode) throws IdsmedBaseException;

	List<ProductWedoctorResponse> getProductDetailList(List<Long> productIds, String langCode) throws IdsmedBaseException;

	List<ProductHierarchyResponse> getProductHierarchyList() throws IdsmedBaseException;

	ProductListResponse resumeInactiveProduct(ProductInactiveRequest request) throws IdsmedBaseException;

	List<BigDecimal> getMinAndMaxProductPrice(String langCode) throws IdsmedBaseException;

	ProductTopRatingAndLatestApprovedResponse getTopRatingAndLatestApprovedProduct(String langCode, String encodedLoginId) throws IdsmedBaseException;

    List<ProductResponse> getAllApprovedProductHaveProductWishlist(String langCode, String encodedLoginId) throws IdsmedBaseException;

    ProductHierarchyLevelThreeResponse getThirdLevelProductHierarchy(String productHierarchyLevelFourCode, String langCode) throws IdsmedBaseException;

    List<KeywordHistoryResponse> getPopularSearchKeyword(Long numberOfKeyword)throws IdsmedBaseException;
}
package com.cmg.idsmed.service.product;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.servlet.http.HttpServletRequest;

import com.cmg.idsmed.common.Constant.IdsmedPermissionCodeConst;
import com.cmg.idsmed.common.Constant.IdsmedRoleCodeConst;
import com.cmg.idsmed.common.Constant.LoggerConstant;
import com.cmg.idsmed.common.Constant.PropertiesLabelConst;
import com.cmg.idsmed.common.enums.*;
import com.cmg.idsmed.dto.account.IdsmedAccountResponse;
import com.cmg.idsmed.dto.masterdata.CountryResponse;
import com.cmg.idsmed.dto.masterdata.ProvinceResponse;
import com.cmg.idsmed.dto.product.*;
import com.cmg.idsmed.dto.product.comparison.ProductCompareDataResponse;
import com.cmg.idsmed.dto.product.comparison.ProductComparisonResponse;
import com.cmg.idsmed.dto.product.wedoctor.ProductListWedoctorResponse;
import com.cmg.idsmed.dto.product.wedoctor.ProductWedoctorResponse;
import com.cmg.idsmed.dto.vendor.VendorSetStatusRequest;
import com.cmg.idsmed.mail.*;
import com.cmg.idsmed.model.entity.auth.IdsmedRole;
import com.cmg.idsmed.model.entity.masterdata.Country;
import com.cmg.idsmed.model.entity.masterdata.Province;
import com.cmg.idsmed.model.entity.product.*;
import com.cmg.idsmed.model.entity.product.list.BestSellerProductGlobal;
import com.cmg.idsmed.model.entity.product.list.HistoryUserProductBySearch;
import com.cmg.idsmed.model.entity.product.log.*;
import com.cmg.idsmed.model.repo.auth.IdsmedRoleRepository;
import com.cmg.idsmed.model.repo.masterdata.CountryRepository;
import com.cmg.idsmed.model.repo.masterdata.ProvinceRepository;
import com.cmg.idsmed.model.repo.product.*;
import com.cmg.idsmed.model.repo.product.list.BestSellerProductGlobalRepository;
import com.cmg.idsmed.model.repo.product.log.*;
import com.cmg.idsmed.service.share.*;
import com.cmg.idsmed.service.task.IdsmedTaskService;
import com.cmg.idsmed.service.user.IdsmedUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.util.Pair;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.cmg.idsmed.common.exception.ErrorInfo;
import com.cmg.idsmed.common.exception.IdsmedBaseException;
import com.cmg.idsmed.common.utils.DateTimeHelper;
import com.cmg.idsmed.common.utils.LangHelper;
import com.cmg.idsmed.common.utils.NumberHelper;
import com.cmg.idsmed.config.rabbitmq.IdsmedQueueConfig;
import com.cmg.idsmed.dto.product.details.ProductMarketingResponse;
import com.cmg.idsmed.dto.product.details.ProductSellingHistoryResponse;
import com.cmg.idsmed.dto.product.details.ProductSellingRateResponse;
import com.cmg.idsmed.dto.product.details.RelatedProductResponse;
import com.cmg.idsmed.dto.product.search.KeywordHistoryResponse;
import com.cmg.idsmed.model.entity.auth.IdsmedAccount;
import com.cmg.idsmed.model.entity.auth.IdsmedUser;
import com.cmg.idsmed.model.entity.masterdata.FavouriteProductBrand;
import com.cmg.idsmed.model.entity.masterdata.FavouriteProductCareArea;
import com.cmg.idsmed.model.entity.product.ProductMedia.MEDIA_TYPE;
import com.cmg.idsmed.model.entity.product.config.ProductLabelConfig;
import com.cmg.idsmed.model.entity.product.details.ProductSellingHistory;
import com.cmg.idsmed.model.entity.product.details.ProductSellingRate;
import com.cmg.idsmed.model.entity.product.details.RelatedProduct;
import com.cmg.idsmed.model.entity.product.search.KeywordHistory;
import com.cmg.idsmed.model.entity.vendor.Vendor;
import com.cmg.idsmed.model.repo.auth.IdsmedAccountRepository;
import com.cmg.idsmed.model.repo.masterdata.FavouriteProductBrandRepository;
import com.cmg.idsmed.model.repo.masterdata.FavouriteProductCareAreaRepository;
import com.cmg.idsmed.model.repo.product.config.ProductLabelConfigurationRepository;
import com.cmg.idsmed.model.repo.product.details.ProductSellingHistoryRepository;
import com.cmg.idsmed.model.repo.product.details.ProductSellingRateRepository;
import com.cmg.idsmed.model.repo.product.details.RelatedProductRepository;
import com.cmg.idsmed.model.repo.product.list.HistoryUserProductRepository;
import com.cmg.idsmed.model.repo.product.search.KeywordHistoryRepository;
import com.cmg.idsmed.model.repo.user.IdsmedUserRepository;
import com.cmg.idsmed.model.repo.vendor.VendorRepository;
import com.cmg.idsmed.service.auth.AuthenticationService;
import com.cmg.idsmed.service.email.EmailNotificationConfigService;
import com.cmg.idsmed.service.masterdata.ProductBrandService;

@Configuration
@Service
public class ProductServiceImpl implements ProductService {

    private static final Logger logger = LoggerFactory.getLogger(ProductServiceImpl.class);

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private ProductMediaRepository productMediaRepository;

    @Autowired
    private ProductFeatureRepository productFeatureRepository;

    @Autowired
    private ProductTechnicalRepository productTechnicalRepository;

    @Autowired
    private ProductCategoryRepository productCategoryRepository;

    @Autowired
    private ProductSecondCategoryRepository productSecondCategoryRepository;

    @Autowired
    private ProductSecondCategoryDetailRepository productSecondCategoryDetailRepository;

    @Autowired
    private MessageResourceService messageResourceService;

    @Autowired
    private ProductBrandRepository productBrandRepository;

    @Autowired
    private ProductCareAreaRepository productCareAreaRepository;

    @Autowired
    private ProductCareAreaDetailRepository productCareAreaDetailRepository;

    @Autowired
    private ProductRejectRepository productRejectRepository;

    @Autowired
    private FileUploadServiceImpl fileUploadService;

    @Autowired
    private ProductBrandService productBrandService;

    @Autowired
    private Environment env;

    @Autowired
    private VendorRepository vendorRepository;

    @Autowired
    private ProductSellingRateRepository productSellingRateRepository;

    @Autowired
    private RelatedProductRepository relatedProductRepository;

    @Autowired
    private ProductSellingHistoryRepository productSellingHistoryRepository;

    @Autowired
    private KeywordHistoryRepository keywordHistoryRepository;

    @Autowired
    private FavouriteProductBrandRepository favouriteProductBrandRepository;

    @Autowired
    private FavouriteProductCareAreaRepository favouriteProductCareAreaRepository;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private HttpServletRequest httpServletRequest;

    @Autowired
    private WebServiceLogService webServiceLogService;

    @Autowired
    private ProductLabelConfigurationRepository productLabelConfigurationRepository;

    @Autowired
    private ProductDocumentAttachmentRepository productDocumentAttachmentRepository;

    @Autowired
    private IdsmedAccountRepository idsmedAccountRepository;

    @Autowired
    private IdsmedUserRepository idsmedUserRepository;

    @Autowired
    private NotificationService notificationService;

    @Autowired
    private EmailService emailService;

    @Autowired
    private ProductExcelService productExcelService;

    @Autowired
    private EmailNotificationConfigService emailNotificationConfigService;

    @Autowired
    private AuthenticationService authenticationService;

    @Autowired
    private HistoryUserProductRepository historyUserProductRepository;

    @Autowired
    private ProductRatingRepository productRatingRepository;

    @Autowired
    private ProductRatingDetailRepository productRatingDetailRepository;

    @Autowired
    private ProductWishlistRepository productWishlistRepository;

    @Autowired
    private ProductWishlistDetailRepository productWishlistDetailRepository;

    @Autowired
    private ProductDocumentAttachmentReminderRepository productDocumentAttachmentReminderRepository;

    @Autowired
    private ProductTechnicalFeatureLabelRepository productTechnicalFeatureLabelRepository;

    @Autowired
    private IdsmedTaskService taskService;

    @Autowired
    private ProductLogRepository productLogRepository;

    @Autowired
    private ProductMediaLogRepository productMediaLogRepository;

    @Autowired
    private ProductTechnicalLogRepository productTechnicalLogRepository;

    @Autowired
    private ProductFeatureLogRepository productFeatureLogRepository;

    @Autowired
    private ProductRejectLogRepository productRejectLogRepository;

    @Autowired
    private ProductCareAreaDetailLogRepository productCareAreaDetailLogRepository;

    @Autowired
    private CountryRepository countryRepository;

    @Autowired
    private ProvinceRepository provinceRepository;

    @Autowired
    private IdsmedUserService userService;

    @Autowired
    private IdsmedRoleRepository idsmedRoleRepository;

    @Autowired
    private ProductHierarchyRepository productHierarchyRepository;

    @Autowired
    private BestSellerProductGlobalRepository bestSellerProductGlobalRepository;

    private static final String PRODUCT_FILE_UPLOAD_BASE_URL_NAME = "image.product.file.base.url";

    private static final String STORAGE_TYPE_PROPERTY_NAME = "storage.type";

    private static final String SERVER_STORAGE_TYPE = "server";

    @SuppressWarnings("unused")
    private static final String OSS_STORAGE_TYPE = "oss";

    private static final Integer PAGE_INDEX_DEFAULT = 0;
    private static final Integer PAGE_SIZE_DEFAULT = 100;

    @SuppressWarnings("unused")
    public static final Integer PAGE_INDEX_DEFAULT_FOR_NEW_API = 19;

    private static final Integer BEST_SELLER_TOP_NUMBER = 5;
    private static final Integer NUMBER_OF_LAST_KEYWORD = 5;

    private static final Integer PRODUCT_COMPARISON_MIN_NUMBER = 2;
    private static final Integer PRODUCT_COMPARISON_MAX_NUMBER = 3;

    @Value("${logger.start.method.description}")
    String START_METHOD_DESCRIPTION;

    @Value("${logger.end.method.description}")
    String END_METHOD_DESCRIPTION;
    private static final String GET_PRODUCT_LIST_FOR_WEDOCTOR_METHOD_NAME = "getProductListForWedoctor";
    private static final String GET_PRODUCT_DETAIL_LIST_METHOD_NAME = "getProductDetailList";
    private static final String GET_PRODUCT_HIERARCHY_LIST_METHOD_NAME = "getProductHierarchyList";
    private static final String GET_PRODUCT_LIST_FOR_VENDOR_METHOD_NAME = "getProductListForVendor";
    private static final String GET_PRODUCT_BY_ID_METHOD_NAME = "getProductById";
    private static final String GET_PRODUCT_BY_ID_FOR_ANONYMOUS_METHOD_NAME = "getProductByIdForAnonymous";
    private static final String GET_PRODUCT_BY_ID_FOR_WEDOCTOR_METHOD_NAME = "getProductByIdForWedoctor";
    private static final String SEARCH_PRODUCT_BY_KEYWORD_METHOD_NAME = "searchProductByKeyword";
    private static final String SEARCH_PRODUCT_BY_KEYWORD_FOR_ANONYMOUS_METHOD_NAME = "searchProductByKeywordForAnonymous";
    private static final String SEARCH_PRODUCT_BY_KEYWORD_FOR_WEDOCTOR_METHOD_NAME = "searchProductByKeywordForWedoctor";
    private static final String SEARCH_PRODUCT_BY_USER_FAVOURITE_METHOD_NAME = "searchProductByUserFavourite";
    private static final String GET_PRODUCT_ADD_INFO_DETAIL_METHOD_NAME = "getProductAddInfoDetail";
    private static final String GET_PRODUCT_LIST_SEARCH_FIELDS_METHOD_NAME = "getProductListSearchFields";
    private static final String CREATE_PRODUCT_METHOD_NAME = "createProduct";
    private static final String UPDATE_PENDING_APPROVED_STATUS_METHOD_NAME = "updatePendingApproveStatus";
    private static final String UPDATE_APPROVED_STATUS_METHOD_NAME = "updateApproveStatus";
    private static final String UPDATE_REJECT_STATUS_METHOD_NAME = "updateRejectStatus";
    private static final String UPDATE_DRAFT_OF_PENDING_METHOD_NAME = "updateDraftOfPendingStatus";
    private static final String GET_LAST_PRODUCT_SEARCH_KEYWORD_METHOD_NAME = "getLastProductSearchKeyword";
    private static final String GET_PRODUCT_LIST_FILTER_METHOD_NAME = "getProductListFilter";
    private static final String GET_PRODUCT_SEARCH_BY_STR_METHOD_NAME = "getProductSearchByStr";
    private static final String GET_PRODUCT_LIST_OF_NEW_RECOMMEND_METHOD_NAME = "getProductListOfNewRecommend";
    private static final String SEARCH_SIMPLE_PRODUCT_BY_KEY_METHOD_NAME = "searchSimpleProductByKey";
    private static final String GET_SIMPLE_PRODUCT_DETAIL_BY_ID_METHOD_NAME = "getSimpleProductDetailById";
    private static final String EDIT_PRODUCT_DOCUMENT_ATTACHMENT_METHOD_NAME = "editProductDocumentAttachment";
    private static final String INACTIVE_PRODUCT_METHOD_NAME = "inactiveProduct";
    private static final String PRODUCT_RATING_METHOD_NAME = "productRating";
    private static final String GET_PRODUCT_RATING_COMMENT_LIST_METHOD_NAME = "getProductRatingCommentList";
    private static final String UPDATE_WISHLIST_IN_PRODUCT_DETAIL_METHOD_NAME = "updateWishlistInProductDetail";
    private static final String UPDATE_WISHLIST_IN_PRODUCT_DETAIL_FOR_WEDOCTOR_METHOD_NAME = "updateWishlistInProductDetailForWedoctor";
    private static final String GET_FAVOURITE_PRODUCT_BY_ID_METHOD_NAME = "getFavouriteProductById";
    private static final String GET_LIST_OF_PRODUCT_TECHNICAL_AND_FEATURE_METHOD_NAME = "getListOfProductTechnicalAndFeature";
    private static final String GET_SUGGESTED_PRODUCTS_METHOD_NAME = "getSuggestedProducts";
    private static final String GET_RECENTLY_SEARCH_PRODUCT_METHOD_NAME = "getRecentlySearchProduct";
    private static final String COMPARE_PRODUCTS_METHOD_NAME = "compareProducts";
    private static final String GET_LIST_OF_MEDICAL_CONSUMABLES_LABEL_METHOD_NAME = "getListOfMedicalConsumablesLabel";
    private static final String RESUME_INACTIVE_PRODUCT_METHOD_NAME = "resumeInactiveProduct";
    private static final String GET_MIN_AND_MAX_PRODUCT_PRICE_METHOD_NAME = "getMinAndMaxProductPrice";
    private static final String GET_TOP_RATING_AND_LATEST_APPROVED_PRODUCT_METHOD_NAME = "getTopRatingAndLatestApprovedProduct";
    private static final String GET_ALL_APPROVED_PRODUCT_HAVE_PRODUCT_WISHLIST_METHOD_NAME = "getAllApprovedProductHaveProductWishlist";

    @Override
    public ProductListWedoctorResponse getProductListForWedoctor(Integer pageIndex, Integer pageSize, String langCode) throws IdsmedBaseException {
        //TODO need to implement full pagination when have time, now this fake pagination
        loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, GET_PRODUCT_LIST_FOR_WEDOCTOR_METHOD_NAME, null);
        pageIndex = pageIndex == null ? PAGE_INDEX_DEFAULT : pageIndex - 1;
        pageSize = pageSize == null ? PAGE_SIZE_DEFAULT : pageSize;
        List<Product> products = productRepository
                .findByStatusOrderByProductNamePrimaryAsc(StatusEnum.APPROVED.getCode());
        ProductListWedoctorResponse productListWedoctorResponse = new ProductListWedoctorResponse(pageIndex, pageSize);
        if (CollectionUtils.isEmpty(products)) {
            webServiceLogService.StoreInWebServiceLogDatabase(httpServletRequest, MethodNameEnum.GETPRODUCTLIST.getName(), HTTPServletReturnStatusEnum.AUTHORIZED.getCode());
            return productListWedoctorResponse;
        }

        int productCount = CollectionUtils.isEmpty(products) ? 0 : products.size();

        Integer pageCount = pageSize >= productCount ? 1
                : (productCount % pageSize > 0 ? productCount / pageSize + 1 : productCount / pageSize);
        Integer pageStartIndex = pageIndex * pageSize >= productCount ? (pageCount - 1) * pageSize
                : pageIndex * pageSize;
        Integer pageLastIndex = (pageIndex + 1) * pageSize >= productCount ? productCount : (pageIndex + 1) * pageSize;

        List<ProductWedoctorResponse> productWedoctorResponses = generateProductWedoctorResponseList(products, pageStartIndex, pageLastIndex);
        productListWedoctorResponse = new ProductListWedoctorResponse(productCount, pageIndex, pageSize,
                productWedoctorResponses);

        webServiceLogService.StoreInWebServiceLogDatabase(httpServletRequest, MethodNameEnum.GETPRODUCTLIST.getName(), HTTPServletReturnStatusEnum.AUTHORIZED.getCode());
        loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, GET_PRODUCT_LIST_FOR_WEDOCTOR_METHOD_NAME, null);

        return productListWedoctorResponse;
    }

    public List<ProductWedoctorResponse> getProductDetailList(List<Long> productIds, String langCode) throws IdsmedBaseException {
        loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, GET_PRODUCT_DETAIL_LIST_METHOD_NAME, null);
        String ossBucketDomain = env.getProperty(PropertiesLabelConst.OSS_BUCKET_DOMAIN);
        if (CollectionUtils.isEmpty(productIds)) {
            return new ArrayList<>();
        }
        List<Product> products = productRepository.findByIdsAndStatus(productIds, StatusEnum.APPROVED.getCode());
        List<ProductWedoctorResponse> productWedoctorResponses = products.stream().map(p -> {
            String companyPrimaryName = p.getVendor().getCompanyNamePrimary();

            sortProductFeatures(p.getProductFeaturesList());
            sortProductTechnicals(p.getProductTechnicalList());

            ProductWedoctorResponse ps = new ProductWedoctorResponse(p, ossBucketDomain);
            ps.setCompanyPrimaryName(StringUtils.isEmpty(companyPrimaryName) ? "" : companyPrimaryName);
            return ps;
        }).collect(Collectors.toList());
        loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, GET_PRODUCT_DETAIL_LIST_METHOD_NAME, null);
        return productWedoctorResponses;
    }

    @Override
    public List<ProductHierarchyResponse> getProductHierarchyList() throws IdsmedBaseException {
        loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, GET_PRODUCT_HIERARCHY_LIST_METHOD_NAME, null);
        List<ProductHierarchyLevelFour> productHierarchies = productHierarchyRepository.findAll();
        List<ProductHierarchyResponse> productHierarchyList = productHierarchies.stream().map(ph -> {
            ProductHierarchyResponse productHierarchyResponse = new ProductHierarchyResponse(ph);
            return productHierarchyResponse;
        }).collect(Collectors.toList());
        loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, GET_PRODUCT_HIERARCHY_LIST_METHOD_NAME, null);
        return productHierarchyList;
    }

    @SuppressWarnings("deprecation")
    @Override
//    @PreAuthorize("@PermissionService.isMethodPermited(authentication, {'PP0000001'})")
    public ProductListResponse getProductListForVendor(Long vendorId, String productNamePrimary, Integer status,
                                                       String productCode, String productModel, String companyNamePrimary, Integer vendorStatus,
                                                       Long vendorCoordinatorId, Integer pageIndex, Integer pageSize)
            throws IdsmedBaseException {
        loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, GET_PRODUCT_LIST_FOR_VENDOR_METHOD_NAME, null);

        String decodeProductNamePrimary = null;
        if (!StringUtils.isEmpty(productNamePrimary)) {
            decodeProductNamePrimary = URLDecoder.decode(productNamePrimary);
        }

        String decodeProductCode = null;
        if (!StringUtils.isEmpty(productCode)) {
            decodeProductCode = URLDecoder.decode(productCode);
        }

        String decodeProductModel = null;
        if (!StringUtils.isEmpty(productModel)) {
            decodeProductModel = URLDecoder.decode(productModel);
        }

        String decodeCompanyNamePrimary = null;
        if (!StringUtils.isEmpty(companyNamePrimary)) {
            decodeCompanyNamePrimary = URLDecoder.decode(companyNamePrimary);
        }

        Optional<Vendor> vendor = Optional.empty();
        if (StringUtils.isEmpty(vendorId) == false) {
            vendor = vendorRepository.findById(vendorId);
        }

        Pair<Integer, List<Product>> products = productRepository.searchProductByCustomCondition(vendor,
                decodeProductNamePrimary, status, decodeProductCode, decodeProductModel, decodeCompanyNamePrimary, vendorStatus, vendorCoordinatorId, pageIndex, pageSize);

        ProductListResponse productListResponse = new ProductListResponse(pageIndex, pageSize);
        if (CollectionUtils.isEmpty(products.getSecond())) {
            return productListResponse;
        }
        int productCount = products.getFirst();
        String ossBucketDomain = env.getProperty(PropertiesLabelConst.OSS_BUCKET_DOMAIN);
        List<ProductResponse> productResponses = products.getSecond().stream().map(p -> {
            sortProductFeatures(p.getProductFeaturesList());
            sortProductTechnicals(p.getProductTechnicalList());
            ProductResponse ps = new ProductResponse(p, ossBucketDomain);
            Vendor vendors = p.getVendor();
            if(vendors.getVendorCoordinator() != null) {
                Optional<IdsmedAccount> idsmedAccountOpt = idsmedAccountRepository.findById(vendors.getVendorCoordinator());
                IdsmedAccount vendorCoordinator = idsmedAccountOpt.get();
                IdsmedUser vendorCoordinatorUser = vendorCoordinator.getIdsmedUser();
                IdsmedAccountResponse idsmedAccountResponse = new IdsmedAccountResponse(vendorCoordinator, vendorCoordinatorUser);
                ps.setIdsmedAccount(idsmedAccountResponse);
            }
            return ps;
        }).collect(Collectors.toList());

        if (pageIndex != null && pageSize != null) {
            productListResponse = new ProductListResponse(productCount, pageIndex, pageSize, productResponses);
        } else {
            productListResponse = new ProductListResponse(productCount, productResponses);
        }
        loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, GET_PRODUCT_LIST_FOR_VENDOR_METHOD_NAME, null);

        return productListResponse;
    }

    @Override
    @PreAuthorize("@PermissionService.isMethodPermited(authentication, {'PP0000001', 'PP0000003', 'PP0000004'" +
            ", 'PP0000005', 'PP0000006', 'PP0000007', 'API0000002', 'PS10000001', 'PS20000001', 'PS30000001'})")
    public ProductResponse getProductById(Long id, String encodedLoginId, String langCode) throws IdsmedBaseException {
        loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, GET_PRODUCT_BY_ID_METHOD_NAME, null);
        String loginId = null;
        if (!StringUtils.isEmpty(encodedLoginId)) {
            loginId = URLDecoder.decode(encodedLoginId);
        }
        Optional<Product> productOpt = productRepository.findById(id);

        //Save to web_service_log
        webServiceLogService.StoreInWebServiceLogDatabase(httpServletRequest, MethodNameEnum.GETPRODUCTDETAIL.getName(), HTTPServletReturnStatusEnum.AUTHORIZED.getCode());

        if (!productOpt.isPresent()) {
            throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.PRODUCT_NOT_FOUND_ERROR, LangHelper.getLangCode(langCode)));
        }

        Product product = productOpt.get();

        product = sortProductComponenents(product);

        // Check is product in user wish list or not
        IdsmedAccount idsmedAccount = idsmedAccountRepository.findByLoginId(loginId);
        ProductWishlistDetail productWishlistDetail = productWishlistDetailRepository.findByProductAndAccount(product, idsmedAccount);
        IdsmedUser user = idsmedUserRepository.findIdsmedUserByIdsmedAccounts(idsmedAccount);

        ProductWishlistResponse wishlistResponse = generateProductWishlistResponse(product, productWishlistDetail != null);

        if (idsmedAccount != null) {
            // update product search frequency by login user (use account's user id instead of login id)
            processProductSearchFrequencyByAccountUser(idsmedAccount.getIdsmedUser().getId(), product);
        }

        // get country by country code given by product request and set the country into countryResponse
        CountryResponse manufacturerCountryResponse = generateCountryResponseByCountryCode(product, langCode);

        // get province by province code given by product request and set the province into provinceResponse
        ProvinceResponse manufacturerProvinceResponse = generateProvinceResponseByProvinceCode(product, langCode);
        ProductResponse productResponse = null;
        String ossBucketDomain = env.getProperty(PropertiesLabelConst.OSS_BUCKET_DOMAIN);
        if (idsmedAccount != null) {
            productResponse = new ProductResponse(product, wishlistResponse, ossBucketDomain);
        } else {
            productResponse = new ProductResponse(product, ossBucketDomain);
        }
        productResponse.setCompanyPrimaryName(product.getVendor().getCompanyNamePrimary());
        if (!CollectionUtils.isEmpty(product.getProductRejectList())) {
            ProductReject pr = Collections.max(product.getProductRejectList(),
                    Comparator.comparing(ProductReject::getId));
            productResponse.setRejectReason(pr.getRemarks());
        }
        if (user != null) {
            productResponse.setProductMarketingResponse(getProductMarkertingDetails(user.getId(), product.getId()));
        }

        productResponse.setManufacturerCountry(manufacturerCountryResponse);
        productResponse.setManufacturerProvince(manufacturerProvinceResponse);
        loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, GET_PRODUCT_BY_ID_METHOD_NAME, null);
        return productResponse;
    }

    @Override
    public ProductResponse getProductByIdForAnonymous(Long id, String langCode) throws IdsmedBaseException {
        loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, GET_PRODUCT_BY_ID_FOR_ANONYMOUS_METHOD_NAME, null);
        Optional<Product> productOpt = productRepository.findById(id);

        //Save to web_service_log
        webServiceLogService.StoreInWebServiceLogDatabase(httpServletRequest, MethodNameEnum.GETPRODUCTDETAIL.getName(), HTTPServletReturnStatusEnum.AUTHORIZED.getCode());

        if (!productOpt.isPresent()) {
            throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.PRODUCT_NOT_FOUND_ERROR, LangHelper.getLangCode(langCode)));
        }

        Product product = productOpt.get();

        product = sortProductComponenents(product);

//        // Check is product in user wish list or not
//        IdsmedAccount idsmedAccount = idsmedAccountRepository.findByLoginId(loginId);
//        ProductWishlistDetail productWishlistDetail = productWishlistDetailRepository.findByProductAndAccount(product, idsmedAccount);
//        IdsmedUser user = idsmedUserRepository.findIdsmedUserByIdsmedAccounts(idsmedAccount);

//        ProductWishlistResponse wishlistResponse = generateProductWishlistResponse(product, productWishlistDetail != null);
//
//        if (idsmedAccount != null) {
//            // update product search frequency by login user (use account's user id instead of login id)
//            processProductSearchFrequencyByAccountUser(idsmedAccount.getIdsmedUser().getId(), product);
//        }

        // get country by country code given by product request and set the country into countryResponse
        CountryResponse manufacturerCountryResponse = generateCountryResponseByCountryCode(product, langCode);

        // get province by province code given by product request and set the province into provinceResponse
        ProvinceResponse manufacturerProvinceResponse = generateProvinceResponseByProvinceCode(product, langCode);
        ProductResponse productResponse = null;
        String ossBucketDomain = env.getProperty(PropertiesLabelConst.OSS_BUCKET_DOMAIN);
        productResponse = new ProductResponse(product, ossBucketDomain);

        productResponse.setCompanyPrimaryName(product.getVendor().getCompanyNamePrimary());
        if (!CollectionUtils.isEmpty(product.getProductRejectList())) {
            ProductReject pr = Collections.max(product.getProductRejectList(),
                    Comparator.comparing(ProductReject::getId));
            productResponse.setRejectReason(pr.getRemarks());
        }
//        if (user != null) {
//            productResponse.setProductMarketingResponse(getProductMarkertingDetails(user.getId(), product.getId()));
//        }

        productResponse.setManufacturerCountry(manufacturerCountryResponse);
        productResponse.setManufacturerProvince(manufacturerProvinceResponse);

        ProductHierarchyResponse productHierarchyResponse = null;

        if(product.getProductHierarchyLevelFour() != null){
            ProductHierarchyLevelFour productHierarchyLevelFour = product.getProductHierarchyLevelFour();
            productHierarchyResponse = new ProductHierarchyResponse(productHierarchyLevelFour);
        }
        productResponse.setProductHierarchy(productHierarchyResponse);

        loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, GET_PRODUCT_BY_ID_FOR_ANONYMOUS_METHOD_NAME, null);

        return productResponse;
    }

    // Update searched frequency of a particular product by user
    private void processProductSearchFrequencyByAccountUser(Long userId, Product product) throws IdsmedBaseException {
        HistoryUserProductBySearch productBySearch = historyUserProductRepository.findFirstByUserIdAndProductId(userId, product.getId());

        if (productBySearch != null) {
            Integer frequency = productBySearch.getFrequency() + 1;
            productBySearch.setFrequency(frequency);
            historyUserProductRepository.save(productBySearch);
        } else {
            HistoryUserProductBySearch historyUserProductBySearch = new HistoryUserProductBySearch(userId, null, null, product, 1);
            historyUserProductRepository.save(historyUserProductBySearch);
        }
    }


    @Override
    @PreAuthorize("@PermissionService.isMethodPermited(authentication, {'PP0000001', 'PP0000003', 'PP0000004'" +
            ", 'PP0000005', 'PP0000006', 'PP0000007', 'API0000002', 'PS10000001', 'PS20000001', 'PS30000001'})")
    public ProductWedoctorResponse getProductByIdForWedoctor(Long id, Long weDoctorCustomerId, String langCode) throws IdsmedBaseException {
        loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, GET_PRODUCT_BY_ID_FOR_WEDOCTOR_METHOD_NAME, null);
        Optional<Product> productOpt = productRepository.findById(id);
        //Save to web_service_log
        webServiceLogService.StoreInWebServiceLogDatabase(httpServletRequest, MethodNameEnum.GETPRODUCTDETAIL.getName(), HTTPServletReturnStatusEnum.AUTHORIZED.getCode());
        String ossBucketDomain = env.getProperty(PropertiesLabelConst.OSS_BUCKET_DOMAIN);

        if (!productOpt.isPresent()) {
            throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.PRODUCT_NOT_FOUND_ERROR, LangHelper.getLangCode(langCode)));
        }

        Product product = productOpt.get();

        product = sortProductComponenents(product);

        // Check this product in wedoctor customer wishlist or not
        ProductWishlistDetail productWishlistDetail = productWishlistDetailRepository.findFirstByProductAndWedoctorCustomerId(product, weDoctorCustomerId);

        ProductWishlistResponse wishlistResponse = generateProductWishlistResponse(product, productWishlistDetail != null);

        // update product search frequency by wedoctor customer
        processProductSearchFrequencyByCustomer(weDoctorCustomerId, product);

        // get country by country code given by product request and set the country into countryResponse
        CountryResponse manufacturerCountryResponse = generateCountryResponseByCountryCode(product, langCode);

        // get province by province code given by product request and set the province into provinceResponse
        ProvinceResponse manufacturerProvinceResponse = generateProvinceResponseByProvinceCode(product, langCode);

        ProductWedoctorResponse productWedoctorResponse = new ProductWedoctorResponse(product, wishlistResponse, ossBucketDomain);
        productWedoctorResponse.setCompanyPrimaryName(product.getVendor().getCompanyNamePrimary());
        if (!CollectionUtils.isEmpty(product.getProductRejectList())) {
            ProductReject pr = Collections.max(product.getProductRejectList(),
                    Comparator.comparing(ProductReject::getId));
            productWedoctorResponse.setRejectReason(pr.getRemarks());
        }
        productWedoctorResponse.setProductMarketingResponse(getProductMarkertingDetailsForWedoctor(weDoctorCustomerId, product.getId()));
        productWedoctorResponse.setManufacturerCountry(manufacturerCountryResponse);
        productWedoctorResponse.setManufacturerProvince(manufacturerProvinceResponse);

        loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, GET_PRODUCT_BY_ID_FOR_WEDOCTOR_METHOD_NAME, null);
        return productWedoctorResponse;
    }

    private Product sortProductComponenents(Product product) {
        if (!CollectionUtils.isEmpty(product.getProductFeaturesList())) {
            sortProductFeatures(product.getProductFeaturesList());
        }

        if (!CollectionUtils.isEmpty(product.getProductTechnicalList())) {
            sortProductTechnicals(product.getProductTechnicalList());
        }

        if (!CollectionUtils.isEmpty(product.getProductCareAreaList())) {
            sortProductCareAreaDetail(product.getProductCareAreaList());
        }
        return product;
    }

    private ProductWishlistResponse generateProductWishlistResponse(Product product, Boolean canGenerateWhishlistResponse) {
        ProductWishlistResponse wishlistResponse = null;
        if (canGenerateWhishlistResponse) {
            wishlistResponse = new ProductWishlistResponse(product, true);
        } else {
            wishlistResponse = new ProductWishlistResponse(product, false);
        }

        return wishlistResponse;
    }

    @SuppressWarnings("deprecation")
    @Override
    @PreAuthorize("@PermissionService.isMethodPermited(authentication, {'PP0000001'" +
            ", 'PP0000003', 'PP0000004', 'PP0000005', 'PP0000006', 'PP0000007', 'API0000003'" +
            ", 'PS10000001', 'PS20000001', 'PS30000001'})")
//    @PreAuthorize("@PermissionService.isMethodPermited(authentication, {'PP0000001', 'PP0000003', 'PP0000004', 'PP0000005', 'PP0000006', 'PP0000007', 'API0000003'})")
    public ProductListResponse searchProductByKeyword(String keyword
            , List<Long> careAreaIds
            , List<Long> categoryIds
            , List<Long> brandIds
            , String type
            , Integer pageIndex
            , Integer pageSize
            , Integer sortFieldCode
            , String sortOrder
            , String encodedLoginId
            , BigDecimal minPrice
            , BigDecimal maxPrice
            , Integer productType
            , String manufacturerCountryCode
            , String manufacturerProvinceCode
            , List<Long> secondCategoryIds
            , String langCode) throws IdsmedBaseException {
        loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, SEARCH_PRODUCT_BY_KEYWORD_METHOD_NAME, null);
        Integer status = StatusEnum.APPROVED.getCode();
        Long userId = null;
        String loginId = null;

        if (!StringUtils.isEmpty(encodedLoginId)) {
            loginId = URLDecoder.decode(encodedLoginId);
            userId = idsmedUserRepository.findUserByLoginIdAndStatus(loginId, status).getId();
        }

//        if (!StringUtils.isEmpty(type) && BEST_SELLER_SEARCH_TYPE.equals(type)) {
//            return new ProductListResponse(0, new ArrayList<ProductResponse>());
//        }

        //Decode keyword to avoid error when keyword has special symbol
        String decodeKeyword = null;
        if (!StringUtils.isEmpty(keyword)) {
            decodeKeyword = URLDecoder.decode(keyword);
        }

        //Get ProductCareArea list
        List<ProductCareArea> careAreas = new ArrayList<>();
        if (!CollectionUtils.isEmpty(careAreaIds)) {
            careAreaIds.forEach(id -> {
                productCareAreaRepository.findById(id).ifPresent(cdt -> {
                    careAreas.add(cdt);
                });

            });
        }

        //Get ProductCategory List
        List<ProductCategory> categories = new ArrayList<>();
        if (!CollectionUtils.isEmpty(categoryIds)) {
            categoryIds.forEach(id -> {
                productCategoryRepository.findById(id).ifPresent(ct -> {
                    categories.add(ct);
                });
            });
        }

        //Get Brand List
        List<ProductBrand> brands = new ArrayList<>();
        if (!CollectionUtils.isEmpty(brandIds)) {
            brandIds.forEach(id -> {
                productBrandRepository.findById(id).ifPresent(br -> {
                    brands.add(br);
                });
            });
        }

        //Check validation productType;
        if (productType != null && !productType.equals(ProductTypeEnum.DOMESTIC.getCode())
                && !productType.equals(ProductTypeEnum.IMPORTED.getCode())) {
            throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.PRODUCT_SEARCH_CRITERIA_NOT_VALID_ERROR, LangHelper.getLangCode(langCode)));
        }

        //Get ProductSecondCategory list
        List<ProductSecondCategory> secondCategories = new ArrayList<>();
        if (!CollectionUtils.isEmpty(secondCategoryIds)) {
            secondCategoryIds.forEach(id -> {
                productSecondCategoryRepository.findById(id).ifPresent(psc -> {
                    secondCategories.add(psc);
                });

            });
        }

        String sortField = null;
        if (sortFieldCode != null) {
            for (ProductCustomRepositoryImpl.FuzzySearchProductSortFieldEnum sfe : ProductCustomRepositoryImpl.FuzzySearchProductSortFieldEnum.values()) {
                if (sfe.getCode().equals(sortFieldCode)) {
                    sortField = sfe.getFieldName();
                    break;
                }
            }
        }

        Pair<Integer, List<Product>> pair = null;
        if (CollectionUtils.isEmpty(careAreas) && CollectionUtils.isEmpty(secondCategories)) {
            pair = productRepository.fuzzySearchProductV2(decodeKeyword, categories, brands
                    , pageIndex, pageSize, sortField, sortOrder, minPrice, maxPrice, productType, manufacturerCountryCode, manufacturerProvinceCode);
        } else {
            pair = productRepository.fuzzySearchAdvanceProductCareAreaSearchFieldV2(decodeKeyword, careAreas, categories, brands
                    , pageIndex, pageSize, sortField, sortOrder, minPrice, maxPrice, productType, manufacturerCountryCode, manufacturerProvinceCode, secondCategories);
        }
        //SAVE Searching Log to keyword_history.
        if (!StringUtils.isEmpty(decodeKeyword)) {
            logSearchingForSCP(userId, decodeKeyword, pair.getSecond(), careAreaIds, categoryIds, brandIds);
        }
        ProductListResponse productListResponse = new ProductListResponse(pageIndex, pageSize);

        if (CollectionUtils.isEmpty(pair.getSecond())) {
            return productListResponse;
        }

        List<ProductResponse> productResponses = new ArrayList<>();
        String ossBucketDomain = env.getProperty(PropertiesLabelConst.OSS_BUCKET_DOMAIN);
        if (!StringUtils.isEmpty(loginId)) {
            productResponses = generateProductResponseList(pair.getSecond(), loginId);
        } else {
            productResponses = pair.getSecond().stream().map(p -> new ProductResponse(p, ossBucketDomain)).collect(Collectors.toList());
        }

        if (pageIndex != null && pageSize != null) {
            productListResponse = new ProductListResponse(pair.getFirst(), pageIndex, pageSize, productResponses);
        } else {
            productListResponse = new ProductListResponse(pair.getFirst(), productResponses);
        }

        //Log for calling api
        webServiceLogService.StoreInWebServiceLogDatabase(httpServletRequest, MethodNameEnum.GETPRODUCTSEARCHBYKEY.getName(), HTTPServletReturnStatusEnum.AUTHORIZED.getCode());

        loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, SEARCH_PRODUCT_BY_KEYWORD_METHOD_NAME, null);

        return productListResponse;
    }

    @Override
    public ProductListResponse searchProductByKeywordForAnonymous(String keyword
            , List<Long> careAreaIds
            , List<Long> categoryIds
            , List<Long> brandIds
            , String type
            , Integer pageIndex
            , Integer pageSize
            , Integer sortFieldCode
            , String sortOrder
            , BigDecimal minPrice
            , BigDecimal maxPrice
            , Integer productType
            , String manufacturerCountryCode
            , String manufacturerProvinceCode
            , List<Long> secondCategoryIds
            , String langCode) throws IdsmedBaseException {
        loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, SEARCH_PRODUCT_BY_KEYWORD_FOR_ANONYMOUS_METHOD_NAME, null);
        long startTime = System.nanoTime();
        Integer status = StatusEnum.APPROVED.getCode();
//        if (!StringUtils.isEmpty(type) && BEST_SELLER_SEARCH_TYPE.equals(type)) {
//            return new ProductListResponse(0, new ArrayList<ProductResponse>());
//        }

        //Decode keyword to avoid error when keyword has special symbol
        String decodeKeyword = null;
        if (!StringUtils.isEmpty(keyword)) {
            decodeKeyword = URLDecoder.decode(keyword);
        }

        //Get ProductCareArea list
        List<ProductCareArea> careAreas = new ArrayList<>();
        if (!CollectionUtils.isEmpty(careAreaIds)) {
            careAreaIds.forEach(id -> {
                productCareAreaRepository.findById(id).ifPresent(cdt -> {
                    careAreas.add(cdt);
                });

            });
        }

        //Get ProductCategory List
        List<ProductCategory> categories = new ArrayList<>();
        if (!CollectionUtils.isEmpty(categoryIds)) {
            categoryIds.forEach(id -> {
                productCategoryRepository.findById(id).ifPresent(ct -> {
                    categories.add(ct);
                });
            });
        }

        //Get Brand List
        List<ProductBrand> brands = new ArrayList<>();
        if (!CollectionUtils.isEmpty(brandIds)) {
            brandIds.forEach(id -> {
                productBrandRepository.findById(id).ifPresent(br -> {
                    brands.add(br);
                });
            });
        }

        //Check validation productType;
        if (productType != null && !productType.equals(ProductTypeEnum.DOMESTIC.getCode())
                && !productType.equals(ProductTypeEnum.IMPORTED.getCode())) {
            throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.PRODUCT_SEARCH_CRITERIA_NOT_VALID_ERROR, LangHelper.getLangCode(langCode)));
        }

        //Get ProductSecondCategory list
        List<ProductSecondCategory> secondCategories = new ArrayList<>();
        if (!CollectionUtils.isEmpty(secondCategoryIds)) {
            secondCategoryIds.forEach(id -> {
                productSecondCategoryRepository.findById(id).ifPresent(psc -> {
                    secondCategories.add(psc);
                });

            });
        }

        String sortField = null;
        if (sortFieldCode != null) {
            for (ProductCustomRepositoryImpl.FuzzySearchProductSortFieldEnum sfe : ProductCustomRepositoryImpl.FuzzySearchProductSortFieldEnum.values()) {
                if (sfe.getCode().equals(sortFieldCode)) {
                    sortField = sfe.getFieldName();
                    break;
                }
            }
        }
        long endblock1Time = System.nanoTime();
        long elapsedTime1 = (endblock1Time -  endblock1Time)/1000000;
        logger.info("Executing time for block 1: {}", elapsedTime1);
        Pair<Integer, List<Product>> pair = null;
        if (CollectionUtils.isEmpty(careAreas) && CollectionUtils.isEmpty(secondCategories)) {
            pair = productRepository.fuzzySearchProductV2(decodeKeyword, categories, brands
                    , pageIndex, pageSize, sortField, sortOrder, minPrice, maxPrice, productType, manufacturerCountryCode, manufacturerProvinceCode);
        } else {
            pair = productRepository.fuzzySearchAdvanceProductCareAreaSearchFieldV2(decodeKeyword, careAreas, categories, brands
                    , pageIndex, pageSize, sortField, sortOrder, minPrice, maxPrice, productType, manufacturerCountryCode, manufacturerProvinceCode, secondCategories);
        }
        //SAVE Searching Log to keyword_history.
        if (!StringUtils.isEmpty(decodeKeyword)) {
            logSearchingForSCP(null, decodeKeyword, pair.getSecond(), careAreaIds, categoryIds, brandIds);
        }
        long endblock2Time = System.nanoTime();
        long elapsedTime2 = (endblock2Time -  endblock1Time)/1000000;
        logger.info("Executing time for block 2: {}", elapsedTime2);
        ProductListResponse productListResponse = new ProductListResponse(pageIndex, pageSize);

        if (CollectionUtils.isEmpty(pair.getSecond())) {
            return productListResponse;
        }
        List<ProductResponse> productResponses = new ArrayList<>();
        String ossBucketDomain = env.getProperty(PropertiesLabelConst.OSS_BUCKET_DOMAIN);
        productResponses = pair.getSecond().stream().map(p -> new ProductResponse(p, ossBucketDomain)).collect(Collectors.toList());

        if (pageIndex != null && pageSize != null) {
            productListResponse = new ProductListResponse(pair.getFirst(), pageIndex, pageSize, productResponses);
        } else {
            productListResponse = new ProductListResponse(pair.getFirst(), productResponses);
        }

        long endblock3Time = System.nanoTime();
        long elapsedTime3 = (endblock3Time -  endblock2Time)/1000000;
        logger.info("Executing time for block 3: {}", elapsedTime3);

        //Log for calling api
        webServiceLogService.StoreInWebServiceLogDatabase(httpServletRequest, MethodNameEnum.GETPRODUCTSEARCHBYKEY.getName(), HTTPServletReturnStatusEnum.AUTHORIZED.getCode());

        //SAVE Searching Log to keyword_history.
        if (!StringUtils.isEmpty(decodeKeyword)) {
            logSearchingForSCP(null, decodeKeyword, pair.getSecond(), careAreaIds, categoryIds, brandIds);
        }
        loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, SEARCH_PRODUCT_BY_KEYWORD_FOR_ANONYMOUS_METHOD_NAME, null);
        long endblock4Time = System.nanoTime();
        long elapsedTime4 = (endblock4Time -  endblock3Time)/1000000;
        logger.info("Executing time for block 4: {}", elapsedTime4);
        return productListResponse;
    }

    @SuppressWarnings("deprecation")
    @Override
    @PreAuthorize("@PermissionService.isMethodPermited(authentication, {'PP0000001'" +
            ", 'PP0000003', 'PP0000004', 'PP0000005', 'PP0000006', 'PP0000007', 'API0000003'" +
            ", 'PS10000001', 'PS20000001', 'PS30000001'})")
    public ProductListResponse searchProductByKeywordForWedoctor(String keyword
            , List<Long> careAreaIds
            , List<Long> categoryIds
            , List<Long> brandIds
            , String type
            , Integer pageIndex
            , Integer pageSize
            , Integer sortFieldCode
            , String sortOrder
            , Long wedoctorCustomerId
            , BigDecimal minPrice
            , BigDecimal maxPrice) throws IdsmedBaseException {
        loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, SEARCH_PRODUCT_BY_KEYWORD_FOR_WEDOCTOR_METHOD_NAME, null);
        Integer status = StatusEnum.APPROVED.getCode();
//        if (!StringUtils.isEmpty(type) && BEST_SELLER_SEARCH_TYPE.equals(type)) {
//            return new ProductListResponse(0, new ArrayList<ProductResponse>());
//        }
        //TODO: decodeKeyword = URLDecoder.decode(keyword,"UTF-8");
        String decodeKeyword = null;
        if (!StringUtils.isEmpty(keyword)) {
            decodeKeyword = URLDecoder.decode(keyword);
        }

        //Get ProductCareArea list
        List<ProductCareArea> careAreas = new ArrayList<>();
        if (!CollectionUtils.isEmpty(careAreaIds)) {
            careAreaIds.forEach(id -> {
                productCareAreaRepository.findById(id).ifPresent(cdt -> {
                    careAreas.add(cdt);
                });

            });
        }

        //Get ProductCategory List
        List<ProductCategory> categories = new ArrayList<>();
        if (!CollectionUtils.isEmpty(categoryIds)) {
            categoryIds.forEach(id -> {
                productCategoryRepository.findById(id).ifPresent(ct -> {
                    categories.add(ct);
                });
            });
        }

        //Get Brand List
        List<ProductBrand> brands = new ArrayList<>();
        if (!CollectionUtils.isEmpty(brandIds)) {
            brandIds.forEach(id -> {
                productBrandRepository.findById(id).ifPresent(br -> {
                    brands.add(br);
                });
            });
        }

        String sortField = null;
        if (sortFieldCode != null) {
            for (ProductCustomRepositoryImpl.FuzzySearchProductSortFieldEnum sfe : ProductCustomRepositoryImpl.FuzzySearchProductSortFieldEnum.values()) {
                if (sfe.getCode().equals(sortFieldCode)) {
                    sortField = sfe.getFieldName();
                    break;
                }
            }
        }

        Pair<Integer, List<Product>> pair = null;
        if (CollectionUtils.isEmpty(careAreas)) {
            pair = productRepository.fuzzySearchProductV2ForWedoctor(decodeKeyword, categories, brands, pageIndex, pageSize, sortField, sortOrder, minPrice, maxPrice);
        } else {
            pair = productRepository.fuzzySearchAdvanceProductCareAreaSearchFieldV2ForWedoctor(decodeKeyword, careAreas, categories, brands, pageIndex, pageSize, sortField, sortOrder, minPrice, maxPrice);
        }

        ProductListResponse productListResponse = new ProductListResponse(pageIndex, pageSize);

        webServiceLogService.StoreInWebServiceLogDatabase(httpServletRequest, MethodNameEnum.GETPRODUCTSEARCHBYKEY.getName(), HTTPServletReturnStatusEnum.AUTHORIZED.getCode());

        if (CollectionUtils.isEmpty(pair.getSecond())) {
            return productListResponse;
        }

        List<ProductResponse> productResponses = generateProductResponseListForWedoctor(pair.getSecond(), wedoctorCustomerId);

        if (pageIndex != null && pageSize != null) {
            productListResponse = new ProductListResponse(pair.getFirst(), pageIndex, pageSize, productResponses);
        } else {
            productListResponse = new ProductListResponse(pair.getFirst(), productResponses);
        }

        //SAVE Searching Log to keyword_history.
        if (!StringUtils.isEmpty(decodeKeyword)) {
            logSearchingForWeDoctor(wedoctorCustomerId, decodeKeyword, pair.getSecond(), careAreaIds, categoryIds, brandIds);
        }
        loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, SEARCH_PRODUCT_BY_KEYWORD_FOR_WEDOCTOR_METHOD_NAME, null);
        return productListResponse;
    }


    private void logSearchingForSCP(Long userId, String keyword, List<Product> products, List<Long> careAreaIds, List<Long> categoryIds, List<Long> brandIds) {
        List<Long> productIds = !CollectionUtils.isEmpty(products)
                ? products.stream().map(p -> p.getId()).collect(Collectors.toList()) : null;
        String stringCareAreaIds = !CollectionUtils.isEmpty(careAreaIds)
                ? careAreaIds.stream().map(caid -> String.valueOf(caid)).collect(Collectors.joining(",")) : null;

        String stringCategoryIds = !CollectionUtils.isEmpty(categoryIds)
                ? categoryIds.stream().map(cid -> String.valueOf(cid)).collect(Collectors.joining(",")) : null;

        String stringBrandIds = !CollectionUtils.isEmpty(brandIds) ? brandIds.stream().map(bid -> String.valueOf(bid)).collect(Collectors.joining(",")) : null;
        String stringProductIds = !CollectionUtils.isEmpty(productIds) ? productIds.stream().map(pid -> String.valueOf(pid)).collect(Collectors.joining(",")) : null;

        KeywordHistory keywordHistory = new KeywordHistory(userId, keyword, stringCareAreaIds
                , stringCategoryIds, stringBrandIds, stringProductIds, true);

        keywordHistoryRepository.save(keywordHistory);
    }

    @SuppressWarnings("deprecation")
    @Override
    @PreAuthorize("@PermissionService.isMethodPermited(authentication, {'PP0000001', 'PP0000003', 'PP0000004', 'PP0000005', 'PP0000006', 'PP0000007', 'API0000003'})")
    public ProductListResponse searchProductByUserFavourite(String keyword, Long wedoctorCustomerId, Integer pageIndex, Integer pageSize) throws IdsmedBaseException {
        loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, SEARCH_PRODUCT_BY_USER_FAVOURITE_METHOD_NAME, null);
        Pageable pageable = null;
        if (pageIndex != null && pageSize != null) {
            pageIndex = pageIndex - 1;
            pageable = PageRequest.of(pageIndex, pageSize);
        }

        List<FavouriteProductCareArea> fCareAreas = favouriteProductCareAreaRepository.findByWedoctorCustomerId(wedoctorCustomerId);
        List<Long> careAreaIds = new ArrayList<Long>();

        if (!CollectionUtils.isEmpty(fCareAreas)) {
            careAreaIds = fCareAreas.stream().map(ca -> ca.getCareAreaId()).collect(Collectors.toList());
        }

        List<FavouriteProductBrand> fProductBrands = favouriteProductBrandRepository.findByWedoctorCustomerId(wedoctorCustomerId);
        List<Long> productBrandIds = new ArrayList<Long>();
        if (!CollectionUtils.isEmpty(fProductBrands)) {
            productBrandIds = fProductBrands.stream().map(pb -> pb.getBrandId()).collect(Collectors.toList());
        }

        String decodeKeyword = null;
        if (!StringUtils.isEmpty(keyword)) {
            decodeKeyword = URLDecoder.decode(keyword);
        }

        Pair<Integer, List<Product>> pair = productRepository.searchByUserFavourite(decodeKeyword, careAreaIds, productBrandIds, pageable);
        List<ProductResponse> productResponses = generateProductResponseList(pair.getSecond());

        //Save to keyword_history
        if (!StringUtils.isEmpty(decodeKeyword)) {
            logSearchingForWeDoctor(wedoctorCustomerId, decodeKeyword, pair.getSecond(), careAreaIds, null, productBrandIds);
        }

        ProductListResponse productListResponse = null;
        if (pageIndex != null && pageSize != null) {
            productListResponse = new ProductListResponse(pair.getFirst(), pageIndex, pageSize, productResponses);
        } else {
            productListResponse = new ProductListResponse(pair.getFirst(), productResponses);
        }

        webServiceLogService.StoreInWebServiceLogDatabase(httpServletRequest, MethodNameEnum.GETPRODUCTSEARCHBYKEY.getName(), HTTPServletReturnStatusEnum.AUTHORIZED.getCode());
        loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, SEARCH_PRODUCT_BY_USER_FAVOURITE_METHOD_NAME, null);

        return productListResponse;
    }

    private void logSearchingForWeDoctor(Long weDoctorCustomerId, String keyword, List<Product> products, List<Long> careAreaIds, List<Long> categoryIds, List<Long> brandIds) {
        List<Long> productIds = !CollectionUtils.isEmpty(products)
                ? products.stream().map(p -> p.getId()).collect(Collectors.toList()) : null;
        String stringCareAreaIds = !CollectionUtils.isEmpty(careAreaIds)
                ? careAreaIds.stream().map(caid -> String.valueOf(caid)).collect(Collectors.joining(",")) : null;

        String stringCategoryIds = !CollectionUtils.isEmpty(categoryIds)
                ? categoryIds.stream().map(cid -> String.valueOf(cid)).collect(Collectors.joining(",")) : null;

        String stringBrandIds = !CollectionUtils.isEmpty(brandIds) ? brandIds.stream().map(bid -> String.valueOf(bid)).collect(Collectors.joining(",")) : null;
        String stringProductIds = !CollectionUtils.isEmpty(productIds) ? productIds.stream().map(pid -> String.valueOf(pid)).collect(Collectors.joining(",")) : null;

        KeywordHistory keywordHistory = new KeywordHistory(weDoctorCustomerId, keyword, stringCareAreaIds
                , stringCategoryIds, stringBrandIds, stringProductIds);

        keywordHistoryRepository.save(keywordHistory);
    }

    @Override
//    @PreAuthorize("@PermissionService.isMethodPermited(authentication,  {'PS10000001', 'PS20000001', 'PS30000001', 'PP0000001'})")
    public ProductAddInfoDetailResponse getProductAddInfoDetail(String langCode) throws IdsmedBaseException {
        loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, GET_PRODUCT_ADD_INFO_DETAIL_METHOD_NAME, null);
        String ossBucketDomain = env.getProperty(PropertiesLabelConst.OSS_BUCKET_DOMAIN);
        List<StatusEnum> status = new ArrayList<>();
        status.add(StatusEnum.APPROVED);
        List<ProductCategoryResponse> productCategory = productCategoryRepository
                .findAll(Sort.by("categoryName").ascending()).stream().map(p -> {
                    ProductCategoryResponse productCategoryResponse = new ProductCategoryResponse(p, langCode);
                    Long totalProducts = productRepository.countAllByProductCategory(p, status);
                    if (totalProducts != null) {
                        productCategoryResponse.setTotalProducts(totalProducts);
                    }
                    return productCategoryResponse;
                })
                .collect(Collectors.toList());

        List<ProductBrandResponse> productBrand = productBrandRepository.findAll(Sort.by("brandName").ascending())
                .stream().map(p -> new ProductBrandResponse(p, ossBucketDomain)).collect(Collectors.toList());

        List<ProductCareAreaResponse> productCareArea = productCareAreaRepository
                .findAll(Sort.by("careAreaName").ascending()).stream().map(p -> {
                    Long totalProducts = productRepository.countAllByProductCareArea(p, status);
                    ProductCareAreaResponse productCareAreaResponse = new ProductCareAreaResponse(p, langCode, ossBucketDomain);
                    if (totalProducts != null) {
                        productCareAreaResponse.setTotalProducts(totalProducts);
                    }
                    return productCareAreaResponse;
                }).collect(Collectors.toList());

        List<ProductSecondCategoryResponse> productSecondCategory = productSecondCategoryRepository
                .findAll(Sort.by("categoryName").ascending())
                .stream()
                .map(psc -> {
                    ProductSecondCategoryResponse productSecondCategoryResponse = new ProductSecondCategoryResponse(psc, langCode);
                    Long totalProducts = productRepository.countAllByProductSecondCategory(psc, status);
                    if (totalProducts != null) {
                        productSecondCategoryResponse.setTotalProducts(totalProducts);
                    }

                    return productSecondCategoryResponse;
                }).collect(Collectors.toList());

        loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, GET_PRODUCT_ADD_INFO_DETAIL_METHOD_NAME, null);
        return new ProductAddInfoDetailResponse(productCategory, productBrand, productCareArea, productSecondCategory);

    }

    public ProductListSearchFieldsResponse getProductListSearchFields(List<Long> vendorIds) throws IdsmedBaseException {
        loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, GET_PRODUCT_LIST_SEARCH_FIELDS_METHOD_NAME, null);
        List<Vendor> vendors = new ArrayList<>();
        if (!CollectionUtils.isEmpty(vendorIds)) {
            vendors = vendorIds.stream().map(id -> vendorRepository.getOne(id)).collect(Collectors.toList());
        }
        List<String> productModels = new ArrayList<>();

        List<String> vendorNames = new ArrayList<>();
        if (!CollectionUtils.isEmpty(vendors)) {
            vendorNames = vendors.stream().map(v -> v.getCompanyNamePrimary()).collect(Collectors.toList());
            Collections.sort(vendorNames);
        } else {
            vendorNames = vendorRepository.findAllVendorNames();
        }

        productModels = productRepository.findAllProductModelByVendors(vendors);

        ProductListSearchFieldsResponse response = new ProductListSearchFieldsResponse(productModels, vendorNames);
        loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, GET_PRODUCT_LIST_SEARCH_FIELDS_METHOD_NAME, null);

        return response;
    }


    @Override
    @PreAuthorize("@PermissionService.isMethodPermited(authentication,  {'PP0000002'})")
    @Transactional
    public ProductResponse create(ProductRequest request, MultipartFile brochureFile, MultipartFile videoFile,
                                  List<MultipartFile> imageFiles, List<MultipartFile> documentAttachments) throws IdsmedBaseException {
        loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, CREATE_PRODUCT_METHOD_NAME, request.getLoginId());

        Long currentAccountId = idsmedAccountRepository.findByLoginId(request.getLoginId()).getId();

        ProductCareArea productCareArea = new ProductCareArea();
        List<ProductCareAreaDetail> productCareAreaDetails = new ArrayList<ProductCareAreaDetail>();
        Integer status = request.getStatus();
        Product product = new Product();
        String[] igroreProperties = {"id", "productCategoryId", "productCareAreaList", "vendorId", "productDocumentAttachmentRequests", "uniqueSellingAdvantageList", "manufacturerCountryCode", "manufacturerProvinceCode"};

        BeanUtils.copyProperties(request, product, igroreProperties);

        product.setManufacturerCountryCode(request.getManufacturerCountryCode());
        product.setManufacturerProvinceCode(request.getManufacturerProvinceCode());
        product.setCreatedBy(currentAccountId);
        product.setUpdatedBy(currentAccountId);

        //Set product category
        ProductCategory productCategory = request.getProductCategoryId() != null
                ? productCategoryRepository.getOne(request.getProductCategoryId())
                : productCategoryRepository.findFirstByCategoryName(ProductCategory.OTHERS_PRODUCT_CATEGORY_NAME);

        if (productCategory != null) {
            product.setProductCategory(productCategory);
        }

        //Set product brand
        ProductBrand productBrand = request.getBrandName() != null
                ? productBrandRepository.findByBrandName(request.getBrandName())
                : productBrandRepository.findFirstByBrandName(ProductBrand.OTHERS_PRODUCT_BRAND_NAME);

        if (productBrand != null) {
            product.setProductBrand(productBrand);
        }

        if(productBrand == null) {
            product = logIntoProductBrandTable(request, productBrand, currentAccountId, product);
        }

        // Set productIdentity
        String productId = UUID.randomUUID().toString();
        product.setProductIdentity(productId);

        if (status == StatusEnum.DRAFT.getCode()) {
            product.setStatus(StatusEnum.DRAFT.getCode());
        }

        if (status == StatusEnum.PENDING_FOR_REVIEW.getCode()) {
            product.setStatus(StatusEnum.PENDING_FOR_REVIEW.getCode());
        }

        if (status == StatusEnum.PENDING_FOR_APPROVAL.getCode()) {
            product.setStatus(StatusEnum.PENDING_FOR_APPROVAL.getCode());
        }

        Optional<Vendor> vendor = vendorRepository.findById(request.getVendorId());
        if (vendor.isPresent()) {
            product.setVendor(vendor.get());
        }

        if (!validateImageFileResolution(imageFiles)) {
            throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.PRODUCT_IMAGE_RESOLUTION_ERROR, LangHelper.getLangCode(request.getLangCode())));
        }

        // this is for generate product hierarchy response, In here have two condition:
        // 1. front end pass name, then will provide code of this name pass from front end
        // 2. front end pass code, then will provide name of this code pass from front end
        ProductHierarchyResponse productHierarchyResponse = generateProductHierarchyResponse(request, product);


        product = productRepository.save(product);

        /**
         * Send email and web notification when product is approved
         */
        IdsmedAccount sender = idsmedAccountRepository.findByLoginId(request.getLoginId());
        List<IdsmedAccount> recipients = new ArrayList<>();
        if (product.getStatus() == StatusEnum.APPROVED.getCode()) {
            recipients = processEmailAndWebNotificationForProductApproval(product, sender);
        }

        Product finalProduct = product;

        // Prepare the product care area detail parameter
        ProductCareAreaDetail productCareAreaDetail = null;
        if (!CollectionUtils.isEmpty(request.getProductCareAreaList())) {
            for (Long careAreaId : request.getProductCareAreaList()) {
                if (careAreaId == null) {
                    continue;
                }
                productCareArea = productCareAreaRepository.findFirstById(careAreaId);
                product = productRepository.getOne(product.getId());
                productCareAreaDetail = new ProductCareAreaDetail(productCareArea, product);
                productCareAreaDetails.add(productCareAreaDetail);
            }
        }
        productCareAreaDetailRepository.saveAll(productCareAreaDetails);

        // Product Second Category
        ProductSecondCategory productSecondCategory = null;
        ProductSecondCategoryDetail productSecondCategoryDetail = null;
        List<ProductSecondCategoryDetail> productSecondCategoryDetailList = new ArrayList<>();
        if (!CollectionUtils.isEmpty(request.getProductSecondCategoryIds())) {
            for (Long productSecondCategoryId : request.getProductSecondCategoryIds()) {
                if (productSecondCategoryId == null) {
                    continue;
                }

                productSecondCategory = productSecondCategoryRepository.findFirstById(productSecondCategoryId);
                product = productRepository.getOne(product.getId());
                productSecondCategoryDetail = new ProductSecondCategoryDetail(productSecondCategory, product);
                productSecondCategoryDetailList.add(productSecondCategoryDetail);
            }
        }

        productSecondCategoryDetailRepository.saveAll(productSecondCategoryDetailList);

        // Prepare the product feature parameters
        List<ProductFeatures> productFeatures = prepareProductFeatures(request.getProductFeatureParameters(), product, currentAccountId, vendor.get());

        if (!CollectionUtils.isEmpty(productFeatures)) {
            productFeatureRepository.saveAll(productFeatures);
        }

        // Process new product features, if label name not exists, save into product_tech_feature table
        List<ProductTechnicalFeatureLabel> productFeatureLabelList = processProductFeaturesLabel(productFeatures, currentAccountId, request.getLangCode());
        productTechnicalFeatureLabelRepository.saveAll(productFeatureLabelList);

        // Prepare the product technical parameters
        List<ProductTechnicalRequest> productTechnicalRequests = request.getProductTechnicalParameters();
        List<ProductTechnical> productTechnicals = new ArrayList<>();

        if (!CollectionUtils.isEmpty(productTechnicalRequests)) {
            productTechnicals = productTechnicalRequests.stream()
                    .map(ptr -> new ProductTechnical(finalProduct, ptr.getTechnicalLabel(), ptr.getTechnicalValue(),
                            ptr.getTechnicalSequence(), vendor.get().getCountryCode(), 1, ptr.getSellingPoint()))
                    .collect(Collectors.toList());
        }

        productTechnicalRepository.saveAll(productTechnicals);

        // Process new product technicals, if label name not exists, save into product_tech_feature table
        List<ProductTechnicalFeatureLabel> productTechnicalLabelList = processProductTechnicalsLabel(productTechnicals, currentAccountId, request.getLangCode());
        productTechnicalFeatureLabelRepository.saveAll(productTechnicalLabelList);

        String langCode = LangHelper.getLangCode(request.getLangCode());
        List<ProductMedia> productMedias = new ArrayList<ProductMedia>();
        // video storing
        if (videoFile != null) {
            productMedias.add(uploadAttachment(product, langCode, videoFile, MEDIA_TYPE.VIDEO));
        }
        // brochure storing
        if (brochureFile != null) {
            productMedias.add(uploadAttachment(product, langCode, brochureFile, MEDIA_TYPE.BROCHURE));
        }

        if (!CollectionUtils.isEmpty(imageFiles)) {
            productMedias = generateProductMediaList(productMedias, imageFiles, product, langCode);
        }

        if (!CollectionUtils.isEmpty(productMedias)) {
            productMedias.stream().forEach(pm -> {
                pm.setCreatedBy(currentAccountId);
            });
        }
        productMediaRepository.saveAll(productMedias);

        List<ProductDocumentAttachmentRequest> documentAttachmentRequests = request.getProductDocumentAttachmentRequests();

        Pair<List<ProductDocumentAttachment>, List<ProductDocumentAttachmentReminder>> productDocumentAttachmentPair = setProductDocumentAttachmentForCreate(product, documentAttachmentRequests
                , LangHelper.getLangCode(request.getLangCode()), documentAttachments, vendor.get().getCountryCode(), currentAccountId);

        String documentAttachmentName = "";
        String fileStatus = "";
        if (!CollectionUtils.isEmpty(documentAttachmentRequests)) {
            List<ProductDocumentAttachment> storedProductDocumentAttachments = productDocumentAttachmentRepository.saveAll(productDocumentAttachmentPair.getFirst());

            if (!CollectionUtils.isEmpty(storedProductDocumentAttachments)) {
                for (int i = 0; i < documentAttachmentRequests.size(); i++) {

                    ProductDocumentAttachmentRequest documentAttachmentRequest = documentAttachmentRequests.get(i);
                    ProductDocumentAttachment documentAttachment = storedProductDocumentAttachments.get(i);

                    List<ProductDocumentAttachmentReminder> reminders = prepareDocumentAttachmentReminders(documentAttachmentRequest, documentAttachment, currentAccountId);
                    List<ProductDocumentAttachmentReminder> storedReminders = productDocumentAttachmentReminderRepository.saveAll(reminders);

                    documentAttachment.setProductDocumentAttachmentReminderList(storedReminders);
                }
                product.setProductDocumentAttachmentList(storedProductDocumentAttachments);
            }

            //----------------- SEND EMAIL & WEB NOTIFICATION FOR PRODUCT CERTIFICATE APPROVAL -----------------
            for (FileAttachmentStatusEnum fileAttachmentStatusEnum : FileAttachmentStatusEnum.values()) {
                if (fileAttachmentStatusEnum.getCode().equals(storedProductDocumentAttachments.get(0).getStatus())) {
                    fileStatus = fileAttachmentStatusEnum.getName();
                }
            }

            documentAttachmentName = storedProductDocumentAttachments
                    .stream()
                    .map(pda -> pda.getDocumentName() + "; ")
                    .collect(Collectors.joining());

            processEmailAndWebNotificationForProductCertificateApproval(
                    product, recipients, documentAttachmentName, fileStatus, sender);
        }


        ProductRating productRating = new ProductRating(product, currentAccountId);
        productRatingRepository.save(productRating);

        ProductWishlist productWishlist = new ProductWishlist(product, 0L, currentAccountId);
        productWishlistRepository.save(productWishlist);

        // get country by country code given by product request and set the country into countryResponse
        CountryResponse manufacturerCountryResponse = generateCountryResponseByCountryCode(request);

        // get province by province code given by product request and set the province into provinceResponse
        ProvinceResponse manufacturerProvinceResponse = generateProvinceResponseByProvinceCode(request);

        // set the country response and province response into product response
        String ossBucketDomain = env.getProperty(PropertiesLabelConst.OSS_BUCKET_DOMAIN);
        ProductResponse productResponse = new ProductResponse(product, ossBucketDomain);
        productResponse.setManufacturerCountry(manufacturerCountryResponse);
        productResponse.setManufacturerProvince(manufacturerProvinceResponse);
        productResponse.setProductHierarchy(productHierarchyResponse);

        /**
         * autoApproveProduct will do checking and approve product if
         * product creator has product_approved permission
         * This will be save status product to approve for auto approve product
         */
        if(!request.getStatus().equals(StatusEnum.DRAFT.getCode())) {
            product = autoApproveProduct(request, product);
            product = productRepository.save(product);
        }

        /**
         * Get product status to do validation due to
         * product auto approval will still create product task list
         */
        if (product.getStatus() == StatusEnum.PENDING_FOR_REVIEW.getCode()) {
            //CREATE REVIEW TASK
            taskService.createProductReviewTask(product);

            recipients = processEmailAndWebNotificationForProductApproval(product, sender);
            processEmailAndWebNotificationForProductCertificateApproval(
                    product, recipients, documentAttachmentName, fileStatus, sender);
        }

        if (product.getStatus() == StatusEnum.PENDING_FOR_APPROVAL.getCode()) {
            taskService.createProductApproveTask(product);

            recipients = processEmailAndWebNotificationForProductApproval(product, sender);
            processEmailAndWebNotificationForProductCertificateApproval(
                    product, recipients, documentAttachmentName, fileStatus, sender);
            processEmailAndWebNotificationRequestToApproveProductToVendorSenior(product, request.getLoginId());
        }

        loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, CREATE_PRODUCT_METHOD_NAME, request.getLoginId());
        return productResponse;
    }

    /**
     * Auto approve when the product creator has the product_approve permission
     * also set all the product related data to approve status
     * @param productRequest
     * @param product
     * @return
     */
    private Product autoApproveProduct(ProductRequest productRequest, Product product){
        if(!StringUtils.isEmpty(productRequest.getLoginId())){
            List<ProductMedia> productMediaList = new ArrayList<>();
            List<ProductSecondCategoryDetail> productSecondCategoryDetailList = new ArrayList<>();
            List<ProductCareAreaDetail> productCareAreaList = new ArrayList<>();
            List<ProductTechnical> productTechnicalList = new ArrayList<>();
            List<ProductFeatures> productFeaturesList = new ArrayList<>();
            List<ProductDocumentAttachment> productDocumentAttachmentList = new ArrayList<>();
            List<ProductRatingDetail> productRatingDetailList = new ArrayList<>();
            List<ProductWishlistDetail> productWishlistDetailList = new ArrayList<>();
            List<BestSellerProductGlobal> bestSellerProductGlobalList = new ArrayList<>();
            List<HistoryUserProductBySearch> historyUserProductBySearchList = new ArrayList<>();
            ProductRating productRating = new ProductRating();
            ProductWishlist productWishlist = new ProductWishlist();

            IdsmedAccount idsmedAccount = idsmedAccountRepository.findByLoginId(productRequest.getLoginId());
            IdsmedUser idsmedUser = idsmedAccount.getIdsmedUser();
            IdsmedUser productApprover = idsmedUserRepository.findByIdsmedUserAndPermissionCode(idsmedUser, IdsmedPermissionCodeConst.PRODUCT_APPROVE);
            if(productApprover != null){
                product.setStatus(StatusEnum.APPROVED.getCode());
                product.setApprovedBy(idsmedAccount.getId());
                product.setApprovedDate(DateTimeHelper.getCurrentTimeUTC());

                if(!CollectionUtils.isEmpty(product.getProductMediaList())){
                    productMediaList = product.getProductMediaList().stream().map(pm ->{
                        pm.setStatus(StatusEnum.APPROVED.getCode());
                        return pm;
                    }).collect(Collectors.toList());
                }

                productMediaRepository.saveAll(productMediaList);

                if(!CollectionUtils.isEmpty(product.getProductSecondCategoryDetails())){
                    productSecondCategoryDetailList = product.getProductSecondCategoryDetails().stream().map(pscd->{
                        pscd.setStatus(StatusEnum.APPROVED.getCode());
                        return pscd;
                    }).collect(Collectors.toList());
                }

                productSecondCategoryDetailRepository.saveAll(productSecondCategoryDetailList);

                if(!CollectionUtils.isEmpty(product.getProductCareAreaList())){
                   productCareAreaList =  product.getProductCareAreaList().stream().map(pca->{
                        pca.setStatus(StatusEnum.APPROVED.getCode());
                        return pca;
                    }).collect(Collectors.toList());
                }

                productCareAreaDetailRepository.saveAll(productCareAreaList);

                if(!CollectionUtils.isEmpty(product.getProductTechnicalList())){
                    productTechnicalList = product.getProductTechnicalList().stream().map(pt->{
                        pt.setStatus(StatusEnum.APPROVED.getCode());
                        return pt;
                    }).collect(Collectors.toList());
                }

                productTechnicalRepository.saveAll(productTechnicalList);

                if(!CollectionUtils.isEmpty(product.getProductFeaturesList())){
                    productFeaturesList = product.getProductFeaturesList().stream().map(pf->{
                        pf.setStatus(StatusEnum.APPROVED.getCode());
                        return pf;
                    }).collect(Collectors.toList());
                }

                productFeatureRepository.saveAll(productFeaturesList);

                if(!CollectionUtils.isEmpty(product.getProductDocumentAttachmentList())){
                    productDocumentAttachmentList = product.getProductDocumentAttachmentList().stream().map(pda->{
                        pda.setStatus(FileAttachmentStatusEnum.APPROVED.getCode());
                        pda.getProductDocumentAttachmentReminderList().stream().map(pdar->{
                            pdar.setStatus(FileAttachmentStatusEnum.APPROVED.getCode());
                            productDocumentAttachmentReminderRepository.save(pdar);
                            return pdar;
                        }).collect(Collectors.toList());
                        return pda;
                    }).collect(Collectors.toList());
                }

                productDocumentAttachmentRepository.saveAll(productDocumentAttachmentList);

                if(product.getProductRating() != null){
                    productRating = product.getProductRating();
                    productRating.setStatus(StatusEnum.APPROVED.getCode());
                }

                productRatingRepository.save(productRating);

                if(!CollectionUtils.isEmpty(product.getProductRatingDetailList())){
                    productRatingDetailList = product.getProductRatingDetailList().stream().map(prd->{
                        prd.setStatus(StatusEnum.APPROVED.getCode());
                        return prd;
                    }).collect(Collectors.toList());
                }

                productRatingDetailRepository.saveAll(productRatingDetailList);

                if(product.getProductWishlist() != null){
                    productWishlist = product.getProductWishlist();
                    productWishlist.setStatus(StatusEnum.APPROVED.getCode());
                }

                productWishlistRepository.save(productWishlist);

                if(!CollectionUtils.isEmpty(product.getProductWishlistDetailList())){
                    productWishlistDetailList = product.getProductWishlistDetailList().stream().map(pwd->{
                        pwd.setStatus(StatusEnum.APPROVED.getCode());
                        return pwd;
                    }).collect(Collectors.toList());
                }

                productWishlistDetailRepository.saveAll(productWishlistDetailList);

                if(!CollectionUtils.isEmpty(product.getBestSellerProductGlobals())){
                    bestSellerProductGlobalList = product.getBestSellerProductGlobals().stream().map(bspg->{
                        bspg.setStatus(StatusEnum.APPROVED.getCode());
                        return bspg;
                    }).collect(Collectors.toList());
                }

                bestSellerProductGlobalRepository.saveAll(bestSellerProductGlobalList);

                if(!CollectionUtils.isEmpty(product.getHistoryUserProductBySearches())){
                    historyUserProductBySearchList = product.getHistoryUserProductBySearches().stream().map(hspbs->{
                        hspbs.setStatus(StatusEnum.APPROVED.getCode());
                        return hspbs;
                    }).collect(Collectors.toList());
                }

                historyUserProductRepository.saveAll(historyUserProductBySearchList);
            }
        }

        return product;
    }

    // set country by country code and set the country into country response
    private CountryResponse generateCountryResponseByCountryCode(ProductRequest request) throws IdsmedBaseException {
        String countryCode = request.getManufacturerCountryCode();
        if (StringUtils.isEmpty(countryCode)) {
            return null;
        }

        Country country = countryRepository.findByCountryCode(request.getManufacturerCountryCode());
        if (country == null) {
            throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.COUNTRY_CAN_NOT_FOUND_ERROR, LangHelper.getLangCode(request.getLangCode())));
        }
        CountryResponse countryResponse = new CountryResponse(country);
        return countryResponse;
    }

    // set province by province code and set the province into province response
    private ProvinceResponse generateProvinceResponseByProvinceCode(ProductRequest request) throws IdsmedBaseException {
        String provinceCode = request.getManufacturerProvinceCode();
        String countryCode = request.getManufacturerCountryCode();
        ProvinceResponse provinceResponse = null;
        if (StringUtils.isEmpty(provinceCode)) {
            return null;
        }

        if (StringUtils.isEmpty(countryCode)) {
            return null;
        }

        List<Province> provinces = provinceRepository.findByProvinceCode(provinceCode);
        Country country = countryRepository.findByCountryCode(countryCode);
        if (country == null) {
            throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.COUNTRY_CAN_NOT_FOUND_ERROR, LangHelper.getLangCode(request.getLangCode())));
        }

        if (CollectionUtils.isEmpty(provinces)) {
            throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.PROVINCE_CAN_NOT_FOUND_ERROR, LangHelper.getLangCode(request.getLangCode())));
        }

        for (Province province : provinces) {
            if (province.getCountry().equals(country)) {
                provinceResponse = new ProvinceResponse(province);
            }
        }
        return provinceResponse;
    }

    // generate country response by country code
    private CountryResponse generateCountryResponseByCountryCode(Product product, String langCode) throws IdsmedBaseException {
        String countryCode = product.getManufacturerCountryCode();
        if (StringUtils.isEmpty(countryCode)) {
            return null;
        }
        Country country = countryRepository.findByCountryCode(countryCode);
        if (country == null) {
            throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.COUNTRY_CAN_NOT_FOUND_ERROR, LangHelper.getLangCode(langCode)));
        }
        CountryResponse countryResponse = new CountryResponse(country);
        return countryResponse;
    }

    // set province by province code and set the province into province response
    private ProvinceResponse generateProvinceResponseByProvinceCode(Product product, String langCode) throws IdsmedBaseException {
        String provinceCode = product.getManufacturerProvinceCode();
        if (StringUtils.isEmpty(provinceCode)) {
            return null;
        }

        List<Province> provinces = provinceRepository.findByProvinceCode(provinceCode);
        Country country = countryRepository.findByCountryCode(product.getManufacturerCountryCode());
        if (country == null) {
            throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.COUNTRY_CAN_NOT_FOUND_ERROR, LangHelper.getLangCode(langCode)));
        }

        if (CollectionUtils.isEmpty(provinces)) {
            throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.PROVINCE_CAN_NOT_FOUND_ERROR, LangHelper.getLangCode(langCode)));
        }

        ProvinceResponse provinceResponse = null;
        for (Province province : provinces) {
            if (province.getCountry().equals(country)) {
                provinceResponse = new ProvinceResponse(province);
            }
        }
        return provinceResponse;
    }

    private ProductHierarchyResponse generateProductHierarchyResponse(ProductRequest request, Product product) throws IdsmedBaseException{
        ProductHierarchyResponse productHierarchyResponse = new ProductHierarchyResponse();
        ProductHierarchyLevelFour productHierarchyLevelFour = null;

        if(!StringUtils.isEmpty(request.getProductHierarchyName())){
            productHierarchyLevelFour = productHierarchyRepository.findFirstByName(request.getProductHierarchyName());
            if(productHierarchyLevelFour == null){
                throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.PRODUCT_HIERARCHY_NOT_FOUND_ERROR, LangHelper.getLangCode(request.getLangCode())));
            }
            String productHierarchyCode = productHierarchyLevelFour.getCode();
            productHierarchyResponse.setName(request.getProductHierarchyName());
            productHierarchyResponse.setCode(productHierarchyCode);
            product.setProductHierarchyLevelFour(productHierarchyLevelFour);
        }
        if(!StringUtils.isEmpty(request.getProductHierarchyCode())){
            productHierarchyLevelFour = productHierarchyRepository.findByCode(request.getProductHierarchyCode());
            if(productHierarchyLevelFour == null){
                throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.PRODUCT_HIERARCHY_NOT_FOUND_ERROR, LangHelper.getLangCode(request.getLangCode())));
            }
            String productHierarchyName = productHierarchyLevelFour.getName();
            productHierarchyResponse.setCode(request.getProductHierarchyCode());
            productHierarchyResponse.setName(productHierarchyName);
            product.setProductHierarchyLevelFour(productHierarchyLevelFour);
        }

        return productHierarchyResponse;
    }
    private Boolean validateImageFileResolution(List<MultipartFile> imageFiles) {
        for (MultipartFile originalImage : imageFiles) {
            File img = createImageFile(originalImage);
            if (!productExcelService.validateProductOriginalImage(img)) {
                img.delete();
                return false;
            }
        }
        return true;
    }

    private List<ProductMedia> generateProductMediaList(List<ProductMedia> productMedias, List<MultipartFile> productMediaList, Product product, String langCode) throws IdsmedBaseException {
        ProductMedia originalImageMedia = null;
        File img = null;
        for (MultipartFile originalImage : productMediaList) {
            img = createImageFile(originalImage);
            //Upload the original file
            String fileName = img.getName();
            originalImageMedia = uploadAttachment(product, langCode, originalImage, MEDIA_TYPE.IMAGE);
            originalImageMedia.setGid(fileName);

            productMedias.add(originalImageMedia);
            //Image process
            for (ImageVersionEnum imageVersion : ImageVersionEnum.values()) {
                if (imageVersion.equals(ImageVersionEnum.VERSION1)) {
                    continue;
                }
                MultipartFile image = ImageProcessServiceImpl.resizeUploadFile(originalImage, imageVersion);
                ProductMedia media = uploadAttachment(product, langCode, image, MEDIA_TYPE.IMAGE);
                media.setVersion(imageVersion.getVersion());
                media.setGid(originalImageMedia.getGid());
                media.setDocumentSeq(originalImageMedia.getDocumentSeq());
                productMedias.add(media);
            }
        }
        img.delete();
        return productMedias;
    }

    private File createImageFile(MultipartFile originalImage) {
        File img = new File(originalImage.getOriginalFilename());
        try {
            img.createNewFile();
            FileOutputStream fos = new FileOutputStream(img);
            fos.write(originalImage.getBytes());
            fos.close();
        } catch (Exception e) {
            logger.error(e.getMessage());
        }

        return img;
    }

    private List<ProductDocumentAttachmentReminder> prepareDocumentAttachmentReminders(ProductDocumentAttachmentRequest pdar, ProductDocumentAttachment productDocumentAttachment, Long currentAccountId) {
        List<ProductDocumentAttachmentReminder> reminders = new ArrayList<ProductDocumentAttachmentReminder>();

        if (!CollectionUtils.isEmpty(pdar.getProductDocumentAttachmentReminderRequests())) {
            reminders.addAll(pdar.getProductDocumentAttachmentReminderRequests().stream().map(pdarr -> {

                ProductDocumentAttachmentReminder reminder;
                if (pdarr.getId() == null) {
                    reminder = new ProductDocumentAttachmentReminder(pdarr, currentAccountId);
                } else {
                    Optional<ProductDocumentAttachmentReminder> existingReminder = productDocumentAttachmentReminderRepository.findById(pdarr.getId());
                    reminder = existingReminder.isPresent() ? existingReminder.get() : new ProductDocumentAttachmentReminder();
                    reminder.updateProductDocumentAttachmentReminder(pdarr.getDaysBefore(), pdarr.getDaysAfter(), pdarr.getInExpiryDate(), pdarr.getByEmail(),
                            pdarr.getByWeb(), pdarr.getBySms(), currentAccountId);
                }

                reminder.setProductDocumentAttachment(productDocumentAttachment);
                return reminder;
            }).collect(Collectors.toList()));
        }
        return reminders;
    }

    private List<ProductFeatures> prepareProductFeatures(List<ProductFeatureRequest> productFeatureRequests, Product product, Long currentAccountId, Vendor vendor) {
        List<ProductFeatures> productFeatures = new ArrayList<>();
        if (!CollectionUtils.isEmpty(productFeatureRequests)) {
            productFeatures = productFeatureRequests.stream()
                    .map(pfr -> {
                        ProductFeatures newProductFeatures = new ProductFeatures(product, pfr.getFeatureLabel(), pfr.getFeatureValue(),
                                pfr.getFeatureSequence(), vendor.getCountryCode(), 1, pfr.getSellingPoint());
                        newProductFeatures.setCreatedBy(currentAccountId);
                        return newProductFeatures;
                    })
                    .collect(Collectors.toList());
        }
        return productFeatures;
    }

    private Pair<List<ProductDocumentAttachment>, List<ProductDocumentAttachmentReminder>> setProductDocumentAttachmentForCreate(Product product, List<ProductDocumentAttachmentRequest> productDocumentAttachmentRequests
            , String langCode, List<MultipartFile> files, String countryCode, Long currentAccountId) throws IdsmedBaseException {
        Pair<List<ProductDocumentAttachment>, List<ProductDocumentAttachmentReminder>> productDocumentAttachmentPair;

        productDocumentAttachmentPair = setProductDocumentAttachment(product, productDocumentAttachmentRequests
                , langCode, files, countryCode);

        if (!CollectionUtils.isEmpty(productDocumentAttachmentPair.getFirst())) {
            productDocumentAttachmentPair.getFirst().forEach(at -> {
                at.setCreatedBy(currentAccountId);
                at.setUpdatedBy(currentAccountId);
            });
        }
        return productDocumentAttachmentPair;
    }

    private Pair<List<ProductDocumentAttachment>, List<ProductDocumentAttachmentReminder>> setProductDocumentAttachmentForEdit(Product product, List<ProductDocumentAttachmentRequest> productDocumentAttachmentRequests
            , String langCode, List<MultipartFile> files, String countryCode, Long currentAccountId) throws IdsmedBaseException {
        Pair<List<ProductDocumentAttachment>, List<ProductDocumentAttachmentReminder>> productDocumentAttachmentPair;

        productDocumentAttachmentPair = setProductDocumentAttachment(product, productDocumentAttachmentRequests
                , langCode, files, countryCode);

        if (!CollectionUtils.isEmpty(productDocumentAttachmentPair.getFirst())) {
            productDocumentAttachmentPair.getFirst().forEach(at -> {
                at.setUpdatedBy(currentAccountId);
            });
        }
        return productDocumentAttachmentPair;
    }

    private Pair<List<ProductDocumentAttachment>, List<ProductDocumentAttachmentReminder>> setProductDocumentAttachment(Product product, List<ProductDocumentAttachmentRequest> productDocumentAttachmentRequests
            , String langCode, List<MultipartFile> files, String countryCode) throws IdsmedBaseException {
        List<ProductDocumentAttachment> productDocumentAttachmentList = new ArrayList<>();
        List<ProductDocumentAttachmentReminder> productDocumentAttachmentReminderList = new ArrayList<>();

        if (!CollectionUtils.isEmpty(productDocumentAttachmentRequests) && !CollectionUtils.isEmpty(files)) {
            for (ProductDocumentAttachmentRequest productDocumentAttachmentRequest : productDocumentAttachmentRequests) {
                ProductDocumentAttachment productDocumentAttachment = new ProductDocumentAttachment();
                productDocumentAttachment.setProduct(product);
                productDocumentAttachment.setCountryCode(countryCode);

                if (productDocumentAttachmentRequest.getFileIndex() != null) {
                    MultipartFile file = files.get(productDocumentAttachmentRequest.getFileIndex());
                    productDocumentAttachment.setDocumentPath(uploadProductDocumentAttachmentFile(langCode, file));
                }

                ProductLabelConfig productLabelConfig = productLabelConfigurationRepository.getOne(productDocumentAttachmentRequest.getProductLabelConfigId());
                productDocumentAttachment.setDocumentName(productDocumentAttachmentRequest.getDocumentName());
                productDocumentAttachment.setDocumentSecondaryName(productDocumentAttachmentRequest.getDocumentSecondaryName());
                productDocumentAttachment.setOriginalFileName(productDocumentAttachmentRequest.getOriginalFileName());
                productDocumentAttachment.setExpiryDate(productDocumentAttachmentRequest.getExpiryDate() != null ? DateTimeHelper.convertZonedDateTimeToUTC(productDocumentAttachmentRequest.getExpiryDate()) : null);
                productDocumentAttachment.setExpDateTimeZone(productDocumentAttachmentRequest.getExpDateTimeZone());
                productDocumentAttachment.setStatus(FileAttachmentStatusEnum.PENDING_FOR_REVIEW.getCode());
                productDocumentAttachment.setProductLabelConfig(productLabelConfig);

                List<ProductDocumentAttachmentReminderRequest> reminderRequests = productDocumentAttachmentRequest.getProductDocumentAttachmentReminderRequests();
                if (!CollectionUtils.isEmpty(reminderRequests)) {
                    for (ProductDocumentAttachmentReminderRequest reminderRequest : reminderRequests) {
                        ProductDocumentAttachmentReminder productDocumentAttachmentReminder = new ProductDocumentAttachmentReminder(reminderRequest);
                        productDocumentAttachmentReminder.setProductDocumentAttachment(productDocumentAttachment);
                        productDocumentAttachmentReminderList.add(productDocumentAttachmentReminder);
                    }
                }
                productDocumentAttachmentList.add(productDocumentAttachment);
            }
        }
        return Pair.of(productDocumentAttachmentList, productDocumentAttachmentReminderList);
    }

    private String uploadProductDocumentAttachmentFile(String langCode, MultipartFile documentAttachment) throws IdsmedBaseException {
        final String STORAGE_TYPE;
        String serverURL = "";
		String relativePath = null;

        try {
            Map<String, String> fileUploadInfoMap = new HashMap<>();
            fileUploadInfoMap = fileUploadService.uploadImage(documentAttachment, EntityType.PRODUCT);
            STORAGE_TYPE = env.getProperty(STORAGE_TYPE_PROPERTY_NAME);
            String baseURL = env.getProperty(PRODUCT_FILE_UPLOAD_BASE_URL_NAME);
            serverURL = StringUtils.isEmpty(baseURL) ? "" : baseURL + "/" + fileUploadInfoMap.get(FileUploadServiceImpl.HASH_FILE_NAME_KEY);
			relativePath = fileUploadInfoMap.get(FileUploadServiceImpl.UPLOAD_URL);
        } catch (IOException e) {
            throw new IdsmedBaseException(
                    messageResourceService.getErrorInfo(ErrorInfo.UPLOAD_FILE_ERROR, LangHelper.getLangCode(langCode)));
        }
        return STORAGE_TYPE.equalsIgnoreCase(SERVER_STORAGE_TYPE) ? serverURL : relativePath;
    }

    private ProductMedia uploadAttachment(Product product, String langCode, MultipartFile file, MEDIA_TYPE type)
            throws IdsmedBaseException {
        final String STORAGE_TYPE = env.getProperty(STORAGE_TYPE_PROPERTY_NAME);
        Map<String, String> fileUploadInfoMap;
        try {
            fileUploadInfoMap = fileUploadService.uploadImage(file, EntityType.PRODUCT);
        } catch (IOException e) {
            throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.UPLOAD_FILE_ERROR, LangHelper.getLangCode(langCode)));
        }

        String documentName = fileUploadInfoMap.get(FileUploadServiceImpl.ORIGINAL_FILE_NAME_KEY);
        String documentPath = fileUploadInfoMap.get(FileUploadServiceImpl.FULL_PATH_KEY);
        String baseURL = env.getProperty(PRODUCT_FILE_UPLOAD_BASE_URL_NAME);
        String serverURL = StringUtils.isEmpty(baseURL) ? ""
                : baseURL + "/" + fileUploadInfoMap.get(FileUploadServiceImpl.HASH_FILE_NAME_KEY);
        String ossURL = fileUploadInfoMap.get(FileUploadServiceImpl.UPLOAD_URL);
        String documentUrl = STORAGE_TYPE.equalsIgnoreCase(SERVER_STORAGE_TYPE) ? serverURL : ossURL;
        ProductMedia pm = new ProductMedia(documentName, type.getType(), documentPath, documentUrl,
                product.getVendor().getCountryCode());
        pm.setProduct(product);
        return pm;
    }

    private void sortProductFeatures(List<ProductFeatures> features) {
        features.sort(new Comparator<ProductFeatures>() {
            @Override
            public int compare(ProductFeatures o1, ProductFeatures o2) {
                return o1.getProductLabelSequence() - o2.getProductLabelSequence();
            }
        });
    }

    private void sortProductTechnicals(List<ProductTechnical> technicals) {
        technicals.sort(new Comparator<ProductTechnical>() {
            @Override
            public int compare(ProductTechnical o1, ProductTechnical o2) {
                return o1.getProductLabelSequence() - o2.getProductLabelSequence();
            }
        });
    }

    private void sortProductCareAreaDetail(List<ProductCareAreaDetail> productCareAreaDetails) {
        productCareAreaDetails.sort(new Comparator<ProductCareAreaDetail>() {
            @Override
            public int compare(ProductCareAreaDetail o1, ProductCareAreaDetail o2) {
                Long result = new Long(o1.getId() - o2.getId());
                return result.intValue();
            }
        });
    }

    @Override
//    @PreAuthorize("@PermissionService.isMethodPermited(authentication,  {'PP0000005'})")
    public ProductResponse updatePendingApproveStatus(ProductRequest request) throws IdsmedBaseException {
        loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, UPDATE_PENDING_APPROVED_STATUS_METHOD_NAME, request.getLoginId());
        Long productId = request.getId();
        Optional<Product> productOpt = productRepository.findById(productId);

        if (!productOpt.isPresent()) {
            logger.error("The Product with id: {} is not existing", request.getId());
            throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.PRODUCT_ID_NOT_EXISTED,
                    LangHelper.getLangCode(request.getLangCode())));
        }

        Product product = productOpt.get();

        IdsmedAccount reviewer = idsmedAccountRepository.findByLoginId(request.getLoginId());
        if (reviewer == null) {
            logger.error("Can't find account with loginId: {}", request.getLoginId());
            throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.ACCOUNT_NOT_EXIST_ERROR, LangHelper.getLangCode(request.getLangCode())));
        }

        //UPDATE ProductDocumentAttachment WITHOUT UPLOAD FILE
        List<ProductDocumentAttachment> documentAttachments = updateProductDocumentAttachmentWithoutUploadFile(request.getProductDocumentAttachmentRequests(), reviewer.getId());
        if (!CollectionUtils.isEmpty(documentAttachments)) {
            documentAttachments = productDocumentAttachmentRepository.saveAll(documentAttachments);
        }

        product.setProductDocumentAttachmentList(documentAttachments);
        product.setExpDate(request.getExpDate());
        product.setStatus(StatusEnum.PENDING_FOR_APPROVAL.getCode());
        productRepository.save(product);

        //UPDATE REVIEW TASK TO COMPLETED STATUS
        taskService.setCompletedProductReviewTask(product, request.getLoginId());

        //CREATE APPROVE TASK
        taskService.createProductApproveTask(product);

        processEmailAndWebNotificationRequestToApproveProductToVendorSenior(product, request.getLoginId());

        loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, UPDATE_PENDING_APPROVED_STATUS_METHOD_NAME, request.getLoginId());
        String ossBucketDomain = env.getProperty(PropertiesLabelConst.OSS_BUCKET_DOMAIN);
        ProductResponse productResponse = new ProductResponse(product, ossBucketDomain);
        return productResponse;
    }

    @Override
    @Transactional
    @PreAuthorize("@PermissionService.isMethodPermited(authentication,  {'PP0000005'})")
    // 23 = Approve Product Permission Id
    public ProductResponse updateApproveStatus(ProductRequest request) throws IdsmedBaseException {
        loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, UPDATE_APPROVED_STATUS_METHOD_NAME, request.getLoginId());
        Long productId = request.getId();
        Optional<Product> existingProductOptional = productRepository.findById(productId);

        List<IdsmedUser> idsmedUserList = authenticationService.getUsersByPermissionCode(IdsmedPermissionCodeConst.PRODUCT_APPROVE);

        List<IdsmedUser> idsmedUsers = idsmedUserRepository.findAllByCompanyCode(existingProductOptional.get().getVendor().getCompanyCode());
        idsmedUserList.addAll(idsmedUsers);

        IdsmedAccount reviewer = idsmedAccountRepository.findByLoginId(request.getLoginId());
        if (reviewer == null) {
            logger.error("Can not find account with loginId: {}", request.getLoginId());
        }

        logger.info("Get created by " + existingProductOptional.get().getCreatedBy());

        //UPDATE ProductDocumentAttachment WITHOUT UPLOAD FILE
        List<ProductDocumentAttachment> documentAttachments = updateProductDocumentAttachmentWithoutUploadFile(request.getProductDocumentAttachmentRequests(), reviewer.getId());
        if (!CollectionUtils.isEmpty(documentAttachments)) {
            productDocumentAttachmentRepository.saveAll(documentAttachments);
        }

        IdsmedAccount idsmedAccount = idsmedAccountRepository.findById(existingProductOptional.get().getCreatedBy()).get();
        IdsmedUser User = idsmedAccount.getIdsmedUser();
        idsmedUserList.add(User);

        List<IdsmedAccount> accountList = new ArrayList<>();
        for (IdsmedUser idsmedUser : idsmedUserList) {
            List<IdsmedAccount> accounts = idsmedUser.getIdsmedAccounts().isEmpty() ? null : idsmedUser.getIdsmedAccounts();
            accountList.addAll(accounts);
        }

        if (!existingProductOptional.isPresent()) {
            logger.error("The Product Id not existing with Product Id : {} ", request.getId());
            throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.PRODUCT_ID_NOT_EXISTED,
                    LangHelper.getLangCode(request.getLangCode())));
        }

        Product existingProduct = existingProductOptional.get();

        existingProduct.setExpDate(request.getExpDate());
        existingProduct.setStatus(StatusEnum.APPROVED.getCode());
        existingProduct.setApprovedBy(request.getApprovedBy());
        existingProduct.setApprovedDate(DateTimeHelper.getCurrentTimeUTC());

        productRepository.save(existingProduct);

        //TODO Log Product and related data.
        saveProductRelatedLogIntoDatabase(existingProduct);

        //UPDATE VENDOR APPROVE TASK TO COMPLETED STATUS
        taskService.setCompletedProductApproveTask(existingProduct, request.getLoginId());

        /**
         * SEND EMAIL AND WEB NOTIFICATION TO PRODUCT CREATOR, VENDOR FIRST & SECOND PERSON, AND VENDOR COORDINATOR
         * THEN SEND PRODUCT CERTIFICATE APPROVAL NOTIFICATION
         */
        logger.info("Sending simple message with product id [] :" + request.getId());
        String fileStatus = "";
        if (!CollectionUtils.isEmpty(documentAttachments)) {
            for (FileAttachmentStatusEnum fileAttachmentStatusEnum : FileAttachmentStatusEnum.values()) {
                if (fileAttachmentStatusEnum.getCode().equals(documentAttachments.get(0).getStatus())) {
                    fileStatus = fileAttachmentStatusEnum.getName();
                }
            }

            String documentAttachmentName = documentAttachments
                    .stream()
                    .map(pda -> pda.getDocumentName() + "; ")
                    .collect(Collectors.joining());

            IdsmedAccount sender = idsmedAccountRepository.findByLoginId(request.getLoginId());

            List<IdsmedAccount> recipients = processEmailAndWebNotificationForProductApproval(existingProduct, reviewer);
            processEmailAndWebNotificationForProductCertificateApproval(
                    existingProduct, recipients, documentAttachmentName, fileStatus, sender);
        }

        //---------------------------- EMAIL & WEB NOTIFICATION FOR SCP ADMIN -----------------------------------
        Pair<String, String> emailPairForSCPAdmin = processEmailNotificationAboutApprovedProductToSCPAdmin(existingProduct, request.getLangCode());

        if (emailPairForSCPAdmin != null) {
            logger.info("Start send product approved notification to SCP Admin with product id  : {} ", request.getId());
            processWebNotificationAboutApprovedProductToSCPAdmin(reviewer, emailPairForSCPAdmin);
            logger.info("End send product approved notification to SCP Admin with product id  : {} ", request.getId());
        }
        String ossBucketDomain = env.getProperty(PropertiesLabelConst.OSS_BUCKET_DOMAIN);
        ProductResponse productResponse = new ProductResponse(existingProduct, ossBucketDomain);

        loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, UPDATE_APPROVED_STATUS_METHOD_NAME, request.getLoginId());
        return productResponse;
    }

    private void saveProductRelatedLogIntoDatabase(Product existingProduct) {
        // store product into product log
        ProductLog productLog = new ProductLog(existingProduct);
        productLogRepository.save(productLog);

        // store product media into product media log
        saveProductMediaLogIntoDatabase(existingProduct);

        // store product features into product features log
        saveProductFeaturesLogIntoDatabase(existingProduct);

        // store product technical into product technical log
        saveProductTechnicalLogIntoDatabase(existingProduct);

        // store product reject into product reject log
        saveProductRejectLogIntoDatabase(existingProduct);

        // store Product Care Area detail Into Database
        saveProductCareAreaDetailLogIntoDatabase(existingProduct);
    }

    @Transactional
    public void saveProductMediaLogIntoDatabase(Product existingProduct) {
        Integer version = existingProduct.getProductVersion();
        if (!CollectionUtils.isEmpty(existingProduct.getProductMediaList())) {
            List<ProductMediaLog> productMediaLogs = existingProduct.getProductMediaList()
                    .stream()
                    .map(pm -> {
                        ProductMediaLog productMediaLog = new ProductMediaLog(pm);
                        productMediaLog.setVersion(version);
                        return productMediaLog;
                    }).collect(Collectors.toList());
            productMediaLogRepository.saveAll(productMediaLogs);
        }

    }

    @Transactional
    public void saveProductFeaturesLogIntoDatabase(Product existingProduct) {
        Integer version = existingProduct.getProductVersion();
        if (!CollectionUtils.isEmpty(existingProduct.getProductFeaturesList())) {
            List<ProductFeaturesLog> productFeaturesLogs = existingProduct.getProductFeaturesList()
                    .stream()
                    .map(pf -> {
                        ProductFeaturesLog productFeaturesLog = new ProductFeaturesLog(pf);
                        productFeaturesLog.setVersion(version);
                        return productFeaturesLog;
                    }).collect(Collectors.toList());
            productFeatureLogRepository.saveAll(productFeaturesLogs);
        }
    }

    @Transactional
    public void saveProductTechnicalLogIntoDatabase(Product existingProduct) {
        Integer version = existingProduct.getProductVersion();
        if (!CollectionUtils.isEmpty(existingProduct.getProductTechnicalList())) {
            List<ProductTechnicalLog> productTechnicalLogs = existingProduct.getProductTechnicalList()
                    .stream()
                    .map(pt -> {
                        ProductTechnicalLog productTechnicalLog = new ProductTechnicalLog(pt);
                        productTechnicalLog.setVersion(version);
                        return productTechnicalLog;
                    }).collect(Collectors.toList());
            productTechnicalLogRepository.saveAll(productTechnicalLogs);
        }
    }

    @Transactional
    public void saveProductRejectLogIntoDatabase(Product existingProduct) {
        Integer version = existingProduct.getProductVersion();
        if (!CollectionUtils.isEmpty(existingProduct.getProductRejectList())) {
            List<ProductRejectLog> productRejectLogs = existingProduct.getProductRejectList()
                    .stream()
                    .map(pr -> {
                        ProductRejectLog productRejectLog = new ProductRejectLog(pr);
                        productRejectLog.setVersion(version);
                        return productRejectLog;
                    }).collect(Collectors.toList());
            productRejectLogRepository.saveAll(productRejectLogs);
        }
    }

    @Transactional
    public void saveProductCareAreaDetailLogIntoDatabase(Product existingProduct) {
        Integer version = existingProduct.getProductVersion();
        if (!CollectionUtils.isEmpty(existingProduct.getProductCareAreaList())) {
            List<ProductCareAreaDetailLog> productCareAreaDetailLogs = existingProduct.getProductCareAreaList()
                    .stream()
                    .map(pcrd -> {
                        ProductCareAreaDetailLog productCareAreaDetailLog = new ProductCareAreaDetailLog(pcrd);
                        productCareAreaDetailLog.setVersion(version);
                        return productCareAreaDetailLog;
                    }).collect(Collectors.toList());
            productCareAreaDetailLogRepository.saveAll(productCareAreaDetailLogs);
        }
    }

    @Override
    @PreAuthorize("@PermissionService.isMethodPermited(authentication,  {'PP0000007'})")
    // 25 = Reject Product Permission Id
    public ProductResponse updateRejectStatus(ProductRequest request) throws IdsmedBaseException {
        loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, UPDATE_REJECT_STATUS_METHOD_NAME, request.getLoginId());
        Long productId = request.getId();
        String rejectReason = request.getRejectReason();

        Optional<Product> productOpt = productRepository.findById(productId);
        if (!productOpt.isPresent()) {
            logger.error("The Product Id not existing with Product Id  : {} ", request.getId());
            throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.PRODUCT_ID_NOT_EXISTED,
                    LangHelper.getLangCode(request.getLangCode())));
        }

        IdsmedAccount reviewer = idsmedAccountRepository.findByLoginId(request.getLoginId());

        Product product = productOpt.get();

        List<IdsmedUser> idsmedUserList = authenticationService.getUsersByPermissionCode(IdsmedPermissionCodeConst.PRODUCT_REJECT);
        List<IdsmedAccount> accountList = new ArrayList<>();
        for (IdsmedUser idsmedUser : idsmedUserList) {
            List<IdsmedAccount> accounts = idsmedUser.getIdsmedAccounts().isEmpty() ? null : idsmedUser.getIdsmedAccounts();
            accountList.addAll(accounts);
        }

        //UPDATE ProductDocumentAttachment WITHOUT UPLOAD FILE
        List<ProductDocumentAttachment> documentAttachments = updateProductDocumentAttachmentWithoutUploadFile(request.getProductDocumentAttachmentRequests(), reviewer.getId());
        if (!CollectionUtils.isEmpty(documentAttachments)) {
            productDocumentAttachmentRepository.saveAll(documentAttachments);
        }

        product.setStatus(StatusEnum.REJECT.getCode());
        Product saveProduct = productRepository.save(product);

        ProductReject productReject = new ProductReject();
        productReject.setProduct(saveProduct);
        productReject.setRemarks(rejectReason);
        productReject.setCountryCode(saveProduct.getVendor().getCountryCode());
        productReject.setStatus(StatusEnum.DRAFT.getCode());
        productRejectRepository.save(productReject);

        //UPDATE TASK TO COMPLETED STATUS
        taskService.setCompletedProductReviewTask(saveProduct, request.getLoginId());
        taskService.setCompletedProductApproveTask(saveProduct, request.getLoginId());
        String ossBucketDomain = env.getProperty(PropertiesLabelConst.OSS_BUCKET_DOMAIN);
        ProductResponse productResponse = new ProductResponse(product, ossBucketDomain);
        productResponse.setRejectReason(rejectReason);

        /**
         * This method will do 3 part of email notification
         * First, send email notification and web notification to product creator and vendor coordinator with
         * account in SMARTSCP system
         * Second, send email notification only to vendor first and second person in charge
         * Third, send email and web notification for product certificate approval
         */
        if (!CollectionUtils.isEmpty(documentAttachments)) {
            String fileStatus = "";
            String documentAttachmentName = "";
            for (FileAttachmentStatusEnum fileAttachmentStatusEnum : FileAttachmentStatusEnum.values()) {
                if (fileAttachmentStatusEnum.getCode().equals(documentAttachments.get(0).getStatus())) {
                    fileStatus = fileAttachmentStatusEnum.getName();
                }
            }

            documentAttachmentName = documentAttachments
                    .stream()
                    .map(pda -> pda.getDocumentName() + "; ")
                    .collect(Collectors.joining());

            List<IdsmedAccount> recipients = processEmailAndWebNotificationForProductRejected(product, rejectReason, reviewer);
            processEmailAndWebNotificationForProductCertificateApproval(
                    product,
                    recipients,
                    documentAttachmentName,
                    fileStatus,
                    reviewer);
        }
        loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, UPDATE_REJECT_STATUS_METHOD_NAME, request.getLoginId());
        return productResponse;
    }

    private List<ProductDocumentAttachment> updateProductDocumentAttachmentWithoutUploadFile(List<ProductDocumentAttachmentRequest> documentAttachmentRequests, Long updatedBy) {
		String ossBucketDomain = env.getProperty(PropertiesLabelConst.OSS_BUCKET_DOMAIN);
		List<ProductDocumentAttachment> documentAttachments = new ArrayList<>();
        if (!CollectionUtils.isEmpty(documentAttachmentRequests)) {
            documentAttachments = documentAttachmentRequests.stream().map(r -> {
                Optional<ProductDocumentAttachment> attachmentOpt = productDocumentAttachmentRepository.findById(r.getId());
                ProductDocumentAttachment documentAttachment = attachmentOpt.get();
                r.setDocumentPath(documentAttachment.getDocumentPath());
                r.setOriginalFileName(documentAttachment.getOriginalFileName());
                documentAttachment.updateDocumentAttachment(r, updatedBy, ossBucketDomain);
                return documentAttachment;
            }).collect(Collectors.toList());
        }
        return documentAttachments;
    }

    @Override
    @PreAuthorize("@PermissionService.isMethodPermited(authentication,  {'PP0000003'})")
    // 21 = Edit Product Permission Id
    public ProductResponse updateDraftOfPendingStatus(ProductRequest request, MultipartFile brochureFile,
                                                      MultipartFile videoFile, List<MultipartFile> imageFiles, List<MultipartFile> productDocumentAttachments) throws IdsmedBaseException {

        loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, UPDATE_DRAFT_OF_PENDING_METHOD_NAME, request.getLoginId());

        return editNormalProduct(request, brochureFile, videoFile, imageFiles, productDocumentAttachments);
    }

    //save into product brand table when the brand is not exist by brand name provided by request
    @Transactional
    public Product logIntoProductBrandTable(ProductRequest productRequest, ProductBrand productBrand, Long currentAccountId, Product product){
        ProductBrand brand = new ProductBrand();
        brand.setBrandName(productRequest.getBrandName());
        brand.setBrandCode(productRequest.getBrandName());
        brand.setCreatedBy(currentAccountId);
        brand.setUpdatedBy(currentAccountId);
        brand.setStatus(StatusEnum.APPROVED.getCode());
        product.setProductBrand(brand);
        productBrandRepository.save(brand);
        return product;
    }

    @Transactional
    public ProductResponse editNormalProduct(ProductRequest request,
                                             MultipartFile brochureFile, MultipartFile videoFile, List<MultipartFile> imageFiles, List<MultipartFile> documentAttachments)
            throws IdsmedBaseException {

        //GET Product FROM database
        Optional<Product> productOpt = productRepository.findById(request.getId());
        if (!productOpt.isPresent()) {
            logger.error("The Product not existing with Product Id: {} ", request.getId());
            throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.PRODUCT_ID_NOT_EXISTED,
                    LangHelper.getLangCode(request.getLangCode())));
        }

        Product product = productOpt.get();
        String countryCode = product.getVendor().getCountryCode();

        //==========================================================================================

        Long currentAccountId = idsmedAccountRepository.findByLoginId(request.getLoginId()).getId();

        List<ProductCareAreaDetail> existingProductCareAreaDetail = product.getProductCareAreaList();
        Integer status = request.getStatus();

        String[] igroreProperties = {"id",
                "status",
                "productCategoryId",
                "productSecondCategoryIds",
                "productCareAreaList",
                "productMediaList",
                "productCertificateList",
                "productRejectList",
                "productFeaturesList",
                "productTechnicalList",
                "productDocumentAttachmentRequests",
                "uniqueSellingAdvantageList",
                "manufacturerCountryCode",
                "manufacturerProvinceCode"};

        BeanUtils.copyProperties(request, product, igroreProperties);

        ProductCategory productCategory = request.getProductCategoryId() != null
                ? productCategoryRepository.getOne(request.getProductCategoryId())
                : productCategoryRepository.findFirstByCategoryName(ProductCategory.OTHERS_PRODUCT_CATEGORY_NAME);
        if (productCategory != null) {
            product.setProductCategory(productCategory);
        }

        ProductBrand productBrand = !StringUtils.isEmpty(request.getBrandName()) ?
                productBrandRepository.findByBrandName(request.getBrandName())
                : productBrandRepository.findFirstByBrandName(ProductBrand.OTHERS_PRODUCT_BRAND_NAME);

        if (productBrand != null) {
            product.setProductBrand(productBrand);
        }

        if(productBrand == null) {
            product = logIntoProductBrandTable(request, productBrand, currentAccountId, product);
        }

        //EDIT ProductDocumentAttachment
        List<ProductDocumentAttachmentRequest> attachmentRequests = request.getProductDocumentAttachmentRequests();
        List<ProductDocumentAttachment> existingDocumentAttachment = product.getProductDocumentAttachmentList();

        if (!CollectionUtils.isEmpty(attachmentRequests)) {
            //If status of existing ProductDocumentAttachment is rejected we upload new file and update fields
            List<ProductDocumentAttachmentRequest> updateRequests =
                    attachmentRequests
                            .stream()
                            .filter(r -> {
                                Optional<ProductDocumentAttachment> existingDocumentAttachmentOpt = existingDocumentAttachment
                                        .stream().filter(f -> f.getId().equals(r.getId())).findFirst();
                                if (existingDocumentAttachmentOpt.isPresent()
                                        && existingDocumentAttachmentOpt.get().getStatus().equals(FileAttachmentStatusEnum.REJECT.getCode())) {
                                    return true;
                                }
                                return false;
                            }).collect(Collectors.toList());
            if (!CollectionUtils.isEmpty(updateRequests)) {
                List<ProductDocumentAttachment> updatedAttachments = updateProductDocumentAttachmentWithUploadFile(updateRequests
                        , documentAttachments
                        , product
                        , product.getCountryCode()
                        , request.getLangCode());
                updatedAttachments.forEach(at -> {
                    at.setStatus(FileAttachmentStatusEnum.PENDING_FOR_REVIEW.getCode());
                });

                if (!CollectionUtils.isEmpty(updatedAttachments)) {
                    productDocumentAttachmentRepository.saveAll(updatedAttachments);

                    List<ProductDocumentAttachmentReminder> tobeDeletedReminder = new ArrayList<ProductDocumentAttachmentReminder>();
                    //Due to this is existing document attachment --> no need get documentAttachment for documentAttachmentReminder from result of repository saveAll
                    updateRequests.stream().forEach(pdar -> {
                        //Get one document attachment entity from updateAttachments
                        ProductDocumentAttachment attachment = updatedAttachments.stream().filter(at -> at.getId().equals(pdar.getId())).findFirst().get();

                        List<ProductDocumentAttachmentReminder> existingReminders = productDocumentAttachmentReminderRepository.findByProductDocumentAttachment(attachment);
                        if (!CollectionUtils.isEmpty(existingReminders)) {
                            tobeDeletedReminder.addAll(existingReminders);
                        }
                        List<ProductDocumentAttachmentReminder> reminders = prepareDocumentAttachmentReminders(pdar, attachment, currentAccountId);
                        List<ProductDocumentAttachmentReminder> storedReminders = productDocumentAttachmentReminderRepository.saveAll(reminders);
                        attachment.setProductDocumentAttachmentReminderList(storedReminders);
                    });
                    productDocumentAttachmentReminderRepository.deleteAll(tobeDeletedReminder);
                }
            }

            //If status of existing ProductDocumentAttachment is approved we create new ProductDocumentAttachment record.
            List<ProductDocumentAttachmentRequest> newRequests =
                    attachmentRequests
                            .stream()
                            .filter(r -> {
                                Optional<ProductDocumentAttachment> existingDocumentAttachmentOpt = existingDocumentAttachment
                                        .stream().filter(f -> f.getId().equals(r.getId())).findFirst();
                                if (existingDocumentAttachmentOpt.isPresent()
                                        && existingDocumentAttachmentOpt.get().getStatus().equals(FileAttachmentStatusEnum.APPROVED.getCode())) {
                                    return true;
                                }
                                return false;
                            }).collect(Collectors.toList());

            if (!CollectionUtils.isEmpty(newRequests)) {
                Pair<List<ProductDocumentAttachment>, List<ProductDocumentAttachmentReminder>> newAttachmentPair = setProductDocumentAttachmentForCreate(product
                        , newRequests
                        , request.getLangCode()
                        , documentAttachments
                        , product.getCountryCode(), currentAccountId);


                if (!CollectionUtils.isEmpty(newAttachmentPair.getFirst())) {
                    //Due to this is new document attachment --> need get documentAttachment for documentAttachmentReminder from result of repository saveAll
                    List<ProductDocumentAttachment> storedDocumentAttachments = productDocumentAttachmentRepository.saveAll(newAttachmentPair.getFirst());

                    //Update product document attachment reminder for each attachment
                    for (int i = 0; i < newRequests.size(); i++) {
                        ProductDocumentAttachmentRequest newRequest = newRequests.get(i);
                        ProductDocumentAttachment documentAttacchment = storedDocumentAttachments.get(i);

                        List<ProductDocumentAttachmentReminder> reminders = prepareDocumentAttachmentReminders(newRequest, documentAttacchment, currentAccountId);
                        List<ProductDocumentAttachmentReminder> storedReminders = productDocumentAttachmentReminderRepository.saveAll(reminders);
                        documentAttacchment.setProductDocumentAttachmentReminderList(storedReminders);
                    }
                }
            }
        }

        if (status == StatusEnum.DRAFT.getCode()) {
            product.setStatus(StatusEnum.DRAFT.getCode());
        }

        if (status == StatusEnum.PENDING_FOR_REVIEW.getCode()) {
            product.setStatus(StatusEnum.PENDING_FOR_REVIEW.getCode());
        }

        if (status == StatusEnum.PENDING_FOR_APPROVAL.getCode()) {
            product.setStatus(StatusEnum.PENDING_FOR_APPROVAL.getCode());
        }

        if (!validateImageFileResolution(imageFiles)) {
            throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.PRODUCT_IMAGE_RESOLUTION_ERROR, LangHelper.getLangCode(request.getLangCode())));
        }

        // auto approve the product and product related table when this product is edit by vendor senior.
        if(!request.getStatus().equals(StatusEnum.DRAFT.getCode())) {
            product = autoApproveProduct(request, product);
        }

        product.setProductVersion(product.getProductVersion() + 1);
        product.setManufacturerCountryCode(request.getManufacturerCountryCode());
        product.setManufacturerProvinceCode(request.getManufacturerProvinceCode());
        product.setUpdatedBy(currentAccountId);

        // this is for generate product hierarchy response, In here have two condition:
        // 1.  front end pass name, then will provide code of this name pass from front end
        // 2. front end pass code, then will provide name of this code pass from front end
        ProductHierarchyResponse productHierarchyResponse = generateProductHierarchyResponse(request, product);


        Product storedProduct = productRepository.save(product);

        if (!CollectionUtils.isEmpty(existingProductCareAreaDetail)) {
            productCareAreaDetailRepository.deleteAll(existingProductCareAreaDetail);
        }

        if (!CollectionUtils.isEmpty(request.getProductCareAreaList())) {
            List<ProductCareAreaDetail> productCareAreaDetails = generateProductCareAreaDetailList(request.getProductCareAreaList(), storedProduct);
            productCareAreaDetailRepository.saveAll(productCareAreaDetails);
        }

        // Product Second Category
        List<ProductSecondCategoryDetail> existingProductSecondCategoryDetail = product.getProductSecondCategoryDetails();
        if (!CollectionUtils.isEmpty(existingProductSecondCategoryDetail)) {
            productSecondCategoryDetailRepository.deleteAll(existingProductSecondCategoryDetail);
        }

        if (!CollectionUtils.isEmpty(request.getProductSecondCategoryIds())) {
            List<ProductSecondCategoryDetail> productSecondCategoryDetails = generateProductSecondCategoryDetailList(
                    request.getProductSecondCategoryIds(),
                    storedProduct
            );

            productSecondCategoryDetailRepository.saveAll(productSecondCategoryDetails);
        }

        // Prepare the product feature parameters
        List<ProductFeatureRequest> productFeatureRequests = request.getProductFeatureParameters();
        if (!CollectionUtils.isEmpty(productFeatureRequests)) {
            // delete the existing product features
            productFeatureRepository.deleteAll(product.getProductFeaturesList());
            List<ProductFeatures> productFeatures = productFeatureRequests
                    .stream().map(pfr -> new ProductFeatures(storedProduct, pfr.getFeatureLabel(),
                            pfr.getFeatureValue(), pfr.getFeatureSequence(), countryCode, 1, pfr.getSellingPoint()))
                    .collect(Collectors.toList());
            productFeatureRepository.saveAll(productFeatures);

            // Process product features, if label name not exists, save into product_tech_feature table
            List<ProductTechnicalFeatureLabel> productFeatureLabelList = processProductFeaturesLabel(productFeatures, currentAccountId, request.getLangCode());
            productTechnicalFeatureLabelRepository.saveAll(productFeatureLabelList);
        }

        // Prepare the product technical parameters
        List<ProductTechnicalRequest> productTechnicalRequests = request.getProductTechnicalParameters();

        if (!CollectionUtils.isEmpty(productTechnicalRequests)) {
            // delete the existing product technical
            productTechnicalRepository.deleteAll(product.getProductTechnicalList());
            List<ProductTechnical> productTechnicals = productTechnicalRequests.stream()
                    .map(ptr -> new ProductTechnical(storedProduct, ptr.getTechnicalLabel(), ptr.getTechnicalValue(),
                            ptr.getTechnicalSequence(), countryCode, 1, ptr.getSellingPoint()))
                    .collect(Collectors.toList());
            productTechnicalRepository.saveAll(productTechnicals);

            // Process product technicals, if label name not exists, save into product_tech_feature table
            List<ProductTechnicalFeatureLabel> productTechnicalLabelList = processProductTechnicalsLabel(productTechnicals, currentAccountId, request.getLangCode());
            productTechnicalFeatureLabelRepository.saveAll(productTechnicalLabelList);
        }

        //ProductMedia PROCESS
        String langCode = LangHelper.getLangCode(request.getLangCode());
        List<ProductMedia> productMediasTobeInserted = new ArrayList<ProductMedia>();
        List<ProductMedia> existingProductMedias = productMediaRepository.findByProductId(product.getId());
        List<ProductMedia> videos = existingProductMedias.stream()
                .filter(pm -> pm.getDocumentType().equalsIgnoreCase(MEDIA_TYPE.VIDEO.getType()))
                .collect(Collectors.toList());
        List<ProductMedia> brouchures = existingProductMedias.stream()
                .filter(pm -> pm.getDocumentType().equalsIgnoreCase(MEDIA_TYPE.BROCHURE.getType()))
                .collect(Collectors.toList());
        List<ProductMedia> images = existingProductMedias.stream()
                .filter(pm -> pm.getDocumentType().equalsIgnoreCase(MEDIA_TYPE.IMAGE.getType()))
                .collect(Collectors.toList());

        // video storing
        if (videoFile != null) {
            productMediaRepository.deleteAll(videos);
            productMediasTobeInserted.add(uploadAttachment(storedProduct, langCode, videoFile, MEDIA_TYPE.VIDEO));
        }
        // brochure storing
        if (brochureFile != null) {
            productMediaRepository.deleteAll(brouchures);
            productMediasTobeInserted.add(uploadAttachment(storedProduct, langCode, brochureFile, MEDIA_TYPE.BROCHURE));
        }
        // images storing
        if (!CollectionUtils.isEmpty(imageFiles)) {
            productMediaRepository.deleteAll(images);

            productMediasTobeInserted = generateProductMediaList(productMediasTobeInserted, imageFiles, product, langCode);
        }

        if (!CollectionUtils.isEmpty(productMediasTobeInserted)) {
            productMediaRepository.saveAll(productMediasTobeInserted);
        }

        // get country by country code given by product request and set the country into countryResponse
        CountryResponse manufacturerCountryResponse = generateCountryResponseByCountryCode(request);

        // get province by province code given by product request and set the province into provinceResponse
        ProvinceResponse manufacturerProvinceResponse = generateProvinceResponseByProvinceCode(request);

        // set the country response, province response and product hierarchy response into product response
        String ossBucketDomain = env.getProperty(PropertiesLabelConst.OSS_BUCKET_DOMAIN);
        ProductResponse productResponse = new ProductResponse(product, ossBucketDomain);
        productResponse.setManufacturerCountry(manufacturerCountryResponse);
        productResponse.setManufacturerProvince(manufacturerProvinceResponse);
        productResponse.setProductHierarchy(productHierarchyResponse);

        /**
         * Get product status to do validation due to
         * product auto approval will still create product task list
         */
        if (product.getStatus() == StatusEnum.PENDING_FOR_REVIEW.getCode()) {
            //CREATE REVIEW TASK
            taskService.createProductReviewTask(product);
        }

        if (product.getStatus() == StatusEnum.PENDING_FOR_APPROVAL.getCode()) {
            taskService.createProductApproveTask(product);
            processEmailAndWebNotificationRequestToApproveProductToVendorSenior(product, request.getLoginId());
        }

        loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, UPDATE_DRAFT_OF_PENDING_METHOD_NAME, request.getLoginId());
        return productResponse;
    }

    private void processEmailAndWebNotificationRequestToApproveProductToVendorSenior(Product product, String loginId) throws IdsmedBaseException {
        // ------------------ EMAIL AND WEB NOTIFICATION TO REQUEST VENDOR'S SENIOR TO APPROVE PRODUCT -----------------
        Pair<String, String> emailPair = null;
        Optional<IdsmedAccount> accountOptional = idsmedAccountRepository.findById(product.getCreatedBy());
        if (accountOptional.isPresent()) {
            IdsmedAccount idsmedAccount = accountOptional.get();
            IdsmedUser productCreatorUser = idsmedUserRepository.findIdsmedUserByIdsmedAccounts(idsmedAccount);
            IdsmedRole role = idsmedRoleRepository.findByCode(IdsmedRoleCodeConst.VENDOR_SENIOR);
            List<IdsmedUser> vendorSeniors = idsmedUserRepository.findVendorSeniorByCompanyCodeAndRole(
                    productCreatorUser.getCompanyCode(), role);

            if (!CollectionUtils.isEmpty(vendorSeniors)) {
                emailPair = processEmailNotificationRequestToApproveProductToVendorSenior(product, vendorSeniors);
            }

            // WEB NOTIFICATION TO REQUEST VENDOR'S SENIOR TO APPROVE PRODUCT
            IdsmedAccount sender = idsmedAccountRepository.findByLoginId(loginId);
            processWebNotificationRequestToApproveProductToVendorSenior(sender, vendorSeniors, emailPair);
        }
    }

    private List<ProductDocumentAttachment> updateProductDocumentAttachmentWithUploadFile(List<ProductDocumentAttachmentRequest> fileAttachmentRequests
            , List<MultipartFile> files
            , Product product
            , String countryCode
            , String langCode) throws IdsmedBaseException {
        List<ProductDocumentAttachment> fileAttachments = new ArrayList<>();
        if (!CollectionUtils.isEmpty(fileAttachmentRequests)) {
            for (ProductDocumentAttachmentRequest attachmentRequest : fileAttachmentRequests) {

                ProductDocumentAttachment attachment = productDocumentAttachmentRepository.getOne(attachmentRequest.getId());
                attachment.setProduct(product);
                attachment.setCountryCode(countryCode);

                if (attachmentRequest.getFileIndex() != null) {
                    MultipartFile file = files.get(attachmentRequest.getFileIndex());
                    String relativePath = uploadProductDocumentAttachmentFile(langCode, file);
                    attachment.setDocumentPath(relativePath);
                }

                attachment.setCountryCode(countryCode);
                attachment.setDocumentName(attachmentRequest.getDocumentName());
                attachment.setDocumentSecondaryName(attachmentRequest.getDocumentSecondaryName());
                attachment.setOriginalFileName(attachmentRequest.getOriginalFileName());
                attachment.setExpiryDate(attachmentRequest.getExpiryDate() != null ? DateTimeHelper.convertZonedDateTimeToUTC(attachmentRequest.getExpiryDate()) : null);
                attachment.setExpDateTimeZone(attachmentRequest.getExpDateTimeZone());
                attachment.setVerify(attachmentRequest.getVerify());
                attachment.setStatus(attachmentRequest.getStatus());

                fileAttachments.add(attachment);
            }
        }

        return fileAttachments;
    }

    private ProductMarketingResponse getProductMarketing(Long productId) {
        ProductMarketingResponse pdr = new ProductMarketingResponse();

        List<ProductSellingRate> productSellingRateRes = productSellingRateRepository.findTopBestSeller(productId, BEST_SELLER_TOP_NUMBER);
        List<ProductSellingRateResponse> psrrs = new ArrayList<ProductSellingRateResponse>();
        if (!CollectionUtils.isEmpty(productSellingRateRes)) {

            productSellingRateRes.stream().forEach(psr -> {
                Optional<Product> tp = productRepository.findById(psr.getRelatedProductId());

                if (tp.isPresent() && tp.get().getStatus() == StatusEnum.APPROVED.getCode()) {
                    psrrs.add(new ProductSellingRateResponse(psr, tp.get().getProductNamePrimary(), getImageUrlsOfProduct(tp)));
                }
            });
            pdr.setProductSellingRates(psrrs);
        }

        List<RelatedProduct> relatedProducts = relatedProductRepository.findByProductId(productId);
        List<RelatedProductResponse> rprs = new ArrayList<RelatedProductResponse>();
        if (!CollectionUtils.isEmpty(relatedProducts)) {

            relatedProducts.stream().forEach(rp -> {
                Optional<Product> tp = productRepository.findById(rp.getRelatedProductId());
                if (tp.isPresent() && tp.get().getStatus() == StatusEnum.APPROVED.getCode()) {
                    rprs.add(new RelatedProductResponse(rp, tp.get().getProductNamePrimary(), getImageUrlsOfProduct(tp)));
                }
            });

            pdr.setRelatedProducts(rprs);
        }

        return pdr;

    }

    private ProductMarketingResponse getProductMarkertingDetails(Long userId, Long productId) throws IdsmedBaseException {

        ProductMarketingResponse pdr = getProductMarketing(productId);

        List<ProductSellingHistory> productSellingHitories = productSellingHistoryRepository.findByUserIdAndProductId(userId, productId);
        List<ProductSellingHistoryResponse> pshs = new ArrayList<ProductSellingHistoryResponse>();
        if (!CollectionUtils.isEmpty(productSellingHitories)) {
            productSellingHitories.stream().forEach(psh -> {
                Optional<Product> product = productRepository.findById(psh.getRelatedProductId());
                if (product.isPresent() && product.get().getStatus() == StatusEnum.APPROVED.getCode()) {
                    List<String> mediaUrls = getImageUrlsOfProduct(product);
                    pshs.add(new ProductSellingHistoryResponse(psh, product.get().getProductNamePrimary(), mediaUrls));
                }
            });
            pdr.setProductSellingHistories(pshs);
        }

        return pdr;
    }

    private ProductMarketingResponse getProductMarkertingDetailsForWedoctor(Long wedoctorCustomerId, Long productId) {
        ProductMarketingResponse pdr = getProductMarketing(productId);
        List<ProductSellingHistory> productSellingHitories = productSellingHistoryRepository.findByWedoctorCustomerIdAndProductId(wedoctorCustomerId, productId);
        List<ProductSellingHistoryResponse> pshs = new ArrayList<ProductSellingHistoryResponse>();
        if (!CollectionUtils.isEmpty(productSellingHitories)) {
            productSellingHitories.stream().forEach(psh -> {
                Optional<Product> product = productRepository.findById(psh.getRelatedProductId());
                if (product.isPresent() && product.get().getStatus() == StatusEnum.APPROVED.getCode()) {
                    List<String> mediaUrls = getImageUrlsOfProduct(product);
                    pshs.add(new ProductSellingHistoryResponse(psh, product.get().getProductNamePrimary(), mediaUrls));
                }
            });
            pdr.setProductSellingHistories(pshs);
        }

        return pdr;
    }


    private List<String> getImageUrlsOfProduct(Optional<Product> product) {
        List<String> mediaUrls = new ArrayList<String>();

        if (product.isPresent() && product.get().getStatus() == StatusEnum.APPROVED.getCode()) {
            List<ProductMedia> medias = product.get().getProductMediaList();
            if (!CollectionUtils.isEmpty(medias)) {
                medias.stream().forEach(m -> {
                    if (m.getDocumentType().equalsIgnoreCase("img")) {
                        mediaUrls.add(m.getDocumentUrl());
                    }
                });
            }
        }
        return mediaUrls;
    }

    public List<KeywordHistoryResponse> getLastProductSearchKeyword(Long userId, Integer number) throws IdsmedBaseException {
        loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, GET_LAST_PRODUCT_SEARCH_KEYWORD_METHOD_NAME, null);
        List<KeywordHistoryResponse> khrs = new ArrayList<KeywordHistoryResponse>();
        List<KeywordHistory> khs = keywordHistoryRepository.findLastNKeywordByUserId(userId, number == null ? NUMBER_OF_LAST_KEYWORD : number);
        if (!CollectionUtils.isEmpty(khs)) {
            khrs = khs.stream().map(kh -> new KeywordHistoryResponse(kh.getKeyword())).collect(Collectors.toList());
        }
        loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, GET_LAST_PRODUCT_SEARCH_KEYWORD_METHOD_NAME, null);
        return khrs;
    }

    @Override
    @PreAuthorize("@PermissionService.isMethodPermited(authentication, {'API0000008', 'PP0000001', 'PS10000001'})")
    public ProductApiListResponse getProductListFilter(Long clientId, String userId, String type, Integer pageIndex, Integer pageSize) throws IdsmedBaseException {
        loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, GET_PRODUCT_LIST_FILTER_METHOD_NAME, null);
        if (type.equalsIgnoreCase(SearchingHistoryUserProductTypeEnum.NEW.getType())) {
            return getProductListForNew(userId, pageIndex, pageSize);
        }

        if (type.equalsIgnoreCase(SearchingHistoryUserProductTypeEnum.HYSTORY.getType())) {
            return getProductListForHistory(clientId, userId, pageIndex, pageSize);
        }

        if (type.equalsIgnoreCase(SearchingHistoryUserProductTypeEnum.BEST.getType())) {
            return getProductListForBest(userId, pageIndex, pageSize);
        }
        loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, GET_PRODUCT_LIST_FILTER_METHOD_NAME, null);
        return null;
    }

    @Override
    public ProductApiListResponse getProductListForNew(String userId, Integer pageIndex, Integer pageSize) throws IdsmedBaseException {

        Pair<Integer, List<Product>> productsPair = productRepository.findAllProductFilterByApproveProductOrderByApprovedDateDesc(pageIndex, pageSize);
        generateHistoryUserProductListNewResponse(productsPair, pageIndex, pageSize);

        webServiceLogService.StoreInWebServiceLogDatabase(httpServletRequest, MethodNameEnum.GETPRODUCTLISTFILTER.getName(), HTTPServletReturnStatusEnum.AUTHORIZED.getCode());

        return generateHistoryUserProductListNewResponse(productsPair, pageIndex, pageSize);
    }

    @Override
    public ProductApiListResponse getProductListForHistory(Long clientId, String userId, Integer pageIndex, Integer pageSize) throws IdsmedBaseException {
        Pageable pageable = null;
        if (pageIndex != null && pageSize != null) {
            pageable = PageRequest.of(pageIndex, pageSize);
        }

        Pair<Integer, List<Product>> productsPair = productRepository.findAllProductBasedOnSearchHistory(clientId, userId, pageable);

        webServiceLogService.StoreInWebServiceLogDatabase(httpServletRequest, MethodNameEnum.GETPRODUCTLISTFILTER.getName(), HTTPServletReturnStatusEnum.AUTHORIZED.getCode());

        return generateHistoryUserProductListNewResponse(productsPair, pageIndex, pageSize);

    }

    @Override
    public ProductApiListResponse getProductListForBest(String userId, Integer pageIndex, Integer pageSize) throws IdsmedBaseException {
        Pageable pageable = null;
        if (pageIndex != null && pageSize != null) {
            pageable = PageRequest.of(pageIndex, pageSize);
        }

        Pair<Integer, List<Product>> productsPair = productRepository.findAllProductBasedOnBestSeller(pageable);

        webServiceLogService.StoreInWebServiceLogDatabase(httpServletRequest, MethodNameEnum.GETPRODUCTLISTFILTER.getName(), HTTPServletReturnStatusEnum.AUTHORIZED.getCode());

        return generateHistoryUserProductListNewResponse(productsPair, pageIndex, pageSize);

    }

    @SuppressWarnings("deprecation")
    @Override
   @PreAuthorize("@PermissionService.isMethodPermited(authentication, {'API0000009'})")
    public ProductApiListResponse getProductSearchByStr(Long wedoctorCustomerId, String keyword, String brand, Integer pageIndex, Integer pageSize, String langCode) throws IdsmedBaseException {
        loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, GET_PRODUCT_SEARCH_BY_STR_METHOD_NAME, null);
        ProductApiListResponse response = null;
        List<ProductApiResponse> responseList = new ArrayList<>();

        webServiceLogService.StoreInWebServiceLogDatabase(httpServletRequest, MethodNameEnum.GETPRODUCTSEARCHBYSTR.getName(), HTTPServletReturnStatusEnum.AUTHORIZED.getCode());

        if (StringUtils.isEmpty(keyword)) {
            throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.KEYWORD_IS_EMPTY_ERROR, LangHelper.getLangCode(langCode)));
        }

        String decodeKeyword = null;
        if (!StringUtils.isEmpty(keyword)) {
            decodeKeyword = URLDecoder.decode(keyword);
        }

        Pair<Integer, List<Product>> product = null;
        Pageable pageable = null;
        if (pageIndex != null && pageSize != null) {
            pageable = PageRequest.of(pageIndex, pageSize);
        }

        if (StringUtils.isEmpty(brand)) {
            product = productRepository.fuzzySearchProduct(decodeKeyword, null, null, null, null, pageable, null, null);
        }

        if (!StringUtils.isEmpty(brand)) {
            ProductBrand productBrand = productBrandRepository.findByBrandName(brand);
            if (productBrand == null) {
                response = new ProductApiListResponse(pageIndex, pageSize);
                return response;
            }
            product = productRepository.fuzzySearchProduct(decodeKeyword, null, productBrand, null, null, pageable, null, null);
        }

        String ossBucketDomain = env.getProperty(PropertiesLabelConst.OSS_BUCKET_DOMAIN);
        for (Product products : product.getSecond()) {
            ProductApiResponse productApiResponse = new ProductApiResponse(products, ossBucketDomain);
            responseList.add(productApiResponse);
        }

        Integer totalCount = product.getFirst();
        if (pageIndex != null && pageSize != null) {
            response = new ProductApiListResponse(totalCount, pageIndex, pageSize, responseList);
        } else {
            response = new ProductApiListResponse(totalCount, responseList);
        }

        //Save keyword to keyword_history
        if (!StringUtils.isEmpty(decodeKeyword)) {
            logSearchingForWeDoctor(wedoctorCustomerId, decodeKeyword, product.getSecond(),
                    null, null, null);
        }
        loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, GET_PRODUCT_SEARCH_BY_STR_METHOD_NAME, null);
        return response;
    }

    @PreAuthorize("@PermissionService.isMethodPermited(authentication, {'API0000005'})")
    public ProductListResponse getProductListOfNewRecommend(Integer duration, Integer pageIndex, Integer pageSize, String langCode) throws IdsmedBaseException {
        loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, GET_PRODUCT_LIST_OF_NEW_RECOMMEND_METHOD_NAME, null);

        pageIndex = pageIndex == null ? PAGE_INDEX_DEFAULT : (pageIndex - 1 < 0 ? 0 : pageIndex - 1);
        pageSize = pageSize == null ? PAGE_SIZE_DEFAULT : pageSize;

        LocalDateTime currentDateTime = DateTimeHelper.getCurrentTimeUTC();
        LocalDateTime nDayBefore = DateTimeHelper.addTimeInUTC(currentDateTime, DateTimeHelper.HOUR, -duration * 24);

        List<Product> products = productRepository.findByStatusAndApprovedDateGreaterThanOrderByApprovedDateDesc(StatusEnum.APPROVED.getCode(), nDayBefore);

        ProductListResponse productListResponse = new ProductListResponse(pageIndex, pageSize);

        webServiceLogService.StoreInWebServiceLogDatabase(httpServletRequest, MethodNameEnum.GETPRODUCTLISTOFNEWRECOMMEND.getName(), HTTPServletReturnStatusEnum.AUTHORIZED.getCode());

        if (CollectionUtils.isEmpty(products)) {
            return productListResponse;
        }

        int productCount = CollectionUtils.isEmpty(products) ? 0 : products.size();

        Integer pageCount = pageSize >= productCount ? 1
                : (productCount % pageSize > 0 ? productCount / pageSize + 1 : productCount / pageSize);
        Integer pageStartIndex = pageIndex * pageSize >= productCount ? (pageCount - 1) * pageSize
                : pageIndex * pageSize;
        Integer pageLastIndex = (pageIndex + 1) * pageSize >= productCount ? productCount : (pageIndex + 1) * pageSize;

        List<ProductResponse> productResponses = generateProductResponseList(products, pageStartIndex, pageLastIndex);
        productListResponse = new ProductListResponse(productCount, pageIndex, pageSize,
                productResponses);

        loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, GET_PRODUCT_LIST_OF_NEW_RECOMMEND_METHOD_NAME, null);

        return productListResponse;
    }

    @Override
    @PreAuthorize("@PermissionService.isMethodPermited(authentication, {'API0000003'})")
    public SimpleProductListResponse searchSimpleProductByKey(String keyword, Long wedoctorCustomerId, Long careArea, Long category, Long brand, Integer pageIndex, Integer pageSize)
            throws IdsmedBaseException {
        loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, SEARCH_SIMPLE_PRODUCT_BY_KEY_METHOD_NAME, null);

        String ossBucketDomain = env.getProperty(PropertiesLabelConst.OSS_BUCKET_DOMAIN);
        Pageable pageable = null;
        if (pageIndex != null && pageSize != null) {
            pageable = PageRequest.of(pageIndex, pageSize);
        }

        ProductCareArea careAreaEntity = null;
        ProductCategory productCategoryEntity = null;
        ProductBrand productBrandEntity = null;

        if (careArea != null) {
            Optional<ProductCareArea> productCareAreaOpt = productCareAreaRepository.findById(careArea);
            if (productCareAreaOpt.isPresent()) {
                careAreaEntity = productCareAreaOpt.get();
            }
        }
        if (category != null) {
            productCategoryEntity = productCategoryRepository.getOne(category);
        }
        if (brand != null) {
            productBrandEntity = productBrandRepository.getOne(brand);
        }

        Pair<Integer, List<Product>> pair = null;
        if (careAreaEntity == null) {
            pair = productRepository.fuzzySearchProduct(keyword, productCategoryEntity, productBrandEntity, null, null, pageable, null, null);
        } else {
            pair = productRepository.fuzzySearchAdvanceProductCareAreaSearchField(keyword, careAreaEntity, productCategoryEntity, productBrandEntity, null, null, pageable, null, null);
        }

        SimpleProductListResponse productListResponse = new SimpleProductListResponse(pageIndex, pageSize);

        if (!StringUtils.isEmpty(keyword)) {
            logSearchingForWeDoctor(wedoctorCustomerId, keyword, pair.getSecond(),
                    null, null, null);
        }


        //SAVE TO web_service_log
        webServiceLogService.StoreInWebServiceLogDatabase(httpServletRequest, MethodNameEnum.GETPRODUCTSEARCHBYKEY.getName(), HTTPServletReturnStatusEnum.AUTHORIZED.getCode());

        if (CollectionUtils.isEmpty(pair.getSecond())) {
            return productListResponse;
        }

        List<SimpleProductSearchResponse> productResponses = pair.getSecond().stream().map(p -> new SimpleProductSearchResponse(p, ossBucketDomain)).collect(Collectors.toList());

        if (pageIndex != null && pageSize != null) {
            productListResponse = new SimpleProductListResponse(pair.getFirst(), pageIndex, pageSize, productResponses);
        } else {
            productListResponse = new SimpleProductListResponse(pair.getFirst(), productResponses);
        }
        loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, SEARCH_SIMPLE_PRODUCT_BY_KEY_METHOD_NAME, null);
        return productListResponse;
    }


    @Override
    @PreAuthorize("@PermissionService.isMethodPermited(authentication, {'API0000011'})")
    public SimpleProductResponse getSimpleProductDetailById(Long id, Long customerId, String langCode) throws IdsmedBaseException {
        loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, GET_SIMPLE_PRODUCT_DETAIL_BY_ID_METHOD_NAME, null);
        Optional<Product> product = productRepository.findById(id);
        String ossBucketDomain = env.getProperty(PropertiesLabelConst.OSS_BUCKET_DOMAIN);
        webServiceLogService.StoreInWebServiceLogDatabase(httpServletRequest, MethodNameEnum.GETSIMPLEPRODUCTDETAIL.getName(), HTTPServletReturnStatusEnum.AUTHORIZED.getCode());

        if (!product.isPresent()) {
            throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.PRODUCT_NOT_FOUND_ERROR, LangHelper.getLangCode(langCode)));
        }

        if (!CollectionUtils.isEmpty(product.get().getProductCareAreaList())) {
            sortProductCareAreaDetail(product.get().getProductCareAreaList());
        }

        if (product.isPresent()) {
            Product productOpt = product.get();
            processProductSearchFrequencyByCustomer(customerId, productOpt);
        }
        loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, GET_SIMPLE_PRODUCT_DETAIL_BY_ID_METHOD_NAME, null);
        return new SimpleProductResponse(product.get(), ossBucketDomain);
    }

    // Update searched frequency of a particular product by customer (wedoctor's customer)
    private void processProductSearchFrequencyByCustomer(Long customerId, Product product) throws IdsmedBaseException {
        if (customerId != null) {
            HistoryUserProductBySearch productBySearch = historyUserProductRepository.findFirstByCustomerIdAndProductId(customerId, product.getId());

            if (productBySearch != null) {
                Integer frequency = productBySearch.getFrequency() + 1;
                productBySearch.setFrequency(frequency);
                historyUserProductRepository.save(productBySearch);
            } else {
                HistoryUserProductBySearch historyUserProductBySearch = new HistoryUserProductBySearch(null, null, customerId, product, 1);
                historyUserProductRepository.save(historyUserProductBySearch);
            }
        } else {
            HistoryUserProductBySearch productBySearch = historyUserProductRepository.findFirstByProductAndCustomerIdIsNull(product);
            if (productBySearch != null) {
                Integer frequency = productBySearch.getFrequency() + 1;
                productBySearch.setFrequency(frequency);
                historyUserProductRepository.save(productBySearch);
            } else {
                HistoryUserProductBySearch historyUserProductBySearch = new HistoryUserProductBySearch(null, null, null, product, 1);
                historyUserProductRepository.save(historyUserProductBySearch);

            }

        }
    }

    private ProductApiListResponse generateHistoryUserProductListNewResponse(Pair<Integer
            , List<Product>> productsPair
            , Integer pageIndex
            , Integer pageSize) {
        Integer productCount = productsPair.getFirst();
        String ossBucketDomain = env.getProperty(PropertiesLabelConst.OSS_BUCKET_DOMAIN);
        List<ProductApiResponse> productApiResponses = productsPair.getSecond()
                .stream()
                .map(p -> new ProductApiResponse(p, ossBucketDomain)).collect(Collectors.toList());

        ProductApiListResponse productApiListResponse = new ProductApiListResponse(pageIndex, pageSize);
        if (pageIndex != null && pageSize != null) {
            productApiListResponse = new ProductApiListResponse(productCount, pageIndex, pageSize, productApiResponses);
        } else {
            productApiListResponse = new ProductApiListResponse(productCount, productApiResponses);
        }

        return productApiListResponse;
    }

    public List<ProductResponse> generateProductResponseList(List<Product> products, Integer pageStartIndex, Integer pageLastIndex) {
        List<ProductResponse> productResponses = products.subList(pageStartIndex, pageLastIndex).stream().map(p -> {
            String companyPrimaryName = p.getVendor().getCompanyNamePrimary();

            sortProductFeatures(p.getProductFeaturesList());
            sortProductTechnicals(p.getProductTechnicalList());
            String ossBucketDomain = env.getProperty(PropertiesLabelConst.OSS_BUCKET_DOMAIN);
            ProductResponse ps = new ProductResponse(p, ossBucketDomain);
            ps.setCompanyPrimaryName(StringUtils.isEmpty(companyPrimaryName) ? "" : companyPrimaryName);
            return ps;
        }).collect(Collectors.toList());

        return productResponses;
    }

    public List<ProductWedoctorResponse> generateProductWedoctorResponseList(List<Product> products, Integer pageStartIndex, Integer pageLastIndex) {
        String ossBucketDomain = env.getProperty(PropertiesLabelConst.OSS_BUCKET_DOMAIN);
        List<ProductWedoctorResponse> productWedoctorResponses = products.subList(pageStartIndex, pageLastIndex).stream().map(p -> {
            String companyPrimaryName = p.getVendor().getCompanyNamePrimary();

            sortProductFeatures(p.getProductFeaturesList());
            sortProductTechnicals(p.getProductTechnicalList());

            ProductWedoctorResponse ps = new ProductWedoctorResponse(p, ossBucketDomain);
            ps.setCompanyPrimaryName(StringUtils.isEmpty(companyPrimaryName) ? "" : companyPrimaryName);
            return ps;
        }).collect(Collectors.toList());

        return productWedoctorResponses;
    }



    public List<ProductResponse> generateProductResponseList(List<Product> products) {
        List<ProductResponse> productResponses = products.stream().map(p -> {
            sortProductFeatures(p.getProductFeaturesList());
            sortProductTechnicals(p.getProductTechnicalList());
            String ossBucketDomain = env.getProperty(PropertiesLabelConst.OSS_BUCKET_DOMAIN);
            ProductResponse ps = new ProductResponse(p, ossBucketDomain);
            return ps;
        }).collect(Collectors.toList());

        return productResponses;
    }

    public List<ProductResponse> generateProductResponseListForWedoctor(List<Product> products, Long wedoctorCustomerId) {
        String ossBucketDomain = env.getProperty(PropertiesLabelConst.OSS_BUCKET_DOMAIN);
        List<ProductResponse> productResponses = products.stream().map(p -> {
            sortProductFeatures(p.getProductFeaturesList());
            sortProductTechnicals(p.getProductTechnicalList());
            ProductResponse ps = new ProductResponse(p, ossBucketDomain);

            if (wedoctorCustomerId != null) {
                ProductWishlistDetail productWishlistDetail = productWishlistDetailRepository.findFirstByProductAndWedoctorCustomerId(p, wedoctorCustomerId);

                ProductWishlistResponse wishlistResponse = null;
                if (productWishlistDetail != null) {
                    wishlistResponse = new ProductWishlistResponse(p, true);
                } else {
                    wishlistResponse = new ProductWishlistResponse(p, false);
                }

                ps = new ProductResponse(p, wishlistResponse, ossBucketDomain);
            } else {

            }

            return ps;
        }).collect(Collectors.toList());

        return productResponses;
    }

    public List<ProductResponse> generateProductResponseList(List<Product> products, String loginId) {
        String ossBucketDomain = env.getProperty(PropertiesLabelConst.OSS_BUCKET_DOMAIN);
        List<ProductResponse> productResponses = products.stream().map(p -> {
            sortProductFeatures(p.getProductFeaturesList());
            sortProductTechnicals(p.getProductTechnicalList());

            IdsmedAccount idsmedAccount = idsmedAccountRepository.findByLoginId(loginId);
            ProductWishlistDetail productWishlistDetail = productWishlistDetailRepository.findByProductAndAccount(p, idsmedAccount);

            ProductWishlistResponse wishlistResponse = null;
            if (productWishlistDetail != null) {
                wishlistResponse = new ProductWishlistResponse(p, true);
            } else {
                wishlistResponse = new ProductWishlistResponse(p, false);
            }

            ProductResponse ps = new ProductResponse(p, wishlistResponse, ossBucketDomain);
            return ps;
        }).collect(Collectors.toList());

        return productResponses;
    }

    public List<ProductCareAreaDetail> generateProductCareAreaDetailList(List<Long> productCareAreaList, Product storedProduct) {
        List<ProductCareAreaDetail> productCareAreaDetails = new ArrayList<>();
        for (Long careAreaId : productCareAreaList) {
            if (careAreaId == null) {
                continue;
            }
            ProductCareArea productCareArea = productCareAreaRepository.findFirstById(careAreaId);
            ProductCareAreaDetail productCareAreaDetail = new ProductCareAreaDetail(productCareArea, storedProduct);
            productCareAreaDetails.add(productCareAreaDetail);
        }
        return productCareAreaDetails;
    }

    public List<ProductSecondCategoryDetail> generateProductSecondCategoryDetailList(List<Long> productSecondCategoryIds, Product product) {
        List<ProductSecondCategoryDetail> productSecondCategoryDetails = new ArrayList<>();
        for (Long productSecondCategoryId : productSecondCategoryIds) {
            if (productSecondCategoryId == null) {
                continue;
            }

            ProductSecondCategory productSecondCategory = productSecondCategoryRepository.findFirstById(productSecondCategoryId);
            ProductSecondCategoryDetail productSecondCategoryDetail = new ProductSecondCategoryDetail(productSecondCategory, product);
            productSecondCategoryDetails.add(productSecondCategoryDetail);
        }

        return productSecondCategoryDetails;
    }


    @Override
    @Transactional
    public List<ProductDocumentAttachmentResponse> editProductDocumentAttachment(
            ProductDocumentAttachmentEditRequest productDocumentAttachmentEditRequest,
            List<MultipartFile> documentAttachments) throws IdsmedBaseException {

        loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, EDIT_PRODUCT_DOCUMENT_ATTACHMENT_METHOD_NAME, productDocumentAttachmentEditRequest.getLoginId());
        String ossBucketDomain = env.getProperty(PropertiesLabelConst.OSS_BUCKET_DOMAIN);

        Optional<Product> productOpt = productRepository.findById(productDocumentAttachmentEditRequest.getProductId());
        Long currentAccountId = idsmedAccountRepository.findByLoginId(productDocumentAttachmentEditRequest.getLoginId()).getId();
        List<ProductDocumentAttachmentRequest> productDocumentAttachmentRequests = productDocumentAttachmentEditRequest.getProductDocumentAttachmentRequestList();
        String langCode = LangHelper.getLangCode(productDocumentAttachmentEditRequest.getLangCode());
        if (!productOpt.isPresent()) {
            throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.PRODUCT_NOT_FOUND_ERROR, langCode));
        }

        List<ProductDocumentAttachmentRequest> newProductDocumentAttachmentRequests = productDocumentAttachmentRequests
                .stream()
                .filter(r -> r.getId() == null)
                .collect(Collectors.toList());

        List<ProductDocumentAttachmentRequest> existingProductDocumentAttachmentRequests = productDocumentAttachmentRequests
                .stream()
                .filter(r -> r.getId() != null)
                .collect(Collectors.toList());

        Pair<List<ProductDocumentAttachment>, List<ProductDocumentAttachmentReminder>> newProductDocumentAttachmentPair = setProductDocumentAttachmentForEdit(productOpt.get(), newProductDocumentAttachmentRequests, langCode, documentAttachments
                , productOpt.get().getVendor().getCountryCode(), currentAccountId);

        Pair<List<ProductDocumentAttachment>, List<ProductDocumentAttachmentReminder>> existingProductDocumentAttachmentPair = updateProductDocumentAttachment(existingProductDocumentAttachmentRequests, documentAttachments, langCode,
                currentAccountId, productOpt.get().getVendor().getCountryCode());

        List<ProductDocumentAttachment> existingProductDocumentAttachments = existingProductDocumentAttachmentPair.getFirst();
        List<ProductDocumentAttachmentReminder> existingProductDocumentAttachmentReminders = existingProductDocumentAttachmentPair.getSecond();

        List<ProductDocumentAttachment> productDocumentAttachments = new ArrayList<ProductDocumentAttachment>();

        if (!CollectionUtils.isEmpty(newProductDocumentAttachmentPair.getFirst())) {
            existingProductDocumentAttachments.addAll(newProductDocumentAttachmentPair.getFirst());
        }

        if (!CollectionUtils.isEmpty(newProductDocumentAttachmentPair.getSecond())) {
            existingProductDocumentAttachmentReminders.addAll(newProductDocumentAttachmentPair.getSecond());
        }

        if (!CollectionUtils.isEmpty(existingProductDocumentAttachments)) {
            existingProductDocumentAttachments = productDocumentAttachmentRepository.saveAll(existingProductDocumentAttachments);
        }

        if (!CollectionUtils.isEmpty(existingProductDocumentAttachmentReminders)) {
            productDocumentAttachmentReminderRepository.saveAll(existingProductDocumentAttachmentReminders);
        }

        if (!CollectionUtils.isEmpty(productDocumentAttachments)) {
            String attachmentDocumentName = productDocumentAttachments
                    .stream()
                    .map(pda -> pda.getDocumentName() + "; ")
                    .collect(Collectors.joining());

            // Get product attachment status
            String fileStatus = null;
            for (FileAttachmentStatusEnum fileAttachmentStatusEnum : FileAttachmentStatusEnum.values()) {
                if (fileAttachmentStatusEnum.getCode().equals(productDocumentAttachments.get(0).getStatus())) {
                    fileStatus = fileAttachmentStatusEnum.getName();
                }
            }

            //--------------- PRODUCT CERTIFICATE APPROVAL SEND EMAIL AND WEB NOTIFICATION START HERE -------------------------
            Product product = productOpt.get();
            List<IdsmedAccount> recipients = new ArrayList<>();

            // FIND PRODUCT CREATOR
            IdsmedAccount productCreator = idsmedAccountRepository.findById(product.getCreatedBy()).get();
            if (productCreator != null) {
                recipients.add(productCreator);
            }

            // FIND VENDOR COORDINATOR
            Vendor vendor = product.getVendor();
            if (vendor != null && vendor.getVendorCoordinator() != null) {
                IdsmedAccount coordinator = idsmedAccountRepository.findById(vendor.getVendorCoordinator()).get();
                if (coordinator != null) {
                    recipients.add(coordinator);
                }
            }

            IdsmedAccount sender = idsmedAccountRepository.findByLoginId(productDocumentAttachmentEditRequest.getLoginId());
            processEmailAndWebNotificationForProductCertificateApproval(
                    product, recipients, attachmentDocumentName, fileStatus, sender);

            if (fileStatus == FileAttachmentStatusEnum.PENDING_FOR_APPROVAL.getName()) {
                Pair<String, String> emailPair = null;
                Optional<IdsmedAccount> accountOptional = idsmedAccountRepository.findById(product.getCreatedBy());
                if (accountOptional.isPresent()) {
                    IdsmedAccount idsmedAccount = accountOptional.get();
                    IdsmedUser productCreatorUser = idsmedUserRepository.findIdsmedUserByIdsmedAccounts(idsmedAccount);
                    IdsmedRole role = idsmedRoleRepository.findByCode(IdsmedRoleCodeConst.VENDOR_SENIOR);
                    List<IdsmedUser> vendorSeniors = idsmedUserRepository.findVendorSeniorByCompanyCodeAndRole(
                            productCreatorUser.getCompanyCode(), role);

                    if (!CollectionUtils.isEmpty(vendorSeniors)) {
                        emailPair = processEmailNotificationForRequestToApproveProductCertificateToVendorSenior(product,
                                attachmentDocumentName, vendorSeniors);
                    }

                    // WEB NOTIFICATION TO REQUEST VENDOR'S SENIOR TO APPROVE PRODUCT
                    processWebNotificationForRequestToApproveProductCertificateToVendorSenior(sender, vendorSeniors, emailPair);
                }
            }

        }

        loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, EDIT_PRODUCT_DOCUMENT_ATTACHMENT_METHOD_NAME, productDocumentAttachmentEditRequest.getLoginId());

        return existingProductDocumentAttachments.stream().map(epda -> new ProductDocumentAttachmentResponse(epda, ossBucketDomain)).collect(Collectors.toList());

    }

    /* this method is update product document attachment with reupload the document attachment and update the document path in database
     *  And also update the expiry date in database
     */
    @Modifying
    private Pair<List<ProductDocumentAttachment>, List<ProductDocumentAttachmentReminder>> updateProductDocumentAttachment(List<ProductDocumentAttachmentRequest> productDocumentAttachmentRequests, List<MultipartFile> files,
                                                                                                                           String langCode, Long currentAccountId, String countryCode) throws IdsmedBaseException {
        List<ProductDocumentAttachment> productDocumentAttachmentList = new ArrayList<>();
        List<ProductDocumentAttachmentReminder> productDocumentAttachmentReminderList = new ArrayList<>();

        if (!CollectionUtils.isEmpty(productDocumentAttachmentRequests)) {
            for (ProductDocumentAttachmentRequest productDocumentAttachmentRequest : productDocumentAttachmentRequests) {
                Optional<ProductDocumentAttachment> productDocumentAttachmentOpt = productDocumentAttachmentRepository.findById(productDocumentAttachmentRequest.getId());
                if (!productDocumentAttachmentOpt.isPresent()) {
                    continue;
                }
                ProductDocumentAttachment productDocumentAttachment = productDocumentAttachmentOpt.get();

                if (productDocumentAttachmentRequest.getFileIndex() != null) {
                    MultipartFile file = files.get(productDocumentAttachmentRequest.getFileIndex());
                    productDocumentAttachment.setDocumentPath(uploadProductDocumentAttachmentFile(langCode, file));
                }

                productDocumentAttachment.setDocumentName(productDocumentAttachmentRequest.getDocumentName());
                productDocumentAttachment.setDocumentSecondaryName(productDocumentAttachmentRequest.getDocumentSecondaryName());
                productDocumentAttachment.setOriginalFileName(productDocumentAttachmentRequest.getOriginalFileName());
                productDocumentAttachment.setExpiryDate(productDocumentAttachmentRequest.getExpiryDate() != null ? DateTimeHelper.convertZonedDateTimeToUTC(productDocumentAttachmentRequest.getExpiryDate()) : null);
                productDocumentAttachment.setExpDateTimeZone(productDocumentAttachmentRequest.getExpDateTimeZone());
                productDocumentAttachment.setStatus(productDocumentAttachmentRequest.getStatus());
                productDocumentAttachment.setUpdatedBy(currentAccountId);

                //delete existing document attachment reminder and create new
                List<ProductDocumentAttachmentReminder> existingReminders = productDocumentAttachment.getProductDocumentAttachmentReminderList();
                if (!CollectionUtils.isEmpty(existingReminders)) {
                    productDocumentAttachmentReminderRepository.deleteAll(existingReminders);
                }

                List<ProductDocumentAttachmentReminderRequest> productDocumentAttachmentReminderRequests = productDocumentAttachmentRequest.getProductDocumentAttachmentReminderRequests();
                if (!CollectionUtils.isEmpty(productDocumentAttachmentReminderRequests)) {
                    for (ProductDocumentAttachmentReminderRequest productDocumentAttachmentReminderRequest : productDocumentAttachmentReminderRequests) {
                        ProductDocumentAttachmentReminder productDocumentAttachmentReminder = new ProductDocumentAttachmentReminder(productDocumentAttachmentReminderRequest);
                        productDocumentAttachmentReminder.setProductDocumentAttachment(productDocumentAttachment);
                        productDocumentAttachmentReminderList.add(productDocumentAttachmentReminder);
                    }
                }
                productDocumentAttachmentList.add(productDocumentAttachment);
            }
        }
        return Pair.of(productDocumentAttachmentList, productDocumentAttachmentReminderList);
    }

    @Override
    @Transactional
    public ProductListResponse inactiveProduct(ProductInactiveRequest request) throws IdsmedBaseException {

        loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, INACTIVE_PRODUCT_METHOD_NAME, request.getLoginId());

        // Get current login user's id
        Long currentUserId = idsmedAccountRepository.findByLoginId(request.getLoginId()).getId();

        List<Product> products = request.getId().stream()
                .filter(p -> {
                    if (productRepository.findById(p).isPresent()) return true;
                    return false;
                }).map(p -> {
                    Product product = productRepository.findById(p).get();
                    product.setUpdatedBy(currentUserId);
                    product.setPreviousStatus(
                            product.getStatus() != null ? product.getStatus() : StatusEnum.INACTIVE.getCode()
                    );
                    product.setStatus(StatusEnum.INACTIVE.getCode());
                    product.setInactiveReason(request.getInactiveReason());
                    return product;
                }).collect(Collectors.toList());

        productRepository.saveAll(products);

        // temporary comment this because just inactive product and no need to inactive the product related data, if future need, will uncomment this code.
        // if (!CollectionUtils.isEmpty(products)) {
        //     productMediaRepository.updateProductMediaStatus(products, currentUserId, new Date(), StatusEnum.ARCHIVED.getCode());
        //     productFeatureRepository.updateProductFeatureStatus(products, currentUserId, new Date(), StatusEnum.ARCHIVED.getCode());
        //     productTechnicalRepository.updateProductTechnicalStatus(products, currentUserId, new Date(), StatusEnum.ARCHIVED.getCode());
        //     productRejectRepository.updateProductRejectStatus(products, currentUserId, new Date(), StatusEnum.ARCHIVED.getCode());
        //
        // }
        String ossBucketDomain = env.getProperty(PropertiesLabelConst.OSS_BUCKET_DOMAIN);
        List<ProductResponse> productResponses = products
                .stream()
                .map(pr -> {
                    ProductResponse productResponse = new ProductResponse(pr, ossBucketDomain);
                    return productResponse;
                }).collect(Collectors.toList());

        ProductListResponse productListResponse = new ProductListResponse(productResponses);
        for(Product product : products) {
            processEmailOfInactiveOrSuspendedProductToAllVendorUser(product.getVendor(), product, request);
        }

        loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, INACTIVE_PRODUCT_METHOD_NAME, request.getLoginId());

        return productListResponse;

    }

    @Override
    public ProductGeneralInfoRateResponse productRating(ProductRatingRequest productRatingRequest) throws IdsmedBaseException {
        Long productId = productRatingRequest.getProductId();
        loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, PRODUCT_RATING_METHOD_NAME, productRatingRequest.getLoginId());
        Optional<Product> productOpt = productRepository.findById(productId);
        //check whether product is present or not
        if (!productOpt.isPresent()) {
            throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.PRODUCT_NOT_FOUND_ERROR, LangHelper.getLangCode(productRatingRequest.getLangCode())));
        }

        Product product = productOpt.get();

        Double score = productRatingRequest.getScore();

        ProductRating productRating = productRatingRepository.findByProduct(product);

        IdsmedAccount idsmedAccount = idsmedAccountRepository.findByLoginId(productRatingRequest.getLoginId());

        IdsmedUser idsmedUser = idsmedUserRepository.findUserByLoginIdAndStatus(productRatingRequest.getLoginId(), StatusEnum.APPROVED.getCode());

        //If ProductRating is null need to create one
        if (productRating == null) {
            productRating = new ProductRating(product, idsmedAccount.getId());
            productRating = productRatingRepository.save(productRating);
        }

        //Set score for product rating
        productRating = setScoreForProductRating(score, productRating);
        ProductRatingDetail productRatingDetail = new ProductRatingDetail(productRatingRequest, idsmedAccount, product);

        productRating.setStatus(StatusEnum.APPROVED.getCode());
        productRating = productRatingRepository.save(productRating);
        productRatingDetail.setProductRating(productRating);
        productRatingDetailRepository.save(productRatingDetail);
        product.setProductRating(productRating);
        productRepository.save(product);

        Long productRateId = productRating.getId() == null ? productRating.getId() : null;

        ProductGeneralInfoRateResponse productGeneralInfoRateResponse = new ProductGeneralInfoRateResponse(productRateId, productRatingDetail, idsmedUser, productRating);

        loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, PRODUCT_RATING_METHOD_NAME, productRatingRequest.getLoginId());

        return productGeneralInfoRateResponse;
    }

    private ProductRating setScoreForProductRating(Double score, ProductRating productRating) {
        //total count for zero star
        if (score.equals(RatingEnum.ZEROSTAR.getScore())) {
            productRating.setTotalCountOfZeroStar(productRating.getTotalCountOfZeroStar() + 1);
            productRating.setTotalOfScore(productRating.getTotalOfScore() + score);
        }

        //total count for one star
        if (score.equals(RatingEnum.ONESTAR.getScore())) {
            productRating.setTotalCountOfOneStar(productRating.getTotalCountOfOneStar() + 1);
            productRating.setTotalOfScore(productRating.getTotalOfScore() + score);
        }

        //total count for two star
        if (score.equals(RatingEnum.TWOSTAR.getScore())) {
            productRating.setTotalCountOfTwoStar(productRating.getTotalCountOfTwoStar() + 1);
            productRating.setTotalOfScore(productRating.getTotalOfScore() + score);
        }

        //total count for three star
        if (score.equals(RatingEnum.THREESTAR.getScore())) {
            productRating.setTotalCountOfThreeStar(productRating.getTotalCountOfThreeStar() + 1);
            productRating.setTotalOfScore(productRating.getTotalOfScore() + score);
        }

        //total count for four star
        if (score.equals(RatingEnum.FOURSTAR.getScore())) {
            productRating.setTotalCountOfFourStar(productRating.getTotalCountOfFourStar() + 1);
            productRating.setTotalOfScore(productRating.getTotalOfScore() + score);
        }

        //total count for five star
        if (score.equals(RatingEnum.FIVESTAR.getScore())) {
            productRating.setTotalCountOfFiveStar(productRating.getTotalCountOfFiveStar() + 1);
            productRating.setTotalOfScore(productRating.getTotalOfScore() + score);
        }
        productRating.setTotalCountOfRating(getTotalCountOfRating(productRating));
        productRating.setAverageScore(NumberHelper.calculateAverageScore(productRating));
        return productRating;
    }

    public Long getTotalCountOfRating(ProductRating productRating) {
        return productRating.getTotalCountOfZeroStar() + productRating.getTotalCountOfOneStar() +
                productRating.getTotalCountOfTwoStar() + productRating.getTotalCountOfThreeStar() +
                productRating.getTotalCountOfFourStar() + productRating.getTotalCountOfFiveStar();
    }

    public ProductRatingDetailListResponse getProductRatingCommentList(Long productId, Double score, Integer pageIndex
            , Integer pageSize, String langCode) throws IdsmedBaseException {
        loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, GET_PRODUCT_RATING_COMMENT_LIST_METHOD_NAME, null);
        ProductRatingDetailListResponse productRatingDetailListResponse = new ProductRatingDetailListResponse(pageIndex, pageSize);
        Optional<Product> productOpt = productRepository.findById(productId);
        if (!productOpt.isPresent()) {
            throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.PRODUCT_NOT_FOUND_ERROR, LangHelper.getLangCode(langCode)));
        }
        Product product = productOpt.get();

        Pair<Integer, List<ProductRatingDetail>> productRatingDetails = productRepository.findProductRatingByCustomCondition(product, score, pageIndex, pageSize);
        if (CollectionUtils.isEmpty(productRatingDetails.getSecond())) {
            return productRatingDetailListResponse;
        }

        Integer productRatingDetailCount = productRatingDetails.getFirst();
        List<ProductRatingDetailResponse> productRatingDetailResponses = productRatingDetails.getSecond()
                .stream()
                .map(prd -> {
                    ProductRatingDetailResponse productRatingDetailResponse = null;
                    Optional<IdsmedAccount> idsmedAccount = idsmedAccountRepository.findById(prd.getCreatedBy());
                    if (idsmedAccount.isPresent()) {
                        IdsmedUser idsmedUser = idsmedUserRepository.findIdsmedUserByIdsmedAccounts(idsmedAccount.get());
                        productRatingDetailResponse = new ProductRatingDetailResponse(prd, idsmedUser);
                    }
                    return productRatingDetailResponse;
                })
                .collect(Collectors.toList());
        if (pageIndex != null && pageSize != null)
            productRatingDetailListResponse = new ProductRatingDetailListResponse(productRatingDetailCount, pageIndex, pageSize, productRatingDetailResponses);
        else
            productRatingDetailListResponse = new ProductRatingDetailListResponse(productRatingDetailCount, productRatingDetailResponses);
        loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, GET_PRODUCT_RATING_COMMENT_LIST_METHOD_NAME, null);
        return productRatingDetailListResponse;
    }

    @Override
    @Transactional
    public ProductWishlistSimpleResponse updateWishlistInProductDetail(ProductWishlistRequest request) throws IdsmedBaseException {
        loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, UPDATE_WISHLIST_IN_PRODUCT_DETAIL_METHOD_NAME, request.getLoginId());
        Optional<Product> productOpt = productRepository.findById(request.getProductId());
        if (!productOpt.isPresent()) {
            throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.PRODUCT_NOT_FOUND_ERROR,
                    LangHelper.getLangCode(request.getLangCode())));
        }

        Product product = productOpt.get();
        // Current login user's account info
        IdsmedAccount idsmedAccount = idsmedAccountRepository.findByLoginId(request.getLoginId());


        // Find if product exists in product wish list table
        ProductWishlist existingProductWishlist = productWishlistRepository.findByProductId(request.getProductId());

        // If existingProductWishlist == null create new ProductWishList and set for totalCount = 1
        if (existingProductWishlist == null && request.getIsLike()) {
            Integer totalWishList = existingProductWishlist.getProductWishlistDetailList().size();
            ProductWishlist productWishlist = new ProductWishlist(product, totalWishList.longValue(), idsmedAccount.getId());
            productWishlist.setStatus(StatusEnum.APPROVED.getCode());
            productWishlist = productWishlistRepository.save(productWishlist);
            ProductWishlistDetail productWishlistDetail = new ProductWishlistDetail(idsmedAccount, product, productWishlist);
            productWishlistDetail = productWishlistDetailRepository.save(productWishlistDetail);
            return new ProductWishlistSimpleResponse(productWishlist, true);
        }

        // Find product added to wish list by current login id
        ProductWishlistDetail existingProductWishlistDetail = productWishlistDetailRepository.findWishListDetailByLoginId(
                product, existingProductWishlist, idsmedAccount.getId());

        ProductWishlistSimpleResponse response = null;

        //Process logic customer like this product
        if (existingProductWishlist != null && request.getIsLike() && existingProductWishlistDetail == null) {
            // increase totalCount by 1 if user like this product
            existingProductWishlist = increaseTotalCountWishlistByOne(existingProductWishlist, idsmedAccount.getId());

            // If no such product record with corresponding login id, add record product wish list detail table

            ProductWishlistDetail newProductWishlistDetail = new ProductWishlistDetail(idsmedAccount, product, existingProductWishlist);

            productWishlistRepository.save(existingProductWishlist);
            productWishlistDetailRepository.save(newProductWishlistDetail);

            response = new ProductWishlistSimpleResponse(existingProductWishlist, true);

        }
        //Customer dislike this product
        if (existingProductWishlist != null && !request.getIsLike()) {
            existingProductWishlist = decreaseTotalCountWishlistByOne(existingProductWishlist, idsmedAccount.getId());
            productWishlistRepository.save(existingProductWishlist);
            // Remove product from user wish list
            productWishlistDetailRepository.deleteById(existingProductWishlistDetail.getId());
            response = new ProductWishlistSimpleResponse(existingProductWishlist, false);
        }

        loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, UPDATE_WISHLIST_IN_PRODUCT_DETAIL_METHOD_NAME, request.getLoginId());

        return response;
    }

    public ProductWishlistSimpleResponse updateWishlistInProductDetailForWedoctor(ProductWishlistForWedoctorRequest request) throws IdsmedBaseException {
        loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, UPDATE_WISHLIST_IN_PRODUCT_DETAIL_FOR_WEDOCTOR_METHOD_NAME, null);

        // get wedoctorCustomerId
        Long wedoctorCustomerId = request.getWedoctorCustomerId();

        Optional<Product> productOpt = productRepository.findById(request.getProductId());
        if (!productOpt.isPresent()) {
            throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.PRODUCT_NOT_FOUND_ERROR,
                    LangHelper.getLangCode(request.getLangCode())));
        }

        Product product = productOpt.get();

        // Find if product exists in product wish list table
        ProductWishlist existingProductWishlist = productWishlistRepository.findFirstByProduct(product);

        // If existingProductWishlist == null create new ProductWishList and set for totalCount = 1
        if (existingProductWishlist == null && request.getIsLike()) {
            ProductWishlist productWishlist = new ProductWishlist(product, 1L, wedoctorCustomerId, true);
            productWishlist = productWishlistRepository.save(productWishlist);
            ProductWishlistDetail productWishlistDetail = new ProductWishlistDetail(wedoctorCustomerId, product, productWishlist);
            productWishlistDetail = productWishlistDetailRepository.save(productWishlistDetail);
            return new ProductWishlistSimpleResponse(productWishlist, true);
        }

        // Find ProductWishListDetail by this wedoctorCustomerId
        ProductWishlistDetail existingProductWishlistDetail = productWishlistDetailRepository.findFirstByProductAndWedoctorCustomerId(product, wedoctorCustomerId);

        ProductWishlistSimpleResponse response = null;

        //Process logic customer like this product
        if (existingProductWishlist != null && request.getIsLike() && existingProductWishlistDetail == null) {
            // increase totalCount by 1 if user like this product
            existingProductWishlist = increaseTotalCountWishlistByOne(existingProductWishlist, wedoctorCustomerId);

            // If no such product record with corresponding login id, add record product wish list detail table
            ProductWishlistDetail newProductWishlistDetail = new ProductWishlistDetail(wedoctorCustomerId, product
                    , existingProductWishlist);

            productWishlistRepository.save(existingProductWishlist);
            productWishlistDetailRepository.save(newProductWishlistDetail);

            response = new ProductWishlistSimpleResponse(existingProductWishlist, true);

        }

        if (existingProductWishlist != null && !request.getIsLike()) {
            existingProductWishlist = decreaseTotalCountWishlistByOne(existingProductWishlist, wedoctorCustomerId);
            productWishlistRepository.save(existingProductWishlist);
            // Remove product from user wish list
            productWishlistDetailRepository.deleteById(existingProductWishlistDetail.getId());
            response = new ProductWishlistSimpleResponse(existingProductWishlist, false);
        }

        loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, UPDATE_WISHLIST_IN_PRODUCT_DETAIL_FOR_WEDOCTOR_METHOD_NAME, null);

        return response;
    }

    private ProductWishlist increaseTotalCountWishlistByOne(ProductWishlist productWishlist, Long updatedById) {
        productWishlist.setTotalWishlist(productWishlist.getTotalWishlist() + 1);
        productWishlist.setUpdatedBy(updatedById);
        return productWishlist;
    }

    private ProductWishlist decreaseTotalCountWishlistByOne(ProductWishlist productWishlist, Long updatedById) {
        productWishlist.setTotalWishlist(productWishlist.getTotalWishlist() - 1);
        productWishlist.setUpdatedBy(updatedById);
        return productWishlist;
    }

    @Override
    public ProductWishlistSimpleResponse getFavouriteProductById(Long id, String langCode, String encodedLoginId) throws IdsmedBaseException {
        loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, GET_FAVOURITE_PRODUCT_BY_ID_METHOD_NAME, encodedLoginId);
        String loginId = null;
        if (!StringUtils.isEmpty(encodedLoginId)) {
            loginId = URLDecoder.decode(encodedLoginId);
        }
        ProductWishlist existingProductWishlist = productWishlistRepository.findByProductId(id);
        IdsmedAccount account = idsmedAccountRepository.findByLoginId(loginId);

        Optional<Product> productOpt = productRepository.findById(id);

        Product product = productOpt.get();

        ProductWishlistSimpleResponse response = null;
        if (existingProductWishlist != null) {
            List<ProductWishlistDetail> productWishlistDetails = existingProductWishlist.getProductWishlistDetailList();

            Optional<ProductWishlistDetail> detailOpt = productWishlistDetails.stream().filter(pwd -> pwd.getIdsmedAccount().getId().equals(account.getId())).findFirst();
            if (detailOpt.isPresent()) {
                response = new ProductWishlistSimpleResponse(product, true);
            } else {
                response = new ProductWishlistSimpleResponse(product, false);
            }

        } else {
            response = new ProductWishlistSimpleResponse(product, false);
        }
        loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, GET_FAVOURITE_PRODUCT_BY_ID_METHOD_NAME, encodedLoginId);
        return response;
    }

    @Override
    public ProductTechFeatureResponse getListOfProductTechnicalAndFeature(String langCode) throws IdsmedBaseException {
        loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, GET_LIST_OF_PRODUCT_TECHNICAL_AND_FEATURE_METHOD_NAME, null);
        List<ProductTechnicalFeatureLabel> productTechnicalFeatureLabelList = productTechnicalFeatureLabelRepository.findAll();

        List<ProductTechnicalFeatureLabel> productTechnicalLabels = new ArrayList<>();
        List<ProductTechnicalFeatureLabel> productFeatureLabels = new ArrayList<>();

        // Loop through and split product feature and technical then save label name only into its own list
        for (ProductTechnicalFeatureLabel ptf : productTechnicalFeatureLabelList) {
            if (ptf.getLabelType() == ProductTechnicalFeatureLabel.LabelTypeEnum.PRODUCT_FEATURE.getCode()) {
                productFeatureLabels.add(ptf);
            } else {
                productTechnicalLabels.add(ptf);
            }
        }

        ProductTechFeatureResponse response = new ProductTechFeatureResponse(productFeatureLabels, productTechnicalLabels, langCode);

        loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, GET_LIST_OF_PRODUCT_TECHNICAL_AND_FEATURE_METHOD_NAME, null);
        return response;
    }

    /**
     * Process checking for list of product feature with particular language code
     * @param productFeatures
     * @param loginId
     * @param langCode
     * @return
     */
    private List<ProductTechnicalFeatureLabel> processProductFeaturesLabel(List<ProductFeatures> productFeatures, Long loginId, String langCode) {

        List<ProductTechnicalFeatureLabel> productTechnicalFeatureLabelList = new ArrayList<>();

        if (!CollectionUtils.isEmpty(productFeatures)) {
            productFeatures.forEach(f -> {
                LangEnum langEnum = LangEnum.CHINA;
                if (langCode != null) {
                    for (LangEnum lang : LangEnum.values()) {
                        if (langCode.equals(lang.getCode())) {
                            langEnum = lang;
                            break;
                        }
                    }
                }

                switch (langEnum) {
                    case ENGLISH:
                        if (!checkProductFeatureTechnicalLabelExists(f.getProductLabel(),
                                ProductTechnicalFeatureLabel.LabelTypeEnum.PRODUCT_FEATURE.getCode(), langEnum)) {
                            ProductTechnicalFeatureLabel ptf = new ProductTechnicalFeatureLabel(
                                    f.getProductLabel(),
                                    null,
                                    null,
                                    null,
                                    null,
                                    null,
                                    ProductTechnicalFeatureLabel.LabelTypeEnum.PRODUCT_FEATURE.getCode(),
                                    0
                            );

                            ptf.setCreatedBy(loginId);
                            ptf.setUpdatedBy(loginId);
                            productTechnicalFeatureLabelList.add(ptf);
                        }
                        break;

                    case CHINA:
                        if (!checkProductFeatureTechnicalLabelExists(f.getProductLabel(),
                                ProductTechnicalFeatureLabel.LabelTypeEnum.PRODUCT_FEATURE.getCode(), langEnum)) {
                            ProductTechnicalFeatureLabel ptf = new ProductTechnicalFeatureLabel(
                                    null,
                                    f.getProductLabel(),
                                    null,
                                    null,
                                    null,
                                    null,
                                    ProductTechnicalFeatureLabel.LabelTypeEnum.PRODUCT_FEATURE.getCode(),
                                    0
                            );

                            ptf.setCreatedBy(loginId);
                            ptf.setUpdatedBy(loginId);
                            productTechnicalFeatureLabelList.add(ptf);
                        }
                        break;

                    case CHINA_TAIWAN:
                        if (!checkProductFeatureTechnicalLabelExists(f.getProductLabel(),
                                ProductTechnicalFeatureLabel.LabelTypeEnum.PRODUCT_FEATURE.getCode(), langEnum)) {
                            ProductTechnicalFeatureLabel ptf = new ProductTechnicalFeatureLabel(
                                    null,
                                    null,
                                    f.getProductLabel(),
                                    null,
                                    null,
                                    null,
                                    ProductTechnicalFeatureLabel.LabelTypeEnum.PRODUCT_FEATURE.getCode(),
                                    0
                            );

                            ptf.setCreatedBy(loginId);
                            ptf.setUpdatedBy(loginId);
                            productTechnicalFeatureLabelList.add(ptf);
                        }
                        break;

                    case VIETNAMESE:
                        if (!checkProductFeatureTechnicalLabelExists(f.getProductLabel(),
                                ProductTechnicalFeatureLabel.LabelTypeEnum.PRODUCT_FEATURE.getCode(), langEnum)) {
                            ProductTechnicalFeatureLabel ptf = new ProductTechnicalFeatureLabel(
                                    null,
                                    null,
                                    null,
                                    f.getProductLabel(),
                                    null,
                                    null,
                                    ProductTechnicalFeatureLabel.LabelTypeEnum.PRODUCT_FEATURE.getCode(),
                                    0
                            );

                            ptf.setCreatedBy(loginId);
                            ptf.setUpdatedBy(loginId);
                            productTechnicalFeatureLabelList.add(ptf);
                        }
                        break;

                    case THAI:
                        if (!checkProductFeatureTechnicalLabelExists(f.getProductLabel(),
                                ProductTechnicalFeatureLabel.LabelTypeEnum.PRODUCT_FEATURE.getCode(), langEnum)) {
                            ProductTechnicalFeatureLabel ptf = new ProductTechnicalFeatureLabel(
                                    null,
                                    null,
                                    null,
                                    null,
                                    f.getProductLabel(),
                                    null,
                                    ProductTechnicalFeatureLabel.LabelTypeEnum.PRODUCT_FEATURE.getCode(),
                                    0
                            );

                            ptf.setCreatedBy(loginId);
                            ptf.setUpdatedBy(loginId);
                            productTechnicalFeatureLabelList.add(ptf);
                        }
                        break;

                    case INDONESIAN:
                        if (!checkProductFeatureTechnicalLabelExists(f.getProductLabel(),
                                ProductTechnicalFeatureLabel.LabelTypeEnum.PRODUCT_FEATURE.getCode(), langEnum)) {
                            ProductTechnicalFeatureLabel ptf = new ProductTechnicalFeatureLabel(
                                    null,
                                    null,
                                    null,
                                    null,
                                    null,
                                    f.getProductLabel(),
                                    ProductTechnicalFeatureLabel.LabelTypeEnum.PRODUCT_FEATURE.getCode(),
                                    0
                            );

                            ptf.setCreatedBy(loginId);
                            ptf.setUpdatedBy(loginId);
                            productTechnicalFeatureLabelList.add(ptf);
                        }
                        break;

                    default:
                        if (!checkProductFeatureTechnicalLabelExists(f.getProductLabel(),
                                ProductTechnicalFeatureLabel.LabelTypeEnum.PRODUCT_FEATURE.getCode(), langEnum)) {
                            ProductTechnicalFeatureLabel ptf = new ProductTechnicalFeatureLabel(
                                    null,
                                    f.getProductLabel(),
                                    null,
                                    null,
                                    null,
                                    null,
                                    ProductTechnicalFeatureLabel.LabelTypeEnum.PRODUCT_FEATURE.getCode(),
                                    0
                            );

                            ptf.setCreatedBy(loginId);
                            ptf.setUpdatedBy(loginId);
                            productTechnicalFeatureLabelList.add(ptf);
                        }
                        break;
                }
            });
        }

        return productTechnicalFeatureLabelList;
    }

    /**
     * Process checking for list of product technical with particular language code
     * @param productTechnicals
     * @param loginId
     * @param langCode
     * @return
     */
    public List<ProductTechnicalFeatureLabel> processProductTechnicalsLabel(List<ProductTechnical> productTechnicals, Long loginId, String langCode) {

        List<ProductTechnicalFeatureLabel> productTechnicalFeatureLabelList = new ArrayList<>();

        if (!CollectionUtils.isEmpty(productTechnicals)) {
            productTechnicals.forEach(f -> {
                LangEnum langEnum = LangEnum.CHINA;
                if (langCode != null) {
                    for (LangEnum lang : LangEnum.values()) {
                        if (langCode.equals(lang.getCode())) {
                            langEnum = lang;
                            break;
                        }
                    }
                }

                switch (langEnum) {
                    case ENGLISH:
                        if (!checkProductFeatureTechnicalLabelExists(f.getProductLabel(),
                                ProductTechnicalFeatureLabel.LabelTypeEnum.PRODUCT_TECHNICAL.getCode(), langEnum)) {
                            ProductTechnicalFeatureLabel ptf = new ProductTechnicalFeatureLabel(
                                    f.getProductLabel(),
                                    null,
                                    null,
                                    null,
                                    null,
                                    null,
                                    ProductTechnicalFeatureLabel.LabelTypeEnum.PRODUCT_TECHNICAL.getCode(),
                                    0
                            );

                            ptf.setCreatedBy(loginId);
                            ptf.setUpdatedBy(loginId);
                            productTechnicalFeatureLabelList.add(ptf);
                        }
                        break;

                    case CHINA:
                        if (!checkProductFeatureTechnicalLabelExists(f.getProductLabel(),
                                ProductTechnicalFeatureLabel.LabelTypeEnum.PRODUCT_TECHNICAL.getCode(), langEnum)) {
                            ProductTechnicalFeatureLabel ptf = new ProductTechnicalFeatureLabel(
                                    null,
                                    f.getProductLabel(),
                                    null,
                                    null,
                                    null,
                                    null,
                                    ProductTechnicalFeatureLabel.LabelTypeEnum.PRODUCT_TECHNICAL.getCode(),
                                    0
                            );

                            ptf.setCreatedBy(loginId);
                            ptf.setUpdatedBy(loginId);
                            productTechnicalFeatureLabelList.add(ptf);
                        }
                        break;

                    case CHINA_TAIWAN:
                        if (!checkProductFeatureTechnicalLabelExists(f.getProductLabel(),
                                ProductTechnicalFeatureLabel.LabelTypeEnum.PRODUCT_TECHNICAL.getCode(), langEnum)) {
                            ProductTechnicalFeatureLabel ptf = new ProductTechnicalFeatureLabel(
                                    null,
                                    null,
                                    f.getProductLabel(),
                                    null,
                                    null,
                                    null,
                                    ProductTechnicalFeatureLabel.LabelTypeEnum.PRODUCT_TECHNICAL.getCode(),
                                    0
                            );

                            ptf.setCreatedBy(loginId);
                            ptf.setUpdatedBy(loginId);
                            productTechnicalFeatureLabelList.add(ptf);
                        }
                        break;

                    case VIETNAMESE:
                        if (!checkProductFeatureTechnicalLabelExists(f.getProductLabel(),
                                ProductTechnicalFeatureLabel.LabelTypeEnum.PRODUCT_TECHNICAL.getCode(), langEnum)) {
                            ProductTechnicalFeatureLabel ptf = new ProductTechnicalFeatureLabel(
                                    null,
                                    null,
                                    null,
                                    f.getProductLabel(),
                                    null,
                                    null,
                                    ProductTechnicalFeatureLabel.LabelTypeEnum.PRODUCT_TECHNICAL.getCode(),
                                    0
                            );

                            ptf.setCreatedBy(loginId);
                            ptf.setUpdatedBy(loginId);
                            productTechnicalFeatureLabelList.add(ptf);
                        }
                        break;

                    case THAI:
                        if (!checkProductFeatureTechnicalLabelExists(f.getProductLabel(),
                                ProductTechnicalFeatureLabel.LabelTypeEnum.PRODUCT_TECHNICAL.getCode(), langEnum)) {
                            ProductTechnicalFeatureLabel ptf = new ProductTechnicalFeatureLabel(
                                    null,
                                    null,
                                    null,
                                    null,
                                    f.getProductLabel(),
                                    null,
                                    ProductTechnicalFeatureLabel.LabelTypeEnum.PRODUCT_TECHNICAL.getCode(),
                                    0
                            );

                            ptf.setCreatedBy(loginId);
                            ptf.setUpdatedBy(loginId);
                            productTechnicalFeatureLabelList.add(ptf);
                        }
                        break;

                    case INDONESIAN:
                        if (!checkProductFeatureTechnicalLabelExists(f.getProductLabel(),
                                ProductTechnicalFeatureLabel.LabelTypeEnum.PRODUCT_TECHNICAL.getCode(), langEnum)) {
                            ProductTechnicalFeatureLabel ptf = new ProductTechnicalFeatureLabel(
                                    null,
                                    null,
                                    null,
                                    null,
                                    null,
                                    f.getProductLabel(),
                                    ProductTechnicalFeatureLabel.LabelTypeEnum.PRODUCT_TECHNICAL.getCode(),
                                    0
                            );

                            ptf.setCreatedBy(loginId);
                            ptf.setUpdatedBy(loginId);
                            productTechnicalFeatureLabelList.add(ptf);
                        }
                        break;

                    default:
                        if (!checkProductFeatureTechnicalLabelExists(f.getProductLabel(),
                                ProductTechnicalFeatureLabel.LabelTypeEnum.PRODUCT_TECHNICAL.getCode(), langEnum)) {
                            ProductTechnicalFeatureLabel ptf = new ProductTechnicalFeatureLabel(
                                    null,
                                    f.getProductLabel(),
                                    null,
                                    null,
                                    null,
                                    null,
                                    ProductTechnicalFeatureLabel.LabelTypeEnum.PRODUCT_TECHNICAL.getCode(),
                                    0
                            );

                            ptf.setCreatedBy(loginId);
                            ptf.setUpdatedBy(loginId);
                            productTechnicalFeatureLabelList.add(ptf);
                        }
                        break;
                }
            });
        }

        return productTechnicalFeatureLabelList;
    }

    /**
     * Check from database is specific product feature with specific language code exists or not
     * @param label
     * @param typeCode
     * @param langEnum
     * @return
     */
    private Boolean checkProductFeatureTechnicalLabelExists(String label, Integer typeCode, LangEnum langEnum) {
        if (langEnum == LangEnum.ENGLISH) {
            if (productTechnicalFeatureLabelRepository.findFirstByLabelNameAndLabelType(label, typeCode) != null) {
                return true;
            }
        }

        if (langEnum == LangEnum.CHINA) {
            if (productTechnicalFeatureLabelRepository.findFirstByLabelNameZhCnAndLabelType(label, typeCode) != null) {
                return true;
            }
        }

        if (langEnum == LangEnum.CHINA_TAIWAN) {
            if (productTechnicalFeatureLabelRepository.findFirstByLabelNameZhTwAndLabelType(label, typeCode) != null) {
                return true;
            }
        }

        if (langEnum == LangEnum.VIETNAMESE) {
            if (productTechnicalFeatureLabelRepository.findFirstByLabelNameViAndLabelType(label, typeCode) != null) {
                return true;
            }
        }

        if (langEnum == LangEnum.THAI) {
            if (productTechnicalFeatureLabelRepository.findFirstByLabelNameThAndLabelType(label, typeCode) != null) {
                return true;
            }
        }

        if (langEnum == LangEnum.INDONESIAN) {
            if (productTechnicalFeatureLabelRepository.findFirstByLabelNameIdAndLabelType(label, typeCode) != null) {
                return true;
            }
        }

        return false;
    }

    /*
     * This method use by third party to get suggested products
     * In database, wedoctorCustomerId is customer_id
     */
    @Override
    public ProductApiListResponse getSuggestedProducts(Long wedoctorCustomerId, Integer pageIndex, Integer pageSize, String langCode) throws IdsmedBaseException {
        loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, GET_SUGGESTED_PRODUCTS_METHOD_NAME, null);

        ProductApiListResponse response = null;
        List<ProductApiResponse> responseList = new ArrayList<>();

        Pair<Integer, List<Product>> suggestedProducts = productRepository.findSuggestedProducts(wedoctorCustomerId, pageIndex, pageSize);
        String ossBucketDomain = env.getProperty(PropertiesLabelConst.OSS_BUCKET_DOMAIN);
        for (Product product : suggestedProducts.getSecond()) {
            ProductApiResponse productApiResponse = new ProductApiResponse(product, ossBucketDomain);
            responseList.add(productApiResponse);
        }

        Integer totalCount = suggestedProducts.getFirst();
        if (pageIndex != null && pageSize != null) {
            response = new ProductApiListResponse(totalCount, pageIndex, pageSize, responseList);
        } else {
            response = new ProductApiListResponse(totalCount, responseList);
        }

        loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, GET_SUGGESTED_PRODUCTS_METHOD_NAME, null);
        webServiceLogService.StoreInWebServiceLogDatabase(httpServletRequest, MethodNameEnum.GETSUGGESTEDPRODUCTS.getName(), HTTPServletReturnStatusEnum.AUTHORIZED.getCode());

        return response;
    }

    public ProductApiListResponse getRecentlySearchProduct(String langCode, Long wedoctorCustomerId, Integer pageIndex, Integer pageSize) throws IdsmedBaseException {
        loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, GET_RECENTLY_SEARCH_PRODUCT_METHOD_NAME, null);
        ProductApiListResponse productApiListResponse = new ProductApiListResponse(pageIndex, pageSize);
        KeywordHistory keywordHistory = keywordHistoryRepository.findFirstByWedoctorCustomerIdAndProductIdsIsNotNullOrderByCreatedDateDesc(wedoctorCustomerId);

        webServiceLogService.StoreInWebServiceLogDatabase(httpServletRequest, MethodNameEnum.GETRECENTLYSEARCHPRODUCT.getName(), HTTPServletReturnStatusEnum.AUTHORIZED.getCode());

        if (keywordHistory == null) {
            return productApiListResponse;
        }

        String stringProductIds = keywordHistory.getProductIds();

        List<Long> productIds = Stream.of(stringProductIds.split(","))
                .map(Long::parseLong)
                .collect(Collectors.toList());

        List<Product> products = productIds.stream().map(id -> {
            Product product = productRepository.findByIdOrderByProductNamePrimaryAsc(id);
            return product;
        }).collect(Collectors.toList());

        String ossBucketDomain = env.getProperty(PropertiesLabelConst.OSS_BUCKET_DOMAIN);
        List<ProductApiResponse> productApiResponses = products.stream().map(p -> new ProductApiResponse(p, ossBucketDomain)).collect(Collectors.toList());

        loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, GET_RECENTLY_SEARCH_PRODUCT_METHOD_NAME, null);
        return new ProductApiListResponse(productApiResponses);
    }

    public List<ProductComparisonResponse> compareProducts(List<Long> productIds, String langCode) throws IdsmedBaseException {
        List<Product> products = new ArrayList<>();
        String ossBucketDomain = env.getProperty(PropertiesLabelConst.OSS_BUCKET_DOMAIN);
        loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, COMPARE_PRODUCTS_METHOD_NAME, null);
        if (productIds.size() < PRODUCT_COMPARISON_MIN_NUMBER || productIds.size() > PRODUCT_COMPARISON_MAX_NUMBER) {
            throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.PRODUCT_NUMBER_COMPARISION_NOT_VALID_ERROR, LangHelper.getLangCode(langCode)));
        }

        for (Long id : productIds) {
            Optional<Product> productOpt = productRepository.findById(id);
            if (!productOpt.isPresent()) {
                throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.PRODUCT_NOT_FOUND_ERROR, LangHelper.getLangCode(langCode)));
            }
            products.add(productOpt.get());
        }

        List<ProductComparisonResponse> result = new ArrayList<>();
        for (Product product : products) {
            ProductComparisonResponse res = new ProductComparisonResponse(product, langCode, ossBucketDomain);
            result.add(res);
        }

        //If there is no feature and technical return response with empty compareData
        if (products.stream().filter(p -> CollectionUtils.isEmpty(p.getProductFeaturesList())).findFirst().isPresent()
                && products.stream().filter(p -> CollectionUtils.isEmpty(p.getProductTechnicalList())).findFirst().isPresent()) {
            return result;
        }

        Map<Product, List<ProductCompareDataResponse>> allCompareDataByProductMap = new HashMap<>();
        Map<Product, List<ProductCompareDataResponse>> featureCompareByProductMap = new HashMap<>();

        Map<Product, List<ProductCompareDataResponse>> technicalCompareDataByProductMap = new HashMap<>();


        if (!products.stream().filter(p -> CollectionUtils.isEmpty(p.getProductFeaturesList())).findFirst().isPresent()) {
            featureCompareByProductMap = fetchProductFeatureCompareData(products);
        }

        if (!products.stream().filter(p -> CollectionUtils.isEmpty(p.getProductTechnicalList())).findFirst().isPresent()) {
            technicalCompareDataByProductMap = fetchProductTechnicalCompareData(products);
        }

        Iterator<Map.Entry<Product, List<ProductCompareDataResponse>>> iterator = null;

        if (!CollectionUtils.isEmpty(featureCompareByProductMap)) {
            allCompareDataByProductMap = featureCompareByProductMap;
            iterator = technicalCompareDataByProductMap.entrySet().iterator();
        } else {
            if (!CollectionUtils.isEmpty(technicalCompareDataByProductMap)) {
                allCompareDataByProductMap = technicalCompareDataByProductMap;
                iterator = technicalCompareDataByProductMap.entrySet().iterator();;
            }
        }

        while (iterator != null && iterator.hasNext()) {
            Map.Entry<Product, List<ProductCompareDataResponse>> entry = iterator.next();
            allCompareDataByProductMap.entrySet().stream().forEach(en -> {
                if (entry.getKey().getId().equals(en.getKey().getId())) {
                    en.getValue().addAll(entry.getValue());
                    en.setValue(en.getValue());
                }
            });
        }

        for (ProductComparisonResponse response : result) {
            Iterator<Map.Entry<Product, List<ProductCompareDataResponse>>> itr = allCompareDataByProductMap.entrySet().iterator();
            while (itr.hasNext()) {
                Map.Entry<Product, List<ProductCompareDataResponse>> entry = itr.next();
                if (response.getId().equals(entry.getKey().getId())) {
                    response.setComparedData(entry.getValue());
                }
            }
        }

        loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, COMPARE_PRODUCTS_METHOD_NAME, null);

        return result;
    }

    public List<ProductComparisonResponse> compareProductsV2(List<Long> productIds, String langCode) throws IdsmedBaseException {
        List<Product> products = new ArrayList<>();
        String ossBucketDomain = env.getProperty(PropertiesLabelConst.OSS_BUCKET_DOMAIN);
        loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, COMPARE_PRODUCTS_METHOD_NAME, null);
        if (productIds.size() < PRODUCT_COMPARISON_MIN_NUMBER || productIds.size() > PRODUCT_COMPARISON_MAX_NUMBER) {
            throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.PRODUCT_NUMBER_COMPARISION_NOT_VALID_ERROR, LangHelper.getLangCode(langCode)));
        }

        for (Long id : productIds) {
            Optional<Product> productOpt = productRepository.findById(id);
            if (!productOpt.isPresent()) {
                throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.PRODUCT_NOT_FOUND_ERROR, LangHelper.getLangCode(langCode)));
            }
            products.add(productOpt.get());
        }

        List<ProductComparisonResponse> result = new ArrayList<>();
        for (Product product : products) {
            ProductComparisonResponse res = new ProductComparisonResponse(product, langCode, ossBucketDomain);
            result.add(res);
        }

        Map<Product, List<Pair<String, String>>> productCompareLabelValueMap = new HashMap<>();
        //Merge list of feature and technical for each product.
        for (Product product : products) {
            List<Pair<String, String>> featuresResponse = product.getProductFeaturesList().stream().map(pf -> {
                Pair<String, String> pair = Pair.of(pf.getProductLabel(), pf.getProductValue());
                return pair;
            }).collect(Collectors.toList());

            List<Pair<String, String>> technialResponse = product.getProductTechnicalList().stream().map(pt -> {
                Pair<String, String> pair = Pair.of(pt.getProductLabel(), pt.getProductValue());
                return pair;
            }).collect(Collectors.toList());

            List<Pair<String, String>> compareResponse = new ArrayList<>();
            compareResponse.addAll(featuresResponse);
            compareResponse.addAll(technialResponse);
            productCompareLabelValueMap.put(product, compareResponse);
        }

        //Get list of label for all products
        List<String> labels = new ArrayList<>();
        Iterator<Map.Entry<Product, List<Pair<String, String>>>> iterator = productCompareLabelValueMap.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<Product, List<Pair<String, String>>> entry = iterator.next();
            if (!StringUtils.isEmpty(entry.getValue())) {
                List<String> strList = entry.getValue().stream().map(pir -> { return pir.getFirst(); }).collect(Collectors.toList());
                if (!StringUtils.isEmpty(strList)) {
                    strList.forEach(str -> {
                        if (!labels.contains(str)) {
                            labels.add(str);
                        }
                    });
                }
            }
        }

        //generate proper comparisonDataResponse for each product
        Map<Product, List<ProductCompareDataResponse>> comparisionDataResponseMap = new HashMap<>();
        Iterator<Map.Entry<Product, List<Pair<String, String>>>> itr = productCompareLabelValueMap.entrySet().iterator();
        while (itr.hasNext()) {
            Map.Entry<Product, List<Pair<String, String>>> entry = itr.next();
            List<ProductCompareDataResponse> dataResponses = new ArrayList<>();
            for (String label : labels) {
                Boolean hasLabel = false;
                if (!CollectionUtils.isEmpty(entry.getValue())) {
                    for (Pair<String, String> pair : entry.getValue()) {
                        if (label.equals(pair.getFirst())) {
                            ProductCompareDataResponse dataResponse = new ProductCompareDataResponse(pair.getFirst(), pair.getSecond());
                            dataResponses.add(dataResponse);
                            hasLabel = true;
                        }
                    }
                }
                if (!hasLabel) {
                    ProductCompareDataResponse dataResponse = new ProductCompareDataResponse(label, "");
                    dataResponses.add(dataResponse);
                }
            }
            comparisionDataResponseMap.put(entry.getKey(), dataResponses);
        }

        //Process final list of comparisonDataResponse.
//        Iterator<Map.Entry<Product, List<ProductCompareDataResponse>>> itrForCompareData = comparisionDataResponseMap.entrySet().iterator();
//        while(itrForCompareData.hasNext()) {
//            Map.Entry<Product, List<ProductCompareDataResponse>> entry = itrForCompareData.next();
//            List<ProductCompareDataResponse> rawCompareDataList = entry.getValue();
//            List<ProductCompareDataResponse> processedCompareDataList = new ArrayList<>();
//            List<Integer> hasChecked = new ArrayList<>();
//
//            if (!CollectionUtils.isEmpty(rawCompareDataList)) {
//                for (int i = 0 ; i < rawCompareDataList.size(); i++) {
//                    for (int j = i++ ; j < rawCompareDataList.size(); j++) {
//                        if(hasChecked.contains(j)) {
//                            continue;
//                        }
//                        if (rawCompareDataList.get(i).getLabel().equals(rawCompareDataList.get(j).getLabel())) {
//                            if (rawCompareDataList.get(i).getValue().equals(rawCompareDataList.get(j).getValue())) {
//                                processedCompareDataList.add(rawCompareDataList.get(i));
//                                hasChecked.add(j);
//                            }
//                            ProductCompareDataResponse res = new ProductCompareDataResponse((rawCompareDataList.get(i).getLabel()), rawCompareDataList.get(i).getValue() + "/" + rawCompareDataList.get(j).getValue());
//                            processedCompareDataList.add(res);
//                        }
//                    }
//                    hasChecked.add(i);
//                }
//            }
//
//            entry.setValue(processedCompareDataList);
//        }

        for (ProductComparisonResponse response: result) {
            Iterator<Map.Entry<Product, List<ProductCompareDataResponse>>> itrFinal = comparisionDataResponseMap.entrySet().iterator();
            while(itrFinal.hasNext()) {
                Map.Entry<Product, List<ProductCompareDataResponse>> entry = itrFinal.next();
                if (response.getId().equals(entry.getKey().getId())) {
                    response.setComparedData(entry.getValue());
                }
            }
        }

        loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, COMPARE_PRODUCTS_METHOD_NAME, null);

        return result;
    }


    private Map<Product, List<ProductCompareDataResponse>> fetchProductTechnicalCompareData(List<Product> products) {
        Map<Product, List<ProductCompareDataResponse>> technicalCompareDataByProductMap = new HashMap<>();

        List<List<ProductTechnical>> productTechnicals = new ArrayList<>();

        for (Product product : products) {
            productTechnicals.add(product.getProductTechnicalList());
        }

        List<ProductTechnical> allValidComparedTechnical = new ArrayList<>();
        //List of ProductFeature for first product.
        List<ProductTechnical> firstListOfTechnical = productTechnicals.get(0);

        //TODO need to apply algorithm for better perfomance when have time
        for (ProductTechnical technical : firstListOfTechnical) {
            List<ProductTechnical> tempList = new ArrayList<>();
            tempList.add(technical);
            for (int i = 1; i < productTechnicals.size(); i++) {
                List<ProductTechnical> needCompareList = productTechnicals.get(i);
                for (ProductTechnical pf : needCompareList) {
                    if (technical.getProductLabel().equals(pf.getProductLabel())) {
                        tempList.add(pf);
                    }
                }
            }
            if (tempList.size() == products.size()) {
                allValidComparedTechnical.addAll(tempList);
            }
        }

        if (CollectionUtils.isEmpty(allValidComparedTechnical)) {

            for (Product product : products) {
                technicalCompareDataByProductMap.put(product, new ArrayList<>());
            }

            return technicalCompareDataByProductMap;
        }

        Map<Product, List<ProductTechnical>> validTechnicalByProductMap = new HashMap<>();

        products.forEach(p -> {
            List<ProductTechnical> technicals = allValidComparedTechnical.stream()
                    .filter(ft -> ft.getProduct().getId().equals(p.getId())).collect(Collectors.toList());
            validTechnicalByProductMap.put(p, technicals);
        });

        technicalCompareDataByProductMap = new HashMap<>();

        Iterator<Map.Entry<Product, List<ProductTechnical>>> iterator = validTechnicalByProductMap.entrySet().iterator();

        while (iterator.hasNext()) {
            Map.Entry<Product, List<ProductTechnical>> entry = iterator.next();
            List<ProductCompareDataResponse> listCompareDataResponse = entry.getValue().stream()
                    .map(ft -> new ProductCompareDataResponse(ft.getProductLabel(), ft.getProductValue()))
                    .collect(Collectors.toList());

            technicalCompareDataByProductMap.put(entry.getKey(), listCompareDataResponse);
        }

        return technicalCompareDataByProductMap;
    }

    private Map<Product, List<ProductCompareDataResponse>> fetchProductFeatureCompareData(List<Product> products) {
        Map<Product, List<ProductCompareDataResponse>> featureCompareDataByProductMap = new HashMap<>();

        List<List<ProductFeatures>> productFeatures = new ArrayList<>();

        for (Product product : products) {
            productFeatures.add(product.getProductFeaturesList());
        }

        List<ProductFeatures> allValidComparedFeatures = new ArrayList<>();
        //List of ProductFeature for first product.
        List<ProductFeatures> firstListOfFeature = productFeatures.get(0);

        //TODO need to apply algorithm for better perfomance when have time
        for (ProductFeatures feature : firstListOfFeature) {
            List<ProductFeatures> tempList = new ArrayList<>();
            tempList.add(feature);
            for (int i = 1; i < productFeatures.size(); i++) {
                List<ProductFeatures> needCompareList = productFeatures.get(i);
                for (ProductFeatures pf : needCompareList) {
                    if (feature.getProductLabel().equals(pf.getProductLabel())) {
                        tempList.add(pf);
                    }
                }
            }
            if (tempList.size() == products.size()) {
                allValidComparedFeatures.addAll(tempList);
            }
        }

        if (CollectionUtils.isEmpty(allValidComparedFeatures)) {

            for (Product product : products) {
                featureCompareDataByProductMap.put(product, new ArrayList<>());
            }

            return featureCompareDataByProductMap;
        }

        Map<Product, List<ProductFeatures>> validFeatureByProductMap = new HashMap<>();

        products.forEach(p -> {
            List<ProductFeatures> features = allValidComparedFeatures.stream()
                    .filter(ft -> ft.getProduct().getId().equals(p.getId())).collect(Collectors.toList());
            validFeatureByProductMap.put(p, features);
        });

        featureCompareDataByProductMap = new HashMap<>();

        Iterator<Map.Entry<Product, List<ProductFeatures>>> iterator = validFeatureByProductMap.entrySet().iterator();

        while (iterator.hasNext()) {
            Map.Entry<Product, List<ProductFeatures>> entry = iterator.next();
            List<ProductCompareDataResponse> listCompareDataResponse = entry.getValue().stream()
                    .map(ft -> new ProductCompareDataResponse(ft.getProductLabel(), ft.getProductValue()))
                    .collect(Collectors.toList());

            featureCompareDataByProductMap.put(entry.getKey(), listCompareDataResponse);
        }

        return featureCompareDataByProductMap;
    }

    private Pair<String, String> processEmailNotificationAboutApprovedProductToSCPAdmin(Product product, String langCode) throws IdsmedBaseException {

        String recipientEmail = emailNotificationConfigService
                .getEmailNotificationConfigByEmailType(EmailTypeEnum.PRODUCT_APPROVED_NOTIFICATION_FOR_SCP_ADMIN)
                .getRecipientList();

        List<IdsmedUser> scpAdminUsers = userService.getAccountListOfSCPAdmin(recipientEmail);

        Pair<String, String> emailPairForSCPAdmin = null;
        for (IdsmedUser user : scpAdminUsers) {
            String name = user.getFirstName() + " " + user.getLastName();
            emailPairForSCPAdmin = emailService.generatedProductApprovedEmailSubjectAndContentForSCPAdmin(
                    name,
                    product.getVendor().getCompanyNamePrimary(),
                    product.getProductNamePrimary(),
                    product.getProductModel(),
                    product.getProductCode());

            EmailObject emailObject = new ProductApproveEmailObject(
                    Arrays.asList(user.getEmail()),
                    env.getProperty(PropertiesLabelConst.SMTP_SUPPORT_MAIL_LABEL),
                    emailPairForSCPAdmin);

            rabbitTemplate.convertAndSend(env.getProperty(IdsmedQueueConfig.RABBIT_MAIL_SERVICE_PROCESS_QUEUE),
                    emailObject);
        }

        return emailPairForSCPAdmin;
    }

    private void processWebNotificationAboutApprovedProductToSCPAdmin(IdsmedAccount sender, Pair<String, String> emailPair) throws IdsmedBaseException {
        // Getting account list for user with Product Approve Permission
        List<IdsmedAccount> accountListWithProductApprovePermission = getAccountListForUserWithProductApprovePermission();

        // Send web notification to SCP Admin
        notificationService.sendProductApprovedNotification(sender, accountListWithProductApprovePermission, emailPair);
    }

    private List<IdsmedAccount> getAccountListForUserWithProductApprovePermission() {
        List<IdsmedUser> idsmedUserList = authenticationService.getUsersByPermissionCode(
                IdsmedPermissionCodeConst.PRODUCT_APPROVE);

        List<IdsmedAccount> accountList = new ArrayList<>();
        for (IdsmedUser idsmedUser : idsmedUserList) {
            List<IdsmedAccount> idsmedAccountList = idsmedUser.getIdsmedAccounts().isEmpty() ? null : idsmedUser.getIdsmedAccounts();
            accountList.addAll(idsmedAccountList);
        }

        return accountList;
    }

    @Transactional
    @Override
    public ProductTechFeatureResponse getListOfMedicalConsumablesLabel(String langCode) throws IdsmedBaseException {
        loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, GET_LIST_OF_MEDICAL_CONSUMABLES_LABEL_METHOD_NAME, null);
        List<ProductTechnicalFeatureLabel> productTechnicalFeatureLabelList = productTechnicalFeatureLabelRepository
                .findByLabelTypeAndForMedicalConsumables(1, 1);

        List<ProductTechnicalFeatureLabel> productTechnicalLabels = new ArrayList<>();
        List<ProductTechnicalFeatureLabel> productFeatureLabels = new ArrayList<>();

        if (!CollectionUtils.isEmpty(productTechnicalFeatureLabelList)) {
            // Loop through and split product feature and technical then save label name only into its own list
            for (ProductTechnicalFeatureLabel label : productTechnicalFeatureLabelList) {
                if (label.getLabelType() == ProductTechnicalFeatureLabel.LabelTypeEnum.PRODUCT_FEATURE.getCode()) {
                    productFeatureLabels.add(label);
                } else {
                    productTechnicalLabels.add(label);
                }
            }
        }

        ProductTechFeatureResponse response = new ProductTechFeatureResponse(productFeatureLabels, productTechnicalLabels, langCode);
        loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, GET_LIST_OF_MEDICAL_CONSUMABLES_LABEL_METHOD_NAME, null);

        return response;
    }

    private Pair<String, String> processEmailNotificationRequestToApproveProductToVendorSenior(Product product,
                                                                                               List<IdsmedUser> vendorSeniors) throws IdsmedBaseException {
        Pair<String, String> emailPair = null;
        List<String> recipients = new ArrayList<>();
        for (IdsmedUser vendorSenior : vendorSeniors) {
            emailPair = emailService.generateRequestToApproveProductEmailSubjectAndContent(product, vendorSenior);
            Boolean isSendingEmail = emailService.checkSendEmail(vendorSenior.getIdsmedAccounts(), EmailTypeEnum.REQUEST_TO_APPROVE_PRODUCT_NOTIFICATION);
            if(isSendingEmail){
                EmailObject emailObject = new RequestToApproveProductEmailObject(
                        Arrays.asList(vendorSenior.getEmail()),
                        env.getProperty(PropertiesLabelConst.SMTP_SUPPORT_MAIL_LABEL),
                        emailPair
                );

                rabbitTemplate.convertAndSend(env.getProperty(IdsmedQueueConfig.RABBIT_MAIL_SERVICE_PROCESS_QUEUE),
                        emailObject);
            }
        }
        return emailPair;
    }

    private void processWebNotificationRequestToApproveProductToVendorSenior(IdsmedAccount sender,
                                                                             List<IdsmedUser> vendorSeniors,
                                                                             Pair<String, String> emailPair) throws IdsmedBaseException {
        for (IdsmedUser idsmedUser : vendorSeniors) {
            List<IdsmedAccount> vendorSeniorAccounts = idsmedAccountRepository.findAllByIdsmedUser(idsmedUser);
            notificationService.sendProductApprovedNotification(sender, vendorSeniorAccounts, emailPair);
        }
    }

    private void processEmailAndWebNotificationForProductCertificateApproval(Product product,
                                                                             List<IdsmedAccount> recipients,
                                                                             String documentAttachmentName,
                                                                             String fileStatus,
                                                                             IdsmedAccount sender) throws IdsmedBaseException {

        logger.info("Start send email notification for product document attachment with product id : {} ", product.getId());
        Pair<String, String> emailPair = null;
        for (IdsmedAccount recipient : recipients) {
            String name = recipient.getIdsmedUser().getFirstName() + " " + recipient.getIdsmedUser().getLastName();
            emailPair = emailService.generateProductCertificateApprovalEmailSubjectAndContent(
                    name, documentAttachmentName, fileStatus);

            Boolean isSendingEmail = emailService.checkSendEmail(
                    Arrays.asList(recipient),
                    EmailTypeEnum.PRODUCT_CERTIFICATE_APPROVAL_NOTIFICATION);
            if(isSendingEmail){
                EmailObject emailObject = new ProductCertificateApprovalEmailObject(
                        Arrays.asList(recipient.getIdsmedUser().getEmail()),
                        env.getProperty(PropertiesLabelConst.SMTP_SUPPORT_MAIL_LABEL),
                        emailPair);

                rabbitTemplate.convertAndSend(env.getProperty(IdsmedQueueConfig.RABBIT_MAIL_SERVICE_PROCESS_QUEUE), emailObject);
            }
        }
        logger.info("Start send email notification for product document attachment with product id : {} ", product.getId());

        logger.info("Start send web notification for product certificate approval with product id : {} ", product.getId());
        notificationService.sendProductCertificateApprovalNotification(sender, recipients, emailPair);
        logger.info("End send web notification for product certificate approval with product id : {} ", product.getId());

        // ------------- SEND EMAIL NOTIFICATION TO VENDOR FIRST & SECOND PERSON -------------
        processEmailNotificationAboutProductCertificateApprovalToVendorPersonInCharge(product, documentAttachmentName, fileStatus);

    }

    private Pair<String, String> processEmailNotificationForRequestToApproveProductCertificateToVendorSenior(Product product,
                                                                                                             String attachmentDocumentName,
                                                                                                             List<IdsmedUser> vendorSeniors) throws IdsmedBaseException {
        Pair<String, String> emailPair = null;

        for (IdsmedUser vendorSenior : vendorSeniors) {
            emailPair = emailService.generateRequestToApproveProductCertificateEmailSubjectAndContent(product, attachmentDocumentName, vendorSenior);
            List<String> recipients = new ArrayList<>();
            Boolean isSendingEmail = emailService.checkSendEmail(vendorSenior.getIdsmedAccounts(), EmailTypeEnum.REQUEST_TO_APPROVE_PRODUCT_CERTIFICATE_NOTIFICATION);
            if(isSendingEmail){
                EmailObject emailObject = new RequestToApproveProductCertificateEmailObject(
                        Arrays.asList(vendorSenior.getEmail()),
                        env.getProperty(PropertiesLabelConst.SMTP_SUPPORT_MAIL_LABEL),
                        emailPair
                );

                rabbitTemplate.convertAndSend(env.getProperty(IdsmedQueueConfig.RABBIT_MAIL_SERVICE_PROCESS_QUEUE),
                        emailObject);
            }
        }

        return emailPair;
    }

    private void processWebNotificationForRequestToApproveProductCertificateToVendorSenior(IdsmedAccount sender,
                                                                                           List<IdsmedUser> vendorSeniors,
                                                                                           Pair<String, String> emailPair) throws IdsmedBaseException {
        for (IdsmedUser idsmedUser : vendorSeniors) {
            List<IdsmedAccount> vendorSeniorAccounts = idsmedAccountRepository.findAllByIdsmedUser(idsmedUser);
            notificationService.sendProductApprovedNotification(sender, vendorSeniorAccounts, emailPair);
        }
    }

    /**
     * This method will do 3 part of email notification
     * First, send email notification and web notification to product creator and vendor coordinator with
     * account in SMARTSCP system
     * Second, send email notification only to vendor first and second person in charge
     * Third, send email and web notification for product certificate approval
     * @param product
     * @param sender
     * @return
     * @throws IdsmedBaseException
     */
    private List<IdsmedAccount> processEmailAndWebNotificationForProductApproval(Product product, IdsmedAccount sender) throws IdsmedBaseException {
        String status = null;
        for (StatusEnum statusEnum : StatusEnum.values()) {
            if (statusEnum.getCode().equals(product.getStatus())) {
                status = statusEnum.getName();
            }
        }

        List<IdsmedAccount> recipients = new ArrayList<>();

        // FIND PRODUCT CREATOR
        IdsmedAccount productCreator = idsmedAccountRepository.findById(product.getCreatedBy()).get();
        if (productCreator != null) {
            recipients.add(productCreator);
        }

        // FIND VENDOR COORDINATOR
        Vendor vendor = product.getVendor();
        if (vendor != null && vendor.getVendorCoordinator() != null) {
            IdsmedAccount coordinator = idsmedAccountRepository.findById(vendor.getVendorCoordinator()).get();
            if (coordinator != null) {
                recipients.add(coordinator);
            }
        }

        Pair<String, String> emailPair = null;
        for (IdsmedAccount recipient : recipients) {
            String name = recipient.getIdsmedUser().getFirstName() + " " + recipient.getIdsmedUser().getLastName();
            emailPair = emailService.generatedProductApprovedEmailSubjectAndContent(
                    name,
                    product.getVendor().getCompanyNamePrimary(),
                    product.getProductNamePrimary(),
                    product.getProductModel(),
                    product.getProductCode(),
                    status);

            // TO CHECK WHETHER USER WANT TO RECEIVE EMAIL NOTIFICATION OR NOT
            Boolean isSendingEmail = emailService.checkSendEmail(Arrays.asList(recipient),
                    EmailTypeEnum.PRODUCT_APPROVED_NOTIFICATION);
            if(isSendingEmail){
                EmailObject emailObject = new ProductApproveEmailObject(
                        Arrays.asList(recipient.getIdsmedUser().getEmail()),
                        env.getProperty(PropertiesLabelConst.SMTP_SUPPORT_MAIL_LABEL),
                        emailPair);

                rabbitTemplate.convertAndSend(env.getProperty(IdsmedQueueConfig.RABBIT_MAIL_SERVICE_PROCESS_QUEUE),
                        emailObject);
            }
        }

        logger.info("Start send product approval notification with product id  : {} ", product.getId());
        notificationService.sendProductApprovedNotification(sender, recipients, emailPair);
        logger.info("End send product approval notification with product id  : {} ", product.getId());

        // ------------- SEND EMAIL NOTIFICATION TO VENDOR FIRST & SECOND PERSON -------------
        processEmailNotificationAboutProductApprovalToVendorPersonInCharge(product, status);

        return recipients;
    }

    /**
     * Send email notification vendor person in charge 1 and 2 regarding product approval
     * @param product
     * @param status
     * @throws IdsmedBaseException
     */
    private void processEmailNotificationAboutProductApprovalToVendorPersonInCharge(Product product, String status) throws IdsmedBaseException {
        List<Pair<String, String>> vendorPersonInChargeRecipients = new ArrayList<>();
        Pair<String, String> account = null;

        // IF VENDOR PERSON IN CHARGE 1 IS NOT NULL
        if (!StringUtils.isEmpty(product.getVendor().getPersonIncharge1Name())) {
            account = Pair.of(product.getVendor().getPersonIncharge1Name(), product.getVendor().getPersonIncharge1Email());
            vendorPersonInChargeRecipients.add(account);
        }

        // IF VENDOR PERSON IN CHARGE 2 IS NOT NULL
        if (!StringUtils.isEmpty(product.getVendor().getPersonIncharge2Name())) {
            account = Pair.of(product.getVendor().getPersonIncharge2Name(), product.getVendor().getPersonIncharge2Email());
            vendorPersonInChargeRecipients.add(account);
        }

        Pair<String, String> emailPair = null;
        for (Pair<String, String> vendorPersonInCharge : vendorPersonInChargeRecipients) {
            emailPair = emailService.generatedProductApprovedEmailSubjectAndContent(
                    vendorPersonInCharge.getFirst(),
                    product.getVendor().getCompanyNamePrimary(),
                    product.getProductNamePrimary(),
                    product.getProductModel(),
                    product.getProductCode(),
                    status);

            EmailObject emailObject = new ProductApproveEmailObject(
                    Arrays.asList(vendorPersonInCharge.getSecond()),
                    env.getProperty(PropertiesLabelConst.SMTP_SUPPORT_MAIL_LABEL),
                    emailPair);

            rabbitTemplate.convertAndSend(env.getProperty(IdsmedQueueConfig.RABBIT_MAIL_SERVICE_PROCESS_QUEUE),
                    emailObject);
        }
    }

    private List<IdsmedAccount> processEmailAndWebNotificationForProductRejected(Product product,
                                                                  String rejectReason,
                                                                  IdsmedAccount sender) throws IdsmedBaseException {
        logger.info("Start send product reject email message with product id  : {} ", product.getId());

        List<IdsmedAccount> recipients = new ArrayList<>();

        // FIND PRODUCT CREATOR
        IdsmedAccount productCreator = idsmedAccountRepository.findById(product.getCreatedBy()).get();
        if (productCreator != null) {
            recipients.add(productCreator);
        }

        // FIND VENDOR COORDINATOR
        Vendor vendor = product.getVendor();
        if (vendor != null && vendor.getVendorCoordinator() != null) {
            IdsmedAccount coordinator = idsmedAccountRepository.findById(vendor.getVendorCoordinator()).get();
            if (coordinator != null) {
                recipients.add(coordinator);
            }
        }

        Pair<String, String> emailPair = null;
        for (IdsmedAccount recipient : recipients) {
            String name = recipient.getIdsmedUser().getFirstName() + " " + recipient.getIdsmedUser().getLastName();
            emailPair = emailService.generatedProductRejectedEmailSubjectAndContent(
                    name,
                    product.getVendor().getCompanyNamePrimary(),
                    product.getProductNamePrimary(),
                    product.getProductModel(),
                    product.getProductCode(),
                    StatusEnum.REJECT.getName(),
                    rejectReason);

            // TO CHECK WHETHER USER WANT TO RECEIVE EMAIL NOTIFICATION OR NOT
            Boolean isSendingEmail = emailService.checkSendEmail(Arrays.asList(recipient),
                    EmailTypeEnum.PRODUCT_APPROVED_NOTIFICATION);

            if(isSendingEmail){
                EmailObject emailObject = new ProductApproveEmailObject(
                        Arrays.asList(recipient.getIdsmedUser().getEmail()),
                        env.getProperty(PropertiesLabelConst.SMTP_SUPPORT_MAIL_LABEL),
                        emailPair);

                rabbitTemplate.convertAndSend(env.getProperty(IdsmedQueueConfig.RABBIT_MAIL_SERVICE_PROCESS_QUEUE),
                        emailObject);
            }
        }

        logger.info("Start send product reject web notification with product id : {} ", product.getId());
        notificationService.sendProductRejectedNotification(sender, recipients, emailPair);
        logger.info("End send product reject web notification with product id : {} ", product.getId());

        logger.info("End product update reject status with product id : {} ", product.getId());

        // ------------- SEND EMAIL NOTIFICATION TO VENDOR FIRST & SECOND PERSON -------------
        processEmailNotificationAboutProductRejectedToVendorPersonInCharge(product, rejectReason, StatusEnum.REJECT.getName());

        return recipients;
    }

    /**
     * Send email notification only regarding product certificate approval to vendor
     * first and second person in charge
     * @param product
     * @param attachmentDocumentName
     * @param fileStatus
     * @throws IdsmedBaseException
     */
    private void processEmailNotificationAboutProductCertificateApprovalToVendorPersonInCharge(Product product,
                                                                                               String attachmentDocumentName,
                                                                                               String fileStatus) throws IdsmedBaseException {
        List<Pair<String, String>> vendorPersonInChargeRecipients = new ArrayList<>();
        Pair<String, String> account = null;

        // IF VENDOR PERSON IN CHARGE 1 IS NOT NULL
        if (!StringUtils.isEmpty(product.getVendor().getPersonIncharge1Name())) {
            account = Pair.of(product.getVendor().getPersonIncharge1Name(), product.getVendor().getPersonIncharge1Email());
            vendorPersonInChargeRecipients.add(account);
        }

        // IF VENDOR PERSON IN CHARGE 2 IS NOT NULL
        if (!StringUtils.isEmpty(product.getVendor().getPersonIncharge2Name())) {
            account = Pair.of(product.getVendor().getPersonIncharge2Name(), product.getVendor().getPersonIncharge2Email());
            vendorPersonInChargeRecipients.add(account);
        }

        Pair<String, String> emailPair = null;
        for (Pair<String, String> vendorPersonInCharge : vendorPersonInChargeRecipients) {
            emailPair = emailService.generateProductCertificateApprovalEmailSubjectAndContent(
                    vendorPersonInCharge.getFirst(), attachmentDocumentName, fileStatus);

            EmailObject emailObject = new ProductCertificateApprovalEmailObject(
                    Arrays.asList(vendorPersonInCharge.getSecond()),
                    env.getProperty(PropertiesLabelConst.SMTP_SUPPORT_MAIL_LABEL),
                    emailPair);

            rabbitTemplate.convertAndSend(env.getProperty(IdsmedQueueConfig.RABBIT_MAIL_SERVICE_PROCESS_QUEUE),
                    emailObject);
        }
    }

    private void processEmailNotificationAboutProductRejectedToVendorPersonInCharge(Product product, String rejectReason, String status) throws IdsmedBaseException {
        List<Pair<String, String>> vendorPersonInChargeRecipients = new ArrayList<>();
        Pair<String, String> account = null;

        // IF VENDOR PERSON IN CHARGE 1 IS NOT NULL
        if (!StringUtils.isEmpty(product.getVendor().getPersonIncharge1Name())) {
            account = Pair.of(product.getVendor().getPersonIncharge1Name(), product.getVendor().getPersonIncharge1Email());
            vendorPersonInChargeRecipients.add(account);
        }

        // IF VENDOR PERSON IN CHARGE 2 IS NOT NULL
        if (!StringUtils.isEmpty(product.getVendor().getPersonIncharge2Name())) {
            account = Pair.of(product.getVendor().getPersonIncharge2Name(), product.getVendor().getPersonIncharge2Email());
            vendorPersonInChargeRecipients.add(account);
        }

        Pair<String, String> emailPair = null;
        for (Pair<String, String> vendorPersonInCharge : vendorPersonInChargeRecipients) {
            emailPair = emailService.generatedProductRejectedEmailSubjectAndContent(
                    vendorPersonInCharge.getFirst(),
                    product.getVendor().getCompanyNamePrimary(),
                    product.getProductNamePrimary(),
                    product.getProductModel(),
                    product.getProductCode(),
                    status,
                    rejectReason);

            EmailObject emailObject = new ProductRejectEmailObject(
                    Arrays.asList(vendorPersonInCharge.getSecond()),
                    env.getProperty(PropertiesLabelConst.SMTP_SUPPORT_MAIL_LABEL),
                    emailPair);

            rabbitTemplate.convertAndSend(env.getProperty(IdsmedQueueConfig.RABBIT_MAIL_SERVICE_PROCESS_QUEUE),
                    emailObject);
        }
    }

    @Override
    @Transactional
    public ProductListResponse resumeInactiveProduct(ProductInactiveRequest request) throws IdsmedBaseException {
        loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, RESUME_INACTIVE_PRODUCT_METHOD_NAME, request.getLoginId());

        // Get current login user's id
        Long currentUserId = idsmedAccountRepository.findByLoginId(request.getLoginId()).getId();
        List<Product> products = request.getId().stream()
                .filter(p -> {
                    if (productRepository.findById(p).isPresent()) return true;
                    return false;
                }).map(p -> {
                    Product product = productRepository.findById(p).get();
                    product.setUpdatedBy(currentUserId);
                    product.setPreviousStatus(
                            product.getStatus() != null ? product.getStatus() : StatusEnum.ARCHIVED.getCode()
                    );
                    product.setStatus(StatusEnum.APPROVED.getCode());
                    return product;
                }).collect(Collectors.toList());

        productRepository.saveAll(products);

        if (!CollectionUtils.isEmpty(products)) {
            productMediaRepository.updateProductMediaStatus(products, currentUserId, DateTimeHelper.getCurrentTimeUTC(), StatusEnum.APPROVED.getCode());
            productFeatureRepository.updateProductFeatureStatus(products, currentUserId, DateTimeHelper.getCurrentTimeUTC(), StatusEnum.APPROVED.getCode());
            productTechnicalRepository.updateProductTechnicalStatus(products, currentUserId, DateTimeHelper.getCurrentTimeUTC(), StatusEnum.APPROVED.getCode());
        }
        String ossBucketDomain = env.getProperty(PropertiesLabelConst.OSS_BUCKET_DOMAIN);
        List<ProductResponse> productResponses = products
                .stream()
                .map(pr -> {
                    ProductResponse productResponse = new ProductResponse(pr, ossBucketDomain);
                    return productResponse;
                }).collect(Collectors.toList());

        ProductListResponse productListResponse = new ProductListResponse(productResponses);

        loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, RESUME_INACTIVE_PRODUCT_METHOD_NAME, request.getLoginId());

        return productListResponse;

    }

    @Override
    public List<BigDecimal> getMinAndMaxProductPrice(String langCode) throws IdsmedBaseException {
        loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, GET_MIN_AND_MAX_PRODUCT_PRICE_METHOD_NAME, null);
        BigDecimal minPrice = productRepository.getMinPriceOfActiveProduct();
        BigDecimal maxPrice = productRepository.getMaxPriceOfActiveProduct();

        List<BigDecimal> result = new ArrayList<>();
        result.add(minPrice);
        result.add(maxPrice);
        loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, GET_MIN_AND_MAX_PRODUCT_PRICE_METHOD_NAME, null);
        return result;
    }

    @Override
    public ProductTopRatingAndLatestApprovedResponse getTopRatingAndLatestApprovedProduct(String langCode, String encodedLoginId) throws IdsmedBaseException {
        loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, GET_TOP_RATING_AND_LATEST_APPROVED_PRODUCT_METHOD_NAME, encodedLoginId);
        String loginId = null;
        if (!StringUtils.isEmpty(encodedLoginId)) {
            loginId = URLDecoder.decode(encodedLoginId);
        }
        IdsmedAccount idsmedAccount = null;
        if (!StringUtils.isEmpty(loginId)) {
            idsmedAccount = idsmedAccountRepository.findByLoginId(loginId);
        }
        ProductTopRatingAndLatestApprovedResponse response = new ProductTopRatingAndLatestApprovedResponse();
        List<Product> topFiveProductRating = productRepository.getTopFiveProductRating();;
        List<Product> latestFiveApprovedProduct = productRepository.getLatestFiveApprovedProducts();

        List<ProductResponse> topRatingResponse = new ArrayList<>();
        List<ProductResponse> latestApprovedProductResponse = new ArrayList<>();
        String ossBucketDomain = env.getProperty(PropertiesLabelConst.OSS_BUCKET_DOMAIN);
        if (idsmedAccount == null) {
            if (!CollectionUtils.isEmpty(topFiveProductRating)) {
                topRatingResponse = topFiveProductRating.stream().map(p -> new ProductResponse(p, ossBucketDomain)).collect(Collectors.toList());
                response.setTopFiveProductRating(topRatingResponse);
            }

            if(!CollectionUtils.isEmpty(latestFiveApprovedProduct)) {
                latestApprovedProductResponse = latestFiveApprovedProduct.stream().map(p -> new ProductResponse(p, ossBucketDomain)).collect(Collectors.toList());
                response.setLatestFiveApprovedProduct(latestApprovedProductResponse);
            }

        } else {
            if(!CollectionUtils.isEmpty(topFiveProductRating)){
                for(Product product : topFiveProductRating){
                    List<ProductWishlist> productWishlists = productWishlistRepository.findProductWishlistByProductAndIdsmedAccount(product, idsmedAccount);
                    ProductResponse productResponse = null;
                    ProductWishlistResponse productWishlistResponse = new ProductWishlistResponse(product, productWishlists.size() != 0);
                    productResponse = new ProductResponse(product, productWishlistResponse, ossBucketDomain);

                    topRatingResponse.add(productResponse);
                }
                response.setTopFiveProductRating(topRatingResponse);
            }

            if(!CollectionUtils.isEmpty(latestFiveApprovedProduct)){
                for(Product product : latestFiveApprovedProduct){
                    List<ProductWishlist> productWishlists = productWishlistRepository.findProductWishlistByProductAndIdsmedAccount(product, idsmedAccount);
                    ProductResponse productResponse = null;

                    ProductWishlistResponse productWishlistResponse = new ProductWishlistResponse(product, productWishlists.size() != 0);
                    productResponse = new ProductResponse(product, productWishlistResponse, ossBucketDomain);

                    latestApprovedProductResponse.add(productResponse);
                }
                response.setLatestFiveApprovedProduct(latestApprovedProductResponse);
            }
        }

        loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, GET_TOP_RATING_AND_LATEST_APPROVED_PRODUCT_METHOD_NAME, encodedLoginId);
        return response;
    }

    @Override
    public List<ProductResponse> getAllApprovedProductHaveProductWishlist(String langCode, String encodedLoginId) throws IdsmedBaseException {
        loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, GET_ALL_APPROVED_PRODUCT_HAVE_PRODUCT_WISHLIST_METHOD_NAME, encodedLoginId);
        String loginId = null;
        if (!StringUtils.isEmpty(encodedLoginId)) {
            loginId = URLDecoder.decode(encodedLoginId);
        }
        IdsmedAccount idsmedAccount = idsmedAccountRepository.findByLoginId(loginId);
        List<ProductResponse> responses = new ArrayList<>();
        if(idsmedAccount == null){
            return responses;
        }
        List<Product> products = productRepository.findAllApprovedProductHaveProductWishlist(idsmedAccount);
        String ossBucketDomain = env.getProperty(PropertiesLabelConst.OSS_BUCKET_DOMAIN);
        if (!CollectionUtils.isEmpty(products)) {
            responses = products.stream().map(p -> new ProductResponse(p, ossBucketDomain)).collect(Collectors.toList());
        }
        loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, GET_ALL_APPROVED_PRODUCT_HAVE_PRODUCT_WISHLIST_METHOD_NAME, encodedLoginId);

        return responses;
    }

    @Override
    public ProductHierarchyLevelThreeResponse getThirdLevelProductHierarchy(String productHierarchyLevelFourCode, String langCode) throws IdsmedBaseException {
        ProductHierarchyLevelFour productHierarchyLevelFour = productHierarchyRepository.findByCode(productHierarchyLevelFourCode);
        if(productHierarchyLevelFour == null){
            throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.PRODUCT_HIERARCHY_NOT_FOUND_ERROR, LangHelper.getLangCode(langCode)));
        }
        ProductHierarchyLevelThree productHierarchyLevelThree = productHierarchyLevelFour.getProductHierarchyLevelThree();
        ProductHierarchyLevelThreeResponse productHierarchyLevelThreeResponse = new ProductHierarchyLevelThreeResponse(productHierarchyLevelThree);
        return productHierarchyLevelThreeResponse;
    }

    @Override
    public List<KeywordHistoryResponse> getPopularSearchKeyword(Long numberOfKeyword) throws IdsmedBaseException{
        List<Object> keywordHistories = keywordHistoryRepository.getPopularSearchKeyword(numberOfKeyword);
        List<KeywordHistoryResponse> keywordHistoryResponses = new ArrayList<>();
        for(Object keywordHistory: keywordHistories){
            if(keywordHistory instanceof String) {
                KeywordHistoryResponse keywordHistoryResponse = new KeywordHistoryResponse((String) keywordHistory);
                keywordHistoryResponses.add(keywordHistoryResponse);
            }
        }

        return keywordHistoryResponses;
    }

    private void loggerForAllMethodInThisService(String methodDescription, String methodName, String loginId){
        logger.info(methodDescription, methodName, loginId != null?loginId:null);
    }

    private void processEmailOfInactiveOrSuspendedProductToAllVendorUser(Vendor vendor, Product product, ProductInactiveRequest request) throws  IdsmedBaseException{
        // Email Notification Part
        logger.info("Start send email to Vendor User for inactive or suspended product with name {}",
            product.getProductNamePrimary());

        List<IdsmedUser> vendorUserList = idsmedUserRepository.findAllByCompanyCode(vendor.getCompanyCode());
        Pair<String, String> emailPair = null;
        for(IdsmedUser vendorUser : vendorUserList) {
            String vendorUserEmail = vendorUser.getEmail();

            emailPair = emailService.generatedInactiveOrSuspendedProductEmailSubjectAndContent(vendorUser,
                    vendor.getCompanyNamePrimary(), product, request.getInactiveReason());

            EmailObject emailForVendorUser = new InactiveOrSuspendedProductToVendorUserEmailObject(Arrays.asList(vendorUserEmail),
                    env.getProperty(PropertiesLabelConst.SMTP_SUPPORT_MAIL_LABEL), emailPair);

            rabbitTemplate.convertAndSend(env.getProperty(IdsmedQueueConfig.RABBIT_MAIL_SERVICE_PROCESS_QUEUE), emailForVendorUser);
        }
        logger.info("End send email to Vendor User for inactive / suspended product with name {}",
                product.getProductNamePrimary());
    }
}

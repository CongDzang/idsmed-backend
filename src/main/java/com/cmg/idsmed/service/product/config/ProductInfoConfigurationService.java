package com.cmg.idsmed.service.product.config;

import com.cmg.idsmed.common.exception.IdsmedBaseException;
import com.cmg.idsmed.dto.product.config.ProductInfoConfigurationListResponse;
import com.cmg.idsmed.dto.product.config.ProductInfoConfigurationRequest;
import com.cmg.idsmed.dto.product.config.ProductInfoConfigurationResponse;
import com.cmg.idsmed.dto.product.config.ProductLabelConfigResponse;

import java.util.List;

public interface ProductInfoConfigurationService {
    ProductInfoConfigurationListResponse getAllProductInfoConfiguration(Long countryId, Long provinceId, Integer status,  Integer pageIndex, Integer pageSize, String langCode) throws IdsmedBaseException;
    ProductInfoConfigurationListResponse getProductInfoConfigByCountryAndProvince(Long countryId, Long provinceId, Integer status, Integer pageIndex, Integer pageSize, String langCode) throws IdsmedBaseException;
    ProductInfoConfigurationResponse getProductInfoConfigurationById(Long id) throws IdsmedBaseException;
    ProductInfoConfigurationResponse createProductConfiguration(ProductInfoConfigurationRequest productInfoConfigurationRequest) throws IdsmedBaseException;
    ProductInfoConfigurationResponse editProductConfiguration(ProductInfoConfigurationRequest productInfoConfigurationRequest) throws IdsmedBaseException;
    List<ProductLabelConfigResponse> getProductLabelConfigs(Long vendorId, Integer status, String langCode) throws IdsmedBaseException;

}

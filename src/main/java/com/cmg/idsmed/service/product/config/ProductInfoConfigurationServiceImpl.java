package com.cmg.idsmed.service.product.config;

import com.cmg.idsmed.common.Constant.LoggerConstant;
import com.cmg.idsmed.common.enums.LangEnum;
import com.cmg.idsmed.common.exception.ErrorInfo;
import com.cmg.idsmed.common.exception.IdsmedBaseException;
import com.cmg.idsmed.common.utils.LangHelper;
import com.cmg.idsmed.dto.masterdata.CountryProvincePairRequest;
import com.cmg.idsmed.dto.product.config.*;
import com.cmg.idsmed.model.entity.masterdata.Country;
import com.cmg.idsmed.model.entity.masterdata.Province;
import com.cmg.idsmed.model.entity.product.config.ProductInfoConfig;
import com.cmg.idsmed.model.entity.product.config.ProductLabelConfig;
import com.cmg.idsmed.model.entity.vendor.Vendor;
import com.cmg.idsmed.model.entity.vendor.VendorAddress;
import com.cmg.idsmed.model.repo.auth.IdsmedAccountRepository;
import com.cmg.idsmed.model.repo.masterdata.CountryRepository;
import com.cmg.idsmed.model.repo.masterdata.ProvinceRepository;
import com.cmg.idsmed.model.repo.product.config.ProductInfoConfigurationRepository;

import com.cmg.idsmed.model.repo.product.config.ProductLabelConfigurationRepository;
import com.cmg.idsmed.model.repo.vendor.VendorRepository;
import com.cmg.idsmed.service.share.MessageResourceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.util.Pair;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Configuration
@Service
public class ProductInfoConfigurationServiceImpl implements ProductInfoConfigurationService {
    private static final Logger logger = LoggerFactory.getLogger(ProductInfoConfigurationServiceImpl.class);
    @Autowired
    private ProductInfoConfigurationRepository productInfoConfigurationRepository;

    @Autowired
    private Environment env;

    @Autowired
    private ProductLabelConfigurationRepository productLabelConfigurationRepository;

    @Autowired
    private IdsmedAccountRepository idsmedAccountRepository;

    @Autowired
    private MessageResourceService messageResourceService;

    @Autowired
    private CountryRepository countryRepository;

    @Autowired
    private ProvinceRepository provinceRepository;

    @Autowired
    private VendorRepository vendorRepository;

    @Value("${logger.start.method.description}")
    String START_METHOD_DESCRIPTION;

    @Value("${logger.end.method.description}")
    String END_METHOD_DESCRIPTION;
    private static final String GET_ALL_PRODUCT_INFO_CONFIGURATION_METHOD_NAME = "getAllProductInfoConfiguration";
    private static final String GET_PRODUCT_INFO_CONFIGURATION_BY_ID_METHOD_NAME = "getProductInfoConfigurationById";
    private static final String CREATE_PRODUCT_CONFIGURATION_METHOD_NAME = "createProductConfiguration";
    private static final String EDIT_PRODUCT_CONFIGURATION_METHOD_NAME = "editProductConfiguration";
    private static final String GET_PRODUCT_LABEL_CONFIGS_METHOD_NAME = "getProductLabelConfigs";


    public ProductInfoConfigurationListResponse getAllProductInfoConfiguration(Long countryId
            , Long provinceId
            , Integer status
            , Integer pageIndex
            , Integer pageSize, String langCode) throws IdsmedBaseException{
        loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, GET_ALL_PRODUCT_INFO_CONFIGURATION_METHOD_NAME, null);
        ProductInfoConfigurationListResponse productConfigurationListResponse = new ProductInfoConfigurationListResponse();

        if (countryId == null && provinceId == null) {
            productConfigurationListResponse = getAllProductInfoConfig(status, pageIndex, pageSize, langCode);
            return productConfigurationListResponse;
        }

        productConfigurationListResponse = getProductInfoConfigByCountryAndProvince(countryId, provinceId, status, pageIndex, pageSize, langCode);

        loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, GET_ALL_PRODUCT_INFO_CONFIGURATION_METHOD_NAME, null);
        return productConfigurationListResponse;
    }

    private ProductInfoConfigurationListResponse getAllProductInfoConfig(Integer status, Integer pageIndex, Integer pageSize, String langCode) {
        Pageable pageable = null;
        if (pageIndex != null && pageSize != null) {
            pageIndex = pageIndex - 1;
            pageable = PageRequest.of(pageIndex, pageSize);
        }

        ProductInfoConfigurationListResponse productConfigListResponse = new ProductInfoConfigurationListResponse();

        Pair<Integer, List<ProductInfoConfig>> resultPair = productInfoConfigurationRepository.findAllProductInfoConfig(status, pageable);
        List<ProductInfoConfigurationResponse> productInfoConfigResponses = resultPair.getSecond()
                .stream()
                .map(pic -> new ProductInfoConfigurationResponse(pic))
                .collect(Collectors.toList());
        if (pageIndex != null && pageSize != null) {
            productConfigListResponse = new ProductInfoConfigurationListResponse(resultPair.getFirst(), pageIndex, pageSize, productInfoConfigResponses);
        } else {
            productConfigListResponse = new ProductInfoConfigurationListResponse(resultPair.getFirst(), productInfoConfigResponses);
        }

        return productConfigListResponse;
    }

    public ProductInfoConfigurationListResponse getProductInfoConfigByCountryAndProvince(Long countryId, Long provinceId, Integer status, Integer pageIndex, Integer pageSize, String langCode){
        ProductInfoConfigurationListResponse productConfigurationListResponse = new ProductInfoConfigurationListResponse();

        Optional<Country> countryOpt = countryRepository.findById(countryId);
        if (!countryOpt.isPresent()) {
            logger.error("Can't find country with Id: {}", countryId);
            return productConfigurationListResponse;
        }

        Country country = countryOpt.get();

        Province province = null;
        if (provinceId != null) {
            Optional<Province> provinceOpt = provinceRepository.findById(provinceId);
            if (provinceOpt.isPresent())
                province = provinceOpt.get();
        }

        Pageable pageable = null;
        if (pageIndex != null && pageSize != null) {
            pageIndex = pageIndex - 1;
            pageable = PageRequest.of(pageIndex, pageSize);
        }

        Pair<Integer, List<ProductInfoConfig>> resultPair =
                productInfoConfigurationRepository.findAllProductInfoConfigByCountryAndProvince(country, province, status, pageable);

        List<ProductInfoConfigurationResponse> productInfoConfigurationResponses = resultPair.getSecond()
                .stream()
                .map(pic -> new ProductInfoConfigurationResponse(pic))
                .collect(Collectors.toList());
        if (pageIndex != null && pageSize != null) {
            productConfigurationListResponse = new ProductInfoConfigurationListResponse(resultPair.getFirst(), pageIndex, pageSize, productInfoConfigurationResponses);
        } else {
            productConfigurationListResponse = new ProductInfoConfigurationListResponse(resultPair.getFirst(), productInfoConfigurationResponses);
        }

        return productConfigurationListResponse;

    }

    @Override
    public ProductInfoConfigurationResponse getProductInfoConfigurationById(Long id) throws IdsmedBaseException {
        loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, GET_PRODUCT_INFO_CONFIGURATION_BY_ID_METHOD_NAME, null);
        Optional<ProductInfoConfig> productInfoConfig = productInfoConfigurationRepository.findById(id);
        if (!productInfoConfig.isPresent()) {
            throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.PRODUCT_INFO_CONFIG_NOT_FOUND_ERROR, "en"));
        }
        ProductInfoConfigurationResponse productInfoConfigurationResponse = new ProductInfoConfigurationResponse(productInfoConfig.get());
        loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, GET_PRODUCT_INFO_CONFIGURATION_BY_ID_METHOD_NAME, null);
        return productInfoConfigurationResponse;
    }

    @Override
    @Transactional
    public ProductInfoConfigurationResponse createProductConfiguration(ProductInfoConfigurationRequest productInfoConfigurationRequest) throws IdsmedBaseException {
        loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, CREATE_PRODUCT_CONFIGURATION_METHOD_NAME, productInfoConfigurationRequest.getLoginId());
        ProductInfoConfig productInfoConfig = new ProductInfoConfig();
        Long requesterId = idsmedAccountRepository.findByLoginId(productInfoConfigurationRequest.getLoginId()).getId();

        Country country = countryRepository.getOne(productInfoConfigurationRequest.getCountryId());

        if (country == null) {
            throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.COUNTRY_CAN_NOT_FOUND_ERROR
                    , LangHelper.getLangCode(LangEnum.CHINA.getCode())));
        }

        if (CollectionUtils.isEmpty(productInfoConfigurationRequest.getProductLabelConfigurationRequestList())) {
            throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.PRODUCT_LABEL_CONFIG_IS_EMPTY_ERROR
                    , LangHelper.getLangCode(LangEnum.CHINA.getCode())));
        }

        productInfoConfig.setCountry(country);

        Province province = null;
        if (productInfoConfigurationRequest.getProvinceId() != null) {
            province = provinceRepository.getOne(productInfoConfigurationRequest.getProvinceId());
        }

        if (province == null) {
            if (checkProductInfoConfigExistsByCountry(country)) {
                throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.PRODUCT_CONFIG_EXIST_ERROR, LangHelper.getLangCode(productInfoConfigurationRequest.getLangCode())));
            }
        } else {
            if (checkProductInfoConfigExistsByCountryAndProvince(country, province)) {
                throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.PRODUCT_CONFIG_EXIST_ERROR, LangHelper.getLangCode(productInfoConfigurationRequest.getLangCode())));
            }
        }
        productInfoConfig.setProvince(province);
        productInfoConfig.setCreatedBy(requesterId);
        productInfoConfig.setUpdatedBy(requesterId);
        productInfoConfig.setStatus(ProductInfoConfig.ProductConfigStatusEnum.ACTIVE.getCode());
        productInfoConfig = productInfoConfigurationRepository.save(productInfoConfig);

        List<ProductLabelConfig> productLabelConfigs = new ArrayList<>();

        if(CollectionUtils.isEmpty(productInfoConfigurationRequest.getProductLabelConfigurationRequestList())){

        }
        if (!CollectionUtils.isEmpty(productInfoConfigurationRequest.getProductLabelConfigurationRequestList())){
            for(ProductLabelConfigurationRequest productLabelConfigurationRequest: productInfoConfigurationRequest.getProductLabelConfigurationRequestList()){
                ProductLabelConfig productLabelConfig = new ProductLabelConfig(productLabelConfigurationRequest);
                productLabelConfig.setProductInfoConfig(productInfoConfig);
                productLabelConfig.setCreatedBy(requesterId);
                productLabelConfig.setUpdatedBy(requesterId);
                productLabelConfig.setSequence(productLabelConfigurationRequest.getSequence());
                productLabelConfig.setStatus(productLabelConfigurationRequest.getStatus());
                productLabelConfigs.add(productLabelConfig);
            }
        }
        if(!CollectionUtils.isEmpty(productLabelConfigs)) {
            productLabelConfigurationRepository.saveAll(productLabelConfigs);
            productInfoConfig.setProductLabelConfigs(productLabelConfigs);
        }
        loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, CREATE_PRODUCT_CONFIGURATION_METHOD_NAME, productInfoConfigurationRequest.getLoginId());
        return new ProductInfoConfigurationResponse(productInfoConfig);
    }

    private Boolean checkProductInfoConfigExistsByCountry(Country country) {
        return productInfoConfigurationRepository.findFirstByCountryAndProvinceIs(country, null) != null;
    }

    private Boolean checkProductInfoConfigExistsByCountryAndProvince(Country country, Province province) {
        return productInfoConfigurationRepository.findFirstByCountryAndProvince(country, province) != null;
    }
    @Override
    @Transactional
    public ProductInfoConfigurationResponse editProductConfiguration(ProductInfoConfigurationRequest productInfoConfigurationRequest) throws IdsmedBaseException {
        loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, EDIT_PRODUCT_CONFIGURATION_METHOD_NAME, productInfoConfigurationRequest.getLoginId());
        String langCode = LangHelper.getLangCode(productInfoConfigurationRequest.getLangCode());
        Long requesterId = idsmedAccountRepository.findByLoginId(productInfoConfigurationRequest.getLoginId()).getId();

        ProductInfoConfig productInfoConfig = productInfoConfigurationRepository.getOne(productInfoConfigurationRequest.getId());

        if(productInfoConfig == null){
            throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.PRODUCT_INFO_CONFIG_NOT_FOUND_ERROR, langCode));
        }
        List<ProductLabelConfigurationRequest> productLabelConfigurationRequests = productInfoConfigurationRequest.getProductLabelConfigurationRequestList();

        //Get list of existing Product label config send along with request.
        List<ProductLabelConfig> existProductLabelConfigs = productLabelConfigurationRequests
                .stream()
                .filter(eplc -> eplc.getId() != null)
                .map(r -> {
                    ProductLabelConfig productLabelConfig = productLabelConfigurationRepository.getOne(r.getId());
                    productLabelConfig.setLabelPrimaryName(r.getLabelPrimaryName());
                    productLabelConfig.setLabelSecondaryName(r.getLabelSecondaryName());
                    productLabelConfig.setUpdatedBy(requesterId);
                    productLabelConfig.setSequence(r.getSequence());
                    productLabelConfig.setIsMandatory(r.getMandatory());
                    productLabelConfig.setIsExpired(r.getExpired());
                    productLabelConfig.setStatus(r.getStatus());
                    return productLabelConfig;
                })
                .collect(Collectors.toList());


        //Get list of new Product label config
        List<ProductLabelConfig> newProductLabelConfigs = productLabelConfigurationRequests
                .stream()
                .filter(nplc -> nplc.getId() == null)
                .map(r -> {
                    ProductLabelConfig productlabelConfig = new ProductLabelConfig(r);
                    productlabelConfig.setProductInfoConfig(productInfoConfig);
                    productlabelConfig.setStatus(r.getStatus());
                    productlabelConfig.setCreatedBy(requesterId);
                    productlabelConfig.setUpdatedBy(requesterId);
                    productlabelConfig.setSequence(r.getSequence());
                    return productlabelConfig;
                })
                .collect(Collectors.toList());

        existProductLabelConfigs.addAll(newProductLabelConfigs);

        existProductLabelConfigs =productLabelConfigurationRepository.saveAll(existProductLabelConfigs);

        productInfoConfig.setProductLabelConfigs(existProductLabelConfigs);

        loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, EDIT_PRODUCT_CONFIGURATION_METHOD_NAME, productInfoConfigurationRequest.getLoginId());

        return new ProductInfoConfigurationResponse(productInfoConfig);
    }

    public List<ProductLabelConfigResponse> getProductLabelConfigs(Long vendorId, Integer status, String langCode) throws IdsmedBaseException {
        loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, GET_PRODUCT_LABEL_CONFIGS_METHOD_NAME,null);
        Vendor vendor = vendorRepository.getOne(vendorId);
        if(vendor == null){
            throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.VENDOR_NOT_FOUND_ERROR, LangHelper.getLangCode(langCode)));
        }
        Country country = null;
        Province province = null;
        
        for(VendorAddress vendorAddress : vendor.getVendorAddresses()){
            if (vendorAddress.getType().equals(VendorAddress.VendorAddressTypeEnum.ADDRESS.getCode())) {
                country = vendorAddress.getCountry();
                province = vendorAddress.getProvince();
            }
        }

        if (country == null) {
            throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.COUNTRY_CAN_NOT_FOUND_ERROR, LangHelper.getLangCode(langCode)));
        }

        ProductInfoConfig productInfoConfig = productInfoConfigurationRepository.findFirstByCountryAndProvince(country, province);
        if(productInfoConfig == null){
            productInfoConfig = productInfoConfigurationRepository.findFirstByCountryAndProvince(country, null);
        }
        List<ProductLabelConfig> productLabelConfigs = productLabelConfigurationRepository.findAllProductLabelConfigByProductInfoConfig(
                productInfoConfig, status);

        List<ProductLabelConfigResponse> productLabelConfigResponses = productLabelConfigs
                .stream()
                .map(plc -> new ProductLabelConfigResponse(plc))
                .collect(Collectors.toList());
        loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, GET_PRODUCT_LABEL_CONFIGS_METHOD_NAME,null);
        return productLabelConfigResponses;
    }
    private void loggerForAllMethodInThisService(String methodDescription, String methodName, String loginId){
        logger.info(methodDescription, methodName, loginId != null?loginId:null);
    }
}

package com.cmg.idsmed.service.schedule;

import com.cmg.idsmed.common.Constant.IdsmedRoleCodeConst;
import com.cmg.idsmed.common.Constant.PropertiesLabelConst;
import com.cmg.idsmed.common.exception.IdsmedBaseException;
import com.cmg.idsmed.common.utils.DateTimeHelper;
import com.cmg.idsmed.config.rabbitmq.IdsmedQueueConfig;
import com.cmg.idsmed.mail.EmailObject;
import com.cmg.idsmed.mail.EmailService;
import com.cmg.idsmed.mail.ProductDocumentAttachmentExpiryReminderEmailObject;
import com.cmg.idsmed.mail.VendorFileAttachmentExpiryReminderEmailObject;
import com.cmg.idsmed.model.entity.auth.IdsmedAccount;
import com.cmg.idsmed.model.entity.auth.IdsmedUser;
import com.cmg.idsmed.model.entity.notification.Notification;
import com.cmg.idsmed.model.entity.product.Product;
import com.cmg.idsmed.model.entity.product.ProductDocumentAttachment;
import com.cmg.idsmed.model.entity.product.ProductDocumentAttachmentReminder;
import com.cmg.idsmed.model.entity.vendor.FileAttachment;
import com.cmg.idsmed.model.entity.vendor.Vendor;
import com.cmg.idsmed.model.entity.vendor.VendorFileAttachmentReminder;
import com.cmg.idsmed.model.repo.auth.IdsmedAccountRepository;
import com.cmg.idsmed.model.repo.product.ProductDocumentAttachmentRepository;
import com.cmg.idsmed.model.repo.vendor.FileAttachmentRepository;
import com.cmg.idsmed.service.share.NotificationService;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.data.util.Pair;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.*;

//@Service
public class ScheduleTask {

//	@Autowired
//	private FileAttachmentRepository fileAttachmentRepository;
//
//	@Autowired
//	private ProductDocumentAttachmentRepository productDocumentAttachmentRepository;
//
//	@Autowired
//	private Environment env;
//
//	@Autowired
//	private EmailService emailService;
//
//	@Autowired
//	private RabbitTemplate rabbitTemplate;
//
//	@Autowired
//	private IdsmedAccountRepository idsmedAccountRepository;
//
//	@Autowired
//	private NotificationService notificationService;
//
//	private static final Logger logger = LoggerFactory.getLogger(ScheduleTask.class);
//
////	@Scheduled(fixedRate = 10000)
//	@Scheduled(cron = "0 0 1 * * ?")
//	public void scheduleVendorAttachmentExpiryReminder() {
//		List<Object[]> reminderData = fileAttachmentRepository.findRunnableReminder();
//
//		List<Pair<FileAttachment, Vendor>> dataPairs = processVendorAttachmentReminderData(reminderData);
//
//		List<IdsmedAccount> superAdminAccount = idsmedAccountRepository.findAccountsByRoleCode(IdsmedRoleCodeConst.SUPER_ADMIN);
//
//		//SEND NOTIFICATION EMAIL
//		List<EmailObject> emailObjects = new ArrayList<>();
//
//		List<Notification> notifications = new ArrayList<>();
//
//		if (!CollectionUtils.isEmpty(dataPairs)) {
//
//			for (Pair<FileAttachment, Vendor> pair : dataPairs) {
//				List<String> recipients = new ArrayList<>();
//				Vendor vendor = pair.getSecond();
//				FileAttachment fileAttachment = pair.getFirst();
//				String firstPICEmail = vendor.getPersonIncharge1Email();
//				String secondPICEmail = vendor.getPersonIncharge2Email();
//				if (!StringUtils.isEmpty(firstPICEmail)) {
//					recipients.add(firstPICEmail);
//				}
//
//				if (!StringUtils.isEmpty(secondPICEmail)) {
//					recipients.add(secondPICEmail);
//				}
//
//				Pair<String, String> emailPair = null;
//
//				try {
//					emailPair = emailService.generateVendorFileAttachmentExpiryEmailSubjectAndContent(vendor.getCompanyNamePrimary()
//							, fileAttachment.getDocumentName(), fileAttachment.getExpiryDate());
//				} catch (IdsmedBaseException e) {
//					logger.error("Error when generate VendorFileAttachmentExpiryEmailSubjectAndContent for document name: {}", fileAttachment.getDocumentName());
//				}
//
//				if (emailPair != null) {
//					emailObjects.add(new VendorFileAttachmentExpiryReminderEmailObject(recipients
//							, env.getProperty(PropertiesLabelConst.SMTP_SUPPORT_MAIL_LABEL), emailPair));
//				}
//
//				//Prepare notification list.
//				List<IdsmedAccount> recipientAccounts = idsmedAccountRepository.findAccountsByCompanyCodeAndEmails(vendor.getCompanyCode(), recipients);
//				Notification notification = notificationService.createVendorFileAttachmentExpiryReminderNotification(superAdminAccount.get(0), recipientAccounts, emailPair);
//				if (notification != null) {
//					notifications.add(notification);
//				}
//
//			}
//
//		}
//
//		if (!CollectionUtils.isEmpty(emailObjects)) {
//			emailObjects.forEach(emailObject -> {
//				rabbitTemplate.convertAndSend(env.getProperty(IdsmedQueueConfig.RABBIT_MAIL_SERVICE_PROCESS_QUEUE), emailObject);
//			});
//		}
//		//SEND NOTIFICATION
//		if (!CollectionUtils.isEmpty(notifications)) {
//			notifications.forEach(notif -> {
//				notificationService.sendVendorFileAttachmentExpiryNotifAndSetStatusToSent(notif);
//			});
//
//		}
//	}
//
//	private List<Pair<FileAttachment, Vendor>> processVendorAttachmentReminderData(List<Object[]> data) {
//		List<Pair<FileAttachment, Vendor>> dataPairs = new ArrayList<>();
//		if (CollectionUtils.isEmpty(data)) {
//			return  dataPairs;
//		}
//
//		for (Object[] e : data) {
//			Vendor vendor = (Vendor) e[0];
//			FileAttachment fileAttachment = (FileAttachment) e[1];
//			VendorFileAttachmentReminder reminder = (VendorFileAttachmentReminder) e[2];
//
//			if (fileAttachment.getExpiryDate() != null) {
//				Date expiryDate = fileAttachment.getExpiryDate();
//
//				if (reminder.getDaysBefore() != null ) {
//					Date currentDate = new Date();
//					Date compareDate = DateTimeHelper.addTime(currentDate, DateTimeHelper.DAY_OF_YEAR, reminder.getDaysBefore());
//					if (DateUtils.isSameDay(compareDate, expiryDate)) {
//						Pair<FileAttachment, Vendor> pair = Pair.of(fileAttachment, vendor);
//						dataPairs.add(pair);
//					}
//
//				}
//
//				if (reminder.getDaysAfter() != null) {
//					Date currentDate = new Date();
//					Date compareDate = DateTimeHelper.addTime(currentDate, DateTimeHelper.DAY_OF_YEAR, -reminder.getDaysAfter());
//					if (DateUtils.isSameDay(compareDate, expiryDate)) {
//						Pair<FileAttachment, Vendor> pair = Pair.of(fileAttachment, vendor);
//						dataPairs.add(pair);
//					}
//				}
//
//				if (reminder.getInExpiryDate() != null && reminder.getInExpiryDate() == true) {
//					Date currentDate = new Date();
//					if (DateUtils.isSameDay(currentDate, expiryDate)) {
//						Pair<FileAttachment, Vendor> pair = Pair.of(fileAttachment, vendor);
//						dataPairs.add(pair);
//					}
//				}
//			}
//		}
//
//		return dataPairs;
//	}
//
////	@Scheduled(fixedRate = 10000)
//	@Scheduled(cron = "0 0 1 * * ?")
//	public void scheduleProductDocumentAttachmentExpiryReminder() {
//		List<Object[]> reminderData = productDocumentAttachmentRepository.findRunnableReminder();
//
//		List<Pair<ProductDocumentAttachment, Product>> dataPairs = processProductDocumentAttachmentReminderData(reminderData);
//
//		//SEND NOTIFICATION EMAIL
//		List<EmailObject> emailObjects = new ArrayList<>();
//
//		List<Notification> notifications = new ArrayList<>();
//
//		if (!CollectionUtils.isEmpty(dataPairs)) {
//
//			for (Pair<ProductDocumentAttachment, Product> pair : dataPairs) {
//				List<String> recipients = new ArrayList<>();
//				Vendor vendor = pair.getSecond().getVendor();
//				Product product = pair.getSecond();
//				ProductDocumentAttachment productDocumentAttachment = pair.getFirst();
//				List<IdsmedAccount> superAdminAccount = idsmedAccountRepository.findAccountsByRoleCode(IdsmedRoleCodeConst.SUPER_ADMIN);
//
//				//get product creator account by product.getCreatedBy()
//				Optional<IdsmedAccount> productCreatorAccount = idsmedAccountRepository.findById(product.getCreatedBy());
//
//				IdsmedUser productCreator = productCreatorAccount.get().getIdsmedUser();
//
//				Pair<String, String> emailPair = null;
//
//				try {
//					emailPair = emailService.generateProductDocumentAttachmentExpiryEmailSubjectAndContent(product.getVendor().getCompanyNamePrimary()
//						, product.getProductNamePrimary(), product.getProductCode(), productDocumentAttachment.getDocumentName(), productDocumentAttachment.getExpiryDate());
//				} catch (IdsmedBaseException e) {
//					logger.error("Error when generate ProductDocumentAttachmentExpiryEmailSubjectAndContent for document name: {}", productDocumentAttachment.getDocumentName());
//				}
//
//				if (emailPair != null) {
//					emailObjects.add(new ProductDocumentAttachmentExpiryReminderEmailObject(Arrays.asList(productCreator.getEmail())
//							, env.getProperty(PropertiesLabelConst.SMTP_SUPPORT_MAIL_LABEL), emailPair));
//				}
//
//				//Prepare notification list.
//				List<IdsmedAccount> recipientAccounts = idsmedAccountRepository.findAccountsByCompanyCodeAndEmails(vendor.getCompanyCode(), Arrays.asList(productCreator.getEmail()));
//				Notification notification = notificationService.createProductDocumentAttachmentExpiryReminderNotification(superAdminAccount.get(0), recipientAccounts, emailPair);
//					if (notification != null) {
//						notifications.add(notification);
//				}
//
//			}
//
//		}
//
//		if (!CollectionUtils.isEmpty(emailObjects)) {
//			emailObjects.forEach(emailObject -> {
//				rabbitTemplate.convertAndSend(env.getProperty(IdsmedQueueConfig.RABBIT_MAIL_SERVICE_PROCESS_QUEUE), emailObject);
//			});
//		}
//
//		//SEND NOTIFICATION
//		if (!CollectionUtils.isEmpty(notifications)) {
//			notifications.forEach(notif -> {
//				notificationService.sendProductDocumentAttachmentExpiryNotifAndSetStatusToSent(notif);
//			});
//		}
//	}
//
//	private List<Pair<ProductDocumentAttachment, Product>> processProductDocumentAttachmentReminderData(List<Object[]> data) {
//		List<Pair<ProductDocumentAttachment, Product>> dataPairs = new ArrayList<>();
//		if (CollectionUtils.isEmpty(data)) {
//			return  dataPairs;
//		}
//
//		for (Object[] e : data) {
//			Product product = (Product) e[0];
//			ProductDocumentAttachment productDocumentAttachment = (ProductDocumentAttachment) e[1];
//			ProductDocumentAttachmentReminder reminder = (ProductDocumentAttachmentReminder) e[2];
//
//			if (productDocumentAttachment.getExpiryDate() != null) {
//				Date expiryDate = productDocumentAttachment.getExpiryDate();
//
//				if (reminder.getDaysBefore() != null ) {
//					Date currentDate = new Date();
//					Date compareDate = DateTimeHelper.addTime(currentDate, DateTimeHelper.DAY_OF_YEAR, reminder.getDaysBefore());
//					if (DateUtils.isSameDay(compareDate, expiryDate)) {
//						Pair<ProductDocumentAttachment, Product> pair = Pair.of(productDocumentAttachment, product);
//						dataPairs.add(pair);
//					}
//
//				}
//
//				if (reminder.getDaysAfter() != null) {
//					Date currentDate = new Date();
//					Date compareDate = DateTimeHelper.addTime(currentDate, DateTimeHelper.DAY_OF_YEAR, -reminder.getDaysAfter());
//					if (DateUtils.isSameDay(compareDate, expiryDate)) {
//						Pair<ProductDocumentAttachment, Product> pair = Pair.of(productDocumentAttachment, product);
//						dataPairs.add(pair);
//					}
//				}
//
//				if (reminder.getInExpiryDate() != null && reminder.getInExpiryDate() == true) {
//					Date currentDate = new Date();
//					if (DateUtils.isSameDay(currentDate, expiryDate)) {
//						Pair<ProductDocumentAttachment, Product> pair = Pair.of(productDocumentAttachment, product);
//						dataPairs.add(pair);
//					}
//				}
//			}
//		}
//
//		return dataPairs;
//	}
}

package com.cmg.idsmed.service.share;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Path;
import java.util.UUID;

import com.cmg.idsmed.common.Constant.PropertiesLabelConst;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.cmg.idsmed.common.Constant.ProductFieldsLength;
import com.cmg.idsmed.common.enums.LangEnum;
import com.cmg.idsmed.common.exception.ErrorInfo;
import com.cmg.idsmed.common.exception.IdsmedBaseException;

@Service
public class FileDownloadService {

	/**
	 * Downloads a file from a URL
	 *
	 * @param fileURL     HTTP URL of the file to be downloaded
	 * @param downloadDir path of the directory to save the file
	 * @throws IOException
	 */
	private static final Logger logger = LoggerFactory.getLogger(FileDownloadService.class);

	@Autowired
	private Environment env;

	@Autowired
	private MessageResourceService messageResourceService;

	public static final String FILE_TEMP_STORAGE_PATH = "idsmed.file.temp.storage";

	public File downloadProductCSVOrExcelFileFromOSS(String fileURL) throws IOException, IdsmedBaseException {
		File file = null;
		if (StringUtils.isEmpty(fileURL)) {
			throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.DOWNLOAD_FILE_FROM_OSS_ERROR, LangEnum.ENGLISH.getCode()));
		}
		try {
			String saveFileDirectory = new File(env.getProperty(FILE_TEMP_STORAGE_PATH).trim()).getAbsolutePath();
			File savedFileDir = new File(saveFileDirectory);
			if (!savedFileDir.exists()) {
				savedFileDir.mkdirs();
			}
			String ossBucketDomain = env.getProperty(PropertiesLabelConst.OSS_BUCKET_DOMAIN);
			fileURL = ossBucketDomain + fileURL;
			String fileName = fileURL.substring(fileURL.lastIndexOf('/') + 1, fileURL.length());
			Path targetPath = new File(savedFileDir + File.separator + fileName).toPath();
			file = new File(targetPath.toString());
			FileUtils.copyURLToFile(new URL(fileURL), file, 10000, 10000);
		} catch (IOException e) {
			logger.error("Download file error: " + e.getMessage());
			return null;
		}
		return file;
	}



	public File downloadFileFromUrl(String fileURL) {
		File file = null;
		if (StringUtils.isEmpty(fileURL)) {
			logger.error("Cant't download file from the empty url: {}", fileURL);
			return null;
		}

		URL url = null;

		try {
			url = new URL(fileURL);
		} catch (MalformedURLException e) {
			logger.error("Cant't download file from the url: {}", fileURL);
			return null;
		}

		try {
			String saveFileDirectory = new File(env.getProperty(FILE_TEMP_STORAGE_PATH).trim()).getAbsolutePath();
			File savedFileDir = new File(saveFileDirectory);
			if (!savedFileDir.exists()) {
				savedFileDir.mkdirs();
			}
			String originalFileName = fileURL.substring(fileURL.lastIndexOf('/') + 1, fileURL.length());
			String originalBaseName = FilenameUtils.getBaseName(url.getPath());
			if(originalBaseName.length() > ProductFieldsLength.PRODUCT_CSV_ORIGINAL_BASE_FILE_NAME_LENGTH) {
				originalBaseName = originalBaseName.substring(0, ProductFieldsLength.PRODUCT_CSV_ORIGINAL_BASE_FILE_NAME_LENGTH);
				originalFileName = originalBaseName + "." + FilenameUtils.getExtension(url.getPath());
			}
			String fileName = UUID.randomUUID().toString().concat(originalFileName.replaceAll("\\s", ""));
			Path targetPath = new File(savedFileDir + File.separator + fileName).toPath();
			file = new File(targetPath.toString());

			FileUtils.copyURLToFile(url, file, 1999999999, 1999999999);
		} catch (Exception e) {
			logger.error("Download file error: " + e.getMessage());
			return null;
		}
		return file;
	}

}

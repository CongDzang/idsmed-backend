package com.cmg.idsmed.service.share;

import com.cmg.idsmed.common.exception.IdsmedBaseException;

import java.util.List;

public interface FileService {

	Boolean replaceSingleLineOfTextFile(String pathToFile, String newText) throws IdsmedBaseException;

	List<String> readTextFile(String pathToFile);

}

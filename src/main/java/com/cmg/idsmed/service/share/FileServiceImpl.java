package com.cmg.idsmed.service.share;

import com.cmg.idsmed.common.exception.IdsmedBaseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

@Service
public class FileServiceImpl implements FileService {
	private static final Logger logger = LoggerFactory.getLogger(FileServiceImpl.class);

	public Boolean replaceSingleLineOfTextFile(String pathToFile, String newText) throws IdsmedBaseException {
		logger.info("Start write new string {} to first line of file {}", newText, pathToFile);
		if (StringUtils.isEmpty(pathToFile)) {
			return false;
		}

		try (FileWriter fileWriter = new FileWriter(pathToFile, false);
			 BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
		) {
				bufferedWriter.write(newText);
		} catch (IOException e) {
			logger.info("Error while writing new string {} to first line of file {}", newText, pathToFile);
			return false;
		}
		logger.info("Start write new string {} to first line of file {}", newText, pathToFile);
		return true;
	}

	@Override
	public List<String> readTextFile(String pathToFile) {
		logger.info("Start readding the file: {}", pathToFile);
		List<String> result = new ArrayList<>();
		try (
				FileReader fr = new FileReader(pathToFile);
				BufferedReader br = new BufferedReader(fr);) {
			String line = null;
			while((line = br.readLine()) != null) {
				result.add(line);
			}
		} catch (IOException e) {
			logger.error("Error while reading the file: {}", pathToFile);
			return result;
		}

		return result;
	}
}

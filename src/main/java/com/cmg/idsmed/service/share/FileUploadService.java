package com.cmg.idsmed.service.share;

import java.io.IOException;
import java.util.Map;

import org.springframework.web.multipart.MultipartFile;

import com.cmg.idsmed.common.enums.EntityType;
import com.cmg.idsmed.common.exception.IdsmedBaseException;
import com.cmg.idsmed.dto.product.ProductCSVUploadRequest;
import com.cmg.idsmed.dto.product.ProductCSVUploadedQueueMessage;
import com.cmg.idsmed.dto.product.ProductExcelUploadRequest;
import com.cmg.idsmed.dto.product.ProductExcelUploadedQueueMessage;
import com.cmg.idsmed.model.entity.product.log.ProductCSVLog;

public interface FileUploadService {
	Map<String, String> uploadImage(MultipartFile uploadFile, EntityType entityType) throws IOException, IdsmedBaseException;
	Boolean uploadProductCSV(ProductCSVUploadRequest request, MultipartFile file) throws IdsmedBaseException;
	Boolean uploadProductExcel(ProductExcelUploadRequest request, MultipartFile file) throws IdsmedBaseException;
	ProductCSVLog logUploadingProductCSVFile(String ossUrl, String originalFileName, String uuIdFileName, Integer status, Long vendorId, String vendorEmail, String companyCode);
	Boolean sendProductCSVUploadedMessage(ProductCSVUploadedQueueMessage message) throws IdsmedBaseException;
	Boolean sendProductExcelUploadedMessage(ProductExcelUploadedQueueMessage message) throws IdsmedBaseException;
	Map<String, String> uploadSingleFileToOss(MultipartFile file, String uploadPath) throws IOException;
}

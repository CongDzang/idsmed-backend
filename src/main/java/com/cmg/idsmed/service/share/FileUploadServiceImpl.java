package com.cmg.idsmed.service.share;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import com.cmg.idsmed.common.Constant.PropertiesLabelConst;
import com.cmg.idsmed.model.entity.auth.IdsmedAccount;
import com.cmg.idsmed.model.entity.vendor.Vendor;
import com.cmg.idsmed.model.repo.auth.IdsmedAccountRepository;
import com.cmg.idsmed.model.repo.vendor.VendorRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.AmqpException;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.FileCopyUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.cmg.idsmed.common.enums.EntityType;
import com.cmg.idsmed.common.enums.LangEnum;
import com.cmg.idsmed.common.enums.ProductCSVStatusEnum;
import com.cmg.idsmed.common.exception.ErrorInfo;
import com.cmg.idsmed.common.exception.IdsmedBaseException;
import com.cmg.idsmed.common.utils.OssUtils;
import com.cmg.idsmed.config.rabbitmq.IdsmedQueueConfig;
import com.cmg.idsmed.dto.product.ProductCSVUploadRequest;
import com.cmg.idsmed.dto.product.ProductCSVUploadedQueueMessage;
import com.cmg.idsmed.dto.product.ProductExcelUploadRequest;
import com.cmg.idsmed.dto.product.ProductExcelUploadedQueueMessage;
import com.cmg.idsmed.model.entity.product.log.ProductCSVLog;
import com.cmg.idsmed.model.repo.product.log.ProductCSVLogRepository;

/**
 * @author congdang
 */
@Service
@Transactional
@PropertySource({"classpath:${SYSTEM_ENVIRONMENT_VARIABLE}_fileconfig.properties", "classpath:${SYSTEM_ENVIRONMENT_VARIABLE}_security.properties"})
public class FileUploadServiceImpl implements FileUploadService{
	
	private static final String STORAGE_TYPE_PROPERTY_NAME = "storage.type";
	private static final String SERVER_STORAGE_TYPE = "server";
	public static final String HASH_FILE_NAME_KEY = "hashed_filename";
	public static final String ORIGINAL_FILE_NAME_KEY = "original_filename";
	public static final String FULL_PATH_KEY = "full_path";
	public static final String UPLOAD_URL = "upload_url";
	public static final String FULL_PATH_STORE_IMAGE_PROPERTIES_KEY = "image.file.store.path";
	private static String IDSMED_MEDIA_PATH = "image.file.media.path";

	public static final String PRODUCT_FOLDER_NAME="product";
	public static final String VENDOR_FOLDER_NAME="vendor";
	public static final String USER_FOLDER_NAME="user";

	private static final Logger logger = LoggerFactory.getLogger(FileUploadServiceImpl.class);

	@Autowired
	private Environment env;
	
	@Autowired
	private OssUtils ossUtils;

	@Autowired
	private MessageResourceService messageResourceService;

	@Autowired
	private ProductCSVLogRepository productCSVLogRepository;

	@Autowired
	private RabbitTemplate rabbitTemplate;

	@Autowired
	private VendorRepository vendorRepository;

	@Autowired
	private IdsmedAccountRepository accountRepository;

	public Map<String, String> uploadImage(MultipartFile uploadFile, EntityType entityType) throws IOException, IdsmedBaseException {
		final String STORAGE_TYPE = env.getProperty(STORAGE_TYPE_PROPERTY_NAME);
		return STORAGE_TYPE.equalsIgnoreCase(SERVER_STORAGE_TYPE) ? uploadImageToServe(uploadFile, entityType) : uploadImageToOss(uploadFile, entityType);
	}

	private Map<String, String> uploadImageToServe(MultipartFile uploadFile, EntityType entityType) throws IOException {
		Map<String, String> map = new HashMap<>();
		String UPLOAD_DIRECTORY;
		String path = env.getProperty(FULL_PATH_STORE_IMAGE_PROPERTIES_KEY)  + File.separator + env.getProperty(IDSMED_MEDIA_PATH) +  File.separator;
		
		switch (entityType) {
		case PRODUCT:
			path = path + PRODUCT_FOLDER_NAME;
			break;
		case VENDOR:
			path = path + VENDOR_FOLDER_NAME;
			break;
			case USER:
			path = path + USER_FOLDER_NAME;
			break;
		default:
			break;
		}
		
		UPLOAD_DIRECTORY = new File(path).getAbsolutePath() + File.separator;
		File uploadDir = new File(UPLOAD_DIRECTORY);
		if (!uploadDir.exists()) {
			uploadDir.mkdirs();
		}
		try {
			map = uploadSingleFile(uploadFile, UPLOAD_DIRECTORY);
		} catch(IOException e) {
			e.printStackTrace();
			throw e;
		}
		return map;
	}
	
	private Map<String, String> uploadImageToOss(MultipartFile uploadFile, EntityType entityType) throws IOException, IdsmedBaseException {
		Map<String, String> map = new HashMap<>();
		String uploadPath = null;
		switch (entityType) {
			case PRODUCT: uploadPath = env.getProperty(OssUtils.IDSMED_PRODUCT_OSS_UPLOAD_PATH); break;
			case VENDOR: uploadPath = env.getProperty(OssUtils.IDSMED_VENDOR_OSS_UPLOAD_PATH); break;
			case USER: uploadPath = env.getProperty(OssUtils.IDSMED_USER_OSS_UPLOAD_PATH); break;
		}

		if (uploadPath == null) {
			logger.error("Can't determine upload path");
			throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.UPLOAD_FILE_ERROR, LangEnum.ENGLISH.getCode()));
		}

		try {
			map = uploadSingleFileToOss(uploadFile, uploadPath);
		} catch(IOException e) {
			e.printStackTrace();
			throw e;
		}
		return map;
	}

	private Map<String, String> uploadSingleFile(MultipartFile file, String uploadDirectory) throws IOException {

		Map<String, String> map = new HashMap<>();
		if(!file.isEmpty()) {
			String fileStoredName = UUID.randomUUID().toString().concat(file.getOriginalFilename().replaceAll("\\s", ""));
			File serverFile = new File(uploadDirectory, fileStoredName);
			try {
				// Create the file on server
				FileCopyUtils.copy(file.getBytes(), serverFile);
			} catch (IOException e) {
				e.printStackTrace();
				throw e;
			}
			map.put(FileUploadServiceImpl.HASH_FILE_NAME_KEY, fileStoredName);
			map.put(FileUploadServiceImpl.ORIGINAL_FILE_NAME_KEY, file.getOriginalFilename());
			map.put(FileUploadServiceImpl.FULL_PATH_KEY, serverFile.getAbsolutePath());
		} else {
			return null;
		}
		return map;
	}
	//This method returns the relative path of the file which uploaded to Alibaba OSS bucket
	public Map<String, String> uploadSingleFileToOss(MultipartFile file, String uploadPath) throws IOException {
		Map<String, String> map = new HashMap<>();
		if(!file.isEmpty()) {
			String fileStoredName = UUID.randomUUID().toString().concat(file.getOriginalFilename().replaceAll("\\s", ""));
			String uploadURL = ossUtils.uploadFileByInputStream(uploadPath, fileStoredName, file.getInputStream());
			String relativePath = null;
			String ossBucketDomain = env.getProperty(PropertiesLabelConst.OSS_BUCKET_DOMAIN);

			if (!StringUtils.isEmpty(uploadURL) && !StringUtils.isEmpty(ossBucketDomain)) {
				relativePath = uploadURL.replace(ossBucketDomain, "");
			}
			map.put(FileUploadServiceImpl.HASH_FILE_NAME_KEY, fileStoredName);
			map.put(FileUploadServiceImpl.ORIGINAL_FILE_NAME_KEY, file.getOriginalFilename());
			map.put(FileUploadServiceImpl.UPLOAD_URL, relativePath);
		} else {
			return null;
		}
		return map;
	}

	public Boolean uploadProductCSV(ProductCSVUploadRequest request, MultipartFile file) throws IdsmedBaseException{
		if (file == null) {
			throw new IdsmedBaseException(
					messageResourceService.getErrorInfo(ErrorInfo.FILE_NOT_FOUND_ERROR, LangEnum.CHINA.getCode()));
		}

		IdsmedAccount uploader = accountRepository.findByLoginId(request.getUploadBy());
		if (uploader == null) {
			messageResourceService.getErrorInfo(ErrorInfo.ACCOUNT_NOT_EXIST_ERROR, LangEnum.CHINA.getCode());
		}

		Map<String, String> uploadInfo = singleUploadCSVProductFile(file);
		if (uploadInfo == null) {
			throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.UPLOAD_FILE_ERROR, LangEnum.ENGLISH.getCode()));
		}
		String originalFileName = uploadInfo.get(FileUploadServiceImpl.ORIGINAL_FILE_NAME_KEY);
		String uuidFileName = uploadInfo.get(FileUploadServiceImpl.HASH_FILE_NAME_KEY);
		String ossCSVUrl = uploadInfo.get(FileUploadServiceImpl.UPLOAD_URL);
		Vendor vendor = vendorRepository.getOne(request.getVendorId());
		String vendorEmail = vendor != null ? vendor.getEmail() : "";
		String companyCode = vendor != null ? vendor.getCompanyCode() : "";

		ProductCSVLog log = logUploadingProductCSVFile(ossCSVUrl, originalFileName, uuidFileName, ProductCSVStatusEnum.UPLOADED.getCode(), request.getVendorId(), vendorEmail, companyCode);


		ProductCSVUploadedQueueMessage message = new ProductCSVUploadedQueueMessage(ossCSVUrl, log.getId(), request.getVendorId(), request.getUploadBy());

		sendProductCSVUploadedMessage(message);

		return true;
	}

	public Boolean uploadProductExcel(ProductExcelUploadRequest request, MultipartFile file) throws IdsmedBaseException{
		if (file == null) {
			throw new IdsmedBaseException(
					messageResourceService.getErrorInfo(ErrorInfo.FILE_NOT_FOUND_ERROR, LangEnum.CHINA.getCode()));
		}

		Map<String, String> uploadInfo = singleUploadExcelProductFile(file);
		if (uploadInfo == null) {
			throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.UPLOAD_FILE_ERROR, LangEnum.ENGLISH.getCode()));
		}
		String originalFileName = uploadInfo.get(FileUploadServiceImpl.ORIGINAL_FILE_NAME_KEY);
		String uuidFileName = uploadInfo.get(FileUploadServiceImpl.HASH_FILE_NAME_KEY);
		String ossExcelUrl = uploadInfo.get(FileUploadServiceImpl.UPLOAD_URL);
		Vendor vendor = vendorRepository.getOne(request.getVendorId());
		String vendorEmail = vendor != null ? vendor.getEmail() : "";
		String companyCode = vendor != null ? vendor.getCompanyCode() : "";
		ProductCSVLog log = logUploadingProductCSVFile(ossExcelUrl, originalFileName, uuidFileName, ProductCSVStatusEnum.UPLOADED.getCode(), request.getVendorId(), vendorEmail, companyCode);

		ProductExcelUploadedQueueMessage  message = new ProductExcelUploadedQueueMessage(ossExcelUrl, log.getId(), request.getVendorId(), request.getUploadBy());

		sendProductExcelUploadedMessage(message);

		return true;
	}

	private Map<String, String> singleUploadExcelProductFile(MultipartFile file) throws IdsmedBaseException {
		Map<String, String> fileUploadInfoMap = new HashMap<>();
		String uploadPath = env.getProperty(OssUtils.IDSMED_PRODUCT_EXCEL_OSS_UPLOAD_PATH);
		try {
			fileUploadInfoMap = uploadSingleFileToOss(file, uploadPath);
		} catch (IOException e) {
			throw new IdsmedBaseException(
					messageResourceService.getErrorInfo(ErrorInfo.UPLOAD_FILE_ERROR, LangEnum.CHINA.getCode()));
		}

		return fileUploadInfoMap;
	}

	private Map<String, String> singleUploadCSVProductFile(MultipartFile file) throws IdsmedBaseException {
		Map<String, String> fileUploadInfoMap = new HashMap<>();
		String uploadPath = env.getProperty(OssUtils.IDSMED_PRODUCT_CSV_OSS_UPLOAD_PATH);
		try {
			fileUploadInfoMap = uploadSingleFileToOss(file, uploadPath);
		} catch (IOException e) {
			throw new IdsmedBaseException(
					messageResourceService.getErrorInfo(ErrorInfo.UPLOAD_FILE_ERROR, LangEnum.CHINA.getCode()));
		}

		return fileUploadInfoMap;
	}

	@Transactional
	public ProductCSVLog logUploadingProductCSVFile(String ossUrl, String originalFileName
			, String uuIdFileName
			, Integer status
			, Long vendorId
			, String vendorEmail
			, String companyCode) {
		ProductCSVLog log = new ProductCSVLog();
		log.setFileUrl(ossUrl);
		log.setOriginalFileName(originalFileName);
		log.setUuid(uuIdFileName);
		log.setStatus(status);
		log.setVendorId(vendorId);
		log.setVendorEmail(vendorEmail);
		log.setCompanyCode(companyCode);
		productCSVLogRepository.save(log);
		return log;
	}

	@Override
	public Boolean sendProductCSVUploadedMessage(ProductCSVUploadedQueueMessage message) throws IdsmedBaseException {
		try {
			rabbitTemplate.convertAndSend(env.getProperty(IdsmedQueueConfig.RABBIT_PRODUCT_CSV_PROCESS_QUEUE), message);
		} catch (AmqpException e) {
			logger.error("Error while send upload csv messgage to queue: {} ", e.getMessage());
		}
		return true;
	}
	
	@Override
	public Boolean sendProductExcelUploadedMessage(ProductExcelUploadedQueueMessage message) throws IdsmedBaseException {
		rabbitTemplate.convertAndSend(env.getProperty(IdsmedQueueConfig.RABBIT_PRODUCT_CSV_PROCESS_QUEUE), message);
		return true;
	}


}

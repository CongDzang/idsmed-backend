package com.cmg.idsmed.service.share;

import com.cmg.idsmed.model.entity.product.ProductBrand;
import com.cmg.idsmed.model.repo.product.ProductBrandRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class IdsmedServiceHelper {
	private static final Logger logger = LoggerFactory.getLogger(IdsmedServiceHelper.class);
	private static final Integer LENGTH_OF_DIGIT = 6;
	private static final Integer LENGTH_OF_ALPHABET = 3;
	@Autowired
	private ProductBrandRepository productBrandRepository;

	public static String randomAlphaNumeric(int count, int countOfDigit) {
		String alphabetString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		String numericString = "0123456789";
		StringBuilder builder = new StringBuilder();

		while (count-- != 0) {

			int character = (int) (Math.random() * alphabetString.length());

			builder.append(alphabetString.charAt(character));

		}
		while (countOfDigit-- != 0) {
			int character = (int) (Math.random() * numericString.length());
			builder.append(numericString.charAt(character));
		}
		return builder.toString();
	}

	public String generateBrandCode(){
		ProductBrand productBrandExisting = null;
		String brandCode;
		int retryCount = 0;
		int retryMax = 10;
		do {
			brandCode = IdsmedServiceHelper.randomAlphaNumeric(LENGTH_OF_ALPHABET, LENGTH_OF_DIGIT);
			productBrandExisting = productBrandRepository.findFirstByBrandCode(brandCode);
			if (productBrandExisting == null) {
				break;
			}
			logger.info("retry count is " + retryCount);
			retryCount++;
		} while (retryCount < retryMax);
		return brandCode;
	}
}

package com.cmg.idsmed.service.share;

import com.cmg.idsmed.common.enums.ImageVersionEnum;
import org.springframework.data.util.Pair;

import java.io.File;
import java.io.IOException;

public interface ImageProcessService {
	Pair<File, Integer> resizeProductImage(String imgUrl, ImageVersionEnum imageVersion) throws IOException;
}

package com.cmg.idsmed.service.share;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Path;

import javax.imageio.IIOException;
import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.cmg.idsmed.common.enums.ImageVersionEnum;
import com.cmg.idsmed.dto.product.CustomMultiPartFile;

@Service
public class ImageProcessServiceImpl implements ImageProcessService {
	public static final String FILE_TEMP_STORAGE_PATH = "idsmed.file.temp.storage";

	private static final Logger logger = LoggerFactory.getLogger(ImageProcessServiceImpl.class);

	@Autowired
	private Environment env;

	private static BufferedImage resizeImage(BufferedImage originalImage, Integer imageWidth, Integer imageHeight,
											 int type) {
		BufferedImage resizedImage = new BufferedImage(imageWidth, imageHeight, BufferedImage.TYPE_INT_RGB);
		Graphics g = resizedImage.getGraphics();
		g.drawImage(originalImage, 0, 0, imageWidth, imageHeight, null);
		g.dispose();
		return resizedImage;
	}

	public Pair<File, Integer> resizeProductImage(String imgUrl, ImageVersionEnum imageVersion) throws IOException {
		if (StringUtils.isEmpty(imgUrl)) {
			logger.error("Can't resized image from an empty url");
		}

		URL url =  null;
		try {
			url = new URL(imgUrl);
		} catch(MalformedURLException e) {
			logger.error("Can't resized image from an malformed url {}", imgUrl);
			return null;
		}
		BufferedImage originalImage = null;
		try {
		originalImage = ImageIO.read(url);
		}catch(IIOException e) {
			logger.error("Can't resized image from unsuppeorted image type {} " , imgUrl);
		}
		if (originalImage == null) {
			logger.error("Can't resized image", imgUrl);
			return null;

		}
		String fileExtension = FilenameUtils.getExtension(url.getPath());
		int type = originalImage.getType();

		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		BufferedImage resignedImg = resizeImage(originalImage, imageVersion.getWidth(), imageVersion.getHeight(), type);
		try {
			ImageIO.write(resignedImg, fileExtension.trim(), bos);
		}catch(IIOException e) {
			logger.error("Can't resized image from unsupported image type[]", imgUrl );
		}
		byte[] imageOutputStream = bos.toByteArray();

		String saveFileDirectory = new File(env.getProperty(FILE_TEMP_STORAGE_PATH).trim()).getAbsolutePath();

		File savedFileDir = new File(saveFileDirectory);
		if (!savedFileDir.exists()) {
			savedFileDir.mkdirs();
		}

		String fileName = FilenameUtils.getBaseName(url.getPath())
				+ "_" + imageVersion.getVersion().toString() + "." + FilenameUtils.getExtension(url.getPath());
		Path targetPath = new File(savedFileDir + File.separator + fileName).toPath();
		File file = new File(targetPath.toString());
		FileUtils.writeByteArrayToFile(file, imageOutputStream);
		return Pair.of(file, imageVersion.getVersion());
	}
	
	public static MultipartFile resizeUploadFile (MultipartFile originFile, ImageVersionEnum imageVersion) {
		BufferedImage originalImage;
		try {
			originalImage = ImageIO.read(originFile.getInputStream());
			if (originalImage == null) {
				logger.info("Can't resized image", originFile.getName());
				return originFile;
				
			}
			String fileExtension = FilenameUtils.getExtension(originFile.getOriginalFilename());
			int type = originalImage.getType();
			
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			BufferedImage resignedImg = resizeImage(originalImage, imageVersion.getWidth(), imageVersion.getHeight(), type);
			ImageIO.write(resignedImg, fileExtension, bos);
			byte[] imageOutputStream = bos.toByteArray();
			
			MultipartFile resizedFile = new CustomMultiPartFile(imageOutputStream, originFile.getOriginalFilename());
			return resizedFile;
		} catch (IOException e) {
			logger.info("Can't resized image", originFile.getName());
			return originFile;
		}
	}

}

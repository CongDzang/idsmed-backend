package com.cmg.idsmed.service.share;

import com.cmg.idsmed.common.exception.ErrorInfo;
import org.springframework.data.util.Pair;

public interface MessageResourceService {
    ErrorInfo getErrorInfo(Pair<Integer, String> errorPair, String langCode);

    String getMessage(String messageKey, String langCode);
}

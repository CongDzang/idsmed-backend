package com.cmg.idsmed.service.share;

import com.cmg.idsmed.common.exception.ErrorInfo;
import com.cmg.idsmed.common.utils.LangHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;

import java.util.Locale;
import java.util.Optional;

@Service
public class MessageResourceServiceImpl implements MessageResourceService{

    @Autowired
    private MessageSource messageSource;

    @Override
    public ErrorInfo getErrorInfo(Pair<Integer, String> errorPair, String langCode) {
        Integer code = Optional.ofNullable(errorPair.getFirst()).orElse(-1);
        Locale locale = LangHelper.getLocale(langCode);
        String messageKey = errorPair.getSecond();
        Object [] objs = {};
        String message= Optional.ofNullable(messageSource.getMessage(messageKey, objs, locale)).orElse("");
        return new ErrorInfo(code, message);
    }

    @Override
    public String getMessage(String messageKey, String langCode) {
        Locale locale = LangHelper.getLocale(langCode);
        Object [] objs = {};
        return Optional.ofNullable(messageSource.getMessage(messageKey, objs, locale)).orElse("");
    }
}

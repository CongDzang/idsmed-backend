package com.cmg.idsmed.service.share;

import com.cmg.idsmed.common.exception.IdsmedBaseException;
import com.cmg.idsmed.dto.notification.NotificationResponse;
import com.cmg.idsmed.dto.notification.NotificationsUpdateStatusRequest;
import com.cmg.idsmed.model.entity.auth.IdsmedAccount;
import com.cmg.idsmed.model.entity.notification.Notification;
import com.cmg.idsmed.model.entity.notification.NotificationRecipient;
import org.springframework.data.util.Pair;
import org.springframework.messaging.MessagingException;
import org.springframework.util.CollectionUtils;

import java.util.List;

public interface NotificationService {
	void sendUserApprovedNotification(IdsmedAccount sender, List<IdsmedAccount> recipients, Pair<String, String> emailPair);

	List<NotificationResponse> getAllNotArchivedNotificationByAccount(String loginId, String langCode) throws IdsmedBaseException;

	Boolean updateNotificationRecipientsStatus(NotificationsUpdateStatusRequest request) throws IdsmedBaseException;

	void sendProductApprovedNotification(IdsmedAccount sender, List<IdsmedAccount> recipients, Pair<String, String> notificationPair);

	void sendVendorRegistrationPendingReviewNotification(List<IdsmedAccount> sender, List<IdsmedAccount> recipients, Pair<String, String> emailPair);

	void sendVendorRegistrationPendingApprovedNotification(IdsmedAccount sender, List<IdsmedAccount> recipients, Pair<String, String> emailPair);

	void sendProductRejectedNotification(IdsmedAccount sender, List<IdsmedAccount> recipients, Pair<String, String> notificationPair);

	void sendProductCertificateApprovalNotification(IdsmedAccount sender, List<IdsmedAccount> recipients, Pair<String, String> notificationPair);

	void sendUserRejectNotification(IdsmedAccount sender, List<IdsmedAccount> recipients, Pair<String, String> notificationPair);

	Notification createVendorFileAttachmentExpiryReminderNotification(IdsmedAccount sender, List<IdsmedAccount> recipients, Pair<String, String> notifDataPair);

	void sendVendorFileAttachmentExpiryNotifAndSetStatusToSent(Notification notification);

	Notification createProductDocumentAttachmentExpiryReminderNotification(IdsmedAccount sender, List<IdsmedAccount> recipients, Pair<String, String> notifDataPair);

	void sendProductDocumentAttachmentExpiryNotifAndSetStatusToSent(Notification notification);

	void sendVendorProfileCertificateNotificationAndSetStatusToSent(IdsmedAccount sender, List<IdsmedAccount> recipients, Pair<String, String> notificationPair);

	void sendPendingApproveVendorUserToVendorAdminAndSetStatusToSent(IdsmedAccount sender, List<IdsmedAccount> recipients, Pair<String, String> notificationPair);

}

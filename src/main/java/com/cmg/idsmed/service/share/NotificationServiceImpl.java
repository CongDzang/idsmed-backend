package com.cmg.idsmed.service.share;

import com.cmg.idsmed.common.Constant.LoggerConstant;
import com.cmg.idsmed.common.enums.WebNotificationTypeEnum;
import com.cmg.idsmed.common.exception.ErrorInfo;
import com.cmg.idsmed.common.exception.IdsmedBaseException;
import com.cmg.idsmed.dto.notification.NotificationResponse;
import com.cmg.idsmed.dto.notification.NotificationsUpdateStatusRequest;
import com.cmg.idsmed.model.entity.auth.IdsmedAccount;
import com.cmg.idsmed.model.entity.notification.Notification;
import com.cmg.idsmed.model.entity.notification.NotificationRecipient;
import com.cmg.idsmed.model.repo.auth.IdsmedAccountRepository;
import com.cmg.idsmed.model.repo.common.NotificationRecipientRepository;
import com.cmg.idsmed.model.repo.common.NotificationRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.util.Pair;
import org.springframework.messaging.MessagingException;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Configuration
@Service
public class NotificationServiceImpl implements NotificationService {
	public static final String TOPIC_NOTIFICATION = "/topic/notification";
	public static final String NOTIFICATION_SUBJECT_LABEL = "subject";
	public static final String NOTIFICATION_CONTENT_LABEL = "content";
	public static final String NOTIFICATION_USER_APPROVED_SUBJECT = "Your account approved. Thank you.";
	public static final String NOTIFICATION_PRODUCT_APPROVED_SUBJECT = "Your product is approved. Thank you.";
	public static final String NOTIFICATION_PRODUCT_REJECT_SUBJECT = "Your product is reject. Thank you.";
	private static final Logger logger = LoggerFactory.getLogger(NotificationServiceImpl.class);

	@Autowired
	private SimpMessagingTemplate template;

	@Autowired
	private NotificationRepository notificationRepository;

	@Autowired
	private NotificationRecipientRepository recipientRepository;

	@Autowired
	private IdsmedAccountRepository accountRepository;

	@Autowired
	private MessageResourceService messageResourceService;

	@Autowired
	private Environment env;

	@Value("${logger.start.method.description}")
	String START_METHOD_DESCRIPTION;

	@Value("${logger.end.method.description}")
	String END_METHOD_DESCRIPTION;
	private static final String GET_ALL_NOT_ARCHIEVED_NOTIFICATION_BY_ACCOUNT_METHOD_NAME = "getAllNotArchivedNotificationByAccount";
	private static final String UPDATE_NOTIFICATION_RECIPIENT_STATUS_METHOD_NAME = "updateNotificationRecipientsStatus";


	public void sendUserApprovedNotification(IdsmedAccount sender, List<IdsmedAccount> recipients, Pair<String, String> emailPair) {
		if (CollectionUtils.isEmpty(recipients)) {
			logger.error("Can't send User Approved Notification - The recipient list is empty");
			return;
		}
		String userName = "";
		String contentTemplate;
		Notification notification = createUserApprovedNotification(sender, recipients, emailPair);
		try {
			sendNotification(notification);
		} catch (MessagingException e) {
			logger.error("Can't send User approved notification", e.getFailedMessage());
			return;
		}

		//Save notification_recipient status to "SENT"
		List<NotificationRecipient> recipientList = notification.getRecipients();
		recipientList.forEach(rp -> {
			rp.setStatus(NotificationRecipient.NotificationRecipientStatus.SENT.getCode());
		});
		recipientRepository.saveAll(recipientList);
	}

	@Transactional
	public Notification createUserApprovedNotification(IdsmedAccount sender, List<IdsmedAccount> recipients, Pair<String, String> emailPair) {
		List<Pair<String, String>> userApprovedNotifInfo = generateNotificationInfo(emailPair);
		String subject = userApprovedNotifInfo.stream()
				.filter(p -> p.getFirst().equals(NOTIFICATION_SUBJECT_LABEL))
				.findFirst()
				.get()
				.getSecond();
		String content = userApprovedNotifInfo.stream()
				.filter(p -> p.getFirst().equals(NOTIFICATION_CONTENT_LABEL))
				.findFirst()
				.get()
				.getSecond();

		Notification notification = new Notification(WebNotificationTypeEnum.USER_APPROVED.getCode(),
				subject, content, sender);

		notification = notificationRepository.save(notification);

		List<NotificationRecipient> recipientList = new ArrayList<>();
		for (IdsmedAccount acc : recipients) {
			recipientList.add(new NotificationRecipient(acc, notification));
		}

		recipientList = recipientRepository.saveAll(recipientList);
		notification.setRecipients(recipientList);
		return notification;
	}

	private List<Pair<String, String>> generateUserApprovedNotificationInfo(Pair<String, String> emailPair) {
		List<Pair<String, String>> userApprovedNotifInfo = new ArrayList<>();
		userApprovedNotifInfo.add(Pair.of(NOTIFICATION_SUBJECT_LABEL, emailPair.getFirst()));
		userApprovedNotifInfo.add(Pair.of(NOTIFICATION_CONTENT_LABEL, emailPair.getSecond()));
		return userApprovedNotifInfo;
	}

	public void sendNotification(Notification notification) throws MessagingException {
		NotificationResponse notificationResponse = new NotificationResponse(notification);
		template.convertAndSend(TOPIC_NOTIFICATION, notificationResponse);
	}

	@Override
	public List<NotificationResponse> getAllNotArchivedNotificationByAccount(String loginId, String langCode) throws IdsmedBaseException {
        loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, GET_ALL_NOT_ARCHIEVED_NOTIFICATION_BY_ACCOUNT_METHOD_NAME, loginId);

		IdsmedAccount account = accountRepository.findByLoginId(loginId);
		if (account == null) {
			logger.error("Account can't be found {}", loginId);
			throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.ACCOUNT_NOT_EXIST_ERROR, langCode));
		}
		List<Notification> notifications = notificationRepository.findAllNotArchivedNotificationByRecipient(account);
		loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, GET_ALL_NOT_ARCHIEVED_NOTIFICATION_BY_ACCOUNT_METHOD_NAME, loginId);
		return notifications.stream().map(n -> new NotificationResponse(n, account)).collect(Collectors.toList());

	}

	@Override
	public Boolean updateNotificationRecipientsStatus(NotificationsUpdateStatusRequest request) throws IdsmedBaseException{
		loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, UPDATE_NOTIFICATION_RECIPIENT_STATUS_METHOD_NAME, request.getLoginId());
		List<Notification> notifications = request.getNotificationIds()
				.stream()
				.map(id -> notificationRepository.getOne(id))
				.filter(n -> n != null)
				.collect(Collectors.toList());

		if (CollectionUtils.isEmpty(notifications)) {
			logger.error("Can't find notification need to update status");
			return false;
		}
		IdsmedAccount account = accountRepository.findByLoginId(request.getLoginId());

		if (account == null) {
			logger.error("Can't find Idsmed Account with loginId: {}", request.getLoginId());
			return false;
		}

		if (!validateNotificationRecipientStatus(request.getStatus())) {
			logger.error("Status is not correct", request.getStatus());
			return false;
		}

		List<NotificationRecipient> notificationRecipients =
				recipientRepository.findAllNotAchivedNotificationRecipientByNotifsAndRecipient(notifications
						, account, NotificationRecipient.NotificationRecipientStatus.ARCHIVED.getCode());
		if (!CollectionUtils.isEmpty(notificationRecipients)) {
			notificationRecipients.forEach(nr -> {
				nr.setStatus(request.getStatus());
			});
			recipientRepository.saveAll(notificationRecipients);
		}
		loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, UPDATE_NOTIFICATION_RECIPIENT_STATUS_METHOD_NAME, request.getLoginId());
		return true;
	}

	private Boolean validateNotificationRecipientStatus(Integer status) {
		for (NotificationRecipient.NotificationRecipientStatus statusEnum : NotificationRecipient.NotificationRecipientStatus.values()) {
			if (statusEnum.getCode().equals(status)) return true;
		}
		return false;
	}

	@Override
	public void sendProductApprovedNotification(IdsmedAccount sender, List<IdsmedAccount> recipients, Pair<String, String> notificationPair) {

		if (CollectionUtils.isEmpty(recipients)) {
			logger.error("Can't send Product Approved Notification - The recipient list is empty");
			return;
		}
		Notification notification = createProductApprovedNotification(sender, recipients, notificationPair);
		try {
			sendNotification(notification);
		} catch (MessagingException e) {
			logger.error("Can't send Product Approved Notification", e.getFailedMessage());
			return;
		}

		//Save notification_recipient status to "SENT"
		List<NotificationRecipient> recipientList = notification.getRecipients();
		recipientList.forEach(rp -> {
			rp.setStatus(NotificationRecipient.NotificationRecipientStatus.SENT.getCode());
		});
		recipientRepository.saveAll(recipientList);
	}

	@Transactional
	public Notification createProductApprovedNotification(IdsmedAccount sender, List<IdsmedAccount> recipients, Pair<String, String> notificationPair) {
		List<Pair<String, String>> productApprovedNotifInfo = generateNotificationInfo(notificationPair);
		String subject = productApprovedNotifInfo.stream()
				.filter(p -> p.getFirst().equals(NOTIFICATION_SUBJECT_LABEL))
				.findFirst()
				.get()
				.getSecond();
		String content = productApprovedNotifInfo.stream()
				.filter(p -> p.getFirst().equals(NOTIFICATION_CONTENT_LABEL))
				.findFirst()
				.get()
				.getSecond();

		Notification notification = new Notification(WebNotificationTypeEnum.PRODUCT_APPROVED.getCode(),
				subject, content, sender);

		notification = notificationRepository.save(notification);

		List<NotificationRecipient> recipientList = new ArrayList<>();
		for (IdsmedAccount acc : recipients) {
			recipientList.add(new NotificationRecipient(acc, notification));
		}

		recipientList = recipientRepository.saveAll(recipientList);
		notification.setRecipients(recipientList);
		return notification;
	}

	private List<Pair<String, String>> generateNotificationInfo(Pair<String, String> notificationPair) {
		List<Pair<String, String>> notificationInfo = new ArrayList<>();
		notificationInfo.add(Pair.of(NOTIFICATION_SUBJECT_LABEL, notificationPair.getFirst()));
		notificationInfo.add(Pair.of(NOTIFICATION_CONTENT_LABEL, notificationPair.getSecond()));
		return notificationInfo;
	}

	@Override
	public void sendVendorRegistrationPendingReviewNotification(List<IdsmedAccount> sender, List<IdsmedAccount> recipients, Pair<String, String> emailPair) {

		if (CollectionUtils.isEmpty(recipients)) {
			logger.error("Can't send Vendor Registration Pending Review Notification - The recipient list is empty");
			return;
		}
		Notification notification = createVendorRegistrationPendingReviewNotification(sender, recipients, emailPair);
		try {
			sendNotification(notification);
		} catch (MessagingException e) {
			logger.error("Can't send Product Reject Notification", e.getFailedMessage());
			return;
		}

		//Save notification_recipient status to "SENT"
		List<NotificationRecipient> recipientList = notification.getRecipients();
		recipientList.forEach(rp -> {
			rp.setStatus(NotificationRecipient.NotificationRecipientStatus.SENT.getCode());
		});
		recipientRepository.saveAll(recipientList);
	}

	@Transactional
	public Notification createVendorRegistrationPendingReviewNotification(List<IdsmedAccount> senders, List<IdsmedAccount> recipients, Pair<String, String> emailPair) {
		List<Pair<String, String>> vendorRegistrationPendingReviewNotifInfo = generateNotificationInfo(emailPair);
		String subject = vendorRegistrationPendingReviewNotifInfo.stream()
				.filter(p -> p.getFirst().equals(NOTIFICATION_SUBJECT_LABEL))
				.findFirst()
				.get()
				.getSecond();
		String content = vendorRegistrationPendingReviewNotifInfo.stream()
				.filter(p -> p.getFirst().equals(NOTIFICATION_CONTENT_LABEL))
				.findFirst()
				.get()
				.getSecond();
		Notification notification = null;
		for(IdsmedAccount sender : senders) {
			notification = new Notification(WebNotificationTypeEnum.VENDOR_REGISTRATION_PENDING_REVIEW.getCode(),
					subject, content, sender);
		}

		notification = notificationRepository.save(notification);

		List<NotificationRecipient> recipientList = new ArrayList<>();
		for (IdsmedAccount acc : recipients) {
			recipientList.add(new NotificationRecipient(acc, notification));
		}

		recipientList = recipientRepository.saveAll(recipientList);
		notification.setRecipients(recipientList);
		return notification;
	}

	@Override
	public void sendVendorRegistrationPendingApprovedNotification(IdsmedAccount sender, List<IdsmedAccount> recipients, Pair<String, String> emailPair) {

		if (CollectionUtils.isEmpty(recipients)) {
			logger.error("Can't send Vendor Registration Pending Approved Notification - The recipient list is empty");
			return;
		}
		Notification notification = createVendorRegistrationPendingApprovedNotification(sender, recipients, emailPair);
		try {
			sendNotification(notification);
		} catch (MessagingException e) {
			logger.error("Can't send Vendor Registration Pending Approved Notification", e.getFailedMessage());
			return;
		}

		//Save notification_recipient status to "SENT"
		List<NotificationRecipient> recipientList = notification.getRecipients();
		recipientList.forEach(rp -> {
			rp.setStatus(NotificationRecipient.NotificationRecipientStatus.SENT.getCode());
		});
		recipientRepository.saveAll(recipientList);
	}

	@Transactional
	public Notification createVendorRegistrationPendingApprovedNotification(IdsmedAccount sender, List<IdsmedAccount> recipients, Pair<String, String> emailPair) {
		List<Pair<String, String>> vendorRegistrationPendingApprovedNotifInfo = generateNotificationInfo(emailPair);
		String subject = vendorRegistrationPendingApprovedNotifInfo.stream()
				.filter(p -> p.getFirst().equals(NOTIFICATION_SUBJECT_LABEL))
				.findFirst()
				.get()
				.getSecond();
		String content = vendorRegistrationPendingApprovedNotifInfo.stream()
				.filter(p -> p.getFirst().equals(NOTIFICATION_CONTENT_LABEL))
				.findFirst()
				.get()
				.getSecond();
		Notification notification = new Notification(WebNotificationTypeEnum.VENDOR_REGISTRATION_PENDING_APPROVED.getCode(),
					subject, content, sender);

		notification = notificationRepository.save(notification);

		List<NotificationRecipient> recipientList = new ArrayList<>();
		for (IdsmedAccount acc : recipients) {
			recipientList.add(new NotificationRecipient(acc, notification));
		}

		recipientList = recipientRepository.saveAll(recipientList);
		notification.setRecipients(recipientList);
		return notification;
	}

	// New
	@Override
	public void sendProductRejectedNotification(IdsmedAccount sender, List<IdsmedAccount> recipients, Pair<String, String> notificationPair) {
		if (CollectionUtils.isEmpty(recipients)) {
			logger.error("Can't send Product Rejected Notification - The recipient list is empty");
			return;
		}
		Notification notification = createProductRejectedNotification(sender, recipients, notificationPair);
		try {
			sendNotification(notification);
		} catch (MessagingException e) {
			logger.error("Can't send Product Rejected Notification", e.getFailedMessage());
			return;
		}

		//Save notification_recipient status to "SENT"
		List<NotificationRecipient> recipientList = notification.getRecipients();
		recipientList.forEach(rp -> {
			rp.setStatus(NotificationRecipient.NotificationRecipientStatus.SENT.getCode());
		});
		recipientRepository.saveAll(recipientList);
	}

	// New
	@Transactional
	public Notification createProductRejectedNotification(IdsmedAccount sender, List<IdsmedAccount> recipients, Pair<String, String> notificationPair) {
		List<Pair<String, String>> productRejectNotifInfo = generateNotificationInfo(notificationPair);
		String subject = productRejectNotifInfo.stream()
				.filter(p -> p.getFirst().equals(NOTIFICATION_SUBJECT_LABEL))
				.findFirst()
				.get()
				.getSecond();
		String content = productRejectNotifInfo.stream()
				.filter(p -> p.getFirst().equals(NOTIFICATION_CONTENT_LABEL))
				.findFirst()
				.get()
				.getSecond();

		Notification notification = new Notification(WebNotificationTypeEnum.PRODUCT_REJECT.getCode(),
				subject, content, sender);

		notification = notificationRepository.save(notification);

		List<NotificationRecipient> recipientList = new ArrayList<>();
		for (IdsmedAccount acc : recipients) {
			recipientList.add(new NotificationRecipient(acc, notification));
		}

		recipientList = recipientRepository.saveAll(recipientList);
		notification.setRecipients(recipientList);
		return notification;
	}

	@Override
	public void sendProductCertificateApprovalNotification(IdsmedAccount sender, List<IdsmedAccount> recipients, Pair<String, String> notificationPair) {

		if (CollectionUtils.isEmpty(recipients)) {
			logger.error("Can't send Product Certificate Approval Notification - The recipient list is empty");
			return;
		}
		Notification notification = createProductApprovedNotification(sender, recipients, notificationPair);
		try {
			sendNotification(notification);
		} catch (MessagingException e) {
			logger.error("Can't send Product Certificate Approval Notification", e.getFailedMessage());
			return;
		}

		//Save notification_recipient status to "SENT"
		List<NotificationRecipient> recipientList = notification.getRecipients();
		recipientList.forEach(rp -> {
			rp.setStatus(NotificationRecipient.NotificationRecipientStatus.SENT.getCode());
		});
		recipientRepository.saveAll(recipientList);
	}

	@Override
	public void sendUserRejectNotification(IdsmedAccount sender, List<IdsmedAccount> recipients, Pair<String, String> notificationPair) {
		List<Pair<String, String>> userRejectNotifInfo = generateNotificationInfo(notificationPair);
		String subject = userRejectNotifInfo.stream()
				.filter(p -> p.getFirst().equals(NOTIFICATION_SUBJECT_LABEL))
				.findFirst()
				.get()
				.getSecond();
		String content = userRejectNotifInfo.stream()
				.filter(p -> p.getFirst().equals(NOTIFICATION_CONTENT_LABEL))
				.findFirst()
				.get()
				.getSecond();

		Notification notification = new Notification(WebNotificationTypeEnum.USER_REJECT.getCode(),
				subject, content, sender);

		notification = notificationRepository.save(notification);

		List<NotificationRecipient> recipientList = new ArrayList<>();
		for (IdsmedAccount acc : recipients) {
			recipientList.add(new NotificationRecipient(acc, notification));
		}

		recipientList = recipientRepository.saveAll(recipientList);
		notification.setRecipients(recipientList);
	}

	@Transactional
	public Notification  createVendorFileAttachmentExpiryReminderNotification(IdsmedAccount sender, List<IdsmedAccount> recipients, Pair<String, String> notifDataPair) {
		if (CollectionUtils.isEmpty(recipients)) {
			return null;
		}
		List<Pair<String, String>>  notifInfo = generateNotificationInfo(notifDataPair);
		String subject = notifInfo.stream()
				.filter(p -> p.getFirst().equals(NOTIFICATION_SUBJECT_LABEL))
				.findFirst()
				.get()
				.getSecond();
		String content = notifInfo.stream()
				.filter(p -> p.getFirst().equals(NOTIFICATION_CONTENT_LABEL))
				.findFirst()
				.get()
				.getSecond();

		Notification notification = new Notification(WebNotificationTypeEnum.VENDOR_FILE_ATTACHMENT_EXPIRY_REMINDER.getCode(),
				subject, content, sender);

		notification = notificationRepository.save(notification);

		List<NotificationRecipient> recipientList = new ArrayList<>();
		for (IdsmedAccount acc : recipients) {
			recipientList.add(new NotificationRecipient(acc, notification));
		}

		recipientList = recipientRepository.saveAll(recipientList);
		notification.setRecipients(recipientList);
		return notification;
	}


	public void sendVendorFileAttachmentExpiryNotifAndSetStatusToSent(Notification notification) {
		logger.info("Start send Vendor File Attachment Expiry Web Notification");
		try {
			sendNotification(notification);
		} catch (MessagingException e) {
			logger.error("Vendor File Attachment Expiry Reminder Notification: {}", e.getFailedMessage());
			return;
		}
		//Save notification_recipient status to "SENT"
		List<NotificationRecipient> recipientList = notification.getRecipients();
		recipientList.forEach(rp -> {
			rp.setStatus(NotificationRecipient.NotificationRecipientStatus.SENT.getCode());
		});

		recipientRepository.saveAll(recipientList);
	}

	@Transactional
	public Notification  createProductDocumentAttachmentExpiryReminderNotification(IdsmedAccount sender, List<IdsmedAccount> recipients, Pair<String, String> notifDataPair) {
		if (CollectionUtils.isEmpty(recipients)) {
			return null;
		}
		List<Pair<String, String>>  notifInfo = generateNotificationInfo(notifDataPair);
		String subject = notifInfo.stream()
				.filter(p -> p.getFirst().equals(NOTIFICATION_SUBJECT_LABEL))
				.findFirst()
				.get()
				.getSecond();
		String content = notifInfo.stream()
				.filter(p -> p.getFirst().equals(NOTIFICATION_CONTENT_LABEL))
				.findFirst()
				.get()
				.getSecond();

		Notification notification = new Notification(WebNotificationTypeEnum.PRODUCT_DOCUMENT_ATTACHMENT_EXPIRY_REMINDER.getCode(),
				subject, content, sender);

		notification = notificationRepository.save(notification);

		List<NotificationRecipient> recipientList = new ArrayList<>();
		for (IdsmedAccount acc : recipients) {
			recipientList.add(new NotificationRecipient(acc, notification));
		}

		recipientList = recipientRepository.saveAll(recipientList);
		notification.setRecipients(recipientList);
		return notification;
	}


	public void sendProductDocumentAttachmentExpiryNotifAndSetStatusToSent(Notification notification) {
		logger.info("Start send Product Document Attachment Expiry Web Notification");
		try {
			sendNotification(notification);
		} catch (MessagingException e) {
			logger.error("Product document Attachment Expiry Reminder Notification: {}", e.getFailedMessage());
			return;
		}
		//Save notification_recipient status to "SENT"
		List<NotificationRecipient> recipientList = notification.getRecipients();
		recipientList.forEach(rp -> {
			rp.setStatus(NotificationRecipient.NotificationRecipientStatus.SENT.getCode());
		});

		recipientRepository.saveAll(recipientList);
	}

	@Override
	public void sendVendorProfileCertificateNotificationAndSetStatusToSent(IdsmedAccount sender, List<IdsmedAccount> recipients, Pair<String, String> notificationPair) {
		if (CollectionUtils.isEmpty(recipients)) {
			logger.error("Can't send Vendor Profile Certificate Notification - The recipient list is empty");
			return;
		}
		Notification notification = createVendorProfileCertificateNotification(sender, recipients, notificationPair);
		try {
			sendNotification(notification);
		} catch (MessagingException e) {
			logger.error("Can't send Product Rejected Notification", e.getFailedMessage());
			return;
		}

		//Save notification_recipient status to "SENT"
		List<NotificationRecipient> recipientList = notification.getRecipients();
		recipientList.forEach(rp -> {
			rp.setStatus(NotificationRecipient.NotificationRecipientStatus.SENT.getCode());
		});
		recipientRepository.saveAll(recipientList);
	}

	private Notification createVendorProfileCertificateNotification(IdsmedAccount sender, List<IdsmedAccount> recipients, Pair<String, String> notificationPair){
		if (CollectionUtils.isEmpty(recipients)) {
			return null;
		}
		List<Pair<String, String>>  vendorProfileCertificateNotificationInfo = generateNotificationInfo(notificationPair);
		String subject = vendorProfileCertificateNotificationInfo.stream()
				.filter(p -> p.getFirst().equals(NOTIFICATION_SUBJECT_LABEL))
				.findFirst()
				.get()
				.getSecond();
		String content = vendorProfileCertificateNotificationInfo.stream()
				.filter(p -> p.getFirst().equals(NOTIFICATION_CONTENT_LABEL))
				.findFirst()
				.get()
				.getSecond();

		Notification notification = new Notification(WebNotificationTypeEnum.VENDOR_PROFILE_CERTIFICATE_APPROVAL.getCode(),
				subject, content, sender);

		notification = notificationRepository.save(notification);

		List<NotificationRecipient> recipientList = new ArrayList<>();
		for (IdsmedAccount acc : recipients) {
			recipientList.add(new NotificationRecipient(acc, notification));
		}

		recipientList = recipientRepository.saveAll(recipientList);
		notification.setRecipients(recipientList);
		return notification;
	}

	@Override
	public void sendPendingApproveVendorUserToVendorAdminAndSetStatusToSent(IdsmedAccount sender, List<IdsmedAccount> recipients, Pair<String, String> notificationPair) {
		if (CollectionUtils.isEmpty(recipients)) {
			logger.error("Can't send Pending Approve Vendor User To Vendor Admin Notification - The recipient list is empty");
			return;
		}
		Notification notification = createPendingApproveVendorUserToVendorAdminNotification(sender, recipients, notificationPair);
		try {
			sendNotification(notification);
		} catch (MessagingException e) {
			logger.error("Can't send Pending Approve Vendor User To Vendor Admin Notification", e.getFailedMessage());
			return;
		}

		//Save notification_recipient status to "SENT"
		List<NotificationRecipient> recipientList = notification.getRecipients();
		recipientList.forEach(rp -> {
			rp.setStatus(NotificationRecipient.NotificationRecipientStatus.SENT.getCode());
		});
		recipientRepository.saveAll(recipientList);
	}

	private Notification createPendingApproveVendorUserToVendorAdminNotification(IdsmedAccount sender, List<IdsmedAccount> recipients, Pair<String, String> notificationPair){
		if (CollectionUtils.isEmpty(recipients)) {
			return null;
		}
		List<Pair<String, String>>  PendingApproveVendorUserToVendorAdminNotificationInfo = generateNotificationInfo(notificationPair);
		String subject = PendingApproveVendorUserToVendorAdminNotificationInfo.stream()
				.filter(p -> p.getFirst().equals(NOTIFICATION_SUBJECT_LABEL))
				.findFirst()
				.get()
				.getSecond();
		String content = PendingApproveVendorUserToVendorAdminNotificationInfo.stream()
				.filter(p -> p.getFirst().equals(NOTIFICATION_CONTENT_LABEL))
				.findFirst()
				.get()
				.getSecond();

		Notification notification = new Notification(WebNotificationTypeEnum.PENDING_APPROVED_VENDOR_USER_TO_VENDOR_ADMIN.getCode(),
				subject, content, sender);

		notification = notificationRepository.save(notification);

		List<NotificationRecipient> recipientList = new ArrayList<>();
		for (IdsmedAccount acc : recipients) {
			recipientList.add(new NotificationRecipient(acc, notification));
		}

		recipientList = recipientRepository.saveAll(recipientList);
		notification.setRecipients(recipientList);
		return notification;
	}
	private void loggerForAllMethodInThisService(String methodDescription, String methodName, String loginId){
		logger.info(methodDescription, methodName, loginId != null?loginId:null);
	}
}


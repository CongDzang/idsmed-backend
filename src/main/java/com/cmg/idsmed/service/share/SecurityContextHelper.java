package com.cmg.idsmed.service.share;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;

import com.cmg.idsmed.model.entity.auth.IdsmedAccount;
import com.cmg.idsmed.model.repo.auth.IdsmedAccountRepository;

@Component
public class SecurityContextHelper {

	private static IdsmedAccountRepository accountRepository;

    @Autowired
    public SecurityContextHelper(IdsmedAccountRepository accountRepository) {
    	SecurityContextHelper.accountRepository = accountRepository;
    }

	public static IdsmedAccount getCurrentAccount () {
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		String loginId = null;
		if (principal instanceof User) {
		  loginId = ((User)principal).getUsername();
		} else {
			loginId = principal.toString();
		}
		return StringUtils.isEmpty(loginId) ? null : accountRepository.findByLoginId(loginId);
	}

	public static Long getCurrentAccountId() {
		return getCurrentAccount() != null ?  getCurrentAccount().getId() : null;
	}
}

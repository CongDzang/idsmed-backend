package com.cmg.idsmed.service.share;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.Size;

import com.cmg.idsmed.dto.thirdparty.CustomerRequest;
import com.cmg.idsmed.model.entity.thirdparty.Customer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cmg.idsmed.model.entity.web.service.log.WebServiceLog;
import com.cmg.idsmed.model.repo.web.service.log.WebServiceLogRepository;

@Service
public class WebServiceLogService {
	private static final Logger logger = LoggerFactory.getLogger(WebServiceLogService.class); 
	@Autowired
	private WebServiceLogRepository webServiceLogRepository;

	
	public void StoreInWebServiceLogDatabase(HttpServletRequest httpServletRequest, String methodName, Integer status) {
		String ipAddress = httpServletRequest.getRemoteAddr();
		String queryString = httpServletRequest.getQueryString();
		WebServiceLog webServiceLog = new WebServiceLog(ipAddress, methodName, queryString, status);
		webServiceLogRepository.save(webServiceLog);
	}

	public void StoreInWebServiceLogDatabase(HttpServletRequest httpServletRequest, CustomerRequest customerRequest, String methodName, Integer status){
		List<String> parameters = new ArrayList<>();
		String ipAddress = httpServletRequest.getRemoteAddr();
		parameters.add(customerRequest.getCustomerId() != null ? customerRequest.getCustomerId().toString() : null);
		parameters.add(customerRequest.getMobile());
		parameters.add(customerRequest.getNickName());
		parameters.add(customerRequest.getRealName());
		parameters.add(customerRequest.getSex() != null ? customerRequest.getSex().toString() : null);
		parameters.add(customerRequest.getBirthDate());
		parameters.add(customerRequest.getEmail());
		parameters.add(customerRequest.getHeadPic());
		parameters.add(customerRequest.getIdCardNo());
		parameters.add(customerRequest.getCompanyName());
		parameters.add(customerRequest.getCompanyType() != null ? customerRequest.getCompanyType().toString() : null);
		parameters.add(customerRequest.getContactName());
		parameters.add(customerRequest.getContactTel());
		parameters.add(customerRequest.getTelephone());
		parameters.add(customerRequest.getProvinceCode());
		parameters.add(customerRequest.getCityCode());
		parameters.add(customerRequest.getDistrictCode());
		parameters.add(customerRequest.getProvinceName());
		parameters.add(customerRequest.getCityName());
		parameters.add(customerRequest.getDistrictName());
		parameters.add(customerRequest.getStatus() != null ? customerRequest.getStatus().toString() : null);
		parameters.add(customerRequest.getAddress());
		String parameter = String.join(",", parameters);
		WebServiceLog webServiceLog = new WebServiceLog(ipAddress, methodName, parameter, status);
		webServiceLogRepository.save(webServiceLog);
	}
}
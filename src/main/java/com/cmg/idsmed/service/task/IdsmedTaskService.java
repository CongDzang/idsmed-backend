package com.cmg.idsmed.service.task;

import com.cmg.idsmed.common.exception.IdsmedBaseException;
import com.cmg.idsmed.dto.task.IdsmedTaskListResponse;
import com.cmg.idsmed.model.entity.auth.IdsmedUser;
import com.cmg.idsmed.model.entity.product.Product;
import com.cmg.idsmed.model.entity.vendor.Vendor;

public interface IdsmedTaskService {
	IdsmedTaskListResponse getTasksByLoginId(String loginId, Integer pageIndex, Integer pageSize, String langCode) throws IdsmedBaseException;

	void createVendorReviewTask(Vendor vendor) throws IdsmedBaseException;

	void setCompletedVendorReviewTask(Vendor vendor, String loginId) throws IdsmedBaseException;

	void createVendorApproveTask(Vendor vendor) throws IdsmedBaseException;

	void setCompletedVendorApproveTask(Vendor vendor, String loginId) throws IdsmedBaseException;

	void createProductReviewTask(Product product) throws IdsmedBaseException;

	void setCompletedProductReviewTask(Product product, String loginId) throws IdsmedBaseException;

	void createProductApproveTask(Product product) throws IdsmedBaseException;

	void setCompletedProductApproveTask(Product product, String loginId) throws IdsmedBaseException;

	void createUserApproveTask(IdsmedUser user) throws IdsmedBaseException;

	void setCompletedUserApproveTask(IdsmedUser user, String loginId) throws IdsmedBaseException;
}

package com.cmg.idsmed.service.task;

import com.cmg.idsmed.common.Constant.IdsmedPermissionCodeConst;
import com.cmg.idsmed.common.Constant.IdsmedRoleCodeConst;
import com.cmg.idsmed.common.Constant.LoggerConstant;
import com.cmg.idsmed.common.Constant.PropertiesLabelConst;
import com.cmg.idsmed.common.enums.RoleTypeEnum;
import com.cmg.idsmed.common.enums.TaskTypeEnum;
import com.cmg.idsmed.common.exception.IdsmedBaseException;
import com.cmg.idsmed.dto.task.IdsmedTaskListResponse;
import com.cmg.idsmed.dto.task.IdsmedTaskResponse;
import com.cmg.idsmed.model.entity.auth.IdsmedAccount;
import com.cmg.idsmed.model.entity.auth.IdsmedRole;
import com.cmg.idsmed.model.entity.auth.IdsmedUser;
import com.cmg.idsmed.model.entity.product.Product;
import com.cmg.idsmed.model.entity.task.IdsmedTask;
import com.cmg.idsmed.model.entity.task.IdsmedTaskExecutor;
import com.cmg.idsmed.model.entity.vendor.Vendor;
import com.cmg.idsmed.model.repo.auth.IdsmedAccountRepository;
import com.cmg.idsmed.model.repo.auth.IdsmedRoleRepository;
import com.cmg.idsmed.model.repo.task.IdsmedTaskExecutorRepository;
import com.cmg.idsmed.model.repo.task.IdsmedTaskRepository;
import com.cmg.idsmed.model.repo.user.IdsmedUserRepository;
import com.cmg.idsmed.service.share.MessageResourceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Configuration
@Service
public class IdsmedTaskServiceImpl implements IdsmedTaskService{

	@Autowired
	private IdsmedTaskRepository taskRepository;

	@Autowired
	private IdsmedAccountRepository idsmedAccountRepository;

	@Autowired
	private IdsmedUserRepository idsmedUserRepository;

	@Autowired
	private IdsmedRoleRepository idsmedRoleRepository;

	@Autowired
	private MessageResourceService messageResourceService;

	@Autowired
	private Environment env;

	@Autowired
	private IdsmedTaskExecutorRepository taskExecutorRepository;

	private static final Logger logger = LoggerFactory.getLogger(IdsmedTaskServiceImpl.class);

	@Value("${logger.start.method.description}")
	String START_METHOD_DESCRIPTION;

	@Value("${logger.end.method.description}")
	String END_METHOD_DESCRIPTION;
	private static final String GET_TASK_BY_LOGIN_ID_METHOD_NAME = "getTasksByLoginId";

	@Override
	public IdsmedTaskListResponse getTasksByLoginId(String loginId, Integer pageIndex, Integer pageSize, String langCode) throws IdsmedBaseException {
		loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, GET_TASK_BY_LOGIN_ID_METHOD_NAME, loginId);
		IdsmedAccount account = idsmedAccountRepository.findByLoginId(loginId);
		IdsmedTaskListResponse response = null;
		if (account == null) {
			logger.error("Can't find account with loginId: {}", loginId);
			return new IdsmedTaskListResponse(pageIndex, pageSize);
		}

		Pair<Integer, List<IdsmedTask>> pair = taskRepository.findTasksByLoginIdAnAndStatus(account, IdsmedTask.TaskStatusEnum.INCOMPLETED.getCode(), pageIndex, pageSize);
		List<IdsmedTaskResponse> taskResponses = pair.getSecond().stream().map(t -> new IdsmedTaskResponse(t)).collect(Collectors.toList());
		if (pageIndex != null && pageSize != null) {
			response = new IdsmedTaskListResponse(pair.getFirst(), pageIndex, pageSize, taskResponses);
		} else {
			response = new IdsmedTaskListResponse(pair.getFirst(), taskResponses);
		}
		loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, GET_TASK_BY_LOGIN_ID_METHOD_NAME, loginId);
		return response;
	}

	@Override
	public void createVendorApproveTask(Vendor vendor) throws IdsmedBaseException {
		List<IdsmedAccount> executors = idsmedAccountRepository.findAllAccountByPermissionCode(IdsmedPermissionCodeConst.VENDOR_APPROVE);
		Long vendorId = vendor.getId();

		if (vendorId == null) {
			logger.error("Vendor has no id with name: {}", vendor.getCompanyNamePrimary());
			return;
		}

		String approveLink = generateActionLink(PropertiesLabelConst.URL_SUB_VENDOR_APPROVE, vendorId);

		String description = "Vendor '" + vendor.getCompanyNamePrimary() + "' is pending for Approval";
		IdsmedTask approveTask = new IdsmedTask(approveLink, UUID.randomUUID().toString(), TaskTypeEnum.VENDOR_APPROVE.getCode(), vendorId, description);
		approveTask.setStatus(IdsmedTask.TaskStatusEnum.INCOMPLETED.getCode());
		approveTask = taskRepository.save(approveTask);

		if (!CollectionUtils.isEmpty(executors)) {
			List<IdsmedTaskExecutor> taskExecutors = createTaskExecutor(executors, approveTask);

			if (!CollectionUtils.isEmpty(taskExecutors)) {
				taskExecutorRepository.saveAll(taskExecutors);
			}
		}
	}

	private List<IdsmedTaskExecutor> createTaskExecutor(List<IdsmedAccount> executors, IdsmedTask task) {
		List<IdsmedTaskExecutor> taskExecutors = new ArrayList<>();
		for (IdsmedAccount executor : executors) {
			IdsmedTaskExecutor taskExecutor = new IdsmedTaskExecutor();
			taskExecutor.setIdsmedTask(task);
			taskExecutor.setIdsmedAccount(executor);
			taskExecutors.add(taskExecutor);
		}

		return taskExecutors;
	}

	private String generateActionLink(String subUrlLabel, Long entityId) {
		String domain = env.getProperty(PropertiesLabelConst.URL_FRONTEND_DOMAIN);
		String subUrl = env.getProperty(subUrlLabel);
		String actionLink = domain + subUrl + "?id=" + entityId;
		return actionLink;
	}

	@Override
	public void createVendorReviewTask(Vendor vendor) throws IdsmedBaseException {
		List<IdsmedAccount> executors = idsmedAccountRepository.findAllAccountByPermissionCode(IdsmedPermissionCodeConst.VENDOR_REVIEW);
		Long vendorId = vendor.getId();
		if (vendorId == null) {
			logger.error("Vendor has no id with name: {}", vendor.getCompanyNamePrimary());
			return;
		}

		String reviewLink = generateActionLink(PropertiesLabelConst.URL_SUB_URL_VENDOR_REVIEW, vendorId);

		String description = "Vendor '" + vendor.getCompanyNamePrimary() + "' is pending for Review";
		IdsmedTask reviewTask = new IdsmedTask(reviewLink,  UUID.randomUUID().toString()
				, TaskTypeEnum.VENDOR_REVIEW.getCode(), vendorId, description);
		reviewTask.setStatus(IdsmedTask.TaskStatusEnum.INCOMPLETED.getCode());
		reviewTask = taskRepository.save(reviewTask);

		if (!CollectionUtils.isEmpty(executors)) {
			List<IdsmedTaskExecutor> taskExecutors = createTaskExecutor(executors, reviewTask);

			if (!CollectionUtils.isEmpty(taskExecutors)) {
				taskExecutorRepository.saveAll(taskExecutors);
			}
		}
	}

	@Override
	public void setCompletedVendorReviewTask(Vendor vendor, String loginId) throws IdsmedBaseException {
		setCompletedTask(vendor, loginId, TaskTypeEnum.VENDOR_REVIEW.getCode());
	}

	@Override
	public void setCompletedVendorApproveTask(Vendor vendor, String loginId) throws IdsmedBaseException {
		setCompletedTask(vendor, loginId, TaskTypeEnum.VENDOR_APPROVE.getCode());
	}

	private void setCompletedTask(Vendor vendor, String loginId, Integer taskTypeCode) throws IdsmedBaseException {
		Long vendorId = vendor.getId();
		List<IdsmedTask> tasks = taskRepository.findByEntityIdAndTaskTypeCodeAndStatus(vendorId
				, taskTypeCode, IdsmedTask.TaskStatusEnum.INCOMPLETED.getCode());
		saveTaskListIntoIdsmedTaskRepository(tasks, loginId);
	}

	private void saveTaskListIntoIdsmedTaskRepository(List<IdsmedTask> tasks, String loginId){
		List<IdsmedTask> taskList = new ArrayList<>();
		if (!CollectionUtils.isEmpty(tasks)) {
			for(IdsmedTask task : tasks) {
				task.setStatus(IdsmedTask.TaskStatusEnum.COMPLETED.getCode());
				task.setDoneBy(loginId);
				taskList.add(task);
			}
			taskRepository.saveAll(taskList);
		}
	}

	@Override
	public void createProductReviewTask(Product product) throws IdsmedBaseException {
		IdsmedRole idsmedRole = idsmedRoleRepository.findByCode(IdsmedRoleCodeConst.VENDOR_SENIOR);
		List<IdsmedUser> idsmedUsers = idsmedUserRepository.findVendorSeniorByCompanyCodeAndRole(product.getVendor().getCompanyCode(), idsmedRole);
		List<IdsmedAccount> executors = null;
		for(IdsmedUser idsmedUser : idsmedUsers){
			executors = idsmedAccountRepository.findAllAccountByPermissionCode(IdsmedPermissionCodeConst.PRODUCT_REVIEW);
		}
		Long productId = product.getId();
		if (productId == null) {
			logger.error("Product has no id with name: {}", product.getProductNamePrimary());
			return;
		}

		String reviewLink = generateActionLink(PropertiesLabelConst.URL_SUB_URL_PRODUCT_REVIEW, productId);
		String description = "Product '" + product.getProductNamePrimary() + "' is pending for Review";
		IdsmedTask reviewTask = new IdsmedTask(reviewLink,  UUID.randomUUID().toString()
				, TaskTypeEnum.PRODUCT_REVIEW.getCode(), productId, description);
		reviewTask.setStatus(IdsmedTask.TaskStatusEnum.INCOMPLETED.getCode());
		reviewTask = taskRepository.save(reviewTask);

		if (!CollectionUtils.isEmpty(executors)) {
			List<IdsmedTaskExecutor> taskExecutors = createTaskExecutor(executors, reviewTask);

			if (!CollectionUtils.isEmpty(taskExecutors)) {
				taskExecutorRepository.saveAll(taskExecutors);
			}
		}
	}

	@Override
	public void setCompletedProductReviewTask(Product product, String loginId) throws IdsmedBaseException {
		setCompletedTask(product, loginId, TaskTypeEnum.PRODUCT_REVIEW.getCode());
	}

	private void setCompletedTask(Product product, String loginId, Integer taskTypeCode) throws IdsmedBaseException {
		Long productId = product.getId();
		List<IdsmedTask> tasks = taskRepository.findByEntityIdAndTaskTypeCodeAndStatus(productId
				, taskTypeCode, IdsmedTask.TaskStatusEnum.INCOMPLETED.getCode());
		saveTaskListIntoIdsmedTaskRepository(tasks, loginId);
	}

	@Override
	public void createProductApproveTask(Product product) throws IdsmedBaseException {
		// Find role by passing vendor senior code and after that find vendor senior by get the companyCode from vendor get by product relationship with vendor.
		IdsmedRole idsmedRole = idsmedRoleRepository.findByCode(IdsmedRoleCodeConst.VENDOR_SENIOR);
		List<IdsmedUser> idsmedUsers = idsmedUserRepository.findVendorSeniorByCompanyCodeAndRole(product.getVendor().getCompanyCode(), idsmedRole);
		List<IdsmedAccount> executors = null;
		for(IdsmedUser idsmedUser : idsmedUsers){
			 executors = idsmedUser.getIdsmedAccounts();
		}

		Long productId = product.getId();

		if (productId == null) {
			logger.error("Product has no id with name: {}", product.getProductNamePrimary());
			return;
		}

		String approveLink = generateActionLink(PropertiesLabelConst.URL_SUB_PRODUCT_APPROVE, productId);
		String description = "Product '" + product.getProductNamePrimary() + "' is pending for Approval";
		IdsmedTask approveTask = new IdsmedTask(approveLink, UUID.randomUUID().toString()
				, TaskTypeEnum.PRODUCT_APPROVE.getCode(), productId, description);
		approveTask.setStatus(IdsmedTask.TaskStatusEnum.INCOMPLETED.getCode());
		approveTask = taskRepository.save(approveTask);

		if (!CollectionUtils.isEmpty(executors)) {
			List<IdsmedTaskExecutor> taskExecutors = createTaskExecutor(executors, approveTask);

			if (!CollectionUtils.isEmpty(taskExecutors)) {
				taskExecutorRepository.saveAll(taskExecutors);
			}
		}
	}

	@Override
	public void setCompletedProductApproveTask(Product product, String loginId) throws IdsmedBaseException {
		setCompletedTask(product, loginId, TaskTypeEnum.PRODUCT_APPROVE.getCode());
	}

	@Override
	public void createUserApproveTask(IdsmedUser user) throws IdsmedBaseException {
		// 1. for scpUserApproved, we need to find all account which have user approve permission
		// 2. then after that find all the user related to this account which have not company code and then after that add into account list
		Long userId = user.getId();
		if(StringUtils.isEmpty(user.getCompanyCode())) {
			List<IdsmedAccount> executors = idsmedAccountRepository.findAllAccountByPermissionCode(IdsmedPermissionCodeConst.USER_APPROVE);
			List<IdsmedAccount> scpUserApprovers = new ArrayList<>();
			for (IdsmedAccount executor : executors) {
				IdsmedUser idsmedUser = executor.getIdsmedUser();
				if (StringUtils.isEmpty(idsmedUser.getCompanyCode())) {
					scpUserApprovers.addAll(idsmedUser.getIdsmedAccounts());
				}
			}

			if (userId == null) {
				logger.error("User has no id with name: {}", user.getFirstName() + " " + user.getLastName());
				return;
			}

			IdsmedTask approveTask = logIntoIdsmedTask(user, userId);

			if (!CollectionUtils.isEmpty(scpUserApprovers)) {
				List<IdsmedTaskExecutor> taskExecutors = createTaskExecutor(scpUserApprovers, approveTask);

				if (!CollectionUtils.isEmpty(taskExecutors)) {
					taskExecutorRepository.saveAll(taskExecutors);
				}
			}
		}

		// for vendor senior, just get the company code with current user and check the vendor senior which is same company code with current user to make sure the vendor senior is same company with current user.
		IdsmedTask approveTask = logIntoIdsmedTask(user, userId);
		List<IdsmedAccount> vendorSeniorAccounts = generateVendorSeniorAccountList(user);
		if(!CollectionUtils.isEmpty(vendorSeniorAccounts)){
			List<IdsmedTaskExecutor> taskExecutors = createTaskExecutor(vendorSeniorAccounts, approveTask);

			if(!CollectionUtils.isEmpty(taskExecutors)){
				taskExecutorRepository.saveAll(taskExecutors);
			}
		}
	}

	private IdsmedTask logIntoIdsmedTask(IdsmedUser user, Long userId){
		String approveLink = generateActionLink(PropertiesLabelConst.URL_SUB_USER_APPROVE, userId);
		String description = "User '" + user.getFirstName() + "' is pending for Approval";
		IdsmedTask approveTask = new IdsmedTask(approveLink, UUID.randomUUID().toString(), TaskTypeEnum.USER_APPROVE.getCode(), userId, description);
		approveTask.setStatus(IdsmedTask.TaskStatusEnum.INCOMPLETED.getCode());
		approveTask = taskRepository.save(approveTask);
		return approveTask;
	}
	private List<IdsmedAccount> generateVendorSeniorAccountList(IdsmedUser idsmedUser){
		IdsmedRole idsmedRole = idsmedRoleRepository.findByCode(IdsmedRoleCodeConst.VENDOR_SENIOR);
		List<IdsmedUser> vendorSeniors = idsmedUserRepository.findVendorSeniorByCompanyCodeAndRole(idsmedUser.getCompanyCode(), idsmedRole);
		List<IdsmedAccount> vendorSeniorAccounts = new ArrayList<>();
		for(IdsmedUser vendorSenior : vendorSeniors){
			vendorSeniorAccounts.addAll(vendorSenior.getIdsmedAccounts());
		}
		return vendorSeniorAccounts;
	}
	@Override
	public void setCompletedUserApproveTask(IdsmedUser user, String loginId) throws IdsmedBaseException {
		setCompletedTask(user, loginId, TaskTypeEnum.USER_APPROVE.getCode());
	}

	private void setCompletedTask(IdsmedUser user, String loginId, Integer taskTypeCode) throws IdsmedBaseException {
		Long userId = user.getId();
		List<IdsmedTask> tasks = taskRepository.findByEntityIdAndTaskTypeCodeAndStatus(userId
				, taskTypeCode, IdsmedTask.TaskStatusEnum.INCOMPLETED.getCode());
		saveTaskListIntoIdsmedTaskRepository(tasks, loginId);
	}
	private void loggerForAllMethodInThisService(String methodDescription, String methodName, String loginId){
		logger.info(methodDescription, methodName, loginId != null?loginId:null);
	}
}

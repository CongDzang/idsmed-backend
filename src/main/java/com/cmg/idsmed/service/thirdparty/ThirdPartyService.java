package com.cmg.idsmed.service.thirdparty;

import com.cmg.idsmed.common.exception.IdsmedBaseException;
import com.cmg.idsmed.dto.thirdparty.CustomerRequest;
import com.cmg.idsmed.dto.thirdparty.CustomerResponse;
import com.cmg.idsmed.model.entity.product.Product;

import java.util.Optional;

public interface ThirdPartyService {
    
    //save into customer table in database
    CustomerResponse saveIntoCustomerTable(CustomerRequest customerRequest) throws IdsmedBaseException;

    //update customer table and set the update value in database
    CustomerResponse updateCustomer(CustomerRequest customerRequest) throws IdsmedBaseException;

}

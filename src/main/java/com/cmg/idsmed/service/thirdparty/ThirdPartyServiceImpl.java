package com.cmg.idsmed.service.thirdparty;

import com.cmg.idsmed.common.Constant.LoggerConstant;
import com.cmg.idsmed.common.enums.HTTPServletReturnStatusEnum;
import com.cmg.idsmed.common.enums.MethodNameEnum;
import com.cmg.idsmed.common.exception.ErrorInfo;
import com.cmg.idsmed.common.exception.IdsmedBaseException;
import com.cmg.idsmed.common.utils.LangHelper;
import com.cmg.idsmed.dto.thirdparty.CustomerRequest;
import com.cmg.idsmed.dto.thirdparty.CustomerResponse;
import com.cmg.idsmed.model.entity.thirdparty.Customer;
import com.cmg.idsmed.model.repo.thirdparty.CustomerRepository;
import com.cmg.idsmed.service.share.MessageResourceService;
import com.cmg.idsmed.service.share.WebServiceLogService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.Optional;

@Configuration
@Service
public class ThirdPartyServiceImpl implements ThirdPartyService {

    private static final Logger logger = LoggerFactory.getLogger(ThirdPartyServiceImpl.class);
    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private MessageResourceService messageResourceService;

    @Autowired
    private WebServiceLogService webServiceLogService;

    @Autowired
    private HttpServletRequest httpServletRequest;

    @Autowired
    private Environment env;

    @Value("${logger.start.method.description}")
    String START_METHOD_DESCRIPTION;

    @Value("${logger.end.method.description}")
    String END_METHOD_DESCRIPTION;
    private static final String SAVE_INTO_CUSTOMER_TABLE_METHOD_NAME = "saveIntoCustomerTable";
    private static final String UPDATE_CUSTOMER_METHOD_NAME = "updateCustomer";

    @Override
    public CustomerResponse saveIntoCustomerTable(CustomerRequest customerRequest) throws IdsmedBaseException{
        loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, SAVE_INTO_CUSTOMER_TABLE_METHOD_NAME, null);

        Customer customer = new Customer(customerRequest);
        customer.setStatus(customerRequest.getStatus());
        customer = customerRepository.save(customer);

        webServiceLogService.StoreInWebServiceLogDatabase(httpServletRequest, customerRequest, MethodNameEnum.CREATECUSTOMER.getName(), HTTPServletReturnStatusEnum.AUTHORIZED.getCode());
        loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, SAVE_INTO_CUSTOMER_TABLE_METHOD_NAME, null);
        return new CustomerResponse(customer);
    }

    @Override
    public CustomerResponse updateCustomer(CustomerRequest customerRequest) throws IdsmedBaseException {
        loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, UPDATE_CUSTOMER_METHOD_NAME, null);
        Customer customer = customerRepository.findByCustomerId(customerRequest.getCustomerId());
        if(customer == null){
            throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.CUSTOMER_NOT_FOUND_ERROR, LangHelper.getLangCode(null)));
        }

        BeanUtils.copyProperties(customerRequest, customer, "id");
        customer.setStatus(customerRequest.getStatus());
        customer = customerRepository.save(customer);

        webServiceLogService.StoreInWebServiceLogDatabase(httpServletRequest, customerRequest,  MethodNameEnum.UPDATECUSTOMER.getName(), HTTPServletReturnStatusEnum.AUTHORIZED.getCode());
        loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, UPDATE_CUSTOMER_METHOD_NAME, null);
        return new CustomerResponse(customer);
    }
    private void loggerForAllMethodInThisService(String methodDescription, String methodName, String loginId){
        logger.info(methodDescription, methodName, loginId != null?loginId:null);
    }
}

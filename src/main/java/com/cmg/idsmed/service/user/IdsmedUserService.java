package com.cmg.idsmed.service.user;

import com.cmg.idsmed.common.enums.IdsmedAccountTypeEnum;
import com.cmg.idsmed.common.enums.StatusEnum;
import com.cmg.idsmed.common.exception.IdsmedBaseException;
import com.cmg.idsmed.dto.account.IdsmedAccountResponse;
import com.cmg.idsmed.dto.account.IdsmedAccountSettingsRequest;
import com.cmg.idsmed.dto.account.IdsmedAccountSettingsResponse;
import com.cmg.idsmed.dto.product.SimpleProductListResponse;
import com.cmg.idsmed.dto.user.*;
import com.cmg.idsmed.model.entity.auth.IdsmedAccount;
import com.cmg.idsmed.model.entity.auth.IdsmedUser;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface IdsmedUserService {
	IdsmedUserListResponse getUserList(String loginId,String loginIdKeyword, String firstname, String lastname, Integer status, Integer pageIndex, Integer pageSize, String langCode) throws IdsmedBaseException;
    IdsmedUserResponse getUserById(long id) throws IdsmedBaseException;
    IdsmedUserResponse create(IdsmedUserRequest request, MultipartFile profileImage ) throws IdsmedBaseException;
    IdsmedUserResponse approve(IdsmedUserRequest request) throws IdsmedBaseException;
    IdsmedUserResponse reject(IdsmedUserRequest request) throws IdsmedBaseException;
    IdsmedUserResponse edit(IdsmedUserRequest request, MultipartFile profileImage) throws IdsmedBaseException;
    IdsmedAccountSettingsResponse editAccountSettings(IdsmedAccountSettingsRequest request) throws IdsmedBaseException;
    IdsmedUserResponse updateUserRole(UserUpdateRoleSimpleRequest request) throws IdsmedBaseException;
    List<IdsmedUser> getAccountListOfSCPAdmin(String recipientEmail) throws IdsmedBaseException;
    List<IdsmedAccountResponse> getVendorCoordinatorList() throws IdsmedBaseException;
    IdsmedUserResponse inactiveUserStatus(UserStatusRequest request) throws IdsmedBaseException;
    IdsmedUserResponse resumeInactiveUserStatus(UserStatusRequest request) throws IdsmedBaseException;
    IdsmedUserResponse reRegisterUserById(Long id, String langCode) throws IdsmedBaseException;
    IdsmedUser uploadAndSetImagesForHospitalAndCorporateUser(IdsmedUser user, MultipartFile profileImage, MultipartFile frontIdImage, MultipartFile backIdImage, String langCode) throws IdsmedBaseException;

    IdsmedAccount generateProperAccount(IdsmedUser user
            , String loginId
            , String password
            , IdsmedAccountTypeEnum accountType
            , StatusEnum status);

    Boolean checkValidNewLoginId(String loginId);

    IdsmedHospitalUserResponse getHospitalUserById(Long id, String LangCode) throws IdsmedBaseException;
    IdsmedCorporateUserResponse getCorporateUserById(Long id, String langCode) throws IdsmedBaseException;
    Boolean individualUserRegister(IdsmedIndividualUserRequest request
            , MultipartFile profileImage, MultipartFile frontIdImage, MultipartFile backIdImage) throws IdsmedBaseException;

    Boolean activateIndividualUser(String uuid, String langCode) throws IdsmedBaseException;

    IdsmedIndividualUserResponse getIndividualUserById(Long id, String langCode) throws IdsmedBaseException;

    IdsmedIndividualUserResponse editIndividuallUser(IdsmedIndividualUserRequest request, MultipartFile profileImage, MultipartFile frontIdImage, MultipartFile backIdImage) throws IdsmedBaseException;
}

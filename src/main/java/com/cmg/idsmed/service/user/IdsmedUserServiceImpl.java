package com.cmg.idsmed.service.user;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import com.cmg.idsmed.common.Constant.IdsmedPermissionCodeConst;
import com.cmg.idsmed.common.Constant.IdsmedRoleCodeConst;
import com.cmg.idsmed.common.Constant.LoggerConstant;
import com.cmg.idsmed.common.Constant.PropertiesLabelConst;
import com.cmg.idsmed.common.enums.*;
import com.cmg.idsmed.common.utils.DateTimeHelper;
import com.cmg.idsmed.common.utils.LangHelper;
import com.cmg.idsmed.common.utils.StringProcessUtils;
import com.cmg.idsmed.dto.account.IdsmedAccountResponse;
import com.cmg.idsmed.dto.corporate.CorporateDepartmentResponse;
import com.cmg.idsmed.dto.corporate.CorporateResponse;
import com.cmg.idsmed.dto.hospital.*;
import com.cmg.idsmed.dto.user.*;
import com.cmg.idsmed.mail.*;
import com.cmg.idsmed.mail.SCPUserRegistrationApprovedEmailObject;
import com.cmg.idsmed.model.entity.auth.*;
import com.cmg.idsmed.model.entity.corporate.Corporate;
import com.cmg.idsmed.model.entity.corporate.CorporateDepartment;
import com.cmg.idsmed.model.entity.hospital.Hospital;
import com.cmg.idsmed.model.entity.hospital.HospitalDepartment;
import com.cmg.idsmed.model.entity.masterdata.Country;
import com.cmg.idsmed.model.entity.masterdata.Province;
import com.cmg.idsmed.model.entity.notification.AccountNotificationSetting;
import com.cmg.idsmed.model.entity.notification.NotificationRecipient;
import com.cmg.idsmed.model.entity.task.IdsmedTaskExecutor;
import com.cmg.idsmed.model.entity.vendor.TenderCategory;
import com.cmg.idsmed.model.repo.common.NotificationRecipientRepository;
import com.cmg.idsmed.model.repo.masterdata.AccountNotificationSettingRepository;
import com.cmg.idsmed.model.repo.masterdata.CountryRepository;
import com.cmg.idsmed.model.repo.masterdata.ProvinceRepository;
import com.cmg.idsmed.model.repo.masterdata.TenderCategoryRepository;
import com.cmg.idsmed.model.repo.task.IdsmedTaskExecutorRepository;
import com.cmg.idsmed.model.repo.user.UserAddressRepository;
import com.cmg.idsmed.service.auth.AuthenticationService;
import com.cmg.idsmed.service.email.EmailNotificationConfigService;
import com.cmg.idsmed.service.task.IdsmedTaskService;
import com.google.j2objc.annotations.AutoreleasePool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.util.Pair;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.cmg.idsmed.common.exception.ErrorInfo;
import com.cmg.idsmed.common.exception.IdsmedBaseException;
import com.cmg.idsmed.config.rabbitmq.IdsmedQueueConfig;
import com.cmg.idsmed.dto.account.IdsmedAccountSettingsRequest;
import com.cmg.idsmed.dto.account.IdsmedAccountSettingsResponse;
import com.cmg.idsmed.model.entity.vendor.Vendor;
import com.cmg.idsmed.model.repo.account.IdsmedAccountSettingsRepository;
import com.cmg.idsmed.model.repo.auth.IdsmedAccountRepository;
import com.cmg.idsmed.model.repo.auth.IdsmedRoleRepository;
import com.cmg.idsmed.model.repo.auth.IdsmedUserRoleRepository;
import com.cmg.idsmed.model.repo.user.IdsmedUserRepository;
import com.cmg.idsmed.model.repo.vendor.VendorRepository;
import com.cmg.idsmed.service.share.FileUploadService;
import com.cmg.idsmed.service.share.FileUploadServiceImpl;
import com.cmg.idsmed.service.share.MessageResourceService;
import com.cmg.idsmed.service.share.NotificationService;

@Configuration
@Service
public class IdsmedUserServiceImpl implements IdsmedUserService {
    private static final Logger logger = LoggerFactory.getLogger(IdsmedUserServiceImpl.class);
    @Autowired
    private IdsmedUserRepository idsmedUserRepository;

    @Autowired
    private IdsmedRoleRepository idsmedRoleRepository;

    @Autowired
    private IdsmedAccountRepository idsmedAccountRepository;

    @Autowired
    private IdsmedAccountSettingsRepository idsmedAccountSettingsRepository;

    @Autowired
    private EmailService emailService;

    @Autowired
    private IdsmedUserRoleRepository idsmedUserRoleRepository;

    @Autowired
    private VendorRepository vendorRepository;

    @Autowired
    private AccountNotificationSettingRepository accountNotificationSettingRepository;

    @Autowired
    private TenderCategoryRepository tenderCategoryRepository;

    @Autowired
    private MessageResourceService messageResourceService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private FileUploadService fileUploadService;

    @Autowired
    private Environment env;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private NotificationService notificationService;

    @Autowired
    private IdsmedTaskService taskService;

    @Autowired
    private EmailNotificationConfigService emailNotificationConfigService;

    @Autowired
    private AuthenticationService authenticationService;

    @Autowired
    private IdsmedTaskExecutorRepository idsmedTaskExecutorRepository;

    @Autowired
    private NotificationRecipientRepository notificationRecipientRepository;

    @Autowired
	private CountryRepository countryRepository;

    @Autowired
	private ProvinceRepository provinceRepository;

    @Autowired
	private UserAddressRepository userAddressRepository;

    private final static String SENIOR_VENDOR_ADMIN_ROLE_CODE = "Ven002";
    private final static String JUNIOR_VENDOR_ADMIN_ROLE_CODE = "Ven001";

    private final static String USER_RE_REGISTER_BASE_LINK_LABEL = "mail.user.register.resubmit.base.link";
    private static final Integer USER_RE_REGISTER_LINK_VALID_PERIOD = 7;

    @Value("${logger.start.method.description}")
    String START_METHOD_DESCRIPTION;

    @Value("${logger.end.method.description}")
    String END_METHOD_DESCRIPTION;
    private static final String GET_USER_LIST_METHOD_NAME = "getUserList";
    private static final String GET_USER_BY_ID_METHOD_NAME = "getUserById";
    private static final String CREATE_IDSMED_USER_METHOD_NAME = "createIdsmedUser";
    private static final String APPROVED_IDSMED_USER_METHOD_NAME = "approveIdsmedUser";
    private static final String REJECT_IDSMED_USER_METHOD_NAME = "rejectIdsmedUser";
    private static final String UPDATE_USER_ROLE_METHOD_NAME = "updateUserRole";
    private static final String EDIT_IDSMED_USER_METHOD_NAME = "editIdsmedUser";
    private static final String GET_VENDOR_COORDINATOR_LIST_METHOD_NAME = "getVendorCoordinatorList";
    private static final String INACTIVE_USER_STATUS_METHOD_NAME = "inactiveUserStatus";
    private static final String RESUME_INACTIVE_USER_STATUS_METHOD_NAME = "resumeInactiveUserStatus";
    private static final String REREGISTER_USER_BY_ID_METHOD_NAME = "reRegisterUserById";
    private static final String EDIT_ACCOUNT_SETTING_METHOD_NAME = "editAccountSettings";

    @Override
    @PreAuthorize("@PermissionService.isMethodPermited(authentication, {'UA0000001'})")
    public IdsmedUserListResponse getUserList(String loginId, String loginIdKeyword, String firstname, String lastname, Integer status, Integer pageIndex, Integer pageSize, String langCode) throws IdsmedBaseException {

        loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, GET_USER_LIST_METHOD_NAME, null);
        IdsmedUserListResponse userListResponse = new IdsmedUserListResponse(pageIndex, pageSize);

        IdsmedUser user = idsmedUserRepository.findUserByLoginIdAndStatus(loginId, StatusEnum.APPROVED.getCode());
        if (user == null) {
            throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.USER_NOT_FOUND_ERROR, LangHelper.getLangCode(langCode)));
        }
        List<IdsmedRole> roles = idsmedRoleRepository.findAllRoleByUserAndStatus(user, StatusEnum.APPROVED.getCode());

        Pair<Integer, List<IdsmedUser>> idsmedUsers = null;

        Optional<IdsmedRole> seniorVendorAdminRoleOpt = roles.stream().filter(r -> r.getCode().equals(SENIOR_VENDOR_ADMIN_ROLE_CODE)).findFirst();
        Optional<IdsmedRole> juniorVendorAdminRoleOpt = roles.stream().filter(r -> r.getCode().equals(JUNIOR_VENDOR_ADMIN_ROLE_CODE)).findFirst();

        if (seniorVendorAdminRoleOpt.isPresent() || juniorVendorAdminRoleOpt.isPresent()) {
            idsmedUsers = idsmedUserRepository.searchUserByCustomCondition(loginIdKeyword
                    , firstname, lastname, user.getCompanyCode(), status, pageIndex, pageSize);
        } else {
            idsmedUsers = idsmedUserRepository.searchUserByCustomCondition(loginIdKeyword
                    , firstname, lastname, null, status, pageIndex, pageSize);
        }

        if (CollectionUtils.isEmpty(idsmedUsers.getSecond())) {
            return userListResponse;
        }
        int userCount = idsmedUsers.getFirst();

        if (userCount == 0) {
            return new IdsmedUserListResponse(userCount, new ArrayList<IdsmedUserResponse>());
        }

        List<IdsmedUserResponse> idsmedUserResponseList = generateUserResponseList(idsmedUsers.getSecond());

        if (pageIndex != null && pageSize != null) {
            userListResponse = new IdsmedUserListResponse(userCount, pageIndex, pageSize, idsmedUserResponseList);
        } else {
            userListResponse = new IdsmedUserListResponse(userCount, idsmedUserResponseList);
        }

        loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, GET_USER_LIST_METHOD_NAME, null);
        return userListResponse;
    }

    public List<IdsmedUserResponse> generateUserResponseList(List<IdsmedUser> idsmedUserList) {
        String ossBucketDomain = env.getProperty(PropertiesLabelConst.OSS_BUCKET_DOMAIN);

        List<IdsmedUserResponse> idsmedUserResponseList = idsmedUserList.stream().map(iu -> {
            List<IdsmedRole> rrs = iu.getUserRoles().stream().map(ur -> ur.getIdsmedRole()).collect(Collectors.toList());
            IdsmedUserResponse iur = new IdsmedUserResponse(iu, rrs, ossBucketDomain);
            if (!StringUtils.isEmpty(iu.getCompanyCode())) {
                Vendor vendor = vendorRepository.findFirstByCompanyCode(iu.getCompanyCode());
                if (vendor != null) {
                    iur.setCompanyPrimaryName(vendor.getCompanyNamePrimary());
                }
            }
            return iur;
        }).collect(Collectors.toList());

        return idsmedUserResponseList;
    }

    @Override
//	@PreAuthorize("@PermissionService.isMethodPermited(authentication, {'UA0000001', 'API0000003', 'API0000002'})")
    public IdsmedUserResponse getUserById(long id) throws IdsmedBaseException {
        loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, GET_USER_BY_ID_METHOD_NAME, null);
        Optional<IdsmedUser> idsmedUserOpt = Optional.ofNullable(idsmedUserRepository.getOne(id));
        IdsmedUser idsmedUser = idsmedUserOpt.get();
        String ossBucketDomain = env.getProperty(PropertiesLabelConst.OSS_BUCKET_DOMAIN);

        logger.info("end getting an user");
        if (idsmedUserOpt.isPresent()) {
            IdsmedUserResponse idsmedUserResponse = new IdsmedUserResponse(idsmedUserOpt.get(), idsmedUserOpt.get().getUserRoles().stream().map(iur -> iur.getIdsmedRole()).collect(Collectors.toList()), ossBucketDomain);
            Vendor vendor = vendorRepository.findFirstByCompanyCode(idsmedUserOpt.get().getCompanyCode());
            if (vendor != null) {
                idsmedUserResponse.setCompanyPrimaryName(vendor.getCompanyNamePrimary());
            }

            List<Integer> accountNotificationSettingIdList = generateAccountNotificationSettingId(idsmedUser);
            List<Long> tenderCategoryIdList = generateCareAreaId(idsmedUser);
            Boolean vendorCoordinator = getVendorCoordinator(idsmedUser);
            idsmedUserResponse.setAccountEmailNotificationPreferencesIdList(accountNotificationSettingIdList);
            idsmedUserResponse.setTenderCategoryIdList(tenderCategoryIdList);
            idsmedUserResponse.setVendorCoordinator(vendorCoordinator);
            loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, GET_USER_BY_ID_METHOD_NAME, null);
            return idsmedUserResponse;
        }
        return null;
    }

    public IdsmedHospitalUserResponse getHospitalUserById(Long id, String langCode) throws IdsmedBaseException {
        String ossBucketDomain = env.getProperty(PropertiesLabelConst.OSS_BUCKET_DOMAIN);
        Optional<IdsmedUser> userOpt = idsmedUserRepository.findById(id);
        if (!userOpt.isPresent()) {
            throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.USER_NOT_FOUND_ERROR, LangHelper.getLangCode(langCode)));
        }
        IdsmedUser user = userOpt.get();
        if (!user.getTypeOfUser().equals(UserTypeEnum.HOSPITAL_USER.getCode())) {
            throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.USER_NOT_FOUND_ERROR, LangHelper.getLangCode(langCode)));
        }

        IdsmedHospitalUserResponse response = new IdsmedHospitalUserResponse();
        IdsmedUserResponse idsmedUserResponse = new IdsmedUserResponse(user, user.getUserRoles().stream().map(iur -> iur.getIdsmedRole()).collect(Collectors.toList()), ossBucketDomain);
        if (idsmedUserResponse != null) {
            response.setIdsmedUserResponse(idsmedUserResponse);
        }
        Hospital hospital = user.getHospital();
        if (hospital != null) {
            HospitalResponse hospitalResponse = new HospitalResponse(hospital);
            response.setHospitalResponse(hospitalResponse);
            List<HospitalDepartment> hospitalDepartments = hospital.getHospitalDepartments();
            if (!CollectionUtils.isEmpty(hospitalDepartments)) {
                response.setHospitalDepartmentResponse(hospitalDepartments.stream().map(hd -> new HospitalDepartmentResponse(hd)).collect(Collectors.toList()));
            }
        }

        return response;
    }

    public IdsmedCorporateUserResponse getCorporateUserById(Long id, String langCode) throws IdsmedBaseException {
        String ossBucketDomain = env.getProperty(PropertiesLabelConst.OSS_BUCKET_DOMAIN);
        Optional<IdsmedUser> userOpt = idsmedUserRepository.findById(id);
        if (!userOpt.isPresent()) {
            throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.USER_NOT_FOUND_ERROR, LangHelper.getLangCode(langCode)));
        }
        IdsmedUser user = userOpt.get();
        if (!user.getTypeOfUser().equals(UserTypeEnum.CORPORATE_USER.getCode())) {
            throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.USER_NOT_FOUND_ERROR, LangHelper.getLangCode(langCode)));
        }

        IdsmedCorporateUserResponse response = new IdsmedCorporateUserResponse();
        IdsmedUserResponse idsmedUserResponse = new IdsmedUserResponse(user, user.getUserRoles().stream().map(iur -> iur.getIdsmedRole()).collect(Collectors.toList()), ossBucketDomain);
        if (idsmedUserResponse != null) {
            response.setIdsmedUserResponse(idsmedUserResponse);
        }
        Corporate corporate = user.getCorporate();
        if (corporate != null) {
            CorporateResponse corporateResponse = new CorporateResponse(corporate);
            response.setCorporateResponse(corporateResponse);
            List<CorporateDepartment> corporateDepartments = corporate.getCorporateDepartments();
            if (!CollectionUtils.isEmpty(corporateDepartments)) {
                response.setCorporateDepartmentResponses(corporateDepartments.stream().map(cd -> new CorporateDepartmentResponse(cd)).collect(Collectors.toList()));
            }
        }

        return response;
    }

    private List<Integer> generateAccountNotificationSettingId(IdsmedUser idsmedUser){
        List<Integer> accountNotificationSettingIdList = new ArrayList<>();
        if(!CollectionUtils.isEmpty(idsmedUser.getIdsmedAccounts())){
            for(IdsmedAccount idsmedAccount : idsmedUser.getIdsmedAccounts()){
                if(!CollectionUtils.isEmpty(idsmedAccount.getAccountNotificationSettingList())){
                    for(AccountNotificationSetting accountNotificationSetting : idsmedAccount.getAccountNotificationSettingList()){
                        Integer notificationType = accountNotificationSetting.getNotificationType();
                        accountNotificationSettingIdList.add(notificationType);
                    }
                }
            }
        }
        return accountNotificationSettingIdList;
    }

    private List<Long> generateCareAreaId(IdsmedUser idsmedUser){
        List<Long> tenderCategoryIdList = new ArrayList<>();
        if(!CollectionUtils.isEmpty(idsmedUser.getIdsmedAccounts())){
            for(IdsmedAccount idsmedAccount : idsmedUser.getIdsmedAccounts()){
                if(!CollectionUtils.isEmpty(idsmedAccount.getTenderCategoryList())){
                    for(TenderCategory tenderCategory : idsmedAccount.getTenderCategoryList()){
                        Long tenderCategoryId = tenderCategory.getCareAreaId();
                        tenderCategoryIdList.add(tenderCategoryId);
                    }
                }
            }
        }
        return tenderCategoryIdList;
    }

    private Boolean getVendorCoordinator(IdsmedUser idsmedUser){
        if(!CollectionUtils.isEmpty(idsmedUser.getIdsmedAccounts())){
            for(IdsmedAccount idsmedAccount : idsmedUser.getIdsmedAccounts()){
                return idsmedAccount.getVendorCoordinator();
            }
        }
        return null;
    }
    @Transactional
    @Override
    public IdsmedUserResponse create(IdsmedUserRequest request, MultipartFile profileImage) throws IdsmedBaseException {
        loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, CREATE_IDSMED_USER_METHOD_NAME, request.getUserLoginId());
        if(StringProcessUtils.hasWhiteSpace(request.getUserLoginId())){
            throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.USER_LOGIN_ID_CANNOT_CONTAIN_SPACE, LangHelper.getLangCode(request.getLangCode())));
        }
        IdsmedAccount existingUser = idsmedAccountRepository.findByLoginId(request.getUserLoginId());

        if (existingUser != null) {
            logger.error("The User login Id: {} is existing", request.getUserLoginId());
            throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.USER_LOGIN_ID_EXIST_ERROR, LangHelper.getLangCode(request.getLangCode())));
        }
        IdsmedUserResponse response = null;
        if (StringUtils.isEmpty(request.getCompanyCode())) {
            response = createUserWithoutCompanyCode(request, profileImage);
        } else {
            if (checkMatchEmail(request.getEmail(), request.getCompanyCode(), request.getLangCode())) {
                response = createUserWithCompanyCode(request, profileImage);
            } else {
                response = createUserWithCompanyCodePendingApprove(request, profileImage);
            }
        }
        loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, CREATE_IDSMED_USER_METHOD_NAME, request.getUserLoginId());        return response;
    }

    private Boolean checkMatchEmail(String requestEmail, String companyCode, String langCode) throws IdsmedBaseException {
        Vendor vendor = vendorRepository.findFirstByCompanyCode(companyCode);
        if (vendor == null) {
            throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.VENDOR_NOT_FOUND_ERROR, LangHelper.getLangCode(langCode)));
        }
        if (!requestEmail.equalsIgnoreCase(vendor.getPersonIncharge1Email())
                && !requestEmail.equalsIgnoreCase(vendor.getPersonIncharge2Email())) {
            return false;
        }
        return true;
    }

    @Transactional
    public IdsmedUserResponse createUserWithoutCompanyCode(IdsmedUserRequest request, MultipartFile profileImage) throws IdsmedBaseException {
        IdsmedAccount idsmedAccount = idsmedAccountRepository.findByLoginId(request.getLoginId());
        IdsmedUser user = new IdsmedUser();
        BeanUtils.copyProperties(request, user, "id");
        user.setStatus(request.getStatus());
        user.setTypeOfUser(TypeOfUserEnum.SCP_USER.getCode());
        user.setCreatedBy(idsmedAccount.getId());
        user.setUpdatedBy(idsmedAccount.getId());

        String ossBucketDomain = env.getProperty(PropertiesLabelConst.OSS_BUCKET_DOMAIN);

        //Upload profile image
        if (profileImage != null) {
            user = uploadAndSetProfileImage(user, profileImage, request.getLangCode());
        }

        // When SCP admin create the user, it should be approved automatically as base on QA
        user = autoApprovedSCPUser(user, request);

        user = idsmedUserRepository.save(user);

        IdsmedAccount account = null;
        // if user status is approved, then the user account also have to be autoapproved to let user able to login.
        if(user.getStatus().equals(StatusEnum.APPROVED.getCode())){
            account = autoApprovedSCPUserAccount(user, request.getUserLoginId(), null, IdsmedAccountTypeEnum.IDSMED_USER, request);
        }

        else{
            account = generateProperAccount(user, request.getUserLoginId(), null, IdsmedAccountTypeEnum.IDSMED_USER, StatusEnum.PENDING_FOR_APPROVAL);
            saveAccountIntoDatabase(account, request);
        }

        //Set accountsetting
        IdsmedAccountSettings accSettings = new IdsmedAccountSettings(account, request.getLangCode());
        idsmedAccountSettingsRepository.save(accSettings);
        List<IdsmedAccount> accounts = new ArrayList<>();
        accounts.add(account);
        user.setIdsmedAccounts(accounts);

        // save into account email notification into database
        saveIntoAccountNotificationSettingTable(account, request);

        // this method is temporary comment because currently it is autoapprove when the user is create by scp admin
        //CREATE APPROVE TASK
        // taskService.createUserApproveTask(user);

        // get status by name in enum
        String status = null;
        for (StatusEnum statusEnum : StatusEnum.values()) {
            if (statusEnum.getCode().equals(user.getStatus())) {
                status = statusEnum.getName();
            }
        }

        //-------------------- SEND EMAIL NOTIFICATION TO SCP USER WITH DEFAULT PASSWORD -----------------------
        processEmailNotificationToApprovedSCPUserWithDefaultPassword(user, status);

        //---------------------------- EMAIL & WEB NOTIFICATION TO SCP ADMIN -----------------------------------

        /**
         * --- What processEmailAndWebNotificationAboutApprovedUserToSCPAdmin() method do ---
         * Get a email address list of SCP Admin from EmailNotificationConfig
         * Check if SCP admin who approved new SCP user is in the list, no need to send
         * because he/she is the one who approved new SCP user.
         * Send to the rest of SCP admin who is not the person approve new SCP admin.
         */
        processEmailAndWebNotificationAboutApprovedUserToSCPAdmin(user, null);

        IdsmedUserResponse response = new IdsmedUserResponse(user, ossBucketDomain);
        return response;
    }

    //This is for generate email pair for notification, cause email pair for email does not suitable, so have to use another email pair to get current login user name.
//    private Pair<String, String> generateEmailPairNotificationForSCPAdmin(IdsmedAccount idsmedAccount, IdsmedUser user) throws IdsmedBaseException{
//        IdsmedUser currentLoginUser = idsmedAccount.getIdsmedUser();
//        Vendor vendor = null;
//        if(!StringUtils.isEmpty(currentLoginUser.getCompanyCode())){
//            vendor = vendorRepository.findFirstByCompanyCode(currentLoginUser.getCompanyCode());
//        }
//        String currentLoginUserName = currentLoginUser.getFirstName() + " " + currentLoginUser.getLastName();
//        String approvedUserName = user.getFirstName() + " " + user.getLastName();
//        Pair<String, String> emailPairNotificationForSCPAdmin = emailService.generatedUserApprovedEmailSubjectAndContentForSCPAdmin(
//                currentLoginUserName, approvedUserName, vendor != null ? vendor.getCompanyNamePrimary() : "-");
//
//        return emailPairNotificationForSCPAdmin;
//    }

    @Transactional
    public IdsmedAccount saveAccountIntoDatabase(IdsmedAccount idsmedAccount, IdsmedUserRequest request){
        if (idsmedAccount != null) {
            idsmedAccount.setPassword(passwordEncoder.encode(DefaultPasswordEnum.DEFAULT_PASSWORD.getPassword()));
            if (request.getVendorCoordinator() != null) {
                idsmedAccount.setVendorCoordinator(request.getVendorCoordinator());
            }
            idsmedAccount = idsmedAccountRepository.save(idsmedAccount);
        }
        return idsmedAccount;
    }

    // if the user who create the SCP user has user approved permission, then it will autoapproved this user.
    // After auto approved this user., it will assign role for thsi user too.
    private IdsmedUser autoApprovedSCPUser(IdsmedUser user, IdsmedUserRequest request) throws IdsmedBaseException{
        IdsmedAccount idsmedAccount = idsmedAccountRepository.findByLoginId(request.getLoginId());
        IdsmedUser idsmedUser = idsmedAccount.getIdsmedUser();
        IdsmedUser userApprover = idsmedUserRepository.findByIdsmedUserAndPermissionCode(idsmedUser, IdsmedPermissionCodeConst.USER_APPROVE);
        if(userApprover != null){
            user.setStatus(StatusEnum.APPROVED.getCode());
            generateUserRole(user, request);
        }
        return user;
    }

    private IdsmedAccount autoApprovedSCPUserAccount(IdsmedUser user, String userLoginId, String password, IdsmedAccountTypeEnum accountType, IdsmedUserRequest request){
        //Generate account
        IdsmedAccount account = generateProperAccount(user, userLoginId, password, accountType, StatusEnum.APPROVED);
        account = saveAccountIntoDatabase(account, request);
        return account;
    }

    private void generateUserRole(IdsmedUser idsmedUser, IdsmedUserRequest request) throws IdsmedBaseException{
        if(request.getRoleId() != null) {
            Optional<IdsmedRole> idsmedRoleOpt = idsmedRoleRepository.findById(request.getRoleId());
            IdsmedRole idsmedRole = idsmedRoleOpt.get();
            if (!idsmedRoleOpt.isPresent()) {
                logger.error("The Role Id not existing");
                throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.ROLE_NOT_FOUND_ERROR, LangHelper.getLangCode(request.getLangCode())));
            }
            IdsmedUserRole idsmedUserRole = new IdsmedUserRole();
            idsmedUserRole.setIdsmedUser(idsmedUser);
            idsmedUserRole.setIdsmedRole(idsmedRole);
            idsmedUserRole.setStatus(StatusEnum.APPROVED.getCode());

            idsmedUserRoleRepository.save(idsmedUserRole);
        }
    }

    @Transactional
    public IdsmedUserResponse createUserWithCompanyCodePendingApprove(IdsmedUserRequest request, MultipartFile profileImage) throws IdsmedBaseException {
        Vendor vendor = vendorRepository.findFirstByCompanyCode(request.getCompanyCode());
        if (vendor == null) {
            throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.VENDOR_NOT_FOUND_ERROR, LangHelper.getLangCode(request.getLangCode())));
        }

        if(vendor.getStatus() != StatusEnum.APPROVED.getCode()){
            throw new IdsmedBaseException((messageResourceService.getErrorInfo(ErrorInfo.VENDOR_IS_NOT_ACTIVE_ERROR, LangHelper.getLangCode(request.getLangCode()))));
        }
        String ossBucketDomain = env.getProperty(PropertiesLabelConst.OSS_BUCKET_DOMAIN);
        IdsmedUserResponse response = null;
        IdsmedUser user = new IdsmedUser();
        BeanUtils.copyProperties(request, user, "id");
        user.setTypeOfUser(TypeOfUserEnum.VENDOR_USER.getCode());
        user.setStatus(StatusEnum.PENDING_FOR_APPROVAL.getCode());

        //Upload profile image
        if (profileImage != null) {
            user = uploadAndSetProfileImage(user, profileImage, request.getLangCode());
        }

        idsmedUserRepository.save(user);

        //Generate account
        IdsmedAccount account = generateProperAccount(user,
                request.getUserLoginId(), request.getPassword(),
                IdsmedAccountTypeEnum.IDSMED_USER, StatusEnum.PENDING_FOR_APPROVAL);
        if (account != null) {
            account = idsmedAccountRepository.save(account);
        }
        user.setCreatedBy(account.getId());
        user.setUpdatedBy(account.getId());
        idsmedUserRepository.save(user);

        //Set accountsetting
        IdsmedAccountSettings accSettings = new IdsmedAccountSettings(account, request.getLangCode());
        idsmedAccountSettingsRepository.save(accSettings);
        List<IdsmedAccount> accounts = new ArrayList<>();
        accounts.add(account);
        user.setIdsmedAccounts(accounts);
        response = new IdsmedUserResponse(user, ossBucketDomain);

        //CREATE APPROVE TASK
        taskService.createUserApproveTask(user);

        // Send email notification to vendor senior with permission to approve new vendor staff
        // No web notification for this
        processEmailNotificationAboutPendingApprovalVendorUserToVendorSenior(user, IdsmedPermissionCodeConst.USER_APPROVE);

        // Send email notification to pending approval vendor user
        // No web notification for this
        processEmailNotificationToPendingApprovalVendorUser(user);

        return response;
    }

    @Transactional
    public IdsmedUserResponse createUserWithCompanyCode(IdsmedUserRequest request, MultipartFile profileImage) throws IdsmedBaseException {
        Vendor vendor = vendorRepository.findFirstByCompanyCode(request.getCompanyCode());
        if (vendor == null) {
            throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.VENDOR_NOT_FOUND_ERROR, LangHelper.getLangCode(request.getLangCode())));
        }
        String ossBucketDomain = env.getProperty(PropertiesLabelConst.OSS_BUCKET_DOMAIN);
        if(vendor.getStatus() != StatusEnum.APPROVED.getCode()){
            throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.VENDOR_IS_NOT_ACTIVE_ERROR, LangHelper.getLangCode(request.getLangCode())));
        }

        IdsmedUserResponse response = null;
        IdsmedUser user = new IdsmedUser();
        BeanUtils.copyProperties(request, user, "id");
        user.setTypeOfUser(TypeOfUserEnum.VENDOR_USER.getCode());
        user.setStatus(StatusEnum.APPROVED.getCode());

        //Upload profile image
        if (profileImage != null) {
            user = uploadAndSetProfileImage(user, profileImage, request.getLangCode());
        }

        idsmedUserRepository.save(user);

        //Generate account
        IdsmedAccount account = generateProperAccount(user, request.getUserLoginId(), request.getPassword(), IdsmedAccountTypeEnum.IDSMED_USER, StatusEnum.APPROVED);
        if (account != null) {
            account = idsmedAccountRepository.save(account);
        }
        //set user create by and update by
        user.setCreatedBy(account.getId());
        user.setUpdatedBy(account.getId());
        idsmedUserRepository.save(user);

        IdsmedRole seniorVendorAdminRole = idsmedRoleRepository.findByCode(SENIOR_VENDOR_ADMIN_ROLE_CODE);
        if (seniorVendorAdminRole == null) {
            throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.ROLE_NOT_FOUND_ERROR
                    , LangHelper.getLangCode(request.getLangCode())));
        }

        //Set role
        IdsmedUserRole idsmedUserRole = null;

        idsmedUserRole = generateProperUserRole(user, seniorVendorAdminRole, request.getLangCode(), StatusEnum.APPROVED);

        if (idsmedUserRole != null) {
            idsmedUserRoleRepository.save(idsmedUserRole);
        }

        //Set accountsetting
        IdsmedAccountSettings accSettings = new IdsmedAccountSettings(account, request.getLangCode());
        idsmedAccountSettingsRepository.save(accSettings);
        List<IdsmedAccount> accounts = new ArrayList<>();
        accounts.add(account);
        user.setIdsmedAccounts(accounts);

        /**
         * New vendor staff will be auto approve if provide company code when register
         * An approved new vendor staff email notification will send to vendor senior
         * based on email address keyed-in by new vendor staff when registering
         * No Web Notification for this
         */
        processEmailNotificationToAutoApprovedVendorUser(user);

        /**
         * Send email notification to SCP Admin to notify auto approved
         * loginId parameter field is empty due to this is auto approve,
         * no user need to filter/exclude out
         */
        processEmailAndWebNotificationAboutApprovedUserToSCPAdmin(user, vendor);

        response = new IdsmedUserResponse(user, ossBucketDomain);

        return response;
    }

    private IdsmedUserRole generateProperUserRole(IdsmedUser user
            , IdsmedRole role, String langCode, StatusEnum status) throws IdsmedBaseException {
        IdsmedUserRole idsmedUserRole = new IdsmedUserRole();
        if (role == null) {
            throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.ROLE_NOT_FOUND_ERROR, LangHelper.getLangCode(langCode)));
        }
        //set user create by and update by
        idsmedUserRole.setIdsmedUser(user);
        idsmedUserRole.setIdsmedRole(role);
        idsmedUserRole.setStatus(status.getCode());

        return idsmedUserRole;
    }

    public IdsmedAccount generateProperAccount(IdsmedUser user
            , String loginId
            , String password
            , IdsmedAccountTypeEnum accountType
            , StatusEnum status) {
        IdsmedAccount account = new IdsmedAccount();
        account.setLoginId(loginId.toLowerCase());
        account.setIdsmedUser(user);
        account.setType(accountType.getCode());
        if (!StringUtils.isEmpty(password)) {
            account.setPassword(passwordEncoder.encode(password));
        }
        account.setStatus(status.getCode());

        return account;
    }

    private IdsmedUser uploadAndSetProfileImage(IdsmedUser user, MultipartFile profileImage, String langCode) throws IdsmedBaseException {
        Map<String, String> fileUploadInfoMap = new HashMap<>();
        try {
            fileUploadInfoMap = fileUploadService.uploadImage(profileImage, EntityType.USER);
        } catch (IOException e) {
            throw new IdsmedBaseException(
                    messageResourceService.getErrorInfo(ErrorInfo.UPLOAD_FILE_ERROR, LangHelper.getLangCode(langCode)));
        }
        String relativePath = fileUploadInfoMap.get(FileUploadServiceImpl.UPLOAD_URL);
        user.setProfileImageUrl(relativePath);
        return user;
    }

    public IdsmedUser uploadAndSetImagesForHospitalAndCorporateUser(IdsmedUser user, MultipartFile profileImage, MultipartFile frontIdImage, MultipartFile backIdImage, String langCode) throws IdsmedBaseException {
        if (profileImage != null) {
            Map<String, String> fileUploadInfoMap = new HashMap<>();
            try {
                fileUploadInfoMap = fileUploadService.uploadImage(profileImage, EntityType.USER);
            } catch (IOException e) {
                throw new IdsmedBaseException(
                        messageResourceService.getErrorInfo(ErrorInfo.UPLOAD_FILE_ERROR, LangHelper.getLangCode(langCode)));
            }
            String relativePath = fileUploadInfoMap.get(FileUploadServiceImpl.UPLOAD_URL);
            user.setProfileImageUrl(relativePath);
        }

        if (frontIdImage != null) {
            Map<String, String> fileUploadInfoMap = new HashMap<>();
            try {
                fileUploadInfoMap = fileUploadService.uploadImage(frontIdImage, EntityType.USER);
            } catch (IOException e) {
                throw new IdsmedBaseException(
                        messageResourceService.getErrorInfo(ErrorInfo.UPLOAD_FILE_ERROR, LangHelper.getLangCode(langCode)));
            }
            String relativePath = fileUploadInfoMap.get(FileUploadServiceImpl.UPLOAD_URL);
            user.setIdCardFrontPic(relativePath);
        }

        if (backIdImage != null) {
            Map<String, String> fileUploadInfoMap = new HashMap<>();
            try {
                fileUploadInfoMap = fileUploadService.uploadImage(backIdImage, EntityType.USER);
            } catch (IOException e) {
                throw new IdsmedBaseException(
                        messageResourceService.getErrorInfo(ErrorInfo.UPLOAD_FILE_ERROR, LangHelper.getLangCode(langCode)));
            }
            String relativePath = fileUploadInfoMap.get(FileUploadServiceImpl.UPLOAD_URL);
            user.setIdCardBackPic(relativePath);
        }
        return user;
    }

    @Transactional
    @PreAuthorize("@PermissionService.isMethodPermited(authentication, {'UA0000004'})")
    public IdsmedUserResponse approve(IdsmedUserRequest request) throws IdsmedBaseException {
        loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, APPROVED_IDSMED_USER_METHOD_NAME, request.getUserLoginId());
        Long userId = request.getId();
        Long roleId = request.getRoleId();

        IdsmedAccount idsmedAccount = idsmedAccountRepository.findByLoginId(request.getLoginId());
        Long currentAccountId = idsmedAccount.getId();
        String ossBucketDomain = env.getProperty(PropertiesLabelConst.OSS_BUCKET_DOMAIN);


        Optional<IdsmedUser> user = idsmedUserRepository.findById(userId);
        Optional<IdsmedRole> idsmedRole = idsmedRoleRepository.findById(Long.valueOf(roleId));

        if (!user.isPresent()) {
            logger.error("The User Id not existing");
            throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.USER_ID_NOT_EXISTED, LangHelper.getLangCode(request.getLangCode())));
        }

        if (!idsmedRole.isPresent()) {
            logger.error("The Role Id not existing");
            throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.ROLE_NOT_FOUND_ERROR, LangHelper.getLangCode(request.getLangCode())));
        }

        List<IdsmedAccount> accounts = user.get().getIdsmedAccounts().isEmpty() ? null : user.get().getIdsmedAccounts();

        if (CollectionUtils.isEmpty(accounts)) {
            logger.error("The User login Id doesn't existing");
            throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.LOGIN_ID_NOT_EXISTED, LangHelper.getLangCode(request.getLangCode())));
        }

        //Check approve status if there is a company code
        Vendor vendor = null;
        if (!StringUtils.isEmpty(user.get().getCompanyCode())) {
            vendor = vendorRepository.findFirstByCompanyCode(user.get().getCompanyCode());
            if (vendor != null && vendor.getStatus().equals(StatusEnum.APPROVED.getCode()) == false) {
                logger.error(String.format("The Vendor linked with the company code %s is not approved", user.get().getCompanyCode()));
                throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.VENDOR_NOT_APPROVED, LangHelper.getLangCode(request.getLangCode())));
            }

            if (vendor == null) {
                logger.error(String.format("The Company Code is not valid : ", user.get().getCompanyCode()));
                throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.VENDOR_NOT_FOUND_ERROR, LangHelper.getLangCode(request.getLangCode())));
            }
        }

        user.get().setStatus(StatusEnum.APPROVED.getCode());
        user.get().setApprovedDate(DateTimeHelper.getCurrentTimeUTC());
        user.get().setApprovedBy(currentAccountId);
        accounts.forEach(a -> {
            a.setStatus(StatusEnum.APPROVED.getCode());
        });

        IdsmedUser storedUser = idsmedUserRepository.save(user.get());
        idsmedAccountRepository.saveAll(accounts);

        IdsmedUserRole idsmedUserRole = new IdsmedUserRole();
        idsmedUserRole.setIdsmedUser(storedUser);
        idsmedUserRole.setIdsmedRole(idsmedRole.get());
        idsmedUserRole.setStatus(StatusEnum.APPROVED.getCode());

        idsmedUserRoleRepository.save(idsmedUserRole);

        IdsmedUserResponse response = new IdsmedUserResponse(user.get(), new ArrayList<IdsmedRole>(), ossBucketDomain);

        List<IdsmedAccount> idsmedAccounts = storedUser.getIdsmedAccounts();

        String status = null;
        for (StatusEnum statusEnum : StatusEnum.values()) {
            if (statusEnum.getCode().equals(storedUser.getStatus())) {
                status = statusEnum.getName();
            }
        }

        //create completed user approve task
        taskService.setCompletedUserApproveTask(storedUser, request.getLoginId());

        //-------------------- SEND EMAIL NOTIFICATION TO SCP USER WITH DEFAULT PASSWORD -----------------------

        if (!StringUtils.isEmpty(user.get().getCompanyCode())) {
            IdsmedUser vendorUser = user.get();
            /**
             * This email notification is for pending approval vendor user
             * who approved by vendor senior
             */
            processEmailNotificationToApprovedVendorUser(vendorUser);
        } else {
            processEmailNotificationToApprovedSCPUserWithDefaultPassword(storedUser, status);
        }

        //---------------------------- EMAIL & WEB NOTIFICATION TO SCP ADMIN -----------------------------------

        /**
         * --- What processEmailAndWebNotificationAboutApprovedUserToSCPAdmin() method do ---
         * Get a email address list of SCP Admin from EmailNotificationConfig
         * Check if SCP admin who approved new SCP user is in the list, no need to send
         * because he/she is the one who approved new SCP user.
         * Send to the rest of SCP admin who is not the person approve new SCP admin.
         */
        processEmailAndWebNotificationAboutApprovedUserToSCPAdmin(storedUser, vendor);
        loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, APPROVED_IDSMED_USER_METHOD_NAME, request.getUserLoginId());
        return response;
    }

    /**
     * Get user account based on the email address from email notification config
     * 2 case will happen in this method
     * 1 - Send email to valid SCP Admin in the system
     * 2 - Send email to potential SCP Admin using the email address registered in Email Notification Config
     */
    public List<IdsmedUser> getAccountListOfSCPAdmin(String settingRecipientEmails) throws IdsmedBaseException {
        List<IdsmedUser> recipients = new ArrayList<>();

        // Get all SCP Admin in system
        List<IdsmedUser> scpAdminUsers = idsmedUserRepository.findUserByRoleCode(IdsmedRoleCodeConst.SCP_ADMIN, StatusEnum.APPROVED.getCode());
        if (!CollectionUtils.isEmpty(scpAdminUsers)) {
            recipients.addAll(scpAdminUsers);
        }

        if (!StringUtils.isEmpty(settingRecipientEmails)) {
            String[] emailStrList = settingRecipientEmails.split(";");

            // Get valid user based on email address
            for (String emailAddress : emailStrList) {
                List<IdsmedUser> users = idsmedUserRepository.findAllByEmail(emailAddress);

                if (!CollectionUtils.isEmpty(users)) {
                    for (IdsmedUser u : users) {
                        Boolean isExist = false;
                        for (IdsmedUser existingUser : recipients) {
                            if (existingUser.getId().equals(u.getId())) {
                                isExist = true;
                                break;
                            }
                        }
                        if (!isExist) {
                            recipients.add(u);
                        }
                    }
                }
            }
        }

        return recipients;
    }

    private List<IdsmedAccount> getAccountListForUserWithUserApprovePermission(IdsmedUser user) {
        List<IdsmedUser> idsmedUserList = new ArrayList<>();
        idsmedUserList = authenticationService.getUsersByPermissionCodeForSCPAdmin(
                IdsmedPermissionCodeConst.USER_APPROVE);



        List<IdsmedAccount> accountList = new ArrayList<>();
        for (IdsmedUser idsmedUser : idsmedUserList) {
            List<IdsmedAccount> idsmedAccountList = idsmedUser.getIdsmedAccounts().isEmpty() ? null : idsmedUser.getIdsmedAccounts();
            if (!CollectionUtils.isEmpty(idsmedAccountList)) {
                accountList.addAll(idsmedAccountList);
            }
        }

        return accountList;
    }

    @Transactional
    @PreAuthorize("@PermissionService.isMethodPermited(authentication, {'UA0000006'})")
    public IdsmedUserResponse reject(IdsmedUserRequest request) throws IdsmedBaseException {
        loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, REJECT_IDSMED_USER_METHOD_NAME, request.getUserLoginId());

        IdsmedAccount idsmedAccount = idsmedAccountRepository.findByLoginId(request.getLoginId());
        Long currentAccountId = idsmedAccount.getId();
        String ossBucketDomain = env.getProperty(PropertiesLabelConst.OSS_BUCKET_DOMAIN);

        Long userId = request.getId();
        String rejectReason = request.getRejectReason();


        Optional<IdsmedUser> userOptional = idsmedUserRepository.findById(Long.valueOf(userId));


        if (!userOptional.isPresent()) {
            logger.error("The User Id not existing");
            throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.USER_ID_NOT_EXISTED, LangHelper.getLangCode(request.getLangCode())));
        }
        IdsmedUser user = userOptional.get();
        
        user.setStatus(StatusEnum.REJECT.getCode());
        user.setRejectReason(rejectReason);

        idsmedUserRepository.save(user);

        IdsmedUserResponse response = new IdsmedUserResponse(user, new ArrayList<>(), ossBucketDomain);

        //create completed user approve task
        taskService.setCompletedUserApproveTask(user, request.getLoginId());

        processUserRegisterRejectEmail(user, rejectReason, request.getLangCode());

        loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, REJECT_IDSMED_USER_METHOD_NAME, request.getUserLoginId());
        return response;
    }

    @Override
    @Transactional
    @Modifying
    public IdsmedUserResponse updateUserRole(UserUpdateRoleSimpleRequest request) throws IdsmedBaseException {
        loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, UPDATE_USER_ROLE_METHOD_NAME, request.getLoginId());
        Optional<IdsmedUser> userOpt = idsmedUserRepository.findById(request.getUserId());
        if (!userOpt.isPresent()) {
            throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.USER_NOT_FOUND_ERROR, LangHelper.getLangCode(request.getLangCode())));
        }
        String ossBucketDomain = env.getProperty(PropertiesLabelConst.OSS_BUCKET_DOMAIN);

        IdsmedUser user = userOpt.get();
        Optional<IdsmedRole> roleOpt = idsmedRoleRepository.findById(request.getRoleId());
        if (!roleOpt.isPresent()) {
            throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.ROLE_NOT_FOUND_ERROR, LangHelper.getLangCode(request.getLangCode())));
        }

        IdsmedRole role = roleOpt.get();

        // logic here is when the company code get from idsmedUser is empty, then implements logic to set vendor coordinator to true or false base on the vendor coordinator value pass from front end.
        if(StringUtils.isEmpty(user.getCompanyCode())){
            List<IdsmedAccount> vendorCoordinatorAccountList = user.getIdsmedAccounts();
            vendorCoordinatorAccountList = vendorCoordinatorAccountList.stream().map(vca->{
                IdsmedAccount vendorCoordinatorAccount = idsmedAccountRepository.findByLoginId(vca.getLoginId());
                if(request.getVendorCoordinator() != null) {
                    vendorCoordinatorAccount.setVendorCoordinator(request.getVendorCoordinator());
                }
                return vendorCoordinatorAccount;
            }).collect(Collectors.toList());

            idsmedAccountRepository.saveAll(vendorCoordinatorAccountList);
        }

        //Delete existing IdsmedUserRole
        List<IdsmedUserRole> existingUserRole = user.getUserRoles();
        if (!CollectionUtils.isEmpty(existingUserRole)) {
            idsmedUserRoleRepository.deleteAll(existingUserRole);
        }

        //Create new IdsmedUserRole
        IdsmedUserRole userRole = new IdsmedUserRole();
        userRole.setIdsmedRole(role);
        userRole.setIdsmedUser(user);
        userRole.setStatus(StatusEnum.APPROVED.getCode());
        List<IdsmedUserRole> userRoles = new ArrayList<>();
        userRoles.add(userRole);

        userRole = idsmedUserRoleRepository.save(userRole);
        List<IdsmedRole> roles = new ArrayList<>();
        roles.add(role);
        user.setUserRoles(userRoles);
        loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, UPDATE_USER_ROLE_METHOD_NAME, request.getLoginId());
        IdsmedUserResponse response = new IdsmedUserResponse(user, roles, ossBucketDomain);
        return response;
    }

    @Override
    @Transactional
    public IdsmedUserResponse edit(IdsmedUserRequest request, MultipartFile profileImage) throws IdsmedBaseException {
        loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, EDIT_IDSMED_USER_METHOD_NAME, request.getLoginId());
        IdsmedUser user = idsmedUserRepository.findById(request.getId()).get();
        IdsmedAccount idsmedAccount = idsmedAccountRepository.findByLoginId(request.getLoginId());
        if (user == null) {
            logger.error("The User Id not existing");
            throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.USER_ID_NOT_EXISTED, LangHelper.getLangCode(request.getLangCode())));
        }
        String ossBucketDomain = env.getProperty(PropertiesLabelConst.OSS_BUCKET_DOMAIN);

        String[] ignoreProperties = {"id", "companyCode", "approvedDate", "approvedBy"};
        BeanUtils.copyProperties(request, user, ignoreProperties);
//        if (user.getApprovedDate() == null)
//            user.setStatus(StatusEnum.PENDING_FOR_APPROVAL.getCode());

        user.setCountryCode(CountryCodeEnum.CHINA.getCode());
        Map<String, String> fileUploadInfoMap = new HashMap<>();
        if (profileImage != null) {
            try {
                fileUploadInfoMap = fileUploadService.uploadImage(profileImage, EntityType.USER);
            } catch (IOException e) {
                throw new IdsmedBaseException(
                        messageResourceService.getErrorInfo(ErrorInfo.UPLOAD_FILE_ERROR, LangHelper.getLangCode(request.getLangCode())));
            }
            String relativePath = fileUploadInfoMap.get(FileUploadServiceImpl.UPLOAD_URL);
            user.setProfileImageUrl(relativePath);
        }

//        user = autoApprovedSCPUser(user, request);

        idsmedUserRepository.save(user);

        idsmedAccount.setVendorCoordinator(request.getVendorCoordinator());
        idsmedAccountRepository.save(idsmedAccount);

        // save into Account notification table and delete all from existing account notification setting.
        saveIntoAccountNotificationSettingTable(idsmedAccount, request);

        //save into TenderCategory table when the care area id list is not emtpy
        saveIntoTenderCategoryTable(idsmedAccount, request);

        /**
         * ---------- CREATE APPROVE TASK ----------
         * Add validation check for create user task list due to user
         * created by SCP admin will be auto approve so no task list should be created
         * while vendor user still need to create task list for resubmit
         */
        if (user.getCompanyCode() != null) {
            taskService.createUserApproveTask(user);
        }

        List<IdsmedUserRole> userRoles = user.getUserRoles();
        List<IdsmedRole> roles = new ArrayList<>();
        if (!CollectionUtils.isEmpty(userRoles)) {
            roles = userRoles.stream().map(ur -> ur.getIdsmedRole()).collect(Collectors.toList());
        }

        // Send email notification to vendor senior with permission to approve new vendor staff
        // No web notification for this
        processEmailNotificationAboutPendingApprovalVendorUserToVendorSenior(user, IdsmedPermissionCodeConst.USER_APPROVE);

        // Send email notification to pending approval vendor user
        // No web notification for this
        processEmailNotificationToPendingApprovalVendorUser(user);

        loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, EDIT_IDSMED_USER_METHOD_NAME, request.getLoginId());
        IdsmedUserResponse response = new IdsmedUserResponse(user, roles, ossBucketDomain);
        return response;
    }

    //Save account notification setting into account notification setting database.
    private void saveIntoAccountNotificationSettingTable(IdsmedAccount idsmedAccount, IdsmedUserRequest request){
        List<AccountNotificationSetting> accountNotificationSettings = new ArrayList<>();
        List<AccountNotificationSetting> existingAccountNotificationSettings = accountNotificationSettingRepository.
                findAllByIdsmedAccount(idsmedAccount);
        if(!CollectionUtils.isEmpty(existingAccountNotificationSettings)){
            accountNotificationSettingRepository.deleteAll(existingAccountNotificationSettings);
        }

        if(!CollectionUtils.isEmpty(request.getAccountEmailNotificationPreferences())){
            for(Integer accountEmailNotificationPreference : request.getAccountEmailNotificationPreferences()){
                AccountNotificationSetting accountNotificationSetting = new AccountNotificationSetting(idsmedAccount,
                        accountEmailNotificationPreference);
                accountNotificationSettings.add(accountNotificationSetting);
            }
        }

        accountNotificationSettingRepository.saveAll(accountNotificationSettings);
    }

    private void saveIntoTenderCategoryTable(IdsmedAccount idsmedAccount, IdsmedUserRequest request){
        List<TenderCategory> tenderCategories = new ArrayList<>();
        if(!CollectionUtils.isEmpty(request.getCareAreaId())){
            tenderCategories = request.getCareAreaId().stream().map(crid ->{
                TenderCategory tenderCategory = new TenderCategory(idsmedAccount, crid);
                return tenderCategory;
            }).collect(Collectors.toList());
        }
        tenderCategoryRepository.saveAll(tenderCategories);
    }

    public IdsmedAccountSettingsResponse editAccountSettings(IdsmedAccountSettingsRequest request) throws IdsmedBaseException {
        loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, EDIT_ACCOUNT_SETTING_METHOD_NAME, request.getLoginId());

        if (StringUtils.isEmpty(request.getUserLoginId()) || StringUtils.isEmpty(request.getCurrentLangCode())) {
            logger.error("User Login Id and Current Lang Code is required");
            throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.ACCOUNT_SETTINGS_MISSING_PARAMETERS, LangHelper.getLangCode(request.getLangCode())));
        }

        IdsmedAccount account = idsmedAccountRepository.findByLoginId(request.getUserLoginId());
        if (account == null) {
            logger.error("Account is not exist with user login id provided");
            throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.ACCOUNT_NOT_EXIST_ERROR, LangHelper.getLangCode(request.getLangCode())));
        }

        IdsmedAccountSettings accountSettings = account.getAccountSettings();

        if (accountSettings == null) {
            accountSettings = new IdsmedAccountSettings(account, request.getCurrentLangCode());
        } else {
            accountSettings.setCurrentLangCode(request.getCurrentLangCode());
        }

        IdsmedAccountSettings storedAccountSettings = idsmedAccountSettingsRepository.save(accountSettings);

        IdsmedAccountSettingsResponse response = new IdsmedAccountSettingsResponse(storedAccountSettings);
        loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, EDIT_ACCOUNT_SETTING_METHOD_NAME, request.getLoginId());
        return response;
    }

    private void processEmailNotificationToAutoApprovedVendorUser(IdsmedUser user) throws IdsmedBaseException {
        List<String> recipients = new ArrayList<>();
        Pair<String, String> emailPair = emailService.generatedVendorUserApprovedEmailSubjectAndContent(
                user.getFirstName() + " " + user.getLastName(),
                StatusEnum.APPROVED.getName()
        );
        if (checkIsVendorPersonInCharge1Email(user.getEmail()) || checkIsVendorPersonInCharge2Email(user.getEmail())) {
            Boolean isSendingEmail = emailService.checkSendEmail(user.getIdsmedAccounts(), EmailTypeEnum.VENDOR_USER_REGISTRATION_APPROVED_NOTIFICATION);
            if(isSendingEmail){
                recipients.add(user.getEmail());
            }

            if(!CollectionUtils.isEmpty(recipients)) {
                EmailObject email = new UserApproveEmailObject(
                        Arrays.asList(user.getEmail()),
                        env.getProperty(PropertiesLabelConst.SMTP_SUPPORT_MAIL_LABEL),
                        emailPair
                );

                rabbitTemplate.convertAndSend(env.getProperty(IdsmedQueueConfig.RABBIT_MAIL_SERVICE_PROCESS_QUEUE), email);
            }

        }
    }

    private Boolean checkIsVendorPersonInCharge1Email(String email) {
        if (!CollectionUtils.isEmpty(vendorRepository.findByPersonIncharge1Email(email))) {
            return true;
        }

        return false;
    }

    private Boolean checkIsVendorPersonInCharge2Email(String email) {
        if (!CollectionUtils.isEmpty(vendorRepository.findByPersonIncharge2Email(email))) {
            return true;
        }

        return false;
    }

    private void processEmailNotificationAboutPendingApprovalVendorUserToVendorSenior(IdsmedUser user, String permissionCode) throws IdsmedBaseException {
        List<IdsmedUser> idsmedUsers = new ArrayList<>();
        idsmedUsers = idsmedUserRepository.findAllByCompanyCodeAndPermissionCode(user.getCompanyCode(), permissionCode);
        List<IdsmedAccount> superAdminAccount = idsmedAccountRepository.findAccountsByRoleCode(IdsmedRoleCodeConst.SUPER_ADMIN);
        List<String> recipients = new ArrayList<>();
        if (!CollectionUtils.isEmpty(idsmedUsers)) {
            for (IdsmedUser iu : idsmedUsers) {
                Pair<String, String> emailPair = emailService.generateNewVendorStaffRegistrationForVendorAdminEmailSubjectAndContent(
                        iu.getFirstName(),
                        iu.getLastName(),
                        user.getFirstName(),
                        user.getLastName(),
                        StatusEnum.PENDING_FOR_APPROVAL.getName()
                );
                Boolean isSendingEmail = emailService.checkSendEmail(iu.getIdsmedAccounts(), EmailTypeEnum.NEW_VENDOR_STAFF_REGISTRATION_NOTIFICATION_FOR_VENDOR_ADMIN);
                if(isSendingEmail){
                    EmailObject email = new NewVendorStaffRegistrationEmailObject(
                            Arrays.asList(iu.getEmail()),
                            env.getProperty(PropertiesLabelConst.SMTP_SUPPORT_MAIL_LABEL),
                            emailPair
                    );

                    rabbitTemplate.convertAndSend(env.getProperty(IdsmedQueueConfig.RABBIT_MAIL_SERVICE_PROCESS_QUEUE), email);
                }
                // Send web notification to Vendor Admin
                notificationService.sendUserApprovedNotification(
                        superAdminAccount.get(0),
                        iu.getIdsmedAccounts(),
                        emailPair);
            }
        }
    }

    private void processEmailNotificationToApprovedSCPUserWithDefaultPassword(IdsmedUser idsmedUser, String status) throws IdsmedBaseException {
        IdsmedAccount idsmedAccount = idsmedAccountRepository.findFirstByIdsmedUser(idsmedUser);
        String loginId = idsmedAccount.getLoginId();

        Pair<String, String> emailPair = null;
        emailPair = emailService.generateApprovedSCPUserWithDefaultPasswordEmailSubjectAndContent(
                idsmedUser.getFirstName(),
                idsmedUser.getLastName(),
                loginId,
                status);

        EmailObject email = new SCPUserRegistrationApprovedEmailObject(
                Arrays.asList(idsmedUser.getEmail()), env.getProperty(PropertiesLabelConst.SMTP_SUPPORT_MAIL_LABEL), emailPair);

        rabbitTemplate.convertAndSend(env.getProperty(IdsmedQueueConfig.RABBIT_MAIL_SERVICE_PROCESS_QUEUE), email);
    }

    /**
     * @param approvedUser - Approved user
     * @param vendor - This parameter for approved vendor user(staff) because need to include company name
     * @throws IdsmedBaseException
     */
    private void processEmailAndWebNotificationAboutApprovedUserToSCPAdmin(IdsmedUser approvedUser,
                                                                           Vendor vendor) throws IdsmedBaseException {
        /**
         * Get recipient list of User Registration Approved Notification for SCP Admin
         * from Email Notification Configuration
         */
        String recipientEmail = emailNotificationConfigService
                .getEmailNotificationConfigByEmailType(EmailTypeEnum.USER_REGISTRATION_APPROVED_NOTIFICATION_FOR_SCP_ADMIN)
                .getRecipientList();

        // Get list of SCP Admin Account
        List<IdsmedUser> scpAdminUsers = getAccountListOfSCPAdmin(recipientEmail);
        List<IdsmedAccount> superAdminAccount = idsmedAccountRepository.findAccountsByRoleCode(IdsmedRoleCodeConst.SUPER_ADMIN);

        Pair<String, String> emailPairForSCPAdmin = null;

        if (!CollectionUtils.isEmpty(scpAdminUsers)) {
            for (IdsmedUser scpAdminUser : scpAdminUsers) {
                String scpAdminName = scpAdminUser.getFirstName() + " " + scpAdminUser.getLastName();
                String approvedUserName = approvedUser.getFirstName() + " " + approvedUser.getLastName();
                emailPairForSCPAdmin = emailService.generatedUserApprovedEmailSubjectAndContentForSCPAdmin(
                        scpAdminName, approvedUserName, vendor != null ? vendor.getCompanyNamePrimary() : "-");

                EmailObject emailObject = new UserApproveEmailObject(
                        Arrays.asList(scpAdminUser.getEmail()),
                        env.getProperty(PropertiesLabelConst.SMTP_SUPPORT_MAIL_LABEL),
                        emailPairForSCPAdmin);

                rabbitTemplate.convertAndSend(env.getProperty(IdsmedQueueConfig.RABBIT_MAIL_SERVICE_PROCESS_QUEUE), emailObject);

                // Send web notification to SCP Admin
                notificationService.sendUserApprovedNotification(
                        superAdminAccount.get(0),
                        scpAdminUser.getIdsmedAccounts(),
                        emailPairForSCPAdmin);
            }
        }
    }

    private void processEmailNotificationToPendingApprovalVendorUser(IdsmedUser user) throws IdsmedBaseException {
        Pair<String, String> emailPair = emailService.generatePendingApprovalVendorUserEmailSubjectAndContent(
                user.getFirstName() + " " + user.getLastName()
        );

        EmailObject email = new NewVendorStaffRegistrationEmailObject(
                Arrays.asList(user.getEmail()),
                env.getProperty(PropertiesLabelConst.SMTP_SUPPORT_MAIL_LABEL),
                emailPair
        );

        rabbitTemplate.convertAndSend(env.getProperty(IdsmedQueueConfig.RABBIT_MAIL_SERVICE_PROCESS_QUEUE), email);
    }

    private void processEmailNotificationToApprovedVendorUser(IdsmedUser user) throws IdsmedBaseException {
        Pair<String, String> emailPair = emailService.generatedVendorUserApprovedEmailSubjectAndContent(
                user.getFirstName() + " " + user.getLastName(),
                StatusEnum.APPROVED.getName()
        );

        Boolean isSendingEmail = emailService.checkSendEmail(user.getIdsmedAccounts(), EmailTypeEnum.VENDOR_USER_REGISTRATION_APPROVED_NOTIFICATION);
        if(isSendingEmail){
            EmailObject email = new UserApproveEmailObject(
                    Arrays.asList(user.getEmail()),
                    env.getProperty(PropertiesLabelConst.SMTP_SUPPORT_MAIL_LABEL),
                    emailPair
            );

            rabbitTemplate.convertAndSend(env.getProperty(IdsmedQueueConfig.RABBIT_MAIL_SERVICE_PROCESS_QUEUE), email);
        }
    }

    @Override
    public List<IdsmedAccountResponse> getVendorCoordinatorList() throws IdsmedBaseException{
        loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, GET_VENDOR_COORDINATOR_LIST_METHOD_NAME, null);
        Boolean isVendorCoordinator = true;
        List<IdsmedAccount> idsmedAccounts = idsmedAccountRepository.findByVendorCoordinator(isVendorCoordinator);
        List<IdsmedAccountResponse> idsmedAccountResponses = new ArrayList<>();
        if(!CollectionUtils.isEmpty(idsmedAccounts)){
            idsmedAccountResponses =  idsmedAccounts.stream().map(ia -> {
                IdsmedUser iu = ia.getIdsmedUser();
                IdsmedAccountResponse idsmedAccountResponse = new IdsmedAccountResponse(ia, iu);
                return idsmedAccountResponse;
            }).collect(Collectors.toList());
        }
        loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, GET_VENDOR_COORDINATOR_LIST_METHOD_NAME, null);
        return idsmedAccountResponses;
    }

    /**
     * To inactive user
     * If current login user is SCP user, with permission, can inactive SCP user and Vendor user
     * If current login user is Vendor user, only can inactive user under same vendor
     * @param request
     * @return
     * @throws IdsmedBaseException
     */
    @Override
    @PreAuthorize("@PermissionService.isMethodPermited(authentication, {'UA0000005'})")
    @Transactional
    public IdsmedUserResponse inactiveUserStatus(UserStatusRequest request) throws IdsmedBaseException {
        loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, INACTIVE_USER_STATUS_METHOD_NAME, request.getLoginId());
        IdsmedAccount account = idsmedAccountRepository.findByLoginId(request.getLoginId());
        if (account == null) {
            throw new IdsmedBaseException(messageResourceService.getErrorInfo(
                    ErrorInfo.ACCOUNT_NOT_EXIST_ERROR, LangHelper.getLangCode(request.getLangCode())));
        }

        IdsmedUser user = idsmedUserRepository.findById(request.getUserId()).get();
        if (user == null) {
            throw new IdsmedBaseException(messageResourceService.getErrorInfo(
                    ErrorInfo.USER_ID_NOT_EXISTED, LangHelper.getLangCode(request.getLangCode())));
        }

        Integer userTaskStatus = StatusEnum.ARCHIVED.getCode();
        Integer notificationStatus = NotificationRecipient.NotificationRecipientStatus.ARCHIVED.getCode();
        Integer userStatus = StatusEnum.INACTIVE.getCode();

        // If no company code, means current login user is SCP user
        if (StringUtils.isEmpty(account.getIdsmedUser().getCompanyCode())) {
            processUserTaskStatus(user, account, userTaskStatus);
            processNotificationRecipientStatus(user, account, notificationStatus);
            processUserStatus(user, account, userStatus, request);
        }

        // If with company code, means current login user is vendor user
        if (!StringUtils.isEmpty(account.getIdsmedUser().getCompanyCode())) {
            if (!user.getCompanyCode().equalsIgnoreCase(account.getIdsmedUser().getCompanyCode())) {
                throw new IdsmedBaseException(messageResourceService.getErrorInfo(
                        ErrorInfo.ACCOUNT_NOT_EXIST_ERROR, LangHelper.getLangCode(request.getLangCode())));
            }

            processUserTaskStatus(user, account, userTaskStatus);
            processNotificationRecipientStatus(user, account, notificationStatus);
            processUserStatus(user, account, userStatus, request);
        }

        IdsmedUserResponse response = new IdsmedUserResponse();
        loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, INACTIVE_USER_STATUS_METHOD_NAME, request.getLoginId());
        return response;
    }

    @Transactional
    public void processUserTaskStatus(IdsmedUser user, IdsmedAccount loginUser, Integer status) {
        if (!CollectionUtils.isEmpty(user.getIdsmedAccounts())) {
            for (IdsmedAccount account : user.getIdsmedAccounts()) {
                List<IdsmedTaskExecutor> taskExecutors = account.getTaskExecutors();
                List<IdsmedTaskExecutor> archiveTaskExecutors = new ArrayList<>();
                if (!CollectionUtils.isEmpty(taskExecutors)) {
                    for (IdsmedTaskExecutor taskExecutor : taskExecutors) {
                        taskExecutor.setStatus(status);
                        taskExecutor.setUpdatedBy(loginUser.getId());
                        archiveTaskExecutors.add(taskExecutor);
                    }

                    if (!CollectionUtils.isEmpty(archiveTaskExecutors)) {
                        idsmedTaskExecutorRepository.saveAll(archiveTaskExecutors);
                    }
                }
            }
        }
    }

    @Transactional
    public void processNotificationRecipientStatus(IdsmedUser user, IdsmedAccount loginUser, Integer status) {
        if (!CollectionUtils.isEmpty(user.getIdsmedAccounts())) {
            for (IdsmedAccount account : user.getIdsmedAccounts()) {
                List<NotificationRecipient> notificationRecipients = account.getNotificationRecipients();
                List<NotificationRecipient> archiveNotificationRecipients = new ArrayList<>();
                if (!CollectionUtils.isEmpty(notificationRecipients)) {
                    for (NotificationRecipient notificationRecipient : notificationRecipients) {
                        notificationRecipient.setStatus(status);
                        notificationRecipient.setUpdatedBy(loginUser.getId());
                        archiveNotificationRecipients.add(notificationRecipient);
                    }

                    if (!CollectionUtils.isEmpty(archiveNotificationRecipients)) {
                        notificationRecipientRepository.saveAll(archiveNotificationRecipients);
                    }
                }
            }
        }
    }

    @Transactional
    public void processUserStatus(IdsmedUser user, IdsmedAccount loginUser, Integer status, UserStatusRequest request) {
        if (!CollectionUtils.isEmpty(user.getIdsmedAccounts())) {
            List<IdsmedAccount> accounts = new ArrayList<>();
            for (IdsmedAccount account : user.getIdsmedAccounts()) {
                account.setStatus(status);
                account.setUpdatedBy(loginUser.getId());
                accounts.add(account);
            }

            if (!CollectionUtils.isEmpty(accounts)) {
                idsmedAccountRepository.saveAll(accounts);
            }

            if(status == StatusEnum.INACTIVE.getCode()){
                user.setStatus(status);
                user.setInactiveReason(request.getInactiveReason());
            }else {
                user.setStatus(status);
            }
            user.setUpdatedBy(loginUser.getId());
            idsmedUserRepository.save(user);
        }
    }

    @Override
    @PreAuthorize("@PermissionService.isMethodPermited(authentication, {'UA0000007'})")
    @Transactional
    public IdsmedUserResponse resumeInactiveUserStatus(UserStatusRequest request) throws IdsmedBaseException {
        loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, RESUME_INACTIVE_USER_STATUS_METHOD_NAME, request.getLoginId());
        IdsmedAccount account = idsmedAccountRepository.findByLoginId(request.getLoginId());
        if (account == null) {
            throw new IdsmedBaseException(messageResourceService.getErrorInfo(
                    ErrorInfo.ACCOUNT_NOT_EXIST_ERROR, LangHelper.getLangCode(request.getLangCode())));
        }

        IdsmedUser user = idsmedUserRepository.findById(request.getUserId()).get();
        if (user == null) {
            throw new IdsmedBaseException(messageResourceService.getErrorInfo(
                    ErrorInfo.USER_ID_NOT_EXISTED, LangHelper.getLangCode(request.getLangCode())));
        }

        Integer userStatus = StatusEnum.APPROVED.getCode();

        // If no company code, means current login user is SCP user
        if (StringUtils.isEmpty(account.getIdsmedUser().getCompanyCode())) {
            processUserStatus(user, account, userStatus, request);
        }

        // If with company code, means current login user is vendor user
        if (!StringUtils.isEmpty(account.getIdsmedUser().getCompanyCode())) {
            if (!user.getCompanyCode().equalsIgnoreCase(account.getIdsmedUser().getCompanyCode())) {
                throw new IdsmedBaseException(messageResourceService.getErrorInfo(
                        ErrorInfo.ACCOUNT_NOT_EXIST_ERROR, LangHelper.getLangCode(request.getLangCode())));
            }

            processUserStatus(user, account, userStatus, request);
        }

        IdsmedUserResponse response = new IdsmedUserResponse();
        loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, RESUME_INACTIVE_USER_STATUS_METHOD_NAME, request.getLoginId());
        return response;
    }

    private void processUserRegisterRejectEmail(IdsmedUser user, String rejectReason, String langCode) throws IdsmedBaseException {
        String baseLink = env.getProperty(USER_RE_REGISTER_BASE_LINK_LABEL);
        String resubmitLink = baseLink + "?id=" + user.getId();

        logger.info("Start send user reject email message with user id  : {} ", user.getId());
        // Loop and send email and web notification
        String status = null;
        for (StatusEnum statusEnum : StatusEnum.values()) {
            if (statusEnum.getCode().equals(user.getStatus())) {
                status = statusEnum.getName();
            }
        }

        String name = user.getFirstName() + " " + user.getLastName();
        Pair<String, String> emailPair = emailService.generatedUserRejectedEmailSubjectAndContent(
                name, status, rejectReason, resubmitLink, langCode);

        EmailObject emailObject = new UserRejectEmailObject(
                Arrays.asList(user.getEmail()),
                env.getProperty(PropertiesLabelConst.SMTP_SUPPORT_MAIL_LABEL),
                emailPair);

        rabbitTemplate.convertAndSend(env.getProperty(IdsmedQueueConfig.RABBIT_MAIL_SERVICE_PROCESS_QUEUE), emailObject);
        logger.info("End of send user reject email message with user id : {} ", user.getId());

//        logger.info("start send user reject web notification with user id : {} ", user.getId());
//        List<IdsmedAccount> userAccounts = user.getIdsmedAccounts();
//        notificationService.sendUserRejectNotification(idsmedAccount, userAccounts, emailPair);
//        logger.info("End send user reject web notification with user id : {} ", user.getId());

    }

    @Override
    public IdsmedUserResponse reRegisterUserById(Long id, String langCode) throws IdsmedBaseException {
        loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, REREGISTER_USER_BY_ID_METHOD_NAME, null);
        LocalDateTime currentDate = DateTimeHelper.getCurrentTimeUTC();
        LocalDateTime compareDate = DateTimeHelper.addTimeInUTC(currentDate,
                DateTimeHelper.DAY_OF_YEAR, -USER_RE_REGISTER_LINK_VALID_PERIOD);

        String ossBucketDomain = env.getProperty(PropertiesLabelConst.OSS_BUCKET_DOMAIN);

        IdsmedUser user = idsmedUserRepository.findUserByStatusAndValidUpdatedDate(id, StatusEnum.REJECT.getCode(), compareDate);
        if (user == null) {
            throw new IdsmedBaseException(messageResourceService.getErrorInfo(
                    ErrorInfo.USER_RE_REGISTER_NOT_FOUND_ERROR, LangHelper.getLangCode(langCode)));
        }

        IdsmedUserResponse response = new IdsmedUserResponse(user, ossBucketDomain);

        loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, REREGISTER_USER_BY_ID_METHOD_NAME, null);

        return response;
    }

    public Boolean checkValidNewLoginId(String loginId) {
        if (StringUtils.isEmpty(loginId)) {
            return false;
        }
        if (loginId.length() > 255) {
            return false;
        }
        IdsmedAccount idsmedAccount = idsmedAccountRepository.findByLoginId(loginId);
        if (idsmedAccount != null) {
            return false;
        }
        return true;
    }
    private void loggerForAllMethodInThisService(String methodDescription, String methodName, String loginId){
        logger.info(methodDescription, methodName, loginId != null?loginId:null);
    }

    @Override
    @Transactional
    public Boolean individualUserRegister(IdsmedIndividualUserRequest request
            , MultipartFile profileImage, MultipartFile frontIdImage, MultipartFile backIdImage) throws IdsmedBaseException {
        if (!validateIndividualUserRegistrationRequest(request)) {
            throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.USER_REQUEST_INFO_NOT_CORRECT, LangHelper.getLangCode(request.getLangCode())));
        }

        //Generate user
        if (!checkValidNewLoginId(request.getEmail())) {
            throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.LOGIN_ID_NOT_VALID_WHEN_REGISTER, LangHelper.getLangCode(request.getLangCode())));
        }
        IdsmedUser individualUser = new IdsmedUser(request);

        individualUser = uploadAndSetImagesForHospitalAndCorporateUser(individualUser, profileImage, frontIdImage, backIdImage, request.getLangCode());
        individualUser.setStatus(StatusEnum.PENDING_FOR_ACTIVATE_BY_REGISTER.getCode());
        String uuid = UUID.randomUUID().toString();
        individualUser.setUuid(uuid);
        individualUser = idsmedUserRepository.save(individualUser);

        //Generate User Address.
		Optional<Country> countryOpt = countryRepository.findById(request.getCountryId());
		Country country = countryOpt.get();
		Optional<Province> provinceOpt = provinceRepository.findById(request.getProvinceId());
		Province province = provinceOpt.get();
		UserAddress userAddress = new UserAddress(request.getFirstAddress(), request.getSecondAddress(), individualUser, country, province, request.getPostcode());
		userAddress = userAddressRepository.save(userAddress);
		List<UserAddress> userAddresses = new ArrayList<>();
		userAddresses.add(userAddress);
		individualUser.setUserAddresses(userAddresses);

        //Set Role for user
        IdsmedUserRole userRole = new IdsmedUserRole();
        userRole.setIdsmedUser(individualUser);
        IdsmedRole role =idsmedRoleRepository.findByCode(IdsmedRoleCodeConst.INDIVIDUAL_USER);
        userRole.setIdsmedRole(role);
        userRole.setStatus(StatusEnum.PENDING_FOR_ACTIVATE_BY_REGISTER.getCode());
        userRole = idsmedUserRoleRepository.save(userRole);
        List<IdsmedUserRole> userRoles = new ArrayList<>();
        individualUser.setUserRoles(userRoles);

        //Generate account
        String loginId = request.getEmail();
        IdsmedAccount account =generateProperAccount(individualUser, loginId, request.getPassword(), IdsmedAccountTypeEnum.INDIVIDUAL_USER, StatusEnum.PENDING_FOR_ACTIVATE_BY_REGISTER);
        account.setIdsmedUser(individualUser);

        account = idsmedAccountRepository.save(account);
        List<IdsmedAccount> accounts = new ArrayList<>();
        accounts.add(account);
        individualUser.setIdsmedAccounts(accounts);

        //Set account setting
        IdsmedAccountSettings accSettings = new IdsmedAccountSettings(account, request.getLangCode());
        idsmedAccountSettingsRepository.save(accSettings);

        processSendActivateLinkEmail(individualUser, request.getLangCode());

        return true;
    }


    @Override
    @Transactional
    public Boolean activateIndividualUser(String uuid, String langCode) throws IdsmedBaseException {
        if (StringUtils.isEmpty(uuid)) {
            throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.USER_CAN_NOT_ACTIVATE_ERROR, LangHelper.getLangCode(langCode)));
        }

        IdsmedUser individualUser = idsmedUserRepository.findFirstByUuid(uuid);
        if (individualUser == null) {
            throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.USER_CAN_NOT_ACTIVATE_ERROR, LangHelper.getLangCode(langCode)));
        }

        //Activate accounts
        List<IdsmedAccount> accounts = individualUser.getIdsmedAccounts();
        if (CollectionUtils.isEmpty(accounts)) {
            throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.USER_CAN_NOT_ACTIVATE_ERROR, LangHelper.getLangCode(langCode)));
        }

        accounts.stream().forEach(a -> {
            a.setStatus(StatusEnum.APPROVED.getCode());
        });

        accounts = idsmedAccountRepository.saveAll(accounts);

        //Set account setting
        for (IdsmedAccount account : accounts) {
            IdsmedAccountSettings accountSettings = account.getAccountSettings();
            if (accountSettings != null) {
                accountSettings.setStatus(StatusEnum.APPROVED.getCode());
                idsmedAccountSettingsRepository.save(accountSettings);
            }
        }

        //Activate user
        if(individualUser.getStatus().equals(StatusEnum.PENDING_FOR_ACTIVATE_BY_REGISTER.getCode())) {
            individualUser.setStatus(StatusEnum.APPROVED.getCode());
            idsmedUserRepository.save(individualUser);
        }

        //Activate UserRole
        individualUser.getUserRoles().forEach(ur -> {
            ur.setStatus(StatusEnum.APPROVED.getCode());
            idsmedUserRoleRepository.save(ur);
        });

        //Send activate success email.
        processSendIndividualUserActivationSuccessEmail(individualUser);

        return true;
    }

    private void processSendIndividualUserActivationSuccessEmail(IdsmedUser user) throws IdsmedBaseException {
        logger.info("Start sending Individual User Registration Success email to register",
                user.getFirstName());
        Pair<String, String> emailPair = null;
        emailPair = emailService.generateIndividualUserRegistrationActivationSuccessEmailSubjectAndContent(user);
        List<String> recipients = new ArrayList<>();
        String emailAddress = user.getEmail();
        recipients.add(emailAddress);
        EmailObject emailObject = new IndividualUserRegistrationSuccessEmailObject(recipients, env.getProperty(PropertiesLabelConst.SMTP_SUPPORT_MAIL_LABEL), emailPair);
        rabbitTemplate.convertAndSend(env.getProperty(IdsmedQueueConfig.RABBIT_MAIL_SERVICE_PROCESS_QUEUE), emailObject);

        logger.info("End sending Individual User Registration Success email to register {}",
                user.getFirstName());
    }

    private void processSendActivateLinkEmail(IdsmedUser user, String langCode) throws IdsmedBaseException {
        // Email Notification Part
        logger.info("Start sending Activate Link Email to register",
                user.getFirstName());

        Pair<String, String> emailPair = null;
        emailPair = emailService.generateIndividualUserRegistrationActivateLinkEmailSubjectAndContent(user, langCode);
        List<String> recipients = new ArrayList<>();
        String emailAddress = user.getEmail();
        recipients.add(emailAddress);
        EmailObject emailObject = new IndividualUserRegistrationActivationLinkEmailObject(recipients, env.getProperty(PropertiesLabelConst.SMTP_SUPPORT_MAIL_LABEL), emailPair);

        rabbitTemplate.convertAndSend(env.getProperty(IdsmedQueueConfig.RABBIT_MAIL_SERVICE_PROCESS_QUEUE), emailObject);

        logger.info("End sending Activate Link Email to register {}",
                user.getFirstName());
    }

    private Boolean validateIndividualUserRegistrationRequest(IdsmedIndividualUserRequest request) {
        if (request == null) {
            return false;
        }
        //Check user request
        if (StringUtils.isEmpty(request.getEmail()) || StringUtils.isEmpty(request.getTitle())
                || StringUtils.isEmpty(request.getIdCardNumber())
                || StringUtils.isEmpty(request.getFirstName())
                || StringUtils.isEmpty(request.getLastName())
                || StringUtils.isEmpty(request.getMobilePhoneNumber())
                || request.getGender() == null
				|| StringUtils.isEmpty(request.getFirstAddress())
				|| request.getCountryId() == null
				|| request.getProvinceId() == null) {
            return false;
        }

        return true;
    }

    public IdsmedIndividualUserResponse getIndividualUserById(Long id, String langCode) throws IdsmedBaseException {
        String ossBucketDomain = env.getProperty(PropertiesLabelConst.OSS_BUCKET_DOMAIN);
        Optional<IdsmedUser> userOpt = idsmedUserRepository.findById(id);
        if (!userOpt.isPresent()) {
            throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.USER_NOT_FOUND_ERROR, LangHelper.getLangCode(langCode)));
        }
        IdsmedUser user = userOpt.get();
        if (!user.getTypeOfUser().equals(UserTypeEnum.INDIVIDUAL_USER.getCode())) {
            throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.USER_NOT_FOUND_ERROR, LangHelper.getLangCode(langCode)));
        }
        List<UserAddress> userAddresses = userAddressRepository.findByIdsmedUserAndStatus(user, EntitySimpleStatusEnum.ACTIVE.getCode());
        IdsmedIndividualUserResponse response = new IdsmedIndividualUserResponse(user, ossBucketDomain, user.getUserAddresses());

        return response;
    }

    @Override
    @Transactional
    public IdsmedIndividualUserResponse editIndividuallUser(IdsmedIndividualUserRequest request, MultipartFile profileImage, MultipartFile frontIdImage, MultipartFile backIdImage) throws IdsmedBaseException {
        String ossBucketDomain = env.getProperty(PropertiesLabelConst.OSS_BUCKET_DOMAIN);

        if (!validateIndividualUserRegistrationRequest(request)) {
            throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.USER_REQUEST_INFO_NOT_CORRECT, LangHelper.getLangCode(request.getLangCode())));
        }

        Optional<IdsmedUser> individualUSerOpt = idsmedUserRepository.findById(request.getId());
        if (!individualUSerOpt.isPresent()) {
            throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.USER_NOT_FOUND_ERROR, LangHelper.getLangCode(request.getLangCode())));
        }

        IdsmedUser individualUser = individualUSerOpt.get();

        individualUser = individualUser.updateRegistrationUser(request);
        individualUser = uploadAndSetImagesForHospitalAndCorporateUser(individualUser, profileImage, frontIdImage, backIdImage, request.getLangCode());
        individualUser = idsmedUserRepository.save(individualUser);

        List<UserAddress> userAddresses = individualUser.getUserAddresses();
        Optional<UserAddress> userAddressOpt = userAddresses.stream().findFirst();
        if (!userAddressOpt.isPresent()) {
            throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.USER_HAS_NOT_BEEN_SET_ADDRESS_ERROR, LangHelper.getLangCode(request.getLangCode())));
        }

        UserAddress userAddress = userAddressOpt.get();

        Optional<Country> newCountryOpt = countryRepository.findById(request.getCountryId());
        Optional<Province> newProvinceOpt = provinceRepository.findById(request.getProvinceId());
        if (!newCountryOpt.isPresent()) {
            throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.COUNTRY_CAN_NOT_FOUND_ERROR, LangHelper.getLangCode(request.getLangCode())));
        }
        if (!newProvinceOpt.isPresent()) {
            throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.PROVINCE_CAN_NOT_FOUND_ERROR, LangHelper.getLangCode(request.getLangCode())));
        }

        Country newCountry = newCountryOpt.get();
        Province newProvince = newProvinceOpt.get();
        if(!newCountry.getCountryCode().equals(userAddress.getCountry().getCountryCode())) {
            userAddress.setCountry(newCountry);
        }
        if (!newProvince.getProvinceCode().equals(userAddress.getProvince().getProvinceCode())) {
            userAddress.setProvince(newProvince);
        }

        userAddress.setPostcode(request.getPostcode());
        userAddress.setFirstAddress(request.getFirstAddress());
        userAddress.setSecondAddress(request.getSecondAddress());

        userAddress = userAddressRepository.save(userAddress);
        List<UserAddress> userAddressList = new ArrayList<>();
        userAddressList.add(userAddress);

        IdsmedIndividualUserResponse response = new IdsmedIndividualUserResponse(individualUser, ossBucketDomain, userAddressList);

        return response;
    }

}

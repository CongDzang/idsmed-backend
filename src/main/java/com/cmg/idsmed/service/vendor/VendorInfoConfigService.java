package com.cmg.idsmed.service.vendor;

import com.cmg.idsmed.common.exception.IdsmedBaseException;
import com.cmg.idsmed.dto.vendor.VendorInfoConfigListResponse;
import com.cmg.idsmed.dto.vendor.VendorInfoConfigRequest;
import com.cmg.idsmed.dto.vendor.VendorInfoConfigResponse;
import com.cmg.idsmed.dto.vendor.VendorLabelConfigResponse;

import java.util.List;

public interface VendorInfoConfigService {
	VendorInfoConfigListResponse getAllVendorConfig(Long countryId
			, Long provinceId
			//, Integer location
			, Integer vendorType
			, Integer status
			, Integer pageIndex
			, Integer pageSize) throws IdsmedBaseException;

	List<VendorLabelConfigResponse> getVendorLabelConfigs(Long countryId, Long provinceId /*Integer location*/, Integer vendorType, Integer status, String langCode) throws IdsmedBaseException;

	VendorInfoConfigResponse create(VendorInfoConfigRequest request) throws IdsmedBaseException;

	VendorInfoConfigResponse getVendorInfoConfigById(Long id, String langCode) throws IdsmedBaseException;

	VendorInfoConfigResponse edit(VendorInfoConfigRequest request) throws IdsmedBaseException;
}

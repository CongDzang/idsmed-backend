package com.cmg.idsmed.service.vendor;

import com.cmg.idsmed.common.Constant.LoggerConstant;
import com.cmg.idsmed.common.exception.ErrorInfo;
import com.cmg.idsmed.common.exception.IdsmedBaseException;
import com.cmg.idsmed.common.utils.LangHelper;
import com.cmg.idsmed.dto.masterdata.CountryProvincePairRequest;
import com.cmg.idsmed.dto.product.ProductListResponse;
import com.cmg.idsmed.dto.vendor.*;
import com.cmg.idsmed.model.entity.auth.IdsmedAccount;
import com.cmg.idsmed.model.entity.masterdata.Country;
import com.cmg.idsmed.model.entity.masterdata.Province;
import com.cmg.idsmed.model.entity.vendor.config.VendorInfoConfig;
import com.cmg.idsmed.model.entity.vendor.config.VendorLabelConfig;
import com.cmg.idsmed.model.repo.auth.IdsmedAccountRepository;
import com.cmg.idsmed.model.repo.masterdata.CountryRepository;
import com.cmg.idsmed.model.repo.masterdata.ProvinceRepository;
import com.cmg.idsmed.model.repo.vendor.VendorInfoConfigRepository;
import com.cmg.idsmed.model.repo.vendor.VendorLabelConfigRepository;
import com.cmg.idsmed.service.share.MessageResourceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Configuration
@Service
public class VendorInfoConfigServiceImpl implements VendorInfoConfigService {
	private static final Logger logger = LoggerFactory.getLogger(VendorInfoConfigServiceImpl.class);

	@Autowired
	private VendorInfoConfigRepository vendorInfoConfigRepository;

	@Autowired
	private CountryRepository countryRepository;

	@Autowired
	private ProvinceRepository provinceRepository;

	@Autowired
	private MessageResourceService messageResourceService;

	@Autowired
	private VendorLabelConfigRepository vendorLabelConfigRepository;

	@Autowired
	private IdsmedAccountRepository idsmedAccountRepository;

	@Autowired
	private Environment env;

	@Value("${logger.start.method.description}")
	String START_METHOD_DESCRIPTION;

	@Value("${logger.end.method.description}")
	String END_METHOD_DESCRIPTION;
	private static final String GET_ALL_VENDOR_CONFIG_METHOD_NAME = "getAllVendorConfig";
	private static final String GET_VENDOR_INFO_CONFIG_BY_ID_METHOD_NAME = "getVendorInfoConfigById";
	private static final String CREATE_VENDOR_INFO_CONFIG_METHOD_NAME = "createVendorInfoConfig";
	private static final String EDIT_VENDOR_INFO_CONFIG_METHOD_NAME = "editVendorInfoConfig";
	private static final String GET_VENDOR_LABEL_CONFIGS_METHOD_NAME = "getVendorLabelConfigs";

	@Override
	public VendorInfoConfigListResponse getAllVendorConfig(Long countryId
			, Long provinceId /*Integer location*/, Integer vendorType
			, Integer status
			, Integer pageIndex
			, Integer pageSize) throws IdsmedBaseException {
		loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, GET_ALL_VENDOR_CONFIG_METHOD_NAME, null);

		VendorInfoConfigListResponse configListResponse = new VendorInfoConfigListResponse();

		if (countryId == null && provinceId == null) {
			if(vendorType == null) {
				configListResponse = getAllVendorInfoConfig(status, pageIndex, pageSize);
				return configListResponse;
			}

			if(vendorType != null){
				configListResponse = getAllVendorInfoConfigByVendorType(vendorType, status, pageIndex, pageSize);
				return configListResponse;
			}
		}

		configListResponse = getVendofInfoConfigByCountryAndProvince(countryId, provinceId /*location */, vendorType, status, pageIndex, pageSize);
		loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, GET_ALL_VENDOR_CONFIG_METHOD_NAME, null);
		return configListResponse;
	}

	@Override
	public VendorInfoConfigResponse getVendorInfoConfigById(Long id, String langCode) throws IdsmedBaseException {
		loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, GET_VENDOR_INFO_CONFIG_BY_ID_METHOD_NAME, null);
        VendorInfoConfig config = vendorInfoConfigRepository.getOne(id);
		if (config == null) {
			throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.VENDOR_INFO_CONFIG_NOT_FOUND_ERROR, LangHelper.getLangCode(langCode)));
		}
		loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, GET_VENDOR_INFO_CONFIG_BY_ID_METHOD_NAME, null);
        return new VendorInfoConfigResponse(config);
	}

	@Override
	@Transactional
	public VendorInfoConfigResponse create(VendorInfoConfigRequest request) throws IdsmedBaseException {
		loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, CREATE_VENDOR_INFO_CONFIG_METHOD_NAME, request.getLoginId());
		VendorInfoConfig vendorInfoConfig = new VendorInfoConfig();
		Long requesterId = idsmedAccountRepository.findByLoginId(request.getLoginId()).getId();
		Country country  = countryRepository.getOne(request.getCountryId());
		if (country == null) {
			throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.COUNTRY_CAN_NOT_FOUND_ERROR
					, LangHelper.getLangCode(request.getLangCode())));
		}

		if(CollectionUtils.isEmpty(request.getVendorLabelConfigs())){
			throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.VENDOR_LABEL_CONFIG_IS_EMPTY_ERROR
					, LangHelper.getLangCode(request.getLangCode())));
		}
		Province province = null;
		if (request.getProvinceId() != null) {
			province = provinceRepository.getOne(request.getProvinceId());
		}

		Integer location = request.getLocation();
		Integer vendorType = request.getVendorType();
		if (province == null) {
			if (checkVendorInfoConfigExistsByCountry(country, location, vendorType)) {
				throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.CONFIG_EXISTS_ERROR, LangHelper.getLangCode(request.getLangCode())));
			}
		} else {
			if (checkVendorInfoConfigExistsByCountryAndProvince(country, province, location, vendorType)) {
				throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.CONFIG_EXISTS_ERROR, LangHelper.getLangCode(request.getLangCode())));
			}
		}

		vendorInfoConfig.setCountry(country);

		vendorInfoConfig.setProvince(province);
		vendorInfoConfig.setLocation(request.getLocation());
		vendorInfoConfig.setVendorType(request.getVendorType());
		
		vendorInfoConfig.setCreatedBy(requesterId);
		vendorInfoConfig.setUpdatedBy(requesterId);
		vendorInfoConfig.setStatus(VendorInfoConfig.VendorInfoConfigStatusEnum.ACTIVE.getCode());

		vendorInfoConfig = vendorInfoConfigRepository.save(vendorInfoConfig);

		List<VendorLabelConfigRequest> vendorLabelConfigRequests = request.getVendorLabelConfigs();

		List<VendorLabelConfig> vendorLabelConfigs = new ArrayList<>();
		if (!CollectionUtils.isEmpty(vendorLabelConfigRequests)) {
			for (VendorLabelConfigRequest labelConfigRequest : vendorLabelConfigRequests) {
				VendorLabelConfig labelConfig = new VendorLabelConfig(labelConfigRequest);
				labelConfig.setVendorInfoConfig(vendorInfoConfig);
				labelConfig.setStatus(labelConfigRequest.getStatus());
				labelConfig.setCreatedBy(requesterId);
				labelConfig.setUpdatedBy(requesterId);
				labelConfig.setSequence(labelConfigRequest.getSequence());
				vendorLabelConfigs.add(labelConfig);
			}
		}

		if (!CollectionUtils.isEmpty(vendorLabelConfigs)) {
			vendorLabelConfigs = vendorLabelConfigRepository.saveAll(vendorLabelConfigs);
			vendorInfoConfig.setVendorLabelConfigs(vendorLabelConfigs);
		}
		loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, CREATE_VENDOR_INFO_CONFIG_METHOD_NAME, request.getLoginId());
		return new VendorInfoConfigResponse(vendorInfoConfig);
	}

	private Boolean checkVendorInfoConfigExistsByCountry(Country country, Integer location, Integer vedorType) {
		return vendorInfoConfigRepository.findFirstByCountryAndProvinceIsAndLocationAndVendorType(country, null, location, vedorType) != null;
	}

	private Boolean checkVendorInfoConfigExistsByCountryAndProvince(Country country, Province province, Integer location, Integer vedorType) {
		return vendorInfoConfigRepository.findFirstByCountryAndProvinceAndLocationAndVendorType(country, province, location, vedorType) != null;
	}

	@Override
	@Transactional
	public VendorInfoConfigResponse edit(VendorInfoConfigRequest request) throws IdsmedBaseException {
		loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, EDIT_VENDOR_INFO_CONFIG_METHOD_NAME, request.getLoginId());
		VendorInfoConfig vendorInfoConfig = vendorInfoConfigRepository.getOne(request.getId());

		Long requesterId = idsmedAccountRepository.findByLoginId(request.getLoginId()).getId();

		if (vendorInfoConfig == null) {
			throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.VENDOR_INFO_CONFIG_NOT_FOUND_ERROR, request.getLangCode()));
		}
		List<VendorLabelConfigRequest> labelConfigRequests = request.getVendorLabelConfigs();
		if (CollectionUtils.isEmpty(labelConfigRequests)) {

		}

		//Get list of existing vendor label config send along with request.
		List<VendorLabelConfig> existLabelConfigs = labelConfigRequests
				.stream()
				.filter(lbcr -> lbcr.getId() != null)
				.map(r -> {
					VendorLabelConfig labelConfig = vendorLabelConfigRepository.getOne(r.getId());
					labelConfig.setLabelPrimaryName(r.getLabelPrimaryName());
					labelConfig.setLabelSecondaryName(r.getLabelSecondaryName());
					labelConfig.setUpdatedBy(requesterId);
					labelConfig.setSequence(r.getSequence());
					labelConfig.setMandatory(r.getMandatory());
					labelConfig.setExpired(r.getExpired());
					labelConfig.setStatus(r.getStatus());
					return labelConfig;
				})
		.collect(Collectors.toList());


		//Get list of new Vendor label config
		List<VendorLabelConfig> newLabelConfigs = labelConfigRequests
				.stream()
				.filter(lbcr -> lbcr.getId() == null)
				.map(r -> {
					VendorLabelConfig labelConfig = new VendorLabelConfig(r);
					labelConfig.setVendorInfoConfig(vendorInfoConfig);
					labelConfig.setStatus(r.getStatus());
					labelConfig.setCreatedBy(requesterId);
					labelConfig.setUpdatedBy(requesterId);
					labelConfig.setSequence(r.getSequence());
					return labelConfig;
				})
				.collect(Collectors.toList());

		existLabelConfigs.addAll(newLabelConfigs);

		existLabelConfigs =vendorLabelConfigRepository.saveAll(existLabelConfigs);

		vendorInfoConfig.setVendorLabelConfigs(existLabelConfigs);

		loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, EDIT_VENDOR_INFO_CONFIG_METHOD_NAME, request.getLoginId());
		return new VendorInfoConfigResponse(vendorInfoConfig);
	}

	@Override
	public List<VendorLabelConfigResponse> getVendorLabelConfigs(Long countryId, Long provinceId /*Integer location*/, Integer vendorType, Integer status, String langCode) throws IdsmedBaseException {
		loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, GET_VENDOR_LABEL_CONFIGS_METHOD_NAME, null);
		Country country = countryRepository.getOne(countryId);
		if (country == null) {
			throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.COUNTRY_CAN_NOT_FOUND_ERROR, LangHelper.getLangCode(langCode)));
		}

		Province province = null;
		if (provinceId != null) {
			province = provinceRepository.getOne(provinceId);
		}

		List<VendorLabelConfig>  vendorLabelConfigs = vendorLabelConfigRepository.findAllVendorLabelConfigByCountryAndProvince(
				country, province /*location*/, vendorType, status);

		List<VendorLabelConfigResponse> vendorLabelConfigResponses = vendorLabelConfigs
				.stream()
				.map(vlc -> new VendorLabelConfigResponse(vlc))
				.collect(Collectors.toList());

		loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, GET_VENDOR_LABEL_CONFIGS_METHOD_NAME, null);
		return vendorLabelConfigResponses;
	}

	private VendorInfoConfigListResponse getAllVendorInfoConfig(Integer status, Integer pageIndex, Integer pageSize) {
		Pageable pageable = null;
		if (pageIndex != null && pageSize != null) {
			pageIndex = pageIndex - 1;
			pageable = PageRequest.of(pageIndex, pageSize);
		}

		VendorInfoConfigListResponse configListResponse = new VendorInfoConfigListResponse();

		Pair<Integer, List<VendorInfoConfig>> resultPair = vendorInfoConfigRepository.findAllVendorInfoConfig(status, pageable);
		List<VendorInfoConfigResponse> vendorInfoConfigResponses = resultPair.getSecond()
				.stream()
				.map(vic -> new VendorInfoConfigResponse(vic))
				.collect(Collectors.toList());
		if (pageIndex != null && pageSize != null) {
			configListResponse = new VendorInfoConfigListResponse(resultPair.getFirst(), pageIndex, pageSize, vendorInfoConfigResponses);
		} else {
			configListResponse = new VendorInfoConfigListResponse(resultPair.getFirst(), vendorInfoConfigResponses);
		}

		return configListResponse;
	}

	private VendorInfoConfigListResponse getAllVendorInfoConfigByVendorType(Integer vendorType, Integer status, Integer pageIndex, Integer pageSize) {
		Pageable pageable = null;
		if (pageIndex != null && pageSize != null) {
			pageIndex = pageIndex - 1;
			pageable = PageRequest.of(pageIndex, pageSize);
		}

		VendorInfoConfigListResponse configListResponse = new VendorInfoConfigListResponse();

		Pair<Integer, List<VendorInfoConfig>> resultPair = vendorInfoConfigRepository.findAllVendorInfoConfigByVendorType(vendorType, status, pageable);
		List<VendorInfoConfigResponse> vendorInfoConfigResponses = resultPair.getSecond()
				.stream()
				.map(vic -> new VendorInfoConfigResponse(vic))
				.collect(Collectors.toList());
		if (pageIndex != null && pageSize != null) {
			configListResponse = new VendorInfoConfigListResponse(resultPair.getFirst(), pageIndex, pageSize, vendorInfoConfigResponses);
		} else {
			configListResponse = new VendorInfoConfigListResponse(resultPair.getFirst(), vendorInfoConfigResponses);
		}

		return configListResponse;
	}

	private VendorInfoConfigListResponse getVendofInfoConfigByCountryAndProvince(Long countryId
			, Long provinceId
			//, Integer location
			, Integer vendorType
			, Integer status
			, Integer pageIndex
			, Integer pageSize) {

		VendorInfoConfigListResponse configListResponse = new VendorInfoConfigListResponse();

		Optional<Country> countryOpt = countryRepository.findById(countryId);
		if (!countryOpt.isPresent()) {
			logger.error("Can't find country with Id: {}", countryId);
			return configListResponse;
		}

		Country country = countryOpt.get();

		Province province = null;
		if (provinceId != null) {
			Optional<Province> provinceOpt = provinceRepository.findById(provinceId);
			if (provinceOpt.isPresent())
				province = provinceOpt.get();
		}

		Pageable pageable = null;
		if (pageIndex != null && pageSize != null) {
			pageIndex = pageIndex - 1;
			pageable = PageRequest.of(pageIndex, pageSize);
		}

		Pair<Integer, List<VendorInfoConfig>> resultPair =
						vendorInfoConfigRepository.findAllVendorInfoConfigByCountryAndProvince(country, province /*location*/, vendorType, status, pageable);

		List<VendorInfoConfigResponse> vendorInfoConfigResponses = resultPair.getSecond()
				.stream()
				.map(vic -> new VendorInfoConfigResponse(vic))
				.collect(Collectors.toList());
		if (pageIndex != null && pageSize != null) {
			configListResponse = new VendorInfoConfigListResponse(resultPair.getFirst(), pageIndex, pageSize, vendorInfoConfigResponses);
		} else {
			configListResponse = new VendorInfoConfigListResponse(resultPair.getFirst(), vendorInfoConfigResponses);
		}

		return configListResponse;
	}
	private void loggerForAllMethodInThisService(String methodDescription, String methodName, String loginId){
		logger.info(methodDescription, methodName, loginId != null?loginId:null);
	}
}

package com.cmg.idsmed.service.vendor;

import java.util.List;

import com.cmg.idsmed.dto.vendor.*;
import org.springframework.web.multipart.MultipartFile;

import com.cmg.idsmed.common.exception.IdsmedBaseException;
import com.cmg.idsmed.dto.masterdata.CompanyBackgroundInfoResponse;

public interface VendorService {
    VendorListResponse getVendorList(String companyNamePrimary, Integer status, Integer vendorCoordinator, Integer pageIndex, Integer pageSize) throws IdsmedBaseException;
	VendorResponse getVendorById(Long id) throws IdsmedBaseException;
	VendorResponse create(VendorRequest request, MultipartFile companyLogo, List<MultipartFile> companyDocuments) throws IdsmedBaseException;
	VendorResponse edit(VendorRequest request, MultipartFile companyLogo, List<MultipartFile> companyDocuments) throws IdsmedBaseException;
	VendorResponse updatePendingApproveStatus(VendorRequest request) throws IdsmedBaseException;
	VendorResponse updateApproveStatus(VendorRequest request) throws IdsmedBaseException;
	VendorResponse updateRejectStatus(VendorRequest request) throws IdsmedBaseException;
	CompanyBackgroundInfoResponse getComapanyBackgroundInfo (String companyCode) throws IdsmedBaseException;
	VendorResponse getReRegisterVendorById(Long id, String langCode) throws IdsmedBaseException;
	List<FileAttachmentResponse> editVendorFileAttachment(VendorFileAttachmentsEditRequest request, List<MultipartFile> uploadFiles) throws IdsmedBaseException;
	VendorResponse setVendorStatus(VendorSetStatusRequest request) throws IdsmedBaseException;
	VendorResponse resumeDeactiveVendor(VendorSetStatusRequest request) throws IdsmedBaseException;
	VendorResponse getVendorCoordinatorByCompanyCode(String companyCode, String langCode) throws IdsmedBaseException;

}

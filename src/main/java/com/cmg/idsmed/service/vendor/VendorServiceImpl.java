package com.cmg.idsmed.service.vendor;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

import com.cmg.idsmed.common.Constant.IdsmedPermissionCodeConst;
import com.cmg.idsmed.common.Constant.IdsmedRoleCodeConst;
import com.cmg.idsmed.common.Constant.PropertiesLabelConst;
import com.cmg.idsmed.common.enums.*;
import com.cmg.idsmed.dto.account.IdsmedAccountResponse;
import com.cmg.idsmed.dto.vendor.*;
import com.cmg.idsmed.mail.*;
import com.cmg.idsmed.model.entity.product.Product;
import com.cmg.idsmed.model.entity.vendor.*;
import com.cmg.idsmed.model.repo.product.ProductRepository;
import com.cmg.idsmed.model.repo.user.IdsmedUserRepository;
import com.cmg.idsmed.model.repo.vendor.*;
import com.cmg.idsmed.service.task.IdsmedTaskService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.util.Pair;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.cmg.idsmed.common.exception.ErrorInfo;
import com.cmg.idsmed.common.exception.IdsmedBaseException;
import com.cmg.idsmed.common.utils.DateTimeHelper;
import com.cmg.idsmed.common.utils.LangHelper;
import com.cmg.idsmed.config.rabbitmq.IdsmedQueueConfig;
import com.cmg.idsmed.dto.masterdata.CompanyBackgroundInfoResponse;
import com.cmg.idsmed.model.entity.auth.IdsmedAccount;
import com.cmg.idsmed.model.entity.auth.IdsmedRole;
import com.cmg.idsmed.model.entity.auth.IdsmedUser;
import com.cmg.idsmed.model.entity.auth.IdsmedUserRole;
import com.cmg.idsmed.model.entity.masterdata.CompanyBackgroundInfo;
import com.cmg.idsmed.model.entity.masterdata.Country;
import com.cmg.idsmed.model.entity.masterdata.Province;
import com.cmg.idsmed.model.entity.vendor.config.VendorLabelConfig;
import com.cmg.idsmed.model.repo.auth.IdsmedAccountRepository;
import com.cmg.idsmed.model.repo.auth.IdsmedRoleRepository;
import com.cmg.idsmed.model.repo.auth.IdsmedUserRoleRepository;
import com.cmg.idsmed.model.repo.masterdata.CompanyBackgroundInfoRepository;
import com.cmg.idsmed.model.repo.masterdata.CountryRepository;
import com.cmg.idsmed.model.repo.masterdata.ProvinceRepository;
import com.cmg.idsmed.service.auth.AuthenticationService;
import com.cmg.idsmed.service.share.FileUploadServiceImpl;
import com.cmg.idsmed.service.share.MessageResourceService;
import com.cmg.idsmed.service.share.NotificationService;

@Configuration
@Service
public class VendorServiceImpl implements VendorService {

	private static final Logger logger = LoggerFactory.getLogger(VendorServiceImpl.class);

	@Autowired
	private VendorRepository vendorRepository;

	@Autowired
	private MessageResourceService messageResourceService;

	@Autowired
	private FileUploadServiceImpl fileUploadService;

	@Autowired
	private FileAttachmentRepository fileAttachmentRepository;

	@Autowired
	private VendorRejectRepository vendorRejectRepository;

	@Autowired
	private CompanyBackgroundInfoRepository companyBackgroundInfoRepository;

	@Autowired
	private IdsmedUserRepository idsmedUserRepository;

	@Autowired
	private RabbitTemplate rabbitTemplate;

	@Autowired
	private Environment env;

	@Autowired
	private VendorAdditionalInfoRepository vendorAdditionalInfoRepository;

	@Autowired
	private VendorLabelConfigRepository labelConfigRepository;

	@Autowired
	private CountryRepository countryRepository;

	@Autowired
	private ProvinceRepository provinceRepository;

	@Autowired
	private VendorAddressRepository vendorAddressRepository;

	@Autowired
	private IdsmedAccountRepository idsmedAccountRepository;

	@Autowired
	private EmailService emailService;

	@Autowired
	private NotificationService notificationService;

	@Autowired
	private AuthenticationService authenticationService;

	@Autowired
	private IdsmedUserRoleRepository idsmedUserRoleRepository;

	@Autowired
	private IdsmedRoleRepository idsmedRoleRepository;

	@Autowired
	private ProductRepository productRepository;

	@Autowired
	private VendorActionLogRepository vendorActionLogRepository;

	@Autowired
	private VendorFileAttachmentReminderRespository vendorFileAttachmentReminderRespository;

	@Autowired
	private IdsmedTaskService taskService;

	private static final Integer LENGTH_OF_COMPANY_CODE = 8;

	private static final String VENDOR_FILE_UPLOAD_BASE_URL_NAME = "image.vendor.file.base.url";

	private static final String STORAGE_TYPE_PROPERTY_NAME = "storage.type";

	private static final String SERVER_STORAGE_TYPE = "server";

	private static final String VENDOR_RE_REGISTER_BASE_LINK_LABEL = "mail.vendor.register.resubmit.base.link";
	private static final String ADMIN_ROLE_CODE = "001";

	private static final Integer VENDOR_RE_REGISTER_LINK_VALID_PERIOD = 7;

	@Value("${logger.start.method.description}")
	String START_METHOD_DESCRIPTION;

	@Value("${logger.end.method.description}")
	String END_METHOD_DESCRIPTION;
	private static final String GET_VENDOR_LIST_METHOD_NAME = "getVendorList";
	private static final String GET_VENDOR_BY_ID_METHOD_NAME = "getVendorById";
	private static final String GET_REREGISTER_VENDOR_BY_ID_METHOD_NAME = "getReRegisterVendorById";
	private static final String CREATE_VENDOR_METHOD_NAME = "createVendor";
	private static final String UPDATE_PENDING_APPROVE_STATUS_METHOD_NAME = "updatePendingApproveStatus";
	private static final String UPDATE_APPROVED_STATUS_METHOD_NAME = "updateApproveStatus";
	private static final String UPDATE_REJECT_STATUS_METHOD_NAME = "updateRejectStatus";
	private static final String EDIT_VENDOR_METHOD_NAME = "editVendor";
	private static final String EDIT_VENDOR_FILE_ATTACHMENT_METHOD_NAME = "editVendorFileAttachment";
	private static final String GET_COMAPANY_BACKGROUND_INFO_METHOD_NAME = "getComapanyBackgroundInfo";
	private static final String RESUME_DEACTIVE_VENDOR_METHOD_NAME = "resumeDeactiveVendor";
	private static final String SET_VENDOR_STATUS_METHOD_NAME = "setVendorStatus";
	private static final String GET_VENDOR_COORDINATOR_BY_COMPANY_CODE = "getVendorCoordinatorByCompanyCode";

	@Override
	//TODO : Need to check if view Vendor is correct or should be List Vendor in idsmed_permission
	@PreAuthorize("@PermissionService.isMethodPermited(authentication, {'VP0000001'})") //12 = View Vendor API - in idsmed_permission
	public VendorListResponse getVendorList(String companyNamePrimary, Integer status, Integer vendorCoordinator, Integer pageIndex, Integer pageSize) throws IdsmedBaseException {
		loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, GET_VENDOR_LIST_METHOD_NAME, null);
		String ossBucketDomain = env.getProperty(PropertiesLabelConst.OSS_BUCKET_DOMAIN);
		Pair<Integer, List<Vendor>> vendors = vendorRepository.searchVendorByCustomCondition(companyNamePrimary, status, vendorCoordinator, pageIndex, pageSize);
		VendorListResponse vendorListResponse = new VendorListResponse(pageIndex, pageSize);

		if(CollectionUtils.isEmpty(vendors.getSecond())) {
			return vendorListResponse;
		}
		int vendorCount = vendors.getFirst();

		List<VendorResponse> vendorResponses = vendors.getSecond()
				.stream()
				.map(v -> {
					IdsmedAccountResponse idsmedAccountResponse = null;
					if(v.getVendorCoordinator() != null){
						Optional<IdsmedAccount> idsmedAccountOpt = idsmedAccountRepository.findById(v.getVendorCoordinator());
						IdsmedAccount idsmedAccount = idsmedAccountOpt.get();
						IdsmedUser idsmedUser = idsmedAccount.getIdsmedUser();
						idsmedAccountResponse = new IdsmedAccountResponse(idsmedAccount, idsmedUser);
					}

					VendorResponse vendorResponse = new VendorResponse(v, ossBucketDomain);
					vendorResponse.setIdsmedAccountResponse(idsmedAccountResponse);
					return vendorResponse;

				})
				.collect(Collectors.toList());

		if(pageIndex != null && pageSize != null) {
			vendorListResponse = new VendorListResponse(vendorCount, pageIndex, pageSize, vendorResponses);
		} else {
			vendorListResponse = new VendorListResponse(vendorCount, vendorResponses);
		}

		loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, GET_VENDOR_LIST_METHOD_NAME, null);
		return vendorListResponse;
	}

	@Override
	@PreAuthorize("@PermissionService.isMethodPermited(authentication, {'VP0000001', 'VPC0000001'})")
	public VendorResponse getVendorById(Long id) throws IdsmedBaseException {
		loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, GET_VENDOR_BY_ID_METHOD_NAME, null);
		Optional<Vendor> vendorOpt = vendorRepository.findById(id);
		String ossBucketDomain = env.getProperty(PropertiesLabelConst.OSS_BUCKET_DOMAIN);
		Vendor vendor = vendorOpt.get();
		if(!vendorOpt.isPresent()) {
			throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.VENDOR_NOT_FOUND_ERROR, "en"));
		}
		VendorResponse vendorResponse = new VendorResponse(vendor, ossBucketDomain);

		if(!CollectionUtils.isEmpty(vendor.getVendorRejectList())) {
			VendorReject vr = Collections.max(vendor.getVendorRejectList(), Comparator.comparing(VendorReject::getId));
			vendorResponse.setVendorRejectReason(vr.getRemarks());
		}

		IdsmedAccountResponse idsmedAccountResponse = null;
		if(vendor.getVendorCoordinator() != null){
			Optional<IdsmedAccount> idsmedAccountOpt = idsmedAccountRepository.findById(vendor.getVendorCoordinator());
			IdsmedAccount idsmedAccount = idsmedAccountOpt.get();
			IdsmedUser idsmedUser = idsmedAccount.getIdsmedUser();
			idsmedAccountResponse = new IdsmedAccountResponse(idsmedAccount, idsmedUser);
		}
		vendorResponse.setIdsmedAccountResponse(idsmedAccountResponse);
		loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, GET_VENDOR_BY_ID_METHOD_NAME, null);
		return vendorResponse;
	}

	@Override
	public VendorResponse getReRegisterVendorById(Long id, String langCode) throws IdsmedBaseException {
		loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, GET_REREGISTER_VENDOR_BY_ID_METHOD_NAME, null);
		String ossBucketDomain = env.getProperty(PropertiesLabelConst.OSS_BUCKET_DOMAIN);
		LocalDateTime currentDate = DateTimeHelper.getCurrentTimeUTC();
		LocalDateTime compareDate = DateTimeHelper.addTimeInUTC(currentDate, DateTimeHelper.DAY_OF_YEAR, -VENDOR_RE_REGISTER_LINK_VALID_PERIOD);
		Vendor vendor = vendorRepository.findVendorByStatusAndValidUpdatedDate(id, StatusEnum.REJECT.getCode(), compareDate);

		if (vendor == null) {
			throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.VENDOR_RE_REGISTER_NOT_FOUND_ERROR, LangHelper.getLangCode(langCode)));
		}

		Long coodinatorAccountId = vendor.getVendorCoordinator();
		IdsmedUser coodinatorUser = null;
		IdsmedAccount coodinatorAccount = null;
		if (coodinatorAccountId != null) {
			Optional<IdsmedAccount> coodinatorAccountOpt = idsmedAccountRepository.findById(coodinatorAccountId);
			if (coodinatorAccountOpt.isPresent()) {
				coodinatorAccount = coodinatorAccountOpt.get();
				coodinatorUser = coodinatorAccount.getIdsmedUser();
			}
		}
		VendorResponse vendorResponse = new VendorResponse(vendor, ossBucketDomain);
		if (coodinatorUser != null && coodinatorAccount != null) {
			IdsmedAccountResponse idsmedAccountResponse = new IdsmedAccountResponse(coodinatorAccount, coodinatorUser);
			vendorResponse.setIdsmedAccountResponse(idsmedAccountResponse);
		}

		if(!CollectionUtils.isEmpty(vendor.getVendorRejectList())) {
			VendorReject vr = Collections.max(vendor.getVendorRejectList(), Comparator.comparing(VendorReject::getId));
			vendorResponse.setVendorRejectReason(vr.getRemarks());
		}

		loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, GET_REREGISTER_VENDOR_BY_ID_METHOD_NAME, null);
		return vendorResponse;
	}

	@Override
	@Transactional
	public VendorResponse create(VendorRequest request, MultipartFile companyLogo, List<MultipartFile> companyDocuments) throws IdsmedBaseException {
		loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, CREATE_VENDOR_METHOD_NAME, request.getLoginId());
		String ossBucketDomain = env.getProperty(PropertiesLabelConst.OSS_BUCKET_DOMAIN);
		IdsmedAccount idsmedAccount = null;

		if(!StringUtils.isEmpty(request.getLoginId())){
			idsmedAccount = idsmedAccountRepository.findByLoginId(request.getLoginId());
		}

		final String STORAGE_TYPE = env.getProperty(STORAGE_TYPE_PROPERTY_NAME);

		if(!isExistedByCode(request.getStatus())) {
			throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.VENDOR_STATUS_NOT_VALID_ERROR, LangHelper.getLangCode(request.getLangCode())));
		}

		//Get user and related info for sending email and notification purpose
		IdsmedRole idsmedRole = idsmedRoleRepository.findByCode(ADMIN_ROLE_CODE);
		List<IdsmedUserRole> idsmedUserRoles = idsmedUserRoleRepository.findAllByIdsmedRole(idsmedRole);
		IdsmedUser adminUser = null;
		List<IdsmedUser> adminUserList = new ArrayList<>();

		List<String> adminRecipients = new ArrayList<>();
		for(IdsmedUserRole idsmedUserRole : idsmedUserRoles){
			adminUser = idsmedUserRole.getIdsmedUser();
			adminUserList.add(adminUser);
		}

		List<IdsmedAccount> adminAccountList = new ArrayList<>();
		for(IdsmedUser adminUsers : adminUserList){
			List<IdsmedAccount> accounts = adminUsers.getIdsmedAccounts().isEmpty() ? null : adminUser.getIdsmedAccounts();
			adminAccountList.addAll(accounts);
		}

		List<IdsmedUser> idsmedUsers = authenticationService.getUsersByPermissionCode(IdsmedPermissionCodeConst.VENDOR_REVIEW);
		List<IdsmedAccount> accountList = new ArrayList<>();
		for(IdsmedUser reviewVendorUser : idsmedUsers) {
			List<IdsmedAccount> accounts = reviewVendorUser.getIdsmedAccounts().isEmpty() ? null : reviewVendorUser.getIdsmedAccounts();
			accountList.addAll(accounts);
		}

		//===========================================================================

		Vendor vendor = new Vendor();
		String[] igroreProperties = {"id", "fileAttachments", "additionalInfos", "vendorAddressRequests", "countryCode", "accountNumber"};

		BeanUtils.copyProperties(request, vendor, igroreProperties);
		Map<String, String> fileUploadInfoMap = new HashMap<>();

		// Initialize base URL for company logo path
		String baseURL = "";

		if (companyLogo != null) {
			//Upload logo image and set url path.
			try {
				fileUploadInfoMap = fileUploadService.uploadImage(companyLogo, EntityType.VENDOR);
			}catch(IOException e) {
				throw new IdsmedBaseException(
						messageResourceService.getErrorInfo(ErrorInfo.UPLOAD_FILE_ERROR, LangHelper.getLangCode(request.getLangCode())));
			}

			baseURL = env.getProperty(VENDOR_FILE_UPLOAD_BASE_URL_NAME);
			String serverURL = StringUtils.isEmpty(baseURL) ? "" : baseURL + "/" + fileUploadInfoMap.get(FileUploadServiceImpl.HASH_FILE_NAME_KEY);
			String relativePath = fileUploadInfoMap.get(FileUploadServiceImpl.UPLOAD_URL);
			vendor.setCompanyLogoPath(STORAGE_TYPE.equalsIgnoreCase(SERVER_STORAGE_TYPE) ? serverURL : relativePath);
		}

		vendor.setStatus(request.getStatus());

		vendor.setCountryCode(LangHelper.getCountryCode(request.getCountryCode()));
		String companyCode  = generateCompanyCode();
		vendor.setCompanyCode(companyCode);

		//Set accountNumber for vendor
		vendor = setVendorAccountNumber(vendor);

		vendor.setCustGroup(Vendor.CUSTGROUP.TRAD.getName());
		vendor.setCurrency(Vendor.CURRENCY.CNY.getName());
		vendor.setVatGst(request.getVatGst());
		vendor.setBankAccountNumber(request.getAccountNumber());

		Vendor storedVendor = vendorRepository.save(vendor);

		//Set VendorAddress
		List<VendorAddress> addresses = setVendorAddresses(request.getVendorAddressRequests(), storedVendor);
		if (!CollectionUtils.isEmpty(addresses)) {
			vendorAddressRepository.saveAll(addresses);
		}

		//Set VendorAdditionalInfo
		List<VendorAdditionalInfo> infos = setVendorAdditionalInfo(request.getAdditionalInfos(), storedVendor);
		if (!CollectionUtils.isEmpty(infos)) {
			vendorAdditionalInfoRepository.saveAll(infos);
		}

		List<Pair<String, String>> recipients = new ArrayList<>();
		Pair<String, String> companyOwner = Pair.of(vendor.getCompanyNamePrimary(), vendor.getEmail());
		recipients.add(companyOwner);

		Pair<String, String> personInCharge1 = Pair.of(vendor.getPersonIncharge1Name(), vendor.getPersonIncharge1Email());
		recipients.add(personInCharge1);

		Pair<String, String> personInCharge2 = null;
		if (!StringUtils.isEmpty(vendor.getPersonIncharge2Email())) {
			personInCharge2 = Pair.of(vendor.getPersonIncharge2Name(), vendor.getPersonIncharge2Email());
			recipients.add(personInCharge2);
		}

		//Process FileAttachment. fileAttachmentRequests variable mapping between title of a vendor document and uploaded file name
		String countryCode = countryRepository.findById(request.getVendorAddressRequests().get(0).getCountryId()).get().getCountryCode();
		List<VendorFileAttachmentRequest> fileAttachmentRequests  = request.getFileAttachments();
		Pair<List<FileAttachment>, List<VendorFileAttachmentReminder>> attachmentPair = setFileAttachments(fileAttachmentRequests
				, companyDocuments
				, storedVendor
				, countryCode
				, baseURL
				, STORAGE_TYPE
				, request.getLangCode());

		if (!CollectionUtils.isEmpty(attachmentPair.getFirst())) {
			fileAttachmentRepository.saveAll(attachmentPair.getFirst());
		}

		if (!CollectionUtils.isEmpty(attachmentPair.getSecond())) {
			vendorFileAttachmentReminderRespository.saveAll(attachmentPair.getSecond());
		}

		//CREATE REVIEW TASK
		if(request.getStatus().equals(StatusEnum.PENDING_FOR_REVIEW.getCode())) {
			taskService.createVendorReviewTask(storedVendor);
			/**
			 * Send email & web notification to ADMIN & SCP USER with permission to review new vendor
			 */
			processEmailAndWebNotificationOfReviewNewVendorToAdmin(adminUserList, adminAccountList, vendor);

			processEmailAndWebNotificationOfReviewNewVendorToSCPUser(idsmedUsers, accountList, vendor, idsmedAccount, request);
		}

		//CREATE APPROVE TASK
		if(request.getStatus().equals(StatusEnum.PENDING_FOR_APPROVAL.getCode())){
			taskService.createVendorApproveTask(storedVendor);
			processEmailAndWebNotificationForVendorPendingApproval(vendor, "");
		}

		/**
		 * Send email notification to Vendor Owner, 1st Person and 2nd Person(if there is one)
		 */
		logger.info("Start send email vendor registration with name {} to queue with name", request.getCompanyNamePrimary());
		for (Pair<String, String> recipient : recipients) {
			Pair<String, String> emailPair = emailService.generatedVendorRegistrationEmailSubjectAndContent(
					recipient.getFirst(),
					vendor.getCompanyNamePrimary()
			);

			EmailObject email = new VendorRegistrationEmailObject(
					Arrays.asList(recipient.getSecond()),
					env.getProperty(PropertiesLabelConst.SMTP_SUPPORT_MAIL_LABEL),
					emailPair);

			rabbitTemplate.convertAndSend(env.getProperty(IdsmedQueueConfig.RABBIT_MAIL_SERVICE_PROCESS_QUEUE), email);
		}

		loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, CREATE_VENDOR_METHOD_NAME, request.getLoginId());
		return new VendorResponse(vendor, ossBucketDomain);
	}

	/**
	 * This method set account number for a vendor
	 * @param vendor
	 * @return
	 */
	private Vendor setVendorAccountNumber(Vendor vendor) {
		Long totalCount = vendorRepository.count();
		String accountNumber = "CNV" + (totalCount + 1);
		if (accountNumber.length() < 10) {
			for (int i = accountNumber.length(); i < 10; i++) {
				if (i < 10) {
					accountNumber = accountNumber.substring(0, 3) + "0" + accountNumber.substring(3, accountNumber.length()).trim();
				}
			}
		}

		vendor.setAccountNumber(accountNumber);
		return vendor;
	}

	private String generateCompanyCode() {
		Vendor vendorExisting = null;
		String companyCode = "";
		int retryCount = 0;
		int retryMax = 10;
		do {
			companyCode = randomAlphaNumeric(LENGTH_OF_COMPANY_CODE);
			vendorExisting = vendorRepository.findFirstByCompanyCode(companyCode);
			if(vendorExisting == null) {
				break;
			}
			logger.info("retry count is " + retryCount);
			retryCount++;
		} while(retryCount < retryMax);

		return companyCode;
	}

	private List<VendorAddress> setVendorAddresses(List<VendorAddressRequest> addressRequests, Vendor vendor) {
		List<VendorAddress> vendorAddresses = addressRequests
				.stream().map(vdr -> {
					Country country = null;
					if (vdr.getCountryId() != null) {
						country = countryRepository.getOne(vdr.getCountryId());
					}

					Province province = null;
					if (vdr.getProvinceId() != null) {
						province = provinceRepository.getOne(vdr.getProvinceId());
					}
					VendorAddress vendorAddress = new VendorAddress(vdr);
					vendorAddress.setVendor(vendor);
					vendorAddress.setCountry(country);
					vendorAddress.setProvince(province);
					vendorAddress.setFromDate(DateTimeHelper.getCurrentTimeUTC());
					vendorAddress.setStatus(EntitySimpleStatusEnum.ACTIVE.getCode());
					return vendorAddress;
				}).collect(Collectors.toList());
		return vendorAddresses;
	}

	private Pair<List<FileAttachment>, List<VendorFileAttachmentReminder>> setFileAttachments(List<VendorFileAttachmentRequest> fileAttachmentRequests
			, List<MultipartFile> files
			, Vendor vendor
			, String countryCode
			, String baseURL
			, String STORAGE_TYPE
			, String langCode) throws IdsmedBaseException {

		List<FileAttachment> fileAttachments = new ArrayList<FileAttachment>();
		List<VendorFileAttachmentReminder> reminders = new ArrayList<>();

		if (!CollectionUtils.isEmpty(fileAttachmentRequests) && !CollectionUtils.isEmpty(files)) {
			for (VendorFileAttachmentRequest fileAttachmentRequest : fileAttachmentRequests) {
				FileAttachment fileAttachment = new FileAttachment();
				fileAttachment.setVendor(vendor);
				fileAttachment.setCountryCode(countryCode);

				if (fileAttachmentRequest.getFileIndex() != null) {
					MultipartFile file  = files.get(fileAttachmentRequest.getFileIndex());
					fileAttachment = uploadFileAttachment(fileAttachment, file, baseURL, STORAGE_TYPE, langCode);
				}

				VendorLabelConfig labelConfig = labelConfigRepository.getOne(fileAttachmentRequest.getVendorLabelConfigId());
				fileAttachment.setDocumentName(fileAttachmentRequest.getDocumentName());
				fileAttachment.setDocumentSecondaryName(fileAttachmentRequest.getDocumentSecondaryName());
				fileAttachment.setOriginalFileName(fileAttachmentRequest.getOriginalFileName());
				fileAttachment.setExpiryDate(fileAttachmentRequest.getExpiryDate() != null ? DateTimeHelper.convertZonedDateTimeToUTC(fileAttachmentRequest.getExpiryDate()) : null);
				fileAttachment.setExpDateTimeZone(fileAttachmentRequest.getExpDateTimeZone());
				fileAttachment.setVerify(fileAttachmentRequest.getVerify());
				fileAttachment.setStatus(FileAttachmentStatusEnum.PENDING_FOR_REVIEW.getCode());
				fileAttachment.setVendorLabelConfig(labelConfig);

				List<VendorFileAttachmentReminderRequest> reminderRequests = fileAttachmentRequest.getVendorFileAttachmentReminders();
				if (!CollectionUtils.isEmpty(reminderRequests)) {
					for (VendorFileAttachmentReminderRequest reminderRequest: reminderRequests) {
						VendorFileAttachmentReminder reminder = new VendorFileAttachmentReminder(reminderRequest);
						reminder.setFileAttachment(fileAttachment);
						reminders.add(reminder);
					}
				}
				fileAttachments.add(fileAttachment);
			}

		}
		return Pair.of(fileAttachments, reminders);
	}

	private FileAttachment uploadFileAttachment(FileAttachment fileAttachment
			, MultipartFile file
			, String baseURL
			, String storageType
			, String langCode) throws IdsmedBaseException {
		if (file != null) {
			try {
				Map<String, String> fileUploadInfoMap = new HashMap<>();
				fileUploadInfoMap = fileUploadService.uploadImage(file, EntityType.VENDOR);
				String serverURL = StringUtils.isEmpty(baseURL) ? null : baseURL + "/" + fileUploadInfoMap.get(FileUploadServiceImpl.HASH_FILE_NAME_KEY);
				String relativePath = fileUploadInfoMap.get(FileUploadServiceImpl.UPLOAD_URL);
				fileAttachment.setDocumentPath(storageType.equalsIgnoreCase(SERVER_STORAGE_TYPE) ? serverURL : relativePath);
			} catch (IOException e) {
				throw new IdsmedBaseException(
						messageResourceService.getErrorInfo(ErrorInfo.UPLOAD_FILE_ERROR, LangHelper.getLangCode(langCode)));
			}
		}
		return fileAttachment;
	}

	@Modifying
	public Pair<List<FileAttachment>, List<VendorFileAttachmentReminder>> updateVendorFileAttachmentWithUploadFile(List<VendorFileAttachmentRequest> fileAttachmentRequests
			, List<MultipartFile> files
			, Vendor vendor
			, String countryCode
			, String baseURL
			, String STORAGE_TYPE
			, String langCode, String loginId) throws IdsmedBaseException {

		List<FileAttachment> fileAttachments = new ArrayList<>();
		List<VendorFileAttachmentReminder> reminders = new ArrayList<>();

		if (!CollectionUtils.isEmpty(fileAttachmentRequests)) {
			for (VendorFileAttachmentRequest fileAttachmentRequest : fileAttachmentRequests) {

				FileAttachment attachment = fileAttachmentRepository.getOne(fileAttachmentRequest.getId());
				attachment.setVendor(vendor);
				attachment.setCountryCode(countryCode);

				if (fileAttachmentRequest.getFileIndex() != null) {
					MultipartFile file  = files.get(fileAttachmentRequest.getFileIndex());
					attachment = uploadFileAttachment(attachment, file, baseURL, STORAGE_TYPE, langCode);
				}

				attachment.setCountryCode(countryCode);
				attachment.setDocumentName(fileAttachmentRequest.getDocumentName());
				attachment.setDocumentSecondaryName(fileAttachmentRequest.getDocumentSecondaryName());
				attachment.setOriginalFileName(fileAttachmentRequest.getOriginalFileName());
				attachment.setExpiryDate(fileAttachmentRequest.getExpiryDate() != null ? DateTimeHelper.convertZonedDateTimeToUTC(fileAttachmentRequest.getExpiryDate()): null);
				attachment.setExpDateTimeZone(fileAttachmentRequest.getExpDateTimeZone());
				attachment.setVerify(fileAttachmentRequest.getVerify());
				attachment.setStatus(fileAttachmentRequest.getStatus());

				//Delete existing reminders then create new from request
				List<VendorFileAttachmentReminder> existingReminders = attachment.getVendorFileAttachmentReminders();
				if (!CollectionUtils.isEmpty(existingReminders)) {
					vendorFileAttachmentReminderRespository.deleteAll(existingReminders);
				}

				List<VendorFileAttachmentReminderRequest> fileAttachmentReminderRequests = fileAttachmentRequest.getVendorFileAttachmentReminders();
				if (!CollectionUtils.isEmpty(fileAttachmentReminderRequests)) {
					for (VendorFileAttachmentReminderRequest r : fileAttachmentReminderRequests) {
						VendorFileAttachmentReminder reminder = new VendorFileAttachmentReminder(r);
						reminder.setFileAttachment(attachment);
						reminders.add(reminder);
					}
				}

				fileAttachments.add(attachment);
			}
		}

		// ------------------ EMAIL AND WEB NOTIFICATION REQUEST TO APPROVE VENDOR CERTIFICATE --------------------
		if (fileAttachmentRequests.get(0).getStatus() == FileAttachmentStatusEnum.PENDING_FOR_APPROVAL.getCode()) {
			Pair<String, String> emailNotificationPair = null;
			Long vendorId = fileAttachmentRequests.get(0).getVendorId();
			Vendor fileAttachmentVendor = vendorRepository.findById(vendorId).get();
			if (vendor != null) {
				IdsmedAccount vendorCoordinator = idsmedAccountRepository.findById(fileAttachmentVendor.getVendorCoordinator()).get();
				if (vendorCoordinator != null) {
					IdsmedUser vendorCoordinatorUser = idsmedUserRepository.findIdsmedUserByIdsmedAccounts(vendorCoordinator);
					if (vendorCoordinatorUser != null) {
						String attachmentDocumentName = fileAttachments
								.stream()
								.map(fa -> fa.getDocumentName() + "; ")
								.collect(Collectors.joining());

						if (vendorCoordinator != null) {
							emailNotificationPair = processEmailNotificationRequestToApproveVendorCertificate(vendorCoordinatorUser,
									attachmentDocumentName, FileAttachmentStatusEnum.PENDING_FOR_APPROVAL.getName());
						}

						IdsmedAccount idsmedAccount = idsmedAccountRepository.findByLoginId(loginId);
						processWebNotificationRequestToApproveVendorCertificate(
								Arrays.asList(idsmedAccount),
								Arrays.asList(vendorCoordinator),
								emailNotificationPair);
					}
				}
			}
		}

		return Pair.of(fileAttachments, reminders);
	}

	private List<VendorAdditionalInfo> setVendorAdditionalInfo(List<VendorAdditionalInfoRequest> infos, Vendor vendor) {

		List<VendorAdditionalInfo> additionalInfos = new ArrayList<>();
		if (!CollectionUtils.isEmpty(infos)) {
			for (VendorAdditionalInfoRequest additionalInfoRequest : infos) {
				VendorLabelConfig labelConfig = labelConfigRepository.getOne(additionalInfoRequest.getVendorLabelConfigId());
				VendorAdditionalInfo info = new VendorAdditionalInfo();
				info.setFieldName(additionalInfoRequest.getFieldName());
				info.setFieldSecondaryName(additionalInfoRequest.getFieldSecondaryName());
				info.setInfoValue(additionalInfoRequest.getInfoValue());
				info.setVendorLabelConfig(labelConfig);
				info.setVendor(vendor);
				additionalInfos.add(info);
			}
		}
		return additionalInfos;
	}


	public static String randomAlphaNumeric(int count) {
		String alphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		StringBuilder builder = new StringBuilder();

		while (count-- != 0) {

			int character = (int)(Math.random()*alphaNumericString.length());

			builder.append(alphaNumericString.charAt(character));

		}
		return builder.toString();
	}
	public boolean isExistedByCode(Integer enumCode) {
		boolean isExist = Arrays.stream(StatusEnum.values()).anyMatch((t)-> t.getCode().equals(enumCode));
		return isExist;
	}

	@Override
	@PreAuthorize("@PermissionService.isMethodPermited(authentication,{'VP0000004'})") //15 = Review Vendor API - in idsmed_permission
	public VendorResponse updatePendingApproveStatus(VendorRequest request)throws IdsmedBaseException {
		// TODO Auto-generated method stub
		String ossBucketDomain = env.getProperty(PropertiesLabelConst.OSS_BUCKET_DOMAIN);
		Long vendorId = request.getId();
		loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, UPDATE_PENDING_APPROVE_STATUS_METHOD_NAME, request.getLoginId());
		Optional<Vendor> storedVendor = vendorRepository.findById(vendorId);

		if(!storedVendor.isPresent()) {
			logger.error("The Vendor Id {} not existing", vendorId);
			throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.VENDOR_ID_NOT_EXISTED, LangHelper.getLangCode(request.getLangCode())));
		}

		Vendor vendor = storedVendor.get();
		//Find reviewer account
		IdsmedAccount reviewer = idsmedAccountRepository.findByLoginId(request.getLoginId());
		if (reviewer == null) {
			logger.error("Error in feching reviewer account from database by loginId {}", request.getLoginId());
			return null;
		}

		//Update list of FileAttachment without upload file.
		List<VendorFileAttachmentRequest> fileAttachmentRequests = request.getFileAttachments();
		if (!CollectionUtils.isEmpty(fileAttachmentRequests)) {
			List<FileAttachment> fileAttachments = updateVendorFileAttachmentWithoutUploadFile(fileAttachmentRequests
					, reviewer.getId());
		}

		vendor.setStatus(StatusEnum.PENDING_FOR_APPROVAL.getCode());
		vendor = vendorRepository.save(vendor);

		//UPDATE REVIEW TASK TO COMPLETED STATUS
		taskService.setCompletedVendorReviewTask(vendor, request.getLoginId());

		//CREATE APPROVE TASK
		taskService.createVendorApproveTask(vendor);

		processEmailAndWebNotificationForVendorPendingApproval(vendor, request.getLoginId());

		loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, UPDATE_PENDING_APPROVE_STATUS_METHOD_NAME, request.getLoginId());
		VendorResponse vendorResponse = new VendorResponse(storedVendor.get(), ossBucketDomain);
		return vendorResponse;
	}

	private void processEmailAndWebNotificationForVendorPendingApproval(Vendor vendor, String loginId) throws IdsmedBaseException {

		List<IdsmedUser> idsmedUserList = authenticationService.getUsersByPermissionCode(IdsmedPermissionCodeConst.VENDOR_APPROVE);
		List<IdsmedAccount> accountList = new ArrayList<>();
		Pair<String, String> emailPair = null;

		for(IdsmedUser approverVendorUser : idsmedUserList) {
			List<IdsmedAccount> accounts = approverVendorUser.getIdsmedAccounts().isEmpty() ? null : approverVendorUser.getIdsmedAccounts();
			accountList.addAll(accounts);
		}

		IdsmedAccount idsmedAccount = null;
		if(!StringUtils.isEmpty(loginId)) {
			idsmedAccount = idsmedAccountRepository.findByLoginId(loginId);
		}else{
			IdsmedRole idsmedRole = idsmedRoleRepository.findByCode(IdsmedRoleCodeConst.SCP_ADMIN);
			List<IdsmedUser> idsmedUser = idsmedUserRepository.findFirstSCPAdminByRoleAndPermissionCode(idsmedRole, IdsmedPermissionCodeConst.VENDOR_APPROVE);
			if (!CollectionUtils.isEmpty(idsmedUser)) {
				// Just to get 1 SCP admin account as a sender for notification
				idsmedAccount = idsmedUser.get(0).getIdsmedAccounts().get(0);
			}
		}
		//SEND EMAIL WITH VENDOR REGISTRATION PENDING APPROVED
		logger.info("go in sending simple message with vendor id [] :" + vendor.getId());
		for(IdsmedUser idsmedUser : idsmedUserList){
			String userName = idsmedUser.getFirstName() + " " + idsmedUser.getLastName();
			String userEmail = idsmedUser.getEmail();
			emailPair = emailService.generatedVendorRegistrationPendingApproveEmailSubjectAndContent(userName, vendor.getCompanyNamePrimary());
			Boolean isSendingEmail = emailService.checkSendEmail(idsmedUser.getIdsmedAccounts(), EmailTypeEnum.REQUEST_TO_APPROVE_VENDOR_REGISTRATION_NOTIFICATION);
			if (isSendingEmail) {
				EmailObject email = new VendorRegistrationPendingApproveEmailObject(Arrays.asList(userEmail), env.getProperty(PropertiesLabelConst.SMTP_SUPPORT_MAIL_LABEL), emailPair);
				rabbitTemplate.convertAndSend(env.getProperty(IdsmedQueueConfig.RABBIT_MAIL_SERVICE_PROCESS_QUEUE), email);
			}
		}
		//Send web notification WITH VENDOR REGISTRATION Pending Approved
		logger.info("start send vendor registration Pending Approved notification with vendor id [] " + vendor.getId());
		notificationService.sendVendorRegistrationPendingApprovedNotification(idsmedAccount, accountList, emailPair);
		logger.info("End send vendor registration notification Pending Approved with vendor id [] " + vendor.getId());
	}

	@Override
	@PreAuthorize("@PermissionService.isMethodPermited(authentication, {'VP0000005'})") //16 = Approve Vendor API - in idsmed_permission
	public VendorResponse updateApproveStatus(VendorRequest request) throws IdsmedBaseException {
		Long vendorId = request.getId();
		loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, UPDATE_APPROVED_STATUS_METHOD_NAME, request.getLoginId());
		Optional<Vendor> vendorOpt = vendorRepository.findById(vendorId);

		if(!vendorOpt.isPresent()) {
			logger.error("The Vendor Id not existing");
			throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.VENDOR_ID_NOT_EXISTED, LangHelper.getLangCode(request.getLangCode())));
		}
		Vendor vendor = vendorOpt.get();
		String ossBucketDomain = env.getProperty(PropertiesLabelConst.OSS_BUCKET_DOMAIN);

		//Get account of approver
		IdsmedAccount approver = idsmedAccountRepository.findByLoginId(request.getLoginId());
		vendor.setStatus(StatusEnum.APPROVED.getCode());
		vendor.setApprovedDate(DateTimeHelper.getCurrentTimeUTC());
		vendor.setApprovedBy(approver.getId());
		vendor.setVendorCoordinator(request.getVendorCoordinator());
		vendor = vendorRepository.save(vendor);

		//UPDATE VENDOR APPROVE TASK TO COMPLETED STATUS
		taskService.setCompletedVendorApproveTask(vendor, request.getLoginId());

		//Update status of
		//Update FileAttachments except upload file
		List<FileAttachment> fileAttachments =  updateVendorFileAttachmentWithoutUploadFile(request.getFileAttachments(), approver.getId());
		fileAttachmentRepository.saveAll(fileAttachments);
		IdsmedAccount idsmedAccount = idsmedAccountRepository.findByLoginId(request.getLoginId());

		//SEND EMAIL FOR VENDOR REGISTRATION APPROVED
		List<IdsmedAccount> idsmedAccounts = new ArrayList<>();
		if(idsmedAccount != null){
			idsmedAccounts.add(idsmedAccount);
		}

		Pair<String, String> emailPair = null;

		List<Pair<String, String>> nameEmailAddressPairs = new ArrayList<>();
		String userName1 = vendor.getPersonIncharge1Name();
		String address1 = vendor.getPersonIncharge1Email();
		Pair<String, String> nameEmailAddressPair1 = Pair.of(userName1, address1);
		nameEmailAddressPairs.add(nameEmailAddressPair1);

		String userName3 = vendor.getCompanyNamePrimary();
		String address3 = vendor.getEmail();
		Pair<String, String> nameEmailAddressPair3 = Pair.of(userName3, address3);
		nameEmailAddressPairs.add(nameEmailAddressPair3);
		if (!StringUtils.isEmpty(vendor.getPersonIncharge2Email()) && !StringUtils.isEmpty(vendor.getPersonIncharge2Name())) {
			String userName2 = vendor.getPersonIncharge2Name();
			String address2 = vendor.getPersonIncharge2Email();
			Pair<String, String> nameEmailAddressPair2 = Pair.of(userName2, address2);
			nameEmailAddressPairs.add(nameEmailAddressPair2);
		}
		for(Pair<String, String> nameEmailAddressPair : nameEmailAddressPairs) {
			List<String> rcps = new ArrayList<>();
			rcps.add(nameEmailAddressPair.getSecond());
			emailPair = emailService.generatedVendorRegistrationApprovedEmailSubjectAndContent(nameEmailAddressPair.getFirst(), vendor.getCompanyNamePrimary(), StatusEnum.APPROVED.getName(), vendor.getCompanyCode());
			EmailObject email = new VendorApprovedEmailObject(rcps, env.getProperty(PropertiesLabelConst.SMTP_SUPPORT_MAIL_LABEL), emailPair);
			rabbitTemplate.convertAndSend(env.getProperty(IdsmedQueueConfig.RABBIT_MAIL_SERVICE_PROCESS_QUEUE), email);
		}
		logger.info("End Sending Email for vendor registration approved with vendor id is [ " + vendorId + "] ");

		loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, UPDATE_APPROVED_STATUS_METHOD_NAME, request.getLoginId());
		VendorResponse vendorResponse = new VendorResponse(vendor, ossBucketDomain);
		return vendorResponse;
	}

	@Override
	@PreAuthorize("@PermissionService.isMethodPermited(authentication, {'VP0000007'})") //18 = Reject Vendor API - in idsmed_permission
	public VendorResponse updateRejectStatus(VendorRequest request) throws IdsmedBaseException {
		loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, UPDATE_REJECT_STATUS_METHOD_NAME, request.getLoginId());
		Long vendorId = request.getId();
		String rejectReason = request.getRejectReason();
		Optional<Vendor> vendorOpt = vendorRepository.findById(vendorId);
		String ossBucketDomain = env.getProperty(PropertiesLabelConst.OSS_BUCKET_DOMAIN);

		if(!vendorOpt.isPresent()) {
			logger.error("The Vendor Id not existing");
			throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.VENDOR_ID_NOT_EXISTED, LangHelper.getLangCode(request.getLangCode())));
		}

		Vendor vendor = vendorOpt.get();

		IdsmedAccount reviewer = idsmedAccountRepository.findByLoginId(request.getLoginId());
		vendor.setUpdatedBy(reviewer.getId());
		vendor.setStatus(StatusEnum.REJECT.getCode());
		Vendor saveVendor = vendorRepository.save(vendor);

		String countryCode = getCountryCodeOfVendor(vendor);

		//Update FileAttachments except upload file
		List<FileAttachment> fileAttachments =  updateVendorFileAttachmentWithoutUploadFile(request.getFileAttachments(), reviewer.getId());
		fileAttachmentRepository.saveAll(fileAttachments);

		//Update other fields
		VendorReject vendorReject = new VendorReject();
		vendorReject.setCreatedBy(reviewer.getId());
		vendorReject.setUpdatedBy(reviewer.getId());
		vendorReject.setVendor(saveVendor);
		vendorReject.setRemarks(rejectReason);
		vendorReject.setCountryCode(countryCode);
		vendorRejectRepository.save(vendorReject);

		sendVendorRegisterRejectEmail(vendor, rejectReason, reviewer, request.getLangCode());

		//UPDATE TASK TO COMPLETED STATUS
		taskService.setCompletedVendorReviewTask(saveVendor, request.getLoginId());
		taskService.setCompletedVendorApproveTask(saveVendor,request.getLoginId());

		loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, UPDATE_REJECT_STATUS_METHOD_NAME, request.getLoginId());
		VendorResponse vendorResponse = new VendorResponse(vendor, ossBucketDomain);
		return vendorResponse;
	}

	private void sendVendorRegisterRejectEmail(Vendor vendor, String rejectReason, IdsmedAccount account, String langCode) throws IdsmedBaseException {
		logger.info("Start send vendor registration reject email with vendor id : " + vendor.getId());

		//SEND RE-REGISTER LINK ALONG WITH EMAIL
		String baseLink = env.getProperty(VENDOR_RE_REGISTER_BASE_LINK_LABEL);
		String resubmitLink = baseLink + "?id=" + vendor.getId();

		List<String> userNames = new ArrayList<>();
		userNames.add(vendor.getPersonIncharge1Name());
		userNames.add(vendor.getPersonIncharge2Name());

		List<String> recipients = new ArrayList<>();
		recipients.add(vendor.getEmail());
		recipients.add(vendor.getPersonIncharge1Email());
		if (!StringUtils.isEmpty(vendor.getPersonIncharge2Email())) {
			recipients.add(vendor.getPersonIncharge2Email());
		}

		//Send email with the correct name to correct recipient
		if (!StringUtils.isEmpty(vendor.getPersonIncharge1Name()) && !StringUtils.isEmpty(vendor.getPersonIncharge1Email())) {
			Pair<String, String> emailPairPic1 = emailService.generatedVendorRegistrationRejectEmailSubjectAndContent(vendor.getPersonIncharge1Name(), vendor.getCompanyNamePrimary(),
					StatusEnum.REJECT.getName(), rejectReason, resubmitLink, langCode);
			EmailObject emailPic1 = new VendorRegisterRejectEmailObject(Arrays.asList(vendor.getPersonIncharge1Email()), env.getProperty(PropertiesLabelConst.SMTP_SUPPORT_MAIL_LABEL), emailPairPic1);
			rabbitTemplate.convertAndSend(env.getProperty(IdsmedQueueConfig.RABBIT_MAIL_SERVICE_PROCESS_QUEUE), emailPic1);
		}

		if (!StringUtils.isEmpty(vendor.getPersonIncharge2Name()) && !StringUtils.isEmpty(vendor.getPersonIncharge2Email())) {
			Pair<String, String> emailPairPic2 = emailService.generatedVendorRegistrationRejectEmailSubjectAndContent(vendor.getPersonIncharge2Name(), vendor.getCompanyNamePrimary(),
					StatusEnum.REJECT.getName(), rejectReason, resubmitLink, langCode);
			EmailObject emailPic2 = new VendorRegisterRejectEmailObject(Arrays.asList(vendor.getPersonIncharge2Email()), env.getProperty(PropertiesLabelConst.SMTP_SUPPORT_MAIL_LABEL), emailPairPic2);
			rabbitTemplate.convertAndSend(env.getProperty(IdsmedQueueConfig.RABBIT_MAIL_SERVICE_PROCESS_QUEUE), emailPic2);
		}

		if (!StringUtils.isEmpty(vendor.getEmail())) {
			Pair<String, String> emailPair = emailService.generatedVendorRegistrationRejectEmailSubjectAndContent(" there", vendor.getCompanyNamePrimary(),
					StatusEnum.REJECT.getName(), rejectReason, resubmitLink, langCode);
			EmailObject email = new VendorRegisterRejectEmailObject(Arrays.asList(vendor.getEmail()), env.getProperty(PropertiesLabelConst.SMTP_SUPPORT_MAIL_LABEL), emailPair);
			rabbitTemplate.convertAndSend(env.getProperty(IdsmedQueueConfig.RABBIT_MAIL_SERVICE_PROCESS_QUEUE), email);
		}
		logger.info("End send vendor registration reject email with vendor id : " + vendor.getId());
	}

	private String getCountryCodeOfVendor(Vendor vendor) {
		Country country = null;
		Optional<VendorAddress> vendorAddress = vendor.getVendorAddresses()
				.stream()
				.filter(vad -> vad.getType().equals(VendorAddress.VendorAddressTypeEnum.ADDRESS.getCode()))
				.findFirst();

		if (vendorAddress.isPresent()) {
			country = vendorAddress.get().getCountry();
		}

		String countryCode = country != null ? country.getCountryCode() : CountryCodeEnum.CHINA.getCode();
		return countryCode;
	}

	@Override
	@Transactional
	@Modifying
//	@PreAuthorize("@PermissionService.isMethodPermited(authentication, {'VP0000002', 'VPC0000002'})")
	public VendorResponse edit(VendorRequest request, MultipartFile companyLogo, List<MultipartFile> companyDocuments)
			throws IdsmedBaseException {
		loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, EDIT_VENDOR_METHOD_NAME, request.getLoginId());
		String baseURL = env.getProperty(VENDOR_FILE_UPLOAD_BASE_URL_NAME);
		String ossBucketDomain = env.getProperty(PropertiesLabelConst.OSS_BUCKET_DOMAIN);
		final String STORAGE_TYPE = env.getProperty(STORAGE_TYPE_PROPERTY_NAME);
		Optional<Vendor> vendorOpt = vendorRepository.findById(request.getId());
		if(!vendorOpt.isPresent()) {
			throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.VENDOR_EXISTS_ERROR, LangHelper.getLangCode(request.getLangCode())));
		}
		Vendor vendor = vendorOpt.get();
		String[] igroreProperties = {"id", "companyCode"
				, "approvedDate"
				, "approvedBy"
				, "vendorAddresses"
				, "fileAttachments"
				, "additionalInfos", "countryCode", "accountNumber", "vendorCoordinator", "companyLogoPath"};
		BeanUtils.copyProperties(request, vendor, igroreProperties);
		Integer status = request.getStatus();
		//TODO Need to implement checking permission
		if (StringUtils.isEmpty(request.getLoginId()) && vendor.getApprovedDate() != null) {
			throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.HAVE_NO_PERMISSION_ERROR, LangHelper.getLangCode(request.getLangCode())));
		}
		//When a Vendor has been approved already the status will not be changed.
		if(vendor.getApprovedDate() == null) {
			if (status != null && status.equals(StatusEnum.PENDING_FOR_REVIEW.getCode())) {
				vendor.setStatus(StatusEnum.PENDING_FOR_REVIEW.getCode());
			}

			if (status != null && status.equals(StatusEnum.PENDING_FOR_APPROVAL.getCode())) {
				vendor.setStatus(StatusEnum.PENDING_FOR_APPROVAL.getCode());
			}
		}

		//Upload new logo if exists
		Map<String, String> fileUploadInfoMap = null;

		try {
			if(companyLogo != null) {
				fileUploadInfoMap = fileUploadService.uploadImage(companyLogo, EntityType.VENDOR);
			}
		}catch(IOException e) {
			throw new IdsmedBaseException(
					messageResourceService.getErrorInfo(ErrorInfo.UPLOAD_FILE_ERROR, LangHelper.getLangCode(request.getLangCode())));
		}

		if(fileUploadInfoMap != null) {
			String serverURL = StringUtils.isEmpty(baseURL) ? "" : baseURL + "/" + fileUploadInfoMap.get(FileUploadServiceImpl.HASH_FILE_NAME_KEY);
			String relativePath = fileUploadInfoMap.get(FileUploadServiceImpl.UPLOAD_URL);
			vendor.setCompanyLogoPath(STORAGE_TYPE.equalsIgnoreCase(SERVER_STORAGE_TYPE) ? serverURL : relativePath);
		}

		//Set Address
		//For now we don't implement logic of saving vendor address history.
		//Will implement it when have requirement from customer.
		List<VendorAddressRequest> addressRequests = request.getVendorAddressRequests();
		List<VendorAddress> existingAddresses = vendor.getVendorAddresses();

		if (!CollectionUtils.isEmpty(addressRequests)) {
			for (VendorAddressRequest adr : addressRequests) {
				ListIterator<VendorAddress> itr = existingAddresses.listIterator();
				while (itr.hasNext()) {
					VendorAddress address = itr.next();
					if (address.getId().equals(adr.getId())) {
						address.updateVendorAddress(adr);
						break;
					}
				}
			}
		}

		if (!CollectionUtils.isEmpty(existingAddresses)) {
			vendorAddressRepository.saveAll(existingAddresses);
		}

		//Set Vendor Additional Info
		List<VendorAdditionalInfoRequest> additionalInfoRequests = request.getAdditionalInfos();
		List<VendorAdditionalInfo> existingInfos = vendor.getAdditionalInfos();

		if (!CollectionUtils.isEmpty(additionalInfoRequests)) {
			for (VendorAdditionalInfoRequest air : additionalInfoRequests ) {
				ListIterator<VendorAdditionalInfo> iterator = existingInfos.listIterator();
				while (iterator.hasNext()) {
					VendorAdditionalInfo info = iterator.next();
					if (info.getId().equals(air.getId())) {
						info.updateVendorAdditionalInfo(air);
						break;
					}
				}

			}
		}

		if (!CollectionUtils.isEmpty(existingInfos)) {
			vendorAdditionalInfoRepository.saveAll(existingInfos);
		}

		//Update FileAttachment
		List<FileAttachment> existingFileAttachments = vendor.getFileAttachmentList();
		List<VendorFileAttachmentRequest> attachmentRequests = request.getFileAttachments();

		if (!CollectionUtils.isEmpty(attachmentRequests)) {
			//If status of existing FileAttachment is rejected we upload new file and update fields
			List<VendorFileAttachmentRequest> updateRequests =
					attachmentRequests
							.stream()
							.filter(r -> {
								Optional<FileAttachment> existingFileAttachmentOpt = existingFileAttachments.stream().filter(f -> f.getId().equals(r.getId())).findFirst();
								if (existingFileAttachmentOpt.isPresent()
										&& existingFileAttachmentOpt.get().getStatus().equals(FileAttachmentStatusEnum.REJECT.getCode())) {
									return true;
								}
								return false;
							}).collect(Collectors.toList());
			if (!CollectionUtils.isEmpty(updateRequests)) {
				Pair<List<FileAttachment>, List<VendorFileAttachmentReminder>> attachmentPair = updateVendorFileAttachmentWithUploadFile(updateRequests
						, companyDocuments
						, vendor
						, vendor.getCountryCode()
						, baseURL
						, STORAGE_TYPE
						, request.getLangCode(), request.getLoginId());
				if (!CollectionUtils.isEmpty(attachmentPair.getFirst())) {
					attachmentPair.getFirst().forEach(at -> {
						at.setStatus(FileAttachmentStatusEnum.PENDING_FOR_REVIEW.getCode());
					});
					fileAttachmentRepository.saveAll(attachmentPair.getFirst());
				}
				if (!CollectionUtils.isEmpty(attachmentPair.getSecond())) {
					vendorFileAttachmentReminderRespository.saveAll(attachmentPair.getSecond());
				}
			}

			//If status of existing FileAttachment is approved we create new FileAttachment record.
			List<VendorFileAttachmentRequest> newRequests =
					attachmentRequests.stream().filter(r -> {
						Optional<FileAttachment> existingFileAttachmentOpt = existingFileAttachments.stream().filter(f -> f.getId().equals(r.getId())).findFirst();
						if (existingFileAttachmentOpt.isPresent()
								&& existingFileAttachmentOpt.get().getStatus().equals(FileAttachmentStatusEnum.APPROVED.getCode())) {
							return true;
						}
						return false;
					}).collect(Collectors.toList());

			if (!CollectionUtils.isEmpty(newRequests)) {
				Pair<List<FileAttachment>, List<VendorFileAttachmentReminder>> attachmentPair = setFileAttachments(newRequests
						, companyDocuments
						, vendor
						, vendor.getCountryCode()
						, baseURL
						, STORAGE_TYPE
						, request.getLangCode());
				if (!CollectionUtils.isEmpty(attachmentPair.getFirst())) {
					fileAttachmentRepository.saveAll(attachmentPair.getFirst());
				}

				if (!CollectionUtils.isEmpty(attachmentPair.getSecond())) {
					vendorFileAttachmentReminderRespository.saveAll(attachmentPair.getSecond());

				}
			}
		}

		if(!StringUtils.isEmpty(request.getAccountNumber())) {
			vendor.setBankAccountNumber(request.getAccountNumber());
		}

		if(request.getVendorCoordinator() != null){
			vendor.setVendorCoordinator(request.getVendorCoordinator());
		}

		vendor = vendorRepository.save(vendor);
		Long coodinatorAccountId = vendor.getVendorCoordinator();
		IdsmedUser coodinatorUser = null;
		IdsmedAccount coodinatorAccount = null;
		if (coodinatorAccountId != null) {
			Optional<IdsmedAccount> coodinatorAccountOpt = idsmedAccountRepository.findById(coodinatorAccountId);
			if (coodinatorAccountOpt.isPresent()) {
				coodinatorAccount = coodinatorAccountOpt.get();
				coodinatorUser = coodinatorAccount.getIdsmedUser();
			}
		}
		VendorResponse vendorResponse = new VendorResponse(vendor, ossBucketDomain);
		if (coodinatorUser != null && coodinatorAccount != null) {
			IdsmedAccountResponse idsmedAccountResponse = new IdsmedAccountResponse(coodinatorAccount, coodinatorUser);
			vendorResponse.setIdsmedAccountResponse(idsmedAccountResponse);
		}

		if(vendor.getApprovedDate() == null) {
			if (status != null && status.equals(StatusEnum.PENDING_FOR_REVIEW.getCode())) {
				taskService.createVendorReviewTask(vendor);

			}

			if (status != null && status.equals(StatusEnum.PENDING_FOR_APPROVAL.getCode())) {
				taskService.createVendorApproveTask(vendor);
				processEmailAndWebNotificationForVendorPendingApproval(vendor, request.getLoginId());
			}
		}

		loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, EDIT_VENDOR_METHOD_NAME, request.getLoginId());
		return vendorResponse;
	}

	@Override
	@Transactional
	public List<FileAttachmentResponse> editVendorFileAttachment(VendorFileAttachmentsEditRequest request, List<MultipartFile> files) throws IdsmedBaseException {
		loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, EDIT_VENDOR_FILE_ATTACHMENT_METHOD_NAME, request.getLoginId());
		Long vendorId = request.getVendorId();
		String ossBucketDomain = env.getProperty(PropertiesLabelConst.OSS_BUCKET_DOMAIN);
		List<VendorFileAttachmentRequest> requests = request.getFileAttachments();
		String langCode = request.getLangCode();

		String baseURL = env.getProperty(VENDOR_FILE_UPLOAD_BASE_URL_NAME);
		final String STORAGE_TYPE = env.getProperty(STORAGE_TYPE_PROPERTY_NAME);

		Vendor vendor = vendorRepository.getOne(vendorId);
		if (vendor == null) {
			throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.VENDOR_NOT_FOUND_ERROR, LangHelper.getLangCode(langCode)));
		}

		List<VendorFileAttachmentRequest> newAttachmentRequests = requests
				.stream()
				.filter(r -> r.getId() == null)
				.collect(Collectors.toList());

		List<VendorFileAttachmentRequest> existingAttachmentRequests = requests
				.stream()
				.filter(r -> r.getId() != null)
				.collect(Collectors.toList());

		Pair<List<FileAttachment>, List<VendorFileAttachmentReminder>> newAttachmentsPair = setFileAttachments(newAttachmentRequests
				, files, vendor, vendor.getCountryCode(), baseURL, STORAGE_TYPE, langCode);

		Pair<List<FileAttachment>, List<VendorFileAttachmentReminder>> existingAttachmentsPair = updateVendorFileAttachmentWithUploadFile(existingAttachmentRequests
				, files, vendor, vendor.getCountryCode(), baseURL, STORAGE_TYPE, langCode, request.getLoginId());

		List<FileAttachment> existingAttachments = existingAttachmentsPair.getFirst();
		List<VendorFileAttachmentReminder> existingReminders = existingAttachmentsPair.getSecond();

		//SEND EMAIL AND NOTIFICATION
		if (isApprovalFileAttachmentProcess(existingAttachments)) {
			//TODO WAITNG FOR ADJUST EMAIL CONFIGURATION FOR THIS TYPE THEN IMPLEMENTS SEND EMAIL AND NOTIFICATION;
			// get the attachment document name from existing attachments list
			String attachmentDocumentName = existingAttachments
					.stream()
					.map(fa -> fa.getDocumentName() + "; ")
					.collect(Collectors.joining());

			// get vendor id by file attachment and assign the sender account base on vendor.getUpdatedBy
			Long vId = existingAttachments.get(0).getVendor().getId();
			Optional<Vendor> fileAttachmentVendor = vendorRepository.findById(vId);

			// This is for storing the idsmedUserlist for person1incharge and idsmeduserlist for person2incharge
			// combine the idsmeduserlist for person1incharge and person2incharge and store inro idsmeduserforsamecompany to check whether this user need to send email or not.
			List<IdsmedUser> idsmedUsersForSameCompany = new ArrayList<>();
			List<IdsmedUser> idsmedUsersForPerson1Incharge = idsmedUserRepository.findIdsmedUserByCompanyCodeAndEmail(vendor.getCompanyCode(), vendor.getPersonIncharge1Email());
			List<IdsmedUser> idsmedUsersForPerson2Incharge = idsmedUserRepository.findIdsmedUserByCompanyCodeAndEmail(vendor.getCompanyCode(), vendor.getPersonIncharge2Email());
			idsmedUsersForSameCompany.addAll(idsmedUsersForPerson1Incharge);
			idsmedUsersForSameCompany.addAll(idsmedUsersForPerson2Incharge);
			List<IdsmedAccount> accountListForSameCompany = new ArrayList<>();
			for (IdsmedUser idsmedUser : idsmedUsersForSameCompany) {
				accountListForSameCompany.addAll(idsmedUser.getIdsmedAccounts());
			}
			Pair<String, String> emailPair = processEmailNotificationVendorProfileCertificateApprovalNotification(existingAttachments.get(0).getVendor(), attachmentDocumentName, FileAttachmentStatusEnum.APPROVED.getName());
			if(!StringUtils.isEmpty(request.getLoginId())) {
				IdsmedAccount senderAccount = idsmedAccountRepository.findByLoginId(request.getLoginId());
				processWebNotificationVendorProfileCertificateApproval(senderAccount, accountListForSameCompany, emailPair);
			}
		}

		if (!CollectionUtils.isEmpty(newAttachmentsPair.getFirst())) {
			existingAttachments.addAll(newAttachmentsPair.getFirst());
		}

		if (!CollectionUtils.isEmpty(newAttachmentsPair.getSecond())) {
			existingReminders.addAll(newAttachmentsPair.getSecond());
		}
		if (!CollectionUtils.isEmpty(existingAttachments)) {
			existingAttachments = fileAttachmentRepository.saveAll(existingAttachments);
		}

		if (!CollectionUtils.isEmpty(existingReminders)) {
			existingReminders = vendorFileAttachmentReminderRespository.saveAll(existingReminders);
		}

		loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, EDIT_VENDOR_FILE_ATTACHMENT_METHOD_NAME, request.getLoginId());
		return existingAttachments.stream().map(att -> new FileAttachmentResponse(att, ossBucketDomain)).collect(Collectors.toList());
	}

	private Boolean isApprovalFileAttachmentProcess(List<FileAttachment> fileAttachments) {
		if (!CollectionUtils.isEmpty(fileAttachments)) {
			Optional<FileAttachment> fileAttachmentOpt = fileAttachments.stream().findFirst();
			if (fileAttachmentOpt.isPresent()) {
				fileAttachmentOpt.get().getStatus().equals(FileAttachmentStatusEnum.APPROVED.getCode());
				return true;
			}
		}
		return false;
	}

	public CompanyBackgroundInfoResponse getComapanyBackgroundInfo (String companyCode) throws IdsmedBaseException {
		loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, GET_COMAPANY_BACKGROUND_INFO_METHOD_NAME, null);
		Optional<CompanyBackgroundInfo> cbi = companyBackgroundInfoRepository.findByCompanyCode(companyCode);

		if (!cbi.isPresent()) {
			throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.COMPANY_CODE_NOT_EXIST, CountryCodeEnum.CHINA.getCode()));
		}
		loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, GET_COMAPANY_BACKGROUND_INFO_METHOD_NAME, null);
		return new CompanyBackgroundInfoResponse(cbi.get());
	}

	private List<FileAttachment> updateVendorFileAttachmentWithoutUploadFile(List<VendorFileAttachmentRequest> attachmentRequests, Long updatedBy) throws IdsmedBaseException {
		List<FileAttachment> fileAttachments = new ArrayList<>();
		String ossBucketDomain = env.getProperty(PropertiesLabelConst.OSS_BUCKET_DOMAIN);

		if (!CollectionUtils.isEmpty(attachmentRequests)) {
			fileAttachments = attachmentRequests.stream().map(r -> {
				Optional<FileAttachment> attachmentOpt = fileAttachmentRepository.findById(r.getId());
				FileAttachment fileAttachment = attachmentOpt.get();
				r.setDocumentPath(fileAttachment.getDocumentPath());
				r.setOriginalFileName(fileAttachment.getOriginalFileName());
				fileAttachment.updateFileAttachment(r, updatedBy, ossBucketDomain);
				return fileAttachment;
			}).collect(Collectors.toList());
		}

		// ------------------ EMAIL AND WEB NOTIFICATION REQUEST TO APPROVE VENDOR CERTIFICATE --------------------
		if (!CollectionUtils.isEmpty(attachmentRequests)) {
			if (attachmentRequests.get(0).getStatus() == FileAttachmentStatusEnum.PENDING_FOR_APPROVAL.getCode()) {
				Pair<String, String> emailNotificationPair = null;
				Long vendorId = attachmentRequests.get(0).getVendorId();
				Vendor vendor = vendorRepository.findById(vendorId).get();
				if (vendor != null) {
					IdsmedAccount vendorCoordinator = idsmedAccountRepository.findById(vendor.getVendorCoordinator()).get();
					if (vendorCoordinator != null) {
						IdsmedUser vendorCoordinatorUser = idsmedUserRepository.findIdsmedUserByIdsmedAccounts(vendorCoordinator);
						if (vendorCoordinatorUser != null) {
							String attachmentDocumentName = fileAttachments
									.stream()
									.map(fa -> fa.getDocumentName() + "; ")
									.collect(Collectors.joining());

							if (vendorCoordinator != null) {
								emailNotificationPair = processEmailNotificationRequestToApproveVendorCertificate(vendorCoordinatorUser,
										attachmentDocumentName, FileAttachmentStatusEnum.PENDING_FOR_APPROVAL.getName());
							}

							IdsmedUser idsmedUser = idsmedUserRepository.findById(updatedBy).get();
							IdsmedAccount sender = idsmedAccountRepository.findFirstByIdsmedUser(idsmedUser);
							processWebNotificationRequestToApproveVendorCertificate(
									Arrays.asList(sender),
									Arrays.asList(vendorCoordinator),
									emailNotificationPair);
						}
					}
				}
			}
		}

		return fileAttachments;
	}

	@Override
	@Transactional
	public VendorResponse setVendorStatus(VendorSetStatusRequest request) throws IdsmedBaseException {
		loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, SET_VENDOR_STATUS_METHOD_NAME, request.getLoginId());
		String ossBucketDomain = env.getProperty(PropertiesLabelConst.OSS_BUCKET_DOMAIN);
		IdsmedAccount loggingAccount = idsmedAccountRepository.findByLoginId(request.getLoginId());
		if (loggingAccount == null) {
			throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.ACCOUNT_NOT_EXIST_ERROR, LangHelper.getLangCode(request.getLangCode())));
		}

		String loginId = loggingAccount.getLoginId();

		Optional<Vendor> vendorOpt = vendorRepository.findById(request.getVendorId());

		if (!vendorOpt.isPresent()) {
			throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.VENDOR_NOT_FOUND_ERROR, LangHelper.getLangCode(request.getLangCode())));
		}

		if (!validVendorActionCodeForSuspendOrTerminate(request.getActionCode())) {
			logger.error("The action code: {} is not valid", request.getActionCode());
			return null;
		}

		Vendor vendor = vendorOpt.get();

		// For inactive/suspend Vendor
		if (request.getActionCode().equals(VendorActionEnum.SUSPEND.getCode())) {
			vendor = suspendVendor(vendor);
			vendor = vendorRepository.save(vendor);
			VendorActionLog log = new VendorActionLog(VendorActionEnum.SUSPEND.getCode(), request.getReason(), request.getReason()
					, loginId, loginId);
			log.setVendor(vendor);
			vendorActionLogRepository.save(log);
			processEmailOfInactiveOrSuspendedVendorToAllVendorUser(vendor, request);
		}

		// For terminate vendor
		if (request.getActionCode().equals(VendorActionEnum.TERMINATE.getCode())) {
			vendor = terminateVendor(vendor);
			vendor = vendorRepository.save(vendor);
			VendorActionLog log = new VendorActionLog(VendorActionEnum.TERMINATE.getCode(), request.getReason(), request.getReason()
					, loginId, loginId);
			log.setVendor(vendor);
			vendorActionLogRepository.save(log);
			processEmailOfTernimationOfVendorToAllVendorUser(vendor, request);
		}

		loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, SET_VENDOR_STATUS_METHOD_NAME, request.getLoginId());

		return new VendorResponse(vendor, ossBucketDomain);
	}

	private Boolean validVendorActionCodeForSuspendOrTerminate(Integer actionCode) {
		if (actionCode.equals(VendorActionEnum.SUSPEND.getCode()) || actionCode.equals(VendorActionEnum.TERMINATE.getCode())) {
			return true;
		}
		return false;
	}

	@Transactional
	public Vendor suspendVendor(Vendor vendor) {
		//Set status of Vendor is suspended(Inactive)
		vendor.setStatus(StatusEnum.INACTIVE.getCode());

		//Inactive Products.
		List<Product> products = vendor.getProductList();
		if (!CollectionUtils.isEmpty(products)) {
			products.forEach(p -> {
				inactiveProduct(p);
			});
			products = productRepository.saveAll(products);
		}

		//Set related account is inactive.
		List<IdsmedAccount> needToInactiveAccounts = setInactiveAccountByCompanyCode(vendor.getCompanyCode());
		if (!CollectionUtils.isEmpty(needToInactiveAccounts)) {
			idsmedAccountRepository.saveAll(needToInactiveAccounts);
		}

		vendor.setProductList(products);

		return vendor;
	}

	private List<IdsmedAccount> setInactiveAccountByCompanyCode(String companyCode) {
		List<IdsmedAccount> relatedAccounts = idsmedAccountRepository.findAccountsByCompanyCode(companyCode);
		List<IdsmedAccount> needToInactiveAccounts = new ArrayList<>();
		if (!relatedAccounts.isEmpty()) {
			needToInactiveAccounts = relatedAccounts
					.stream()
					.filter(acc -> !acc.getStatus().equals(StatusEnum.ARCHIVED.getCode()))
					.collect(Collectors.toList());
			if (!CollectionUtils.isEmpty(needToInactiveAccounts)) {
				needToInactiveAccounts.forEach(acc -> {
					acc.setPreviousStatus(acc.getStatus());
					acc.setStatus(StatusEnum.INACTIVE.getCode());
				});
			}
		}

		Integer status = StatusEnum.INACTIVE.getCode();
		// Inactive/Suspend vendor user
		processSetVendorUserStatus(companyCode, status);

		return needToInactiveAccounts;
	}

	private List<IdsmedAccount> setTerminateAccountByCompanyCode(String companyCode) {
		List<IdsmedAccount> relatedAccounts = idsmedAccountRepository.findAccountsByCompanyCode(companyCode);
		List<IdsmedAccount> needToTerminateAccounts = new ArrayList<>();
		if (!relatedAccounts.isEmpty()) {
			needToTerminateAccounts = relatedAccounts
					.stream()
					.filter(acc -> !acc.getStatus().equals(StatusEnum.ARCHIVED.getCode()))
					.collect(Collectors.toList());
			if (!CollectionUtils.isEmpty(needToTerminateAccounts)) {
				needToTerminateAccounts.forEach(acc -> {
					acc.setPreviousStatus(acc.getStatus());
					acc.setStatus(StatusEnum.TERMINATED.getCode());
				});
			}
		}

		Integer status = StatusEnum.TERMINATED.getCode();
		// Terminate vendor user
		processSetVendorUserStatus(companyCode, status);

		return needToTerminateAccounts;
	}

	@Transactional
	public void processSetVendorUserStatus(String companyCode, Integer status) {
		List<IdsmedUser> relatedUsers = idsmedUserRepository.findAllByCompanyCode(companyCode);
		List<IdsmedUser> needToInactiveUsers = new ArrayList<>();
		if (!relatedUsers.isEmpty()) {
			needToInactiveUsers = relatedUsers
					.stream()
					.filter(user -> !user.getStatus().equals(StatusEnum.ARCHIVED.getCode()))
					.collect(Collectors.toList());
			if (!CollectionUtils.isEmpty(needToInactiveUsers)) {
				needToInactiveUsers.forEach(user -> {
					user.setPreviousStatus(user.getStatus());
					user.setStatus(status);
				});
			}

			idsmedUserRepository.saveAll(needToInactiveUsers);
		}
	}

	@Transactional
	public Vendor terminateVendor(Vendor vendor) {
		//Set status of Vendor to terminate
		vendor.setStatus(StatusEnum.TERMINATED.getCode());

		//Terminate Products.
		List<Product> products = vendor.getProductList();
		if (!CollectionUtils.isEmpty(products)) {
			products.forEach(p -> {
				terminateProduct(p);
			});
			products = productRepository.saveAll(products);
		}

		//Set related account is inactive.
		List<IdsmedAccount> needToTerminateAccounts = setTerminateAccountByCompanyCode(vendor.getCompanyCode());
		if (!CollectionUtils.isEmpty(needToTerminateAccounts)) {
			idsmedAccountRepository.saveAll(needToTerminateAccounts);
		}

		vendor.setProductList(products);

		return vendor;
	}

	/**
	 * This method save existing status to previous_status
	 * then set status of product to inactive
	 * @param product
	 * @return
	 */
	private Product inactiveProduct(Product product) {
		Integer existingStatus = product.getStatus();
		product.setPreviousStatus(existingStatus);
		product.setStatus(StatusEnum.INACTIVE.getCode());
		return product;
	}

	/**
	 * This method save existing status to previous status
	 * then set status of product to terminate
	 * @param product
	 * @return
	 */
	private Product terminateProduct(Product product) {
		Integer existingStatus = product.getStatus();
		product.setPreviousStatus(existingStatus);
		product.setStatus(StatusEnum.TERMINATED.getCode());
		return product;
	}

	@Override
	@Transactional
	public VendorResponse resumeDeactiveVendor(VendorSetStatusRequest request) throws IdsmedBaseException {
		loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, RESUME_DEACTIVE_VENDOR_METHOD_NAME, request.getLoginId());
		String ossBucketDomain = env.getProperty(PropertiesLabelConst.OSS_BUCKET_DOMAIN);
		IdsmedAccount loggingAccount = idsmedAccountRepository.findByLoginId(request.getLoginId());
		if (loggingAccount == null) {
			throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.ACCOUNT_NOT_EXIST_ERROR, LangHelper.getLangCode(request.getLangCode())));
		}

		String loginId = loggingAccount.getLoginId();

		Optional<Vendor> vendorOpt = vendorRepository.findById(request.getVendorId());

		if (!vendorOpt.isPresent()) {
			throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.VENDOR_NOT_FOUND_ERROR, LangHelper.getLangCode(request.getLangCode())));
		}

		if (!validVendorActionCodeForResume(request.getActionCode())) {
			logger.error("The action code: {} is not valid", request.getActionCode());
			return null;
		}

		Vendor vendor = vendorOpt.get();

		vendor = resumeDeactiveVendor(vendor);
		VendorActionLog log = new VendorActionLog(VendorActionEnum.RESUME.getCode(), request.getReason(), request.getReason()
				, loginId, loginId);
		log.setVendor(vendor);
		vendorActionLogRepository.save(log);
		loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, RESUME_DEACTIVE_VENDOR_METHOD_NAME, request.getLoginId());
		return new VendorResponse(vendor, ossBucketDomain);
	}

	@Override
	public VendorResponse getVendorCoordinatorByCompanyCode(String companyCode, String langCode) throws IdsmedBaseException {
		loggerForAllMethodInThisService(START_METHOD_DESCRIPTION, GET_VENDOR_COORDINATOR_BY_COMPANY_CODE, null);
		String ossBucketDomain = env.getProperty(PropertiesLabelConst.OSS_BUCKET_DOMAIN);
		Vendor vendor = vendorRepository.findFirstByCompanyCode(companyCode);
		if(vendor == null){
			throw new IdsmedBaseException(messageResourceService.getErrorInfo(ErrorInfo.VENDOR_NOT_FOUND_ERROR, LangHelper.getLangCode(langCode)));
		}
		VendorResponse vendorResponse = new VendorResponse(vendor, ossBucketDomain);
		if(vendor.getVendorCoordinator() != null) {
			Long vendorCoordinatorId = vendor.getVendorCoordinator();
			Optional<IdsmedAccount> idsmedAccountOpt = idsmedAccountRepository.findById(vendorCoordinatorId);
			IdsmedAccount vendorCoordinator = idsmedAccountOpt.get();
			IdsmedUser vendorCoordinatorUser = vendorCoordinator.getIdsmedUser();
			IdsmedAccountResponse vendorCoordinatorResponse = new IdsmedAccountResponse(vendorCoordinator, vendorCoordinatorUser);
			vendorResponse.setIdsmedAccountResponse(vendorCoordinatorResponse);
		}
		loggerForAllMethodInThisService(END_METHOD_DESCRIPTION, GET_VENDOR_COORDINATOR_BY_COMPANY_CODE, null);
		return vendorResponse;
	}

	@Transactional
	public Vendor resumeDeactiveVendor(Vendor vendor) {
		//Set status of Vendor is APPROVED
		vendor.setStatus(StatusEnum.APPROVED.getCode());

		//Recover the product status.
		List<Product> products = vendor.getProductList();
		if (!CollectionUtils.isEmpty(products)) {
			List<Product> recoveryProducts = products
					.stream()
					.filter(p -> !p.getStatus().equals(StatusEnum.ARCHIVED.getCode()))
					.collect(Collectors.toList());
			if (!CollectionUtils.isEmpty(recoveryProducts)) {
				recoveryProducts.forEach( p -> {
					if (p.getPreviousStatus() != null) {
						p.setStatus(p.getPreviousStatus());
					}
				});
			}
			productRepository.saveAll(recoveryProducts);
		}

		//Recover accounts:
		List<IdsmedAccount> accounts = idsmedAccountRepository.findAccountsByCompanyCode(vendor.getCompanyCode());
		List<IdsmedAccount> needToRecoverAccounts = accounts
				.stream()
				.filter(acc -> acc.getStatus().equals(StatusEnum.ARCHIVED.getCode()))
				.collect(Collectors.toList());
		if (!CollectionUtils.isEmpty(needToRecoverAccounts)) {
			needToRecoverAccounts.forEach(acc -> {
				if (acc.getPreviousStatus() != null) {
					acc.setStatus(acc.getPreviousStatus());
				}
			});
			idsmedAccountRepository.saveAll(needToRecoverAccounts);
		}

		return vendor;
	}

	private Boolean validVendorActionCodeForResume(Integer actionCode) {
		if (actionCode.equals(VendorActionEnum.RESUME.getCode())) {
			return true;
		}
		return false;
	}

	private void processEmailAndWebNotificationOfReviewNewVendorToAdmin(List<IdsmedUser> adminList,
																		List<IdsmedAccount> adminAccountList,
																		Vendor vendor) throws IdsmedBaseException {

		// Email Notification Part
		logger.info("Start send email to ADMIN for pending review new vendor registration with name {}",
				vendor.getCompanyNamePrimary());

		Pair<String, String> emailPair = null;
		for(IdsmedUser idsmedUser : adminList) {
			String userName = idsmedUser.getFirstName() + " " + idsmedUser.getLastName();
			String userEmail = idsmedUser.getEmail();

			emailPair = emailService.generatedVendorRegistrationPendingReviewEmailSubjectAndContent(userName,
					vendor.getCompanyNamePrimary());

			EmailObject emailForAdmin = new VendorRegistrationPendingReviewEmailObject(Arrays.asList(userEmail),
					env.getProperty(PropertiesLabelConst.SMTP_SUPPORT_MAIL_LABEL), emailPair);

			rabbitTemplate.convertAndSend(env.getProperty(IdsmedQueueConfig.RABBIT_MAIL_SERVICE_PROCESS_QUEUE), emailForAdmin);
		}
		logger.info("End send email to ADMIN for pending review new vendor registration with name {}",
				vendor.getCompanyNamePrimary());

		// Web Notification Part
		logger.info("Start send web notification to ADMIN for pending review new vendor registration with name {}",
				vendor.getCompanyNamePrimary());

		notificationService.sendVendorRegistrationPendingReviewNotification(adminAccountList,
				adminAccountList, emailPair);

		logger.info("End send web notification to ADMIN for pending review new vendor registration with name {}",
				vendor.getCompanyNamePrimary());
	}

	private void processEmailOfInactiveOrSuspendedVendorToAllVendorUser(Vendor vendor, VendorSetStatusRequest request) throws IdsmedBaseException {

		// Email Notification Part
		logger.info("Start send email to Vendor User for inactive or suspended vendor with name {}",
				vendor.getCompanyNamePrimary());

		List<IdsmedUser> vendorUserList = idsmedUserRepository.findAllByCompanyCode(vendor.getCompanyCode());
		Pair<String, String> emailPair = null;
		for(IdsmedUser vendorUser : vendorUserList) {
			String vendorUserEmail = vendorUser.getEmail();

			emailPair = emailService.generatedInactiveOrSuspendedVendorEmailSubjectAndContent(vendorUser,
					vendor.getCompanyNamePrimary(), request.getReason());

			EmailObject emailForVendorUser = new InactiveOrSuspendedVendorToVendorUserEmailObject(Arrays.asList(vendorUserEmail),
					env.getProperty(PropertiesLabelConst.SMTP_SUPPORT_MAIL_LABEL), emailPair);

			rabbitTemplate.convertAndSend(env.getProperty(IdsmedQueueConfig.RABBIT_MAIL_SERVICE_PROCESS_QUEUE), emailForVendorUser);
		}
		logger.info("End send email to Vendor User for inactive / suspended vendor with name {}",
				vendor.getCompanyNamePrimary());
	}

	private void processEmailOfTernimationOfVendorToAllVendorUser(Vendor vendor, VendorSetStatusRequest request) throws IdsmedBaseException {

		// Email Notification Part
		logger.info("Start send email to Vendor User for terminate vendor with name {}",
				vendor.getCompanyNamePrimary());

		List<IdsmedUser> vendorUserList = idsmedUserRepository.findAllByCompanyCode(vendor.getCompanyCode());
		Pair<String, String> emailPair = null;
		for(IdsmedUser vendorUser : vendorUserList) {
			String vendorUserEmail = vendorUser.getEmail();

			emailPair = emailService.generatedTerminateVendorEmailSubjectAndContent(vendorUser,
					vendor.getCompanyNamePrimary(), request.getReason());

			EmailObject emailForAdmin = new TerminateVendorToVendorUserEmailObject(Arrays.asList(vendorUserEmail),
					env.getProperty(PropertiesLabelConst.SMTP_SUPPORT_MAIL_LABEL), emailPair);

			rabbitTemplate.convertAndSend(env.getProperty(IdsmedQueueConfig.RABBIT_MAIL_SERVICE_PROCESS_QUEUE), emailForAdmin);
		}
		logger.info("End send email to Vendor User for terminate vendor with name {}",
				vendor.getCompanyNamePrimary());
	}


	private void processEmailAndWebNotificationOfReviewNewVendorToSCPUser(List<IdsmedUser> scpVendorReviewerList,
																		  List<IdsmedAccount> scpVendorReviewerAccountList,
																		  Vendor vendor, IdsmedAccount idsmedAccount, VendorRequest request) throws IdsmedBaseException {
		Pair<String, String> emailPair = null;

		// Email Notification Part

		logger.info("Start send email to SCP User for pending review new vendor registration with name {}",
				vendor.getCompanyNamePrimary());

		List<String> recipients = new ArrayList<>();
		for (IdsmedUser idsmedUser : scpVendorReviewerList) {
			String userName = idsmedUser.getFirstName() + " " + idsmedUser.getLastName();
			String userEmail = idsmedUser.getEmail();
			emailPair = emailService.generatedVendorRegistrationPendingReviewEmailSubjectAndContent(userName,
					vendor.getCompanyNamePrimary());
			Boolean isSendingEmail = emailService.checkSendEmail(idsmedUser.getIdsmedAccounts(), EmailTypeEnum.REQUEST_TO_REVIEW_VENDOR_REGISTRATION_NOTIFICATION);
			if (isSendingEmail) {
				EmailObject emailForAdmin = new VendorRegistrationPendingReviewEmailObject(Arrays.asList(userEmail),
						env.getProperty(PropertiesLabelConst.SMTP_SUPPORT_MAIL_LABEL), emailPair);

				rabbitTemplate.convertAndSend(env.getProperty(IdsmedQueueConfig.RABBIT_MAIL_SERVICE_PROCESS_QUEUE), emailForAdmin);
			}
		}
		logger.info("End send email to SCP User for pending review new vendor registration with name {}",
				vendor.getCompanyNamePrimary());

		// Web Notification Part
		logger.info("Start send web notification to SCP User for pending review new vendor registration with name {}",
				vendor.getCompanyNamePrimary());

		notificationService.sendVendorRegistrationPendingReviewNotification(scpVendorReviewerAccountList,
				scpVendorReviewerAccountList, emailPair);

		logger.info("End send web notification to SCP User for pending review new vendor registration with name {}",
				vendor.getCompanyNamePrimary());
	}

	private Pair<String, String> processEmailNotificationRequestToApproveVendorCertificate(IdsmedUser vendorCoordinator,
																						   String attachmentDocumentName,
																						   String status) throws IdsmedBaseException {
		Pair<String, String> emailPair = null;
		List<String> recipients = new ArrayList<>();
		emailPair = emailService.generateRequestToApproveVendorCertificateEmailSubjectAndContent(
				vendorCoordinator,
				attachmentDocumentName,
				status
		);
		Boolean isSendingEmail = emailService.checkSendEmail(vendorCoordinator.getIdsmedAccounts(), EmailTypeEnum.REQUEST_TO_APPROVE_VENDOR_CERTIFICATE_NOTIFICATION);
		if (isSendingEmail) {
			EmailObject emailForAdmin = new RequestToApproveVendorCertificateEmailObject(
					Arrays.asList(vendorCoordinator.getEmail()),
					env.getProperty(PropertiesLabelConst.SMTP_SUPPORT_MAIL_LABEL),
					emailPair
			);

			rabbitTemplate.convertAndSend(env.getProperty(IdsmedQueueConfig.RABBIT_MAIL_SERVICE_PROCESS_QUEUE), emailForAdmin);
		}
		return emailPair;
	}

	private void processWebNotificationRequestToApproveVendorCertificate(List<IdsmedAccount> sender,
																		 List<IdsmedAccount> vendorCoordinator,
																		 Pair<String, String> emailPair) throws IdsmedBaseException {
		notificationService.sendVendorRegistrationPendingReviewNotification(
				sender, vendorCoordinator, emailPair);
	}

	private Pair<String, String> processEmailNotificationVendorProfileCertificateApprovalNotification(Vendor vendor,
																									  String attachmentDocumentName,
																									  String status) throws IdsmedBaseException {
		Pair<String, String> emailPair = null;
		List<String> recipients = new ArrayList<>();
		recipients.add(vendor.getPersonIncharge1Email());
		List<IdsmedAccount> recipientAccounts = new ArrayList<>();
		// Find vendor coordinator
		if (vendor != null && vendor.getVendorCoordinator() != null) {
			IdsmedAccount coordinator = idsmedAccountRepository.findById(vendor.getVendorCoordinator()).get();
			if (coordinator != null) {
				recipientAccounts.add(coordinator);
			}
		}
		if(!StringUtils.isEmpty(vendor.getPersonIncharge2Email())){
			recipients.add(vendor.getPersonIncharge2Email());
		}
		emailPair = emailService.generateVendorProfileCertificateApprovalEmailSubjectAndContent(vendor,
				attachmentDocumentName,
				status
		);

		for(IdsmedAccount recipientAccount : recipientAccounts) {
			Boolean isSendingEmail = emailService.checkSendEmail(recipientAccounts, EmailTypeEnum.VENDOR_PROFILE_CERTIFICATE_APPROVAL_NOTIFICATION);
			if (isSendingEmail) {
				EmailObject emailForAdmin = new VendorProfileCertificateApprovedEmailObject(
						Arrays.asList(recipientAccount.getIdsmedUser().getEmail()),
						env.getProperty(PropertiesLabelConst.SMTP_SUPPORT_MAIL_LABEL),
						emailPair
				);

				rabbitTemplate.convertAndSend(env.getProperty(IdsmedQueueConfig.RABBIT_MAIL_SERVICE_PROCESS_QUEUE), emailForAdmin);
			}
		}
		processEmailNotificationVendorProfileCertificateApprovalNotificationForFirstPersoninChargeAndSecondPersoninCharge(recipients, emailPair);
		return emailPair;
	}

	private void processEmailNotificationVendorProfileCertificateApprovalNotificationForFirstPersoninChargeAndSecondPersoninCharge(List<String> recipients, Pair<String, String> emailPair){
		EmailObject emailForAdmin = new VendorProfileCertificateApprovedEmailObject(
				recipients,
				env.getProperty(PropertiesLabelConst.SMTP_SUPPORT_MAIL_LABEL),
				emailPair
		);
		rabbitTemplate.convertAndSend(env.getProperty(IdsmedQueueConfig.RABBIT_MAIL_SERVICE_PROCESS_QUEUE), emailForAdmin);
	}

	private void processWebNotificationVendorProfileCertificateApproval(IdsmedAccount sender,
																		List<IdsmedAccount> idsmedAccountsForSameCompany,
																		Pair<String, String> emailPair) throws IdsmedBaseException {
		notificationService.sendVendorProfileCertificateNotificationAndSetStatusToSent(
				sender, idsmedAccountsForSameCompany, emailPair);
	}
	private void loggerForAllMethodInThisService(String methodDescription, String methodName, String loginId){
		logger.info(methodDescription, methodName, loginId != null?loginId:null);
	}
}

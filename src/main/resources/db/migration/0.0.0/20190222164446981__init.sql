--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.10
-- Dumped by pg_dump version 9.6.10

-- Started on 2019-02-13 14:40:23 +08

-- SET statement_timeout = 0;
-- SET lock_timeout = 0;
-- SET idle_in_transaction_session_timeout = 0;
-- SET client_encoding = 'UTF8';
-- SET standard_conforming_strings = on;
-- SELECT pg_catalog.set_config('search_path', '', false);
-- SET check_function_bodies = false;
-- SET client_min_messages = warning;
-- SET row_security = off;

--
-- TOC entry 3989 (class 1262 OID 97244)
-- Name: idsmed; Type: DATABASE; Schema: -; Owner: dbadmin
--

-- CREATE DATABASE idsmed WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'C' LC_CTYPE = 'C';
--
--
-- ALTER DATABASE idsmed OWNER TO dbadmin;
--
-- \connect idsmed

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 8 (class 2615 OID 97245)
-- Name: idsmed; Type: SCHEMA; Schema: -; Owner: dbadmin
--

-- CREATE SCHEMA idsmed;


ALTER SCHEMA idsmed OWNER TO dbadmin;

--
-- TOC entry 1 (class 3079 OID 12655)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner:
--

--CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 3991 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner:
--

--COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- TOC entry 366 (class 1255 OID 97246)
-- Name: column_exists(text, text); Type: FUNCTION; Schema: idsmed; Owner: dbadmin
--

CREATE FUNCTION idsmed.column_exists(ptable text, pcolumn text) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
DECLARE result bool;
BEGIN
    -- Does the requested column exist?
    SELECT COUNT(*) INTO result
    FROM information_schema.columns
    WHERE
      table_name = ptable and
      column_name = pcolumn;
    RETURN result;
END$$;


ALTER FUNCTION idsmed.column_exists(ptable text, pcolumn text) OWNER TO dbadmin;

--
-- TOC entry 367 (class 1255 OID 97247)
-- Name: rename_column(text, text, text); Type: FUNCTION; Schema: idsmed; Owner: dbadmin
--

CREATE FUNCTION idsmed.rename_column(ptable text, pcolumn text, new_name text) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
    -- Rename the column if it exists.
    IF column_exists(ptable, pcolumn) THEN
        EXECUTE FORMAT('ALTER TABLE %I RENAME COLUMN %I TO %I;',
            ptable, pcolumn, new_name);
    END IF;
END$$;


ALTER FUNCTION idsmed.rename_column(ptable text, pcolumn text, new_name text) OWNER TO dbadmin;

--
-- TOC entry 368 (class 1255 OID 97248)
-- Name: column_exists(text, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.column_exists(ptable text, pcolumn text) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
DECLARE result bool;
BEGIN
    -- Does the requested column exist?
    SELECT COUNT(*) INTO result
    FROM information_schema.columns
    WHERE
      table_name = ptable and
      column_name = pcolumn;
    RETURN result;
END$$;


ALTER FUNCTION public.column_exists(ptable text, pcolumn text) OWNER TO postgres;

--
-- TOC entry 381 (class 1255 OID 97249)
-- Name: rename_column(text, text, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.rename_column(ptable text, pcolumn text, new_name text) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
    -- Rename the column if it exists.
    IF column_exists(ptable, pcolumn) THEN
        EXECUTE FORMAT('ALTER TABLE %I RENAME COLUMN %I TO %I;',
            ptable, pcolumn, new_name);
    END IF;
END$$;


ALTER FUNCTION public.rename_column(ptable text, pcolumn text, new_name text) OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 186 (class 1259 OID 97250)
-- Name: account_notification_setting; Type: TABLE; Schema: idsmed; Owner: dbadmin
--

CREATE TABLE idsmed.account_notification_setting (
    id bigint NOT NULL,
    idsmed_account_id bigint,
    notification_type integer,
    created_datetime timestamp without time zone DEFAULT now() NOT NULL,
    updated_datetime timestamp without time zone DEFAULT now() NOT NULL,
    created_by bigint DEFAULT '-1'::integer NOT NULL,
    updated_by bigint DEFAULT '-1'::integer NOT NULL,
    status integer DEFAULT 3 NOT NULL
);


ALTER TABLE idsmed.account_notification_setting OWNER TO dbadmin;

--
-- TOC entry 187 (class 1259 OID 97258)
-- Name: account_notification_setting_id_seq; Type: SEQUENCE; Schema: idsmed; Owner: dbadmin
--

CREATE SEQUENCE idsmed.account_notification_setting_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE idsmed.account_notification_setting_id_seq OWNER TO dbadmin;

--
-- TOC entry 3994 (class 0 OID 0)
-- Dependencies: 187
-- Name: account_notification_setting_id_seq; Type: SEQUENCE OWNED BY; Schema: idsmed; Owner: dbadmin
--

ALTER SEQUENCE idsmed.account_notification_setting_id_seq OWNED BY idsmed.account_notification_setting.id;


--
-- TOC entry 188 (class 1259 OID 97260)
-- Name: batch_job_execution; Type: TABLE; Schema: idsmed; Owner: dbadmin
--

CREATE TABLE idsmed.batch_job_execution (
    job_execution_id bigint NOT NULL,
    version bigint,
    job_instance_id bigint NOT NULL,
    create_time timestamp without time zone NOT NULL,
    start_time timestamp without time zone,
    end_time timestamp without time zone,
    status character varying(10),
    exit_code character varying(2500),
    exit_message character varying(2500),
    last_updated timestamp without time zone,
    job_configuration_location character varying(2500)
);


ALTER TABLE idsmed.batch_job_execution OWNER TO dbadmin;

--
-- TOC entry 189 (class 1259 OID 97266)
-- Name: batch_job_execution_context; Type: TABLE; Schema: idsmed; Owner: dbadmin
--

CREATE TABLE idsmed.batch_job_execution_context (
    job_execution_id bigint NOT NULL,
    short_context character varying(2500) NOT NULL,
    serialized_context text
);


ALTER TABLE idsmed.batch_job_execution_context OWNER TO dbadmin;

--
-- TOC entry 190 (class 1259 OID 97272)
-- Name: batch_job_execution_params; Type: TABLE; Schema: idsmed; Owner: dbadmin
--

CREATE TABLE idsmed.batch_job_execution_params (
    job_execution_id bigint NOT NULL,
    type_cd character varying(6) NOT NULL,
    key_name character varying(100) NOT NULL,
    string_val character varying(250),
    date_val timestamp without time zone,
    long_val bigint,
    double_val double precision,
    identifying character(1) NOT NULL
);


ALTER TABLE idsmed.batch_job_execution_params OWNER TO dbadmin;

--
-- TOC entry 191 (class 1259 OID 97275)
-- Name: batch_job_instance; Type: TABLE; Schema: idsmed; Owner: dbadmin
--

CREATE TABLE idsmed.batch_job_instance (
    job_instance_id bigint NOT NULL,
    version bigint,
    job_name character varying(100) NOT NULL,
    job_key character varying(32) NOT NULL
);


ALTER TABLE idsmed.batch_job_instance OWNER TO dbadmin;

--
-- TOC entry 192 (class 1259 OID 97278)
-- Name: batch_step_execution; Type: TABLE; Schema: idsmed; Owner: dbadmin
--

CREATE TABLE idsmed.batch_step_execution (
    step_execution_id bigint NOT NULL,
    version bigint NOT NULL,
    step_name character varying(100) NOT NULL,
    job_execution_id bigint NOT NULL,
    start_time timestamp without time zone NOT NULL,
    end_time timestamp without time zone,
    status character varying(10),
    commit_count bigint,
    read_count bigint,
    filter_count bigint,
    write_count bigint,
    read_skip_count bigint,
    write_skip_count bigint,
    process_skip_count bigint,
    rollback_count bigint,
    exit_code character varying(2500),
    exit_message character varying(2500),
    last_updated timestamp without time zone
);


ALTER TABLE idsmed.batch_step_execution OWNER TO dbadmin;

--
-- TOC entry 193 (class 1259 OID 97284)
-- Name: batch_step_execution_context; Type: TABLE; Schema: idsmed; Owner: dbadmin
--

CREATE TABLE idsmed.batch_step_execution_context (
    step_execution_id bigint NOT NULL,
    short_context character varying(2500) NOT NULL,
    serialized_context text
);


ALTER TABLE idsmed.batch_step_execution_context OWNER TO dbadmin;

--
-- TOC entry 194 (class 1259 OID 97290)
-- Name: best_seller_product_global; Type: TABLE; Schema: idsmed; Owner: dbadmin
--

CREATE TABLE idsmed.best_seller_product_global (
    id bigint NOT NULL,
    product_id bigint,
    frequency integer,
    status integer,
    created_datetime timestamp without time zone DEFAULT now() NOT NULL,
    updated_datetime timestamp without time zone DEFAULT now() NOT NULL,
    created_by bigint DEFAULT '-1'::integer NOT NULL,
    updated_by bigint DEFAULT '-1'::integer NOT NULL
);


ALTER TABLE idsmed.best_seller_product_global OWNER TO dbadmin;

--
-- TOC entry 195 (class 1259 OID 97297)
-- Name: best_seller_product_global_id_seq; Type: SEQUENCE; Schema: idsmed; Owner: dbadmin
--

CREATE SEQUENCE idsmed.best_seller_product_global_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE idsmed.best_seller_product_global_id_seq OWNER TO dbadmin;

--
-- TOC entry 4001 (class 0 OID 0)
-- Dependencies: 195
-- Name: best_seller_product_global_id_seq; Type: SEQUENCE OWNED BY; Schema: idsmed; Owner: dbadmin
--

ALTER SEQUENCE idsmed.best_seller_product_global_id_seq OWNED BY idsmed.best_seller_product_global.id;


--
-- TOC entry 196 (class 1259 OID 97299)
-- Name: company_background_info; Type: TABLE; Schema: idsmed; Owner: dbadmin
--

CREATE TABLE idsmed.company_background_info (
    id bigint NOT NULL,
    company_code character varying(10) NOT NULL,
    annual_sales numeric(18,2),
    no_of_employee bigint,
    primary_line character varying(500),
    mgmt_director_1 character varying(500),
    mgmt_director_2 character varying(500),
    mgmt_director_3 character varying(500),
    mgmt_director_4 character varying(500),
    created_datetime timestamp without time zone DEFAULT now() NOT NULL,
    updated_datetime timestamp without time zone DEFAULT now() NOT NULL,
    created_by bigint DEFAULT '-1'::integer NOT NULL,
    updated_by bigint DEFAULT '-1'::integer NOT NULL,
    status integer DEFAULT 1
);


ALTER TABLE idsmed.company_background_info OWNER TO dbadmin;

--
-- TOC entry 197 (class 1259 OID 97310)
-- Name: company_background_info_id_seq; Type: SEQUENCE; Schema: idsmed; Owner: dbadmin
--

CREATE SEQUENCE idsmed.company_background_info_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE idsmed.company_background_info_id_seq OWNER TO dbadmin;

--
-- TOC entry 4002 (class 0 OID 0)
-- Dependencies: 197
-- Name: company_background_info_id_seq; Type: SEQUENCE OWNED BY; Schema: idsmed; Owner: dbadmin
--

ALTER SEQUENCE idsmed.company_background_info_id_seq OWNED BY idsmed.company_background_info.id;


--
-- TOC entry 198 (class 1259 OID 97312)
-- Name: country; Type: TABLE; Schema: idsmed; Owner: dbadmin
--

CREATE TABLE idsmed.country (
    id bigint NOT NULL,
    english_name character varying(1000),
    chinese_name character varying(1000),
    english_brief_name character varying(500),
    chinese_brief_name character varying(500),
    country_code character varying(50),
    created_datetime timestamp without time zone DEFAULT now() NOT NULL,
    updated_datetime timestamp without time zone DEFAULT now() NOT NULL,
    created_by bigint DEFAULT '-1'::integer NOT NULL,
    updated_by bigint DEFAULT '-1'::integer NOT NULL,
    status integer DEFAULT 1
);


ALTER TABLE idsmed.country OWNER TO dbadmin;

--
-- TOC entry 199 (class 1259 OID 97323)
-- Name: country_id_seq; Type: SEQUENCE; Schema: idsmed; Owner: dbadmin
--

CREATE SEQUENCE idsmed.country_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE idsmed.country_id_seq OWNER TO dbadmin;

--
-- TOC entry 4003 (class 0 OID 0)
-- Dependencies: 199
-- Name: country_id_seq; Type: SEQUENCE OWNED BY; Schema: idsmed; Owner: dbadmin
--

ALTER SEQUENCE idsmed.country_id_seq OWNED BY idsmed.country.id;


--
-- TOC entry 200 (class 1259 OID 97325)
-- Name: currency_setting; Type: TABLE; Schema: idsmed; Owner: dbadmin
--

CREATE TABLE idsmed.currency_setting (
    id bigint NOT NULL,
    country_code character varying(50),
    primary_currency_code character varying(50),
    primary_currency_name character varying,
    created_datetime timestamp without time zone DEFAULT now() NOT NULL,
    updated_datetime timestamp without time zone DEFAULT now() NOT NULL,
    created_by bigint DEFAULT '-1'::integer NOT NULL,
    updated_by bigint DEFAULT '-1'::integer NOT NULL,
    status integer DEFAULT 1
);


ALTER TABLE idsmed.currency_setting OWNER TO dbadmin;

--
-- TOC entry 201 (class 1259 OID 97336)
-- Name: currency_setting_id_seq; Type: SEQUENCE; Schema: idsmed; Owner: dbadmin
--

CREATE SEQUENCE idsmed.currency_setting_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE idsmed.currency_setting_id_seq OWNER TO dbadmin;

--
-- TOC entry 4004 (class 0 OID 0)
-- Dependencies: 201
-- Name: currency_setting_id_seq; Type: SEQUENCE OWNED BY; Schema: idsmed; Owner: dbadmin
--

ALTER SEQUENCE idsmed.currency_setting_id_seq OWNED BY idsmed.currency_setting.id;


--
-- TOC entry 202 (class 1259 OID 97338)
-- Name: customer; Type: TABLE; Schema: idsmed; Owner: dbadmin
--

CREATE TABLE idsmed.customer (
    id bigint NOT NULL,
    customer_id bigint,
    mobile character varying(11),
    nick_name character varying(32),
    real_name character varying(32),
    sex integer,
    birth_date character varying(16),
    email character varying(64),
    head_pic character varying(255),
    status integer DEFAULT 1 NOT NULL,
    id_card_no character varying(32),
    company_name character varying(64),
    company_type integer,
    contact_name character varying(32),
    contact_tel character varying(32),
    telephone character varying(32),
    province_code character varying(16),
    city_code character varying(16),
    district_code character varying(16),
    province_name character varying(16),
    city_name character varying(16),
    district_name character varying(16),
    address character varying(128),
    created_datetime timestamp without time zone DEFAULT now() NOT NULL,
    updated_datetime timestamp without time zone DEFAULT now() NOT NULL,
    created_by bigint DEFAULT '-1'::integer NOT NULL,
    updated_by bigint DEFAULT '-1'::integer NOT NULL
);


ALTER TABLE idsmed.customer OWNER TO dbadmin;

--
-- TOC entry 203 (class 1259 OID 97349)
-- Name: customer_id_seq; Type: SEQUENCE; Schema: idsmed; Owner: dbadmin
--

CREATE SEQUENCE idsmed.customer_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE idsmed.customer_id_seq OWNER TO dbadmin;

--
-- TOC entry 4005 (class 0 OID 0)
-- Dependencies: 203
-- Name: customer_id_seq; Type: SEQUENCE OWNED BY; Schema: idsmed; Owner: dbadmin
--

ALTER SEQUENCE idsmed.customer_id_seq OWNED BY idsmed.customer.id;


--
-- TOC entry 204 (class 1259 OID 97351)
-- Name: email_notification_config; Type: TABLE; Schema: idsmed; Owner: dbadmin
--

CREATE TABLE idsmed.email_notification_config (
    id bigint NOT NULL,
    template_id bigint,
    email_type integer,
    enabled boolean DEFAULT false NOT NULL,
    recipient_list character varying(500),
    country_code character varying(10) NOT NULL,
    created_datetime timestamp without time zone,
    updated_datetime timestamp without time zone,
    created_by bigint DEFAULT '-1'::integer NOT NULL,
    updated_by bigint DEFAULT '-1'::integer NOT NULL,
    status integer DEFAULT 1 NOT NULL,
    title character varying,
    notification_name character varying(100),
    notification_config_type integer
);


ALTER TABLE idsmed.email_notification_config OWNER TO dbadmin;

--
-- TOC entry 205 (class 1259 OID 97361)
-- Name: email_notification_config_id_seq; Type: SEQUENCE; Schema: idsmed; Owner: dbadmin
--

CREATE SEQUENCE idsmed.email_notification_config_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE idsmed.email_notification_config_id_seq OWNER TO dbadmin;

--
-- TOC entry 4006 (class 0 OID 0)
-- Dependencies: 205
-- Name: email_notification_config_id_seq; Type: SEQUENCE OWNED BY; Schema: idsmed; Owner: dbadmin
--

ALTER SEQUENCE idsmed.email_notification_config_id_seq OWNED BY idsmed.email_notification_config.id;


--
-- TOC entry 206 (class 1259 OID 97363)
-- Name: email_template; Type: TABLE; Schema: idsmed; Owner: dbadmin
--

CREATE TABLE idsmed.email_template (
    id bigint NOT NULL,
    template_name character varying(100) NOT NULL,
    subject character varying(100) NOT NULL,
    content text,
    created_datetime timestamp(6) without time zone DEFAULT now() NOT NULL,
    updated_datetime timestamp(6) without time zone DEFAULT now() NOT NULL,
    created_by bigint DEFAULT '-1'::integer NOT NULL,
    updated_by bigint DEFAULT '-1'::integer NOT NULL,
    status integer DEFAULT 1 NOT NULL
);


ALTER TABLE idsmed.email_template OWNER TO dbadmin;

--
-- TOC entry 207 (class 1259 OID 97374)
-- Name: email_template_id_seq; Type: SEQUENCE; Schema: idsmed; Owner: dbadmin
--

CREATE SEQUENCE idsmed.email_template_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE idsmed.email_template_id_seq OWNER TO dbadmin;

--
-- TOC entry 4007 (class 0 OID 0)
-- Dependencies: 207
-- Name: email_template_id_seq; Type: SEQUENCE OWNED BY; Schema: idsmed; Owner: dbadmin
--

ALTER SEQUENCE idsmed.email_template_id_seq OWNED BY idsmed.email_template.id;


--
-- TOC entry 208 (class 1259 OID 97376)
-- Name: favourite_product_brand; Type: TABLE; Schema: idsmed; Owner: dbadmin
--

CREATE TABLE idsmed.favourite_product_brand (
    id bigint NOT NULL,
    user_id bigint,
    brand_id bigint,
    created_datetime timestamp without time zone DEFAULT now() NOT NULL,
    updated_datetime timestamp without time zone DEFAULT now() NOT NULL,
    created_by bigint DEFAULT '-1'::integer NOT NULL,
    updated_by bigint DEFAULT '-1'::integer NOT NULL,
    status integer DEFAULT 1,
    user_third_party character varying(100),
    wedoctor_customer_id bigint
);


ALTER TABLE idsmed.favourite_product_brand OWNER TO dbadmin;

--
-- TOC entry 209 (class 1259 OID 97384)
-- Name: favourite_product_brand_id_seq; Type: SEQUENCE; Schema: idsmed; Owner: dbadmin
--

CREATE SEQUENCE idsmed.favourite_product_brand_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE idsmed.favourite_product_brand_id_seq OWNER TO dbadmin;

--
-- TOC entry 4008 (class 0 OID 0)
-- Dependencies: 209
-- Name: favourite_product_brand_id_seq; Type: SEQUENCE OWNED BY; Schema: idsmed; Owner: dbadmin
--

ALTER SEQUENCE idsmed.favourite_product_brand_id_seq OWNED BY idsmed.favourite_product_brand.id;


--
-- TOC entry 210 (class 1259 OID 97386)
-- Name: favourite_product_care_area; Type: TABLE; Schema: idsmed; Owner: dbadmin
--

CREATE TABLE idsmed.favourite_product_care_area (
    id bigint NOT NULL,
    user_id bigint,
    care_area_id bigint,
    created_datetime timestamp without time zone DEFAULT now() NOT NULL,
    updated_datetime timestamp without time zone DEFAULT now() NOT NULL,
    created_by bigint DEFAULT '-1'::integer NOT NULL,
    updated_by bigint DEFAULT '-1'::integer NOT NULL,
    status integer DEFAULT 1,
    wedoctor_customer_id bigint
);


ALTER TABLE idsmed.favourite_product_care_area OWNER TO dbadmin;

--
-- TOC entry 211 (class 1259 OID 97394)
-- Name: favourite_product_care_area_id_seq; Type: SEQUENCE; Schema: idsmed; Owner: dbadmin
--

CREATE SEQUENCE idsmed.favourite_product_care_area_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE idsmed.favourite_product_care_area_id_seq OWNER TO dbadmin;

--
-- TOC entry 4009 (class 0 OID 0)
-- Dependencies: 211
-- Name: favourite_product_care_area_id_seq; Type: SEQUENCE OWNED BY; Schema: idsmed; Owner: dbadmin
--

ALTER SEQUENCE idsmed.favourite_product_care_area_id_seq OWNED BY idsmed.favourite_product_care_area.id;


--
-- TOC entry 212 (class 1259 OID 97396)
-- Name: file_attachment; Type: TABLE; Schema: idsmed; Owner: dbadmin
--

CREATE TABLE idsmed.file_attachment (
    id bigint NOT NULL,
    vendor_id bigint,
    document_name character varying(100),
    document_type character varying(100),
    document_path text,
    remarks text,
    country_code character varying(10) NOT NULL,
    created_datetime timestamp without time zone DEFAULT now() NOT NULL,
    updated_datetime timestamp without time zone DEFAULT now() NOT NULL,
    created_by bigint DEFAULT '-1'::integer NOT NULL,
    updated_by bigint DEFAULT '-1'::integer NOT NULL,
    status integer DEFAULT 0 NOT NULL,
    vendor_label_config_id bigint,
    original_file_name character varying(1000),
    document_secondary_name character varying(500),
    expiry_date timestamp without time zone,
    is_verify boolean,
    verify_by bigint,
    verify_datetime timestamp without time zone
);


ALTER TABLE idsmed.file_attachment OWNER TO dbadmin;

--
-- TOC entry 213 (class 1259 OID 97407)
-- Name: file_attachment_id_seq; Type: SEQUENCE; Schema: idsmed; Owner: dbadmin
--

CREATE SEQUENCE idsmed.file_attachment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE idsmed.file_attachment_id_seq OWNER TO dbadmin;

--
-- TOC entry 4010 (class 0 OID 0)
-- Dependencies: 213
-- Name: file_attachment_id_seq; Type: SEQUENCE OWNED BY; Schema: idsmed; Owner: dbadmin
--

ALTER SEQUENCE idsmed.file_attachment_id_seq OWNED BY idsmed.file_attachment.id;


--
-- TOC entry 214 (class 1259 OID 97409)
-- Name: flyway_schema_history; Type: TABLE; Schema: idsmed; Owner: dbadmin
--

-- CREATE TABLE idsmed.flyway_schema_history (
--     installed_rank integer NOT NULL,
--     version character varying(50),
--     description character varying(200) NOT NULL,
--     type character varying(20) NOT NULL,
--     script character varying(1000) NOT NULL,
--     checksum integer,
--     installed_by character varying(100) NOT NULL,
--     installed_on timestamp without time zone DEFAULT now() NOT NULL,
--     execution_time integer NOT NULL,
--     success boolean NOT NULL
-- );


ALTER TABLE idsmed.flyway_schema_history OWNER TO dbadmin;

--
-- TOC entry 215 (class 1259 OID 97416)
-- Name: history_user_product_by_search; Type: TABLE; Schema: idsmed; Owner: dbadmin
--

CREATE TABLE idsmed.history_user_product_by_search (
    id bigint NOT NULL,
    user_id bigint,
    product_id bigint,
    frequency integer,
    status integer,
    created_datetime timestamp without time zone DEFAULT now() NOT NULL,
    updated_datetime timestamp without time zone DEFAULT now() NOT NULL,
    created_by bigint DEFAULT '-1'::integer NOT NULL,
    updated_by bigint DEFAULT '-1'::integer NOT NULL,
    third_party_id character varying(100),
    customer_id bigint
);


ALTER TABLE idsmed.history_user_product_by_search OWNER TO dbadmin;

--
-- TOC entry 216 (class 1259 OID 97423)
-- Name: history_user_product_by_search_id_seq; Type: SEQUENCE; Schema: idsmed; Owner: dbadmin
--

CREATE SEQUENCE idsmed.history_user_product_by_search_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE idsmed.history_user_product_by_search_id_seq OWNER TO dbadmin;

--
-- TOC entry 4011 (class 0 OID 0)
-- Dependencies: 216
-- Name: history_user_product_by_search_id_seq; Type: SEQUENCE OWNED BY; Schema: idsmed; Owner: dbadmin
--

ALTER SEQUENCE idsmed.history_user_product_by_search_id_seq OWNED BY idsmed.history_user_product_by_search.id;


--
-- TOC entry 217 (class 1259 OID 97425)
-- Name: idsmed_account; Type: TABLE; Schema: idsmed; Owner: dbadmin
--

CREATE TABLE idsmed.idsmed_account (
    id bigint NOT NULL,
    login_id character varying(255) NOT NULL,
    idsmed_user_id bigint,
    subscriber_id bigint,
    password character varying(200),
    type integer NOT NULL,
    status integer DEFAULT 1,
    created_datetime timestamp without time zone DEFAULT now() NOT NULL,
    updated_datetime timestamp without time zone DEFAULT now() NOT NULL,
    created_by bigint DEFAULT '-1'::integer NOT NULL,
    updated_by bigint DEFAULT '-1'::integer NOT NULL,
    previous_status integer,
    vendor_coordinator boolean
);


ALTER TABLE idsmed.idsmed_account OWNER TO dbadmin;

--
-- TOC entry 218 (class 1259 OID 97433)
-- Name: idsmed_account_id_seq; Type: SEQUENCE; Schema: idsmed; Owner: dbadmin
--

CREATE SEQUENCE idsmed.idsmed_account_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE idsmed.idsmed_account_id_seq OWNER TO dbadmin;

--
-- TOC entry 4012 (class 0 OID 0)
-- Dependencies: 218
-- Name: idsmed_account_id_seq; Type: SEQUENCE OWNED BY; Schema: idsmed; Owner: dbadmin
--

ALTER SEQUENCE idsmed.idsmed_account_id_seq OWNED BY idsmed.idsmed_account.id;


--
-- TOC entry 219 (class 1259 OID 97435)
-- Name: idsmed_account_settings; Type: TABLE; Schema: idsmed; Owner: dbadmin
--

CREATE TABLE idsmed.idsmed_account_settings (
    id bigint NOT NULL,
    account_id bigint,
    current_lang_code character varying(50),
    created_datetime timestamp without time zone DEFAULT now() NOT NULL,
    updated_datetime timestamp without time zone DEFAULT now() NOT NULL,
    created_by bigint DEFAULT '-1'::integer NOT NULL,
    updated_by bigint DEFAULT '-1'::integer NOT NULL,
    status integer DEFAULT 1
);


ALTER TABLE idsmed.idsmed_account_settings OWNER TO dbadmin;

--
-- TOC entry 220 (class 1259 OID 97443)
-- Name: idsmed_account_settings_id_seq; Type: SEQUENCE; Schema: idsmed; Owner: dbadmin
--

CREATE SEQUENCE idsmed.idsmed_account_settings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE idsmed.idsmed_account_settings_id_seq OWNER TO dbadmin;

--
-- TOC entry 4013 (class 0 OID 0)
-- Dependencies: 220
-- Name: idsmed_account_settings_id_seq; Type: SEQUENCE OWNED BY; Schema: idsmed; Owner: dbadmin
--

ALTER SEQUENCE idsmed.idsmed_account_settings_id_seq OWNED BY idsmed.idsmed_account_settings.id;


--
-- TOC entry 221 (class 1259 OID 97445)
-- Name: idsmed_function; Type: TABLE; Schema: idsmed; Owner: dbadmin
--

CREATE TABLE idsmed.idsmed_function (
    id bigint NOT NULL,
    code character varying(100) NOT NULL,
    name character varying(255) NOT NULL,
    description text NOT NULL,
    status integer DEFAULT 1,
    created_datetime timestamp without time zone DEFAULT now() NOT NULL,
    updated_datetime timestamp without time zone DEFAULT now() NOT NULL,
    created_by bigint DEFAULT '-1'::integer NOT NULL,
    updated_by bigint DEFAULT '-1'::integer NOT NULL
);


ALTER TABLE idsmed.idsmed_function OWNER TO dbadmin;

--
-- TOC entry 222 (class 1259 OID 97456)
-- Name: idsmed_function_id_seq; Type: SEQUENCE; Schema: idsmed; Owner: dbadmin
--

CREATE SEQUENCE idsmed.idsmed_function_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE idsmed.idsmed_function_id_seq OWNER TO dbadmin;

--
-- TOC entry 4014 (class 0 OID 0)
-- Dependencies: 222
-- Name: idsmed_function_id_seq; Type: SEQUENCE OWNED BY; Schema: idsmed; Owner: dbadmin
--

ALTER SEQUENCE idsmed.idsmed_function_id_seq OWNED BY idsmed.idsmed_function.id;


--
-- TOC entry 223 (class 1259 OID 97458)
-- Name: idsmed_permission; Type: TABLE; Schema: idsmed; Owner: dbadmin
--

CREATE TABLE idsmed.idsmed_permission (
    id bigint NOT NULL,
    function_id bigint,
    code character varying(100) NOT NULL,
    name character varying(255) NOT NULL,
    description text NOT NULL,
    seq integer NOT NULL,
    status integer DEFAULT 1,
    country_code character varying(10) NOT NULL,
    created_datetime timestamp without time zone DEFAULT now() NOT NULL,
    updated_datetime timestamp without time zone DEFAULT now() NOT NULL,
    created_by bigint DEFAULT '-1'::integer NOT NULL,
    updated_by bigint DEFAULT '-1'::integer NOT NULL
);


ALTER TABLE idsmed.idsmed_permission OWNER TO dbadmin;

--
-- TOC entry 224 (class 1259 OID 97469)
-- Name: idsmed_permission_id_seq; Type: SEQUENCE; Schema: idsmed; Owner: dbadmin
--

CREATE SEQUENCE idsmed.idsmed_permission_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE idsmed.idsmed_permission_id_seq OWNER TO dbadmin;

--
-- TOC entry 4015 (class 0 OID 0)
-- Dependencies: 224
-- Name: idsmed_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: idsmed; Owner: dbadmin
--

ALTER SEQUENCE idsmed.idsmed_permission_id_seq OWNED BY idsmed.idsmed_permission.id;


--
-- TOC entry 225 (class 1259 OID 97471)
-- Name: idsmed_role; Type: TABLE; Schema: idsmed; Owner: dbadmin
--

CREATE TABLE idsmed.idsmed_role (
    id bigint NOT NULL,
    code character varying(100) NOT NULL,
    name character varying(255) NOT NULL,
    description text,
    status integer DEFAULT 1 NOT NULL,
    country_code character varying(10) NOT NULL,
    created_datetime timestamp without time zone,
    updated_datetime timestamp without time zone,
    created_by bigint DEFAULT '-1'::integer NOT NULL,
    updated_by bigint DEFAULT '-1'::integer NOT NULL,
    role_type integer
);


ALTER TABLE idsmed.idsmed_role OWNER TO dbadmin;

--
-- TOC entry 226 (class 1259 OID 97480)
-- Name: idsmed_role_id_seq; Type: SEQUENCE; Schema: idsmed; Owner: dbadmin
--

CREATE SEQUENCE idsmed.idsmed_role_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE idsmed.idsmed_role_id_seq OWNER TO dbadmin;

--
-- TOC entry 4016 (class 0 OID 0)
-- Dependencies: 226
-- Name: idsmed_role_id_seq; Type: SEQUENCE OWNED BY; Schema: idsmed; Owner: dbadmin
--

ALTER SEQUENCE idsmed.idsmed_role_id_seq OWNED BY idsmed.idsmed_role.id;


--
-- TOC entry 227 (class 1259 OID 97482)
-- Name: idsmed_role_permission; Type: TABLE; Schema: idsmed; Owner: dbadmin
--

CREATE TABLE idsmed.idsmed_role_permission (
    id bigint NOT NULL,
    role_id bigint,
    permission_id bigint,
    status integer NOT NULL,
    country_code character varying(10) NOT NULL,
    created_datetime timestamp without time zone,
    updated_datetime timestamp without time zone,
    created_by bigint DEFAULT '-1'::integer NOT NULL,
    updated_by bigint DEFAULT '-1'::integer NOT NULL
);


ALTER TABLE idsmed.idsmed_role_permission OWNER TO dbadmin;

--
-- TOC entry 228 (class 1259 OID 97487)
-- Name: idsmed_role_permission_id_seq; Type: SEQUENCE; Schema: idsmed; Owner: dbadmin
--

CREATE SEQUENCE idsmed.idsmed_role_permission_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE idsmed.idsmed_role_permission_id_seq OWNER TO dbadmin;

--
-- TOC entry 4017 (class 0 OID 0)
-- Dependencies: 228
-- Name: idsmed_role_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: idsmed; Owner: dbadmin
--

ALTER SEQUENCE idsmed.idsmed_role_permission_id_seq OWNED BY idsmed.idsmed_role_permission.id;


--
-- TOC entry 229 (class 1259 OID 97489)
-- Name: idsmed_subscriber_role; Type: TABLE; Schema: idsmed; Owner: dbadmin
--

CREATE TABLE idsmed.idsmed_subscriber_role (
    id bigint NOT NULL,
    subscriber_id bigint,
    role_id bigint,
    created_datetime timestamp without time zone,
    updated_datetime timestamp without time zone,
    created_by bigint DEFAULT '-1'::integer NOT NULL,
    updated_by bigint DEFAULT '-1'::integer NOT NULL,
    status integer
);


ALTER TABLE idsmed.idsmed_subscriber_role OWNER TO dbadmin;

--
-- TOC entry 230 (class 1259 OID 97494)
-- Name: idsmed_subscriber_role_id_seq; Type: SEQUENCE; Schema: idsmed; Owner: dbadmin
--

CREATE SEQUENCE idsmed.idsmed_subscriber_role_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE idsmed.idsmed_subscriber_role_id_seq OWNER TO dbadmin;

--
-- TOC entry 4018 (class 0 OID 0)
-- Dependencies: 230
-- Name: idsmed_subscriber_role_id_seq; Type: SEQUENCE OWNED BY; Schema: idsmed; Owner: dbadmin
--

ALTER SEQUENCE idsmed.idsmed_subscriber_role_id_seq OWNED BY idsmed.idsmed_subscriber_role.id;


--
-- TOC entry 231 (class 1259 OID 97496)
-- Name: idsmed_task; Type: TABLE; Schema: idsmed; Owner: dbadmin
--

CREATE TABLE idsmed.idsmed_task (
    id bigint NOT NULL,
    action_link character varying,
    task_code character varying(255),
    task_type_code integer,
    entity_id bigint,
    done_by character varying(255),
    created_datetime timestamp without time zone DEFAULT now() NOT NULL,
    updated_datetime timestamp without time zone DEFAULT now() NOT NULL,
    created_by bigint DEFAULT '-1'::integer NOT NULL,
    updated_by bigint DEFAULT '-1'::integer NOT NULL,
    status integer DEFAULT 1 NOT NULL,
    description character varying
);


ALTER TABLE idsmed.idsmed_task OWNER TO dbadmin;

--
-- TOC entry 232 (class 1259 OID 97507)
-- Name: idsmed_task_executor; Type: TABLE; Schema: idsmed; Owner: dbadmin
--

CREATE TABLE idsmed.idsmed_task_executor (
    id bigint NOT NULL,
    idsmed_task_id bigint,
    idsmed_account_id bigint,
    created_datetime timestamp without time zone DEFAULT now() NOT NULL,
    updated_datetime timestamp without time zone DEFAULT now() NOT NULL,
    created_by bigint DEFAULT '-1'::integer NOT NULL,
    updated_by bigint DEFAULT '-1'::integer NOT NULL,
    status integer DEFAULT 1 NOT NULL
);


ALTER TABLE idsmed.idsmed_task_executor OWNER TO dbadmin;

--
-- TOC entry 233 (class 1259 OID 97515)
-- Name: idsmed_task_executor_id_seq; Type: SEQUENCE; Schema: idsmed; Owner: dbadmin
--

CREATE SEQUENCE idsmed.idsmed_task_executor_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE idsmed.idsmed_task_executor_id_seq OWNER TO dbadmin;

--
-- TOC entry 4019 (class 0 OID 0)
-- Dependencies: 233
-- Name: idsmed_task_executor_id_seq; Type: SEQUENCE OWNED BY; Schema: idsmed; Owner: dbadmin
--

ALTER SEQUENCE idsmed.idsmed_task_executor_id_seq OWNED BY idsmed.idsmed_task_executor.id;


--
-- TOC entry 234 (class 1259 OID 97517)
-- Name: idsmed_task_id_seq; Type: SEQUENCE; Schema: idsmed; Owner: dbadmin
--

CREATE SEQUENCE idsmed.idsmed_task_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE idsmed.idsmed_task_id_seq OWNER TO dbadmin;

--
-- TOC entry 4020 (class 0 OID 0)
-- Dependencies: 234
-- Name: idsmed_task_id_seq; Type: SEQUENCE OWNED BY; Schema: idsmed; Owner: dbadmin
--

ALTER SEQUENCE idsmed.idsmed_task_id_seq OWNED BY idsmed.idsmed_task.id;


--
-- TOC entry 235 (class 1259 OID 97519)
-- Name: idsmed_user; Type: TABLE; Schema: idsmed; Owner: dbadmin
--

CREATE TABLE idsmed.idsmed_user (
    id bigint NOT NULL,
    first_name character varying(100),
    last_name character varying(200),
    email character varying(200),
    country_code character varying(50),
    status integer DEFAULT 1,
    created_datetime timestamp without time zone,
    updated_datetime timestamp without time zone,
    created_by bigint DEFAULT '-1'::integer NOT NULL,
    updated_by bigint DEFAULT '-1'::integer NOT NULL,
    reject_reason character varying(300),
    company_code character varying(100),
    qq character varying(100),
    wechat character varying(100),
    phone_number character varying(50),
    approved_by bigint,
    approved_datetime timestamp without time zone,
    profile_image_url text,
    type_of_user integer,
    previous_status integer
);


ALTER TABLE idsmed.idsmed_user OWNER TO dbadmin;

--
-- TOC entry 236 (class 1259 OID 97528)
-- Name: idsmed_user_id_seq; Type: SEQUENCE; Schema: idsmed; Owner: dbadmin
--

CREATE SEQUENCE idsmed.idsmed_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE idsmed.idsmed_user_id_seq OWNER TO dbadmin;

--
-- TOC entry 4021 (class 0 OID 0)
-- Dependencies: 236
-- Name: idsmed_user_id_seq; Type: SEQUENCE OWNED BY; Schema: idsmed; Owner: dbadmin
--

ALTER SEQUENCE idsmed.idsmed_user_id_seq OWNED BY idsmed.idsmed_user.id;


--
-- TOC entry 237 (class 1259 OID 97530)
-- Name: idsmed_user_role; Type: TABLE; Schema: idsmed; Owner: dbadmin
--

CREATE TABLE idsmed.idsmed_user_role (
    id bigint NOT NULL,
    user_id bigint,
    role_id bigint,
    created_datetime timestamp without time zone,
    updated_datetime timestamp without time zone,
    created_by bigint DEFAULT '-1'::integer NOT NULL,
    updated_by bigint DEFAULT '-1'::integer NOT NULL,
    status integer
);


ALTER TABLE idsmed.idsmed_user_role OWNER TO dbadmin;

--
-- TOC entry 238 (class 1259 OID 97535)
-- Name: idsmed_user_role_id_seq; Type: SEQUENCE; Schema: idsmed; Owner: dbadmin
--

CREATE SEQUENCE idsmed.idsmed_user_role_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE idsmed.idsmed_user_role_id_seq OWNER TO dbadmin;

--
-- TOC entry 4022 (class 0 OID 0)
-- Dependencies: 238
-- Name: idsmed_user_role_id_seq; Type: SEQUENCE OWNED BY; Schema: idsmed; Owner: dbadmin
--

ALTER SEQUENCE idsmed.idsmed_user_role_id_seq OWNED BY idsmed.idsmed_user_role.id;


--
-- TOC entry 239 (class 1259 OID 97537)
-- Name: keyword_history; Type: TABLE; Schema: idsmed; Owner: dbadmin
--

CREATE TABLE idsmed.keyword_history (
    id bigint NOT NULL,
    user_id bigint,
    keyword character varying(500) NOT NULL,
    created_datetime timestamp without time zone DEFAULT now() NOT NULL,
    updated_datetime timestamp without time zone DEFAULT now() NOT NULL,
    created_by bigint DEFAULT '-1'::integer NOT NULL,
    updated_by bigint DEFAULT '-1'::integer NOT NULL,
    status integer DEFAULT 1,
    wedoctor_customer_id bigint,
    care_area_ids character varying,
    category_ids character varying,
    brand_ids character varying,
    product_ids character varying
);


ALTER TABLE idsmed.keyword_history OWNER TO dbadmin;

--
-- TOC entry 240 (class 1259 OID 97548)
-- Name: keyword_history_id_seq; Type: SEQUENCE; Schema: idsmed; Owner: dbadmin
--

CREATE SEQUENCE idsmed.keyword_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE idsmed.keyword_history_id_seq OWNER TO dbadmin;

--
-- TOC entry 4023 (class 0 OID 0)
-- Dependencies: 240
-- Name: keyword_history_id_seq; Type: SEQUENCE OWNED BY; Schema: idsmed; Owner: dbadmin
--

ALTER SEQUENCE idsmed.keyword_history_id_seq OWNED BY idsmed.keyword_history.id;


--
-- TOC entry 241 (class 1259 OID 97550)
-- Name: mail_sending_log; Type: TABLE; Schema: idsmed; Owner: dbadmin
--

CREATE TABLE idsmed.mail_sending_log (
    id bigint NOT NULL,
    mail_purpose character varying(100),
    sender character varying(100),
    recipients character varying(500),
    result character varying(10),
    error_code integer,
    error_description character varying(500),
    followup_action character varying(500),
    created_datetime timestamp without time zone DEFAULT now() NOT NULL,
    updated_datetime timestamp without time zone DEFAULT now() NOT NULL,
    created_by bigint DEFAULT '-1'::integer NOT NULL,
    updated_by bigint DEFAULT '-1'::integer NOT NULL,
    status integer DEFAULT 1,
    subject text,
    content text
);


ALTER TABLE idsmed.mail_sending_log OWNER TO dbadmin;

--
-- TOC entry 242 (class 1259 OID 97561)
-- Name: mail_sending_log_id_seq; Type: SEQUENCE; Schema: idsmed; Owner: dbadmin
--

CREATE SEQUENCE idsmed.mail_sending_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE idsmed.mail_sending_log_id_seq OWNER TO dbadmin;

--
-- TOC entry 4024 (class 0 OID 0)
-- Dependencies: 242
-- Name: mail_sending_log_id_seq; Type: SEQUENCE OWNED BY; Schema: idsmed; Owner: dbadmin
--

ALTER SEQUENCE idsmed.mail_sending_log_id_seq OWNED BY idsmed.mail_sending_log.id;


--
-- TOC entry 243 (class 1259 OID 97563)
-- Name: notification; Type: TABLE; Schema: idsmed; Owner: dbadmin
--

CREATE TABLE idsmed.notification (
    id bigint NOT NULL,
    idsmed_account_id bigint,
    type integer NOT NULL,
    subject character varying(500),
    content text,
    status integer DEFAULT 0 NOT NULL,
    created_datetime timestamp without time zone DEFAULT now() NOT NULL,
    updated_datetime timestamp without time zone DEFAULT now() NOT NULL,
    created_by bigint DEFAULT '-1'::integer NOT NULL,
    updated_by bigint DEFAULT '-1'::integer NOT NULL
);


ALTER TABLE idsmed.notification OWNER TO dbadmin;

--
-- TOC entry 244 (class 1259 OID 97574)
-- Name: notification_id_seq; Type: SEQUENCE; Schema: idsmed; Owner: dbadmin
--

CREATE SEQUENCE idsmed.notification_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE idsmed.notification_id_seq OWNER TO dbadmin;

--
-- TOC entry 4025 (class 0 OID 0)
-- Dependencies: 244
-- Name: notification_id_seq; Type: SEQUENCE OWNED BY; Schema: idsmed; Owner: dbadmin
--

ALTER SEQUENCE idsmed.notification_id_seq OWNED BY idsmed.notification.id;


--
-- TOC entry 245 (class 1259 OID 97576)
-- Name: notification_recipient; Type: TABLE; Schema: idsmed; Owner: dbadmin
--

CREATE TABLE idsmed.notification_recipient (
    id bigint NOT NULL,
    idsmed_account_id bigint,
    notification_id bigint,
    created_datetime timestamp without time zone DEFAULT now() NOT NULL,
    updated_datetime timestamp without time zone DEFAULT now() NOT NULL,
    created_by bigint DEFAULT '-1'::integer NOT NULL,
    updated_by bigint DEFAULT '-1'::integer NOT NULL,
    status integer DEFAULT 0 NOT NULL
);


ALTER TABLE idsmed.notification_recipient OWNER TO dbadmin;

--
-- TOC entry 246 (class 1259 OID 97584)
-- Name: notification_recipient_id_seq; Type: SEQUENCE; Schema: idsmed; Owner: dbadmin
--

CREATE SEQUENCE idsmed.notification_recipient_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE idsmed.notification_recipient_id_seq OWNER TO dbadmin;

--
-- TOC entry 4026 (class 0 OID 0)
-- Dependencies: 246
-- Name: notification_recipient_id_seq; Type: SEQUENCE OWNED BY; Schema: idsmed; Owner: dbadmin
--

ALTER SEQUENCE idsmed.notification_recipient_id_seq OWNED BY idsmed.notification_recipient.id;


--
-- TOC entry 247 (class 1259 OID 97586)
-- Name: notification_wildcard; Type: TABLE; Schema: idsmed; Owner: dbadmin
--

CREATE TABLE idsmed.notification_wildcard (
    id bigint NOT NULL,
    wildcard_name character varying(100),
    wildcard_value character varying(100),
    created_datetime timestamp without time zone DEFAULT now() NOT NULL,
    updated_datetime timestamp without time zone DEFAULT now() NOT NULL,
    created_by bigint DEFAULT '-1'::integer NOT NULL,
    updated_by bigint DEFAULT '-1'::integer NOT NULL,
    status integer DEFAULT 1 NOT NULL
);


ALTER TABLE idsmed.notification_wildcard OWNER TO dbadmin;

--
-- TOC entry 248 (class 1259 OID 97594)
-- Name: notification_wildcard_id_seq; Type: SEQUENCE; Schema: idsmed; Owner: dbadmin
--

CREATE SEQUENCE idsmed.notification_wildcard_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE idsmed.notification_wildcard_id_seq OWNER TO dbadmin;

--
-- TOC entry 4027 (class 0 OID 0)
-- Dependencies: 248
-- Name: notification_wildcard_id_seq; Type: SEQUENCE OWNED BY; Schema: idsmed; Owner: dbadmin
--

ALTER SEQUENCE idsmed.notification_wildcard_id_seq OWNED BY idsmed.notification_wildcard.id;


--
-- TOC entry 249 (class 1259 OID 97596)
-- Name: od_big_order; Type: TABLE; Schema: idsmed; Owner: dbadmin
--

CREATE TABLE idsmed.od_big_order (
    id bigint NOT NULL,
    big_order_no character varying(32) NOT NULL,
    title character varying(64) NOT NULL,
    user_id integer NOT NULL,
    mobile character varying(16) NOT NULL,
    order_status integer NOT NULL,
    pay_method integer,
    pay_status integer NOT NULL,
    is_self_order integer,
    total_amount numeric(18,2) NOT NULL,
    total_goods_amount numeric(18,2) NOT NULL,
    total_freight numeric(18,2) NOT NULL,
    total_discount_amount numeric(18,2) NOT NULL,
    real_paid_amount numeric(18,2) NOT NULL,
    order_time timestamp without time zone NOT NULL,
    paid_time timestamp without time zone,
    pay_no character varying(32),
    remark character varying(128),
    create_time timestamp without time zone NOT NULL,
    update_time timestamp without time zone,
    status integer DEFAULT 0 NOT NULL,
    created_datetime timestamp without time zone DEFAULT now() NOT NULL,
    updated_datetime timestamp without time zone DEFAULT now() NOT NULL,
    created_by bigint DEFAULT '-1'::integer NOT NULL,
    updated_by bigint DEFAULT '-1'::integer NOT NULL,
    tender_no character varying(100)
);


ALTER TABLE idsmed.od_big_order OWNER TO dbadmin;

--
-- TOC entry 250 (class 1259 OID 97604)
-- Name: od_big_order_id_seq; Type: SEQUENCE; Schema: idsmed; Owner: dbadmin
--

CREATE SEQUENCE idsmed.od_big_order_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE idsmed.od_big_order_id_seq OWNER TO dbadmin;

--
-- TOC entry 4028 (class 0 OID 0)
-- Dependencies: 250
-- Name: od_big_order_id_seq; Type: SEQUENCE OWNED BY; Schema: idsmed; Owner: dbadmin
--

ALTER SEQUENCE idsmed.od_big_order_id_seq OWNED BY idsmed.od_big_order.id;


--
-- TOC entry 251 (class 1259 OID 97606)
-- Name: od_delivery_product; Type: TABLE; Schema: idsmed; Owner: dbadmin
--

CREATE TABLE idsmed.od_delivery_product (
    id bigint NOT NULL,
    od_order_delivery_id bigint,
    goods_id bigint NOT NULL,
    num integer NOT NULL,
    status integer DEFAULT 0 NOT NULL,
    created_datetime timestamp without time zone DEFAULT now() NOT NULL,
    updated_datetime timestamp without time zone DEFAULT now() NOT NULL,
    created_by bigint DEFAULT '-1'::integer NOT NULL,
    updated_by bigint DEFAULT '-1'::integer NOT NULL
);


ALTER TABLE idsmed.od_delivery_product OWNER TO dbadmin;

--
-- TOC entry 252 (class 1259 OID 97614)
-- Name: od_delivery_product_id_seq; Type: SEQUENCE; Schema: idsmed; Owner: dbadmin
--

CREATE SEQUENCE idsmed.od_delivery_product_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE idsmed.od_delivery_product_id_seq OWNER TO dbadmin;

--
-- TOC entry 4029 (class 0 OID 0)
-- Dependencies: 252
-- Name: od_delivery_product_id_seq; Type: SEQUENCE OWNED BY; Schema: idsmed; Owner: dbadmin
--

ALTER SEQUENCE idsmed.od_delivery_product_id_seq OWNED BY idsmed.od_delivery_product.id;


--
-- TOC entry 253 (class 1259 OID 97616)
-- Name: od_order_delivery; Type: TABLE; Schema: idsmed; Owner: dbadmin
--

CREATE TABLE idsmed.od_order_delivery (
    id bigint NOT NULL,
    od_small_order_id bigint,
    small_order_no character varying(32),
    member_id bigint NOT NULL,
    freight numeric(18,2) NOT NULL,
    ship_type integer,
    logis_comp_id integer,
    logis_comp_code character varying(16),
    logis_comp_name character varying(64),
    delivery_no character varying(32),
    mobile character varying(16),
    idcard_no character varying(32),
    remark character varying(128),
    create_time timestamp without time zone NOT NULL,
    mail_status integer,
    mail_update_time timestamp without time zone,
    status integer DEFAULT 0 NOT NULL,
    created_datetime timestamp without time zone DEFAULT now() NOT NULL,
    updated_datetime timestamp without time zone DEFAULT now() NOT NULL,
    created_by bigint DEFAULT '-1'::integer NOT NULL,
    updated_by bigint DEFAULT '-1'::integer NOT NULL,
    supplier_delivery_no character varying(32)
);


ALTER TABLE idsmed.od_order_delivery OWNER TO dbadmin;

--
-- TOC entry 254 (class 1259 OID 97624)
-- Name: od_order_delivery_id_seq; Type: SEQUENCE; Schema: idsmed; Owner: dbadmin
--

CREATE SEQUENCE idsmed.od_order_delivery_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE idsmed.od_order_delivery_id_seq OWNER TO dbadmin;

--
-- TOC entry 4030 (class 0 OID 0)
-- Dependencies: 254
-- Name: od_order_delivery_id_seq; Type: SEQUENCE OWNED BY; Schema: idsmed; Owner: dbadmin
--

ALTER SEQUENCE idsmed.od_order_delivery_id_seq OWNED BY idsmed.od_order_delivery.id;


--
-- TOC entry 255 (class 1259 OID 97626)
-- Name: od_order_delivery_record; Type: TABLE; Schema: idsmed; Owner: dbadmin
--

CREATE TABLE idsmed.od_order_delivery_record (
    id bigint NOT NULL,
    od_order_delivery_id bigint,
    delivery_id bigint,
    delivery_no character varying(32),
    delivery_time character varying(32),
    delivery_info character varying(512),
    status integer DEFAULT 0 NOT NULL,
    created_datetime timestamp without time zone DEFAULT now() NOT NULL,
    updated_datetime timestamp without time zone DEFAULT now() NOT NULL,
    created_by bigint DEFAULT '-1'::integer NOT NULL,
    updated_by bigint DEFAULT '-1'::integer NOT NULL
);


ALTER TABLE idsmed.od_order_delivery_record OWNER TO dbadmin;

--
-- TOC entry 256 (class 1259 OID 97637)
-- Name: od_order_delivery_record_id_seq; Type: SEQUENCE; Schema: idsmed; Owner: dbadmin
--

CREATE SEQUENCE idsmed.od_order_delivery_record_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE idsmed.od_order_delivery_record_id_seq OWNER TO dbadmin;

--
-- TOC entry 4031 (class 0 OID 0)
-- Dependencies: 256
-- Name: od_order_delivery_record_id_seq; Type: SEQUENCE OWNED BY; Schema: idsmed; Owner: dbadmin
--

ALTER SEQUENCE idsmed.od_order_delivery_record_id_seq OWNED BY idsmed.od_order_delivery_record.id;


--
-- TOC entry 257 (class 1259 OID 97639)
-- Name: od_order_item; Type: TABLE; Schema: idsmed; Owner: dbadmin
--

CREATE TABLE idsmed.od_order_item (
    id bigint NOT NULL,
    od_small_order_id bigint,
    small_order_no character varying(32) NOT NULL,
    title character varying(64) NOT NULL,
    goods_id bigint NOT NULL,
    goods_pic character varying(255),
    sku_code character varying(32) NOT NULL,
    property character varying(64) NOT NULL,
    price numeric(8,2) NOT NULL,
    num integer NOT NULL,
    weight double precision,
    item_amount numeric(8,2),
    discount_amount numeric(8,2),
    real_paid_amount numeric(8,2),
    create_time timestamp without time zone NOT NULL,
    update_time timestamp without time zone,
    warehouse_id integer,
    send_out_status integer,
    delivery_id bigint,
    status integer DEFAULT 0 NOT NULL,
    created_datetime timestamp without time zone DEFAULT now() NOT NULL,
    updated_datetime timestamp without time zone DEFAULT now() NOT NULL,
    created_by bigint DEFAULT '-1'::integer NOT NULL,
    updated_by bigint DEFAULT '-1'::integer NOT NULL,
    order_status integer NOT NULL,
    cancel_reason text
);


ALTER TABLE idsmed.od_order_item OWNER TO dbadmin;

--
-- TOC entry 258 (class 1259 OID 97650)
-- Name: od_order_item_id_seq; Type: SEQUENCE; Schema: idsmed; Owner: dbadmin
--

CREATE SEQUENCE idsmed.od_order_item_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE idsmed.od_order_item_id_seq OWNER TO dbadmin;

--
-- TOC entry 4032 (class 0 OID 0)
-- Dependencies: 258
-- Name: od_order_item_id_seq; Type: SEQUENCE OWNED BY; Schema: idsmed; Owner: dbadmin
--

ALTER SEQUENCE idsmed.od_order_item_id_seq OWNED BY idsmed.od_order_item.id;


--
-- TOC entry 259 (class 1259 OID 97652)
-- Name: od_order_receive_address; Type: TABLE; Schema: idsmed; Owner: dbadmin
--

CREATE TABLE idsmed.od_order_receive_address (
    id bigint NOT NULL,
    od_small_order_id bigint,
    small_order_no character varying(32) NOT NULL,
    mobile character varying(16) NOT NULL,
    receiver_name character varying(32) NOT NULL,
    province_code character varying(16) NOT NULL,
    city_code character varying(16),
    district_code character varying(16),
    province_name character varying(16) NOT NULL,
    city_name character varying(16),
    district_name character varying(16),
    address character varying(128),
    idcard_no character varying(32),
    create_time timestamp without time zone NOT NULL,
    status integer DEFAULT 0 NOT NULL,
    created_datetime timestamp without time zone DEFAULT now() NOT NULL,
    updated_datetime timestamp without time zone DEFAULT now() NOT NULL,
    created_by bigint DEFAULT '-1'::integer NOT NULL,
    updated_by bigint DEFAULT '-1'::integer NOT NULL,
    billing_address character varying(1000)
);


ALTER TABLE idsmed.od_order_receive_address OWNER TO dbadmin;

--
-- TOC entry 260 (class 1259 OID 97663)
-- Name: od_order_receive_address_id_seq; Type: SEQUENCE; Schema: idsmed; Owner: dbadmin
--

CREATE SEQUENCE idsmed.od_order_receive_address_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE idsmed.od_order_receive_address_id_seq OWNER TO dbadmin;

--
-- TOC entry 4033 (class 0 OID 0)
-- Dependencies: 260
-- Name: od_order_receive_address_id_seq; Type: SEQUENCE OWNED BY; Schema: idsmed; Owner: dbadmin
--

ALTER SEQUENCE idsmed.od_order_receive_address_id_seq OWNED BY idsmed.od_order_receive_address.id;


--
-- TOC entry 261 (class 1259 OID 97665)
-- Name: od_small_order; Type: TABLE; Schema: idsmed; Owner: dbadmin
--

CREATE TABLE idsmed.od_small_order (
    id bigint NOT NULL,
    od_big_order_id bigint,
    merchant_id bigint NOT NULL,
    big_order_no character varying(32) NOT NULL,
    small_order_no character varying(32) NOT NULL,
    title character varying(64) NOT NULL,
    user_id integer NOT NULL,
    mobile character varying(16) NOT NULL,
    order_status integer NOT NULL,
    pay_method integer,
    pay_status integer NOT NULL,
    delivery_status integer,
    evaluate_status integer,
    total_item_amount numeric(18,2) NOT NULL,
    total_freight numeric(18,2) NOT NULL,
    total_discount_amount numeric(18,2) NOT NULL,
    real_paid_amount numeric(18,2) NOT NULL,
    order_time timestamp without time zone NOT NULL,
    paid_time timestamp without time zone,
    remark character varying(128),
    create_time timestamp without time zone NOT NULL,
    update_time timestamp without time zone,
    status integer DEFAULT 0 NOT NULL,
    created_datetime timestamp without time zone DEFAULT now() NOT NULL,
    updated_datetime timestamp without time zone DEFAULT now() NOT NULL,
    created_by bigint DEFAULT '-1'::integer NOT NULL,
    updated_by bigint DEFAULT '-1'::integer NOT NULL,
    epo character varying(32),
    rpo character varying(32)
);


ALTER TABLE idsmed.od_small_order OWNER TO dbadmin;

--
-- TOC entry 262 (class 1259 OID 97673)
-- Name: od_small_order_id_seq; Type: SEQUENCE; Schema: idsmed; Owner: dbadmin
--

CREATE SEQUENCE idsmed.od_small_order_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE idsmed.od_small_order_id_seq OWNER TO dbadmin;

--
-- TOC entry 4034 (class 0 OID 0)
-- Dependencies: 262
-- Name: od_small_order_id_seq; Type: SEQUENCE OWNED BY; Schema: idsmed; Owner: dbadmin
--

ALTER SEQUENCE idsmed.od_small_order_id_seq OWNED BY idsmed.od_small_order.id;


--
-- TOC entry 263 (class 1259 OID 97675)
-- Name: product; Type: TABLE; Schema: idsmed; Owner: dbadmin
--

CREATE TABLE idsmed.product (
    id bigint NOT NULL,
    product_version integer,
    product_identity character varying(500) NOT NULL,
    product_category_id bigint,
    product_name_primary character varying(1000) NOT NULL,
    product_name_secondary character varying(1000),
    product_code character varying(100),
    product_model character varying(100),
    product_description text,
    vendor_id bigint,
    registration_number character varying(500),
    hash_tag character varying(5000),
    exp_date date,
    unit character varying(100),
    packaging character varying(200),
    min_of_order integer,
    verified_by bigint,
    approved_by bigint,
    remarks character varying(3000),
    country_code character varying(10),
    created_datetime timestamp without time zone DEFAULT now() NOT NULL,
    updated_datetime timestamp without time zone DEFAULT now() NOT NULL,
    created_by bigint DEFAULT '-1'::integer NOT NULL,
    updated_by bigint DEFAULT '-1'::integer NOT NULL,
    status integer DEFAULT 0 NOT NULL,
    price numeric(18,2),
    product_brand_id bigint,
    area character varying(1000),
    approved_datetime timestamp without time zone,
    other_description character varying(1000),
    is_govt_subsidized boolean DEFAULT false,
    previous_status integer,
    can_procure boolean DEFAULT false,
    device_category bigint DEFAULT 1,
    delivery_lead_time character varying(50),
    sku_number character varying,
    product_functionality_and_claims character varying,
    disclaimer character varying,
    instructions_for_use character varying,
    product_website character varying,
    manufacturer_country_code character varying(50),
    manufacturer_province_code character varying(50),
    product_type integer,
    pack_of_size character varying(1000),
    manufacturer_location character varying(1000),
    product_hierarchy_id bigint,
    equipment_type integer
);


ALTER TABLE idsmed.product OWNER TO dbadmin;

--
-- TOC entry 264 (class 1259 OID 97689)
-- Name: product_brand; Type: TABLE; Schema: idsmed; Owner: dbadmin
--

CREATE TABLE idsmed.product_brand (
    id bigint NOT NULL,
    brand_name character varying(100) NOT NULL,
    brand_code character varying(100) NOT NULL,
    country_code character varying(10),
    created_datetime timestamp without time zone DEFAULT now() NOT NULL,
    updated_datetime timestamp without time zone DEFAULT now() NOT NULL,
    created_by bigint DEFAULT '-1'::integer NOT NULL,
    updated_by bigint DEFAULT '-1'::integer NOT NULL,
    status integer DEFAULT 0 NOT NULL,
    area character varying(1000)
);


ALTER TABLE idsmed.product_brand OWNER TO dbadmin;

--
-- TOC entry 265 (class 1259 OID 97700)
-- Name: product_brand_id_seq; Type: SEQUENCE; Schema: idsmed; Owner: dbadmin
--

CREATE SEQUENCE idsmed.product_brand_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE idsmed.product_brand_id_seq OWNER TO dbadmin;

--
-- TOC entry 4035 (class 0 OID 0)
-- Dependencies: 265
-- Name: product_brand_id_seq; Type: SEQUENCE OWNED BY; Schema: idsmed; Owner: dbadmin
--

ALTER SEQUENCE idsmed.product_brand_id_seq OWNED BY idsmed.product_brand.id;


--
-- TOC entry 266 (class 1259 OID 97702)
-- Name: product_care_area; Type: TABLE; Schema: idsmed; Owner: dbadmin
--

CREATE TABLE idsmed.product_care_area (
    id bigint NOT NULL,
    care_area_name_zh_cn character varying(300),
    care_area_name_zh_tw character varying(300),
    care_area_name_th character varying(300),
    care_area_name_id character varying(300),
    care_area_name_vi character varying(300),
    created_datetime timestamp without time zone DEFAULT now() NOT NULL,
    updated_datetime timestamp without time zone DEFAULT now() NOT NULL,
    created_by bigint DEFAULT '-1'::integer NOT NULL,
    updated_by bigint DEFAULT '-1'::integer NOT NULL,
    status integer DEFAULT 0 NOT NULL,
    care_area_name character varying(300),
    document_path text,
    document_url text,
    document_url_wedoctor text
);


ALTER TABLE idsmed.product_care_area OWNER TO dbadmin;

--
-- TOC entry 267 (class 1259 OID 97713)
-- Name: product_care_area_detail; Type: TABLE; Schema: idsmed; Owner: dbadmin
--

CREATE TABLE idsmed.product_care_area_detail (
    id bigint NOT NULL,
    product_id bigint,
    product_care_area_id bigint,
    status integer NOT NULL,
    country_code character varying(10) NOT NULL,
    created_datetime timestamp without time zone,
    updated_datetime timestamp without time zone,
    created_by bigint DEFAULT '-1'::integer NOT NULL,
    updated_by bigint DEFAULT '-1'::integer NOT NULL
);


ALTER TABLE idsmed.product_care_area_detail OWNER TO dbadmin;

--
-- TOC entry 268 (class 1259 OID 97718)
-- Name: product_care_area_detail_id_seq; Type: SEQUENCE; Schema: idsmed; Owner: dbadmin
--

CREATE SEQUENCE idsmed.product_care_area_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE idsmed.product_care_area_detail_id_seq OWNER TO dbadmin;

--
-- TOC entry 4036 (class 0 OID 0)
-- Dependencies: 268
-- Name: product_care_area_detail_id_seq; Type: SEQUENCE OWNED BY; Schema: idsmed; Owner: dbadmin
--

ALTER SEQUENCE idsmed.product_care_area_detail_id_seq OWNED BY idsmed.product_care_area_detail.id;


--
-- TOC entry 269 (class 1259 OID 97720)
-- Name: product_care_area_detail_log; Type: TABLE; Schema: idsmed; Owner: dbadmin
--

CREATE TABLE idsmed.product_care_area_detail_log (
    id bigint NOT NULL,
    product_id bigint,
    product_care_area_id bigint,
    status integer NOT NULL,
    country_code character varying(10),
    created_datetime timestamp without time zone,
    updated_datetime timestamp without time zone,
    created_by bigint DEFAULT '-1'::integer NOT NULL,
    updated_by bigint DEFAULT '-1'::integer NOT NULL,
    version integer
);


ALTER TABLE idsmed.product_care_area_detail_log OWNER TO dbadmin;

--
-- TOC entry 270 (class 1259 OID 97725)
-- Name: product_care_area_detail_log_id_seq; Type: SEQUENCE; Schema: idsmed; Owner: dbadmin
--

CREATE SEQUENCE idsmed.product_care_area_detail_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE idsmed.product_care_area_detail_log_id_seq OWNER TO dbadmin;

--
-- TOC entry 4037 (class 0 OID 0)
-- Dependencies: 270
-- Name: product_care_area_detail_log_id_seq; Type: SEQUENCE OWNED BY; Schema: idsmed; Owner: dbadmin
--

ALTER SEQUENCE idsmed.product_care_area_detail_log_id_seq OWNED BY idsmed.product_care_area_detail_log.id;


--
-- TOC entry 271 (class 1259 OID 97727)
-- Name: product_care_area_id_seq; Type: SEQUENCE; Schema: idsmed; Owner: dbadmin
--

CREATE SEQUENCE idsmed.product_care_area_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE idsmed.product_care_area_id_seq OWNER TO dbadmin;

--
-- TOC entry 4038 (class 0 OID 0)
-- Dependencies: 271
-- Name: product_care_area_id_seq; Type: SEQUENCE OWNED BY; Schema: idsmed; Owner: dbadmin
--

ALTER SEQUENCE idsmed.product_care_area_id_seq OWNED BY idsmed.product_care_area.id;


--
-- TOC entry 272 (class 1259 OID 97729)
-- Name: product_category; Type: TABLE; Schema: idsmed; Owner: dbadmin
--

CREATE TABLE idsmed.product_category (
    id bigint NOT NULL,
    category_name character varying(300),
    category_name_zh_cn character varying(300),
    category_name_zh_tw character varying(300),
    category_name_th character varying(300),
    category_name_id character varying(300),
    category_name_vi character varying(300),
    created_datetime timestamp without time zone DEFAULT now() NOT NULL,
    updated_datetime timestamp without time zone DEFAULT now() NOT NULL,
    created_by bigint DEFAULT '-1'::integer NOT NULL,
    updated_by bigint DEFAULT '-1'::integer NOT NULL,
    status integer DEFAULT 0 NOT NULL
);


ALTER TABLE idsmed.product_category OWNER TO dbadmin;

--
-- TOC entry 273 (class 1259 OID 97740)
-- Name: product_category_id_seq; Type: SEQUENCE; Schema: idsmed; Owner: dbadmin
--

CREATE SEQUENCE idsmed.product_category_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE idsmed.product_category_id_seq OWNER TO dbadmin;

--
-- TOC entry 4039 (class 0 OID 0)
-- Dependencies: 273
-- Name: product_category_id_seq; Type: SEQUENCE OWNED BY; Schema: idsmed; Owner: dbadmin
--

ALTER SEQUENCE idsmed.product_category_id_seq OWNED BY idsmed.product_category.id;


--
-- TOC entry 274 (class 1259 OID 97742)
-- Name: product_certificate_log; Type: TABLE; Schema: idsmed; Owner: dbadmin
--

CREATE TABLE idsmed.product_certificate_log (
    id bigint NOT NULL,
    product_id bigint NOT NULL,
    version integer NOT NULL,
    document_name character varying(100) NOT NULL,
    document_type character varying(100) NOT NULL,
    document_path text NOT NULL,
    remarks text,
    country_code character varying(10),
    created_datetime timestamp without time zone DEFAULT now() NOT NULL,
    updated_datetime timestamp without time zone DEFAULT now() NOT NULL,
    created_by bigint DEFAULT '-1'::integer NOT NULL,
    updated_by bigint DEFAULT '-1'::integer NOT NULL,
    status integer DEFAULT 0 NOT NULL,
    document_url text
);


ALTER TABLE idsmed.product_certificate_log OWNER TO dbadmin;

--
-- TOC entry 275 (class 1259 OID 97753)
-- Name: product_certificate_log_id_seq; Type: SEQUENCE; Schema: idsmed; Owner: dbadmin
--

CREATE SEQUENCE idsmed.product_certificate_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE idsmed.product_certificate_log_id_seq OWNER TO dbadmin;

--
-- TOC entry 4040 (class 0 OID 0)
-- Dependencies: 275
-- Name: product_certificate_log_id_seq; Type: SEQUENCE OWNED BY; Schema: idsmed; Owner: dbadmin
--

ALTER SEQUENCE idsmed.product_certificate_log_id_seq OWNED BY idsmed.product_certificate_log.id;


--
-- TOC entry 276 (class 1259 OID 97755)
-- Name: product_csv_log; Type: TABLE; Schema: idsmed; Owner: dbadmin
--

CREATE TABLE idsmed.product_csv_log (
    id bigint NOT NULL,
    uuid character varying(500) NOT NULL,
    original_file_name character varying(1000),
    file_url text,
    status integer,
    error_description text,
    created_datetime timestamp without time zone DEFAULT now() NOT NULL,
    updated_datetime timestamp without time zone DEFAULT now() NOT NULL,
    created_by bigint DEFAULT '-1'::integer NOT NULL,
    updated_by bigint DEFAULT '-1'::integer NOT NULL,
    error_row_info text,
    product_created_info text,
    total_product_created integer,
    vendor_id bigint,
    vendor_email character varying(100),
    company_code character varying(100)
);


ALTER TABLE idsmed.product_csv_log OWNER TO dbadmin;

--
-- TOC entry 277 (class 1259 OID 97765)
-- Name: product_csv_log_id_seq; Type: SEQUENCE; Schema: idsmed; Owner: dbadmin
--

CREATE SEQUENCE idsmed.product_csv_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE idsmed.product_csv_log_id_seq OWNER TO dbadmin;

--
-- TOC entry 4041 (class 0 OID 0)
-- Dependencies: 277
-- Name: product_csv_log_id_seq; Type: SEQUENCE OWNED BY; Schema: idsmed; Owner: dbadmin
--

ALTER SEQUENCE idsmed.product_csv_log_id_seq OWNED BY idsmed.product_csv_log.id;


--
-- TOC entry 278 (class 1259 OID 97767)
-- Name: product_document_attachment; Type: TABLE; Schema: idsmed; Owner: dbadmin
--

CREATE TABLE idsmed.product_document_attachment (
    id bigint NOT NULL,
    product_id bigint,
    product_label_config_id bigint,
    original_file_name character varying(1000),
    document_name character varying(500),
    document_secondary_name character varying(500),
    expiry_date timestamp(6) without time zone,
    document_type character varying(100),
    document_path text,
    remarks text,
    country_code character varying(10),
    created_datetime timestamp without time zone DEFAULT now() NOT NULL,
    updated_datetime timestamp without time zone DEFAULT now() NOT NULL,
    created_by bigint DEFAULT '-1'::integer NOT NULL,
    updated_by bigint DEFAULT '-1'::integer NOT NULL,
    status integer DEFAULT 1,
    is_verify boolean,
    verify_by bigint,
    verify_datetime timestamp without time zone
);


ALTER TABLE idsmed.product_document_attachment OWNER TO dbadmin;

--
-- TOC entry 279 (class 1259 OID 97778)
-- Name: product_document_attachment_id_seq; Type: SEQUENCE; Schema: idsmed; Owner: dbadmin
--

CREATE SEQUENCE idsmed.product_document_attachment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE idsmed.product_document_attachment_id_seq OWNER TO dbadmin;

--
-- TOC entry 4042 (class 0 OID 0)
-- Dependencies: 279
-- Name: product_document_attachment_id_seq; Type: SEQUENCE OWNED BY; Schema: idsmed; Owner: dbadmin
--

ALTER SEQUENCE idsmed.product_document_attachment_id_seq OWNED BY idsmed.product_document_attachment.id;


--
-- TOC entry 280 (class 1259 OID 97780)
-- Name: product_document_attachment_reminder; Type: TABLE; Schema: idsmed; Owner: dbadmin
--

CREATE TABLE idsmed.product_document_attachment_reminder (
    id bigint NOT NULL,
    product_document_attachment_id bigint,
    days_before integer,
    days_after integer,
    in_expiry_date boolean DEFAULT false,
    by_email boolean DEFAULT true,
    by_web boolean DEFAULT true,
    by_sms boolean DEFAULT true,
    created_datetime timestamp(6) without time zone DEFAULT now() NOT NULL,
    updated_datetime timestamp(6) without time zone DEFAULT now() NOT NULL,
    created_by bigint DEFAULT '-1'::integer NOT NULL,
    updated_by bigint DEFAULT '-1'::integer NOT NULL,
    status integer DEFAULT 1 NOT NULL
);


ALTER TABLE idsmed.product_document_attachment_reminder OWNER TO dbadmin;

--
-- TOC entry 281 (class 1259 OID 97792)
-- Name: product_document_attachment_reminder_id_seq; Type: SEQUENCE; Schema: idsmed; Owner: dbadmin
--

CREATE SEQUENCE idsmed.product_document_attachment_reminder_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE idsmed.product_document_attachment_reminder_id_seq OWNER TO dbadmin;

--
-- TOC entry 4043 (class 0 OID 0)
-- Dependencies: 281
-- Name: product_document_attachment_reminder_id_seq; Type: SEQUENCE OWNED BY; Schema: idsmed; Owner: dbadmin
--

ALTER SEQUENCE idsmed.product_document_attachment_reminder_id_seq OWNED BY idsmed.product_document_attachment_reminder.id;


--
-- TOC entry 282 (class 1259 OID 97794)
-- Name: product_features; Type: TABLE; Schema: idsmed; Owner: dbadmin
--

CREATE TABLE idsmed.product_features (
    id bigint NOT NULL,
    product_id bigint NOT NULL,
    version integer NOT NULL,
    product_label character varying(100) NOT NULL,
    product_value character varying(1000) NOT NULL,
    product_label_sequence integer NOT NULL,
    country_code character varying(10),
    created_datetime timestamp without time zone DEFAULT now() NOT NULL,
    updated_datetime timestamp without time zone DEFAULT now() NOT NULL,
    created_by bigint DEFAULT '-1'::integer NOT NULL,
    updated_by bigint DEFAULT '-1'::integer NOT NULL,
    status integer DEFAULT 0 NOT NULL,
    selling_point boolean DEFAULT false
);


ALTER TABLE idsmed.product_features OWNER TO dbadmin;

--
-- TOC entry 283 (class 1259 OID 97806)
-- Name: product_features_id_seq; Type: SEQUENCE; Schema: idsmed; Owner: dbadmin
--

CREATE SEQUENCE idsmed.product_features_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE idsmed.product_features_id_seq OWNER TO dbadmin;

--
-- TOC entry 4044 (class 0 OID 0)
-- Dependencies: 283
-- Name: product_features_id_seq; Type: SEQUENCE OWNED BY; Schema: idsmed; Owner: dbadmin
--

ALTER SEQUENCE idsmed.product_features_id_seq OWNED BY idsmed.product_features.id;


--
-- TOC entry 284 (class 1259 OID 97808)
-- Name: product_features_log; Type: TABLE; Schema: idsmed; Owner: dbadmin
--

CREATE TABLE idsmed.product_features_log (
    id bigint NOT NULL,
    product_id bigint,
    version integer,
    product_label character varying(100),
    product_value character varying(1000),
    product_label_sequence integer,
    country_code character varying(10),
    created_datetime timestamp without time zone DEFAULT now() NOT NULL,
    updated_datetime timestamp without time zone DEFAULT now() NOT NULL,
    created_by bigint DEFAULT '-1'::integer NOT NULL,
    updated_by bigint DEFAULT '-1'::integer NOT NULL,
    status integer DEFAULT 0 NOT NULL
);


ALTER TABLE idsmed.product_features_log OWNER TO dbadmin;

--
-- TOC entry 285 (class 1259 OID 97819)
-- Name: product_features_log_id_seq; Type: SEQUENCE; Schema: idsmed; Owner: dbadmin
--

CREATE SEQUENCE idsmed.product_features_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE idsmed.product_features_log_id_seq OWNER TO dbadmin;

--
-- TOC entry 4045 (class 0 OID 0)
-- Dependencies: 285
-- Name: product_features_log_id_seq; Type: SEQUENCE OWNED BY; Schema: idsmed; Owner: dbadmin
--

ALTER SEQUENCE idsmed.product_features_log_id_seq OWNED BY idsmed.product_features_log.id;


--
-- TOC entry 286 (class 1259 OID 97821)
-- Name: product_hierarchy; Type: TABLE; Schema: idsmed; Owner: dbadmin
--

CREATE TABLE idsmed.product_hierarchy (
    id bigint NOT NULL,
    name character varying(255),
    code character varying(100),
    status integer DEFAULT 1 NOT NULL,
    created_datetime timestamp without time zone,
    updated_datetime timestamp without time zone,
    created_by bigint DEFAULT '-1'::integer NOT NULL,
    updated_by bigint DEFAULT '-1'::integer NOT NULL,
    name_zh_cn character varying
);


ALTER TABLE idsmed.product_hierarchy OWNER TO dbadmin;

--
-- TOC entry 287 (class 1259 OID 97830)
-- Name: product_hierarchy_id_seq; Type: SEQUENCE; Schema: idsmed; Owner: dbadmin
--

CREATE SEQUENCE idsmed.product_hierarchy_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE idsmed.product_hierarchy_id_seq OWNER TO dbadmin;

--
-- TOC entry 4046 (class 0 OID 0)
-- Dependencies: 287
-- Name: product_hierarchy_id_seq; Type: SEQUENCE OWNED BY; Schema: idsmed; Owner: dbadmin
--

ALTER SEQUENCE idsmed.product_hierarchy_id_seq OWNED BY idsmed.product_hierarchy.id;


--
-- TOC entry 288 (class 1259 OID 97832)
-- Name: product_id_seq; Type: SEQUENCE; Schema: idsmed; Owner: dbadmin
--

CREATE SEQUENCE idsmed.product_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE idsmed.product_id_seq OWNER TO dbadmin;

--
-- TOC entry 4047 (class 0 OID 0)
-- Dependencies: 288
-- Name: product_id_seq; Type: SEQUENCE OWNED BY; Schema: idsmed; Owner: dbadmin
--

ALTER SEQUENCE idsmed.product_id_seq OWNED BY idsmed.product.id;


--
-- TOC entry 289 (class 1259 OID 97834)
-- Name: product_info_config; Type: TABLE; Schema: idsmed; Owner: dbadmin
--

CREATE TABLE idsmed.product_info_config (
    id bigint NOT NULL,
    country_id bigint,
    province_id bigint,
    created_datetime timestamp without time zone DEFAULT now() NOT NULL,
    updated_datetime timestamp without time zone DEFAULT now() NOT NULL,
    created_by bigint DEFAULT '-1'::integer NOT NULL,
    updated_by bigint DEFAULT '-1'::integer NOT NULL,
    status integer DEFAULT 1
);


ALTER TABLE idsmed.product_info_config OWNER TO dbadmin;

--
-- TOC entry 290 (class 1259 OID 97842)
-- Name: product_info_config_id_seq; Type: SEQUENCE; Schema: idsmed; Owner: dbadmin
--

CREATE SEQUENCE idsmed.product_info_config_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE idsmed.product_info_config_id_seq OWNER TO dbadmin;

--
-- TOC entry 4048 (class 0 OID 0)
-- Dependencies: 290
-- Name: product_info_config_id_seq; Type: SEQUENCE OWNED BY; Schema: idsmed; Owner: dbadmin
--

ALTER SEQUENCE idsmed.product_info_config_id_seq OWNED BY idsmed.product_info_config.id;


--
-- TOC entry 291 (class 1259 OID 97844)
-- Name: product_label_config; Type: TABLE; Schema: idsmed; Owner: dbadmin
--

CREATE TABLE idsmed.product_label_config (
    id bigint NOT NULL,
    product_info_config_id bigint,
    label_primary_name character varying(500),
    label_secondary_name character varying(500),
    is_mandatory boolean DEFAULT false,
    is_expired boolean DEFAULT false,
    type integer,
    sequence integer,
    created_datetime timestamp without time zone DEFAULT now() NOT NULL,
    updated_datetime timestamp without time zone DEFAULT now() NOT NULL,
    created_by bigint DEFAULT '-1'::integer NOT NULL,
    updated_by bigint DEFAULT '-1'::integer NOT NULL,
    status integer DEFAULT 1
);


ALTER TABLE idsmed.product_label_config OWNER TO dbadmin;

--
-- TOC entry 292 (class 1259 OID 97857)
-- Name: product_label_config_id_seq; Type: SEQUENCE; Schema: idsmed; Owner: dbadmin
--

CREATE SEQUENCE idsmed.product_label_config_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE idsmed.product_label_config_id_seq OWNER TO dbadmin;

--
-- TOC entry 4049 (class 0 OID 0)
-- Dependencies: 292
-- Name: product_label_config_id_seq; Type: SEQUENCE OWNED BY; Schema: idsmed; Owner: dbadmin
--

ALTER SEQUENCE idsmed.product_label_config_id_seq OWNED BY idsmed.product_label_config.id;


--
-- TOC entry 293 (class 1259 OID 97859)
-- Name: product_log; Type: TABLE; Schema: idsmed; Owner: dbadmin
--

CREATE TABLE idsmed.product_log (
    id bigint NOT NULL,
    product_version integer,
    product_identity character varying(500),
    product_care_area_id bigint,
    product_category_id bigint,
    product_name_primary character varying(1000) NOT NULL,
    product_name_secondary character varying(1000),
    product_code character varying(100),
    product_model character varying(100),
    product_description text,
    vendor_id bigint,
    registration_number character varying(500),
    hash_tag character varying(5000),
    exp_date date,
    unit character varying(100),
    packaging character varying(200),
    min_of_order integer,
    product_brochure_path text,
    product_video_path text,
    verified_by bigint,
    approved_by bigint,
    remarks character varying(3000),
    country_code character varying(10),
    created_datetime timestamp without time zone DEFAULT now() NOT NULL,
    updated_datetime timestamp without time zone DEFAULT now() NOT NULL,
    created_by bigint DEFAULT '-1'::integer NOT NULL,
    updated_by bigint DEFAULT '-1'::integer NOT NULL,
    status integer DEFAULT 0,
    price numeric(18,2),
    product_brand_id bigint,
    product_brochure_url text,
    product_video_url text,
    area character varying(1000),
    other_description character varying(1000),
    is_govt_subsidized boolean DEFAULT false,
    previous_status integer,
    can_procure boolean DEFAULT false,
    device_category bigint DEFAULT 1,
    product_id bigint,
    approved_datetime timestamp without time zone,
    document_attachment_id text,
    delivery_lead_time character varying(50)
);


ALTER TABLE idsmed.product_log OWNER TO dbadmin;

--
-- TOC entry 294 (class 1259 OID 97873)
-- Name: product_log_id_seq; Type: SEQUENCE; Schema: idsmed; Owner: dbadmin
--

CREATE SEQUENCE idsmed.product_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE idsmed.product_log_id_seq OWNER TO dbadmin;

--
-- TOC entry 4050 (class 0 OID 0)
-- Dependencies: 294
-- Name: product_log_id_seq; Type: SEQUENCE OWNED BY; Schema: idsmed; Owner: dbadmin
--

ALTER SEQUENCE idsmed.product_log_id_seq OWNED BY idsmed.product_log.id;


--
-- TOC entry 295 (class 1259 OID 97875)
-- Name: product_media; Type: TABLE; Schema: idsmed; Owner: dbadmin
--

CREATE TABLE idsmed.product_media (
    id bigint NOT NULL,
    product_id bigint NOT NULL,
    version integer NOT NULL,
    document_name character varying,
    document_type character varying(100),
    document_path text,
    document_seq integer NOT NULL,
    remarks text,
    country_code character varying(10),
    created_datetime timestamp without time zone DEFAULT now() NOT NULL,
    updated_datetime timestamp without time zone DEFAULT now() NOT NULL,
    created_by bigint DEFAULT '-1'::integer NOT NULL,
    updated_by bigint DEFAULT '-1'::integer NOT NULL,
    status integer DEFAULT 0 NOT NULL,
    document_url text,
    gid character varying(100),
    customer_url text
);


ALTER TABLE idsmed.product_media OWNER TO dbadmin;

--
-- TOC entry 296 (class 1259 OID 97886)
-- Name: product_media_id_seq; Type: SEQUENCE; Schema: idsmed; Owner: dbadmin
--

CREATE SEQUENCE idsmed.product_media_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE idsmed.product_media_id_seq OWNER TO dbadmin;

--
-- TOC entry 4051 (class 0 OID 0)
-- Dependencies: 296
-- Name: product_media_id_seq; Type: SEQUENCE OWNED BY; Schema: idsmed; Owner: dbadmin
--

ALTER SEQUENCE idsmed.product_media_id_seq OWNED BY idsmed.product_media.id;


--
-- TOC entry 297 (class 1259 OID 97888)
-- Name: product_media_log; Type: TABLE; Schema: idsmed; Owner: dbadmin
--

CREATE TABLE idsmed.product_media_log (
    id bigint NOT NULL,
    product_id bigint,
    version integer,
    document_name character varying(100),
    document_type character varying(100),
    document_path text,
    document_seq integer,
    remarks text,
    country_code character varying(10),
    created_datetime timestamp without time zone DEFAULT now() NOT NULL,
    updated_datetime timestamp without time zone DEFAULT now() NOT NULL,
    created_by bigint DEFAULT '-1'::integer NOT NULL,
    updated_by bigint DEFAULT '-1'::integer NOT NULL,
    status integer DEFAULT 0 NOT NULL,
    document_url text,
    gid character varying(100),
    customer_url text
);


ALTER TABLE idsmed.product_media_log OWNER TO dbadmin;

--
-- TOC entry 298 (class 1259 OID 97899)
-- Name: product_media_log_id_seq; Type: SEQUENCE; Schema: idsmed; Owner: dbadmin
--

CREATE SEQUENCE idsmed.product_media_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE idsmed.product_media_log_id_seq OWNER TO dbadmin;

--
-- TOC entry 4052 (class 0 OID 0)
-- Dependencies: 298
-- Name: product_media_log_id_seq; Type: SEQUENCE OWNED BY; Schema: idsmed; Owner: dbadmin
--

ALTER SEQUENCE idsmed.product_media_log_id_seq OWNED BY idsmed.product_media_log.id;


--
-- TOC entry 299 (class 1259 OID 97901)
-- Name: product_rating; Type: TABLE; Schema: idsmed; Owner: dbadmin
--

CREATE TABLE idsmed.product_rating (
    id bigint NOT NULL,
    product_id bigint,
    average_score double precision,
    total_count_of_rating bigint,
    created_datetime timestamp without time zone DEFAULT now() NOT NULL,
    updated_datetime timestamp without time zone DEFAULT now() NOT NULL,
    created_by bigint DEFAULT '-1'::integer NOT NULL,
    updated_by bigint DEFAULT '-1'::integer NOT NULL,
    status integer DEFAULT 1 NOT NULL,
    total_count_of_five_star bigint,
    total_count_of_four_star bigint,
    total_count_of_three_star bigint,
    total_count_of_two_star bigint,
    total_count_of_one_star bigint,
    total_count_of_zero_star bigint,
    total_of_score double precision
);


ALTER TABLE idsmed.product_rating OWNER TO dbadmin;

--
-- TOC entry 300 (class 1259 OID 97909)
-- Name: product_rating_detail; Type: TABLE; Schema: idsmed; Owner: dbadmin
--

CREATE TABLE idsmed.product_rating_detail (
    id bigint NOT NULL,
    product_id bigint,
    idsmed_account_id bigint,
    product_rating_id bigint,
    score double precision,
    comment character varying(3000),
    created_datetime timestamp without time zone DEFAULT now() NOT NULL,
    updated_datetime timestamp without time zone DEFAULT now() NOT NULL,
    created_by bigint DEFAULT '-1'::integer NOT NULL,
    updated_by bigint DEFAULT '-1'::integer NOT NULL,
    status integer DEFAULT 1 NOT NULL
);


ALTER TABLE idsmed.product_rating_detail OWNER TO dbadmin;

--
-- TOC entry 301 (class 1259 OID 97920)
-- Name: product_rating_detail_id_seq; Type: SEQUENCE; Schema: idsmed; Owner: dbadmin
--

CREATE SEQUENCE idsmed.product_rating_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE idsmed.product_rating_detail_id_seq OWNER TO dbadmin;

--
-- TOC entry 4053 (class 0 OID 0)
-- Dependencies: 301
-- Name: product_rating_detail_id_seq; Type: SEQUENCE OWNED BY; Schema: idsmed; Owner: dbadmin
--

ALTER SEQUENCE idsmed.product_rating_detail_id_seq OWNED BY idsmed.product_rating_detail.id;


--
-- TOC entry 302 (class 1259 OID 97922)
-- Name: product_rating_id_seq; Type: SEQUENCE; Schema: idsmed; Owner: dbadmin
--

CREATE SEQUENCE idsmed.product_rating_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE idsmed.product_rating_id_seq OWNER TO dbadmin;

--
-- TOC entry 4054 (class 0 OID 0)
-- Dependencies: 302
-- Name: product_rating_id_seq; Type: SEQUENCE OWNED BY; Schema: idsmed; Owner: dbadmin
--

ALTER SEQUENCE idsmed.product_rating_id_seq OWNED BY idsmed.product_rating.id;


--
-- TOC entry 303 (class 1259 OID 97924)
-- Name: product_reject; Type: TABLE; Schema: idsmed; Owner: dbadmin
--

CREATE TABLE idsmed.product_reject (
    id bigint NOT NULL,
    product_id bigint,
    version integer,
    remarks text,
    country_code character varying(10),
    created_datetime timestamp without time zone DEFAULT now() NOT NULL,
    updated_datetime timestamp without time zone DEFAULT now() NOT NULL,
    created_by bigint DEFAULT '-1'::integer NOT NULL,
    updated_by bigint DEFAULT '-1'::integer NOT NULL,
    status integer DEFAULT 0 NOT NULL
);


ALTER TABLE idsmed.product_reject OWNER TO dbadmin;

--
-- TOC entry 304 (class 1259 OID 97935)
-- Name: product_reject_id_seq; Type: SEQUENCE; Schema: idsmed; Owner: dbadmin
--

CREATE SEQUENCE idsmed.product_reject_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE idsmed.product_reject_id_seq OWNER TO dbadmin;

--
-- TOC entry 4055 (class 0 OID 0)
-- Dependencies: 304
-- Name: product_reject_id_seq; Type: SEQUENCE OWNED BY; Schema: idsmed; Owner: dbadmin
--

ALTER SEQUENCE idsmed.product_reject_id_seq OWNED BY idsmed.product_reject.id;


--
-- TOC entry 305 (class 1259 OID 97937)
-- Name: product_reject_log; Type: TABLE; Schema: idsmed; Owner: dbadmin
--

CREATE TABLE idsmed.product_reject_log (
    id bigint NOT NULL,
    product_id bigint,
    version integer,
    remarks text,
    country_code character varying(10),
    created_datetime timestamp without time zone DEFAULT now() NOT NULL,
    updated_datetime timestamp without time zone DEFAULT now() NOT NULL,
    created_by bigint DEFAULT '-1'::integer NOT NULL,
    updated_by bigint DEFAULT '-1'::integer NOT NULL,
    status integer DEFAULT 0 NOT NULL
);


ALTER TABLE idsmed.product_reject_log OWNER TO dbadmin;

--
-- TOC entry 306 (class 1259 OID 97948)
-- Name: product_reject_log_id_seq; Type: SEQUENCE; Schema: idsmed; Owner: dbadmin
--

CREATE SEQUENCE idsmed.product_reject_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE idsmed.product_reject_log_id_seq OWNER TO dbadmin;

--
-- TOC entry 4056 (class 0 OID 0)
-- Dependencies: 306
-- Name: product_reject_log_id_seq; Type: SEQUENCE OWNED BY; Schema: idsmed; Owner: dbadmin
--

ALTER SEQUENCE idsmed.product_reject_log_id_seq OWNED BY idsmed.product_reject_log.id;


--
-- TOC entry 307 (class 1259 OID 97950)
-- Name: product_related; Type: TABLE; Schema: idsmed; Owner: dbadmin
--

CREATE TABLE idsmed.product_related (
    id bigint NOT NULL,
    product_id bigint,
    related_product_id bigint,
    created_datetime timestamp without time zone DEFAULT now() NOT NULL,
    updated_datetime timestamp without time zone DEFAULT now() NOT NULL,
    created_by bigint DEFAULT '-1'::integer NOT NULL,
    updated_by bigint DEFAULT '-1'::integer NOT NULL,
    status integer DEFAULT 1
);


ALTER TABLE idsmed.product_related OWNER TO dbadmin;

--
-- TOC entry 308 (class 1259 OID 97958)
-- Name: product_related_id_seq; Type: SEQUENCE; Schema: idsmed; Owner: dbadmin
--

CREATE SEQUENCE idsmed.product_related_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE idsmed.product_related_id_seq OWNER TO dbadmin;

--
-- TOC entry 4057 (class 0 OID 0)
-- Dependencies: 308
-- Name: product_related_id_seq; Type: SEQUENCE OWNED BY; Schema: idsmed; Owner: dbadmin
--

ALTER SEQUENCE idsmed.product_related_id_seq OWNED BY idsmed.product_related.id;


--
-- TOC entry 309 (class 1259 OID 97960)
-- Name: product_second_category; Type: TABLE; Schema: idsmed; Owner: dbadmin
--

CREATE TABLE idsmed.product_second_category (
    id bigint NOT NULL,
    category_name character varying(300),
    category_name_zh_cn character varying(300),
    category_name_zh_tw character varying(300),
    category_name_th character varying(300),
    category_name_id character varying(300),
    category_name_vi character varying(300),
    created_datetime timestamp without time zone DEFAULT now() NOT NULL,
    updated_datetime timestamp without time zone DEFAULT now() NOT NULL,
    created_by bigint DEFAULT '-1'::integer NOT NULL,
    updated_by bigint DEFAULT '-1'::integer NOT NULL,
    status integer DEFAULT 3 NOT NULL
);


ALTER TABLE idsmed.product_second_category OWNER TO dbadmin;

--
-- TOC entry 310 (class 1259 OID 97971)
-- Name: product_second_category_detail; Type: TABLE; Schema: idsmed; Owner: dbadmin
--

CREATE TABLE idsmed.product_second_category_detail (
    id bigint NOT NULL,
    product_id bigint,
    product_second_category_id bigint,
    created_datetime timestamp without time zone DEFAULT now() NOT NULL,
    updated_datetime timestamp without time zone DEFAULT now() NOT NULL,
    created_by bigint DEFAULT '-1'::integer NOT NULL,
    updated_by bigint DEFAULT '-1'::integer NOT NULL,
    status integer NOT NULL
);


ALTER TABLE idsmed.product_second_category_detail OWNER TO dbadmin;

--
-- TOC entry 311 (class 1259 OID 97978)
-- Name: product_second_category_detail_id_seq; Type: SEQUENCE; Schema: idsmed; Owner: dbadmin
--

CREATE SEQUENCE idsmed.product_second_category_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE idsmed.product_second_category_detail_id_seq OWNER TO dbadmin;

--
-- TOC entry 4058 (class 0 OID 0)
-- Dependencies: 311
-- Name: product_second_category_detail_id_seq; Type: SEQUENCE OWNED BY; Schema: idsmed; Owner: dbadmin
--

ALTER SEQUENCE idsmed.product_second_category_detail_id_seq OWNED BY idsmed.product_second_category_detail.id;


--
-- TOC entry 312 (class 1259 OID 97980)
-- Name: product_second_category_id_seq; Type: SEQUENCE; Schema: idsmed; Owner: dbadmin
--

CREATE SEQUENCE idsmed.product_second_category_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE idsmed.product_second_category_id_seq OWNER TO dbadmin;

--
-- TOC entry 4059 (class 0 OID 0)
-- Dependencies: 312
-- Name: product_second_category_id_seq; Type: SEQUENCE OWNED BY; Schema: idsmed; Owner: dbadmin
--

ALTER SEQUENCE idsmed.product_second_category_id_seq OWNED BY idsmed.product_second_category.id;


--
-- TOC entry 313 (class 1259 OID 97982)
-- Name: product_selling_history; Type: TABLE; Schema: idsmed; Owner: dbadmin
--

CREATE TABLE idsmed.product_selling_history (
    id bigint NOT NULL,
    user_id bigint,
    product_id bigint,
    related_product_id bigint,
    purchase_date date,
    current_lang_code character varying(50),
    created_datetime timestamp without time zone DEFAULT now() NOT NULL,
    updated_datetime timestamp without time zone DEFAULT now() NOT NULL,
    created_by bigint DEFAULT '-1'::integer NOT NULL,
    updated_by bigint DEFAULT '-1'::integer NOT NULL,
    status integer DEFAULT 1,
    wedoctor_customer_id bigint
);


ALTER TABLE idsmed.product_selling_history OWNER TO dbadmin;

--
-- TOC entry 314 (class 1259 OID 97990)
-- Name: product_selling_history_id_seq; Type: SEQUENCE; Schema: idsmed; Owner: dbadmin
--

CREATE SEQUENCE idsmed.product_selling_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE idsmed.product_selling_history_id_seq OWNER TO dbadmin;

--
-- TOC entry 4060 (class 0 OID 0)
-- Dependencies: 314
-- Name: product_selling_history_id_seq; Type: SEQUENCE OWNED BY; Schema: idsmed; Owner: dbadmin
--

ALTER SEQUENCE idsmed.product_selling_history_id_seq OWNED BY idsmed.product_selling_history.id;


--
-- TOC entry 315 (class 1259 OID 97992)
-- Name: product_selling_rate; Type: TABLE; Schema: idsmed; Owner: dbadmin
--

CREATE TABLE idsmed.product_selling_rate (
    id bigint NOT NULL,
    product_id bigint,
    purchase_feq bigint,
    related_product_id bigint,
    created_datetime timestamp without time zone DEFAULT now() NOT NULL,
    updated_datetime timestamp without time zone DEFAULT now() NOT NULL,
    created_by bigint DEFAULT '-1'::integer NOT NULL,
    updated_by bigint DEFAULT '-1'::integer NOT NULL,
    status integer DEFAULT 1
);


ALTER TABLE idsmed.product_selling_rate OWNER TO dbadmin;

--
-- TOC entry 316 (class 1259 OID 98000)
-- Name: product_selling_rate_id_seq; Type: SEQUENCE; Schema: idsmed; Owner: dbadmin
--

CREATE SEQUENCE idsmed.product_selling_rate_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE idsmed.product_selling_rate_id_seq OWNER TO dbadmin;

--
-- TOC entry 4061 (class 0 OID 0)
-- Dependencies: 316
-- Name: product_selling_rate_id_seq; Type: SEQUENCE OWNED BY; Schema: idsmed; Owner: dbadmin
--

ALTER SEQUENCE idsmed.product_selling_rate_id_seq OWNED BY idsmed.product_selling_rate.id;


--
-- TOC entry 317 (class 1259 OID 98002)
-- Name: product_tech_feature; Type: TABLE; Schema: idsmed; Owner: dbadmin
--

CREATE TABLE idsmed.product_tech_feature (
    id bigint NOT NULL,
    label_name character varying(100),
    label_type integer,
    created_datetime timestamp without time zone DEFAULT now() NOT NULL,
    updated_datetime timestamp without time zone DEFAULT now() NOT NULL,
    created_by bigint DEFAULT '-1'::integer NOT NULL,
    updated_by bigint DEFAULT '-1'::integer NOT NULL,
    status integer DEFAULT 0 NOT NULL,
    label_name_zh_cn character varying(100),
    label_name_zh_tw character varying(100),
    label_name_vi character varying(100),
    label_name_th character varying(100),
    label_name_id character varying(100),
    for_medical_consumables integer
);


ALTER TABLE idsmed.product_tech_feature OWNER TO dbadmin;

--
-- TOC entry 318 (class 1259 OID 98013)
-- Name: product_tech_feature_id_seq; Type: SEQUENCE; Schema: idsmed; Owner: dbadmin
--

CREATE SEQUENCE idsmed.product_tech_feature_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE idsmed.product_tech_feature_id_seq OWNER TO dbadmin;

--
-- TOC entry 4062 (class 0 OID 0)
-- Dependencies: 318
-- Name: product_tech_feature_id_seq; Type: SEQUENCE OWNED BY; Schema: idsmed; Owner: dbadmin
--

ALTER SEQUENCE idsmed.product_tech_feature_id_seq OWNED BY idsmed.product_tech_feature.id;


--
-- TOC entry 319 (class 1259 OID 98015)
-- Name: product_technical; Type: TABLE; Schema: idsmed; Owner: dbadmin
--

CREATE TABLE idsmed.product_technical (
    id bigint NOT NULL,
    product_id bigint NOT NULL,
    version integer NOT NULL,
    product_label character varying(100) NOT NULL,
    product_value character varying(1000) NOT NULL,
    product_label_sequence integer NOT NULL,
    country_code character varying(10),
    created_datetime timestamp without time zone DEFAULT now() NOT NULL,
    updated_datetime timestamp without time zone DEFAULT now() NOT NULL,
    created_by bigint DEFAULT '-1'::integer NOT NULL,
    updated_by bigint DEFAULT '-1'::integer NOT NULL,
    status integer DEFAULT 0 NOT NULL,
    selling_point boolean DEFAULT false
);


ALTER TABLE idsmed.product_technical OWNER TO dbadmin;

--
-- TOC entry 320 (class 1259 OID 98027)
-- Name: product_technical_id_seq; Type: SEQUENCE; Schema: idsmed; Owner: dbadmin
--

CREATE SEQUENCE idsmed.product_technical_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE idsmed.product_technical_id_seq OWNER TO dbadmin;

--
-- TOC entry 4063 (class 0 OID 0)
-- Dependencies: 320
-- Name: product_technical_id_seq; Type: SEQUENCE OWNED BY; Schema: idsmed; Owner: dbadmin
--

ALTER SEQUENCE idsmed.product_technical_id_seq OWNED BY idsmed.product_technical.id;


--
-- TOC entry 321 (class 1259 OID 98029)
-- Name: product_technical_log; Type: TABLE; Schema: idsmed; Owner: dbadmin
--

CREATE TABLE idsmed.product_technical_log (
    id bigint NOT NULL,
    product_id bigint NOT NULL,
    version integer NOT NULL,
    product_label character varying(100),
    product_value character varying(1000),
    product_label_sequence integer,
    country_code character varying(10),
    created_datetime timestamp without time zone DEFAULT now() NOT NULL,
    updated_datetime timestamp without time zone DEFAULT now() NOT NULL,
    created_by bigint DEFAULT '-1'::integer NOT NULL,
    updated_by bigint DEFAULT '-1'::integer NOT NULL,
    status integer DEFAULT 0 NOT NULL
);


ALTER TABLE idsmed.product_technical_log OWNER TO dbadmin;

--
-- TOC entry 322 (class 1259 OID 98040)
-- Name: product_technical_log_id_seq; Type: SEQUENCE; Schema: idsmed; Owner: dbadmin
--

CREATE SEQUENCE idsmed.product_technical_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE idsmed.product_technical_log_id_seq OWNER TO dbadmin;

--
-- TOC entry 4064 (class 0 OID 0)
-- Dependencies: 322
-- Name: product_technical_log_id_seq; Type: SEQUENCE OWNED BY; Schema: idsmed; Owner: dbadmin
--

ALTER SEQUENCE idsmed.product_technical_log_id_seq OWNED BY idsmed.product_technical_log.id;


--
-- TOC entry 323 (class 1259 OID 98042)
-- Name: product_wishlist; Type: TABLE; Schema: idsmed; Owner: dbadmin
--

CREATE TABLE idsmed.product_wishlist (
    id bigint NOT NULL,
    product_id bigint,
    total_wishlist bigint,
    created_datetime timestamp(6) without time zone DEFAULT now() NOT NULL,
    updated_datetime timestamp(6) without time zone DEFAULT now() NOT NULL,
    created_by bigint DEFAULT '-1'::integer NOT NULL,
    updated_by bigint DEFAULT '-1'::integer NOT NULL,
    status integer DEFAULT 1 NOT NULL
);


ALTER TABLE idsmed.product_wishlist OWNER TO dbadmin;

--
-- TOC entry 324 (class 1259 OID 98050)
-- Name: product_wishlist_detail; Type: TABLE; Schema: idsmed; Owner: dbadmin
--

CREATE TABLE idsmed.product_wishlist_detail (
    id bigint NOT NULL,
    idsmed_account_id bigint,
    product_id bigint,
    product_wishlist_id bigint,
    created_datetime timestamp(6) without time zone DEFAULT now() NOT NULL,
    updated_datetime timestamp(6) without time zone DEFAULT now() NOT NULL,
    created_by bigint DEFAULT '-1'::integer NOT NULL,
    updated_by bigint DEFAULT '-1'::integer NOT NULL,
    status integer DEFAULT 1 NOT NULL,
    wedoctor_customer_id bigint
);


ALTER TABLE idsmed.product_wishlist_detail OWNER TO dbadmin;

--
-- TOC entry 325 (class 1259 OID 98058)
-- Name: product_wishlist_detail_id_seq; Type: SEQUENCE; Schema: idsmed; Owner: dbadmin
--

CREATE SEQUENCE idsmed.product_wishlist_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE idsmed.product_wishlist_detail_id_seq OWNER TO dbadmin;

--
-- TOC entry 4065 (class 0 OID 0)
-- Dependencies: 325
-- Name: product_wishlist_detail_id_seq; Type: SEQUENCE OWNED BY; Schema: idsmed; Owner: dbadmin
--

ALTER SEQUENCE idsmed.product_wishlist_detail_id_seq OWNED BY idsmed.product_wishlist_detail.id;


--
-- TOC entry 326 (class 1259 OID 98060)
-- Name: product_wishlist_id_seq; Type: SEQUENCE; Schema: idsmed; Owner: dbadmin
--

CREATE SEQUENCE idsmed.product_wishlist_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE idsmed.product_wishlist_id_seq OWNER TO dbadmin;

--
-- TOC entry 4066 (class 0 OID 0)
-- Dependencies: 326
-- Name: product_wishlist_id_seq; Type: SEQUENCE OWNED BY; Schema: idsmed; Owner: dbadmin
--

ALTER SEQUENCE idsmed.product_wishlist_id_seq OWNED BY idsmed.product_wishlist.id;


--
-- TOC entry 327 (class 1259 OID 98062)
-- Name: province; Type: TABLE; Schema: idsmed; Owner: dbadmin
--

CREATE TABLE idsmed.province (
    id bigint NOT NULL,
    country_id bigint,
    english_name character varying(1000),
    chinese_name character varying(1000),
    english_brief_name character varying(500),
    chinese_brief_name character varying(500),
    province_code character varying(50),
    created_datetime timestamp without time zone DEFAULT now() NOT NULL,
    updated_datetime timestamp without time zone DEFAULT now() NOT NULL,
    created_by bigint DEFAULT '-1'::integer NOT NULL,
    updated_by bigint DEFAULT '-1'::integer NOT NULL,
    status integer DEFAULT 1
);


ALTER TABLE idsmed.province OWNER TO dbadmin;

--
-- TOC entry 328 (class 1259 OID 98073)
-- Name: province_id_seq; Type: SEQUENCE; Schema: idsmed; Owner: dbadmin
--

CREATE SEQUENCE idsmed.province_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE idsmed.province_id_seq OWNER TO dbadmin;

--
-- TOC entry 4067 (class 0 OID 0)
-- Dependencies: 328
-- Name: province_id_seq; Type: SEQUENCE OWNED BY; Schema: idsmed; Owner: dbadmin
--

ALTER SEQUENCE idsmed.province_id_seq OWNED BY idsmed.province.id;


--
-- TOC entry 329 (class 1259 OID 98075)
-- Name: schedule_setting; Type: TABLE; Schema: idsmed; Owner: dbadmin
--

CREATE TABLE idsmed.schedule_setting (
    id bigint NOT NULL,
    schedule_task_code character varying,
    cron_expression character varying,
    fixed_rate integer,
    created_datetime timestamp without time zone DEFAULT now() NOT NULL,
    updated_datetime timestamp without time zone DEFAULT now() NOT NULL,
    created_by bigint DEFAULT '-1'::integer NOT NULL,
    updated_by bigint DEFAULT '-1'::integer NOT NULL,
    status integer DEFAULT 3 NOT NULL
);


ALTER TABLE idsmed.schedule_setting OWNER TO dbadmin;

--
-- TOC entry 330 (class 1259 OID 98086)
-- Name: schedule_setting_id_seq; Type: SEQUENCE; Schema: idsmed; Owner: dbadmin
--

CREATE SEQUENCE idsmed.schedule_setting_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE idsmed.schedule_setting_id_seq OWNER TO dbadmin;

--
-- TOC entry 4068 (class 0 OID 0)
-- Dependencies: 330
-- Name: schedule_setting_id_seq; Type: SEQUENCE OWNED BY; Schema: idsmed; Owner: dbadmin
--

ALTER SEQUENCE idsmed.schedule_setting_id_seq OWNED BY idsmed.schedule_setting.id;


--
-- TOC entry 331 (class 1259 OID 98088)
-- Name: subscriber; Type: TABLE; Schema: idsmed; Owner: dbadmin
--

CREATE TABLE idsmed.subscriber (
    id bigint NOT NULL,
    name_primary character varying(250) NOT NULL,
    name_secondary character varying(250),
    client_id character varying(250) NOT NULL,
    exp_date timestamp without time zone,
    created_datetime timestamp without time zone DEFAULT now() NOT NULL,
    updated_datetime timestamp without time zone DEFAULT now() NOT NULL,
    created_by bigint DEFAULT '-1'::integer NOT NULL,
    updated_by bigint DEFAULT '-1'::integer NOT NULL,
    status integer DEFAULT 0 NOT NULL,
    country_code character varying(10) NOT NULL,
    primary_lang character varying(10) NOT NULL
);


ALTER TABLE idsmed.subscriber OWNER TO dbadmin;

--
-- TOC entry 332 (class 1259 OID 98099)
-- Name: subscriber_access_right; Type: TABLE; Schema: idsmed; Owner: dbadmin
--

CREATE TABLE idsmed.subscriber_access_right (
    id bigint NOT NULL,
    subscriber_id bigint NOT NULL,
    method character varying(250) NOT NULL,
    country_code character varying(10) NOT NULL,
    created_datetime timestamp without time zone DEFAULT now() NOT NULL,
    updated_datetime timestamp without time zone DEFAULT now() NOT NULL,
    created_by bigint DEFAULT '-1'::integer NOT NULL,
    updated_by bigint DEFAULT '-1'::integer NOT NULL,
    status integer DEFAULT 0 NOT NULL
);


ALTER TABLE idsmed.subscriber_access_right OWNER TO dbadmin;

--
-- TOC entry 333 (class 1259 OID 98107)
-- Name: subscriber_access_right_id_seq; Type: SEQUENCE; Schema: idsmed; Owner: dbadmin
--

CREATE SEQUENCE idsmed.subscriber_access_right_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE idsmed.subscriber_access_right_id_seq OWNER TO dbadmin;

--
-- TOC entry 4069 (class 0 OID 0)
-- Dependencies: 333
-- Name: subscriber_access_right_id_seq; Type: SEQUENCE OWNED BY; Schema: idsmed; Owner: dbadmin
--

ALTER SEQUENCE idsmed.subscriber_access_right_id_seq OWNED BY idsmed.subscriber_access_right.id;


--
-- TOC entry 334 (class 1259 OID 98109)
-- Name: subscriber_id_seq; Type: SEQUENCE; Schema: idsmed; Owner: dbadmin
--

CREATE SEQUENCE idsmed.subscriber_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE idsmed.subscriber_id_seq OWNER TO dbadmin;

--
-- TOC entry 4070 (class 0 OID 0)
-- Dependencies: 334
-- Name: subscriber_id_seq; Type: SEQUENCE OWNED BY; Schema: idsmed; Owner: dbadmin
--

ALTER SEQUENCE idsmed.subscriber_id_seq OWNED BY idsmed.subscriber.id;


--
-- TOC entry 335 (class 1259 OID 98111)
-- Name: subscriber_ip; Type: TABLE; Schema: idsmed; Owner: dbadmin
--

CREATE TABLE idsmed.subscriber_ip (
    id bigint NOT NULL,
    subscriber_id bigint NOT NULL,
    ip_address character varying(25),
    country_code character varying(10) NOT NULL,
    created_datetime timestamp without time zone DEFAULT now() NOT NULL,
    updated_datetime timestamp without time zone DEFAULT now() NOT NULL,
    created_by bigint DEFAULT '-1'::integer NOT NULL,
    updated_by bigint DEFAULT '-1'::integer NOT NULL,
    status integer DEFAULT 0 NOT NULL
);


ALTER TABLE idsmed.subscriber_ip OWNER TO dbadmin;

--
-- TOC entry 336 (class 1259 OID 98119)
-- Name: subscriber_ip_id_seq; Type: SEQUENCE; Schema: idsmed; Owner: dbadmin
--

CREATE SEQUENCE idsmed.subscriber_ip_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE idsmed.subscriber_ip_id_seq OWNER TO dbadmin;

--
-- TOC entry 4071 (class 0 OID 0)
-- Dependencies: 336
-- Name: subscriber_ip_id_seq; Type: SEQUENCE OWNED BY; Schema: idsmed; Owner: dbadmin
--

ALTER SEQUENCE idsmed.subscriber_ip_id_seq OWNED BY idsmed.subscriber_ip.id;


--
-- TOC entry 337 (class 1259 OID 98121)
-- Name: tender_category; Type: TABLE; Schema: idsmed; Owner: dbadmin
--

CREATE TABLE idsmed.tender_category (
    id bigint NOT NULL,
    idsmed_account_id bigint,
    care_area_id bigint,
    created_datetime timestamp without time zone DEFAULT now() NOT NULL,
    updated_datetime timestamp without time zone DEFAULT now() NOT NULL,
    created_by bigint DEFAULT '-1'::integer NOT NULL,
    updated_by bigint DEFAULT '-1'::integer NOT NULL,
    status integer DEFAULT 3 NOT NULL
);


ALTER TABLE idsmed.tender_category OWNER TO dbadmin;

--
-- TOC entry 338 (class 1259 OID 98129)
-- Name: tender_category_id_seq; Type: SEQUENCE; Schema: idsmed; Owner: dbadmin
--

CREATE SEQUENCE idsmed.tender_category_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE idsmed.tender_category_id_seq OWNER TO dbadmin;

--
-- TOC entry 4072 (class 0 OID 0)
-- Dependencies: 338
-- Name: tender_category_id_seq; Type: SEQUENCE OWNED BY; Schema: idsmed; Owner: dbadmin
--

ALTER SEQUENCE idsmed.tender_category_id_seq OWNED BY idsmed.tender_category.id;


--
-- TOC entry 339 (class 1259 OID 98131)
-- Name: token_information; Type: TABLE; Schema: idsmed; Owner: dbadmin
--

CREATE TABLE idsmed.token_information (
    id bigint NOT NULL,
    subscriber_id bigint NOT NULL,
    token_string character varying(250) NOT NULL,
    refresh_token_string character varying(250),
    remarks text,
    token_string_exp_date timestamp without time zone DEFAULT now() NOT NULL,
    refresh_token_string_exp_date timestamp without time zone DEFAULT now() NOT NULL,
    created_datetime timestamp without time zone DEFAULT now() NOT NULL,
    updated_datetime timestamp without time zone DEFAULT now() NOT NULL,
    created_by bigint DEFAULT '-1'::integer NOT NULL,
    updated_by bigint DEFAULT '-1'::integer NOT NULL,
    status integer DEFAULT 0 NOT NULL
);


ALTER TABLE idsmed.token_information OWNER TO dbadmin;

--
-- TOC entry 340 (class 1259 OID 98144)
-- Name: token_information_id_seq; Type: SEQUENCE; Schema: idsmed; Owner: dbadmin
--

CREATE SEQUENCE idsmed.token_information_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE idsmed.token_information_id_seq OWNER TO dbadmin;

--
-- TOC entry 4073 (class 0 OID 0)
-- Dependencies: 340
-- Name: token_information_id_seq; Type: SEQUENCE OWNED BY; Schema: idsmed; Owner: dbadmin
--

ALTER SEQUENCE idsmed.token_information_id_seq OWNED BY idsmed.token_information.id;


--
-- TOC entry 341 (class 1259 OID 98146)
-- Name: vendor; Type: TABLE; Schema: idsmed; Owner: dbadmin
--

CREATE TABLE idsmed.vendor (
    id bigint NOT NULL,
    company_name_primary character varying(200),
    company_name_secondary character varying(200),
    address text,
    phone_number character varying(100),
    email character varying(100),
    person_incharge1_name character varying(100),
    person_incharge1_phone character varying(100),
    person_incharge1_email character varying(100),
    person_incharge1_wechat character varying(100),
    person_incharge1_qq character varying(100),
    person_incharge2_name character varying(100),
    person_incharge2_phone character varying(100),
    person_incharge2_email character varying(100),
    person_incharge2_wechat character varying(100),
    person_incharge2_qq character varying(100),
    company_code character varying(100),
    country_code character varying(10),
    created_datetime timestamp without time zone DEFAULT now() NOT NULL,
    updated_datetime timestamp without time zone DEFAULT now() NOT NULL,
    created_by bigint DEFAULT '-1'::integer NOT NULL,
    updated_by bigint DEFAULT '-1'::integer NOT NULL,
    status integer DEFAULT 0 NOT NULL,
    company_logo_path text,
    approved_by bigint,
    approved_datetime timestamp without time zone,
    type integer,
    account_number character varying(10),
    cust_group character varying(10),
    currency character varying(10),
    vat_gst character varying(50),
    vendor_coordinator bigint,
    bank_of_deposit character varying,
    billing_info_company_name character varying,
    billing_info_telephone character varying,
    bank_account_number character varying
);


ALTER TABLE idsmed.vendor OWNER TO dbadmin;

--
-- TOC entry 342 (class 1259 OID 98157)
-- Name: vendor_action_log; Type: TABLE; Schema: idsmed; Owner: dbadmin
--

CREATE TABLE idsmed.vendor_action_log (
    id bigint NOT NULL,
    vendor_id bigint,
    action_code integer NOT NULL,
    reason character varying,
    detail_describer character varying,
    handler_login_id character varying(255),
    approver_login_id character varying(255),
    ref_code character varying(100),
    created_datetime timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE idsmed.vendor_action_log OWNER TO dbadmin;

--
-- TOC entry 343 (class 1259 OID 98164)
-- Name: vendor_action_log_id_seq; Type: SEQUENCE; Schema: idsmed; Owner: dbadmin
--

CREATE SEQUENCE idsmed.vendor_action_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE idsmed.vendor_action_log_id_seq OWNER TO dbadmin;

--
-- TOC entry 4074 (class 0 OID 0)
-- Dependencies: 343
-- Name: vendor_action_log_id_seq; Type: SEQUENCE OWNED BY; Schema: idsmed; Owner: dbadmin
--

ALTER SEQUENCE idsmed.vendor_action_log_id_seq OWNED BY idsmed.vendor_action_log.id;


--
-- TOC entry 344 (class 1259 OID 98166)
-- Name: vendor_additional_info; Type: TABLE; Schema: idsmed; Owner: dbadmin
--

CREATE TABLE idsmed.vendor_additional_info (
    id bigint NOT NULL,
    vendor_id bigint,
    d_id bigint,
    field_name character varying(500),
    info_value text,
    created_datetime timestamp without time zone DEFAULT now() NOT NULL,
    updated_datetime timestamp without time zone DEFAULT now() NOT NULL,
    created_by bigint DEFAULT '-1'::integer NOT NULL,
    updated_by bigint DEFAULT '-1'::integer NOT NULL,
    status integer DEFAULT 1,
    vendor_label_config_id bigint,
    field_secondary_name character varying(500)
);


ALTER TABLE idsmed.vendor_additional_info OWNER TO dbadmin;

--
-- TOC entry 345 (class 1259 OID 98177)
-- Name: vendor_additional_info_id_seq; Type: SEQUENCE; Schema: idsmed; Owner: dbadmin
--

CREATE SEQUENCE idsmed.vendor_additional_info_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE idsmed.vendor_additional_info_id_seq OWNER TO dbadmin;

--
-- TOC entry 4075 (class 0 OID 0)
-- Dependencies: 345
-- Name: vendor_additional_info_id_seq; Type: SEQUENCE OWNED BY; Schema: idsmed; Owner: dbadmin
--

ALTER SEQUENCE idsmed.vendor_additional_info_id_seq OWNED BY idsmed.vendor_additional_info.id;


--
-- TOC entry 346 (class 1259 OID 98179)
-- Name: vendor_address; Type: TABLE; Schema: idsmed; Owner: dbadmin
--

CREATE TABLE idsmed.vendor_address (
    id bigint NOT NULL,
    vendor_id bigint,
    country_id bigint,
    province_id bigint,
    type integer DEFAULT 1,
    first_address character varying(1000),
    second_address character varying(1000),
    third_address character varying(1000),
    postcode character varying(200),
    from_date timestamp without time zone,
    to_date timestamp without time zone,
    created_datetime timestamp without time zone DEFAULT now() NOT NULL,
    updated_datetime timestamp without time zone DEFAULT now() NOT NULL,
    created_by bigint DEFAULT '-1'::integer NOT NULL,
    updated_by bigint DEFAULT '-1'::integer NOT NULL,
    status integer DEFAULT 1
);


ALTER TABLE idsmed.vendor_address OWNER TO dbadmin;

--
-- TOC entry 347 (class 1259 OID 98191)
-- Name: vendor_address_id_seq; Type: SEQUENCE; Schema: idsmed; Owner: dbadmin
--

CREATE SEQUENCE idsmed.vendor_address_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE idsmed.vendor_address_id_seq OWNER TO dbadmin;

--
-- TOC entry 4076 (class 0 OID 0)
-- Dependencies: 347
-- Name: vendor_address_id_seq; Type: SEQUENCE OWNED BY; Schema: idsmed; Owner: dbadmin
--

ALTER SEQUENCE idsmed.vendor_address_id_seq OWNED BY idsmed.vendor_address.id;


--
-- TOC entry 348 (class 1259 OID 98193)
-- Name: vendor_file_attachment_reminder; Type: TABLE; Schema: idsmed; Owner: dbadmin
--

CREATE TABLE idsmed.vendor_file_attachment_reminder (
    id bigint NOT NULL,
    file_attachment_id bigint,
    days_before integer,
    days_after integer,
    in_expiry_date boolean DEFAULT true,
    by_email boolean DEFAULT true,
    by_web boolean DEFAULT true,
    by_sms boolean DEFAULT false,
    created_datetime timestamp without time zone DEFAULT now() NOT NULL,
    updated_datetime timestamp without time zone DEFAULT now() NOT NULL,
    created_by bigint DEFAULT '-1'::integer NOT NULL,
    updated_by bigint DEFAULT '-1'::integer NOT NULL,
    status integer DEFAULT 1 NOT NULL
);


ALTER TABLE idsmed.vendor_file_attachment_reminder OWNER TO dbadmin;

--
-- TOC entry 349 (class 1259 OID 98205)
-- Name: vendor_file_attachment_reminder_id_seq; Type: SEQUENCE; Schema: idsmed; Owner: dbadmin
--

CREATE SEQUENCE idsmed.vendor_file_attachment_reminder_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE idsmed.vendor_file_attachment_reminder_id_seq OWNER TO dbadmin;

--
-- TOC entry 4077 (class 0 OID 0)
-- Dependencies: 349
-- Name: vendor_file_attachment_reminder_id_seq; Type: SEQUENCE OWNED BY; Schema: idsmed; Owner: dbadmin
--

ALTER SEQUENCE idsmed.vendor_file_attachment_reminder_id_seq OWNED BY idsmed.vendor_file_attachment_reminder.id;


--
-- TOC entry 350 (class 1259 OID 98207)
-- Name: vendor_id_seq; Type: SEQUENCE; Schema: idsmed; Owner: dbadmin
--

CREATE SEQUENCE idsmed.vendor_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE idsmed.vendor_id_seq OWNER TO dbadmin;

--
-- TOC entry 4078 (class 0 OID 0)
-- Dependencies: 350
-- Name: vendor_id_seq; Type: SEQUENCE OWNED BY; Schema: idsmed; Owner: dbadmin
--

ALTER SEQUENCE idsmed.vendor_id_seq OWNED BY idsmed.vendor.id;


--
-- TOC entry 351 (class 1259 OID 98209)
-- Name: vendor_info_config; Type: TABLE; Schema: idsmed; Owner: dbadmin
--

CREATE TABLE idsmed.vendor_info_config (
    id bigint NOT NULL,
    country_id bigint,
    province_id bigint,
    created_datetime timestamp without time zone DEFAULT now() NOT NULL,
    updated_datetime timestamp without time zone DEFAULT now() NOT NULL,
    created_by bigint DEFAULT '-1'::integer NOT NULL,
    updated_by bigint DEFAULT '-1'::integer NOT NULL,
    status integer DEFAULT 1,
    location integer,
    vendor_type integer
);


ALTER TABLE idsmed.vendor_info_config OWNER TO dbadmin;

--
-- TOC entry 352 (class 1259 OID 98217)
-- Name: vendor_info_config_id_seq; Type: SEQUENCE; Schema: idsmed; Owner: dbadmin
--

CREATE SEQUENCE idsmed.vendor_info_config_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE idsmed.vendor_info_config_id_seq OWNER TO dbadmin;

--
-- TOC entry 4079 (class 0 OID 0)
-- Dependencies: 352
-- Name: vendor_info_config_id_seq; Type: SEQUENCE OWNED BY; Schema: idsmed; Owner: dbadmin
--

ALTER SEQUENCE idsmed.vendor_info_config_id_seq OWNED BY idsmed.vendor_info_config.id;


--
-- TOC entry 353 (class 1259 OID 98219)
-- Name: vendor_label_config; Type: TABLE; Schema: idsmed; Owner: dbadmin
--

CREATE TABLE idsmed.vendor_label_config (
    id bigint NOT NULL,
    vendor_info_config_id bigint,
    label_primary_name character varying(500),
    label_secondary_name character varying(500),
    is_mandatory boolean DEFAULT false,
    is_expired boolean DEFAULT false,
    type integer,
    sequence integer,
    created_datetime timestamp without time zone DEFAULT now() NOT NULL,
    updated_datetime timestamp without time zone DEFAULT now() NOT NULL,
    created_by bigint DEFAULT '-1'::integer NOT NULL,
    updated_by bigint DEFAULT '-1'::integer NOT NULL,
    status integer DEFAULT 1
);


ALTER TABLE idsmed.vendor_label_config OWNER TO dbadmin;

--
-- TOC entry 354 (class 1259 OID 98232)
-- Name: vendor_label_config_id_seq; Type: SEQUENCE; Schema: idsmed; Owner: dbadmin
--

CREATE SEQUENCE idsmed.vendor_label_config_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE idsmed.vendor_label_config_id_seq OWNER TO dbadmin;

--
-- TOC entry 4080 (class 0 OID 0)
-- Dependencies: 354
-- Name: vendor_label_config_id_seq; Type: SEQUENCE OWNED BY; Schema: idsmed; Owner: dbadmin
--

ALTER SEQUENCE idsmed.vendor_label_config_id_seq OWNED BY idsmed.vendor_label_config.id;


--
-- TOC entry 355 (class 1259 OID 98234)
-- Name: vendor_reject; Type: TABLE; Schema: idsmed; Owner: dbadmin
--

CREATE TABLE idsmed.vendor_reject (
    id bigint NOT NULL,
    vendor_id bigint,
    remarks text,
    country_code character varying(10),
    created_datetime timestamp without time zone DEFAULT now() NOT NULL,
    updated_datetime timestamp without time zone DEFAULT now() NOT NULL,
    created_by bigint DEFAULT '-1'::integer NOT NULL,
    updated_by bigint DEFAULT '-1'::integer NOT NULL,
    status integer DEFAULT 0 NOT NULL
);


ALTER TABLE idsmed.vendor_reject OWNER TO dbadmin;

--
-- TOC entry 356 (class 1259 OID 98245)
-- Name: vendor_reject_id_seq; Type: SEQUENCE; Schema: idsmed; Owner: dbadmin
--

CREATE SEQUENCE idsmed.vendor_reject_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE idsmed.vendor_reject_id_seq OWNER TO dbadmin;

--
-- TOC entry 4081 (class 0 OID 0)
-- Dependencies: 356
-- Name: vendor_reject_id_seq; Type: SEQUENCE OWNED BY; Schema: idsmed; Owner: dbadmin
--

ALTER SEQUENCE idsmed.vendor_reject_id_seq OWNED BY idsmed.vendor_reject.id;


--
-- TOC entry 357 (class 1259 OID 98247)
-- Name: verifying_work_flow_setting; Type: TABLE; Schema: idsmed; Owner: dbadmin
--

CREATE TABLE idsmed.verifying_work_flow_setting (
    id bigint NOT NULL,
    country_code character varying(50),
    work_flow_code character varying(50),
    process_nodes character varying,
    description character varying,
    created_datetime timestamp without time zone DEFAULT now() NOT NULL,
    updated_datetime timestamp without time zone DEFAULT now() NOT NULL,
    created_by bigint DEFAULT '-1'::integer NOT NULL,
    updated_by bigint DEFAULT '-1'::integer NOT NULL,
    status integer DEFAULT 1
);


ALTER TABLE idsmed.verifying_work_flow_setting OWNER TO dbadmin;

--
-- TOC entry 358 (class 1259 OID 98258)
-- Name: verifying_work_flow_setting_id_seq; Type: SEQUENCE; Schema: idsmed; Owner: dbadmin
--

CREATE SEQUENCE idsmed.verifying_work_flow_setting_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE idsmed.verifying_work_flow_setting_id_seq OWNER TO dbadmin;

--
-- TOC entry 4082 (class 0 OID 0)
-- Dependencies: 358
-- Name: verifying_work_flow_setting_id_seq; Type: SEQUENCE OWNED BY; Schema: idsmed; Owner: dbadmin
--

ALTER SEQUENCE idsmed.verifying_work_flow_setting_id_seq OWNED BY idsmed.verifying_work_flow_setting.id;


--
-- TOC entry 359 (class 1259 OID 98260)
-- Name: webservice_log; Type: TABLE; Schema: idsmed; Owner: dbadmin
--

CREATE TABLE idsmed.webservice_log (
    id bigint NOT NULL,
    method character varying(500) NOT NULL,
    ip_add character varying(16) NOT NULL,
    parameter text,
    created_datetime timestamp without time zone DEFAULT now() NOT NULL,
    updated_datetime timestamp without time zone DEFAULT now() NOT NULL,
    created_by bigint DEFAULT '-1'::integer NOT NULL,
    updated_by bigint DEFAULT '-1'::integer NOT NULL,
    status integer DEFAULT 0 NOT NULL
);


ALTER TABLE idsmed.webservice_log OWNER TO dbadmin;

--
-- TOC entry 360 (class 1259 OID 98271)
-- Name: webservice_log_id_seq; Type: SEQUENCE; Schema: idsmed; Owner: dbadmin
--

CREATE SEQUENCE idsmed.webservice_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE idsmed.webservice_log_id_seq OWNER TO dbadmin;

--
-- TOC entry 4083 (class 0 OID 0)
-- Dependencies: 360
-- Name: webservice_log_id_seq; Type: SEQUENCE OWNED BY; Schema: idsmed; Owner: dbadmin
--

ALTER SEQUENCE idsmed.webservice_log_id_seq OWNED BY idsmed.webservice_log.id;


--
-- TOC entry 361 (class 1259 OID 98273)
-- Name: idsmed_function; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.idsmed_function (
    id bigint NOT NULL,
    code character varying(100) NOT NULL,
    name character varying(255) NOT NULL,
    description character varying(1000) NOT NULL,
    status integer DEFAULT 1,
    created_datetime timestamp without time zone DEFAULT now() NOT NULL,
    updated_datetime timestamp without time zone DEFAULT now() NOT NULL,
    created_by bigint DEFAULT '-1'::integer NOT NULL,
    updated_by bigint DEFAULT '-1'::integer NOT NULL
);


ALTER TABLE public.idsmed_function OWNER TO postgres;

--
-- TOC entry 362 (class 1259 OID 98284)
-- Name: idsmed_function_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.idsmed_function_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.idsmed_function_id_seq OWNER TO postgres;

--
-- TOC entry 4085 (class 0 OID 0)
-- Dependencies: 362
-- Name: idsmed_function_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.idsmed_function_id_seq OWNED BY public.idsmed_function.id;


--
-- TOC entry 363 (class 1259 OID 98286)
-- Name: idsmed_permission; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.idsmed_permission (
    id bigint NOT NULL,
    function_id bigint,
    code character varying(100) NOT NULL,
    name character varying(255) NOT NULL,
    description character varying(1000) NOT NULL,
    status integer DEFAULT 1,
    created_datetime timestamp without time zone DEFAULT now() NOT NULL,
    updated_datetime timestamp without time zone DEFAULT now() NOT NULL,
    created_by bigint DEFAULT '-1'::integer NOT NULL,
    updated_by bigint DEFAULT '-1'::integer NOT NULL
);


ALTER TABLE public.idsmed_permission OWNER TO postgres;

--
-- TOC entry 364 (class 1259 OID 98297)
-- Name: idsmed_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.idsmed_permission_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.idsmed_permission_id_seq OWNER TO postgres;

--
-- TOC entry 4088 (class 0 OID 0)
-- Dependencies: 364
-- Name: idsmed_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.idsmed_permission_id_seq OWNED BY public.idsmed_permission.id;


--
-- TOC entry 365 (class 1259 OID 98299)
-- Name: webservice_log; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.webservice_log (
    id bigint NOT NULL,
    created_by bigint NOT NULL,
    created_datetime timestamp without time zone,
    status integer,
    updated_by bigint NOT NULL,
    updated_datetime timestamp without time zone,
    ip_add character varying(16) NOT NULL,
    method character varying(500) NOT NULL,
    parameter character varying(1000)
);


ALTER TABLE public.webservice_log OWNER TO postgres;

--
-- TOC entry 2878 (class 2604 OID 98305)
-- Name: account_notification_setting id; Type: DEFAULT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.account_notification_setting ALTER COLUMN id SET DEFAULT nextval('idsmed.account_notification_setting_id_seq'::regclass);


--
-- TOC entry 2883 (class 2604 OID 98306)
-- Name: best_seller_product_global id; Type: DEFAULT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.best_seller_product_global ALTER COLUMN id SET DEFAULT nextval('idsmed.best_seller_product_global_id_seq'::regclass);


--
-- TOC entry 2889 (class 2604 OID 98307)
-- Name: company_background_info id; Type: DEFAULT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.company_background_info ALTER COLUMN id SET DEFAULT nextval('idsmed.company_background_info_id_seq'::regclass);


--
-- TOC entry 2895 (class 2604 OID 98308)
-- Name: country id; Type: DEFAULT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.country ALTER COLUMN id SET DEFAULT nextval('idsmed.country_id_seq'::regclass);


--
-- TOC entry 2901 (class 2604 OID 98309)
-- Name: currency_setting id; Type: DEFAULT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.currency_setting ALTER COLUMN id SET DEFAULT nextval('idsmed.currency_setting_id_seq'::regclass);


--
-- TOC entry 2907 (class 2604 OID 98310)
-- Name: customer id; Type: DEFAULT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.customer ALTER COLUMN id SET DEFAULT nextval('idsmed.customer_id_seq'::regclass);


--
-- TOC entry 2912 (class 2604 OID 98311)
-- Name: email_notification_config id; Type: DEFAULT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.email_notification_config ALTER COLUMN id SET DEFAULT nextval('idsmed.email_notification_config_id_seq'::regclass);


--
-- TOC entry 2918 (class 2604 OID 98312)
-- Name: email_template id; Type: DEFAULT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.email_template ALTER COLUMN id SET DEFAULT nextval('idsmed.email_template_id_seq'::regclass);


--
-- TOC entry 2924 (class 2604 OID 98313)
-- Name: favourite_product_brand id; Type: DEFAULT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.favourite_product_brand ALTER COLUMN id SET DEFAULT nextval('idsmed.favourite_product_brand_id_seq'::regclass);


--
-- TOC entry 2930 (class 2604 OID 98314)
-- Name: favourite_product_care_area id; Type: DEFAULT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.favourite_product_care_area ALTER COLUMN id SET DEFAULT nextval('idsmed.favourite_product_care_area_id_seq'::regclass);


--
-- TOC entry 2936 (class 2604 OID 98315)
-- Name: file_attachment id; Type: DEFAULT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.file_attachment ALTER COLUMN id SET DEFAULT nextval('idsmed.file_attachment_id_seq'::regclass);


--
-- TOC entry 2942 (class 2604 OID 98316)
-- Name: history_user_product_by_search id; Type: DEFAULT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.history_user_product_by_search ALTER COLUMN id SET DEFAULT nextval('idsmed.history_user_product_by_search_id_seq'::regclass);


--
-- TOC entry 2948 (class 2604 OID 98317)
-- Name: idsmed_account id; Type: DEFAULT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.idsmed_account ALTER COLUMN id SET DEFAULT nextval('idsmed.idsmed_account_id_seq'::regclass);


--
-- TOC entry 2954 (class 2604 OID 98318)
-- Name: idsmed_account_settings id; Type: DEFAULT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.idsmed_account_settings ALTER COLUMN id SET DEFAULT nextval('idsmed.idsmed_account_settings_id_seq'::regclass);


--
-- TOC entry 2960 (class 2604 OID 98319)
-- Name: idsmed_function id; Type: DEFAULT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.idsmed_function ALTER COLUMN id SET DEFAULT nextval('idsmed.idsmed_function_id_seq'::regclass);


--
-- TOC entry 2966 (class 2604 OID 98320)
-- Name: idsmed_permission id; Type: DEFAULT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.idsmed_permission ALTER COLUMN id SET DEFAULT nextval('idsmed.idsmed_permission_id_seq'::regclass);


--
-- TOC entry 2970 (class 2604 OID 98321)
-- Name: idsmed_role id; Type: DEFAULT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.idsmed_role ALTER COLUMN id SET DEFAULT nextval('idsmed.idsmed_role_id_seq'::regclass);


--
-- TOC entry 2973 (class 2604 OID 98322)
-- Name: idsmed_role_permission id; Type: DEFAULT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.idsmed_role_permission ALTER COLUMN id SET DEFAULT nextval('idsmed.idsmed_role_permission_id_seq'::regclass);


--
-- TOC entry 2976 (class 2604 OID 98323)
-- Name: idsmed_subscriber_role id; Type: DEFAULT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.idsmed_subscriber_role ALTER COLUMN id SET DEFAULT nextval('idsmed.idsmed_subscriber_role_id_seq'::regclass);


--
-- TOC entry 2982 (class 2604 OID 98324)
-- Name: idsmed_task id; Type: DEFAULT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.idsmed_task ALTER COLUMN id SET DEFAULT nextval('idsmed.idsmed_task_id_seq'::regclass);


--
-- TOC entry 2988 (class 2604 OID 98325)
-- Name: idsmed_task_executor id; Type: DEFAULT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.idsmed_task_executor ALTER COLUMN id SET DEFAULT nextval('idsmed.idsmed_task_executor_id_seq'::regclass);


--
-- TOC entry 2992 (class 2604 OID 98326)
-- Name: idsmed_user id; Type: DEFAULT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.idsmed_user ALTER COLUMN id SET DEFAULT nextval('idsmed.idsmed_user_id_seq'::regclass);


--
-- TOC entry 2995 (class 2604 OID 98327)
-- Name: idsmed_user_role id; Type: DEFAULT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.idsmed_user_role ALTER COLUMN id SET DEFAULT nextval('idsmed.idsmed_user_role_id_seq'::regclass);


--
-- TOC entry 3001 (class 2604 OID 98328)
-- Name: keyword_history id; Type: DEFAULT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.keyword_history ALTER COLUMN id SET DEFAULT nextval('idsmed.keyword_history_id_seq'::regclass);


--
-- TOC entry 3007 (class 2604 OID 98329)
-- Name: mail_sending_log id; Type: DEFAULT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.mail_sending_log ALTER COLUMN id SET DEFAULT nextval('idsmed.mail_sending_log_id_seq'::regclass);


--
-- TOC entry 3013 (class 2604 OID 98330)
-- Name: notification id; Type: DEFAULT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.notification ALTER COLUMN id SET DEFAULT nextval('idsmed.notification_id_seq'::regclass);


--
-- TOC entry 3019 (class 2604 OID 98331)
-- Name: notification_recipient id; Type: DEFAULT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.notification_recipient ALTER COLUMN id SET DEFAULT nextval('idsmed.notification_recipient_id_seq'::regclass);


--
-- TOC entry 3025 (class 2604 OID 98332)
-- Name: notification_wildcard id; Type: DEFAULT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.notification_wildcard ALTER COLUMN id SET DEFAULT nextval('idsmed.notification_wildcard_id_seq'::regclass);


--
-- TOC entry 3031 (class 2604 OID 98333)
-- Name: od_big_order id; Type: DEFAULT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.od_big_order ALTER COLUMN id SET DEFAULT nextval('idsmed.od_big_order_id_seq'::regclass);


--
-- TOC entry 3037 (class 2604 OID 98334)
-- Name: od_delivery_product id; Type: DEFAULT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.od_delivery_product ALTER COLUMN id SET DEFAULT nextval('idsmed.od_delivery_product_id_seq'::regclass);


--
-- TOC entry 3043 (class 2604 OID 98335)
-- Name: od_order_delivery id; Type: DEFAULT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.od_order_delivery ALTER COLUMN id SET DEFAULT nextval('idsmed.od_order_delivery_id_seq'::regclass);


--
-- TOC entry 3049 (class 2604 OID 98336)
-- Name: od_order_delivery_record id; Type: DEFAULT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.od_order_delivery_record ALTER COLUMN id SET DEFAULT nextval('idsmed.od_order_delivery_record_id_seq'::regclass);


--
-- TOC entry 3055 (class 2604 OID 98337)
-- Name: od_order_item id; Type: DEFAULT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.od_order_item ALTER COLUMN id SET DEFAULT nextval('idsmed.od_order_item_id_seq'::regclass);


--
-- TOC entry 3061 (class 2604 OID 98338)
-- Name: od_order_receive_address id; Type: DEFAULT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.od_order_receive_address ALTER COLUMN id SET DEFAULT nextval('idsmed.od_order_receive_address_id_seq'::regclass);


--
-- TOC entry 3067 (class 2604 OID 98339)
-- Name: od_small_order id; Type: DEFAULT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.od_small_order ALTER COLUMN id SET DEFAULT nextval('idsmed.od_small_order_id_seq'::regclass);


--
-- TOC entry 3076 (class 2604 OID 98340)
-- Name: product id; Type: DEFAULT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product ALTER COLUMN id SET DEFAULT nextval('idsmed.product_id_seq'::regclass);


--
-- TOC entry 3082 (class 2604 OID 98341)
-- Name: product_brand id; Type: DEFAULT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_brand ALTER COLUMN id SET DEFAULT nextval('idsmed.product_brand_id_seq'::regclass);


--
-- TOC entry 3088 (class 2604 OID 98342)
-- Name: product_care_area id; Type: DEFAULT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_care_area ALTER COLUMN id SET DEFAULT nextval('idsmed.product_care_area_id_seq'::regclass);


--
-- TOC entry 3091 (class 2604 OID 98343)
-- Name: product_care_area_detail id; Type: DEFAULT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_care_area_detail ALTER COLUMN id SET DEFAULT nextval('idsmed.product_care_area_detail_id_seq'::regclass);


--
-- TOC entry 3094 (class 2604 OID 98344)
-- Name: product_care_area_detail_log id; Type: DEFAULT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_care_area_detail_log ALTER COLUMN id SET DEFAULT nextval('idsmed.product_care_area_detail_log_id_seq'::regclass);


--
-- TOC entry 3100 (class 2604 OID 98345)
-- Name: product_category id; Type: DEFAULT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_category ALTER COLUMN id SET DEFAULT nextval('idsmed.product_category_id_seq'::regclass);


--
-- TOC entry 3106 (class 2604 OID 98346)
-- Name: product_certificate_log id; Type: DEFAULT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_certificate_log ALTER COLUMN id SET DEFAULT nextval('idsmed.product_certificate_log_id_seq'::regclass);


--
-- TOC entry 3111 (class 2604 OID 98347)
-- Name: product_csv_log id; Type: DEFAULT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_csv_log ALTER COLUMN id SET DEFAULT nextval('idsmed.product_csv_log_id_seq'::regclass);


--
-- TOC entry 3117 (class 2604 OID 98348)
-- Name: product_document_attachment id; Type: DEFAULT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_document_attachment ALTER COLUMN id SET DEFAULT nextval('idsmed.product_document_attachment_id_seq'::regclass);


--
-- TOC entry 3127 (class 2604 OID 98349)
-- Name: product_document_attachment_reminder id; Type: DEFAULT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_document_attachment_reminder ALTER COLUMN id SET DEFAULT nextval('idsmed.product_document_attachment_reminder_id_seq'::regclass);


--
-- TOC entry 3134 (class 2604 OID 98350)
-- Name: product_features id; Type: DEFAULT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_features ALTER COLUMN id SET DEFAULT nextval('idsmed.product_features_id_seq'::regclass);


--
-- TOC entry 3140 (class 2604 OID 98351)
-- Name: product_features_log id; Type: DEFAULT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_features_log ALTER COLUMN id SET DEFAULT nextval('idsmed.product_features_log_id_seq'::regclass);


--
-- TOC entry 3144 (class 2604 OID 98352)
-- Name: product_hierarchy id; Type: DEFAULT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_hierarchy ALTER COLUMN id SET DEFAULT nextval('idsmed.product_hierarchy_id_seq'::regclass);


--
-- TOC entry 3150 (class 2604 OID 98353)
-- Name: product_info_config id; Type: DEFAULT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_info_config ALTER COLUMN id SET DEFAULT nextval('idsmed.product_info_config_id_seq'::regclass);


--
-- TOC entry 3158 (class 2604 OID 98354)
-- Name: product_label_config id; Type: DEFAULT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_label_config ALTER COLUMN id SET DEFAULT nextval('idsmed.product_label_config_id_seq'::regclass);


--
-- TOC entry 3167 (class 2604 OID 98355)
-- Name: product_log id; Type: DEFAULT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_log ALTER COLUMN id SET DEFAULT nextval('idsmed.product_log_id_seq'::regclass);


--
-- TOC entry 3173 (class 2604 OID 98356)
-- Name: product_media id; Type: DEFAULT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_media ALTER COLUMN id SET DEFAULT nextval('idsmed.product_media_id_seq'::regclass);


--
-- TOC entry 3179 (class 2604 OID 98357)
-- Name: product_media_log id; Type: DEFAULT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_media_log ALTER COLUMN id SET DEFAULT nextval('idsmed.product_media_log_id_seq'::regclass);


--
-- TOC entry 3185 (class 2604 OID 98358)
-- Name: product_rating id; Type: DEFAULT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_rating ALTER COLUMN id SET DEFAULT nextval('idsmed.product_rating_id_seq'::regclass);


--
-- TOC entry 3191 (class 2604 OID 98359)
-- Name: product_rating_detail id; Type: DEFAULT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_rating_detail ALTER COLUMN id SET DEFAULT nextval('idsmed.product_rating_detail_id_seq'::regclass);


--
-- TOC entry 3197 (class 2604 OID 98360)
-- Name: product_reject id; Type: DEFAULT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_reject ALTER COLUMN id SET DEFAULT nextval('idsmed.product_reject_id_seq'::regclass);


--
-- TOC entry 3203 (class 2604 OID 98361)
-- Name: product_reject_log id; Type: DEFAULT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_reject_log ALTER COLUMN id SET DEFAULT nextval('idsmed.product_reject_log_id_seq'::regclass);


--
-- TOC entry 3209 (class 2604 OID 98362)
-- Name: product_related id; Type: DEFAULT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_related ALTER COLUMN id SET DEFAULT nextval('idsmed.product_related_id_seq'::regclass);


--
-- TOC entry 3215 (class 2604 OID 98363)
-- Name: product_second_category id; Type: DEFAULT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_second_category ALTER COLUMN id SET DEFAULT nextval('idsmed.product_second_category_id_seq'::regclass);


--
-- TOC entry 3220 (class 2604 OID 98364)
-- Name: product_second_category_detail id; Type: DEFAULT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_second_category_detail ALTER COLUMN id SET DEFAULT nextval('idsmed.product_second_category_detail_id_seq'::regclass);


--
-- TOC entry 3226 (class 2604 OID 98365)
-- Name: product_selling_history id; Type: DEFAULT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_selling_history ALTER COLUMN id SET DEFAULT nextval('idsmed.product_selling_history_id_seq'::regclass);


--
-- TOC entry 3232 (class 2604 OID 98366)
-- Name: product_selling_rate id; Type: DEFAULT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_selling_rate ALTER COLUMN id SET DEFAULT nextval('idsmed.product_selling_rate_id_seq'::regclass);


--
-- TOC entry 3238 (class 2604 OID 98367)
-- Name: product_tech_feature id; Type: DEFAULT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_tech_feature ALTER COLUMN id SET DEFAULT nextval('idsmed.product_tech_feature_id_seq'::regclass);


--
-- TOC entry 3245 (class 2604 OID 98368)
-- Name: product_technical id; Type: DEFAULT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_technical ALTER COLUMN id SET DEFAULT nextval('idsmed.product_technical_id_seq'::regclass);


--
-- TOC entry 3251 (class 2604 OID 98369)
-- Name: product_technical_log id; Type: DEFAULT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_technical_log ALTER COLUMN id SET DEFAULT nextval('idsmed.product_technical_log_id_seq'::regclass);


--
-- TOC entry 3257 (class 2604 OID 98370)
-- Name: product_wishlist id; Type: DEFAULT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_wishlist ALTER COLUMN id SET DEFAULT nextval('idsmed.product_wishlist_id_seq'::regclass);


--
-- TOC entry 3263 (class 2604 OID 98371)
-- Name: product_wishlist_detail id; Type: DEFAULT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_wishlist_detail ALTER COLUMN id SET DEFAULT nextval('idsmed.product_wishlist_detail_id_seq'::regclass);


--
-- TOC entry 3269 (class 2604 OID 98372)
-- Name: province id; Type: DEFAULT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.province ALTER COLUMN id SET DEFAULT nextval('idsmed.province_id_seq'::regclass);


--
-- TOC entry 3275 (class 2604 OID 98373)
-- Name: schedule_setting id; Type: DEFAULT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.schedule_setting ALTER COLUMN id SET DEFAULT nextval('idsmed.schedule_setting_id_seq'::regclass);


--
-- TOC entry 3281 (class 2604 OID 98374)
-- Name: subscriber id; Type: DEFAULT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.subscriber ALTER COLUMN id SET DEFAULT nextval('idsmed.subscriber_id_seq'::regclass);


--
-- TOC entry 3287 (class 2604 OID 98375)
-- Name: subscriber_access_right id; Type: DEFAULT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.subscriber_access_right ALTER COLUMN id SET DEFAULT nextval('idsmed.subscriber_access_right_id_seq'::regclass);


--
-- TOC entry 3293 (class 2604 OID 98376)
-- Name: subscriber_ip id; Type: DEFAULT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.subscriber_ip ALTER COLUMN id SET DEFAULT nextval('idsmed.subscriber_ip_id_seq'::regclass);


--
-- TOC entry 3299 (class 2604 OID 98377)
-- Name: tender_category id; Type: DEFAULT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.tender_category ALTER COLUMN id SET DEFAULT nextval('idsmed.tender_category_id_seq'::regclass);


--
-- TOC entry 3307 (class 2604 OID 98378)
-- Name: token_information id; Type: DEFAULT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.token_information ALTER COLUMN id SET DEFAULT nextval('idsmed.token_information_id_seq'::regclass);


--
-- TOC entry 3313 (class 2604 OID 98379)
-- Name: vendor id; Type: DEFAULT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.vendor ALTER COLUMN id SET DEFAULT nextval('idsmed.vendor_id_seq'::regclass);


--
-- TOC entry 3315 (class 2604 OID 98380)
-- Name: vendor_action_log id; Type: DEFAULT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.vendor_action_log ALTER COLUMN id SET DEFAULT nextval('idsmed.vendor_action_log_id_seq'::regclass);


--
-- TOC entry 3321 (class 2604 OID 98381)
-- Name: vendor_additional_info id; Type: DEFAULT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.vendor_additional_info ALTER COLUMN id SET DEFAULT nextval('idsmed.vendor_additional_info_id_seq'::regclass);


--
-- TOC entry 3328 (class 2604 OID 98382)
-- Name: vendor_address id; Type: DEFAULT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.vendor_address ALTER COLUMN id SET DEFAULT nextval('idsmed.vendor_address_id_seq'::regclass);


--
-- TOC entry 3338 (class 2604 OID 98383)
-- Name: vendor_file_attachment_reminder id; Type: DEFAULT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.vendor_file_attachment_reminder ALTER COLUMN id SET DEFAULT nextval('idsmed.vendor_file_attachment_reminder_id_seq'::regclass);


--
-- TOC entry 3344 (class 2604 OID 98384)
-- Name: vendor_info_config id; Type: DEFAULT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.vendor_info_config ALTER COLUMN id SET DEFAULT nextval('idsmed.vendor_info_config_id_seq'::regclass);


--
-- TOC entry 3352 (class 2604 OID 98385)
-- Name: vendor_label_config id; Type: DEFAULT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.vendor_label_config ALTER COLUMN id SET DEFAULT nextval('idsmed.vendor_label_config_id_seq'::regclass);


--
-- TOC entry 3358 (class 2604 OID 98386)
-- Name: vendor_reject id; Type: DEFAULT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.vendor_reject ALTER COLUMN id SET DEFAULT nextval('idsmed.vendor_reject_id_seq'::regclass);


--
-- TOC entry 3364 (class 2604 OID 98387)
-- Name: verifying_work_flow_setting id; Type: DEFAULT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.verifying_work_flow_setting ALTER COLUMN id SET DEFAULT nextval('idsmed.verifying_work_flow_setting_id_seq'::regclass);


--
-- TOC entry 3370 (class 2604 OID 98388)
-- Name: webservice_log id; Type: DEFAULT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.webservice_log ALTER COLUMN id SET DEFAULT nextval('idsmed.webservice_log_id_seq'::regclass);


--
-- TOC entry 3376 (class 2604 OID 98389)
-- Name: idsmed_function id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.idsmed_function ALTER COLUMN id SET DEFAULT nextval('public.idsmed_function_id_seq'::regclass);


--
-- TOC entry 3382 (class 2604 OID 98390)
-- Name: idsmed_permission id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.idsmed_permission ALTER COLUMN id SET DEFAULT nextval('public.idsmed_permission_id_seq'::regclass);


--
-- TOC entry 3982 (class 2613 OID 20456)
-- Name: 20456; Type: BLOB; Schema: -; Owner: dbadmin
--

SELECT pg_catalog.lo_create('20456');


ALTER LARGE OBJECT 20456 OWNER TO dbadmin;

--
-- TOC entry 3802 (class 0 OID 97250)
-- Dependencies: 186
-- Data for Name: account_notification_setting; Type: TABLE DATA; Schema: idsmed; Owner: dbadmin
--



--
-- TOC entry 4091 (class 0 OID 0)
-- Dependencies: 187
-- Name: account_notification_setting_id_seq; Type: SEQUENCE SET; Schema: idsmed; Owner: dbadmin
--

SELECT pg_catalog.setval('idsmed.account_notification_setting_id_seq', 6, true);


--
-- TOC entry 3804 (class 0 OID 97260)
-- Dependencies: 188
-- Data for Name: batch_job_execution; Type: TABLE DATA; Schema: idsmed; Owner: dbadmin
--



--
-- TOC entry 3805 (class 0 OID 97266)
-- Dependencies: 189
-- Data for Name: batch_job_execution_context; Type: TABLE DATA; Schema: idsmed; Owner: dbadmin
--



--
-- TOC entry 3806 (class 0 OID 97272)
-- Dependencies: 190
-- Data for Name: batch_job_execution_params; Type: TABLE DATA; Schema: idsmed; Owner: dbadmin
--



--
-- TOC entry 3807 (class 0 OID 97275)
-- Dependencies: 191
-- Data for Name: batch_job_instance; Type: TABLE DATA; Schema: idsmed; Owner: dbadmin
--



--
-- TOC entry 3808 (class 0 OID 97278)
-- Dependencies: 192
-- Data for Name: batch_step_execution; Type: TABLE DATA; Schema: idsmed; Owner: dbadmin
--



--
-- TOC entry 3809 (class 0 OID 97284)
-- Dependencies: 193
-- Data for Name: batch_step_execution_context; Type: TABLE DATA; Schema: idsmed; Owner: dbadmin
--



--
-- TOC entry 3810 (class 0 OID 97290)
-- Dependencies: 194
-- Data for Name: best_seller_product_global; Type: TABLE DATA; Schema: idsmed; Owner: dbadmin
--



--
-- TOC entry 4092 (class 0 OID 0)
-- Dependencies: 195
-- Name: best_seller_product_global_id_seq; Type: SEQUENCE SET; Schema: idsmed; Owner: dbadmin
--

SELECT pg_catalog.setval('idsmed.best_seller_product_global_id_seq', 1, false);


--
-- TOC entry 3812 (class 0 OID 97299)
-- Dependencies: 196
-- Data for Name: company_background_info; Type: TABLE DATA; Schema: idsmed; Owner: dbadmin
--



--
-- TOC entry 4093 (class 0 OID 0)
-- Dependencies: 197
-- Name: company_background_info_id_seq; Type: SEQUENCE SET; Schema: idsmed; Owner: dbadmin
--

SELECT pg_catalog.setval('idsmed.company_background_info_id_seq', 1, true);


--
-- TOC entry 3814 (class 0 OID 97312)
-- Dependencies: 198
-- Data for Name: country; Type: TABLE DATA; Schema: idsmed; Owner: dbadmin
--

INSERT INTO idsmed.country VALUES (1, 'People''s Republic of China', '中华人民共和国', 'China', '中国', 'cn', '2018-10-29 16:17:43.274025', '2018-10-29 16:17:43.274025', 1, 1, 1);


--
-- TOC entry 4094 (class 0 OID 0)
-- Dependencies: 199
-- Name: country_id_seq; Type: SEQUENCE SET; Schema: idsmed; Owner: dbadmin
--

SELECT pg_catalog.setval('idsmed.country_id_seq', 1, true);


--
-- TOC entry 3816 (class 0 OID 97325)
-- Dependencies: 200
-- Data for Name: currency_setting; Type: TABLE DATA; Schema: idsmed; Owner: dbadmin
--

INSERT INTO idsmed.currency_setting VALUES (2, 'th', 'THB', 'Baht', '2019-01-22 09:22:37.17827', '2019-01-22 09:22:37.17827', 1, 1, 1);
INSERT INTO idsmed.currency_setting VALUES (3, 'vn', 'VND', 'Dong', '2019-01-22 09:22:37.17827', '2019-01-22 09:22:37.17827', 1, 1, 1);
INSERT INTO idsmed.currency_setting VALUES (4, 'id', 'IDR', 'Rupiah', '2019-01-22 09:22:37.17827', '2019-01-22 09:22:37.17827', 1, 1, 1);
INSERT INTO idsmed.currency_setting VALUES (5, 'my', 'MYR', 'Malaysian Ringgit', '2019-01-22 09:22:37.17827', '2019-01-22 09:22:37.17827', 1, 1, 1);
INSERT INTO idsmed.currency_setting VALUES (1, 'cn', 'RMB', 'Yuan Renminbi', '2019-01-22 09:22:37.17827', '2019-01-22 09:22:37.17827', 1, 1, 1);


--
-- TOC entry 4095 (class 0 OID 0)
-- Dependencies: 201
-- Name: currency_setting_id_seq; Type: SEQUENCE SET; Schema: idsmed; Owner: dbadmin
--

SELECT pg_catalog.setval('idsmed.currency_setting_id_seq', 5, true);


--
-- TOC entry 3818 (class 0 OID 97338)
-- Dependencies: 202
-- Data for Name: customer; Type: TABLE DATA; Schema: idsmed; Owner: dbadmin
--



--
-- TOC entry 4096 (class 0 OID 0)
-- Dependencies: 203
-- Name: customer_id_seq; Type: SEQUENCE SET; Schema: idsmed; Owner: dbadmin
--

SELECT pg_catalog.setval('idsmed.customer_id_seq', 5, true);


--
-- TOC entry 3820 (class 0 OID 97351)
-- Dependencies: 204
-- Data for Name: email_notification_config; Type: TABLE DATA; Schema: idsmed; Owner: dbadmin
--

INSERT INTO idsmed.email_notification_config VALUES (21, 20, 21, true, NULL, 'cn', '2018-12-20 15:53:28.685474', '2018-12-20 15:53:28.685474', 1, 1, 1, 'User reset password email', 'User Reset Password Notification', 1);
INSERT INTO idsmed.email_notification_config VALUES (22, 41, 22, true, NULL, 'en', '2019-01-07 08:47:47.794', '2019-01-07 08:47:47.794', 1, 1, 0, NULL, 'SCP User Registration Approved Notification', 1);
INSERT INTO idsmed.email_notification_config VALUES (1, 12, 0, true, NULL, 'cn', '2018-11-10 10:46:41.084406', '2019-01-07 08:47:47.796', 1, 1, 1, NULL, 'Product Approved Notification', 3);
INSERT INTO idsmed.email_notification_config VALUES (8, 14, 1, true, NULL, 'cn', '2018-11-10 10:46:41.084406', '2019-01-07 08:47:47.796', 1, 1, 1, NULL, 'Product Approved Notification For SCP Admin', 3);
INSERT INTO idsmed.email_notification_config VALUES (9, 15, 2, true, NULL, 'cn', '2018-11-10 10:46:41.084406', '2019-01-07 08:47:47.796', 1, 1, 1, NULL, 'Product Batch Upload Result Notification', 3);
INSERT INTO idsmed.email_notification_config VALUES (10, 17, 3, true, NULL, 'cn', '2018-11-10 10:46:41.084406', '2019-01-07 08:47:47.796', 1, 1, 1, NULL, 'Product Certificate Approval Notification', 3);
INSERT INTO idsmed.email_notification_config VALUES (4, 16, 4, true, NULL, 'cn', '2018-11-10 10:46:41.084406', '2019-01-07 08:47:47.796', 1, 1, 1, NULL, 'Product Certificate Expiry Notification', 3);
INSERT INTO idsmed.email_notification_config VALUES (11, 13, 5, true, NULL, 'cn', '2018-11-10 10:46:41.084406', '2019-01-07 08:47:47.796', 1, 1, 1, NULL, 'Product Rejected Notification', 3);
INSERT INTO idsmed.email_notification_config VALUES (23, 46, 23, true, NULL, 'en', '2019-01-22 09:22:37.249305', '2019-01-22 09:22:37.249305', 1, 1, 1, NULL, 'Request to Approve Product Notification', 3);
INSERT INTO idsmed.email_notification_config VALUES (24, 47, 24, true, NULL, 'en', '2019-01-22 09:22:37.249305', '2019-01-22 09:22:37.249305', 1, 1, 1, NULL, 'Request to Approve Product Certificate Notification', 3);
INSERT INTO idsmed.email_notification_config VALUES (6, 3, 6, true, NULL, 'cn', '2018-11-10 10:46:41.084406', '2019-01-07 08:47:47.796', 1, 1, 1, NULL, 'Request to Approve Vendor Registration Notification', 2);
INSERT INTO idsmed.email_notification_config VALUES (3, 2, 7, true, NULL, 'cn', '2018-11-10 10:46:41.084406', '2019-01-07 08:47:47.796', 1, 1, 1, NULL, 'Request to Review Vendor Registration Notification', 2);
INSERT INTO idsmed.email_notification_config VALUES (13, 11, 11, true, NULL, 'cn', '2018-11-10 10:46:41.084406', '2019-01-07 08:47:47.796', 1, 1, 1, NULL, 'Vendor Profile Certificate Approval Notification', 2);
INSERT INTO idsmed.email_notification_config VALUES (14, 10, 12, true, NULL, 'cn', '2018-11-10 10:46:41.084406', '2019-01-07 08:47:47.796', 1, 1, 1, NULL, 'Vendor Profile Certificate Expiry Notification', 2);
INSERT INTO idsmed.email_notification_config VALUES (7, 4, 13, true, NULL, 'cn', '2018-11-10 10:46:41.084406', '2019-01-07 08:47:47.796', 1, 1, 1, NULL, 'Vendor Registration Approved Notification', 2);
INSERT INTO idsmed.email_notification_config VALUES (2, 1, 14, true, NULL, 'cn', '2018-11-10 10:46:41.084406', '2019-01-07 08:47:47.796', 1, 1, 1, NULL, 'Vendor Registration Notification', 2);
INSERT INTO idsmed.email_notification_config VALUES (16, 5, 15, true, NULL, 'cn', '2018-11-10 10:46:41.084406', '2019-01-07 08:47:47.796', 1, 1, 1, NULL, 'Vendor Registration Rejected Notification', 2);
INSERT INTO idsmed.email_notification_config VALUES (25, 48, 25, true, NULL, 'en', '2019-01-22 09:22:37.249305', '2019-01-22 09:22:37.249305', 1, 1, 1, NULL, 'Request to Approve Vendor Certificate Notification', 2);
INSERT INTO idsmed.email_notification_config VALUES (5, 7, 8, true, NULL, 'cn', '2018-11-10 10:46:41.084406', '2019-01-07 08:47:47.795', 1, 1, 1, NULL, 'Vendor User Registration Approved Notification', 1);
INSERT INTO idsmed.email_notification_config VALUES (15, 9, 9, true, NULL, 'cn', '2018-11-10 10:46:41.084406', '2019-01-07 08:47:47.795', 1, 1, 1, NULL, 'User Registration Approved Notification For SCP Admin', 1);
INSERT INTO idsmed.email_notification_config VALUES (12, 8, 10, true, NULL, 'cn', '2018-11-10 10:46:41.084406', '2019-01-07 08:47:47.795', 1, 1, 1, NULL, 'User Registration Rejected Notification', 1);
INSERT INTO idsmed.email_notification_config VALUES (17, 6, 16, true, NULL, 'cn', '2018-11-10 10:46:41.084406', '2019-01-07 08:47:47.796', 1, 1, 1, NULL, 'Vendor User Registration Notification', 1);
INSERT INTO idsmed.email_notification_config VALUES (18, 18, 17, true, NULL, 'cn', NULL, '2019-01-07 08:47:47.796', -1, 1, 1, NULL, 'New Vendor Staff Registration Notification for Vendor Admin', 1);


--
-- TOC entry 4097 (class 0 OID 0)
-- Dependencies: 205
-- Name: email_notification_config_id_seq; Type: SEQUENCE SET; Schema: idsmed; Owner: dbadmin
--

SELECT pg_catalog.setval('idsmed.email_notification_config_id_seq', 25, true);


--
-- TOC entry 3822 (class 0 OID 97363)
-- Dependencies: 206
-- Data for Name: email_template; Type: TABLE DATA; Schema: idsmed; Owner: dbadmin
--

INSERT INTO idsmed.email_template VALUES (14, 'Product Approved Notification Template For SCP Admin', 'Smart SCP-产品批准通知', '<p>Hi,</p><p>我们想要通知您，在[#VENDOR_NAME]下有一个产品已通过审批，以下是产品的简介:</p><p>&nbsp;</p><p>产品名称: [#PRODUCT_NAME]</p><p>产品型号: [#PRODUCT_MODEL]</p><p>产品代码: [#PRODUCT_CODE]</p><p><br></p><p>感谢并致以最诚挚的问候！</p><p>idsWeDoc运营团队</p>', '2018-11-07 17:57:49.954', '2018-11-10 18:36:19.977', 1, 1, 1);
INSERT INTO idsmed.email_template VALUES (18, 'New Vendor User Registration Notification for Vendor Senior', 'Smart SCP-新供应商用户注册通知', '<p>Hi [#VENDOR_NAME],</p><p>贵公司一名新员工在我们平台注册了一个用户账号，请登录系统审核新员工提交的信息。</p><p>&nbsp;</p><p>感谢并致以最诚挚的问候！</p><p>idsWeDoc运营团队</p>', '2018-12-12 17:44:23.594', '2018-12-20 15:47:40.806', 1, 1, 1);
INSERT INTO idsmed.email_template VALUES (10, 'Vendor Profile Certificate Expiry Reminder Template', 'Smart SCP-供应商资质证书到期提醒', '<p>Hi [#VENDOR_NAME],</p><p>贵公司的一份资质证明文件即将到期，以下是证件信息:</p><p><br></p><p>证件名称: [#VENDOR_DOCUMENT_NAME]</p><p>有效期至: [#VENDOR_DOCUMENT_EXP_DATE]</p><p><br></p><p>请尽快上传最新的资质证明文件。</p><p>感谢并致以最诚挚的问候！</p><p>idsWeDoc运营团队</p><p><br></p>', '2018-11-07 15:45:28.675', '2018-11-07 15:45:28.675', 1, 1, 1);
INSERT INTO idsmed.email_template VALUES (11, 'Vendor Profile Certificate Approval Notification Template', 'Smart SCP-公司资质证书批准通知', '<p>Hi [#VENDOR_NAME],</p><p>我们想要通知您，由您或您的团队上传的公司资质证明文件的状态。</p><p><br></p><p>证件名称: [#VENDOR_DOCUMENT_NAME]</p><p>状态: [#VENDOR_DOCUMENT_APP_STATUS]</p><p><br></p><p>感谢并致以最诚挚的问候！</p><p>idsWeDoc运营团队</p>', '2018-11-07 16:13:28.03', '2018-11-07 16:31:48.353', 1, 1, 1);
INSERT INTO idsmed.email_template VALUES (16, 'Product Certificate Expiry Reminder Template', 'Smart SCP-产品证书到期提醒', '<p>Hi [#VENDOR_NAME],</p><p>贵公司的一份产品证明文件即将到期，以下是证件信息:</p><p><br></p><p><strong>产品名称</strong>: [#PRODUCT_NAME]</p><p><strong>产品代码</strong>: [#PRODUCT_CODE]</p><p><strong>证件名称</strong>: [#PRODUCT_DOCUMENT_NAME]</p><p><strong>有效期至</strong>: [#PRODUCT_DOCUMENT_EXP_DATE]</p><p><br></p><p>请尽快上传最新的证明文件。</p><p><br></p><p>感谢并致以最诚挚的问候！</p><p>idsWeDoc运营团队</p>', '2018-11-07 18:10:43.749', '2018-11-07 18:10:43.749', 1, 1, 1);
INSERT INTO idsmed.email_template VALUES (17, 'Product Certificate Approval Notification Template', 'Smart SCP-产品证书批准通知', '<p>Hi [#VENDOR_NAME],</p><p>我们想要通知您，由您或您的团队上传的产品证明文件的状态。</p><p><br></p><p>证件名称: [#PRODUCT_DOCUMENT_NAME]</p><p>状态: [#PRODUCT_DOCUMENT_APP_STATUS]</p><p><br></p><p>感谢并致以最诚挚的问候！</p><p>idsWeDoc运营团队</p>', '2018-11-07 18:12:09.759', '2018-11-07 18:12:09.759', 1, 1, 1);
INSERT INTO idsmed.email_template VALUES (1, 'Vendor Registration Notification Template', 'Smart SCP-公司注册通知', '<p>Dear [#RECIPIENT_NAME],</p><p>感谢您在我们平台注册您的公司[#VENDOR_NAME] 资料，你的申请已提交审批，我们将向您提供的主要和第二联系人发送关于您公司注册状态的邮件通知。</p><p>&nbsp;</p><p>感谢并致以最诚挚的问候！</p><p>idsWeDoc运营团队</p>', '2018-11-07 10:35:36.103', '2018-11-07 10:35:36.103', 1, 1, 1);
INSERT INTO idsmed.email_template VALUES (2, 'Request to Review Vendor Registration Notification Template', 'Smart SCP-申请审核公司注册', '<p>Hi [#RECIPIENT_NAME],</p><p>一个新的供应商 [#VENDOR_NAME] 已经在我们平台上注册，请登录系统审核供应商提交的信息。</p><p>&nbsp;</p><p>感谢并致以最诚挚的问候！</p><p>idsWeDoc运营团队</p>', '2018-11-07 10:40:05.569', '2018-11-07 10:43:09.597', 1, 1, 1);
INSERT INTO idsmed.email_template VALUES (3, 'Request to Approve Vendor Registration Notification Template', 'Smart SCP-申请批准公司注册', '<p>Hi [#RECIPIENT_NAME],</p><p>一个新的供应商 [#VENDOR_NAME] 已经在我们平台注册，并且已通过团队的信息审核，请登录系统审核/批准这个申请。</p><p>&nbsp;</p><p>感谢并致以最诚挚的问候！</p><p>idsWeDoc运营团队</p>', '2018-11-07 10:45:16.551', '2018-11-07 10:45:28.256', 1, 1, 1);
INSERT INTO idsmed.email_template VALUES (4, 'Vendor Registration Approved Template', 'Smart SCP-公司注册状态通知', '<p>Hi [#RECIPIENT_NAME],</p><p>我们想要通知您注册的[#VENDOR_NAME]的注册状态。</p><p>&nbsp;</p><p>状态: [#VENDOR_APP_STATUS]</p><p>公司代码: [#COMPANY_CODE]</p><p>&nbsp;</p><p>请使用上述的公司代码注册您的员工帐号。</p><p>我们很高兴您能加入我们！</p><p>&nbsp;</p><p>感谢并致以最诚挚的问候！</p><p>idsWeDoc运营团队</p>', '2018-11-07 10:48:56.066', '2018-11-07 11:01:10.473', 1, 1, 1);
INSERT INTO idsmed.email_template VALUES (5, 'Vendor Registration Rejected Template', 'Smart SCP-公司注册状态通知', '<p>Hi [#RECIPIENT_NAME],</p><p>我们想要通知您注册申请的状态。 [#VENDOR_NAME] in IDSMed.</p><p>&nbsp;</p><p>状态: [#VENDOR_APP_STATUS]</p><p>原因: [#VENDOR_REJECT_REASON]</p><p><br></p><p></p><p>请点击这个链接  [#VENDOR_RESUBMIT_PROFILE_URL] 修改资料并重新提交给我们审批。</p><p><br></p><p>感谢并致以最诚挚的问候！</p><p>idsWeDoc运营团队</p>', '2018-11-07 10:58:41.206', '2018-11-07 10:58:41.206', 1, 1, 1);
INSERT INTO idsmed.email_template VALUES (6, 'Vendor User Registration Notification Template', 'Smart SCP-用户注册通知', '<p>Hi [#RECIPIENT_NAME],</p><p>感谢您在IDSMed注册帐号，您的注册目前正在等待您上级的批准。一旦您的账号注册被批准，我们将向您发送关于您的帐户注册状态的邮件通知。</p><p>&nbsp;</p><p>感谢并致以最诚挚的问候！</p><p>idsWeDoc运营团队</p>', '2018-11-07 14:00:15.726', '2018-11-07 14:00:15.726', 1, 1, 1);
INSERT INTO idsmed.email_template VALUES (7, 'User Registration Approved Notification Template', 'Smart SCP-用户注册批准通知', '<p>Hi [#RECIPIENT_NAME],</p><p>我们想要通知您的账户注册状态。</p><p>&nbsp;</p><p>状态: [#USER_APP_STATUS]</p><p><br></p><p>您现在可以登录系统进行日常操作。</p><p>&nbsp;</p><p><br></p><p>感谢并致以最诚挚的问候！</p><p>idsWeDoc运营团队</p>', '2018-11-07 14:12:25.656', '2018-11-07 14:12:25.656', 1, 1, 1);
INSERT INTO idsmed.email_template VALUES (8, 'User Registration Rejected Notification Template', 'Smart SCP-用户注册退回通知', '<p>Hi [#RECIPIENT_NAME],</p><p>我们想要通知您的账户注册状态。</p><p>&nbsp;</p><p>状态: [#USER_APP_STATUS]</p><p>原因: [#USER_REJECT_REASON]</p><p><br></p><p>请点击这个链接  [#USER_REGISTRATION_RESUBMIT_URL] 修改资料并重新提交给我们审批。</p><p>&nbsp;</p><p><br></p><p>感谢并致以最诚挚的问候！</p><p>idsWeDoc运营团队</p>', '2018-11-07 14:14:29.176', '2019-01-29 14:15:07.637', 1, 1, 1);
INSERT INTO idsmed.email_template VALUES (9, 'User Registration Approved Notification Template For SCP Admin', 'Smart SCP-新用户注册通知', '<p>Hi [#RECIPIENT_NAME],</p><p>我们想要通知您，一个新用户的帐户注册已经被批准。</p><p>以下是账户信息:</p><p><br></p><p>用户名字: [#USER_NAME]</p><p>公司 (供应商用户): [#VENDOR_NAME]</p><p>&nbsp;</p><p>感谢并致以最诚挚的问候！</p><p>idsWeDoc运营团队</p>', '2018-11-07 14:18:10.969', '2018-11-07 17:58:04.683', 1, 1, 1);
INSERT INTO idsmed.email_template VALUES (12, 'Product Approved Notification Template', 'Smart SCP-产品批准通知', '<p>Hi [#RECIPIENT_NAME],</p><p>我们想要通知您在[#VENDOR_NAME]下注册的产品的状态，您只有在系统中添加了新产品或更新了现有产品后才会收到此邮件。</p><p>&nbsp;</p><p><strong>产品名称</strong>: [#PRODUCT_NAME]</p><p><strong>产品型号</strong>: [#PRODUCT_MODEL]</p><p><strong>产品代码</strong>: [#PRODUCT_CODE]</p><p><strong>状态</strong>: [#PRODUCT_APP_STATUS]</p><p>&nbsp;</p><p>感谢并致以最诚挚的问候！</p><p>idsWeDoc运营团队</p>', '2018-11-07 16:46:59.089', '2018-11-07 16:47:52.942', 1, 1, 1);
INSERT INTO idsmed.email_template VALUES (13, 'Product Rejected Notification Template', 'Smart SCP-产品退回通知', '<p>Hi [#RECIPIENT_NAME],</p><p>我们想通知您在 [#VENDOR_NAME]下产品的注册状态， 您只有在系统中添加了新产品或更新了现有产品后才会收到此邮件。</p><p>&nbsp;</p><p>产品名称: [#PRODUCT_NAME]</p><p>产品型号: [#PRODUCT_MODEL]</p><p>产品代码: [#PRODUCT_CODE]</p><p>状态: [#PRODUCT_APP_STATUS]</p><p><strong>原因</strong>: [#PRODUCT_REJECT_REASON]</p><p>&nbsp;</p><p>感谢并致以最诚挚的问候！</p><p>idsWeDoc运营团队</p>', '2018-11-07 16:47:40.173', '2018-11-09 20:54:38.3', 1, 1, 1);
INSERT INTO idsmed.email_template VALUES (15, 'Product Batch Upload Result Template', 'Smart SCP-产品批量上传结果', '<p>Hi [#RECIPIENT_NAME],</p><p>我们想要通知您，您在 [#VENDOR_NAME] 下的产品批量上传状态如下:</p><p><br></p><p><strong>文件名称</strong>: [#PRODUCT_BATCH_UPLOAD_FILENAME]</p><p><br></p><p><strong><em>结果</em></strong>:</p><p><br></p><p>[#PRODUCT_BATCH_UPLOAD_RESULT]</p><p><br></p><p>感谢并致以最诚挚的问候！</p><p>idsWeDoc运营团队</p>', '2018-11-07 18:03:45.799', '2018-11-07 18:03:45.799', 1, 1, 1);
INSERT INTO idsmed.email_template VALUES (20, 'User Forgot Password Notification', 'Smart SCP-用户帐户临时密码通知', '<p>Hi [#RECIPIENT_NAME],</p><p>您已经为您的Smart SCP用户帐户申请了一个临时密码。</p><p>您的临时密码是：[#USER_DEFAULT_PASSWORD]</p><p><br></p><p>请使用临时密码登录系统，并尽快更新您的密码。</p><p>&nbsp;</p><p><br></p><p>感谢并致以最诚挚的问候！</p><p>idsWeDoc运营团队</p>', '2018-12-17 18:49:04.103856', '2019-01-03 11:04:04.077', 1, 1, 1);
INSERT INTO idsmed.email_template VALUES (41, 'SCP User Registration Approved Notification Template', 'Smart SCP-用户注册批准通知', '<p>Hi [#RECIPIENT_NAME],</p><p>我们想要通知您在Smart SCP的账户注册状态。</p><p>&nbsp;</p><p>用户ID: [#USER_ID]</p><p>初始密码: [#USER_DEFAULT_PASSWORD]</p><p>状态: [#USER_APP_STATUS]</p><p>&nbsp;</p><p>您现在可以登录系统进行日常操作。</p><p>&nbsp;</p><p>&nbsp;</p><p>感谢并致以最诚挚的问候！</p><p>idsWeDoc运营团队</p>', '2018-12-31 17:10:50.850258', '2018-12-31 17:10:50.850258', 1, 1, 1);
INSERT INTO idsmed.email_template VALUES (46, 'Request to Approve Product Notification', 'Smart SCP-产品审批申请', '<p>Hi [#RECIPIENT_NAME],</p><p>贵公司职员已将一款新产品登录在Smart SCP，详情如下:</p><p><br></p><p>产品名称 : [#PRODUCT_NAME]</p><p>产品型号: [#PRODUCT_MODEL]</p><p>产品代码: [#PRODUCT_CODE]</p><p><br></p><p>该产品目前正在等待您的审批，请登录系统审核这个产品注册。</p><p><br></p><p>感谢并致以最诚挚的问候！</p><p>idsWeDoc运营团队</p>', '2019-01-22 09:22:37.249305', '2019-01-22 09:22:37.249305', 1, 1, 1);
INSERT INTO idsmed.email_template VALUES (47, 'Request to Approve Product Certificate Notification', 'Smart SCP-申请批准产品证书', '<p>Hi [#RECIPIENT_NAME],</p><p>贵公司的员工在Smart SCP里更新了一张产品证书，详情如下:</p><p><br></p><p>产品名称 : [#PRODUCT_NAME]</p><p><span style=color: rgb(44, 68, 91);">证件名称: [#PRODUCT_DOCUMENT_NAME]</span></p><p><br></p><p>这个证书目前正在等待您的审批，请登录系统审核证书信息。</p><p><br></p><p>感谢并致以最诚挚的问候！</p><p>idsWeDoc运营团队</p>', '2019-01-22 09:22:37.249305', '2019-01-22 09:22:37.249305', 1, 1, 1);
INSERT INTO idsmed.email_template VALUES (48, 'Request to Approve Vendor Certificate Notification', 'Smart SCP-申请批准公司资质证书', '<p>Hi [#RECIPIENT_NAME],</p><p>供应商资质证书已由供应商的用户在Smart SCP内更新，详情如下:</p><p><br></p><p>文件名称: [#VENDOR_DOCUMENT_NAME]</p><p>状态: [#VENDOR_DOCUMENT_APP_STATUS]</p><p><br></p><p>证书目前正在等待您的批准，请登录系统审批相关信息。</p><p><br></p><p>感谢并致以最诚挚的问候！</p><p>idsWeDoc运营团队</p>', '2019-01-22 09:22:37.249305', '2019-01-22 09:22:37.249305', 1, 1, 1);


--
-- TOC entry 4098 (class 0 OID 0)
-- Dependencies: 207
-- Name: email_template_id_seq; Type: SEQUENCE SET; Schema: idsmed; Owner: dbadmin
--

SELECT pg_catalog.setval('idsmed.email_template_id_seq', 48, true);


--
-- TOC entry 3824 (class 0 OID 97376)
-- Dependencies: 208
-- Data for Name: favourite_product_brand; Type: TABLE DATA; Schema: idsmed; Owner: dbadmin
--



--
-- TOC entry 4099 (class 0 OID 0)
-- Dependencies: 209
-- Name: favourite_product_brand_id_seq; Type: SEQUENCE SET; Schema: idsmed; Owner: dbadmin
--

SELECT pg_catalog.setval('idsmed.favourite_product_brand_id_seq', 2, true);


--
-- TOC entry 3826 (class 0 OID 97386)
-- Dependencies: 210
-- Data for Name: favourite_product_care_area; Type: TABLE DATA; Schema: idsmed; Owner: dbadmin
--



--
-- TOC entry 4100 (class 0 OID 0)
-- Dependencies: 211
-- Name: favourite_product_care_area_id_seq; Type: SEQUENCE SET; Schema: idsmed; Owner: dbadmin
--

SELECT pg_catalog.setval('idsmed.favourite_product_care_area_id_seq', 2, true);


--
-- TOC entry 3828 (class 0 OID 97396)
-- Dependencies: 212
-- Data for Name: file_attachment; Type: TABLE DATA; Schema: idsmed; Owner: dbadmin
--



--
-- TOC entry 4101 (class 0 OID 0)
-- Dependencies: 213
-- Name: file_attachment_id_seq; Type: SEQUENCE SET; Schema: idsmed; Owner: dbadmin
--

SELECT pg_catalog.setval('idsmed.file_attachment_id_seq', 170, true);


--
-- TOC entry 3830 (class 0 OID 97409)
-- Dependencies: 214
-- Data for Name: flyway_schema_history; Type: TABLE DATA; Schema: idsmed; Owner: dbadmin
--

--
-- TOC entry 3831 (class 0 OID 97416)
-- Dependencies: 215
-- Data for Name: history_user_product_by_search; Type: TABLE DATA; Schema: idsmed; Owner: dbadmin
--



--
-- TOC entry 4102 (class 0 OID 0)
-- Dependencies: 216
-- Name: history_user_product_by_search_id_seq; Type: SEQUENCE SET; Schema: idsmed; Owner: dbadmin
--

SELECT pg_catalog.setval('idsmed.history_user_product_by_search_id_seq', 200, true);


--
-- TOC entry 3833 (class 0 OID 97425)
-- Dependencies: 217
-- Data for Name: idsmed_account; Type: TABLE DATA; Schema: idsmed; Owner: dbadmin
--

INSERT INTO idsmed.idsmed_account VALUES (1, 'admin', 1, NULL, '$2a$10$8f7GNkehu6q5Qu5Y0T8JeeZnA1a5u/NpsWlffTT9P7FSGUfVnjawi', 0, 3, '2018-09-09 10:04:34', '2019-01-12 15:29:53.992', 1, 1, NULL, NULL);
INSERT INTO idsmed.idsmed_account VALUES (105, 'superadmin', 106, NULL, '$2a$10$4/fqKXba98Ed2meWDdiMv.36qk6YAHi9CeYSbSpB1yvCfymX.Eszq', 0, 3, '2019-01-07 17:25:58.228499', '2019-01-07 17:25:58.228499', 1, 1, NULL, NULL);


--
-- TOC entry 4103 (class 0 OID 0)
-- Dependencies: 218
-- Name: idsmed_account_id_seq; Type: SEQUENCE SET; Schema: idsmed; Owner: dbadmin
--

SELECT pg_catalog.setval('idsmed.idsmed_account_id_seq', 143, true);


--
-- TOC entry 3835 (class 0 OID 97435)
-- Dependencies: 219
-- Data for Name: idsmed_account_settings; Type: TABLE DATA; Schema: idsmed; Owner: dbadmin
--

INSERT INTO idsmed.idsmed_account_settings VALUES (109, 105, 'en', '2019-01-15 10:43:57.359', '2019-01-15 10:43:57.359', -1, -1, 0);
INSERT INTO idsmed.idsmed_account_settings VALUES (1, 1, 'en', '2018-10-12 22:49:58.9', '2019-01-22 11:18:05.987', -1, -1, 0);


--
-- TOC entry 4104 (class 0 OID 0)
-- Dependencies: 220
-- Name: idsmed_account_settings_id_seq; Type: SEQUENCE SET; Schema: idsmed; Owner: dbadmin
--

SELECT pg_catalog.setval('idsmed.idsmed_account_settings_id_seq', 137, true);


--
-- TOC entry 3837 (class 0 OID 97445)
-- Dependencies: 221
-- Data for Name: idsmed_function; Type: TABLE DATA; Schema: idsmed; Owner: dbadmin
--

INSERT INTO idsmed.idsmed_function VALUES (1, 'UA', 'User Management', 'User Management', 0, '2018-09-07 11:32:05', '2018-09-07 11:32:05', 1, 1);
INSERT INTO idsmed.idsmed_function VALUES (2, 'VP', 'Vendor Management', 'Vendor Management', 0, '2018-09-07 11:32:05', '2018-09-07 11:32:05', 1, 1);
INSERT INTO idsmed.idsmed_function VALUES (3, 'PP', 'Product Management', 'Product Management', 0, '2018-09-07 11:32:05', '2018-09-07 11:32:05', 1, 1);
INSERT INTO idsmed.idsmed_function VALUES (4, 'GT', 'Access Right Management', 'Access Right Management', 0, '2018-09-07 11:32:05', '2018-09-07 11:32:05', 1, 1);
INSERT INTO idsmed.idsmed_function VALUES (5, 'API', 'API Service Management', 'API Service Management', 0, '2018-09-07 11:32:05', '2018-09-07 11:32:05', 1, 1);
INSERT INTO idsmed.idsmed_function VALUES (6, 'VPC', 'Vendor Profile Configuration', 'Vendor Profile Configuration', 3, '2018-10-29 16:16:48.396855', '2018-10-29 16:16:48.396855', 1, 1);
INSERT INTO idsmed.idsmed_function VALUES (7, 'PS', 'Product Search', 'Product Search', 3, '2018-10-29 16:16:48.396855', '2018-10-29 16:16:48.396855', 1, 1);
INSERT INTO idsmed.idsmed_function VALUES (8, 'VC', 'Vendor Configuration', 'Vendor Configuration', 3, '2018-10-29 16:16:48.396855', '2018-10-29 16:16:48.396855', 1, 1);
INSERT INTO idsmed.idsmed_function VALUES (9, 'PC', 'Product Configuration', 'Product Configuration', 3, '2018-10-29 16:16:48.396855', '2018-10-29 16:16:48.396855', 1, 1);
INSERT INTO idsmed.idsmed_function VALUES (10, 'EC', 'Email Configuration', 'Email Configuration', 3, '2018-10-29 16:16:48.396855', '2018-10-29 16:16:48.396855', 1, 1);
INSERT INTO idsmed.idsmed_function VALUES (13, 'ENC', 'Email Notification Configuration', 'Email Notification Configuration', 3, '2018-10-29 16:16:48.396855', '2018-10-29 16:16:48.396855', 1, 1);
INSERT INTO idsmed.idsmed_function VALUES (11, 'PS1', 'Main Page 1', 'Main Page 1', 3, '2018-10-29 16:16:48.396855', '2018-10-29 16:16:48.396855', 1, 1);
INSERT INTO idsmed.idsmed_function VALUES (12, 'PS2', 'Main Page 2', 'Main Page 2', 3, '2018-10-29 16:16:48.396855', '2018-10-29 16:16:48.396855', 1, 1);
INSERT INTO idsmed.idsmed_function VALUES (14, 'PS3', 'Main Page 3', 'Main Page 3', 3, '2018-11-23 10:09:29.310751', '2018-11-23 10:09:29.310751', 1, 1);
INSERT INTO idsmed.idsmed_function VALUES (15, 'PN', 'Procurement', 'Procurement', 3, '2019-01-07 08:09:01.327536', '2019-01-07 08:09:01.327536', 1, 1);
INSERT INTO idsmed.idsmed_function VALUES (16, 'TE', 'Tender', 'Tender', 3, '2019-01-07 08:09:01.327536', '2019-01-07 08:09:01.327536', 1, 1);


--
-- TOC entry 4105 (class 0 OID 0)
-- Dependencies: 222
-- Name: idsmed_function_id_seq; Type: SEQUENCE SET; Schema: idsmed; Owner: dbadmin
--

SELECT pg_catalog.setval('idsmed.idsmed_function_id_seq', 16, true);


--
-- TOC entry 3839 (class 0 OID 97458)
-- Dependencies: 223
-- Data for Name: idsmed_permission; Type: TABLE DATA; Schema: idsmed; Owner: dbadmin
--

INSERT INTO idsmed.idsmed_permission VALUES (29, 5, 'API0000004', 'GetCareAreaList', 'Get Care Area List API', 1, 3, 'cn', '2018-09-07 11:32:05', '2018-09-07 11:32:05', 1, 1);
INSERT INTO idsmed.idsmed_permission VALUES (30, 5, 'API0000005', 'getProductListOfNewRecommend', 'Get Product List of New Recommend API', 1, 3, 'cn', '2018-09-07 11:32:05', '2018-09-07 11:32:05', 1, 1);
INSERT INTO idsmed.idsmed_permission VALUES (31, 5, 'API0000006', 'getBrandListAPI', 'Get Brand List API', 1, 3, 'cn', '2018-09-07 11:32:05', '2018-09-07 11:32:05', 1, 1);
INSERT INTO idsmed.idsmed_permission VALUES (32, 5, 'API0000007', 'getCategoryListAPI', 'Get Category List API', 1, 3, 'cn', '2018-09-07 11:32:05', '2018-09-07 11:32:05', 1, 1);
INSERT INTO idsmed.idsmed_permission VALUES (33, 5, 'API0000011', 'getSimpleProductDetail', 'Get Simple Product Detail API', 1, 3, 'cn', '2018-10-18 12:12:41.092974', '2018-10-18 12:12:41.092974', 1, 1);
INSERT INTO idsmed.idsmed_permission VALUES (34, 5, 'API0000008', 'getProductListFilter', 'Get Product List Filter API', 1, 3, 'cn', '2018-10-18 12:12:41.092974', '2018-10-18 12:12:41.092974', 1, 1);
INSERT INTO idsmed.idsmed_permission VALUES (35, 5, 'API0000009', 'geProductSearchByStr', 'Get Product Search By API', 1, 3, 'cn', '2018-10-18 12:12:41.092974', '2018-10-18 12:12:41.092974', 1, 1);
INSERT INTO idsmed.idsmed_permission VALUES (36, 5, 'API0000010', 'getFavouriteBrand', 'Get Favourite Brand API', 1, 3, 'cn', '2018-10-18 12:12:41.092974', '2018-10-18 12:12:41.092974', 1, 1);
INSERT INTO idsmed.idsmed_permission VALUES (25, 3, 'PP0000007', 'REJECT', 'Reject Product', 1, 3, 'cn', '2018-09-07 11:32:05', '2018-09-07 11:32:05', 1, 1);
INSERT INTO idsmed.idsmed_permission VALUES (26, 5, 'API0000001', 'GetProductList', 'Get Product List API', 1, 3, 'cn', '2018-09-07 11:32:05', '2018-09-07 11:32:05', 1, 1);
INSERT INTO idsmed.idsmed_permission VALUES (27, 5, 'API0000002', 'GetProductDetail', 'Get Product Detail API', 1, 3, 'cn', '2018-09-07 11:32:05', '2018-09-07 11:32:05', 1, 1);
INSERT INTO idsmed.idsmed_permission VALUES (28, 5, 'API0000003', 'GetProductSearchByKey', 'Get Product By Keyword', 1, 3, 'cn', '2018-09-07 11:32:05', '2018-09-07 11:32:05', 1, 1);
INSERT INTO idsmed.idsmed_permission VALUES (21, 3, 'PP0000003', 'EDIT', 'Edit Product', 1, 3, 'cn', '2018-09-07 11:32:05', '2018-09-07 11:32:05', 1, 1);
INSERT INTO idsmed.idsmed_permission VALUES (22, 3, 'PP0000004', 'REVIEW_DOC', 'Review Product', 1, 3, 'cn', '2018-09-07 11:32:05', '2018-09-07 11:32:05', 1, 1);
INSERT INTO idsmed.idsmed_permission VALUES (23, 3, 'PP0000005', 'APPROVE', 'Approve Product', 1, 3, 'cn', '2018-09-07 11:32:05', '2018-09-07 11:32:05', 1, 1);
INSERT INTO idsmed.idsmed_permission VALUES (24, 3, 'PP0000006', 'INACTIVE', 'Set Inactive Product', 1, 3, 'cn', '2018-09-07 11:32:05', '2018-09-07 11:32:05', 1, 1);
INSERT INTO idsmed.idsmed_permission VALUES (2, 1, 'UA0000002', 'ADD', 'Add User', 1, 3, 'cn', '2018-09-07 11:32:05', '2018-09-07 11:32:05', 1, 1);
INSERT INTO idsmed.idsmed_permission VALUES (1, 1, 'UA0000001', 'VIEW', 'View User', 1, 3, 'cn', '2018-09-07 11:32:05', '2018-09-07 11:32:05', 1, 1);
INSERT INTO idsmed.idsmed_permission VALUES (4, 1, 'UA0000004', 'APPROVE', 'Approve User', 1, 3, 'cn', '2018-09-07 11:32:05', '2018-09-07 11:32:05', 1, 1);
INSERT INTO idsmed.idsmed_permission VALUES (3, 1, 'UA0000003', 'EDIT', 'Edit User', 1, 3, 'cn', '2018-09-07 11:32:05', '2018-09-07 11:32:05', 1, 1);
INSERT INTO idsmed.idsmed_permission VALUES (6, 1, 'UA0000006', 'REJECT', 'Reject User', 1, 3, 'cn', '2018-09-07 11:32:05', '2018-09-07 11:32:05', 1, 1);
INSERT INTO idsmed.idsmed_permission VALUES (5, 1, 'UA0000005', 'INACTIVE', 'Set Inactive User', 1, 3, 'cn', '2018-09-07 11:32:05', '2018-09-07 11:32:05', 1, 1);
INSERT INTO idsmed.idsmed_permission VALUES (8, 4, 'GT0000002', 'ADD', 'Add Access Right', 1, 3, 'cn', '2018-09-07 11:32:05', '2018-09-07 11:32:05', 1, 1);
INSERT INTO idsmed.idsmed_permission VALUES (7, 4, 'GT0000001', 'VIEW', 'View Access Right', 1, 3, 'cn', '2018-09-07 11:32:05', '2018-09-07 11:32:05', 1, 1);
INSERT INTO idsmed.idsmed_permission VALUES (10, 4, 'GT0000004', 'APPROVE', 'Approve Access Right', 1, 3, 'cn', '2018-09-07 11:32:05', '2018-09-07 11:32:05', 1, 1);
INSERT INTO idsmed.idsmed_permission VALUES (9, 4, 'GT0000003', 'EDIT', 'Edit Access Right', 1, 3, 'cn', '2018-09-07 11:32:05', '2018-09-07 11:32:05', 1, 1);
INSERT INTO idsmed.idsmed_permission VALUES (12, 2, 'VP0000001', 'VIEW', 'View Vendor', 1, 3, 'cn', '2018-09-07 11:32:05', '2018-09-07 11:32:05', 1, 1);
INSERT INTO idsmed.idsmed_permission VALUES (11, 4, 'GT0000005', 'INACTIVE', 'Set Inactive Access Right', 1, 3, 'cn', '2018-09-07 11:32:05', '2018-09-07 11:32:05', 1, 1);
INSERT INTO idsmed.idsmed_permission VALUES (14, 2, 'VP0000003', 'EDIT', 'Edit Vendor', 1, 3, 'cn', '2018-09-07 11:32:05', '2018-09-07 11:32:05', 1, 1);
INSERT INTO idsmed.idsmed_permission VALUES (13, 2, 'VP0000002', 'ADD', 'Add Vendor', 1, 3, 'cn', '2018-09-07 11:32:05', '2018-09-07 11:32:05', 1, 1);
INSERT INTO idsmed.idsmed_permission VALUES (16, 2, 'VP0000005', 'APPROVE', 'Approve Vendor', 1, 3, 'cn', '2018-09-07 11:32:05', '2018-09-07 11:32:05', 1, 1);
INSERT INTO idsmed.idsmed_permission VALUES (15, 2, 'VP0000004', 'REVIEW_DOC', 'Review Vendor', 1, 3, 'cn', '2018-09-07 11:32:05', '2018-09-07 11:32:05', 1, 1);
INSERT INTO idsmed.idsmed_permission VALUES (18, 2, 'VP0000007', 'REJECT', 'Reject Vendor', 1, 3, 'cn', '2018-09-07 11:32:05', '2018-09-07 11:32:05', 1, 1);
INSERT INTO idsmed.idsmed_permission VALUES (17, 2, 'VP0000006', 'INACTIVE', 'Set Inactive Vendor', 1, 3, 'cn', '2018-09-07 11:32:05', '2018-09-07 11:32:05', 1, 1);
INSERT INTO idsmed.idsmed_permission VALUES (20, 3, 'PP0000002', 'ADD', 'Add Product', 1, 3, 'cn', '2018-09-07 11:32:05', '2018-09-07 11:32:05', 1, 1);
INSERT INTO idsmed.idsmed_permission VALUES (19, 3, 'PP0000001', 'VIEW', 'View Product', 1, 3, 'cn', '2018-09-07 11:32:05', '2018-09-07 11:32:05', 1, 1);
INSERT INTO idsmed.idsmed_permission VALUES (37, 6, 'VPC0000001', 'VIEW', 'View Company Profile', 1, 3, 'cn', '2018-10-29 16:16:48.396855', '2018-10-29 16:16:48.396855', 1, 1);
INSERT INTO idsmed.idsmed_permission VALUES (38, 6, 'VPC0000002', 'EDIT', 'Edit Company Profile', 1, 3, 'cn', '2018-10-29 16:16:48.396855', '2018-10-29 16:16:48.396855', 1, 1);
INSERT INTO idsmed.idsmed_permission VALUES (40, 7, 'PS00000001', 'VIEW', 'View Product Search', 1, 3, 'cn', '2018-10-29 16:16:48.396855', '2018-10-29 16:16:48.396855', 1, 1);
INSERT INTO idsmed.idsmed_permission VALUES (41, 8, 'VC00000001', 'VIEW', 'View Vendor Configuration', 1, 3, 'cn', '2018-10-29 16:16:48.396855', '2018-10-29 16:16:48.396855', 1, 1);
INSERT INTO idsmed.idsmed_permission VALUES (42, 8, 'VC00000002', 'ADD', 'Add Vendor Configuration', 1, 3, 'cn', '2018-10-29 16:16:48.396855', '2018-10-29 16:16:48.396855', 1, 1);
INSERT INTO idsmed.idsmed_permission VALUES (43, 8, 'VC00000003', 'EDIT', 'Edit Vendor Configuration', 1, 3, 'cn', '2018-10-29 16:16:48.396855', '2018-10-29 16:16:48.396855', 1, 1);
INSERT INTO idsmed.idsmed_permission VALUES (44, 8, 'VC00000004', 'APPROVE', 'Approve Vendor Configuration', 1, 3, 'cn', '2018-10-29 16:16:48.396855', '2018-10-29 16:16:48.396855', 1, 1);
INSERT INTO idsmed.idsmed_permission VALUES (46, 9, 'PC00000001', 'VIEW', 'View Product Configuration', 1, 3, 'cn', '2018-10-29 16:16:48.396855', '2018-10-29 16:16:48.396855', 1, 1);
INSERT INTO idsmed.idsmed_permission VALUES (47, 9, 'PC00000002', 'ADD', 'Add Product Configuration', 1, 3, 'cn', '2018-10-29 16:16:48.396855', '2018-10-29 16:16:48.396855', 1, 1);
INSERT INTO idsmed.idsmed_permission VALUES (48, 9, 'PC00000003', 'EDIT', 'Edit Product Configuration', 1, 3, 'cn', '2018-10-29 16:16:48.396855', '2018-10-29 16:16:48.396855', 1, 1);
INSERT INTO idsmed.idsmed_permission VALUES (49, 9, 'PC00000004', 'APPROVE', 'Approve Product Configuration', 1, 3, 'cn', '2018-10-29 16:16:48.396855', '2018-10-29 16:16:48.396855', 1, 1);
INSERT INTO idsmed.idsmed_permission VALUES (51, 10, 'EC00000001', 'VIEW', 'View Email Configuration', 1, 3, 'cn', '2018-10-29 16:16:48.396855', '2018-10-29 16:16:48.396855', 1, 1);
INSERT INTO idsmed.idsmed_permission VALUES (52, 10, 'EC00000002', 'ADD', 'Add Email Configuration', 1, 3, 'cn', '2018-10-29 16:16:48.396855', '2018-10-29 16:16:48.396855', 1, 1);
INSERT INTO idsmed.idsmed_permission VALUES (53, 10, 'EC00000003', 'EDIT', 'Edit Email Configuration', 1, 3, 'cn', '2018-10-29 16:16:48.396855', '2018-10-29 16:16:48.396855', 1, 1);
INSERT INTO idsmed.idsmed_permission VALUES (54, 10, 'EC00000004', 'APPROVE', 'Approve Email Configuration', 1, 3, 'cn', '2018-10-29 16:16:48.396855', '2018-10-29 16:16:48.396855', 1, 1);
INSERT INTO idsmed.idsmed_permission VALUES (58, 13, 'ENC0000001', 'EDIT', 'Edit Email Notification Configuration', 1, 3, 'cn', '2018-10-29 16:16:48.396855', '2018-10-29 16:16:48.396855', 1, 1);
INSERT INTO idsmed.idsmed_permission VALUES (59, 13, 'ENC0000002', 'VIEW', 'View Email Notification Configuration', 1, 3, 'cn', '2018-10-29 19:15:05.261449', '2018-10-29 19:15:05.261449', 1, 1);
INSERT INTO idsmed.idsmed_permission VALUES (60, 1, 'UA0000007', 'RESUME/ACTIVATE', 'Resume / Activate User Profile', 1, 3, 'cn', '2018-11-21 20:24:49.650011', '2018-11-21 20:24:49.650011', 1, 1);
INSERT INTO idsmed.idsmed_permission VALUES (61, 2, 'VP0000008', 'RESUME/ACTIVATE', 'Resume / Activate Vendor Profile', 1, 3, 'cn', '2018-11-21 20:24:49.650011', '2018-11-21 20:24:49.650011', 1, 1);
INSERT INTO idsmed.idsmed_permission VALUES (62, 2, 'VP0000009', 'TERMINATE', 'Terminate Vendor Profile', 1, 3, 'cn', '2018-11-21 20:24:49.650011', '2018-11-21 20:24:49.650011', 1, 1);
INSERT INTO idsmed.idsmed_permission VALUES (63, 3, 'PP0000008', 'RESUME/ACTIVATE', 'Resume / Activate Product', 1, 3, 'cn', '2018-11-21 20:24:49.650011', '2018-11-21 20:24:49.650011', 1, 1);
INSERT INTO idsmed.idsmed_permission VALUES (56, 11, 'PS10000001', 'VIEW', 'View Product Main Page 1', 1, 3, 'cn', '2018-10-29 16:16:48.396855', '2018-10-29 16:16:48.396855', 1, 1);
INSERT INTO idsmed.idsmed_permission VALUES (57, 12, 'PS20000001', 'VIEW', 'View Product Main Page 2', 1, 3, 'cn', '2018-10-29 16:16:48.396855', '2018-10-29 16:16:48.396855', 1, 1);
INSERT INTO idsmed.idsmed_permission VALUES (64, 15, 'PN0000001', 'VIEW', 'View Order List', 1, 3, 'cn', '2019-01-07 08:09:01.327536', '2019-01-07 08:09:01.327536', 1, 1);
INSERT INTO idsmed.idsmed_permission VALUES (65, 15, 'PN0000002', 'EDIT', 'Edit Order List', 1, 3, 'cn', '2019-01-07 08:09:01.327536', '2019-01-07 08:09:01.327536', 1, 1);
INSERT INTO idsmed.idsmed_permission VALUES (66, 16, 'TE0000001', 'VIEW', 'View Tender List', 1, 3, 'cn', '2019-01-07 08:09:01.327536', '2019-01-07 08:09:01.327536', 1, 1);
INSERT INTO idsmed.idsmed_permission VALUES (67, 16, 'TE0000002', 'EDIT', 'Edit Tender List', 1, 3, 'cn', '2019-01-07 08:09:01.327536', '2019-01-07 08:09:01.327536', 1, 1);
INSERT INTO idsmed.idsmed_permission VALUES (68, 4, 'GT0000006', 'RESUME/ACTIVATE', 'Resume / Activate Access Right', 1, 3, 'cn', '2019-01-28 14:46:37.885278', '2019-01-28 14:46:37.885278', 1, 1);


--
-- TOC entry 4106 (class 0 OID 0)
-- Dependencies: 224
-- Name: idsmed_permission_id_seq; Type: SEQUENCE SET; Schema: idsmed; Owner: dbadmin
--

SELECT pg_catalog.setval('idsmed.idsmed_permission_id_seq', 68, true);


--
-- TOC entry 3841 (class 0 OID 97471)
-- Dependencies: 225
-- Data for Name: idsmed_role; Type: TABLE DATA; Schema: idsmed; Owner: dbadmin
--

INSERT INTO idsmed.idsmed_role VALUES (17, '017', 'SCP Admin', NULL, 3, 'cn', '2018-11-11 16:21:09.2', '2018-11-11 16:21:09.2', -1, -1, 1);
INSERT INTO idsmed.idsmed_role VALUES (29, '029', 'IT - idsMED WeDoc Admin', NULL, 3, 'cn', '2019-01-25 17:26:34.301', '2019-01-25 17:26:34.301', -1, -1, 1);
INSERT INTO idsmed.idsmed_role VALUES (30, '030', 'IT - idsMED CMG', NULL, 3, 'cn', '2019-01-25 17:27:33.847', '2019-01-25 17:27:33.847', -1, -1, 1);
INSERT INTO idsmed.idsmed_role VALUES (31, '031', 'Operation', NULL, 3, 'cn', '2019-01-25 21:53:51.983', '2019-01-25 21:53:51.983', -1, -1, 1);
INSERT INTO idsmed.idsmed_role VALUES (32, '032', 'Sales', NULL, 3, 'cn', '2019-01-28 09:39:42.229', '2019-01-28 09:39:42.229', -1, -1, 1);
INSERT INTO idsmed.idsmed_role VALUES (14, 'Ven001', 'Vendor User', NULL, 3, 'cn', '2018-11-07 16:51:08.142', '2019-01-28 12:14:42.741', -1, -1, 0);
INSERT INTO idsmed.idsmed_role VALUES (15, 'Ven002', 'Vendor Admin', NULL, 3, 'cn', '2018-11-07 16:58:34.453', '2019-01-28 12:14:52.335', -1, -1, 0);
INSERT INTO idsmed.idsmed_role VALUES (1, '001', 'Super Admin', 'Super Admin', 3, 'cn', '2018-09-07 11:32:05', '2018-10-29 19:00:18.175', 1, 1, 1);
INSERT INTO idsmed.idsmed_role VALUES (8, '008', 'We Doctor Role', 'We Doctor Role', 3, 'cn', '2018-09-07 11:32:05', '2018-10-30 15:32:13.918', 1, 1, 1);


--
-- TOC entry 4107 (class 0 OID 0)
-- Dependencies: 226
-- Name: idsmed_role_id_seq; Type: SEQUENCE SET; Schema: idsmed; Owner: dbadmin
--

SELECT pg_catalog.setval('idsmed.idsmed_role_id_seq', 33, true);


--
-- TOC entry 3843 (class 0 OID 97482)
-- Dependencies: 227
-- Data for Name: idsmed_role_permission; Type: TABLE DATA; Schema: idsmed; Owner: dbadmin
--

INSERT INTO idsmed.idsmed_role_permission VALUES (1144, 29, 7, 0, 'cn', '2019-01-25 17:26:34.383', '2019-01-25 17:26:34.383', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1145, 29, 8, 0, 'cn', '2019-01-25 17:26:34.384', '2019-01-25 17:26:34.384', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1146, 29, 9, 0, 'cn', '2019-01-25 17:26:34.385', '2019-01-25 17:26:34.385', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1147, 29, 11, 0, 'cn', '2019-01-25 17:26:34.386', '2019-01-25 17:26:34.386', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1148, 29, 1, 0, 'cn', '2019-01-25 17:26:34.387', '2019-01-25 17:26:34.387', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1149, 29, 2, 0, 'cn', '2019-01-25 17:26:34.388', '2019-01-25 17:26:34.388', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1150, 29, 3, 0, 'cn', '2019-01-25 17:26:34.389', '2019-01-25 17:26:34.389', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1151, 29, 4, 0, 'cn', '2019-01-25 17:26:34.39', '2019-01-25 17:26:34.39', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1152, 29, 6, 0, 'cn', '2019-01-25 17:26:34.391', '2019-01-25 17:26:34.391', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1153, 29, 5, 0, 'cn', '2019-01-25 17:26:34.391', '2019-01-25 17:26:34.391', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1154, 29, 60, 0, 'cn', '2019-01-25 17:26:34.392', '2019-01-25 17:26:34.392', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1155, 29, 12, 0, 'cn', '2019-01-25 17:26:34.393', '2019-01-25 17:26:34.393', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1156, 29, 13, 0, 'cn', '2019-01-25 17:26:34.394', '2019-01-25 17:26:34.394', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1157, 29, 14, 0, 'cn', '2019-01-25 17:26:34.395', '2019-01-25 17:26:34.395', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1334, 15, 1, 0, 'cn', '2019-01-28 12:16:44.72', '2019-01-28 12:16:44.72', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1335, 15, 3, 0, 'cn', '2019-01-28 12:16:44.721', '2019-01-28 12:16:44.721', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1336, 15, 4, 0, 'cn', '2019-01-28 12:16:44.722', '2019-01-28 12:16:44.722', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1337, 15, 5, 0, 'cn', '2019-01-28 12:16:44.723', '2019-01-28 12:16:44.723', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1338, 15, 6, 0, 'cn', '2019-01-28 12:16:44.724', '2019-01-28 12:16:44.724', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1339, 15, 19, 0, 'cn', '2019-01-28 12:16:44.724', '2019-01-28 12:16:44.724', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1158, 29, 15, 0, 'cn', '2019-01-25 17:26:34.396', '2019-01-25 17:26:34.396', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1159, 29, 16, 0, 'cn', '2019-01-25 17:26:34.397', '2019-01-25 17:26:34.397', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1160, 29, 18, 0, 'cn', '2019-01-25 17:26:34.397', '2019-01-25 17:26:34.397', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1161, 29, 17, 0, 'cn', '2019-01-25 17:26:34.398', '2019-01-25 17:26:34.398', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1162, 29, 61, 0, 'cn', '2019-01-25 17:26:34.399', '2019-01-25 17:26:34.399', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1163, 29, 62, 0, 'cn', '2019-01-25 17:26:34.4', '2019-01-25 17:26:34.4', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1164, 29, 40, 0, 'cn', '2019-01-25 17:26:34.401', '2019-01-25 17:26:34.401', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1165, 29, 56, 0, 'cn', '2019-01-25 17:26:34.402', '2019-01-25 17:26:34.402', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1166, 29, 57, 0, 'cn', '2019-01-25 17:26:34.403', '2019-01-25 17:26:34.403', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1167, 29, 19, 0, 'cn', '2019-01-25 17:26:34.403', '2019-01-25 17:26:34.403', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1168, 29, 20, 0, 'cn', '2019-01-25 17:26:34.404', '2019-01-25 17:26:34.404', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1169, 29, 21, 0, 'cn', '2019-01-25 17:26:34.405', '2019-01-25 17:26:34.405', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1170, 29, 22, 0, 'cn', '2019-01-25 17:26:34.406', '2019-01-25 17:26:34.406', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1171, 29, 23, 0, 'cn', '2019-01-25 17:26:34.407', '2019-01-25 17:26:34.407', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1172, 29, 25, 0, 'cn', '2019-01-25 17:26:34.408', '2019-01-25 17:26:34.408', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1173, 29, 24, 0, 'cn', '2019-01-25 17:26:34.408', '2019-01-25 17:26:34.408', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1174, 29, 63, 0, 'cn', '2019-01-25 17:26:34.409', '2019-01-25 17:26:34.409', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1175, 29, 41, 0, 'cn', '2019-01-25 17:26:34.41', '2019-01-25 17:26:34.41', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1176, 29, 46, 0, 'cn', '2019-01-25 17:26:34.411', '2019-01-25 17:26:34.411', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1177, 29, 51, 0, 'cn', '2019-01-25 17:26:34.412', '2019-01-25 17:26:34.412', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1178, 29, 59, 0, 'cn', '2019-01-25 17:26:34.413', '2019-01-25 17:26:34.413', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1179, 29, 42, 0, 'cn', '2019-01-25 17:26:34.413', '2019-01-25 17:26:34.413', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1180, 29, 47, 0, 'cn', '2019-01-25 17:26:34.414', '2019-01-25 17:26:34.414', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1181, 29, 52, 0, 'cn', '2019-01-25 17:26:34.415', '2019-01-25 17:26:34.415', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1182, 29, 43, 0, 'cn', '2019-01-25 17:26:34.416', '2019-01-25 17:26:34.416', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1183, 29, 48, 0, 'cn', '2019-01-25 17:26:34.417', '2019-01-25 17:26:34.417', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1184, 29, 53, 0, 'cn', '2019-01-25 17:26:34.418', '2019-01-25 17:26:34.418', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1185, 29, 58, 0, 'cn', '2019-01-25 17:26:34.42', '2019-01-25 17:26:34.42', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1186, 29, 64, 0, 'cn', '2019-01-25 17:26:34.42', '2019-01-25 17:26:34.42', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1187, 29, 65, 0, 'cn', '2019-01-25 17:26:34.421', '2019-01-25 17:26:34.421', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1188, 29, 67, 0, 'cn', '2019-01-25 17:26:34.422', '2019-01-25 17:26:34.422', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1189, 29, 66, 0, 'cn', '2019-01-25 17:26:34.423', '2019-01-25 17:26:34.423', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1215, 30, 57, 0, 'cn', '2019-01-25 17:27:33.966', '2019-01-25 17:27:33.966', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1216, 30, 19, 0, 'cn', '2019-01-25 17:27:33.967', '2019-01-25 17:27:33.967', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1217, 30, 20, 0, 'cn', '2019-01-25 17:27:33.967', '2019-01-25 17:27:33.967', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1218, 30, 21, 0, 'cn', '2019-01-25 17:27:33.968', '2019-01-25 17:27:33.968', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1219, 30, 22, 0, 'cn', '2019-01-25 17:27:33.969', '2019-01-25 17:27:33.969', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1220, 30, 23, 0, 'cn', '2019-01-25 17:27:33.97', '2019-01-25 17:27:33.97', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1221, 30, 25, 0, 'cn', '2019-01-25 17:27:33.971', '2019-01-25 17:27:33.971', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1222, 30, 24, 0, 'cn', '2019-01-25 17:27:33.972', '2019-01-25 17:27:33.972', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1223, 30, 63, 0, 'cn', '2019-01-25 17:27:33.973', '2019-01-25 17:27:33.973', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1224, 30, 41, 0, 'cn', '2019-01-25 17:27:33.974', '2019-01-25 17:27:33.974', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1225, 30, 46, 0, 'cn', '2019-01-25 17:27:33.975', '2019-01-25 17:27:33.975', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1226, 30, 51, 0, 'cn', '2019-01-25 17:27:33.976', '2019-01-25 17:27:33.976', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1227, 30, 59, 0, 'cn', '2019-01-25 17:27:33.977', '2019-01-25 17:27:33.977', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1190, 30, 7, 0, 'cn', '2019-01-25 17:27:33.942', '2019-01-25 17:27:33.942', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1191, 30, 8, 0, 'cn', '2019-01-25 17:27:33.943', '2019-01-25 17:27:33.943', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1192, 30, 9, 0, 'cn', '2019-01-25 17:27:33.944', '2019-01-25 17:27:33.944', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1340, 15, 20, 0, 'cn', '2019-01-28 12:16:44.725', '2019-01-28 12:16:44.725', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1341, 15, 21, 0, 'cn', '2019-01-28 12:16:44.726', '2019-01-28 12:16:44.726', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1342, 15, 22, 0, 'cn', '2019-01-28 12:16:44.727', '2019-01-28 12:16:44.727', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1343, 15, 23, 0, 'cn', '2019-01-28 12:16:44.728', '2019-01-28 12:16:44.728', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1344, 15, 24, 0, 'cn', '2019-01-28 12:16:44.729', '2019-01-28 12:16:44.729', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1345, 15, 25, 0, 'cn', '2019-01-28 12:16:44.729', '2019-01-28 12:16:44.729', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1346, 15, 37, 0, 'cn', '2019-01-28 12:16:44.73', '2019-01-28 12:16:44.73', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1347, 15, 38, 0, 'cn', '2019-01-28 12:16:44.731', '2019-01-28 12:16:44.731', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1348, 15, 63, 0, 'cn', '2019-01-28 12:16:44.732', '2019-01-28 12:16:44.732', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1117, 17, 7, 0, 'cn', '2019-01-25 17:13:51.026', '2019-01-25 17:13:51.026', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1118, 17, 8, 0, 'cn', '2019-01-25 17:13:51.027', '2019-01-25 17:13:51.027', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1119, 17, 9, 0, 'cn', '2019-01-25 17:13:51.028', '2019-01-25 17:13:51.028', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1120, 17, 11, 0, 'cn', '2019-01-25 17:13:51.029', '2019-01-25 17:13:51.029', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1121, 17, 40, 0, 'cn', '2019-01-25 17:13:51.03', '2019-01-25 17:13:51.03', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1122, 17, 41, 0, 'cn', '2019-01-25 17:13:51.031', '2019-01-25 17:13:51.031', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1123, 17, 42, 0, 'cn', '2019-01-25 17:13:51.032', '2019-01-25 17:13:51.032', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1124, 17, 43, 0, 'cn', '2019-01-25 17:13:51.033', '2019-01-25 17:13:51.033', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1125, 17, 46, 0, 'cn', '2019-01-25 17:13:51.034', '2019-01-25 17:13:51.034', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1126, 17, 47, 0, 'cn', '2019-01-25 17:13:51.035', '2019-01-25 17:13:51.035', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1127, 17, 48, 0, 'cn', '2019-01-25 17:13:51.036', '2019-01-25 17:13:51.036', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1128, 17, 51, 0, 'cn', '2019-01-25 17:13:51.037', '2019-01-25 17:13:51.037', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1129, 17, 52, 0, 'cn', '2019-01-25 17:13:51.038', '2019-01-25 17:13:51.038', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1130, 17, 53, 0, 'cn', '2019-01-25 17:13:51.039', '2019-01-25 17:13:51.039', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1131, 17, 56, 0, 'cn', '2019-01-25 17:13:51.04', '2019-01-25 17:13:51.04', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1132, 17, 57, 0, 'cn', '2019-01-25 17:13:51.041', '2019-01-25 17:13:51.041', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1133, 17, 58, 0, 'cn', '2019-01-25 17:13:51.042', '2019-01-25 17:13:51.042', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1134, 17, 59, 0, 'cn', '2019-01-25 17:13:51.043', '2019-01-25 17:13:51.043', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1135, 17, 64, 0, 'cn', '2019-01-25 17:13:51.044', '2019-01-25 17:13:51.044', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1136, 17, 65, 0, 'cn', '2019-01-25 17:13:51.045', '2019-01-25 17:13:51.045', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1137, 17, 66, 0, 'cn', '2019-01-25 17:13:51.045', '2019-01-25 17:13:51.045', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1138, 17, 67, 0, 'cn', '2019-01-25 17:13:51.046', '2019-01-25 17:13:51.046', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1139, 17, 20, 0, 'cn', '2019-01-25 17:13:51.047', '2019-01-25 17:13:51.047', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1140, 17, 21, 0, 'cn', '2019-01-25 17:13:51.048', '2019-01-25 17:13:51.048', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1141, 17, 22, 0, 'cn', '2019-01-25 17:13:51.049', '2019-01-25 17:13:51.049', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1142, 17, 63, 0, 'cn', '2019-01-25 17:13:51.05', '2019-01-25 17:13:51.05', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1143, 17, 10, 0, 'cn', '2019-01-25 17:13:51.051', '2019-01-25 17:13:51.051', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1193, 30, 1, 0, 'cn', '2019-01-25 17:27:33.945', '2019-01-25 17:27:33.945', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1194, 30, 2, 0, 'cn', '2019-01-25 17:27:33.946', '2019-01-25 17:27:33.946', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1195, 30, 3, 0, 'cn', '2019-01-25 17:27:33.947', '2019-01-25 17:27:33.947', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (629, 8, 26, 0, 'cn', '2018-11-26 11:33:03.561', '2018-11-26 11:33:03.561', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (630, 8, 27, 0, 'cn', '2018-11-26 11:33:03.562', '2018-11-26 11:33:03.562', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (631, 8, 28, 0, 'cn', '2018-11-26 11:33:03.563', '2018-11-26 11:33:03.563', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (632, 8, 29, 0, 'cn', '2018-11-26 11:33:03.564', '2018-11-26 11:33:03.564', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (633, 8, 30, 0, 'cn', '2018-11-26 11:33:03.565', '2018-11-26 11:33:03.565', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (634, 8, 31, 0, 'cn', '2018-11-26 11:33:03.566', '2018-11-26 11:33:03.566', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (635, 8, 32, 0, 'cn', '2018-11-26 11:33:03.567', '2018-11-26 11:33:03.567', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (636, 8, 33, 0, 'cn', '2018-11-26 11:33:03.567', '2018-11-26 11:33:03.567', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (637, 8, 34, 0, 'cn', '2018-11-26 11:33:03.568', '2018-11-26 11:33:03.568', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (638, 8, 35, 0, 'cn', '2018-11-26 11:33:03.569', '2018-11-26 11:33:03.569', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (639, 8, 36, 0, 'cn', '2018-11-26 11:33:03.57', '2018-11-26 11:33:03.57', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (640, 8, 40, 0, 'cn', '2018-11-26 11:33:03.571', '2018-11-26 11:33:03.571', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (641, 8, 56, 0, 'cn', '2018-11-26 11:33:03.572', '2018-11-26 11:33:03.572', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (642, 8, 57, 0, 'cn', '2018-11-26 11:33:03.573', '2018-11-26 11:33:03.573', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1349, 14, 19, 0, 'cn', '2019-01-28 12:18:09.459', '2019-01-28 12:18:09.459', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1196, 30, 4, 0, 'cn', '2019-01-25 17:27:33.948', '2019-01-25 17:27:33.948', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1197, 30, 5, 0, 'cn', '2019-01-25 17:27:33.949', '2019-01-25 17:27:33.949', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1198, 30, 6, 0, 'cn', '2019-01-25 17:27:33.95', '2019-01-25 17:27:33.95', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1199, 30, 11, 0, 'cn', '2019-01-25 17:27:33.951', '2019-01-25 17:27:33.951', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1200, 30, 60, 0, 'cn', '2019-01-25 17:27:33.952', '2019-01-25 17:27:33.952', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1201, 30, 12, 0, 'cn', '2019-01-25 17:27:33.953', '2019-01-25 17:27:33.953', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1202, 30, 13, 0, 'cn', '2019-01-25 17:27:33.954', '2019-01-25 17:27:33.954', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1203, 30, 14, 0, 'cn', '2019-01-25 17:27:33.955', '2019-01-25 17:27:33.955', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1204, 30, 15, 0, 'cn', '2019-01-25 17:27:33.955', '2019-01-25 17:27:33.955', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1205, 30, 16, 0, 'cn', '2019-01-25 17:27:33.956', '2019-01-25 17:27:33.956', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1350, 14, 20, 0, 'cn', '2019-01-28 12:18:09.46', '2019-01-28 12:18:09.46', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1351, 14, 21, 0, 'cn', '2019-01-28 12:18:09.461', '2019-01-28 12:18:09.461', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1352, 14, 37, 0, 'cn', '2019-01-28 12:18:09.462', '2019-01-28 12:18:09.462', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1098, 17, 1, 0, 'cn', '2019-01-25 17:13:51.008', '2019-01-25 17:13:51.008', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1099, 17, 2, 0, 'cn', '2019-01-25 17:13:51.009', '2019-01-25 17:13:51.009', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1100, 17, 3, 0, 'cn', '2019-01-25 17:13:51.01', '2019-01-25 17:13:51.01', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1101, 17, 4, 0, 'cn', '2019-01-25 17:13:51.011', '2019-01-25 17:13:51.011', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1102, 17, 5, 0, 'cn', '2019-01-25 17:13:51.012', '2019-01-25 17:13:51.012', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1103, 17, 6, 0, 'cn', '2019-01-25 17:13:51.013', '2019-01-25 17:13:51.013', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1104, 17, 60, 0, 'cn', '2019-01-25 17:13:51.014', '2019-01-25 17:13:51.014', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1105, 17, 12, 0, 'cn', '2019-01-25 17:13:51.015', '2019-01-25 17:13:51.015', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1106, 17, 14, 0, 'cn', '2019-01-25 17:13:51.016', '2019-01-25 17:13:51.016', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1107, 17, 15, 0, 'cn', '2019-01-25 17:13:51.016', '2019-01-25 17:13:51.016', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1108, 17, 16, 0, 'cn', '2019-01-25 17:13:51.018', '2019-01-25 17:13:51.018', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1109, 17, 17, 0, 'cn', '2019-01-25 17:13:51.018', '2019-01-25 17:13:51.018', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1110, 17, 18, 0, 'cn', '2019-01-25 17:13:51.019', '2019-01-25 17:13:51.019', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1111, 17, 61, 0, 'cn', '2019-01-25 17:13:51.02', '2019-01-25 17:13:51.02', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1112, 17, 62, 0, 'cn', '2019-01-25 17:13:51.021', '2019-01-25 17:13:51.021', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1113, 17, 19, 0, 'cn', '2019-01-25 17:13:51.022', '2019-01-25 17:13:51.022', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1114, 17, 23, 0, 'cn', '2019-01-25 17:13:51.023', '2019-01-25 17:13:51.023', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1115, 17, 24, 0, 'cn', '2019-01-25 17:13:51.024', '2019-01-25 17:13:51.024', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1116, 17, 25, 0, 'cn', '2019-01-25 17:13:51.025', '2019-01-25 17:13:51.025', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1206, 30, 18, 0, 'cn', '2019-01-25 17:27:33.957', '2019-01-25 17:27:33.957', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1207, 30, 17, 0, 'cn', '2019-01-25 17:27:33.958', '2019-01-25 17:27:33.958', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1208, 30, 61, 0, 'cn', '2019-01-25 17:27:33.959', '2019-01-25 17:27:33.959', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1209, 30, 62, 0, 'cn', '2019-01-25 17:27:33.96', '2019-01-25 17:27:33.96', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1210, 30, 37, 0, 'cn', '2019-01-25 17:27:33.961', '2019-01-25 17:27:33.961', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1211, 30, 38, 0, 'cn', '2019-01-25 17:27:33.962', '2019-01-25 17:27:33.962', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1213, 30, 40, 0, 'cn', '2019-01-25 17:27:33.964', '2019-01-25 17:27:33.964', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1214, 30, 56, 0, 'cn', '2019-01-25 17:27:33.965', '2019-01-25 17:27:33.965', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1228, 30, 42, 0, 'cn', '2019-01-25 17:27:33.978', '2019-01-25 17:27:33.978', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1229, 30, 47, 0, 'cn', '2019-01-25 17:27:33.979', '2019-01-25 17:27:33.979', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1230, 30, 52, 0, 'cn', '2019-01-25 17:27:33.98', '2019-01-25 17:27:33.98', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1231, 30, 43, 0, 'cn', '2019-01-25 17:27:33.98', '2019-01-25 17:27:33.98', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1232, 30, 48, 0, 'cn', '2019-01-25 17:27:33.981', '2019-01-25 17:27:33.981', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1233, 30, 53, 0, 'cn', '2019-01-25 17:27:33.982', '2019-01-25 17:27:33.982', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1234, 30, 58, 0, 'cn', '2019-01-25 17:27:33.983', '2019-01-25 17:27:33.983', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1235, 30, 64, 0, 'cn', '2019-01-25 17:27:33.984', '2019-01-25 17:27:33.984', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1236, 30, 65, 0, 'cn', '2019-01-25 17:27:33.985', '2019-01-25 17:27:33.985', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1237, 30, 66, 0, 'cn', '2019-01-25 17:27:33.986', '2019-01-25 17:27:33.986', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1238, 30, 67, 0, 'cn', '2019-01-25 17:27:33.987', '2019-01-25 17:27:33.987', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1251, 31, 1, 0, 'cn', '2019-01-28 09:17:03.331', '2019-01-28 09:17:03.331', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1252, 31, 12, 0, 'cn', '2019-01-28 09:17:03.333', '2019-01-28 09:17:03.333', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1253, 31, 14, 0, 'cn', '2019-01-28 09:17:03.333', '2019-01-28 09:17:03.333', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1254, 31, 15, 0, 'cn', '2019-01-28 09:17:03.334', '2019-01-28 09:17:03.334', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1255, 31, 16, 0, 'cn', '2019-01-28 09:17:03.335', '2019-01-28 09:17:03.335', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1256, 31, 17, 0, 'cn', '2019-01-28 09:17:03.336', '2019-01-28 09:17:03.336', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1257, 31, 18, 0, 'cn', '2019-01-28 09:17:03.337', '2019-01-28 09:17:03.337', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1258, 31, 61, 0, 'cn', '2019-01-28 09:17:03.338', '2019-01-28 09:17:03.338', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1259, 31, 62, 0, 'cn', '2019-01-28 09:17:03.339', '2019-01-28 09:17:03.339', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1260, 31, 19, 0, 'cn', '2019-01-28 09:17:03.34', '2019-01-28 09:17:03.34', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1261, 31, 21, 0, 'cn', '2019-01-28 09:17:03.34', '2019-01-28 09:17:03.34', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1262, 31, 22, 0, 'cn', '2019-01-28 09:17:03.341', '2019-01-28 09:17:03.341', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1263, 31, 40, 0, 'cn', '2019-01-28 09:17:03.342', '2019-01-28 09:17:03.342', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1264, 31, 56, 0, 'cn', '2019-01-28 09:17:03.343', '2019-01-28 09:17:03.343', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1265, 31, 57, 0, 'cn', '2019-01-28 09:17:03.344', '2019-01-28 09:17:03.344', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1280, 32, 12, 0, 'cn', '2019-01-28 11:36:18.25', '2019-01-28 11:36:18.25', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1281, 32, 14, 0, 'cn', '2019-01-28 11:36:18.251', '2019-01-28 11:36:18.251', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1282, 32, 15, 0, 'cn', '2019-01-28 11:36:18.252', '2019-01-28 11:36:18.252', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1283, 32, 16, 0, 'cn', '2019-01-28 11:36:18.253', '2019-01-28 11:36:18.253', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1284, 32, 17, 0, 'cn', '2019-01-28 11:36:18.254', '2019-01-28 11:36:18.254', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1285, 32, 18, 0, 'cn', '2019-01-28 11:36:18.255', '2019-01-28 11:36:18.255', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1286, 32, 61, 0, 'cn', '2019-01-28 11:36:18.257', '2019-01-28 11:36:18.257', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1287, 32, 62, 0, 'cn', '2019-01-28 11:36:18.258', '2019-01-28 11:36:18.258', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1288, 32, 19, 0, 'cn', '2019-01-28 11:36:18.258', '2019-01-28 11:36:18.258', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1289, 32, 21, 0, 'cn', '2019-01-28 11:36:18.259', '2019-01-28 11:36:18.259', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1290, 32, 22, 0, 'cn', '2019-01-28 11:36:18.26', '2019-01-28 11:36:18.26', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1291, 32, 40, 0, 'cn', '2019-01-28 11:36:18.261', '2019-01-28 11:36:18.261', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1292, 32, 56, 0, 'cn', '2019-01-28 11:36:18.262', '2019-01-28 11:36:18.262', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1293, 32, 57, 0, 'cn', '2019-01-28 11:36:18.263', '2019-01-28 11:36:18.263', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1294, 32, 1, 0, 'cn', '2019-01-28 11:36:18.264', '2019-01-28 11:36:18.264', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1405, 1, 1, 0, 'cn', '2019-01-28 17:06:14.232', '2019-01-28 17:06:14.232', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1406, 1, 2, 0, 'cn', '2019-01-28 17:06:14.233', '2019-01-28 17:06:14.233', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1407, 1, 3, 0, 'cn', '2019-01-28 17:06:14.234', '2019-01-28 17:06:14.234', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1408, 1, 4, 0, 'cn', '2019-01-28 17:06:14.235', '2019-01-28 17:06:14.235', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1409, 1, 5, 0, 'cn', '2019-01-28 17:06:14.236', '2019-01-28 17:06:14.236', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1410, 1, 6, 0, 'cn', '2019-01-28 17:06:14.238', '2019-01-28 17:06:14.238', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1411, 1, 12, 0, 'cn', '2019-01-28 17:06:14.239', '2019-01-28 17:06:14.239', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1412, 1, 13, 0, 'cn', '2019-01-28 17:06:14.24', '2019-01-28 17:06:14.24', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1413, 1, 14, 0, 'cn', '2019-01-28 17:06:14.241', '2019-01-28 17:06:14.241', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1414, 1, 15, 0, 'cn', '2019-01-28 17:06:14.242', '2019-01-28 17:06:14.242', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1415, 1, 16, 0, 'cn', '2019-01-28 17:06:14.243', '2019-01-28 17:06:14.243', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1416, 1, 17, 0, 'cn', '2019-01-28 17:06:14.244', '2019-01-28 17:06:14.244', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1417, 1, 18, 0, 'cn', '2019-01-28 17:06:14.246', '2019-01-28 17:06:14.246', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1418, 1, 19, 0, 'cn', '2019-01-28 17:06:14.246', '2019-01-28 17:06:14.246', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1419, 1, 20, 0, 'cn', '2019-01-28 17:06:14.247', '2019-01-28 17:06:14.247', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1420, 1, 21, 0, 'cn', '2019-01-28 17:06:14.248', '2019-01-28 17:06:14.248', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1421, 1, 22, 0, 'cn', '2019-01-28 17:06:14.249', '2019-01-28 17:06:14.249', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1422, 1, 23, 0, 'cn', '2019-01-28 17:06:14.25', '2019-01-28 17:06:14.25', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1423, 1, 24, 0, 'cn', '2019-01-28 17:06:14.251', '2019-01-28 17:06:14.251', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1424, 1, 25, 0, 'cn', '2019-01-28 17:06:14.252', '2019-01-28 17:06:14.252', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1425, 1, 7, 0, 'cn', '2019-01-28 17:06:14.253', '2019-01-28 17:06:14.253', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1426, 1, 8, 0, 'cn', '2019-01-28 17:06:14.254', '2019-01-28 17:06:14.254', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1427, 1, 9, 0, 'cn', '2019-01-28 17:06:14.255', '2019-01-28 17:06:14.255', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1428, 1, 10, 0, 'cn', '2019-01-28 17:06:14.256', '2019-01-28 17:06:14.256', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1429, 1, 11, 0, 'cn', '2019-01-28 17:06:14.257', '2019-01-28 17:06:14.257', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1430, 1, 37, 0, 'cn', '2019-01-28 17:06:14.258', '2019-01-28 17:06:14.258', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1431, 1, 38, 0, 'cn', '2019-01-28 17:06:14.259', '2019-01-28 17:06:14.259', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1432, 1, 40, 0, 'cn', '2019-01-28 17:06:14.26', '2019-01-28 17:06:14.26', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1433, 1, 41, 0, 'cn', '2019-01-28 17:06:14.26', '2019-01-28 17:06:14.26', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1434, 1, 42, 0, 'cn', '2019-01-28 17:06:14.261', '2019-01-28 17:06:14.261', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1435, 1, 43, 0, 'cn', '2019-01-28 17:06:14.262', '2019-01-28 17:06:14.262', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1436, 1, 44, 0, 'cn', '2019-01-28 17:06:14.263', '2019-01-28 17:06:14.263', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1437, 1, 46, 0, 'cn', '2019-01-28 17:06:14.264', '2019-01-28 17:06:14.264', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1438, 1, 47, 0, 'cn', '2019-01-28 17:06:14.265', '2019-01-28 17:06:14.265', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1439, 1, 48, 0, 'cn', '2019-01-28 17:06:14.266', '2019-01-28 17:06:14.266', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1440, 1, 49, 0, 'cn', '2019-01-28 17:06:14.267', '2019-01-28 17:06:14.267', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1441, 1, 51, 0, 'cn', '2019-01-28 17:06:14.268', '2019-01-28 17:06:14.268', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1442, 1, 52, 0, 'cn', '2019-01-28 17:06:14.269', '2019-01-28 17:06:14.269', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1443, 1, 53, 0, 'cn', '2019-01-28 17:06:14.27', '2019-01-28 17:06:14.27', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1444, 1, 54, 0, 'cn', '2019-01-28 17:06:14.271', '2019-01-28 17:06:14.271', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1445, 1, 56, 0, 'cn', '2019-01-28 17:06:14.272', '2019-01-28 17:06:14.272', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1446, 1, 57, 0, 'cn', '2019-01-28 17:06:14.273', '2019-01-28 17:06:14.273', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1447, 1, 58, 0, 'cn', '2019-01-28 17:06:14.274', '2019-01-28 17:06:14.274', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1448, 1, 59, 0, 'cn', '2019-01-28 17:06:14.275', '2019-01-28 17:06:14.275', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1449, 1, 64, 0, 'cn', '2019-01-28 17:06:14.276', '2019-01-28 17:06:14.276', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1450, 1, 65, 0, 'cn', '2019-01-28 17:06:14.277', '2019-01-28 17:06:14.277', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1451, 1, 66, 0, 'cn', '2019-01-28 17:06:14.278', '2019-01-28 17:06:14.278', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1452, 1, 67, 0, 'cn', '2019-01-28 17:06:14.279', '2019-01-28 17:06:14.279', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1453, 1, 68, 0, 'cn', '2019-01-28 17:06:14.279', '2019-01-28 17:06:14.279', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1454, 1, 60, 0, 'cn', '2019-01-28 17:06:14.281', '2019-01-28 17:06:14.281', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1455, 1, 61, 0, 'cn', '2019-01-28 17:06:14.281', '2019-01-28 17:06:14.281', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1456, 1, 62, 0, 'cn', '2019-01-28 17:06:14.282', '2019-01-28 17:06:14.282', -1, -1);
INSERT INTO idsmed.idsmed_role_permission VALUES (1457, 1, 63, 0, 'cn', '2019-01-28 17:06:14.283', '2019-01-28 17:06:14.283', -1, -1);


--
-- TOC entry 4108 (class 0 OID 0)
-- Dependencies: 228
-- Name: idsmed_role_permission_id_seq; Type: SEQUENCE SET; Schema: idsmed; Owner: dbadmin
--

SELECT pg_catalog.setval('idsmed.idsmed_role_permission_id_seq', 1457, true);


--
-- TOC entry 3845 (class 0 OID 97489)
-- Dependencies: 229
-- Data for Name: idsmed_subscriber_role; Type: TABLE DATA; Schema: idsmed; Owner: dbadmin
--



--
-- TOC entry 4109 (class 0 OID 0)
-- Dependencies: 230
-- Name: idsmed_subscriber_role_id_seq; Type: SEQUENCE SET; Schema: idsmed; Owner: dbadmin
--

SELECT pg_catalog.setval('idsmed.idsmed_subscriber_role_id_seq', 2, false);


--
-- TOC entry 3847 (class 0 OID 97496)
-- Dependencies: 231
-- Data for Name: idsmed_task; Type: TABLE DATA; Schema: idsmed; Owner: dbadmin
--



--
-- TOC entry 3848 (class 0 OID 97507)
-- Dependencies: 232
-- Data for Name: idsmed_task_executor; Type: TABLE DATA; Schema: idsmed; Owner: dbadmin
--



--
-- TOC entry 4110 (class 0 OID 0)
-- Dependencies: 233
-- Name: idsmed_task_executor_id_seq; Type: SEQUENCE SET; Schema: idsmed; Owner: dbadmin
--

SELECT pg_catalog.setval('idsmed.idsmed_task_executor_id_seq', 3780, true);


--
-- TOC entry 4111 (class 0 OID 0)
-- Dependencies: 234
-- Name: idsmed_task_id_seq; Type: SEQUENCE SET; Schema: idsmed; Owner: dbadmin
--

SELECT pg_catalog.setval('idsmed.idsmed_task_id_seq', 306, true);


--
-- TOC entry 3851 (class 0 OID 97519)
-- Dependencies: 235
-- Data for Name: idsmed_user; Type: TABLE DATA; Schema: idsmed; Owner: dbadmin
--

INSERT INTO idsmed.idsmed_user VALUES (1, 'cong', 'dzang', 'chong88sian@live.com', NULL, 3, NULL, '2019-01-07 14:17:38.632', -1, -1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO idsmed.idsmed_user VALUES (106, 'super', 'admin', 'chong88sian@gmail.com', NULL, 3, NULL, NULL, -1, -1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);


--
-- TOC entry 4112 (class 0 OID 0)
-- Dependencies: 236
-- Name: idsmed_user_id_seq; Type: SEQUENCE SET; Schema: idsmed; Owner: dbadmin
--

SELECT pg_catalog.setval('idsmed.idsmed_user_id_seq', 144, true);


--
-- TOC entry 3853 (class 0 OID 97530)
-- Dependencies: 237
-- Data for Name: idsmed_user_role; Type: TABLE DATA; Schema: idsmed; Owner: dbadmin
--

INSERT INTO idsmed.idsmed_user_role VALUES (87, 1, 17, '2019-01-07 14:17:38.628', '2019-01-07 14:17:38.628', -1, -1, 3);
INSERT INTO idsmed.idsmed_user_role VALUES (96, 106, 1, '2019-01-07 17:28:42.417718', '2019-01-07 17:28:42.417718', 1, 1, 3);


--
-- TOC entry 4113 (class 0 OID 0)
-- Dependencies: 238
-- Name: idsmed_user_role_id_seq; Type: SEQUENCE SET; Schema: idsmed; Owner: dbadmin
--

SELECT pg_catalog.setval('idsmed.idsmed_user_role_id_seq', 123, true);


--
-- TOC entry 3855 (class 0 OID 97537)
-- Dependencies: 239
-- Data for Name: keyword_history; Type: TABLE DATA; Schema: idsmed; Owner: dbadmin
--



--
-- TOC entry 4114 (class 0 OID 0)
-- Dependencies: 240
-- Name: keyword_history_id_seq; Type: SEQUENCE SET; Schema: idsmed; Owner: dbadmin
--

SELECT pg_catalog.setval('idsmed.keyword_history_id_seq', 103, true);


--
-- TOC entry 3857 (class 0 OID 97550)
-- Dependencies: 241
-- Data for Name: mail_sending_log; Type: TABLE DATA; Schema: idsmed; Owner: dbadmin
--



--
-- TOC entry 4115 (class 0 OID 0)
-- Dependencies: 242
-- Name: mail_sending_log_id_seq; Type: SEQUENCE SET; Schema: idsmed; Owner: dbadmin
--

SELECT pg_catalog.setval('idsmed.mail_sending_log_id_seq', 1961, true);


--
-- TOC entry 3859 (class 0 OID 97563)
-- Dependencies: 243
-- Data for Name: notification; Type: TABLE DATA; Schema: idsmed; Owner: dbadmin
--

INSERT INTO idsmed.notification VALUES (347, 1, 2, 'IDSMed - Product Approval Notification', '<p>Hi huehue,</p><p>We would like to inform you the status of your product registered under VP Company in IDSMed. You will only receive this email if you have added new product / edit the existing product in the system.</p><p>&nbsp;</p><p><strong>Product Name</strong>: Knife</p><p><strong>Product Model</strong>: </p><p><strong>Product Code</strong>: JSUT1</p><p><strong>Status</strong>: Approved</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>IDSMed Operation Team</p>', 0, '2018-11-22 12:06:44.129', '2018-11-22 12:06:44.15', -1, -1);
INSERT INTO idsmed.notification VALUES (350, 1, 5, 'IDSMed – Request to Review Company Registration', '<p>Hi cong dzang,</p><p>A new vendor testVendor12345 has registered into IDSMed. Kindly login to the system to review the information provided.</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>IDSMed Operation Team</p>', 0, '2018-11-26 14:14:18.365', '2018-11-26 14:14:18.388', -1, -1);
INSERT INTO idsmed.notification VALUES (352, 1, 5, 'IDSMed – Request to Review Company Registration', '<p>Hi cong dzang,</p><p>A new vendor idswedoctor has registered into IDSMed. Kindly login to the system to review the information provided.</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>IDSMed Operation Team</p>', 0, '2018-11-26 16:31:56.061', '2018-11-26 16:31:56.094', -1, -1);
INSERT INTO idsmed.notification VALUES (354, 1, 5, 'IDSMed – Request to Review Company Registration', '<p>Hi cong dzang,</p><p>A new vendor Mummy Chef has registered into IDSMed. Kindly login to the system to review the information provided.</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>IDSMed Operation Team</p>', 0, '2018-11-28 09:34:05.68', '2018-11-28 09:34:05.685', -1, -1);
INSERT INTO idsmed.notification VALUES (360, 1, 2, 'IDSMed - Product Rejected Notification', '<p>Hi mummy01,</p><p>We would like to inform you the status of your product registered under Mummy Chef in IDSMed. You will only receive this email if you have added new product / edit the existing product in the system.</p><p>&nbsp;</p><p>Product Name: Product Name Dental</p><p>Product Model: </p><p>Product Code: DTL003234</p><p>Status: Rejected</p><p><strong>Reason</strong>: Reject Reason</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>IDSMed Operation Team</p>', 0, '2018-11-29 10:02:31.468', '2018-11-29 10:02:31.484', -1, -1);
INSERT INTO idsmed.notification VALUES (363, 1, 2, 'IDSMed - Product Certificate Approval Notification', '<p>Hi mummy01,</p><p>We would like to inform you the status of a Product document uploaded by you / your team in IDSMed.</p><p><br></p><p>Document Name: 00000; </p><p>Status: Reject</p><p><br></p><p>Thanks and Best Regards,</p><p>IDSMed Operation Team</p>', 0, '2018-11-29 10:02:31.496', '2018-11-29 10:02:31.508', -1, -1);
INSERT INTO idsmed.notification VALUES (366, 1, 2, 'IDSMed - Product Approval Notification', '<p>Hi mummy01,</p><p>We would like to inform you the status of your product registered under Mummy Chef in IDSMed. You will only receive this email if you have added new product / edit the existing product in the system.</p><p>&nbsp;</p><p><strong>Product Name</strong>: 90 Degree Angle Huber needle</p><p><strong>Product Model</strong>: </p><p><strong>Product Code</strong>: PRD-ECAT-00000102a</p><p><strong>Status</strong>: Approved</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>IDSMed Operation Team</p>', 0, '2018-12-03 16:28:33.811', '2018-12-03 16:28:33.826', -1, -1);
INSERT INTO idsmed.notification VALUES (369, 1, 2, 'IDSMed - Product Rejected Notification', '<p>Hi mummy01,</p><p>We would like to inform you the status of your product registered under Mummy Chef in IDSMed. You will only receive this email if you have added new product / edit the existing product in the system.</p><p>&nbsp;</p><p>Product Name: ARROW CVC Set: 14 GA X 8 (20 cm)</p><p>Product Model: </p><p>Product Code: PRD-ECAT-00000186</p><p>Status: Rejected</p><p><strong>Reason</strong>: Incomplete Data</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>IDSMed Operation Team</p>', 0, '2018-12-03 16:59:29.341', '2018-12-03 16:59:29.354', -1, -1);
INSERT INTO idsmed.notification VALUES (372, 1, 5, 'IDSMed – Request to Review Company Registration', '<p>Hi cong dzang,</p><p>A new vendor Medic System Ltd has registered into IDSMed. Kindly login to the system to review the information provided.</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>IDSMed Operation Team</p>', 0, '2018-12-07 15:49:32.856', '2018-12-07 15:49:32.965', -1, -1);
INSERT INTO idsmed.notification VALUES (374, 1, 3, 'IDSMed – Request to Approve Company Registration', '<p>Hi Test User 1,</p><p>A new vendor Medic System Ltd has registered into IDSMed and has been reviewed by the team. Kindly login to the system to review and approve the information provided.</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>IDSMed Operation Team</p>', 0, '2018-12-07 15:50:18.605', '2018-12-07 15:50:18.63', -1, -1);
INSERT INTO idsmed.notification VALUES (387, 1, 2, 'IDSMed - Product Approval Notification', '<p>Hi mummy01,</p><p>We would like to inform you the status of your product registered under Mummy Chef in IDSMed. You will only receive this email if you have added new product / edit the existing product in the system.</p><p>&nbsp;</p><p><strong>Product Name</strong>: 90 Degree Angle Huber needle</p><p><strong>Product Model</strong>: </p><p><strong>Product Code</strong>: PRD-ECAT-00000102a</p><p><strong>Status</strong>: Approved</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>IDSMed Operation Team</p>', 0, '2018-12-10 12:48:38.925', '2018-12-10 12:48:38.937', -1, -1);
INSERT INTO idsmed.notification VALUES (397, 1, 2, 'IDSMed - Product Approval Notification', '<p>Hi chiggies,</p><p>We would like to inform you the status of your product registered under Mummy Chef in IDSMed. You will only receive this email if you have added new product / edit the existing product in the system.</p><p>&nbsp;</p><p><strong>Product Name</strong>: ARROW CVC Set: 14 GA X 8 (24 cm)</p><p><strong>Product Model</strong>: </p><p><strong>Product Code</strong>: PRD-ECAT-00000102c</p><p><strong>Status</strong>: Approved</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>IDSMed Operation Team</p>', 0, '2018-12-12 10:19:23.469', '2018-12-12 10:19:23.482', -1, -1);
INSERT INTO idsmed.notification VALUES (399, 1, 3, 'IDSMed - Product Rejected Notification', '<p>Hi idsmed,</p><p>We would like to inform you the status of your product registered under IDSMed in IDSMed. You will only receive this email if you have added new product / edit the existing product in the system.</p><p>&nbsp;</p><p>Product Name: TEST 1234</p><p>Product Model: </p><p>Product Code: OJKLM111</p><p>Status: Rejected</p><p><strong>Reason</strong>: terrible</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>IDSMed Operation Team</p>', 0, '2018-12-12 14:39:03.59', '2018-12-12 14:39:03.599', -1, -1);
INSERT INTO idsmed.notification VALUES (401, 1, 10, 'IDSMed - Product Certificate Expiry Reminder', '<p>Hi IDSMed,</p><p>One of your product''s document certificate are expiring soon.Below is the information of the document certificate:</p><p><br></p><p><strong>Product Name</strong>: testing23456</p><p><strong>Product Code</strong>: testing</p><p><strong>Document Name</strong>: 3</p><p><strong>Expiry Date</strong>: 2018-12-13 08:00:00.0</p><p><br></p><p>Kindly upload the latest certification/document soonest possible.</p><p><br></p><p>Thanks and Best Regards,</p><p>IDSMed Operation Team</p>', 0, '2018-12-13 01:00:00.14', '2018-12-13 01:00:00.146', -1, -1);
INSERT INTO idsmed.notification VALUES (403, 1, 10, 'IDSMed - Product Certificate Expiry Reminder', '<p>Hi IDSMed,</p><p>One of your product''s document certificate are expiring soon.Below is the information of the document certificate:</p><p><br></p><p><strong>Product Name</strong>: testing23456</p><p><strong>Product Code</strong>: testing</p><p><strong>Document Name</strong>: 2</p><p><strong>Expiry Date</strong>: 2018-12-13 08:00:00.0</p><p><br></p><p>Kindly upload the latest certification/document soonest possible.</p><p><br></p><p>Thanks and Best Regards,</p><p>IDSMed Operation Team</p>', 0, '2018-12-13 01:00:00.169', '2018-12-13 01:00:00.172', -1, -1);
INSERT INTO idsmed.notification VALUES (407, 1, 2, 'IDSMed - Product Approval Notification', '<p>Hi chiggies,</p><p>We would like to inform you the status of your product registered under Mummy Chef in IDSMed. You will only receive this email if you have added new product / edit the existing product in the system.</p><p>&nbsp;</p><p><strong>Product Name</strong>: ARROW CVC Set: 14 GA X 8 (43cm)</p><p><strong>Product Model</strong>: </p><p><strong>Product Code</strong>: DTL003234121FF</p><p><strong>Status</strong>: Approved</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>IDSMed Operation Team</p>', 0, '2018-12-13 11:18:08.612', '2018-12-13 11:18:08.621', -1, -1);
INSERT INTO idsmed.notification VALUES (409, 1, 2, 'IDSMed - Product Approval Notification', '<p>Hi chiggies,</p><p>We would like to inform you the status of your product registered under Mummy Chef in IDSMed. You will only receive this email if you have added new product / edit the existing product in the system.</p><p>&nbsp;</p><p><strong>Product Name</strong>: ARROW CVC Set: 14 GA X 8 (42 cm)</p><p><strong>Product Model</strong>: </p><p><strong>Product Code</strong>: PRD-ECAT-0000010211</p><p><strong>Status</strong>: Approved</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>IDSMed Operation Team</p>', 0, '2018-12-13 11:18:24.46', '2018-12-13 11:18:24.468', -1, -1);
INSERT INTO idsmed.notification VALUES (411, 1, 5, 'IDSMed – Request to Review Company Registration', '<p>Hi cong dzang,</p><p>A new vendor Angelic Garden has registered into IDSMed. Kindly login to the system to review the information provided.</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>IDSMed Operation Team</p>', 0, '2018-12-13 11:36:45.724', '2018-12-13 11:36:45.738', -1, -1);
INSERT INTO idsmed.notification VALUES (413, 1, 6, 'IDSMed – Request to Approve Company Registration', '<p>Hi Test User 1,</p><p>A new vendor Angelic Garden has registered into IDSMed and has been reviewed by the team. Kindly login to the system to review and approve the information provided.</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>IDSMed Operation Team</p>', 0, '2018-12-13 11:37:32.712', '2018-12-13 11:37:32.738', -1, -1);
INSERT INTO idsmed.notification VALUES (3350, 1, 6, 'IDSMed – Request to Approve Company Registration', '<p>Hi Alice  Synthesis Thirty,</p><p>A new vendor Charles Sturt has registered with us and the information was duly reviewed  by the team.  Kindly login to the system to review/approve the application.</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-29 14:26:11.315', '2019-01-29 14:26:11.345', -1, -1);
INSERT INTO idsmed.notification VALUES (428, 1, 10, 'IDSMed - Product Certificate Expiry Reminder', '<p>Hi IDSMed,</p><p>One of your product''s document certificate are expiring soon.Below is the information of the document certificate:</p><p><br></p><p><strong>Product Name</strong>: testing23456</p><p><strong>Product Code</strong>: testing</p><p><strong>Document Name</strong>: 2</p><p><strong>Expiry Date</strong>: 2018-12-13 08:00:00.0</p><p><br></p><p>Kindly upload the latest certification/document soonest possible.</p><p><br></p><p>Thanks and Best Regards,</p><p>IDSMed Operation Team</p>', 0, '2018-12-13 13:00:00.111', '2018-12-13 13:00:00.113', -1, -1);
INSERT INTO idsmed.notification VALUES (430, 1, 10, 'IDSMed - Product Certificate Expiry Reminder', '<p>Hi IDSMed,</p><p>One of your product''s document certificate are expiring soon.Below is the information of the document certificate:</p><p><br></p><p><strong>Product Name</strong>: testing23456</p><p><strong>Product Code</strong>: testing</p><p><strong>Document Name</strong>: 3</p><p><strong>Expiry Date</strong>: 2018-12-13 08:00:00.0</p><p><br></p><p>Kindly upload the latest certification/document soonest possible.</p><p><br></p><p>Thanks and Best Regards,</p><p>IDSMed Operation Team</p>', 0, '2018-12-13 13:00:00.132', '2018-12-13 13:00:00.134', -1, -1);
INSERT INTO idsmed.notification VALUES (432, 1, 10, 'IDSMed - Product Certificate Expiry Reminder', '<p>Hi Mummy Chef,</p><p>One of your product''s document certificate are expiring soon.Below is the information of the document certificate:</p><p><br></p><p><strong>Product Name</strong>: ARROW CVC Set: 14 GA X 8 (41cm)</p><p><strong>Product Code</strong>: DTL003234121F</p><p><strong>Document Name</strong>: 00000</p><p><strong>Expiry Date</strong>: 2018-12-13 00:00:00.0</p><p><br></p><p>Kindly upload the latest certification/document soonest possible.</p><p><br></p><p>Thanks and Best Regards,</p><p>IDSMed Operation Team</p>', 0, '2018-12-13 13:00:00.151', '2018-12-13 13:00:00.153', -1, -1);
INSERT INTO idsmed.notification VALUES (434, 1, 10, 'IDSMed - Product Certificate Expiry Reminder', '<p>Hi Mummy Chef,</p><p>One of your product''s document certificate are expiring soon.Below is the information of the document certificate:</p><p><br></p><p><strong>Product Name</strong>: ARROW CVC Set: 14 GA X 8 (41cm)</p><p><strong>Product Code</strong>: DTL003234121F</p><p><strong>Document Name</strong>: 00000</p><p><strong>Expiry Date</strong>: 2018-12-13 00:00:00.0</p><p><br></p><p>Kindly upload the latest certification/document soonest possible.</p><p><br></p><p>Thanks and Best Regards,</p><p>IDSMed Operation Team</p>', 0, '2018-12-13 13:00:00.17', '2018-12-13 13:00:00.172', -1, -1);
INSERT INTO idsmed.notification VALUES (436, 1, 10, 'IDSMed - Product Certificate Expiry Reminder', '<p>Hi Mummy Chef,</p><p>One of your product''s document certificate are expiring soon.Below is the information of the document certificate:</p><p><br></p><p><strong>Product Name</strong>: ARROW CVC Set: 14 GA X 8 (41cm)</p><p><strong>Product Code</strong>: DTL003234121F</p><p><strong>Document Name</strong>: 00000</p><p><strong>Expiry Date</strong>: 2018-12-13 00:00:00.0</p><p><br></p><p>Kindly upload the latest certification/document soonest possible.</p><p><br></p><p>Thanks and Best Regards,</p><p>IDSMed Operation Team</p>', 0, '2018-12-13 13:00:00.19', '2018-12-13 13:00:00.192', -1, -1);
INSERT INTO idsmed.notification VALUES (438, 1, 10, 'IDSMed - Product Certificate Expiry Reminder', '<p>Hi Mummy Chef,</p><p>One of your product''s document certificate are expiring soon.Below is the information of the document certificate:</p><p><br></p><p><strong>Product Name</strong>: ARROW CVC Set: 14 GA X 8 (41cm)</p><p><strong>Product Code</strong>: DTL003234121F</p><p><strong>Document Name</strong>: 00000</p><p><strong>Expiry Date</strong>: 2018-12-13 00:00:00.0</p><p><br></p><p>Kindly upload the latest certification/document soonest possible.</p><p><br></p><p>Thanks and Best Regards,</p><p>IDSMed Operation Team</p>', 0, '2018-12-13 13:00:00.209', '2018-12-13 13:00:00.211', -1, -1);
INSERT INTO idsmed.notification VALUES (3168, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi ML Lee,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: ML Lee</p><p>Company (For Vendor User): abcMED</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-29 10:36:30.958', '2019-01-29 10:36:31.033', -1, -1);
INSERT INTO idsmed.notification VALUES (2770, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Joey Ling,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Joey Ling</p><p>Company (For Vendor User): -</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-24 16:40:41.676', '2019-01-24 16:40:41.741', -1, -1);
INSERT INTO idsmed.notification VALUES (3170, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi ML Lee,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: ML Lee</p><p>Company (For Vendor User): abcMED</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-29 10:36:30.968', '2019-01-29 10:36:31.033', -1, -1);
INSERT INTO idsmed.notification VALUES (725, 1, 5, 'IDSMed – Request to Review Company Registration', '<p>Hi cong dzang,</p><p>A new vendor Forus Health 关注健康 has registered into IDSMed. Kindly login to the system to review the information provided.</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>IDSMed Operation Team</p>', 0, '2018-12-24 13:55:05.148', '2018-12-24 13:55:05.151', -1, -1);
INSERT INTO idsmed.notification VALUES (440, 1, 10, 'IDSMed - Product Certificate Expiry Reminder', '<p>Hi Mummy Chef,</p><p>One of your product''s document certificate are expiring soon.Below is the information of the document certificate:</p><p><br></p><p><strong>Product Name</strong>: ARROW CVC Set: 14 GA X 8 (41cm)</p><p><strong>Product Code</strong>: DTL003234121F</p><p><strong>Document Name</strong>: 00000</p><p><strong>Expiry Date</strong>: 2018-12-13 00:00:00.0</p><p><br></p><p>Kindly upload the latest certification/document soonest possible.</p><p><br></p><p>Thanks and Best Regards,</p><p>IDSMed Operation Team</p>', 0, '2018-12-13 13:00:00.23', '2018-12-13 13:00:00.232', -1, -1);
INSERT INTO idsmed.notification VALUES (442, 1, 10, 'IDSMed - Product Certificate Expiry Reminder', '<p>Hi Mummy Chef,</p><p>One of your product''s document certificate are expiring soon.Below is the information of the document certificate:</p><p><br></p><p><strong>Product Name</strong>: ARROW CVC Set: 14 GA X 8 (41cm)</p><p><strong>Product Code</strong>: DTL003234121F</p><p><strong>Document Name</strong>: 00000</p><p><strong>Expiry Date</strong>: 2018-12-13 00:00:00.0</p><p><br></p><p>Kindly upload the latest certification/document soonest possible.</p><p><br></p><p>Thanks and Best Regards,</p><p>IDSMed Operation Team</p>', 0, '2018-12-13 13:00:00.251', '2018-12-13 13:00:00.253', -1, -1);
INSERT INTO idsmed.notification VALUES (444, 1, 10, 'IDSMed - Product Certificate Expiry Reminder', '<p>Hi Mummy Chef,</p><p>One of your product''s document certificate are expiring soon.Below is the information of the document certificate:</p><p><br></p><p><strong>Product Name</strong>: ARROW CVC Set: 14 GA X 8 (41cm)</p><p><strong>Product Code</strong>: DTL003234121F</p><p><strong>Document Name</strong>: 00000</p><p><strong>Expiry Date</strong>: 2018-12-13 00:00:00.0</p><p><br></p><p>Kindly upload the latest certification/document soonest possible.</p><p><br></p><p>Thanks and Best Regards,</p><p>IDSMed Operation Team</p>', 0, '2018-12-13 13:00:00.27', '2018-12-13 13:00:00.272', -1, -1);
INSERT INTO idsmed.notification VALUES (446, 1, 10, 'IDSMed - Product Certificate Expiry Reminder', '<p>Hi Mummy Chef,</p><p>One of your product''s document certificate are expiring soon.Below is the information of the document certificate:</p><p><br></p><p><strong>Product Name</strong>: ARROW CVC Set: 14 GA X 8 (41cm)</p><p><strong>Product Code</strong>: DTL003234121F</p><p><strong>Document Name</strong>: 00000</p><p><strong>Expiry Date</strong>: 2018-12-13 00:00:00.0</p><p><br></p><p>Kindly upload the latest certification/document soonest possible.</p><p><br></p><p>Thanks and Best Regards,</p><p>IDSMed Operation Team</p>', 0, '2018-12-13 13:00:00.289', '2018-12-13 13:00:00.296', -1, -1);
INSERT INTO idsmed.notification VALUES (448, 1, 10, 'IDSMed - Product Certificate Expiry Reminder', '<p>Hi Mummy Chef,</p><p>One of your product''s document certificate are expiring soon.Below is the information of the document certificate:</p><p><br></p><p><strong>Product Name</strong>: ARROW CVC Set: 14 GA X 8 (41cm)</p><p><strong>Product Code</strong>: DTL003234121F</p><p><strong>Document Name</strong>: 00000</p><p><strong>Expiry Date</strong>: 2018-12-13 00:00:00.0</p><p><br></p><p>Kindly upload the latest certification/document soonest possible.</p><p><br></p><p>Thanks and Best Regards,</p><p>IDSMed Operation Team</p>', 0, '2018-12-13 13:00:00.312', '2018-12-13 13:00:00.314', -1, -1);
INSERT INTO idsmed.notification VALUES (450, 1, 10, 'IDSMed - Product Certificate Expiry Reminder', '<p>Hi Mummy Chef,</p><p>One of your product''s document certificate are expiring soon.Below is the information of the document certificate:</p><p><br></p><p><strong>Product Name</strong>: ARROW CVC Set: 14 GA X 8 (41cm)</p><p><strong>Product Code</strong>: DTL003234121F</p><p><strong>Document Name</strong>: 00000</p><p><strong>Expiry Date</strong>: 2018-12-13 00:00:00.0</p><p><br></p><p>Kindly upload the latest certification/document soonest possible.</p><p><br></p><p>Thanks and Best Regards,</p><p>IDSMed Operation Team</p>', 0, '2018-12-13 13:00:00.33', '2018-12-13 13:00:00.332', -1, -1);
INSERT INTO idsmed.notification VALUES (2772, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Joey Ling,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Joey Ling</p><p>Company (For Vendor User): -</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-24 16:40:41.681', '2019-01-24 16:40:41.741', -1, -1);
INSERT INTO idsmed.notification VALUES (454, 1, 8, 'IDSMed - User Registration Approval Notification', '<p>Hi Yuuki Asuna,</p><p>We would like to inform you the status of your account registration in IDSMed.</p><p>&nbsp;</p><p>Status: Rejected</p><p>Reason: R</p><p><br></p><p>Please login to the system and amend the necessarily information @ My Profile before you resubmit the information for approval.</p><p>&nbsp;</p><p><br></p><p>Thanks and Best Regards,</p><p>IDSMed Operation Team</p>', 0, '2018-12-14 09:26:52.979', '2018-12-14 09:26:52.985', -1, -1);
INSERT INTO idsmed.notification VALUES (456, 1, 1, 'IDSMed - User Registration Approval Notification', '<p>Hi Shino Asada,</p><p>We would like to inform you the status of your account registration in IDSMed.</p><p>&nbsp;</p><p>Status: Approved</p><p><br></p><p>You may login to the system now to perform your daily operation.</p><p>&nbsp;</p><p><br></p><p>Thanks and Best Regards,</p><p>IDSMed Operation Team</p>', 0, '2018-12-14 09:29:08.019', '2018-12-14 09:29:08.048', -1, -1);
INSERT INTO idsmed.notification VALUES (458, 1, 1, 'IDSMed - User Registration Approval Notification', '<p>Hi Shino Asada,</p><p>We would like to inform you the status of your account registration in IDSMed.</p><p>&nbsp;</p><p>Status: [#USER_APP_STATUS]</p><p><br></p><p>You may login to the system now to perform your daily operation.</p><p>&nbsp;</p><p><br></p><p>Thanks and Best Regards,</p><p>IDSMed Operation Team</p>', 0, '2018-12-14 09:29:08.076', '2018-12-14 09:29:08.097', -1, -1);
INSERT INTO idsmed.notification VALUES (476, 1, 5, 'IDSMed – Request to Review Company Registration', '<p>Hi cong dzang,</p><p>A new vendor testing conpany 20 has registered into IDSMed. Kindly login to the system to review the information provided.</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>IDSMed Operation Team</p>', 0, '2018-12-14 17:17:29.523', '2018-12-14 17:17:29.532', -1, -1);
INSERT INTO idsmed.notification VALUES (478, 1, 6, 'IDSMed – Request to Approve Company Registration', '<p>Hi Shino Asada,</p><p>A new vendor testing conpany 20 has registered into IDSMed and has been reviewed by the team. Kindly login to the system to review and approve the information provided.</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>IDSMed Operation Team</p>', 0, '2018-12-14 17:18:22.99', '2018-12-14 17:18:23.015', -1, -1);
INSERT INTO idsmed.notification VALUES (614, 1, 5, 'IDSMed – Request to Review Company Registration', '<p>Hi cong dzang,</p><p>A new vendor testing conpany 1001 has registered into IDSMed. Kindly login to the system to review the information provided.</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>IDSMed Operation Team</p>', 0, '2018-12-20 09:02:24.773', '2018-12-20 09:02:24.79', -1, -1);
INSERT INTO idsmed.notification VALUES (509, 1, 5, 'IDSMed – Request to Review Company Registration', '<p>Hi cong dzang,</p><p>A new vendor testing conpany 1000 has registered into IDSMed. Kindly login to the system to review the information provided.</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>IDSMed Operation Team</p>', 0, '2018-12-18 12:25:42.726', '2018-12-18 12:25:42.738', -1, -1);
INSERT INTO idsmed.notification VALUES (511, 1, 6, 'IDSMed – Request to Approve Company Registration', '<p>Hi Shino Asada,</p><p>A new vendor testing conpany 1000 has registered into IDSMed and has been reviewed by the team. Kindly login to the system to review and approve the information provided.</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>IDSMed Operation Team</p>', 0, '2018-12-18 12:26:05.125', '2018-12-18 12:26:05.152', -1, -1);
INSERT INTO idsmed.notification VALUES (1128, 1, 5, 'IDSMed – Request to Review Company Registration', '<p>Hi cong dzang,</p><p>A new vendor Company Today has registered with us. Kindly login to the system to review the information provided.</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2018-12-28 18:58:49.638', '2018-12-28 18:58:49.646', -1, -1);
INSERT INTO idsmed.notification VALUES (577, 1, 2, 'IDSMed - Product Approval Notification', '<p>Hi nagisa,</p><p>We would like to inform you the status of your product registered under Angelic Garden in IDSMed. You will only receive this email if you have added new product / edit the existing product in the system.</p><p>&nbsp;</p><p><strong>Product Name</strong>: Thermal Paper</p><p><strong>Product Model</strong>: 2036970-002</p><p><strong>Product Code</strong>:  MDC 729 42181716 008</p><p><strong>Status</strong>: Approved</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>IDSMed Operation Team</p>', 0, '2018-12-19 11:32:41.644', '2018-12-19 11:32:41.663', -1, -1);
INSERT INTO idsmed.notification VALUES (580, 1, 6, 'IDSMed – Request to Approve Company Registration', '<p>Hi Shino Asada,</p><p>A new vendor testing company 10 has registered into IDSMed and has been reviewed by the team. Kindly login to the system to review and approve the information provided.</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>IDSMed Operation Team</p>', 0, '2018-12-19 17:15:37.694', '2018-12-19 17:15:37.728', -1, -1);
INSERT INTO idsmed.notification VALUES (2774, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Joey Ling,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Joey Ling</p><p>Company (For Vendor User): -</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-24 16:40:41.686', '2019-01-24 16:40:41.741', -1, -1);
INSERT INTO idsmed.notification VALUES (616, 1, 5, 'IDSMed – Request to Review Company Registration', '<p>Hi cong dzang,</p><p>A new vendor October Cherry has registered into IDSMed. Kindly login to the system to review the information provided.</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>IDSMed Operation Team</p>', 0, '2018-12-20 10:32:35.854', '2018-12-20 10:32:35.858', -1, -1);
INSERT INTO idsmed.notification VALUES (625, 1, 2, 'IDSMed - Product Approval Notification', '<p>Hi zhao,</p><p>We would like to inform you the status of your product registered under October Cherry in IDSMed. You will only receive this email if you have added new product / edit the existing product in the system.</p><p>&nbsp;</p><p><strong>Product Name</strong>: Nexum Thermal Paper</p><p><strong>Product Model</strong>: </p><p><strong>Product Code</strong>: Paper0199413</p><p><strong>Status</strong>: Approved</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>IDSMed Operation Team</p>', 0, '2018-12-20 10:55:06.63', '2018-12-20 10:55:06.673', -1, -1);
INSERT INTO idsmed.notification VALUES (627, 1, 2, 'IDSMed - Product Certificate Approval Notification', '<p>Hi zhao,</p><p>We would like to inform you the status of a Product document uploaded by you / your team in IDSMed.</p><p><br></p><p>Document Name: 00000; </p><p>Status: Approved</p><p><br></p><p>Thanks and Best Regards,</p><p>IDSMed Operation Team</p>', 0, '2018-12-20 10:55:06.655', '2018-12-20 10:55:06.673', -1, -1);
INSERT INTO idsmed.notification VALUES (631, 1, 1, 'IDSMed - User Registration Approval Notification', '<p>Hi Huang Rong,</p><p>We would like to inform you the status of your account registration in IDSMed.</p><p>&nbsp;</p><p>Status: Approved</p><p><br></p><p>You may login to the system now to perform your daily operation.</p><p>&nbsp;</p><p><br></p><p>Thanks and Best Regards,</p><p>IDSMed Operation Team</p>', 0, '2018-12-20 15:47:43.05', '2018-12-20 15:47:43.089', -1, -1);
INSERT INTO idsmed.notification VALUES (633, 1, 1, 'IDSMed - New User Registration Notification', '<p>Hi Huang Rong,</p><p>We would like to inform you that a new user account registration in IDSMed has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Huang Rong</p><p>Company (For Vendor User): </p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>IDSMed Operation Team</p>', 0, '2018-12-20 15:47:43.14', '2018-12-20 15:47:43.163', -1, -1);
INSERT INTO idsmed.notification VALUES (652, 1, 5, 'IDSMed – Request to Review Company Registration', '<p>Hi cong dzang,</p><p>A new vendor Weixin Healthcare has registered into IDSMed. Kindly login to the system to review the information provided.</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>IDSMed Operation Team</p>', 0, '2018-12-20 15:59:34.275', '2018-12-20 15:59:34.3', -1, -1);
INSERT INTO idsmed.notification VALUES (661, 1, 5, 'IDSMed – Request to Review Company Registration', '<p>Hi cong dzang,</p><p>A new vendor Gonna Reject This has registered into IDSMed. Kindly login to the system to review the information provided.</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>IDSMed Operation Team</p>', 0, '2018-12-21 15:33:58.536', '2018-12-21 15:33:58.541', -1, -1);
INSERT INTO idsmed.notification VALUES (688, 1, 5, 'IDSMed – Request to Review Company Registration', '<p>Hi cong dzang,</p><p>A new vendor vendor test 2343 has registered into IDSMed. Kindly login to the system to review the information provided.</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>IDSMed Operation Team</p>', 0, '2018-12-21 18:22:39.335', '2018-12-21 18:22:39.394', -1, -1);
INSERT INTO idsmed.notification VALUES (2776, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Joey Ling,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Joey Ling</p><p>Company (For Vendor User): -</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-24 16:40:41.692', '2019-01-24 16:40:41.741', -1, -1);
INSERT INTO idsmed.notification VALUES (690, 1, 5, 'IDSMed – Request to Review Company Registration', '<p>Hi cong dzang,</p><p>A new vendor vendor test 87fdsafdsg has registered into IDSMed. Kindly login to the system to review the information provided.</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>IDSMed Operation Team</p>', 0, '2018-12-21 18:26:33.251', '2018-12-21 18:26:33.26', -1, -1);
INSERT INTO idsmed.notification VALUES (713, 1, 5, 'IDSMed – Request to Review Company Registration', '<p>Hi cong dzang,</p><p>A new vendor Forus Health 关注健康 has registered into IDSMed. Kindly login to the system to review the information provided.</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>IDSMed Operation Team</p>', 0, '2018-12-24 11:01:43.316', '2018-12-24 11:01:43.347', -1, -1);
INSERT INTO idsmed.notification VALUES (715, 1, 5, 'IDSMed – Request to Review Company Registration', '<p>Hi cong dzang,</p><p>A new vendor Forus Health 关注健康 has registered into IDSMed. Kindly login to the system to review the information provided.</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>IDSMed Operation Team</p>', 0, '2018-12-24 12:35:34.094', '2018-12-24 12:35:34.117', -1, -1);
INSERT INTO idsmed.notification VALUES (717, 1, 5, 'IDSMed – Request to Review Company Registration', '<p>Hi cong dzang,</p><p>A new vendor Forus Health 关注健康 has registered into IDSMed. Kindly login to the system to review the information provided.</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>IDSMed Operation Team</p>', 0, '2018-12-24 13:01:32.809', '2018-12-24 13:01:32.859', -1, -1);
INSERT INTO idsmed.notification VALUES (719, 1, 5, 'IDSMed – Request to Review Company Registration', '<p>Hi cong dzang,</p><p>A new vendor Forus Health 关注健康 has registered into IDSMed. Kindly login to the system to review the information provided.</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>IDSMed Operation Team</p>', 0, '2018-12-24 13:10:51.827', '2018-12-24 13:10:51.866', -1, -1);
INSERT INTO idsmed.notification VALUES (721, 1, 5, 'IDSMed – Request to Review Company Registration', '<p>Hi cong dzang,</p><p>A new vendor Forus Health 关注健康 has registered into IDSMed. Kindly login to the system to review the information provided.</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>IDSMed Operation Team</p>', 0, '2018-12-24 13:25:16.282', '2018-12-24 13:25:16.286', -1, -1);
INSERT INTO idsmed.notification VALUES (723, 1, 5, 'IDSMed – Request to Review Company Registration', '<p>Hi cong dzang,</p><p>A new vendor Forus Health 关注健康 has registered into IDSMed. Kindly login to the system to review the information provided.</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>IDSMed Operation Team</p>', 0, '2018-12-24 13:38:15.246', '2018-12-24 13:38:15.25', -1, -1);
INSERT INTO idsmed.notification VALUES (727, 1, 6, 'IDSMed – Request to Approve Company Registration', '<p>Hi Shino Asada,</p><p>A new vendor Forus Health 关注健康 has registered into IDSMed and has been reviewed by the team. Kindly login to the system to review and approve the information provided.</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>IDSMed Operation Team</p>', 0, '2018-12-24 13:55:44.204', '2018-12-24 13:55:44.221', -1, -1);
INSERT INTO idsmed.notification VALUES (734, 1, 2, 'IDSMed - Product Approval Notification', '<p>Hi chiggies,</p><p>We would like to inform you the status of your product registered under Mummy Chef in IDSMed. You will only receive this email if you have added new product / edit the existing product in the system.</p><p>&nbsp;</p><p><strong>Product Name</strong>: Dexa Thermal Paper</p><p><strong>Product Model</strong>: </p><p><strong>Product Code</strong>: MTP392042</p><p><strong>Status</strong>: Approved</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>IDSMed Operation Team</p>', 0, '2018-12-24 15:03:35.464', '2018-12-24 15:03:35.473', -1, -1);
INSERT INTO idsmed.notification VALUES (758, 1, 2, 'IDSMed - Product Approval Notification', '<p>Hi cph1,</p><p>We would like to inform you the status of your product registered under Forus Health 关注健康 in IDSMed. You will only receive this email if you have added new product / edit the existing product in the system.</p><p>&nbsp;</p><p><strong>Product Name</strong>: 手提長凳顶部眼底相機</p><p><strong>Product Model</strong>: 3nethra classic</p><p><strong>Product Code</strong>: CE0843</p><p><strong>Status</strong>: Approved</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>IDSMed Operation Team</p>', 0, '2018-12-24 17:38:28.422', '2018-12-24 17:38:28.456', -1, -1);
INSERT INTO idsmed.notification VALUES (761, 1, 2, 'IDSMed - Product Certificate Approval Notification', '<p>Hi cph1,</p><p>We would like to inform you the status of a Product document uploaded by you / your team in IDSMed.</p><p><br></p><p>Document Name: o1; </p><p>Status: Approved</p><p><br></p><p>Thanks and Best Regards,</p><p>IDSMed Operation Team</p>', 0, '2018-12-24 17:38:28.448', '2018-12-24 17:38:28.456', -1, -1);
INSERT INTO idsmed.notification VALUES (767, 1, 5, 'IDSMed – Request to Review Company Registration', '<p>Hi cong dzang,</p><p>A new vendor 亿万物联网 EVVO IOT PTE. LTD. has registered into IDSMed. Kindly login to the system to review the information provided.</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>IDSMed Operation Team</p>', 0, '2018-12-25 11:18:59.314', '2018-12-25 11:18:59.317', -1, -1);
INSERT INTO idsmed.notification VALUES (2433, 105, 5, 'IDSMed – Request to Review Company Registration', '<p>Hi super admin,</p><p>A new vendor 克里斯提娜医疗器械有限公司 has registered with us. Kindly login to the system to review the information provided.</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-15 10:44:15.401', '2019-01-15 10:44:15.454', -1, -1);
INSERT INTO idsmed.notification VALUES (769, 1, 1, 'IDSMed - User Registration Approval Notification', '<p>Hi 培华 丛,</p><p>We would like to inform you the status of your account registration in IDSMed.</p><p>&nbsp;</p><p>Status: Approved</p><p><br></p><p>You may login to the system now to perform your daily operation.</p><p>&nbsp;</p><p><br></p><p>Thanks and Best Regards,</p><p>IDSMed Operation Team</p>', 0, '2018-12-25 11:33:50.639', '2018-12-25 11:33:50.647', -1, -1);
INSERT INTO idsmed.notification VALUES (771, 1, 1, 'IDSMed - New User Registration Notification', '<p>Hi 培华 丛,</p><p>We would like to inform you that a new user account registration in IDSMed has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: 培华 丛</p><p>Company (For Vendor User): </p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>IDSMed Operation Team</p>', 0, '2018-12-25 11:33:50.68', '2018-12-25 11:33:50.699', -1, -1);
INSERT INTO idsmed.notification VALUES (791, 1, 6, 'IDSMed – Request to Approve Company Registration', '<p>Hi Shino Asada,</p><p>A new vendor 亿万物联网 EVVO IOT PTE. LTD. has registered into IDSMed and has been reviewed by the team. Kindly login to the system to review and approve the information provided.</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>IDSMed Operation Team</p>', 0, '2018-12-25 12:27:29.199', '2018-12-25 12:27:29.219', -1, -1);
INSERT INTO idsmed.notification VALUES (798, 1, 2, 'IDSMed - Product Approval Notification', '<p>Hi cph2,</p><p>We would like to inform you the status of your product registered under 亿万物联网 EVVO IOT PTE. LTD. in IDSMed. You will only receive this email if you have added new product / edit the existing product in the system.</p><p>&nbsp;</p><p><strong>Product Name</strong>: 亿万物联网</p><p><strong>Product Model</strong>: EVVO IOT</p><p><strong>Product Code</strong>: CE0844</p><p><strong>Status</strong>: Approved</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>IDSMed Operation Team</p>', 0, '2018-12-25 13:31:58.757', '2018-12-25 13:31:58.784', -1, -1);
INSERT INTO idsmed.notification VALUES (800, 1, 2, 'IDSMed - Product Certificate Approval Notification', '<p>Hi cph2,</p><p>We would like to inform you the status of a Product document uploaded by you / your team in IDSMed.</p><p><br></p><p>Document Name: 00000; 2; </p><p>Status: Approved</p><p><br></p><p>Thanks and Best Regards,</p><p>IDSMed Operation Team</p>', 0, '2018-12-25 13:31:58.772', '2018-12-25 13:31:58.784', -1, -1);
INSERT INTO idsmed.notification VALUES (829, 1, 1, 'IDSMed - User Registration Approval Notification', '<p>Hi Jay Lo,</p><p>We would like to inform you the status of your account registration in IDSMed.</p><p>&nbsp;</p><p>Status: Approved</p><p><br></p><p>You may login to the system now to perform your daily operation.</p><p>&nbsp;</p><p><br></p><p>Thanks and Best Regards,</p><p>IDSMed Operation Team</p>', 0, '2018-12-26 10:04:35.968', '2018-12-26 10:04:35.974', -1, -1);
INSERT INTO idsmed.notification VALUES (831, 1, 1, 'IDSMed - New User Registration Notification', '<p>Hi Jay Lo,</p><p>We would like to inform you that a new user account registration in IDSMed has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Jay Lo</p><p>Company (For Vendor User): </p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>IDSMed Operation Team</p>', 0, '2018-12-26 10:04:36.004', '2018-12-26 10:04:36.023', -1, -1);
INSERT INTO idsmed.notification VALUES (852, 1, 5, 'IDSMed – Request to Review Company Registration', '<p>Hi cong dzang,</p><p>A new vendor Reject this vendor has registered into IDSMed. Kindly login to the system to review the information provided.</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>IDSMed Operation Team</p>', 0, '2018-12-26 12:48:47.17', '2018-12-26 12:48:47.173', -1, -1);
INSERT INTO idsmed.notification VALUES (856, 1, 5, 'IDSMed – Request to Review Company Registration', '<p>Hi cong dzang,</p><p>A new vendor Testing QQ has registered into IDSMed. Kindly login to the system to review the information provided.</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>IDSMed Operation Team</p>', 0, '2018-12-26 17:03:29.345', '2018-12-26 17:03:29.35', -1, -1);
INSERT INTO idsmed.notification VALUES (858, 1, 5, 'IDSMed – Request to Review Company Registration', '<p>Hi cong dzang,</p><p>A new vendor 163 VP has registered into IDSMed. Kindly login to the system to review the information provided.</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>IDSMed Operation Team</p>', 0, '2018-12-26 17:13:07.276', '2018-12-26 17:13:07.28', -1, -1);
INSERT INTO idsmed.notification VALUES (860, 1, 2, 'IDSMed - Product Approval Notification', '<p>Hi nagisa,</p><p>We would like to inform you the status of your product registered under Angelic Garden in IDSMed. You will only receive this email if you have added new product / edit the existing product in the system.</p><p>&nbsp;</p><p><strong>Product Name</strong>: Umi Thermal Paper</p><p><strong>Product Model</strong>: </p><p><strong>Product Code</strong>: MTP39204212</p><p><strong>Status</strong>: Approved</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>IDSMed Operation Team</p>', 0, '2018-12-27 09:05:00.292', '2018-12-27 09:05:00.356', -1, -1);
INSERT INTO idsmed.notification VALUES (863, 1, 2, 'IDSMed - Product Certificate Approval Notification', '<p>Hi nagisa,</p><p>We would like to inform you the status of a Product document uploaded by you / your team in IDSMed.</p><p><br></p><p>Document Name: 00000; </p><p>Status: Approved</p><p><br></p><p>Thanks and Best Regards,</p><p>IDSMed Operation Team</p>', 0, '2018-12-27 09:05:00.338', '2018-12-27 09:05:00.356', -1, -1);
INSERT INTO idsmed.notification VALUES (2778, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Joey Ling,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Joey Ling</p><p>Company (For Vendor User): -</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-24 16:40:41.698', '2019-01-24 16:40:41.741', -1, -1);
INSERT INTO idsmed.notification VALUES (2780, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Joey Ling,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Joey Ling</p><p>Company (For Vendor User): -</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-24 16:40:41.705', '2019-01-24 16:40:41.741', -1, -1);
INSERT INTO idsmed.notification VALUES (880, 1, 2, 'IDSMed - Product Approval Notification', '<p>Hi nagisa,</p><p>We would like to inform you the status of your product registered under Angelic Garden in IDSMed. You will only receive this email if you have added new product / edit the existing product in the system.</p><p>&nbsp;</p><p><strong>Product Name</strong>: Umi Thermal Paper</p><p><strong>Product Model</strong>: </p><p><strong>Product Code</strong>: MTP39204212</p><p><strong>Status</strong>: Approved</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>IDSMed Operation Team</p>', 0, '2018-12-27 09:20:01.909', '2018-12-27 09:20:01.968', -1, -1);
INSERT INTO idsmed.notification VALUES (883, 1, 2, 'IDSMed - Product Certificate Approval Notification', '<p>Hi nagisa,</p><p>We would like to inform you the status of a Product document uploaded by you / your team in IDSMed.</p><p><br></p><p>Document Name: 00000; </p><p>Status: Approved</p><p><br></p><p>Thanks and Best Regards,</p><p>IDSMed Operation Team</p>', 0, '2018-12-27 09:20:01.949', '2018-12-27 09:20:01.968', -1, -1);
INSERT INTO idsmed.notification VALUES (900, 1, 2, 'IDSMed - Product Approval Notification', '<p>Hi nagisa,</p><p>We would like to inform you the status of your product registered under Angelic Garden in IDSMed. You will only receive this email if you have added new product / edit the existing product in the system.</p><p>&nbsp;</p><p><strong>Product Name</strong>: Umi Thermal Paper</p><p><strong>Product Model</strong>: </p><p><strong>Product Code</strong>: MTP39204212</p><p><strong>Status</strong>: Approved</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>IDSMed Operation Team</p>', 0, '2018-12-27 09:26:36.502', '2018-12-27 09:26:36.553', -1, -1);
INSERT INTO idsmed.notification VALUES (903, 1, 2, 'IDSMed - Product Certificate Approval Notification', '<p>Hi nagisa,</p><p>We would like to inform you the status of a Product document uploaded by you / your team in IDSMed.</p><p><br></p><p>Document Name: 00000; </p><p>Status: Approved</p><p><br></p><p>Thanks and Best Regards,</p><p>IDSMed Operation Team</p>', 0, '2018-12-27 09:26:36.535', '2018-12-27 09:26:36.553', -1, -1);
INSERT INTO idsmed.notification VALUES (920, 1, 5, 'IDSMed – Request to Review Company Registration', '<p>Hi cong dzang,</p><p>A new vendor Torn City has registered into IDSMed. Kindly login to the system to review the information provided.</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>IDSMed Operation Team</p>', 0, '2018-12-27 09:58:49.404', '2018-12-27 09:58:49.407', -1, -1);
INSERT INTO idsmed.notification VALUES (922, 1, 6, 'IDSMed – Request to Approve Company Registration', '<p>Hi Shino Asada,</p><p>A new vendor Torn City has registered into IDSMed and has been reviewed by the team. Kindly login to the system to review and approve the information provided.</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>IDSMed Operation Team</p>', 0, '2018-12-27 09:59:13.14', '2018-12-27 09:59:13.161', -1, -1);
INSERT INTO idsmed.notification VALUES (929, 1, 5, 'IDSMed – Request to Review Company Registration', '<p>Hi cong dzang,</p><p>A new vendor Testing Email Okay has registered with us. Kindly login to the system to review the information provided.</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2018-12-27 11:01:31.686', '2018-12-27 11:01:31.689', -1, -1);
INSERT INTO idsmed.notification VALUES (931, 1, 5, 'IDSMed – Request to Review Company Registration', '<p>Hi cong dzang,</p><p>A new vendor 医疗器械 has registered with us. Kindly login to the system to review the information provided.</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2018-12-27 12:50:38.583', '2018-12-27 12:50:38.586', -1, -1);
INSERT INTO idsmed.notification VALUES (940, 1, 5, 'IDSMed – Request to Review Company Registration', '<p>Hi cong dzang,</p><p>A new vendor 医疗器械 has registered with us. Kindly login to the system to review the information provided.</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2018-12-27 13:40:41.304', '2018-12-27 13:40:41.307', -1, -1);
INSERT INTO idsmed.notification VALUES (949, 1, 2, 'IDSMed - Product Approval Notification', '<p>Hi nagisa,</p><p>We would like to inform you the status of your product registered under Angelic Garden. You will only receive this email if you have added new product / edited the existing product in the system.</p><p>&nbsp;</p><p><strong>Product Name</strong>: Eternal Thermal Paper</p><p><strong>Product Model</strong>: </p><p><strong>Product Code</strong>: MTP392042121</p><p><strong>Status</strong>: Approved</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2018-12-27 14:12:00.94', '2018-12-27 14:12:01.001', -1, -1);
INSERT INTO idsmed.notification VALUES (952, 1, 2, 'IDSMed - Product Certificate Approval Notification', '<p>Hi nagisa,</p><p>We would like to inform you the status of a Product document uploaded by you / your team.</p><p><br></p><p>Document Name: 00000; </p><p>Status: Approved</p><p><br></p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2018-12-27 14:12:00.982', '2018-12-27 14:12:01.001', -1, -1);
INSERT INTO idsmed.notification VALUES (995, 1, 2, 'IDSMed - Product Approval Notification', '<p>Hi idsmed,</p><p>We would like to inform you the status of your product registered under IDSMed. You will only receive this email if you have added new product / edited the existing product in the system.</p><p>&nbsp;</p><p><strong>Product Name</strong>: Product Chong</p><p><strong>Product Model</strong>: </p><p><strong>Product Code</strong>: 12345</p><p><strong>Status</strong>: Approved</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2018-12-27 14:18:50.852', '2018-12-27 14:18:50.857', -1, -1);
INSERT INTO idsmed.notification VALUES (2782, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Joey Ling,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Joey Ling</p><p>Company (For Vendor User): -</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-24 16:40:41.71', '2019-01-24 16:40:41.741', -1, -1);
INSERT INTO idsmed.notification VALUES (2784, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Joey Ling,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Joey Ling</p><p>Company (For Vendor User): -</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-24 16:40:41.721', '2019-01-24 16:40:41.741', -1, -1);
INSERT INTO idsmed.notification VALUES (1130, 1, 6, 'IDSMed – Request to Approve Company Registration', '<p>Hi Shino Asada,</p><p>A new vendor Company Today has registered with us and the information was duly reviewed  by the team.  Kindly login to the system to review/approve the application.</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2018-12-28 18:59:46.299', '2018-12-28 18:59:46.317', -1, -1);
INSERT INTO idsmed.notification VALUES (997, 1, 2, 'IDSMed - Product Approval Notification', '<p>Hi test12272,</p><p>We would like to inform you the status of your product registered under 医疗器械. You will only receive this email if you have added new product / edited the existing product in the system.</p><p>&nbsp;</p><p><strong>Product Name</strong>: 亿万物联网1</p><p><strong>Product Model</strong>: EVVO IOT</p><p><strong>Product Code</strong>: CE0845</p><p><strong>Status</strong>: Approved</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2018-12-27 14:41:22.328', '2018-12-27 14:41:22.422', -1, -1);
INSERT INTO idsmed.notification VALUES (1001, 1, 2, 'IDSMed - Product Certificate Approval Notification', '<p>Hi test12272,</p><p>We would like to inform you the status of a Product document uploaded by you / your team.</p><p><br></p><p>Document Name: Business License; </p><p>Status: Approved</p><p><br></p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2018-12-27 14:41:22.402', '2018-12-27 14:41:22.422', -1, -1);
INSERT INTO idsmed.notification VALUES (1010, 1, 2, 'IDSMed - Product Approval Notification', '<p>Hi nagisa,</p><p>We would like to inform you the status of your product registered under Angelic Garden. You will only receive this email if you have added new product / edited the existing product in the system.</p><p>&nbsp;</p><p><strong>Product Name</strong>: Who Thermal Paper</p><p><strong>Product Model</strong>: </p><p><strong>Product Code</strong>: MTP39204212a</p><p><strong>Status</strong>: Approved</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2018-12-27 15:13:02.507', '2018-12-27 15:13:02.561', -1, -1);
INSERT INTO idsmed.notification VALUES (1013, 1, 2, 'IDSMed - Product Certificate Approval Notification', '<p>Hi nagisa,</p><p>We would like to inform you the status of a Product document uploaded by you / your team.</p><p><br></p><p>Document Name: 00000; </p><p>Status: Approved</p><p><br></p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2018-12-27 15:13:02.543', '2018-12-27 15:13:02.561', -1, -1);
INSERT INTO idsmed.notification VALUES (1030, 1, 5, 'IDSMed – Request to Review Company Registration', '<p>Hi cong dzang,</p><p>A new vendor Mamak Special has registered with us. Kindly login to the system to review the information provided.</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2018-12-27 16:19:00.446', '2018-12-27 16:19:00.479', -1, -1);
INSERT INTO idsmed.notification VALUES (1032, 1, 5, 'IDSMed – Request to Review Company Registration', '<p>Hi cong dzang,</p><p>A new vendor Test Vendor has registered with us. Kindly login to the system to review the information provided.</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2018-12-27 16:19:04.647', '2018-12-27 16:19:04.651', -1, -1);
INSERT INTO idsmed.notification VALUES (1034, 1, 5, 'IDSMed – Request to Review Company Registration', '<p>Hi cong dzang,</p><p>A new vendor Resubmit company has registered with us. Kindly login to the system to review the information provided.</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2018-12-27 16:28:46.71', '2018-12-27 16:28:46.718', -1, -1);
INSERT INTO idsmed.notification VALUES (1036, 1, 6, 'IDSMed – Request to Approve Company Registration', '<p>Hi Shino Asada,</p><p>A new vendor Test Vendor has registered with us and the information was duly reviewed  by the team.  Kindly login to the system to review/approve the application.</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2018-12-27 16:29:21.959', '2018-12-27 16:29:21.991', -1, -1);
INSERT INTO idsmed.notification VALUES (1043, 1, 5, 'IDSMed – Request to Review Company Registration', '<p>Hi cong dzang,</p><p>A new vendor Resubmit company 1 has registered with us. Kindly login to the system to review the information provided.</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2018-12-27 16:52:41.208', '2018-12-27 16:52:41.212', -1, -1);
INSERT INTO idsmed.notification VALUES (1045, 1, 5, 'IDSMed – Request to Review Company Registration', '<p>Hi cong dzang,</p><p>A new vendor Resubmit company 2 has registered with us. Kindly login to the system to review the information provided.</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2018-12-27 17:12:02.911', '2018-12-27 17:12:02.917', -1, -1);
INSERT INTO idsmed.notification VALUES (1074, 1, 1, 'IDSMed - User Registration Approval Notification', '<p>Hi New SCP User 1,</p><p>We would like to inform you the status of your account registration.</p><p>&nbsp;</p><p>Status: Approved</p><p><br></p><p>You may login to the system now to perform your daily operation.</p><p>&nbsp;</p><p><br></p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2018-12-27 17:19:45.984', '2018-12-27 17:19:45.994', -1, -1);
INSERT INTO idsmed.notification VALUES (1076, 1, 1, 'IDSMed - New User Registration Notification', '<p>Hi New SCP User 1,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: New SCP User 1</p><p>Company (For Vendor User): </p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2018-12-27 17:19:46.03', '2018-12-27 17:19:46.055', -1, -1);
INSERT INTO idsmed.notification VALUES (2786, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Joey Ling,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Joey Ling</p><p>Company (For Vendor User): -</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-24 16:40:41.728', '2019-01-24 16:40:41.741', -1, -1);
INSERT INTO idsmed.notification VALUES (1101, 1, 2, 'IDSMed - Product Approval Notification', '<p>Hi vendorjunior2,</p><p>We would like to inform you the status of your product registered under Test Vendor. You will only receive this email if you have added new product / edited the existing product in the system.</p><p>&nbsp;</p><p><strong>Product Name</strong>: Tooth brush</p><p><strong>Product Model</strong>: PM001</p><p><strong>Product Code</strong>: PC001</p><p><strong>Status</strong>: Approved</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2018-12-28 09:49:38.404', '2018-12-28 09:49:38.476', -1, -1);
INSERT INTO idsmed.notification VALUES (2788, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Joey Ling,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Joey Ling</p><p>Company (For Vendor User): -</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-24 16:40:41.734', '2019-01-24 16:40:41.741', -1, -1);
INSERT INTO idsmed.notification VALUES (2790, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Joey Ling,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Joey Ling</p><p>Company (For Vendor User): -</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-24 16:40:41.739', '2019-01-24 16:40:41.741', -1, -1);
INSERT INTO idsmed.notification VALUES (1104, 1, 2, 'IDSMed - Product Certificate Approval Notification', '<p>Hi vendorjunior2,</p><p>We would like to inform you the status of a Product document uploaded by you / your team.</p><p><br></p><p>Document Name: Product Certificate; </p><p>Status: Approved</p><p><br></p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2018-12-28 09:49:38.455', '2018-12-28 09:49:38.476', -1, -1);
INSERT INTO idsmed.notification VALUES (1119, 1, 5, 'IDSMed – Request to Review Company Registration', '<p>Hi cong dzang,</p><p>A new vendor Vincent Eric Cong Wan has registered with us. Kindly login to the system to review the information provided.</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2018-12-28 15:29:17.85', '2018-12-28 15:29:17.853', -1, -1);
INSERT INTO idsmed.notification VALUES (1137, 1, 2, 'IDSMed - Product Approval Notification', '<p>Hi chiggytest,</p><p>We would like to inform you the status of your product registered under Company Today. You will only receive this email if you have added new product / edited the existing product in the system.</p><p>&nbsp;</p><p><strong>Product Name</strong>: Capital Thermal Paper</p><p><strong>Product Model</strong>: </p><p><strong>Product Code</strong>: MTP39204212aqef</p><p><strong>Status</strong>: Approved</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2018-12-28 19:03:37.013', '2018-12-28 19:03:37.04', -1, -1);
INSERT INTO idsmed.notification VALUES (1139, 1, 2, 'IDSMed - Product Certificate Approval Notification', '<p>Hi chiggytest,</p><p>We would like to inform you the status of a Product document uploaded by you / your team.</p><p><br></p><p>Document Name: Product Certificate; </p><p>Status: Approved</p><p><br></p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2018-12-28 19:03:37.03', '2018-12-28 19:03:37.04', -1, -1);
INSERT INTO idsmed.notification VALUES (1146, 1, 2, 'IDSMed - Product Approval Notification', '<p>Hi virada,</p><p>We would like to inform you the status of your product registered under Angelic Garden. You will only receive this email if you have added new product / edited the existing product in the system.</p><p>&nbsp;</p><p><strong>Product Name</strong>: Medic Consume</p><p><strong>Product Model</strong>: </p><p><strong>Product Code</strong>: MTP392042121aa</p><p><strong>Status</strong>: Approved</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2018-12-28 19:51:06.058', '2018-12-28 19:51:06.269', -1, -1);
INSERT INTO idsmed.notification VALUES (1148, 1, 2, 'IDSMed - Product Certificate Approval Notification', '<p>Hi virada,</p><p>We would like to inform you the status of a Product document uploaded by you / your team.</p><p><br></p><p>Document Name: Product Certificate; </p><p>Status: Approved</p><p><br></p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2018-12-28 19:51:06.235', '2018-12-28 19:51:06.269', -1, -1);
INSERT INTO idsmed.notification VALUES (1166, 1, 10, 'IDSMed - Product Certificate Expiry Reminder', '<p>Hi Company Today,</p><p>One of your product''s document certificate are expiring soon.Below is the information of the document certificate:</p><p><br></p><p><strong>Product Name</strong>: Capital Thermal Paper</p><p><strong>Product Code</strong>: MTP39204212aqef</p><p><strong>Document Name</strong>: Product Certificate</p><p><strong>Expiry Date</strong>: 2018-12-29 00:00:00.0</p><p><br></p><p>Kindly upload the latest certification/document soonest possible.</p><p><br></p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2018-12-28 20:03:00.169', '2018-12-28 20:03:00.184', -1, -1);
INSERT INTO idsmed.notification VALUES (1171, 1, 10, 'IDSMed - Product Certificate Expiry Reminder', '<p>Hi Angelic Garden,</p><p>One of your product''s document certificate are expiring soon.Below is the information of the document certificate:</p><p><br></p><p><strong>Product Name</strong>: Medic Consume</p><p><strong>Product Code</strong>: MTP392042121aa</p><p><strong>Document Name</strong>: Product Certificate</p><p><strong>Expiry Date</strong>: 2018-12-28 00:00:00.0</p><p><br></p><p>Kindly upload the latest certification/document soonest possible.</p><p><br></p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2018-12-28 20:03:00.279', '2018-12-28 20:03:00.281', -1, -1);
INSERT INTO idsmed.notification VALUES (2792, 1, 6, 'IDSMed – Request to Approve Company Registration', '<p>Hi Joey Ling,</p><p>A new vendor abcMED has registered with us and the information was duly reviewed  by the team.  Kindly login to the system to review/approve the application.</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-24 17:07:54.463', '2019-01-24 17:07:54.49', -1, -1);
INSERT INTO idsmed.notification VALUES (2855, 1, 6, 'IDSMed – Request to Approve Company Registration', '<p>Hi Joey Ling,</p><p>A new vendor Clannad After Story has registered with us and the information was duly reviewed  by the team.  Kindly login to the system to review/approve the application.</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-25 12:23:36.659', '2019-01-25 12:23:36.686', -1, -1);
INSERT INTO idsmed.notification VALUES (2906, 1, 6, 'IDSMed – Request to Approve Company Registration', '<p>Hi Joey Ling,</p><p>A new vendor ly test has registered with us and the information was duly reviewed  by the team.  Kindly login to the system to review/approve the application.</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-25 13:44:15.219', '2019-01-25 13:44:15.242', -1, -1);
INSERT INTO idsmed.notification VALUES (2925, 1, 6, 'IDSMed – Request to Approve Company Registration', '<p>Hi Joey Ling,</p><p>A new vendor test vendor approval has registered with us and the information was duly reviewed  by the team.  Kindly login to the system to review/approve the application.</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-25 15:08:02.59', '2019-01-25 15:08:02.617', -1, -1);
INSERT INTO idsmed.notification VALUES (1165, 1, 9, 'IDSMed - Vendor Profile Certificate Expiry Reminder', '<p>Hi Company Today,</p><p>One of your document certificate is expiring soon.Below is the information of the document certificate:</p><p><br></p><p>Document Name: Business License</p><p>Expiry Date: 2018-12-28 00:00:00.0</p><p><br></p><p>Kindly upload the latest certification/document soonest possible. </p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p><p><br></p>', 0, '2018-12-28 20:03:00.165', '2018-12-28 20:03:00.184', -1, -1);
INSERT INTO idsmed.notification VALUES (1169, 1, 9, 'IDSMed - Vendor Profile Certificate Expiry Reminder', '<p>Hi Company Today,</p><p>One of your document certificate is expiring soon.Below is the information of the document certificate:</p><p><br></p><p>Document Name: Trademark Registration Certificate</p><p>Expiry Date: 2018-12-29 00:00:00.0</p><p><br></p><p>Kindly upload the latest certification/document soonest possible. </p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p><p><br></p>', 0, '2018-12-28 20:03:00.26', '2018-12-28 20:03:00.267', -1, -1);
INSERT INTO idsmed.notification VALUES (1173, 1, 9, 'IDSMed - Vendor Profile Certificate Expiry Reminder', '<p>Hi Company Today,</p><p>One of your document certificate is expiring soon.Below is the information of the document certificate:</p><p><br></p><p>Document Name: Business License</p><p>Expiry Date: 2018-12-28 00:00:00.0</p><p><br></p><p>Kindly upload the latest certification/document soonest possible. </p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p><p><br></p>', 0, '2018-12-29 20:03:00.072', '2018-12-29 20:03:00.076', -1, -1);
INSERT INTO idsmed.notification VALUES (1175, 1, 9, 'IDSMed - Vendor Profile Certificate Expiry Reminder', '<p>Hi Company Today,</p><p>One of your document certificate is expiring soon.Below is the information of the document certificate:</p><p><br></p><p>Document Name: Trademark Registration Certificate</p><p>Expiry Date: 2018-12-29 00:00:00.0</p><p><br></p><p>Kindly upload the latest certification/document soonest possible. </p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p><p><br></p>', 0, '2018-12-29 20:03:00.093', '2018-12-29 20:03:00.096', -1, -1);
INSERT INTO idsmed.notification VALUES (1177, 1, 10, 'IDSMed - Product Certificate Expiry Reminder', '<p>Hi Company Today,</p><p>One of your product''s document certificate are expiring soon.Below is the information of the document certificate:</p><p><br></p><p><strong>Product Name</strong>: Capital Thermal Paper</p><p><strong>Product Code</strong>: MTP39204212aqef</p><p><strong>Document Name</strong>: Product Certificate</p><p><strong>Expiry Date</strong>: 2018-12-29 00:00:00.0</p><p><br></p><p>Kindly upload the latest certification/document soonest possible.</p><p><br></p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2018-12-29 20:03:00.17', '2018-12-29 20:03:00.173', -1, -1);
INSERT INTO idsmed.notification VALUES (1179, 1, 10, 'IDSMed - Product Certificate Expiry Reminder', '<p>Hi Angelic Garden,</p><p>One of your product''s document certificate are expiring soon.Below is the information of the document certificate:</p><p><br></p><p><strong>Product Name</strong>: Medic Consume</p><p><strong>Product Code</strong>: MTP392042121aa</p><p><strong>Document Name</strong>: Product Certificate</p><p><strong>Expiry Date</strong>: 2018-12-28 00:00:00.0</p><p><br></p><p>Kindly upload the latest certification/document soonest possible.</p><p><br></p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2018-12-29 20:03:00.201', '2018-12-29 20:03:00.204', -1, -1);
INSERT INTO idsmed.notification VALUES (1181, 1, 9, 'IDSMed - Vendor Profile Certificate Expiry Reminder', '<p>Hi Company Today,</p><p>One of your document certificate is expiring soon.Below is the information of the document certificate:</p><p><br></p><p>Document Name: Trademark Registration Certificate</p><p>Expiry Date: 2018-12-29 00:00:00.0</p><p><br></p><p>Kindly upload the latest certification/document soonest possible. </p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p><p><br></p>', 0, '2018-12-30 20:03:00.164', '2018-12-30 20:03:00.172', -1, -1);
INSERT INTO idsmed.notification VALUES (1183, 1, 10, 'IDSMed - Product Certificate Expiry Reminder', '<p>Hi Company Today,</p><p>One of your product''s document certificate are expiring soon.Below is the information of the document certificate:</p><p><br></p><p><strong>Product Name</strong>: Capital Thermal Paper</p><p><strong>Product Code</strong>: MTP39204212aqef</p><p><strong>Document Name</strong>: Product Certificate</p><p><strong>Expiry Date</strong>: 2018-12-29 00:00:00.0</p><p><br></p><p>Kindly upload the latest certification/document soonest possible.</p><p><br></p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2018-12-30 20:03:00.189', '2018-12-30 20:03:00.195', -1, -1);
INSERT INTO idsmed.notification VALUES (1185, 1, 5, 'IDSMed – Request to Review Company Registration', '<p>Hi cong dzang,</p><p>A new vendor dxc has registered with us. Kindly login to the system to review the information provided.</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-03 10:37:08.523', '2019-01-03 10:37:08.608', -1, -1);
INSERT INTO idsmed.notification VALUES (1202, 1, 1, 'IDSMed - New User Registration Notification', '<p>Hi sam reddy,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: sam reddy</p><p>Company (For Vendor User): dxc</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-03 10:42:42.678', '2019-01-03 10:42:42.705', -1, -1);
INSERT INTO idsmed.notification VALUES (1261, 1, 2, 'IDSMed - Product Approval Notification', '<p>Hi,</p><p>We would like to inform you that a product has been approved under dxc. Below is the summary of the product:</p><p>&nbsp;</p><p>Product Name: fhgf</p><p>Product Model: </p><p>Product Code: 123</p><p><br></p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-03 11:00:21.658', '2019-01-03 11:00:21.67', -1, -1);
INSERT INTO idsmed.notification VALUES (1258, 1, 2, 'IDSMed - Product Approval Notification', '<p>Hi hari reddy,</p><p>We would like to inform you the status of your product registered under dxc. You will only receive this email if you have added new product / edited the existing product in the system.</p><p>&nbsp;</p><p><strong>Product Name</strong>: fhgf</p><p><strong>Product Model</strong>: </p><p><strong>Product Code</strong>: 123</p><p><strong>Status</strong>: Approved</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-03 11:00:21.633', '2019-01-03 11:00:21.67', -1, -1);
INSERT INTO idsmed.notification VALUES (1266, 1, 5, 'IDSMed – Request to Review Company Registration', '<p>Hi cong dzang,</p><p>A new vendor samHP has registered with us. Kindly login to the system to review the information provided.</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-03 11:26:17.77', '2019-01-03 11:26:17.808', -1, -1);
INSERT INTO idsmed.notification VALUES (1311, 1, 1, 'IDSMed - New User Registration Notification', '<p>Hi sam reddyy,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: sam reddyy</p><p>Company (For Vendor User): samHP</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-03 11:31:49.62', '2019-01-03 11:31:49.654', -1, -1);
INSERT INTO idsmed.notification VALUES (1369, 1, 2, 'IDSMed - Product Approval Notification', '<p>Hi IDSMed Staff 1,</p><p>We would like to inform you the status of your product registered under IDSMed. You will only receive this email if you have added new product / edited the existing product in the system.</p><p>&nbsp;</p><p><strong>Product Name</strong>: CADD Extension Sets 60 Inch/1.7 ml</p><p><strong>Product Model</strong>: </p><p><strong>Product Code</strong>: PRD-ECAT-00000011</p><p><strong>Status</strong>: Approved</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-03 14:45:05.087', '2019-01-03 14:45:05.118', -1, -1);
INSERT INTO idsmed.notification VALUES (1371, 1, 2, 'IDSMed - Product Approval Notification', '<p>Hi,</p><p>We would like to inform you that a product has been approved under IDSMed. Below is the summary of the product:</p><p>&nbsp;</p><p>Product Name: CADD Extension Sets 60 Inch/1.7 ml</p><p>Product Model: </p><p>Product Code: PRD-ECAT-00000011</p><p><br></p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-03 14:45:05.108', '2019-01-03 14:45:05.118', -1, -1);
INSERT INTO idsmed.notification VALUES (1376, 1, 2, 'IDSMed - Product Approval Notification', '<p>Hi IDSMed Staff 1,</p><p>We would like to inform you the status of your product registered under IDSMed. You will only receive this email if you have added new product / edited the existing product in the system.</p><p>&nbsp;</p><p><strong>Product Name</strong>: HEMOSTAT GELITA SPON RAPID 3 , 80 X 25 X 4 MM</p><p><strong>Product Model</strong>: </p><p><strong>Product Code</strong>: PRD-ECAT-00000131</p><p><strong>Status</strong>: Approved</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-03 14:45:33.797', '2019-01-03 14:45:33.819', -1, -1);
INSERT INTO idsmed.notification VALUES (1378, 1, 2, 'IDSMed - Product Approval Notification', '<p>Hi,</p><p>We would like to inform you that a product has been approved under IDSMed. Below is the summary of the product:</p><p>&nbsp;</p><p>Product Name: HEMOSTAT GELITA SPON RAPID 3 , 80 X 25 X 4 MM</p><p>Product Model: </p><p>Product Code: PRD-ECAT-00000131</p><p><br></p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-03 14:45:33.812', '2019-01-03 14:45:33.819', -1, -1);
INSERT INTO idsmed.notification VALUES (1383, 1, 2, 'IDSMed - Product Approval Notification', '<p>Hi IDSMed Staff 1,</p><p>We would like to inform you the status of your product registered under IDSMed. You will only receive this email if you have added new product / edited the existing product in the system.</p><p>&nbsp;</p><p><strong>Product Name</strong>: Cutera Xeo</p><p><strong>Product Model</strong>: </p><p><strong>Product Code</strong>: PRD-ECAT-00000001</p><p><strong>Status</strong>: Approved</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-03 14:45:55.496', '2019-01-03 14:45:55.52', -1, -1);
INSERT INTO idsmed.notification VALUES (1385, 1, 2, 'IDSMed - Product Approval Notification', '<p>Hi,</p><p>We would like to inform you that a product has been approved under IDSMed. Below is the summary of the product:</p><p>&nbsp;</p><p>Product Name: Cutera Xeo</p><p>Product Model: </p><p>Product Code: PRD-ECAT-00000001</p><p><br></p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-03 14:45:55.512', '2019-01-03 14:45:55.52', -1, -1);
INSERT INTO idsmed.notification VALUES (1390, 1, 2, 'IDSMed - Product Approval Notification', '<p>Hi IDSMed Staff 1,</p><p>We would like to inform you the status of your product registered under IDSMed. You will only receive this email if you have added new product / edited the existing product in the system.</p><p>&nbsp;</p><p><strong>Product Name</strong>: Product Chong</p><p><strong>Product Model</strong>: </p><p><strong>Product Code</strong>: 12345</p><p><strong>Status</strong>: Approved</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-03 16:44:32.633', '2019-01-03 16:44:32.66', -1, -1);
INSERT INTO idsmed.notification VALUES (1392, 1, 2, 'IDSMed - Product Approval Notification', '<p>Hi,</p><p>We would like to inform you that a product has been approved under IDSMed. Below is the summary of the product:</p><p>&nbsp;</p><p>Product Name: Product Chong</p><p>Product Model: </p><p>Product Code: 12345</p><p><br></p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-03 16:44:32.65', '2019-01-03 16:44:32.66', -1, -1);
INSERT INTO idsmed.notification VALUES (1397, 1, 5, 'IDSMed – Request to Review Company Registration', '<p>Hi cong dzang,</p><p>A new vendor Hospital 1 has registered with us. Kindly login to the system to review the information provided.</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-03 18:38:29.946', '2019-01-03 18:38:30.125', -1, -1);
INSERT INTO idsmed.notification VALUES (1409, 1, 2, 'IDSMed - Product Approval Notification', '<p>Hi IDSMed Staff 1,</p><p>We would like to inform you the status of your product registered under IDSMed. You will only receive this email if you have added new product / edited the existing product in the system.</p><p>&nbsp;</p><p><strong>Product Name</strong>: 拜富利登塗藥冠狀動脈支架系統</p><p><strong>Product Model</strong>: </p><p><strong>Product Code</strong>: PRD-ECAT-00000001</p><p><strong>Status</strong>: Approved</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-04 08:58:47.389', '2019-01-04 08:58:47.48', -1, -1);
INSERT INTO idsmed.notification VALUES (1411, 1, 2, 'IDSMed - Product Approval Notification', '<p>Hi,</p><p>We would like to inform you that a product has been approved under IDSMed. Below is the summary of the product:</p><p>&nbsp;</p><p>Product Name: 拜富利登塗藥冠狀動脈支架系統</p><p>Product Model: </p><p>Product Code: PRD-ECAT-00000001</p><p><br></p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-04 08:58:47.461', '2019-01-04 08:58:47.48', -1, -1);
INSERT INTO idsmed.notification VALUES (1416, 1, 1, 'IDSMed - New User Registration Notification', '<p>Hi Chong Sian,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Chong Sian</p><p>Company (For Vendor User): -</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-07 08:49:40.029', '2019-01-07 08:49:40.098', -1, -1);
INSERT INTO idsmed.notification VALUES (1445, 1, 1, 'IDSMed - New User Registration Notification', '<p>Hi Robin  Hood,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Robin  Hood</p><p>Company (For Vendor User): -</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-07 11:25:21.895', '2019-01-07 11:25:21.944', -1, -1);
INSERT INTO idsmed.notification VALUES (1475, 1, 1, 'IDSMed - New User Registration Notification', '<p>Hi Marie Marianne,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Marie Marianne</p><p>Company (For Vendor User): -</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-07 11:50:53.201', '2019-01-07 11:50:53.234', -1, -1);
INSERT INTO idsmed.notification VALUES (1506, 1, 1, 'IDSMed - New User Registration Notification', '<p>Hi Asuna Venus,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Asuna Venus</p><p>Company (For Vendor User): -</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-07 11:59:10.654', '2019-01-07 11:59:10.684', -1, -1);
INSERT INTO idsmed.notification VALUES (1537, 1, 1, 'IDSMed - New User Registration Notification', '<p>Hi Chong Sian,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Chong Sian</p><p>Company (For Vendor User): -</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-07 12:11:52.204', '2019-01-07 12:11:52.237', -1, -1);
INSERT INTO idsmed.notification VALUES (1569, 1, 1, 'IDSMed - New User Registration Notification', '<p>Hi Chong Sian,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Chong Sian</p><p>Company (For Vendor User): -</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-07 12:41:11.419', '2019-01-07 12:41:11.454', -1, -1);
INSERT INTO idsmed.notification VALUES (1601, 1, 1, 'IDSMed - New User Registration Notification', '<p>Hi Chong Sian,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Chong Sian</p><p>Company (For Vendor User): -</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-07 14:12:37.24', '2019-01-07 14:12:37.289', -1, -1);
INSERT INTO idsmed.notification VALUES (1633, 1, 5, 'IDSMed – Request to Review Company Registration', '<p>Hi cong dzang,</p><p>A new vendor ABC Medical has registered with us. Kindly login to the system to review the information provided.</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-07 14:15:12.012', '2019-01-07 14:15:12.078', -1, -1);
INSERT INTO idsmed.notification VALUES (1655, 1, 1, 'IDSMed - New User Registration Notification', '<p>Hi Chong Sian,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Chong Sian</p><p>Company (For Vendor User): -</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-07 14:21:07.096', '2019-01-07 14:21:07.124', -1, -1);
INSERT INTO idsmed.notification VALUES (1717, 1, 1, 'IDSMed - New User Registration Notification', '<p>Hi Chong Sian,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Chong Sian</p><p>Company (For Vendor User): -</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-07 14:30:02.514', '2019-01-07 14:30:02.543', -1, -1);
INSERT INTO idsmed.notification VALUES (1759, 1, 1, 'IDSMed - New User Registration Notification', '<p>Hi Chong Sian,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Chong Sian</p><p>Company (For Vendor User): -</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-07 16:40:32.547', '2019-01-07 16:40:32.582', -1, -1);
INSERT INTO idsmed.notification VALUES (1855, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi First User,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: First User</p><p>Company (For Vendor User): ABC Medical</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-07 17:37:16.52', '2019-01-07 17:37:16.553', -1, -1);
INSERT INTO idsmed.notification VALUES (1923, 105, 9, 'IDSMed - Vendor Profile Certificate Expiry Reminder', '<p>Hi dxc,</p><p>One of your document certificate is expiring soon.Below is the information of the document certificate:</p><p><br></p><p>Document Name: Trademark Registration Certificate</p><p>Expiry Date: 2019-02-10 00:00:00.0</p><p><br></p><p>Kindly upload the latest certification/document soonest possible. </p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p><p><br></p>', 0, '2019-01-11 17:40:46.959', '2019-01-11 17:40:46.97', -1, -1);
INSERT INTO idsmed.notification VALUES (1925, 1, 1, 'IDSMed - New User Registration Notification', '<p>Hi Kee Tuan Hwa,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Kee Tuan Hwa</p><p>Company (For Vendor User): -</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-12 15:10:05.336', '2019-01-12 15:10:05.374', -1, -1);
INSERT INTO idsmed.notification VALUES (1967, 1, 1, 'IDSMed - New User Registration Notification', '<p>Hi Tony Stark,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Tony Stark</p><p>Company (For Vendor User): -</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-12 15:48:37.356', '2019-01-12 15:48:37.395', -1, -1);
INSERT INTO idsmed.notification VALUES (2010, 1, 1, 'IDSMed - New User Registration Notification', '<p>Hi Thor Odinson,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Thor Odinson</p><p>Company (For Vendor User): -</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-12 15:52:52.918', '2019-01-12 15:52:52.957', -1, -1);
INSERT INTO idsmed.notification VALUES (2096, 1, 1, 'IDSMed - New User Registration Notification', '<p>Hi admin,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: admin</p><p>Company (For Vendor User): -</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-12 16:13:34.772', '2019-01-12 16:13:34.834', -1, -1);
INSERT INTO idsmed.notification VALUES (2140, 1, 1, 'IDSMed - New User Registration Notification', '<p>Hi admin,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: admin</p><p>Company (For Vendor User): -</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-12 16:15:23.414', '2019-01-12 16:15:23.461', -1, -1);
INSERT INTO idsmed.notification VALUES (2216, 105, 5, 'IDSMed – Request to Review Company Registration', '<p>Hi super admin,</p><p>A new vendor KEECOMPANY has registered with us. Kindly login to the system to review the information provided.</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-12 17:04:18.431', '2019-01-12 17:04:18.516', -1, -1);
INSERT INTO idsmed.notification VALUES (2241, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi ,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: </p><p>Company (For Vendor User): KEECOMPANY</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-12 17:15:18.37', '2019-01-12 17:15:18.415', -1, -1);
INSERT INTO idsmed.notification VALUES (2331, 1, 2, 'IDSMed - Product Approval Notification', '<p>Hi KEE KEE,</p><p>We would like to inform you the status of your product registered under KEECOMPANY. You will only receive this email if you have added new product / edited the existing product in the system.</p><p>&nbsp;</p><p><strong>Product Name</strong>: Product123</p><p><strong>Product Model</strong>: </p><p><strong>Product Code</strong>: 123456</p><p><strong>Status</strong>: Approved</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-12 18:02:59.916', '2019-01-12 18:03:00.055', -1, -1);
INSERT INTO idsmed.notification VALUES (2334, 1, 2, 'IDSMed - Product Approval Notification', '<p>Hi,</p><p>We would like to inform you that a product has been approved under KEECOMPANY. Below is the summary of the product:</p><p>&nbsp;</p><p>Product Name: Product123</p><p>Product Model: </p><p>Product Code: 123456</p><p><br></p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-12 18:02:59.962', '2019-01-12 18:03:00.055', -1, -1);
INSERT INTO idsmed.notification VALUES (2347, 1, 2, 'IDSMed - Product Certificate Approval Notification', '<p>Hi keecompanyj,</p><p>We would like to inform you the status of a Product document uploaded by you / your team.</p><p><br></p><p>Document Name: Product Certificate; </p><p>Status: Approved</p><p><br></p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-12 18:03:00.018', '2019-01-12 18:03:00.055', -1, -1);
INSERT INTO idsmed.notification VALUES (2408, 105, 5, 'IDSMed – Request to Review Company Registration', '<p>Hi super admin,</p><p>A new vendor Fonsmedic has registered with us. Kindly login to the system to review the information provided.</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-15 10:38:56.056', '2019-01-15 10:38:56.117', -1, -1);
INSERT INTO idsmed.notification VALUES (2425, 1, 6, 'IDSMed – Request to Approve Company Registration', '<p>Hi Steve Rogers,</p><p>A new vendor Fonsmedic has registered with us and the information was duly reviewed  by the team.  Kindly login to the system to review/approve the application.</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-15 10:40:51.742', '2019-01-15 10:40:51.769', -1, -1);
INSERT INTO idsmed.notification VALUES (2467, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi ,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: </p><p>Company (For Vendor User): Fonsmedic</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-15 10:48:06.768', '2019-01-15 10:48:06.811', -1, -1);
INSERT INTO idsmed.notification VALUES (2450, 105, 5, 'IDSMed – Request to Review Company Registration', '<p>Hi super admin,</p><p>A new vendor 德润特数字影像科技（北京）有限公司 has registered with us. Kindly login to the system to review the information provided.</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-15 10:47:18.748', '2019-01-15 10:47:18.809', -1, -1);
INSERT INTO idsmed.notification VALUES (2513, 1, 6, 'IDSMed – Request to Approve Company Registration', '<p>Hi Steve Rogers,</p><p>A new vendor 德润特数字影像科技（北京）有限公司 has registered with us and the information was duly reviewed  by the team.  Kindly login to the system to review/approve the application.</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-15 10:49:01.356', '2019-01-15 10:49:01.374', -1, -1);
INSERT INTO idsmed.notification VALUES (2811, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Michelle Lee,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Michelle Lee</p><p>Company (For Vendor User): abcMED</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-24 17:20:07.691', '2019-01-24 17:20:07.76', -1, -1);
INSERT INTO idsmed.notification VALUES (2813, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Michelle Lee,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Michelle Lee</p><p>Company (For Vendor User): abcMED</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-24 17:20:07.7', '2019-01-24 17:20:07.76', -1, -1);
INSERT INTO idsmed.notification VALUES (2815, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Michelle Lee,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Michelle Lee</p><p>Company (For Vendor User): abcMED</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-24 17:20:07.711', '2019-01-24 17:20:07.76', -1, -1);
INSERT INTO idsmed.notification VALUES (2817, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Michelle Lee,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Michelle Lee</p><p>Company (For Vendor User): abcMED</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-24 17:20:07.717', '2019-01-24 17:20:07.76', -1, -1);
INSERT INTO idsmed.notification VALUES (2819, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Michelle Lee,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Michelle Lee</p><p>Company (For Vendor User): abcMED</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-24 17:20:07.725', '2019-01-24 17:20:07.76', -1, -1);
INSERT INTO idsmed.notification VALUES (2821, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Michelle Lee,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Michelle Lee</p><p>Company (For Vendor User): abcMED</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-24 17:20:07.73', '2019-01-24 17:20:07.76', -1, -1);
INSERT INTO idsmed.notification VALUES (2823, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Michelle Lee,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Michelle Lee</p><p>Company (For Vendor User): abcMED</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-24 17:20:07.735', '2019-01-24 17:20:07.76', -1, -1);
INSERT INTO idsmed.notification VALUES (2825, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Michelle Lee,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Michelle Lee</p><p>Company (For Vendor User): abcMED</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-24 17:20:07.741', '2019-01-24 17:20:07.76', -1, -1);
INSERT INTO idsmed.notification VALUES (2827, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Michelle Lee,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Michelle Lee</p><p>Company (For Vendor User): abcMED</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-24 17:20:07.746', '2019-01-24 17:20:07.76', -1, -1);
INSERT INTO idsmed.notification VALUES (2829, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Michelle Lee,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Michelle Lee</p><p>Company (For Vendor User): abcMED</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-24 17:20:07.752', '2019-01-24 17:20:07.76', -1, -1);
INSERT INTO idsmed.notification VALUES (2521, 1, 6, 'IDSMed – Request to Approve Company Registration', '<p>Hi Steve Rogers,</p><p>A new vendor 克里斯提娜医疗器械有限公司 has registered with us and the information was duly reviewed  by the team.  Kindly login to the system to review/approve the application.</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-15 10:55:26.326', '2019-01-15 10:55:26.347', -1, -1);
INSERT INTO idsmed.notification VALUES (2529, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi ,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: </p><p>Company (For Vendor User): 克里斯提娜医疗器械有限公司</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-15 11:05:05.866', '2019-01-15 11:05:05.904', -1, -1);
INSERT INTO idsmed.notification VALUES (2576, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi ,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: </p><p>Company (For Vendor User): 德润特数字影像科技（北京）有限公司</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-15 17:22:56.018', '2019-01-15 17:22:56.061', -1, -1);
INSERT INTO idsmed.notification VALUES (2624, 1, 6, 'IDSMed – Request to Approve Company Registration', '<p>Hi Steve Rogers,</p><p>A new vendor VendorFileAttachment has registered with us and the information was duly reviewed  by the team.  Kindly login to the system to review/approve the application.</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-22 10:16:49.315', '2019-01-22 10:16:49.5', -1, -1);
INSERT INTO idsmed.notification VALUES (2642, 1, 6, 'IDSMed – Request to Approve Company Registration', '<p>Hi Steve Rogers,</p><p>A new vendor Mamak Medical Special has registered with us and the information was duly reviewed  by the team.  Kindly login to the system to review/approve the application.</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-23 15:54:42.504', '2019-01-23 15:54:42.598', -1, -1);
INSERT INTO idsmed.notification VALUES (2831, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Michelle Lee,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Michelle Lee</p><p>Company (For Vendor User): abcMED</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-24 17:20:07.757', '2019-01-24 17:20:07.76', -1, -1);
INSERT INTO idsmed.notification VALUES (3172, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi ML Lee,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: ML Lee</p><p>Company (For Vendor User): abcMED</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-29 10:36:30.977', '2019-01-29 10:36:31.033', -1, -1);
INSERT INTO idsmed.notification VALUES (3174, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi ML Lee,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: ML Lee</p><p>Company (For Vendor User): abcMED</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-29 10:36:30.984', '2019-01-29 10:36:31.033', -1, -1);
INSERT INTO idsmed.notification VALUES (3176, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi ML Lee,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: ML Lee</p><p>Company (For Vendor User): abcMED</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-29 10:36:30.992', '2019-01-29 10:36:31.033', -1, -1);
INSERT INTO idsmed.notification VALUES (2660, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Mamak  Special,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Mamak  Special</p><p>Company (For Vendor User): Mamak Medical Special</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-23 16:01:12.491', '2019-01-23 16:01:12.72', -1, -1);
INSERT INTO idsmed.notification VALUES (2662, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Mamak  Special,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Mamak  Special</p><p>Company (For Vendor User): Mamak Medical Special</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-23 16:01:12.517', '2019-01-23 16:01:12.72', -1, -1);
INSERT INTO idsmed.notification VALUES (3178, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi ML Lee,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: ML Lee</p><p>Company (For Vendor User): abcMED</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-29 10:36:31.015', '2019-01-29 10:36:31.033', -1, -1);
INSERT INTO idsmed.notification VALUES (3180, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi ML Lee,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: ML Lee</p><p>Company (For Vendor User): abcMED</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-29 10:36:31.029', '2019-01-29 10:36:31.033', -1, -1);
INSERT INTO idsmed.notification VALUES (2664, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Mamak  Special,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Mamak  Special</p><p>Company (For Vendor User): Mamak Medical Special</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-23 16:01:12.544', '2019-01-23 16:01:12.72', -1, -1);
INSERT INTO idsmed.notification VALUES (2666, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Mamak  Special,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Mamak  Special</p><p>Company (For Vendor User): Mamak Medical Special</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-23 16:01:12.564', '2019-01-23 16:01:12.72', -1, -1);
INSERT INTO idsmed.notification VALUES (2668, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Mamak  Special,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Mamak  Special</p><p>Company (For Vendor User): Mamak Medical Special</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-23 16:01:12.584', '2019-01-23 16:01:12.72', -1, -1);
INSERT INTO idsmed.notification VALUES (2670, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Mamak  Special,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Mamak  Special</p><p>Company (For Vendor User): Mamak Medical Special</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-23 16:01:12.598', '2019-01-23 16:01:12.72', -1, -1);
INSERT INTO idsmed.notification VALUES (2672, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Mamak  Special,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Mamak  Special</p><p>Company (For Vendor User): Mamak Medical Special</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-23 16:01:12.629', '2019-01-23 16:01:12.72', -1, -1);
INSERT INTO idsmed.notification VALUES (2674, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Mamak  Special,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Mamak  Special</p><p>Company (For Vendor User): Mamak Medical Special</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-23 16:01:12.659', '2019-01-23 16:01:12.721', -1, -1);
INSERT INTO idsmed.notification VALUES (2676, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Mamak  Special,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Mamak  Special</p><p>Company (For Vendor User): Mamak Medical Special</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-23 16:01:12.685', '2019-01-23 16:01:12.721', -1, -1);
INSERT INTO idsmed.notification VALUES (2678, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Mamak  Special,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Mamak  Special</p><p>Company (For Vendor User): Mamak Medical Special</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-23 16:01:12.712', '2019-01-23 16:01:12.721', -1, -1);
INSERT INTO idsmed.notification VALUES (2998, 1, 6, 'IDSMed – Request to Approve Company Registration', '<p>Hi Joey Ling,</p><p>A new vendor ly company 1 has registered with us and the information was duly reviewed  by the team.  Kindly login to the system to review/approve the application.</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-28 14:12:58.316', '2019-01-28 14:12:58.344', -1, -1);
INSERT INTO idsmed.notification VALUES (3017, 1, 6, 'IDSMed – Request to Approve Company Registration', '<p>Hi Joey Ling,</p><p>A new vendor ly1 has registered with us and the information was duly reviewed  by the team.  Kindly login to the system to review/approve the application.</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-28 14:21:03.237', '2019-01-28 14:21:03.277', -1, -1);
INSERT INTO idsmed.notification VALUES (3036, 1, 6, 'IDSMed – Request to Approve Company Registration', '<p>Hi Joey Ling,</p><p>A new vendor ly1 has registered with us and the information was duly reviewed  by the team.  Kindly login to the system to review/approve the application.</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-28 14:24:54.243', '2019-01-28 14:24:54.267', -1, -1);
INSERT INTO idsmed.notification VALUES (3055, 1, 6, 'IDSMed – Request to Approve Company Registration', '<p>Hi Joey Ling,</p><p>A new vendor ly c1 has registered with us and the information was duly reviewed  by the team.  Kindly login to the system to review/approve the application.</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-28 14:44:20.679', '2019-01-28 14:44:20.702', -1, -1);
INSERT INTO idsmed.notification VALUES (2684, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Kipsy Cana,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Kipsy Cana</p><p>Company (For Vendor User): Mamak Medical Special</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-24 10:49:14.685', '2019-01-24 10:49:14.786', -1, -1);
INSERT INTO idsmed.notification VALUES (2686, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Kipsy Cana,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Kipsy Cana</p><p>Company (For Vendor User): Mamak Medical Special</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-24 10:49:14.698', '2019-01-24 10:49:14.786', -1, -1);
INSERT INTO idsmed.notification VALUES (2688, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Kipsy Cana,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Kipsy Cana</p><p>Company (For Vendor User): Mamak Medical Special</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-24 10:49:14.705', '2019-01-24 10:49:14.786', -1, -1);
INSERT INTO idsmed.notification VALUES (2690, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Kipsy Cana,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Kipsy Cana</p><p>Company (For Vendor User): Mamak Medical Special</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-24 10:49:14.714', '2019-01-24 10:49:14.786', -1, -1);
INSERT INTO idsmed.notification VALUES (2692, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Kipsy Cana,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Kipsy Cana</p><p>Company (For Vendor User): Mamak Medical Special</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-24 10:49:14.727', '2019-01-24 10:49:14.786', -1, -1);
INSERT INTO idsmed.notification VALUES (2694, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Kipsy Cana,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Kipsy Cana</p><p>Company (For Vendor User): Mamak Medical Special</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-24 10:49:14.739', '2019-01-24 10:49:14.787', -1, -1);
INSERT INTO idsmed.notification VALUES (2696, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Kipsy Cana,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Kipsy Cana</p><p>Company (For Vendor User): Mamak Medical Special</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-24 10:49:14.75', '2019-01-24 10:49:14.787', -1, -1);
INSERT INTO idsmed.notification VALUES (2698, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Kipsy Cana,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Kipsy Cana</p><p>Company (For Vendor User): Mamak Medical Special</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-24 10:49:14.758', '2019-01-24 10:49:14.787', -1, -1);
INSERT INTO idsmed.notification VALUES (2700, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Kipsy Cana,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Kipsy Cana</p><p>Company (For Vendor User): Mamak Medical Special</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-24 10:49:14.77', '2019-01-24 10:49:14.787', -1, -1);
INSERT INTO idsmed.notification VALUES (2702, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Kipsy Cana,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Kipsy Cana</p><p>Company (For Vendor User): Mamak Medical Special</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-24 10:49:14.782', '2019-01-24 10:49:14.787', -1, -1);
INSERT INTO idsmed.notification VALUES (2706, 1, 6, 'IDSMed – Request to Approve Company Registration', '<p>Hi Steve Rogers,</p><p>A new vendor NewCom has registered with us and the information was duly reviewed  by the team.  Kindly login to the system to review/approve the application.</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-24 14:44:11.343', '2019-01-24 14:44:11.388', -1, -1);
INSERT INTO idsmed.notification VALUES (3074, 105, 2, 'IDSMed - Product Approval Notification', '<p>Hi,</p><p>We would like to inform you that a product has been approved under Torn City. Below is the summary of the product:</p><p>&nbsp;</p><p>Product Name: Test 1</p><p>Product Model: </p><p>Product Code: PRD-ECAT-00000127</p><p><br></p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-28 17:06:48.479', '2019-01-28 17:06:48.633', -1, -1);
INSERT INTO idsmed.notification VALUES (2833, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi ML PL,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: ML PL</p><p>Company (For Vendor User): abcMED</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-24 17:30:36.418', '2019-01-24 17:30:36.499', -1, -1);
INSERT INTO idsmed.notification VALUES (2835, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi ML PL,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: ML PL</p><p>Company (For Vendor User): abcMED</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-24 17:30:36.426', '2019-01-24 17:30:36.499', -1, -1);
INSERT INTO idsmed.notification VALUES (2724, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Wed Wed,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Wed Wed</p><p>Company (For Vendor User): NewCom</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-24 14:46:59.907', '2019-01-24 14:47:00.043', -1, -1);
INSERT INTO idsmed.notification VALUES (2726, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Wed Wed,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Wed Wed</p><p>Company (For Vendor User): NewCom</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-24 14:46:59.921', '2019-01-24 14:47:00.043', -1, -1);
INSERT INTO idsmed.notification VALUES (2728, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Wed Wed,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Wed Wed</p><p>Company (For Vendor User): NewCom</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-24 14:46:59.933', '2019-01-24 14:47:00.043', -1, -1);
INSERT INTO idsmed.notification VALUES (2730, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Wed Wed,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Wed Wed</p><p>Company (For Vendor User): NewCom</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-24 14:46:59.944', '2019-01-24 14:47:00.043', -1, -1);
INSERT INTO idsmed.notification VALUES (2732, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Wed Wed,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Wed Wed</p><p>Company (For Vendor User): NewCom</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-24 14:46:59.953', '2019-01-24 14:47:00.043', -1, -1);
INSERT INTO idsmed.notification VALUES (2734, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Wed Wed,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Wed Wed</p><p>Company (For Vendor User): NewCom</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-24 14:46:59.981', '2019-01-24 14:47:00.043', -1, -1);
INSERT INTO idsmed.notification VALUES (2736, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Wed Wed,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Wed Wed</p><p>Company (For Vendor User): NewCom</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-24 14:47:00', '2019-01-24 14:47:00.044', -1, -1);
INSERT INTO idsmed.notification VALUES (2738, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Wed Wed,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Wed Wed</p><p>Company (For Vendor User): NewCom</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-24 14:47:00.023', '2019-01-24 14:47:00.044', -1, -1);
INSERT INTO idsmed.notification VALUES (2740, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Wed Wed,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Wed Wed</p><p>Company (For Vendor User): NewCom</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-24 14:47:00.031', '2019-01-24 14:47:00.044', -1, -1);
INSERT INTO idsmed.notification VALUES (2742, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Wed Wed,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Wed Wed</p><p>Company (For Vendor User): NewCom</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-24 14:47:00.04', '2019-01-24 14:47:00.044', -1, -1);
INSERT INTO idsmed.notification VALUES (2744, 1, 8, 'IDSMed - User Registration Approval Notification', '<p>Hi asdasd asdada,</p><p>We would like to inform you the status of your account registration.</p><p>&nbsp;</p><p>Status: Rejected</p><p>Reason: asdasdas</p><p><br></p><p>Please login to the system and amend the necessary information @ My Profile before you resubmission.</p><p>&nbsp;</p><p><br></p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-24 16:18:54.759', '2019-01-24 16:18:54.761', -1, -1);
INSERT INTO idsmed.notification VALUES (2837, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi ML PL,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: ML PL</p><p>Company (For Vendor User): abcMED</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-24 17:30:36.431', '2019-01-24 17:30:36.499', -1, -1);
INSERT INTO idsmed.notification VALUES (2839, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi ML PL,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: ML PL</p><p>Company (For Vendor User): abcMED</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-24 17:30:36.441', '2019-01-24 17:30:36.499', -1, -1);
INSERT INTO idsmed.notification VALUES (2841, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi ML PL,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: ML PL</p><p>Company (For Vendor User): abcMED</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-24 17:30:36.446', '2019-01-24 17:30:36.499', -1, -1);
INSERT INTO idsmed.notification VALUES (2843, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi ML PL,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: ML PL</p><p>Company (For Vendor User): abcMED</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-24 17:30:36.465', '2019-01-24 17:30:36.499', -1, -1);
INSERT INTO idsmed.notification VALUES (2845, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi ML PL,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: ML PL</p><p>Company (For Vendor User): abcMED</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-24 17:30:36.471', '2019-01-24 17:30:36.499', -1, -1);
INSERT INTO idsmed.notification VALUES (2847, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi ML PL,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: ML PL</p><p>Company (For Vendor User): abcMED</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-24 17:30:36.481', '2019-01-24 17:30:36.499', -1, -1);
INSERT INTO idsmed.notification VALUES (2748, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Joey Ling,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Joey Ling</p><p>Company (For Vendor User): -</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-24 16:39:48.626', '2019-01-24 16:39:48.708', -1, -1);
INSERT INTO idsmed.notification VALUES (2750, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Joey Ling,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Joey Ling</p><p>Company (For Vendor User): -</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-24 16:39:48.634', '2019-01-24 16:39:48.708', -1, -1);
INSERT INTO idsmed.notification VALUES (2752, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Joey Ling,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Joey Ling</p><p>Company (For Vendor User): -</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-24 16:39:48.639', '2019-01-24 16:39:48.709', -1, -1);
INSERT INTO idsmed.notification VALUES (2754, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Joey Ling,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Joey Ling</p><p>Company (For Vendor User): -</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-24 16:39:48.647', '2019-01-24 16:39:48.709', -1, -1);
INSERT INTO idsmed.notification VALUES (2756, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Joey Ling,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Joey Ling</p><p>Company (For Vendor User): -</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-24 16:39:48.653', '2019-01-24 16:39:48.709', -1, -1);
INSERT INTO idsmed.notification VALUES (2758, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Joey Ling,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Joey Ling</p><p>Company (For Vendor User): -</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-24 16:39:48.659', '2019-01-24 16:39:48.709', -1, -1);
INSERT INTO idsmed.notification VALUES (2760, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Joey Ling,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Joey Ling</p><p>Company (For Vendor User): -</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-24 16:39:48.671', '2019-01-24 16:39:48.709', -1, -1);
INSERT INTO idsmed.notification VALUES (2762, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Joey Ling,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Joey Ling</p><p>Company (For Vendor User): -</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-24 16:39:48.682', '2019-01-24 16:39:48.709', -1, -1);
INSERT INTO idsmed.notification VALUES (2764, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Joey Ling,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Joey Ling</p><p>Company (For Vendor User): -</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-24 16:39:48.689', '2019-01-24 16:39:48.709', -1, -1);
INSERT INTO idsmed.notification VALUES (2766, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Joey Ling,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Joey Ling</p><p>Company (For Vendor User): -</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-24 16:39:48.696', '2019-01-24 16:39:48.709', -1, -1);
INSERT INTO idsmed.notification VALUES (2768, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Joey Ling,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Joey Ling</p><p>Company (For Vendor User): -</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-24 16:39:48.704', '2019-01-24 16:39:48.709', -1, -1);
INSERT INTO idsmed.notification VALUES (2849, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi ML PL,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: ML PL</p><p>Company (For Vendor User): abcMED</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-24 17:30:36.487', '2019-01-24 17:30:36.499', -1, -1);
INSERT INTO idsmed.notification VALUES (2851, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi ML PL,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: ML PL</p><p>Company (For Vendor User): abcMED</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-24 17:30:36.491', '2019-01-24 17:30:36.499', -1, -1);
INSERT INTO idsmed.notification VALUES (2853, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi ML PL,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: ML PL</p><p>Company (For Vendor User): abcMED</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-24 17:30:36.496', '2019-01-24 17:30:36.499', -1, -1);
INSERT INTO idsmed.notification VALUES (3122, 1, 6, 'IDSMed – Request to Approve Company Registration', '<p>Hi Joey Ling,</p><p>A new vendor Knights of Blood has registered with us and the information was duly reviewed  by the team.  Kindly login to the system to review/approve the application.</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-29 10:05:01.507', '2019-01-29 10:05:01.598', -1, -1);
INSERT INTO idsmed.notification VALUES (2874, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Nagisa Furukawa,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Nagisa Furukawa</p><p>Company (For Vendor User): Clannad After Story</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-25 12:25:04.914', '2019-01-25 12:25:04.968', -1, -1);
INSERT INTO idsmed.notification VALUES (2876, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Nagisa Furukawa,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Nagisa Furukawa</p><p>Company (For Vendor User): Clannad After Story</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-25 12:25:04.921', '2019-01-25 12:25:04.968', -1, -1);
INSERT INTO idsmed.notification VALUES (2878, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Nagisa Furukawa,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Nagisa Furukawa</p><p>Company (For Vendor User): Clannad After Story</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-25 12:25:04.927', '2019-01-25 12:25:04.968', -1, -1);
INSERT INTO idsmed.notification VALUES (2880, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Nagisa Furukawa,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Nagisa Furukawa</p><p>Company (For Vendor User): Clannad After Story</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-25 12:25:04.932', '2019-01-25 12:25:04.968', -1, -1);
INSERT INTO idsmed.notification VALUES (2882, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Nagisa Furukawa,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Nagisa Furukawa</p><p>Company (For Vendor User): Clannad After Story</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-25 12:25:04.937', '2019-01-25 12:25:04.968', -1, -1);
INSERT INTO idsmed.notification VALUES (2884, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Nagisa Furukawa,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Nagisa Furukawa</p><p>Company (For Vendor User): Clannad After Story</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-25 12:25:04.942', '2019-01-25 12:25:04.968', -1, -1);
INSERT INTO idsmed.notification VALUES (2886, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Nagisa Furukawa,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Nagisa Furukawa</p><p>Company (For Vendor User): Clannad After Story</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-25 12:25:04.946', '2019-01-25 12:25:04.968', -1, -1);
INSERT INTO idsmed.notification VALUES (2888, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Nagisa Furukawa,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Nagisa Furukawa</p><p>Company (For Vendor User): Clannad After Story</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-25 12:25:04.952', '2019-01-25 12:25:04.968', -1, -1);
INSERT INTO idsmed.notification VALUES (2890, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Nagisa Furukawa,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Nagisa Furukawa</p><p>Company (For Vendor User): Clannad After Story</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-25 12:25:04.956', '2019-01-25 12:25:04.968', -1, -1);
INSERT INTO idsmed.notification VALUES (2892, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Nagisa Furukawa,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Nagisa Furukawa</p><p>Company (For Vendor User): Clannad After Story</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-25 12:25:04.961', '2019-01-25 12:25:04.968', -1, -1);
INSERT INTO idsmed.notification VALUES (2894, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Nagisa Furukawa,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Nagisa Furukawa</p><p>Company (For Vendor User): Clannad After Story</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-25 12:25:04.966', '2019-01-25 12:25:04.968', -1, -1);
INSERT INTO idsmed.notification VALUES (3141, 1, 6, 'IDSMed – Request to Approve Company Registration', '<p>Hi Joey Ling,</p><p>A new vendor Laughing Coffin has registered with us and the information was duly reviewed  by the team.  Kindly login to the system to review/approve the application.</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-29 10:34:00.808', '2019-01-29 10:34:00.841', -1, -1);
INSERT INTO idsmed.notification VALUES (3226, 1, 6, 'IDSMed – Request to Approve Company Registration', '<p>Hi Joey Ling,</p><p>A new vendor vendor register has registered with us and the information was duly reviewed  by the team.  Kindly login to the system to review/approve the application.</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-29 11:18:17.781', '2019-01-29 11:18:17.81', -1, -1);
INSERT INTO idsmed.notification VALUES (3245, 1, 6, 'IDSMed – Request to Approve Company Registration', '<p>Hi Joey Ling,</p><p>A new vendor ly1 has registered with us and the information was duly reviewed  by the team.  Kindly login to the system to review/approve the application.</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-29 11:19:21.872', '2019-01-29 11:19:21.917', -1, -1);
INSERT INTO idsmed.notification VALUES (3264, 1, 6, 'IDSMed – Request to Approve Company Registration', '<p>Hi Joey Ling,</p><p>A new vendor company send grid has registered with us and the information was duly reviewed  by the team.  Kindly login to the system to review/approve the application.</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-29 11:52:54.433', '2019-01-29 11:52:54.463', -1, -1);
INSERT INTO idsmed.notification VALUES (3283, 1, 6, 'IDSMed – Request to Approve Company Registration', '<p>Hi Joey Ling,</p><p>A new vendor Rising of Shield Hero has registered with us and the information was duly reviewed  by the team.  Kindly login to the system to review/approve the application.</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-29 11:58:33.713', '2019-01-29 11:58:33.777', -1, -1);
INSERT INTO idsmed.notification VALUES (2954, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Laksa Boi,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Laksa Boi</p><p>Company (For Vendor User): Mamak Medical Special</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-25 16:14:53.748', '2019-01-25 16:14:53.912', -1, -1);
INSERT INTO idsmed.notification VALUES (2956, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Laksa Boi,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Laksa Boi</p><p>Company (For Vendor User): Mamak Medical Special</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-25 16:14:53.755', '2019-01-25 16:14:53.912', -1, -1);
INSERT INTO idsmed.notification VALUES (2958, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Laksa Boi,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Laksa Boi</p><p>Company (For Vendor User): Mamak Medical Special</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-25 16:14:53.769', '2019-01-25 16:14:53.912', -1, -1);
INSERT INTO idsmed.notification VALUES (2960, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Laksa Boi,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Laksa Boi</p><p>Company (For Vendor User): Mamak Medical Special</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-25 16:14:53.776', '2019-01-25 16:14:53.912', -1, -1);
INSERT INTO idsmed.notification VALUES (2962, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Laksa Boi,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Laksa Boi</p><p>Company (For Vendor User): Mamak Medical Special</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-25 16:14:53.792', '2019-01-25 16:14:53.912', -1, -1);
INSERT INTO idsmed.notification VALUES (2964, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Laksa Boi,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Laksa Boi</p><p>Company (For Vendor User): Mamak Medical Special</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-25 16:14:53.802', '2019-01-25 16:14:53.912', -1, -1);
INSERT INTO idsmed.notification VALUES (2966, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Laksa Boi,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Laksa Boi</p><p>Company (For Vendor User): Mamak Medical Special</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-25 16:14:53.836', '2019-01-25 16:14:53.912', -1, -1);
INSERT INTO idsmed.notification VALUES (2968, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Laksa Boi,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Laksa Boi</p><p>Company (For Vendor User): Mamak Medical Special</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-25 16:14:53.844', '2019-01-25 16:14:53.912', -1, -1);
INSERT INTO idsmed.notification VALUES (2970, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Laksa Boi,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Laksa Boi</p><p>Company (For Vendor User): Mamak Medical Special</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-25 16:14:53.855', '2019-01-25 16:14:53.912', -1, -1);
INSERT INTO idsmed.notification VALUES (2972, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Laksa Boi,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Laksa Boi</p><p>Company (For Vendor User): Mamak Medical Special</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-25 16:14:53.875', '2019-01-25 16:14:53.912', -1, -1);
INSERT INTO idsmed.notification VALUES (2974, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Laksa Boi,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Laksa Boi</p><p>Company (For Vendor User): Mamak Medical Special</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-25 16:14:53.909', '2019-01-25 16:14:53.912', -1, -1);
INSERT INTO idsmed.notification VALUES (3160, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi ML Lee,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: ML Lee</p><p>Company (For Vendor User): abcMED</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-29 10:36:30.895', '2019-01-29 10:36:31.033', -1, -1);
INSERT INTO idsmed.notification VALUES (3162, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi ML Lee,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: ML Lee</p><p>Company (For Vendor User): abcMED</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-29 10:36:30.91', '2019-01-29 10:36:31.033', -1, -1);
INSERT INTO idsmed.notification VALUES (3164, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi ML Lee,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: ML Lee</p><p>Company (For Vendor User): abcMED</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-29 10:36:30.916', '2019-01-29 10:36:31.033', -1, -1);
INSERT INTO idsmed.notification VALUES (3166, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi ML Lee,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: ML Lee</p><p>Company (For Vendor User): abcMED</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-29 10:36:30.943', '2019-01-29 10:36:31.033', -1, -1);
INSERT INTO idsmed.notification VALUES (3182, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Knight Mamak,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Knight Mamak</p><p>Company (For Vendor User): Knights of Blood</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-29 10:47:50.241', '2019-01-29 10:47:50.364', -1, -1);
INSERT INTO idsmed.notification VALUES (3184, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Knight Mamak,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Knight Mamak</p><p>Company (For Vendor User): Knights of Blood</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-29 10:47:50.253', '2019-01-29 10:47:50.364', -1, -1);
INSERT INTO idsmed.notification VALUES (3186, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Knight Mamak,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Knight Mamak</p><p>Company (For Vendor User): Knights of Blood</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-29 10:47:50.276', '2019-01-29 10:47:50.364', -1, -1);
INSERT INTO idsmed.notification VALUES (3188, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Knight Mamak,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Knight Mamak</p><p>Company (For Vendor User): Knights of Blood</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-29 10:47:50.288', '2019-01-29 10:47:50.364', -1, -1);
INSERT INTO idsmed.notification VALUES (3190, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Knight Mamak,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Knight Mamak</p><p>Company (For Vendor User): Knights of Blood</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-29 10:47:50.307', '2019-01-29 10:47:50.364', -1, -1);
INSERT INTO idsmed.notification VALUES (3192, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Knight Mamak,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Knight Mamak</p><p>Company (For Vendor User): Knights of Blood</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-29 10:47:50.314', '2019-01-29 10:47:50.365', -1, -1);
INSERT INTO idsmed.notification VALUES (3194, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Knight Mamak,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Knight Mamak</p><p>Company (For Vendor User): Knights of Blood</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-29 10:47:50.321', '2019-01-29 10:47:50.365', -1, -1);
INSERT INTO idsmed.notification VALUES (3196, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Knight Mamak,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Knight Mamak</p><p>Company (For Vendor User): Knights of Blood</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-29 10:47:50.327', '2019-01-29 10:47:50.365', -1, -1);
INSERT INTO idsmed.notification VALUES (3198, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Knight Mamak,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Knight Mamak</p><p>Company (For Vendor User): Knights of Blood</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-29 10:47:50.339', '2019-01-29 10:47:50.365', -1, -1);
INSERT INTO idsmed.notification VALUES (3200, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Knight Mamak,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Knight Mamak</p><p>Company (For Vendor User): Knights of Blood</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-29 10:47:50.352', '2019-01-29 10:47:50.365', -1, -1);
INSERT INTO idsmed.notification VALUES (3202, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Knight Mamak,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Knight Mamak</p><p>Company (For Vendor User): Knights of Blood</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-29 10:47:50.361', '2019-01-29 10:47:50.365', -1, -1);
INSERT INTO idsmed.notification VALUES (3204, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Blood Knight,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Blood Knight</p><p>Company (For Vendor User): Knights of Blood</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-29 10:59:54.822', '2019-01-29 10:59:54.916', -1, -1);
INSERT INTO idsmed.notification VALUES (3206, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Blood Knight,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Blood Knight</p><p>Company (For Vendor User): Knights of Blood</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-29 10:59:54.83', '2019-01-29 10:59:54.916', -1, -1);
INSERT INTO idsmed.notification VALUES (3208, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Blood Knight,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Blood Knight</p><p>Company (For Vendor User): Knights of Blood</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-29 10:59:54.836', '2019-01-29 10:59:54.916', -1, -1);
INSERT INTO idsmed.notification VALUES (3210, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Blood Knight,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Blood Knight</p><p>Company (For Vendor User): Knights of Blood</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-29 10:59:54.843', '2019-01-29 10:59:54.916', -1, -1);
INSERT INTO idsmed.notification VALUES (3212, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Blood Knight,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Blood Knight</p><p>Company (For Vendor User): Knights of Blood</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-29 10:59:54.851', '2019-01-29 10:59:54.916', -1, -1);
INSERT INTO idsmed.notification VALUES (3214, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Blood Knight,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Blood Knight</p><p>Company (For Vendor User): Knights of Blood</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-29 10:59:54.857', '2019-01-29 10:59:54.916', -1, -1);
INSERT INTO idsmed.notification VALUES (3216, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Blood Knight,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Blood Knight</p><p>Company (For Vendor User): Knights of Blood</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-29 10:59:54.874', '2019-01-29 10:59:54.916', -1, -1);
INSERT INTO idsmed.notification VALUES (3218, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Blood Knight,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Blood Knight</p><p>Company (For Vendor User): Knights of Blood</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-29 10:59:54.882', '2019-01-29 10:59:54.916', -1, -1);
INSERT INTO idsmed.notification VALUES (3220, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Blood Knight,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Blood Knight</p><p>Company (For Vendor User): Knights of Blood</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-29 10:59:54.888', '2019-01-29 10:59:54.916', -1, -1);
INSERT INTO idsmed.notification VALUES (3222, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Blood Knight,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Blood Knight</p><p>Company (For Vendor User): Knights of Blood</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-29 10:59:54.903', '2019-01-29 10:59:54.916', -1, -1);
INSERT INTO idsmed.notification VALUES (3224, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Blood Knight,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Blood Knight</p><p>Company (For Vendor User): Knights of Blood</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-29 10:59:54.912', '2019-01-29 10:59:54.916', -1, -1);
INSERT INTO idsmed.notification VALUES (3302, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Alice  Synthesis Thirty,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Alice  Synthesis Thirty</p><p>Company (For Vendor User): -</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-29 12:24:03.193', '2019-01-29 12:24:03.287', -1, -1);
INSERT INTO idsmed.notification VALUES (3304, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Alice  Synthesis Thirty,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Alice  Synthesis Thirty</p><p>Company (For Vendor User): -</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-29 12:24:03.202', '2019-01-29 12:24:03.287', -1, -1);
INSERT INTO idsmed.notification VALUES (3306, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Alice  Synthesis Thirty,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Alice  Synthesis Thirty</p><p>Company (For Vendor User): -</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-29 12:24:03.209', '2019-01-29 12:24:03.287', -1, -1);
INSERT INTO idsmed.notification VALUES (3308, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Alice  Synthesis Thirty,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Alice  Synthesis Thirty</p><p>Company (For Vendor User): -</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-29 12:24:03.214', '2019-01-29 12:24:03.287', -1, -1);
INSERT INTO idsmed.notification VALUES (3310, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Alice  Synthesis Thirty,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Alice  Synthesis Thirty</p><p>Company (For Vendor User): -</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-29 12:24:03.219', '2019-01-29 12:24:03.287', -1, -1);
INSERT INTO idsmed.notification VALUES (3312, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Alice  Synthesis Thirty,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Alice  Synthesis Thirty</p><p>Company (For Vendor User): -</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-29 12:24:03.232', '2019-01-29 12:24:03.287', -1, -1);
INSERT INTO idsmed.notification VALUES (3314, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Alice  Synthesis Thirty,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Alice  Synthesis Thirty</p><p>Company (For Vendor User): -</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-29 12:24:03.241', '2019-01-29 12:24:03.287', -1, -1);
INSERT INTO idsmed.notification VALUES (3316, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Alice  Synthesis Thirty,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Alice  Synthesis Thirty</p><p>Company (For Vendor User): -</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-29 12:24:03.246', '2019-01-29 12:24:03.287', -1, -1);
INSERT INTO idsmed.notification VALUES (3318, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Alice  Synthesis Thirty,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Alice  Synthesis Thirty</p><p>Company (For Vendor User): -</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-29 12:24:03.254', '2019-01-29 12:24:03.287', -1, -1);
INSERT INTO idsmed.notification VALUES (3320, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Alice  Synthesis Thirty,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Alice  Synthesis Thirty</p><p>Company (For Vendor User): -</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-29 12:24:03.272', '2019-01-29 12:24:03.287', -1, -1);
INSERT INTO idsmed.notification VALUES (3322, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Alice  Synthesis Thirty,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Alice  Synthesis Thirty</p><p>Company (For Vendor User): -</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-29 12:24:03.28', '2019-01-29 12:24:03.287', -1, -1);
INSERT INTO idsmed.notification VALUES (3324, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Alice  Synthesis Thirty,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Alice  Synthesis Thirty</p><p>Company (For Vendor User): -</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-29 12:24:03.284', '2019-01-29 12:24:03.287', -1, -1);
INSERT INTO idsmed.notification VALUES (3326, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Naofumi Iwatani,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Naofumi Iwatani</p><p>Company (For Vendor User): Rising of Shield Hero</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-29 12:37:05.197', '2019-01-29 12:37:05.302', -1, -1);
INSERT INTO idsmed.notification VALUES (3328, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Naofumi Iwatani,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Naofumi Iwatani</p><p>Company (For Vendor User): Rising of Shield Hero</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-29 12:37:05.203', '2019-01-29 12:37:05.302', -1, -1);
INSERT INTO idsmed.notification VALUES (3330, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Naofumi Iwatani,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Naofumi Iwatani</p><p>Company (For Vendor User): Rising of Shield Hero</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-29 12:37:05.208', '2019-01-29 12:37:05.302', -1, -1);
INSERT INTO idsmed.notification VALUES (3332, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Naofumi Iwatani,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Naofumi Iwatani</p><p>Company (For Vendor User): Rising of Shield Hero</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-29 12:37:05.213', '2019-01-29 12:37:05.302', -1, -1);
INSERT INTO idsmed.notification VALUES (3334, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Naofumi Iwatani,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Naofumi Iwatani</p><p>Company (For Vendor User): Rising of Shield Hero</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-29 12:37:05.219', '2019-01-29 12:37:05.302', -1, -1);
INSERT INTO idsmed.notification VALUES (3336, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Naofumi Iwatani,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Naofumi Iwatani</p><p>Company (For Vendor User): Rising of Shield Hero</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-29 12:37:05.231', '2019-01-29 12:37:05.302', -1, -1);
INSERT INTO idsmed.notification VALUES (3338, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Naofumi Iwatani,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Naofumi Iwatani</p><p>Company (For Vendor User): Rising of Shield Hero</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-29 12:37:05.236', '2019-01-29 12:37:05.302', -1, -1);
INSERT INTO idsmed.notification VALUES (3340, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Naofumi Iwatani,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Naofumi Iwatani</p><p>Company (For Vendor User): Rising of Shield Hero</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-29 12:37:05.241', '2019-01-29 12:37:05.302', -1, -1);
INSERT INTO idsmed.notification VALUES (3342, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Naofumi Iwatani,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Naofumi Iwatani</p><p>Company (For Vendor User): Rising of Shield Hero</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-29 12:37:05.257', '2019-01-29 12:37:05.302', -1, -1);
INSERT INTO idsmed.notification VALUES (3344, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Naofumi Iwatani,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Naofumi Iwatani</p><p>Company (For Vendor User): Rising of Shield Hero</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-29 12:37:05.277', '2019-01-29 12:37:05.302', -1, -1);
INSERT INTO idsmed.notification VALUES (3346, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Naofumi Iwatani,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Naofumi Iwatani</p><p>Company (For Vendor User): Rising of Shield Hero</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-29 12:37:05.288', '2019-01-29 12:37:05.302', -1, -1);
INSERT INTO idsmed.notification VALUES (3348, 105, 1, 'IDSMed - New User Registration Notification', '<p>Hi Naofumi Iwatani,</p><p>We would like to inform you that a new user''s account registration has been approved.</p><p>Below is the information of the account:</p><p><br></p><p>Username: Naofumi Iwatani</p><p>Company (For Vendor User): Rising of Shield Hero</p><p>&nbsp;</p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 0, '2019-01-29 12:37:05.3', '2019-01-29 12:37:05.302', -1, -1);


--
-- TOC entry 4116 (class 0 OID 0)
-- Dependencies: 244
-- Name: notification_id_seq; Type: SEQUENCE SET; Schema: idsmed; Owner: dbadmin
--

SELECT pg_catalog.setval('idsmed.notification_id_seq', 3369, true);


--
-- TOC entry 3861 (class 0 OID 97576)
-- Dependencies: 245
-- Data for Name: notification_recipient; Type: TABLE DATA; Schema: idsmed; Owner: dbadmin
--



--
-- TOC entry 4117 (class 0 OID 0)
-- Dependencies: 246
-- Name: notification_recipient_id_seq; Type: SEQUENCE SET; Schema: idsmed; Owner: dbadmin
--

SELECT pg_catalog.setval('idsmed.notification_recipient_id_seq', 1, false);


--
-- TOC entry 3863 (class 0 OID 97586)
-- Dependencies: 247
-- Data for Name: notification_wildcard; Type: TABLE DATA; Schema: idsmed; Owner: dbadmin
--

INSERT INTO idsmed.notification_wildcard VALUES (1, 'Username', '[#USER_NAME]', '2018-12-20 15:45:30.491944', '2018-12-20 15:45:30.491944', 1, 1, 3);
INSERT INTO idsmed.notification_wildcard VALUES (2, 'Login ID', '[#USER_ID]', '2018-12-20 15:45:30.491944', '2018-12-20 15:45:30.491944', 1, 1, 3);
INSERT INTO idsmed.notification_wildcard VALUES (3, 'User Approval Status', '[#USER_APP_STATUS]', '2018-12-20 15:45:30.491944', '2018-12-20 15:45:30.491944', 1, 1, 3);
INSERT INTO idsmed.notification_wildcard VALUES (4, 'User Reject Reason', '[#USER_REJECT_REASON]', '2018-12-20 15:45:30.491944', '2018-12-20 15:45:30.491944', 1, 1, 3);
INSERT INTO idsmed.notification_wildcard VALUES (5, 'Vendor Name', '[#VENDOR_NAME]', '2018-12-20 15:45:30.491944', '2018-12-20 15:45:30.491944', 1, 1, 3);
INSERT INTO idsmed.notification_wildcard VALUES (6, 'Company Code', '[#COMPANY_CODE]', '2018-12-20 15:45:30.491944', '2018-12-20 15:45:30.491944', 1, 1, 3);
INSERT INTO idsmed.notification_wildcard VALUES (7, 'Vendor Approval Status', '[#VENDOR_APP_STATUS]', '2018-12-20 15:45:30.491944', '2018-12-20 15:45:30.491944', 1, 1, 3);
INSERT INTO idsmed.notification_wildcard VALUES (8, 'Vendor Reject Reason', '[#VENDOR_REJECT_REASON]', '2018-12-20 15:45:30.491944', '2018-12-20 15:45:30.491944', 1, 1, 3);
INSERT INTO idsmed.notification_wildcard VALUES (9, 'Vendor Expiry Date', '[#VENDOR_EXP_DATE]', '2018-12-20 15:45:30.491944', '2018-12-20 15:45:30.491944', 1, 1, 3);
INSERT INTO idsmed.notification_wildcard VALUES (10, 'Product Name', '[#PRODUCT_NAME]', '2018-12-20 15:45:30.491944', '2018-12-20 15:45:30.491944', 1, 1, 3);
INSERT INTO idsmed.notification_wildcard VALUES (11, 'Product Model', '[#PRODUCT_MODEL]', '2018-12-20 15:45:30.491944', '2018-12-20 15:45:30.491944', 1, 1, 3);
INSERT INTO idsmed.notification_wildcard VALUES (12, 'Product Code', '[#PRODUCT_CODE]', '2018-12-20 15:45:30.491944', '2018-12-20 15:45:30.491944', 1, 1, 3);
INSERT INTO idsmed.notification_wildcard VALUES (13, 'Product Approval Status', '[#PRODUCT_APP_STATUS]', '2018-12-20 15:45:30.491944', '2018-12-20 15:45:30.491944', 1, 1, 3);
INSERT INTO idsmed.notification_wildcard VALUES (14, 'Product Reject Reason', '[#PRODUCT_REJECT_REASON]', '2018-12-20 15:45:30.491944', '2018-12-20 15:45:30.491944', 1, 1, 3);
INSERT INTO idsmed.notification_wildcard VALUES (15, 'Product Expiry date', '[#PRODUCT_EXP_DATE]', '2018-12-20 15:45:30.491944', '2018-12-20 15:45:30.491944', 1, 1, 3);
INSERT INTO idsmed.notification_wildcard VALUES (16, 'Product Batch Upload Filename', '[#PRODUCT_BATCH_UPLOAD_FILENAME]', '2018-12-20 15:45:30.491944', '2018-12-20 15:45:30.491944', 1, 1, 3);
INSERT INTO idsmed.notification_wildcard VALUES (17, 'Product Batch Upload Result', '[#PRODUCT_BATCH_UPLOAD_RESULT]', '2018-12-20 15:45:30.491944', '2018-12-20 15:45:30.491944', 1, 1, 3);
INSERT INTO idsmed.notification_wildcard VALUES (18, 'SCP Domain URL', '[#SCP_URL]', '2018-12-20 15:45:30.491944', '2018-12-20 15:45:30.491944', 1, 1, 3);
INSERT INTO idsmed.notification_wildcard VALUES (20, 'Vendor Document Name', '[#VENDOR_DOCUMENT_NAME]', '2018-12-20 15:45:30.491944', '2018-12-20 15:45:30.491944', 1, 1, 3);
INSERT INTO idsmed.notification_wildcard VALUES (21, 'Vendor Document Expiry Date', '[#VENDOR_DOCUMENT_EXP_DATE]', '2018-12-20 15:45:30.491944', '2018-12-20 15:45:30.491944', 1, 1, 3);
INSERT INTO idsmed.notification_wildcard VALUES (22, 'Vendor Document Approval Status', '[#VENDOR_DOCUMENT_APP_STATUS]', '2018-12-20 15:45:30.491944', '2018-12-20 15:45:30.491944', 1, 1, 3);
INSERT INTO idsmed.notification_wildcard VALUES (23, 'Product Document Name', '[#PRODUCT_DOCUMENT_NAME]', '2018-12-20 15:45:30.491944', '2018-12-20 15:45:30.491944', 1, 1, 3);
INSERT INTO idsmed.notification_wildcard VALUES (24, 'Product Document Expiry Date', '[#PRODUCT_DOCUMENT_EXP_DATE]', '2018-12-20 15:45:30.491944', '2018-12-20 15:45:30.491944', 1, 1, 3);
INSERT INTO idsmed.notification_wildcard VALUES (25, 'Product Document Approval Status', '[#PRODUCT_DOCUMENT_APP_STATUS]', '2018-12-20 15:45:30.491944', '2018-12-20 15:45:30.491944', 1, 1, 3);
INSERT INTO idsmed.notification_wildcard VALUES (26, 'Recipient Name', '[#RECIPIENT_NAME]', '2018-12-20 15:45:30.491944', '2018-12-20 15:45:30.491944', 1, 1, 3);
INSERT INTO idsmed.notification_wildcard VALUES (27, 'Sender Name', '[#SENDER_NAME]', '2018-12-20 15:45:30.491944', '2018-12-20 15:45:30.491944', 1, 1, 3);
INSERT INTO idsmed.notification_wildcard VALUES (28, 'User Default Password', '[#USER_DEFAULT_PASSWORD]', '2018-12-31 17:10:50.824868', '2018-12-31 17:10:50.824868', 1, 1, 3);
INSERT INTO idsmed.notification_wildcard VALUES (19, 'Vendor Registration Resubmit URL', '[#VENDOR_RESUBMIT_PROFILE_URL]', '2018-12-20 15:45:30.491944', '2018-12-20 15:45:30.491944', 1, 1, 3);
INSERT INTO idsmed.notification_wildcard VALUES (29, 'User Registration Resubmit URL', '[#USER_REGISTRATION_RESUBMIT_URL]', '2019-01-28 14:46:37.724983', '2019-01-28 14:46:37.724983', 1, 1, 3);


--
-- TOC entry 4118 (class 0 OID 0)
-- Dependencies: 248
-- Name: notification_wildcard_id_seq; Type: SEQUENCE SET; Schema: idsmed; Owner: dbadmin
--

SELECT pg_catalog.setval('idsmed.notification_wildcard_id_seq', 29, true);


--
-- TOC entry 3865 (class 0 OID 97596)
-- Dependencies: 249
-- Data for Name: od_big_order; Type: TABLE DATA; Schema: idsmed; Owner: dbadmin
--



--
-- TOC entry 4119 (class 0 OID 0)
-- Dependencies: 250
-- Name: od_big_order_id_seq; Type: SEQUENCE SET; Schema: idsmed; Owner: dbadmin
--

SELECT pg_catalog.setval('idsmed.od_big_order_id_seq', 1, false);


--
-- TOC entry 3867 (class 0 OID 97606)
-- Dependencies: 251
-- Data for Name: od_delivery_product; Type: TABLE DATA; Schema: idsmed; Owner: dbadmin
--



--
-- TOC entry 4120 (class 0 OID 0)
-- Dependencies: 252
-- Name: od_delivery_product_id_seq; Type: SEQUENCE SET; Schema: idsmed; Owner: dbadmin
--

SELECT pg_catalog.setval('idsmed.od_delivery_product_id_seq', 1, false);


--
-- TOC entry 3869 (class 0 OID 97616)
-- Dependencies: 253
-- Data for Name: od_order_delivery; Type: TABLE DATA; Schema: idsmed; Owner: dbadmin
--



--
-- TOC entry 4121 (class 0 OID 0)
-- Dependencies: 254
-- Name: od_order_delivery_id_seq; Type: SEQUENCE SET; Schema: idsmed; Owner: dbadmin
--

SELECT pg_catalog.setval('idsmed.od_order_delivery_id_seq', 1, false);


--
-- TOC entry 3871 (class 0 OID 97626)
-- Dependencies: 255
-- Data for Name: od_order_delivery_record; Type: TABLE DATA; Schema: idsmed; Owner: dbadmin
--



--
-- TOC entry 4122 (class 0 OID 0)
-- Dependencies: 256
-- Name: od_order_delivery_record_id_seq; Type: SEQUENCE SET; Schema: idsmed; Owner: dbadmin
--

SELECT pg_catalog.setval('idsmed.od_order_delivery_record_id_seq', 1, false);


--
-- TOC entry 3873 (class 0 OID 97639)
-- Dependencies: 257
-- Data for Name: od_order_item; Type: TABLE DATA; Schema: idsmed; Owner: dbadmin
--



--
-- TOC entry 4123 (class 0 OID 0)
-- Dependencies: 258
-- Name: od_order_item_id_seq; Type: SEQUENCE SET; Schema: idsmed; Owner: dbadmin
--

SELECT pg_catalog.setval('idsmed.od_order_item_id_seq', 1, false);


--
-- TOC entry 3875 (class 0 OID 97652)
-- Dependencies: 259
-- Data for Name: od_order_receive_address; Type: TABLE DATA; Schema: idsmed; Owner: dbadmin
--



--
-- TOC entry 4124 (class 0 OID 0)
-- Dependencies: 260
-- Name: od_order_receive_address_id_seq; Type: SEQUENCE SET; Schema: idsmed; Owner: dbadmin
--

SELECT pg_catalog.setval('idsmed.od_order_receive_address_id_seq', 1, false);


--
-- TOC entry 3877 (class 0 OID 97665)
-- Dependencies: 261
-- Data for Name: od_small_order; Type: TABLE DATA; Schema: idsmed; Owner: dbadmin
--



--
-- TOC entry 4125 (class 0 OID 0)
-- Dependencies: 262
-- Name: od_small_order_id_seq; Type: SEQUENCE SET; Schema: idsmed; Owner: dbadmin
--

SELECT pg_catalog.setval('idsmed.od_small_order_id_seq', 1, false);


--
-- TOC entry 3879 (class 0 OID 97675)
-- Dependencies: 263
-- Data for Name: product; Type: TABLE DATA; Schema: idsmed; Owner: dbadmin
--



--
-- TOC entry 3880 (class 0 OID 97689)
-- Dependencies: 264
-- Data for Name: product_brand; Type: TABLE DATA; Schema: idsmed; Owner: dbadmin
--

INSERT INTO idsmed.product_brand VALUES (431, 'Others', '845', NULL, '2018-10-03 19:45:08.743666', '2018-10-03 19:45:08.743666', 3, 3, 3, NULL);


--
-- TOC entry 4126 (class 0 OID 0)
-- Dependencies: 265
-- Name: product_brand_id_seq; Type: SEQUENCE SET; Schema: idsmed; Owner: dbadmin
--

SELECT pg_catalog.setval('idsmed.product_brand_id_seq', 674, true);


--
-- TOC entry 3882 (class 0 OID 97702)
-- Dependencies: 266
-- Data for Name: product_care_area; Type: TABLE DATA; Schema: idsmed; Owner: dbadmin
--

INSERT INTO idsmed.product_care_area VALUES (12, '医疗技术', NULL, NULL, NULL, NULL, '2018-08-24 15:48:11.044347', '2018-08-24 15:48:11.044347', 1, 1, 3, 'Medical IT', NULL, 'http://idsmeddev.oss-cn-hongkong.aliyuncs.com/idsmedmedia/product/product_carearea/Medical_IT.png', 'http://idsmeddev.oss-cn-hongkong.aliyuncs.com/idsmedmedia/product/wedoctor_product_carearea/Medical_IT.png');
INSERT INTO idsmed.product_care_area VALUES (13, '医疗耗材', NULL, NULL, NULL, NULL, '2018-08-24 15:48:11.044347', '2018-08-24 15:48:11.044347', 1, 1, 3, 'Medical Consumables', NULL, 'http://idsmeddev.oss-cn-hongkong.aliyuncs.com/idsmedmedia/product/product_carearea/Medical_Consumables.png', 'http://idsmeddev.oss-cn-hongkong.aliyuncs.com/idsmedmedia/product/wedoctor_product_carearea/Medical_Consumables.png');
INSERT INTO idsmed.product_care_area VALUES (14, '实验', NULL, NULL, NULL, NULL, '2018-08-24 15:48:11.044347', '2018-08-24 15:48:11.044347', 1, 1, 3, 'Laboratory', NULL, 'http://idsmeddev.oss-cn-hongkong.aliyuncs.com/idsmedmedia/product/product_carearea/Laboratory.png', 'http://idsmeddev.oss-cn-hongkong.aliyuncs.com/idsmedmedia/product/wedoctor_product_carearea/Laboratory.png');
INSERT INTO idsmed.product_care_area VALUES (26, '生物医学服务', NULL, NULL, NULL, NULL, '2018-08-24 15:48:11.044347', '2018-08-24 15:48:11.044347', 1, 1, 3, 'Bio-Medical Services', NULL, 'http://idsmeddev.oss-cn-hongkong.aliyuncs.com/idsmedmedia/product/product_carearea/Bio_Medical_Services.png', 'http://idsmeddev.oss-cn-hongkong.aliyuncs.com/idsmedmedia/product/wedoctor_product_carearea/Bio_Medical_Services.png');
INSERT INTO idsmed.product_care_area VALUES (27, '麻醉学', NULL, NULL, NULL, NULL, '2018-08-24 15:48:11.044347', '2018-08-24 15:48:11.044347', 1, 1, 3, 'Anesthesiology & Analgesia', NULL, 'http://idsmeddev.oss-cn-hongkong.aliyuncs.com/idsmedmedia/product/product_carearea/Anesthesiology_and_Analgesia.png', 'http://idsmeddev.oss-cn-hongkong.aliyuncs.com/idsmedmedia/product/wedoctor_product_carearea/Anesthesiology_and_Analgesia.png');
INSERT INTO idsmed.product_care_area VALUES (28, '美容', NULL, NULL, NULL, NULL, '2018-08-24 15:48:11.044347', '2018-08-24 15:48:11.044347', 1, 1, 3, 'Aesthetic', NULL, 'http://idsmeddev.oss-cn-hongkong.aliyuncs.com/idsmedmedia/product/product_carearea/Aesthetic.png', 'http://idsmeddev.oss-cn-hongkong.aliyuncs.com/idsmedmedia/product/wedoctor_product_carearea/Aesthetic.png');
INSERT INTO idsmed.product_care_area VALUES (30, '医疗设施', NULL, NULL, NULL, NULL, '2018-10-03 19:45:37.119176', '2018-10-03 19:45:37.119176', 1, 1, 3, 'Medical Facility', NULL, 'http://idsmeddev.oss-cn-hongkong.aliyuncs.com/idsmedmedia/product/product_carearea/Medical_Facility.png', 'http://idsmeddev.oss-cn-hongkong.aliyuncs.com/idsmedmedia/product/wedoctor_product_carearea/Medical_Facility.png');
INSERT INTO idsmed.product_care_area VALUES (29, '其他', NULL, NULL, NULL, NULL, '2018-08-24 15:48:11.044347', '2018-08-24 15:48:11.044347', 1, 1, 3, 'OTHERS', NULL, NULL, NULL);
INSERT INTO idsmed.product_care_area VALUES (15, '重症监护', NULL, NULL, NULL, NULL, '2018-08-24 15:48:11.044347', '2018-08-24 15:48:11.044347', 1, 1, 3, 'Intensive Care', NULL, 'http://idsmeddev.oss-cn-hongkong.aliyuncs.com/idsmedmedia/product/product_carearea/Intensive_Care.png', 'http://idsmeddev.oss-cn-hongkong.aliyuncs.com/idsmedmedia/product/wedoctor_product_carearea/Intensive_Care.png');
INSERT INTO idsmed.product_care_area VALUES (16, '感染控制', NULL, NULL, NULL, NULL, '2018-08-24 15:48:11.044347', '2018-08-24 15:48:11.044347', 1, 1, 3, 'Infection control', NULL, 'http://idsmeddev.oss-cn-hongkong.aliyuncs.com/idsmedmedia/product/product_carearea/Infection_Control.png', 'http://idsmeddev.oss-cn-hongkong.aliyuncs.com/idsmedmedia/product/wedoctor_product_carearea/Infection_Control.png');
INSERT INTO idsmed.product_care_area VALUES (17, '保健教育', NULL, NULL, NULL, NULL, '2018-08-24 15:48:11.044347', '2018-08-24 15:48:11.044347', 1, 1, 3, 'Healthcare Education', NULL, 'http://idsmeddev.oss-cn-hongkong.aliyuncs.com/idsmedmedia/product/product_carearea/Healthcare_Education.png', 'http://idsmeddev.oss-cn-hongkong.aliyuncs.com/idsmedmedia/product/wedoctor_product_carearea/Healthcare_Education.png');
INSERT INTO idsmed.product_care_area VALUES (18, '老人药学', NULL, NULL, NULL, NULL, '2018-08-24 15:48:11.044347', '2018-08-24 15:48:11.044347', 1, 1, 3, 'Geriatric Medicine', NULL, 'http://idsmeddev.oss-cn-hongkong.aliyuncs.com/idsmedmedia/product/product_carearea/Geriatric_Medicine.png', 'http://idsmeddev.oss-cn-hongkong.aliyuncs.com/idsmedmedia/product/wedoctor_product_carearea/Geriatric_Medicine.png');
INSERT INTO idsmed.product_care_area VALUES (19, '普通手术', NULL, NULL, NULL, NULL, '2018-08-24 15:48:11.044347', '2018-08-24 15:48:11.044347', 1, 1, 3, 'General Surgery', NULL, 'http://idsmeddev.oss-cn-hongkong.aliyuncs.com/idsmedmedia/product/product_carearea/General_Surgery.png', 'http://idsmeddev.oss-cn-hongkong.aliyuncs.com/idsmedmedia/product/wedoctor_product_carearea/General_Surgery.png');
INSERT INTO idsmed.product_care_area VALUES (20, '肠胃科', NULL, NULL, NULL, NULL, '2018-08-24 15:48:11.044347', '2018-08-24 15:48:11.044347', 1, 1, 3, 'Gastroenterology', NULL, 'http://idsmeddev.oss-cn-hongkong.aliyuncs.com/idsmedmedia/product/product_carearea/Gastroenterology.png', 'http://idsmeddev.oss-cn-hongkong.aliyuncs.com/idsmedmedia/product/wedoctor_product_carearea/Gastroenterology.png');
INSERT INTO idsmed.product_care_area VALUES (21, '急救护理', NULL, NULL, NULL, NULL, '2018-08-24 15:48:11.044347', '2018-08-24 15:48:11.044347', 1, 1, 3, 'Emergency Care', NULL, 'http://idsmeddev.oss-cn-hongkong.aliyuncs.com/idsmedmedia/product/product_carearea/Emergency_Care.png', 'http://idsmeddev.oss-cn-hongkong.aliyuncs.com/idsmedmedia/product/wedoctor_product_carearea/Emergency_Care.png');
INSERT INTO idsmed.product_care_area VALUES (22, '耳鼻喉科 ', NULL, NULL, NULL, NULL, '2018-08-24 15:48:11.044347', '2018-08-24 15:48:11.044347', 1, 1, 3, 'Ear, Nose & Throat', NULL, 'http://idsmeddev.oss-cn-hongkong.aliyuncs.com/idsmedmedia/product/product_carearea/Ear_Nose_and_Throat.png', 'http://idsmeddev.oss-cn-hongkong.aliyuncs.com/idsmedmedia/product/wedoctor_product_carearea/Ear_Nose_and_Throat.png');
INSERT INTO idsmed.product_care_area VALUES (23, '诊断成像', NULL, NULL, NULL, NULL, '2018-08-24 15:48:11.044347', '2018-08-24 15:48:11.044347', 1, 1, 3, 'Diagnostic Imaging', NULL, 'http://idsmeddev.oss-cn-hongkong.aliyuncs.com/idsmedmedia/product/product_carearea/Diagnostic_Care.png', 'http://idsmeddev.oss-cn-hongkong.aliyuncs.com/idsmedmedia/product/wedoctor_product_carearea/Diagnostic_Care.png');
INSERT INTO idsmed.product_care_area VALUES (24, '牙科', NULL, NULL, NULL, NULL, '2018-08-24 15:48:11.044347', '2018-08-24 15:48:11.044347', 1, 1, 3, 'Dental', NULL, 'http://idsmeddev.oss-cn-hongkong.aliyuncs.com/idsmedmedia/product/product_carearea/Dental.png', 'http://idsmeddev.oss-cn-hongkong.aliyuncs.com/idsmedmedia/product/wedoctor_product_carearea/Dental.png');
INSERT INTO idsmed.product_care_area VALUES (25, '心臟科', NULL, NULL, NULL, NULL, '2018-08-24 15:48:11.044347', '2018-08-24 15:48:11.044347', 1, 1, 3, 'Cardio Vascular', NULL, 'http://idsmeddev.oss-cn-hongkong.aliyuncs.com/idsmedmedia/product/product_carearea/Cardio_Vascular.png', 'http://idsmeddev.oss-cn-hongkong.aliyuncs.com/idsmedmedia/product/wedoctor_product_carearea/Cardio_Vascular.png');
INSERT INTO idsmed.product_care_area VALUES (1, '伤口管理', NULL, NULL, NULL, NULL, '2018-08-24 15:48:11.044347', '2018-08-24 15:48:11.044347', 1, 1, 3, 'Wound Management', NULL, 'http://idsmeddev.oss-cn-hongkong.aliyuncs.com/idsmedmedia/product/product_carearea/Wound_Management.png', 'http://idsmeddev.oss-cn-hongkong.aliyuncs.com/idsmedmedia/product/wedoctor_product_carearea/Wound_Management.png');
INSERT INTO idsmed.product_care_area VALUES (2, '兽医科', NULL, NULL, NULL, NULL, '2018-08-24 15:48:11.044347', '2018-08-24 15:48:11.044347', 1, 1, 3, 'Veterinary', NULL, 'http://idsmeddev.oss-cn-hongkong.aliyuncs.com/idsmedmedia/product/product_carearea/Veterinary.png', 'http://idsmeddev.oss-cn-hongkong.aliyuncs.com/idsmedmedia/product/wedoctor_product_carearea/Veterinary.png');
INSERT INTO idsmed.product_care_area VALUES (3, '手术室', NULL, NULL, NULL, NULL, '2018-08-24 15:48:11.044347', '2018-08-24 15:48:11.044347', 1, 1, 3, 'Surgical Workplace', NULL, 'http://idsmeddev.oss-cn-hongkong.aliyuncs.com/idsmedmedia/product/product_carearea/Surgical_Workplace.png', 'http://idsmeddev.oss-cn-hongkong.aliyuncs.com/idsmedmedia/product/wedoctor_product_carearea/Surgical_Workplace.png');
INSERT INTO idsmed.product_care_area VALUES (4, '呼吸保健', NULL, NULL, NULL, NULL, '2018-08-24 15:48:11.044347', '2018-08-24 15:48:11.044347', 1, 1, 3, 'Respiratory Care', NULL, 'http://idsmeddev.oss-cn-hongkong.aliyuncs.com/idsmedmedia/product/product_carearea/Respiratory_Care.png', 'http://idsmeddev.oss-cn-hongkong.aliyuncs.com/idsmedmedia/product/wedoctor_product_carearea/Respiratory_Care.png');
INSERT INTO idsmed.product_care_area VALUES (5, '初度保健', NULL, NULL, NULL, NULL, '2018-08-24 15:48:11.044347', '2018-08-24 15:48:11.044347', 1, 1, 3, 'Primary Care', NULL, 'http://idsmeddev.oss-cn-hongkong.aliyuncs.com/idsmedmedia/product/product_carearea/Primary_Care.png', 'http://idsmeddev.oss-cn-hongkong.aliyuncs.com/idsmedmedia/product/wedoctor_product_carearea/Primary_Care.png');
INSERT INTO idsmed.product_care_area VALUES (6, '物理疗法与复原', NULL, NULL, NULL, NULL, '2018-08-24 15:48:11.044347', '2018-08-24 15:48:11.044347', 1, 1, 3, 'Physio & Rehab', NULL, 'http://idsmeddev.oss-cn-hongkong.aliyuncs.com/idsmedmedia/product/product_carearea/Physio_and_Rehab.png', 'http://idsmeddev.oss-cn-hongkong.aliyuncs.com/idsmedmedia/product/wedoctor_product_carearea/Physio_and_Rehab.png');
INSERT INTO idsmed.product_care_area VALUES (7, '患者支援系統', NULL, NULL, NULL, NULL, '2018-08-24 15:48:11.044347', '2018-08-24 15:48:11.044347', 1, 1, 3, 'Patient Support System', NULL, 'http://idsmeddev.oss-cn-hongkong.aliyuncs.com/idsmedmedia/product/product_carearea/Patient_Support_System.png', 'http://idsmeddev.oss-cn-hongkong.aliyuncs.com/idsmedmedia/product/wedoctor_product_carearea/Patient_Support_System.png');
INSERT INTO idsmed.product_care_area VALUES (8, '筋骨科', NULL, NULL, NULL, NULL, '2018-08-24 15:48:11.044347', '2018-08-24 15:48:11.044347', 1, 1, 3, 'Orthopedic', NULL, 'http://idsmeddev.oss-cn-hongkong.aliyuncs.com/idsmedmedia/product/product_carearea/Orthopedic.png', 'http://idsmeddev.oss-cn-hongkong.aliyuncs.com/idsmedmedia/product/wedoctor_product_carearea/Orthopedic.png');
INSERT INTO idsmed.product_care_area VALUES (9, '眼科', NULL, NULL, NULL, NULL, '2018-08-24 15:48:11.044347', '2018-08-24 15:48:11.044347', 1, 1, 3, 'Ophthalmology', NULL, 'http://idsmeddev.oss-cn-hongkong.aliyuncs.com/idsmedmedia/product/product_carearea/Opthalmology.png', 'http://idsmeddev.oss-cn-hongkong.aliyuncs.com/idsmedmedia/product/wedoctor_product_carearea/Opthalmology.png');
INSERT INTO idsmed.product_care_area VALUES (10, '肿瘤科', NULL, NULL, NULL, NULL, '2018-08-24 15:48:11.044347', '2018-08-24 15:48:11.044347', 1, 1, 3, 'Oncology', NULL, 'http://idsmeddev.oss-cn-hongkong.aliyuncs.com/idsmedmedia/product/product_carearea/Oncology.png', 'http://idsmeddev.oss-cn-hongkong.aliyuncs.com/idsmedmedia/product/wedoctor_product_carearea/Oncology.png');
INSERT INTO idsmed.product_care_area VALUES (11, '妇产科与围产期', NULL, NULL, NULL, NULL, '2018-08-24 15:48:11.044347', '2018-08-24 15:48:11.044347', 1, 1, 3, 'O&G and Perinatal', NULL, 'http://idsmeddev.oss-cn-hongkong.aliyuncs.com/idsmedmedia/product/product_carearea/O_and_G_and_Peri_Natal.png', 'http://idsmeddev.oss-cn-hongkong.aliyuncs.com/idsmedmedia/product/wedoctor_product_carearea/O_and_G_and_Peri_Natal.png');


--
-- TOC entry 3883 (class 0 OID 97713)
-- Dependencies: 267
-- Data for Name: product_care_area_detail; Type: TABLE DATA; Schema: idsmed; Owner: dbadmin
--



--
-- TOC entry 4127 (class 0 OID 0)
-- Dependencies: 268
-- Name: product_care_area_detail_id_seq; Type: SEQUENCE SET; Schema: idsmed; Owner: dbadmin
--

SELECT pg_catalog.setval('idsmed.product_care_area_detail_id_seq', 4675, true);


--
-- TOC entry 3885 (class 0 OID 97720)
-- Dependencies: 269
-- Data for Name: product_care_area_detail_log; Type: TABLE DATA; Schema: idsmed; Owner: dbadmin
--



--
-- TOC entry 4128 (class 0 OID 0)
-- Dependencies: 270
-- Name: product_care_area_detail_log_id_seq; Type: SEQUENCE SET; Schema: idsmed; Owner: dbadmin
--

SELECT pg_catalog.setval('idsmed.product_care_area_detail_log_id_seq', 22, true);


--
-- TOC entry 4129 (class 0 OID 0)
-- Dependencies: 271
-- Name: product_care_area_id_seq; Type: SEQUENCE SET; Schema: idsmed; Owner: dbadmin
--

SELECT pg_catalog.setval('idsmed.product_care_area_id_seq', 40, true);


--
-- TOC entry 3888 (class 0 OID 97729)
-- Dependencies: 272
-- Data for Name: product_category; Type: TABLE DATA; Schema: idsmed; Owner: dbadmin
--

INSERT INTO idsmed.product_category VALUES (1, 'Consumables', NULL, NULL, NULL, NULL, NULL, '2018-08-24 15:52:12.446983', '2018-08-24 15:52:12.446983', 1, 1, 3);
INSERT INTO idsmed.product_category VALUES (2, 'Accessories', NULL, NULL, NULL, NULL, NULL, '2018-08-24 15:52:12.446983', '2018-08-24 15:52:12.446983', 1, 1, 3);
INSERT INTO idsmed.product_category VALUES (3, 'Equipment', NULL, NULL, NULL, NULL, NULL, '2018-08-24 15:52:12.446983', '2018-08-24 15:52:12.446983', 1, 1, 3);
INSERT INTO idsmed.product_category VALUES (4, 'Parts', NULL, NULL, NULL, NULL, NULL, '2018-08-24 15:52:12.446983', '2018-08-24 15:52:12.446983', 1, 1, 3);
INSERT INTO idsmed.product_category VALUES (5, 'OTHERS', NULL, NULL, NULL, NULL, NULL, '2018-08-24 15:52:12.446983', '2018-08-24 15:52:12.446983', 1, 1, 3);


--
-- TOC entry 4130 (class 0 OID 0)
-- Dependencies: 273
-- Name: product_category_id_seq; Type: SEQUENCE SET; Schema: idsmed; Owner: dbadmin
--

SELECT pg_catalog.setval('idsmed.product_category_id_seq', 6, false);


--
-- TOC entry 3890 (class 0 OID 97742)
-- Dependencies: 274
-- Data for Name: product_certificate_log; Type: TABLE DATA; Schema: idsmed; Owner: dbadmin
--



--
-- TOC entry 4131 (class 0 OID 0)
-- Dependencies: 275
-- Name: product_certificate_log_id_seq; Type: SEQUENCE SET; Schema: idsmed; Owner: dbadmin
--

SELECT pg_catalog.setval('idsmed.product_certificate_log_id_seq', 1, false);


--
-- TOC entry 3892 (class 0 OID 97755)
-- Dependencies: 276
-- Data for Name: product_csv_log; Type: TABLE DATA; Schema: idsmed; Owner: dbadmin
--



--
-- TOC entry 4132 (class 0 OID 0)
-- Dependencies: 277
-- Name: product_csv_log_id_seq; Type: SEQUENCE SET; Schema: idsmed; Owner: dbadmin
--

SELECT pg_catalog.setval('idsmed.product_csv_log_id_seq', 60, true);


--
-- TOC entry 3894 (class 0 OID 97767)
-- Dependencies: 278
-- Data for Name: product_document_attachment; Type: TABLE DATA; Schema: idsmed; Owner: dbadmin
--



--
-- TOC entry 4133 (class 0 OID 0)
-- Dependencies: 279
-- Name: product_document_attachment_id_seq; Type: SEQUENCE SET; Schema: idsmed; Owner: dbadmin
--

SELECT pg_catalog.setval('idsmed.product_document_attachment_id_seq', 71, true);


--
-- TOC entry 3896 (class 0 OID 97780)
-- Dependencies: 280
-- Data for Name: product_document_attachment_reminder; Type: TABLE DATA; Schema: idsmed; Owner: dbadmin
--



--
-- TOC entry 4134 (class 0 OID 0)
-- Dependencies: 281
-- Name: product_document_attachment_reminder_id_seq; Type: SEQUENCE SET; Schema: idsmed; Owner: dbadmin
--

SELECT pg_catalog.setval('idsmed.product_document_attachment_reminder_id_seq', 219, true);


--
-- TOC entry 3898 (class 0 OID 97794)
-- Dependencies: 282
-- Data for Name: product_features; Type: TABLE DATA; Schema: idsmed; Owner: dbadmin
--



--
-- TOC entry 4135 (class 0 OID 0)
-- Dependencies: 283
-- Name: product_features_id_seq; Type: SEQUENCE SET; Schema: idsmed; Owner: dbadmin
--

SELECT pg_catalog.setval('idsmed.product_features_id_seq', 1196, true);


--
-- TOC entry 3900 (class 0 OID 97808)
-- Dependencies: 284
-- Data for Name: product_features_log; Type: TABLE DATA; Schema: idsmed; Owner: dbadmin
--



--
-- TOC entry 4136 (class 0 OID 0)
-- Dependencies: 285
-- Name: product_features_log_id_seq; Type: SEQUENCE SET; Schema: idsmed; Owner: dbadmin
--

SELECT pg_catalog.setval('idsmed.product_features_log_id_seq', 31, true);


--
-- TOC entry 3902 (class 0 OID 97821)
-- Dependencies: 286
-- Data for Name: product_hierarchy; Type: TABLE DATA; Schema: idsmed; Owner: dbadmin
--

INSERT INTO idsmed.product_hierarchy VALUES (38, '软组织超声手术仪', '01010101', 3, '2019-01-28 14:46:38.104184', '2019-01-28 14:46:38.104184', 1, 1, '软组织超声手术仪');
INSERT INTO idsmed.product_hierarchy VALUES (39, '外科超声手术系统', '01010102', 3, '2019-01-28 14:46:38.104184', '2019-01-28 14:46:38.104184', 1, 1, '外科超声手术系统');
INSERT INTO idsmed.product_hierarchy VALUES (40, '超声手术系统', '01010103', 3, '2019-01-28 14:46:38.104184', '2019-01-28 14:46:38.104184', 1, 1, '超声手术系统');
INSERT INTO idsmed.product_hierarchy VALUES (41, '超声切割止血刀系统', '01010104', 3, '2019-01-28 14:46:38.104184', '2019-01-28 14:46:38.104184', 1, 1, '超声切割止血刀系统');
INSERT INTO idsmed.product_hierarchy VALUES (42, '软组织超声手术系统', '01010105', 3, '2019-01-28 14:46:38.104184', '2019-01-28 14:46:38.104184', 1, 1, '软组织超声手术系统');
INSERT INTO idsmed.product_hierarchy VALUES (43, '超声手术刀', '01010106', 3, '2019-01-28 14:46:38.104184', '2019-01-28 14:46:38.104184', 1, 1, '超声手术刀');
INSERT INTO idsmed.product_hierarchy VALUES (52, '其他', '01010115', 3, '2019-01-28 14:46:38.104184', '2019-01-28 14:46:38.104184', 1, 1, '其他');
INSERT INTO idsmed.product_hierarchy VALUES (44, '超声刀系统', '01010107', 3, '2019-01-28 14:46:38.104184', '2019-01-28 14:46:38.104184', 1, 1, '超声刀系统');
INSERT INTO idsmed.product_hierarchy VALUES (45, '超声脂肪乳化仪', '01010108', 3, '2019-01-28 14:46:38.104184', '2019-01-28 14:46:38.104184', 1, 1, '超声脂肪乳化仪');
INSERT INTO idsmed.product_hierarchy VALUES (46, '超声外科吸引系统', '01010109', 3, '2019-01-28 14:46:38.104184', '2019-01-28 14:46:38.104184', 1, 1, '超声外科吸引系统');
INSERT INTO idsmed.product_hierarchy VALUES (47, '软组织超声手术仪', '01010110', 3, '2019-01-28 14:46:38.104184', '2019-01-28 14:46:38.104184', 1, 1, '软组织超声手术仪');
INSERT INTO idsmed.product_hierarchy VALUES (48, '软组织超声手术系统', '01010111', 3, '2019-01-28 14:46:38.104184', '2019-01-28 14:46:38.104184', 1, 1, '软组织超声手术系统');
INSERT INTO idsmed.product_hierarchy VALUES (49, '超声骨科手术仪', '01010112', 3, '2019-01-28 14:46:38.104184', '2019-01-28 14:46:38.104184', 1, 1, '超声骨科手术仪');
INSERT INTO idsmed.product_hierarchy VALUES (50, '超声骨组织手术系统', '01010113', 3, '2019-01-28 14:46:38.104184', '2019-01-28 14:46:38.104184', 1, 1, '超声骨组织手术系统');
INSERT INTO idsmed.product_hierarchy VALUES (51, '超声碎石系统', '01010114', 3, '2019-01-28 14:46:38.104184', '2019-01-28 14:46:38.104184', 1, 1, '超声碎石系统');
INSERT INTO idsmed.product_hierarchy VALUES (53, '超声治疗仪', '01010201', 3, '2019-02-11 11:56:45.394142', '2019-02-11 11:56:45.394142', 1, 1, '超声治疗仪');
INSERT INTO idsmed.product_hierarchy VALUES (54, '超声治疗系统', '01010202', 3, '2019-02-11 11:56:45.394142', '2019-02-11 11:56:45.394142', 1, 1, '超声治疗系统');
INSERT INTO idsmed.product_hierarchy VALUES (55, '减脂聚焦超声治疗系统', '01010203', 3, '2019-02-11 11:56:45.394142', '2019-02-11 11:56:45.394142', 1, 1, '减脂聚焦超声治疗系统');
INSERT INTO idsmed.product_hierarchy VALUES (56, '磁共振引导高强度聚焦超声治疗系统', '01010204', 3, '2019-02-11 11:56:45.394142', '2019-02-11 11:56:45.394142', 1, 1, '磁共振引导高强度聚焦超声治疗系统');
INSERT INTO idsmed.product_hierarchy VALUES (57, '肿瘤消融聚焦超声治疗系统', '01010205', 3, '2019-02-11 11:56:45.394142', '2019-02-11 11:56:45.394142', 1, 1, '肿瘤消融聚焦超声治疗系统');
INSERT INTO idsmed.product_hierarchy VALUES (58, '肿瘤聚焦超声治疗系统', '01010206', 3, '2019-02-11 11:56:45.394142', '2019-02-11 11:56:45.394142', 1, 1, '肿瘤聚焦超声治疗系统');
INSERT INTO idsmed.product_hierarchy VALUES (59, '肿瘤高强度聚焦超声治疗系统', '01010207', 3, '2019-02-11 11:56:45.394142', '2019-02-11 11:56:45.394142', 1, 1, '肿瘤高强度聚焦超声治疗系统');
INSERT INTO idsmed.product_hierarchy VALUES (60, '其他', '01010208', 3, '2019-02-11 11:56:45.394142', '2019-02-11 11:56:45.394142', 1, 1, '其他');


--
-- TOC entry 4137 (class 0 OID 0)
-- Dependencies: 287
-- Name: product_hierarchy_id_seq; Type: SEQUENCE SET; Schema: idsmed; Owner: dbadmin
--

SELECT pg_catalog.setval('idsmed.product_hierarchy_id_seq', 60, true);


--
-- TOC entry 4138 (class 0 OID 0)
-- Dependencies: 288
-- Name: product_id_seq; Type: SEQUENCE SET; Schema: idsmed; Owner: dbadmin
--

SELECT pg_catalog.setval('idsmed.product_id_seq', 4656, true);


--
-- TOC entry 3905 (class 0 OID 97834)
-- Dependencies: 289
-- Data for Name: product_info_config; Type: TABLE DATA; Schema: idsmed; Owner: dbadmin
--



--
-- TOC entry 4139 (class 0 OID 0)
-- Dependencies: 290
-- Name: product_info_config_id_seq; Type: SEQUENCE SET; Schema: idsmed; Owner: dbadmin
--

SELECT pg_catalog.setval('idsmed.product_info_config_id_seq', 8, true);


--
-- TOC entry 3907 (class 0 OID 97844)
-- Dependencies: 291
-- Data for Name: product_label_config; Type: TABLE DATA; Schema: idsmed; Owner: dbadmin
--



--
-- TOC entry 4140 (class 0 OID 0)
-- Dependencies: 292
-- Name: product_label_config_id_seq; Type: SEQUENCE SET; Schema: idsmed; Owner: dbadmin
--

SELECT pg_catalog.setval('idsmed.product_label_config_id_seq', 19, true);


--
-- TOC entry 3909 (class 0 OID 97859)
-- Dependencies: 293
-- Data for Name: product_log; Type: TABLE DATA; Schema: idsmed; Owner: dbadmin
--



--
-- TOC entry 4141 (class 0 OID 0)
-- Dependencies: 294
-- Name: product_log_id_seq; Type: SEQUENCE SET; Schema: idsmed; Owner: dbadmin
--

SELECT pg_catalog.setval('idsmed.product_log_id_seq', 23, true);


--
-- TOC entry 3911 (class 0 OID 97875)
-- Dependencies: 295
-- Data for Name: product_media; Type: TABLE DATA; Schema: idsmed; Owner: dbadmin
--



--
-- TOC entry 4142 (class 0 OID 0)
-- Dependencies: 296
-- Name: product_media_id_seq; Type: SEQUENCE SET; Schema: idsmed; Owner: dbadmin
--

SELECT pg_catalog.setval('idsmed.product_media_id_seq', 16409, true);


--
-- TOC entry 3913 (class 0 OID 97888)
-- Dependencies: 297
-- Data for Name: product_media_log; Type: TABLE DATA; Schema: idsmed; Owner: dbadmin
--



--
-- TOC entry 4143 (class 0 OID 0)
-- Dependencies: 298
-- Name: product_media_log_id_seq; Type: SEQUENCE SET; Schema: idsmed; Owner: dbadmin
--

SELECT pg_catalog.setval('idsmed.product_media_log_id_seq', 106, true);


--
-- TOC entry 3915 (class 0 OID 97901)
-- Dependencies: 299
-- Data for Name: product_rating; Type: TABLE DATA; Schema: idsmed; Owner: dbadmin
--



--
-- TOC entry 3916 (class 0 OID 97909)
-- Dependencies: 300
-- Data for Name: product_rating_detail; Type: TABLE DATA; Schema: idsmed; Owner: dbadmin
--



--
-- TOC entry 4144 (class 0 OID 0)
-- Dependencies: 301
-- Name: product_rating_detail_id_seq; Type: SEQUENCE SET; Schema: idsmed; Owner: dbadmin
--

SELECT pg_catalog.setval('idsmed.product_rating_detail_id_seq', 16, true);


--
-- TOC entry 4145 (class 0 OID 0)
-- Dependencies: 302
-- Name: product_rating_id_seq; Type: SEQUENCE SET; Schema: idsmed; Owner: dbadmin
--

SELECT pg_catalog.setval('idsmed.product_rating_id_seq', 2167, true);


--
-- TOC entry 3919 (class 0 OID 97924)
-- Dependencies: 303
-- Data for Name: product_reject; Type: TABLE DATA; Schema: idsmed; Owner: dbadmin
--



--
-- TOC entry 4146 (class 0 OID 0)
-- Dependencies: 304
-- Name: product_reject_id_seq; Type: SEQUENCE SET; Schema: idsmed; Owner: dbadmin
--

SELECT pg_catalog.setval('idsmed.product_reject_id_seq', 18, true);


--
-- TOC entry 3921 (class 0 OID 97937)
-- Dependencies: 305
-- Data for Name: product_reject_log; Type: TABLE DATA; Schema: idsmed; Owner: dbadmin
--



--
-- TOC entry 4147 (class 0 OID 0)
-- Dependencies: 306
-- Name: product_reject_log_id_seq; Type: SEQUENCE SET; Schema: idsmed; Owner: dbadmin
--

SELECT pg_catalog.setval('idsmed.product_reject_log_id_seq', 1, true);


--
-- TOC entry 3923 (class 0 OID 97950)
-- Dependencies: 307
-- Data for Name: product_related; Type: TABLE DATA; Schema: idsmed; Owner: dbadmin
--



--
-- TOC entry 4148 (class 0 OID 0)
-- Dependencies: 308
-- Name: product_related_id_seq; Type: SEQUENCE SET; Schema: idsmed; Owner: dbadmin
--

SELECT pg_catalog.setval('idsmed.product_related_id_seq', 3, true);


--
-- TOC entry 3925 (class 0 OID 97960)
-- Dependencies: 309
-- Data for Name: product_second_category; Type: TABLE DATA; Schema: idsmed; Owner: dbadmin
--

INSERT INTO idsmed.product_second_category VALUES (1, 'Respiratory', '呼吸', NULL, NULL, NULL, NULL, '2018-12-31 17:10:50.697836', '2018-12-31 17:10:50.697836', -1, -1, 3);
INSERT INTO idsmed.product_second_category VALUES (2, 'Anesthesia', '麻醉', NULL, NULL, NULL, NULL, '2018-12-31 17:10:50.697836', '2018-12-31 17:10:50.697836', -1, -1, 3);
INSERT INTO idsmed.product_second_category VALUES (3, 'Urology', '泌尿科学', NULL, NULL, NULL, NULL, '2018-12-31 17:10:50.697836', '2018-12-31 17:10:50.697836', -1, -1, 3);
INSERT INTO idsmed.product_second_category VALUES (4, 'Surgical kit', '手术套件', NULL, NULL, NULL, NULL, '2018-12-31 17:10:50.697836', '2018-12-31 17:10:50.697836', -1, -1, 3);
INSERT INTO idsmed.product_second_category VALUES (5, 'Instrument', '仪器', NULL, NULL, NULL, NULL, '2018-12-31 17:10:50.697836', '2018-12-31 17:10:50.697836', -1, -1, 3);
INSERT INTO idsmed.product_second_category VALUES (6, 'Central Sterile Supply Department', 'CSSD', NULL, NULL, NULL, NULL, '2018-12-31 17:10:50.697836', '2018-12-31 17:10:50.697836', -1, -1, 3);
INSERT INTO idsmed.product_second_category VALUES (7, 'Hospital fabric', '医院面料', NULL, NULL, NULL, NULL, '2018-12-31 17:10:50.697836', '2018-12-31 17:10:50.697836', -1, -1, 3);
INSERT INTO idsmed.product_second_category VALUES (8, 'Syringe and needle', '注射器和针头', NULL, NULL, NULL, NULL, '2018-12-31 17:10:50.697836', '2018-12-31 17:10:50.697836', -1, -1, 3);
INSERT INTO idsmed.product_second_category VALUES (9, 'Infusion', '注入', NULL, NULL, NULL, NULL, '2018-12-31 17:10:50.697836', '2018-12-31 17:10:50.697836', -1, -1, 3);
INSERT INTO idsmed.product_second_category VALUES (10, 'Blood collection', '血液采集', NULL, NULL, NULL, NULL, '2018-12-31 17:10:50.697836', '2018-12-31 17:10:50.697836', -1, -1, 3);
INSERT INTO idsmed.product_second_category VALUES (11, 'Diagnostics', '诊断', NULL, NULL, NULL, NULL, '2018-12-31 17:10:50.697836', '2018-12-31 17:10:50.697836', -1, -1, 3);
INSERT INTO idsmed.product_second_category VALUES (12, 'Laboratory', '实验室', NULL, NULL, NULL, NULL, '2018-12-31 17:10:50.697836', '2018-12-31 17:10:50.697836', -1, -1, 3);
INSERT INTO idsmed.product_second_category VALUES (13, 'ECG', '心电图', NULL, NULL, NULL, NULL, '2018-12-31 17:10:50.697836', '2018-12-31 17:10:50.697836', -1, -1, 3);
INSERT INTO idsmed.product_second_category VALUES (14, 'Glove', '手套', NULL, NULL, NULL, NULL, '2018-12-31 17:10:50.697836', '2018-12-31 17:10:50.697836', -1, -1, 3);
INSERT INTO idsmed.product_second_category VALUES (15, 'Wound dressing', '伤口敷料', NULL, NULL, NULL, NULL, '2018-12-31 17:10:50.697836', '2018-12-31 17:10:50.697836', -1, -1, 3);
INSERT INTO idsmed.product_second_category VALUES (16, 'Ostomy', '造口术', NULL, NULL, NULL, NULL, '2018-12-31 17:10:50.697836', '2018-12-31 17:10:50.697836', -1, -1, 3);
INSERT INTO idsmed.product_second_category VALUES (17, 'Clean & Disinfect', '清洁和消毒', NULL, NULL, NULL, NULL, '2018-12-31 17:10:50.697836', '2018-12-31 17:10:50.697836', -1, -1, 3);
INSERT INTO idsmed.product_second_category VALUES (18, 'Apparel', '服饰', NULL, NULL, NULL, NULL, '2018-12-31 17:10:50.697836', '2018-12-31 17:10:50.697836', -1, -1, 3);
INSERT INTO idsmed.product_second_category VALUES (19, 'Face mask', '面具', NULL, NULL, NULL, NULL, '2018-12-31 17:10:50.697836', '2018-12-31 17:10:50.697836', -1, -1, 3);
INSERT INTO idsmed.product_second_category VALUES (20, 'Orthopedic', '骨科', NULL, NULL, NULL, NULL, '2018-12-31 17:10:50.697836', '2018-12-31 17:10:50.697836', -1, -1, 3);
INSERT INTO idsmed.product_second_category VALUES (21, 'Patient care', '病人护理', NULL, NULL, NULL, NULL, '2018-12-31 17:10:50.697836', '2018-12-31 17:10:50.697836', -1, -1, 3);
INSERT INTO idsmed.product_second_category VALUES (22, 'Durable Medical Equipment', 'DME', NULL, NULL, NULL, NULL, '2018-12-31 17:10:50.697836', '2018-12-31 17:10:50.697836', -1, -1, 3);


--
-- TOC entry 3926 (class 0 OID 97971)
-- Dependencies: 310
-- Data for Name: product_second_category_detail; Type: TABLE DATA; Schema: idsmed; Owner: dbadmin
--



--
-- TOC entry 4149 (class 0 OID 0)
-- Dependencies: 311
-- Name: product_second_category_detail_id_seq; Type: SEQUENCE SET; Schema: idsmed; Owner: dbadmin
--

SELECT pg_catalog.setval('idsmed.product_second_category_detail_id_seq', 3, true);


--
-- TOC entry 4150 (class 0 OID 0)
-- Dependencies: 312
-- Name: product_second_category_id_seq; Type: SEQUENCE SET; Schema: idsmed; Owner: dbadmin
--

SELECT pg_catalog.setval('idsmed.product_second_category_id_seq', 22, true);


--
-- TOC entry 3929 (class 0 OID 97982)
-- Dependencies: 313
-- Data for Name: product_selling_history; Type: TABLE DATA; Schema: idsmed; Owner: dbadmin
--



--
-- TOC entry 4151 (class 0 OID 0)
-- Dependencies: 314
-- Name: product_selling_history_id_seq; Type: SEQUENCE SET; Schema: idsmed; Owner: dbadmin
--

SELECT pg_catalog.setval('idsmed.product_selling_history_id_seq', 3, true);


--
-- TOC entry 3931 (class 0 OID 97992)
-- Dependencies: 315
-- Data for Name: product_selling_rate; Type: TABLE DATA; Schema: idsmed; Owner: dbadmin
--



--
-- TOC entry 4152 (class 0 OID 0)
-- Dependencies: 316
-- Name: product_selling_rate_id_seq; Type: SEQUENCE SET; Schema: idsmed; Owner: dbadmin
--

SELECT pg_catalog.setval('idsmed.product_selling_rate_id_seq', 10, true);


--
-- TOC entry 3933 (class 0 OID 98002)
-- Dependencies: 317
-- Data for Name: product_tech_feature; Type: TABLE DATA; Schema: idsmed; Owner: dbadmin
--

INSERT INTO idsmed.product_tech_feature VALUES (70, 'dimension(L.W.H)', 1, '2019-01-03 18:23:48.498504', '2019-01-03 18:23:48.498504', 1, 1, 3, '尺寸（长.宽.高）', '尺寸（長.寬.高）', NULL, NULL, NULL, 1);
INSERT INTO idsmed.product_tech_feature VALUES (71, 'gross weight', 1, '2019-01-03 18:23:48.498504', '2019-01-03 18:23:48.498504', 1, 1, 3, '总重量', '總重量', NULL, NULL, NULL, 1);
INSERT INTO idsmed.product_tech_feature VALUES (72, 'material weight', 1, '2019-01-03 18:23:48.498504', '2019-01-03 18:23:48.498504', 1, 1, 3, '材料重量', '材料重量', NULL, NULL, NULL, 1);
INSERT INTO idsmed.product_tech_feature VALUES (73, 'material', 1, '2019-01-03 18:23:48.498504', '2019-01-03 18:23:48.498504', 1, 1, 3, '材料', '材料', NULL, NULL, NULL, 1);
INSERT INTO idsmed.product_tech_feature VALUES (74, 'technology', 1, '2019-01-03 18:23:48.498504', '2019-01-03 18:23:48.498504', 1, 1, 3, '技术', '技術', NULL, NULL, NULL, 1);
INSERT INTO idsmed.product_tech_feature VALUES (75, 'applied standards', 1, '2019-01-03 18:23:48.498504', '2019-01-03 18:23:48.498504', 1, 1, 3, '应用标准', '應用標準', NULL, NULL, NULL, 1);
INSERT INTO idsmed.product_tech_feature VALUES (76, 'shelf life(days)', 1, '2019-01-03 18:23:48.498504', '2019-01-03 18:23:48.498504', 1, 1, 3, '保质期（天）', '保質期（天）', NULL, NULL, NULL, 1);
INSERT INTO idsmed.product_tech_feature VALUES (77, 'sterilization(Y/N)', 1, '2019-01-03 18:23:48.498504', '2019-01-03 18:23:48.498504', 1, 1, 3, '消毒（Y/N）', '消毒（Y/N）', NULL, NULL, NULL, 1);
INSERT INTO idsmed.product_tech_feature VALUES (78, 'storage conditions', 1, '2019-01-03 18:23:48.498504', '2019-01-03 18:23:48.498504', 1, 1, 3, '储藏条件', '儲藏條件', NULL, NULL, NULL, 1);
INSERT INTO idsmed.product_tech_feature VALUES (79, 'single/multiple use', 1, '2019-01-03 18:23:48.498504', '2019-01-03 18:23:48.498504', 1, 1, 3, '单次/多次使用', '單次/多次使用', NULL, NULL, NULL, 1);


--
-- TOC entry 4153 (class 0 OID 0)
-- Dependencies: 318
-- Name: product_tech_feature_id_seq; Type: SEQUENCE SET; Schema: idsmed; Owner: dbadmin
--

SELECT pg_catalog.setval('idsmed.product_tech_feature_id_seq', 79, true);


--
-- TOC entry 3935 (class 0 OID 98015)
-- Dependencies: 319
-- Data for Name: product_technical; Type: TABLE DATA; Schema: idsmed; Owner: dbadmin
--



--
-- TOC entry 4154 (class 0 OID 0)
-- Dependencies: 320
-- Name: product_technical_id_seq; Type: SEQUENCE SET; Schema: idsmed; Owner: dbadmin
--

SELECT pg_catalog.setval('idsmed.product_technical_id_seq', 648, true);


--
-- TOC entry 3937 (class 0 OID 98029)
-- Dependencies: 321
-- Data for Name: product_technical_log; Type: TABLE DATA; Schema: idsmed; Owner: dbadmin
--



--
-- TOC entry 4155 (class 0 OID 0)
-- Dependencies: 322
-- Name: product_technical_log_id_seq; Type: SEQUENCE SET; Schema: idsmed; Owner: dbadmin
--

SELECT pg_catalog.setval('idsmed.product_technical_log_id_seq', 26, true);


--
-- TOC entry 3939 (class 0 OID 98042)
-- Dependencies: 323
-- Data for Name: product_wishlist; Type: TABLE DATA; Schema: idsmed; Owner: dbadmin
--



--
-- TOC entry 3940 (class 0 OID 98050)
-- Dependencies: 324
-- Data for Name: product_wishlist_detail; Type: TABLE DATA; Schema: idsmed; Owner: dbadmin
--



--
-- TOC entry 4156 (class 0 OID 0)
-- Dependencies: 325
-- Name: product_wishlist_detail_id_seq; Type: SEQUENCE SET; Schema: idsmed; Owner: dbadmin
--

SELECT pg_catalog.setval('idsmed.product_wishlist_detail_id_seq', 17, true);


--
-- TOC entry 4157 (class 0 OID 0)
-- Dependencies: 326
-- Name: product_wishlist_id_seq; Type: SEQUENCE SET; Schema: idsmed; Owner: dbadmin
--

SELECT pg_catalog.setval('idsmed.product_wishlist_id_seq', 2167, true);


--
-- TOC entry 3943 (class 0 OID 98062)
-- Dependencies: 327
-- Data for Name: province; Type: TABLE DATA; Schema: idsmed; Owner: dbadmin
--

INSERT INTO idsmed.province VALUES (1, 1, 'Anhui', '安徽省', 'Anhui', '安徽省', 'AH', '2018-10-29 16:17:43.274025', '2018-10-29 16:17:43.274025', 1, 1, 1);
INSERT INTO idsmed.province VALUES (2, 1, 'Beijing', '北京市', 'Beijing', '北京市', 'BJ', '2018-10-29 16:17:43.274025', '2018-10-29 16:17:43.274025', 1, 1, 1);
INSERT INTO idsmed.province VALUES (3, 1, 'Chongqing', '重庆市', 'Chongqing', '重庆市', 'CQ', '2018-10-29 16:17:43.274025', '2018-10-29 16:17:43.274025', 1, 1, 1);
INSERT INTO idsmed.province VALUES (4, 1, 'Fujian', '福建省', 'Fujian', '福建省', 'FJ', '2018-10-29 16:17:43.274025', '2018-10-29 16:17:43.274025', 1, 1, 1);
INSERT INTO idsmed.province VALUES (5, 1, 'Gansu', '甘肃省', 'Gansu', '甘肃省', 'GS', '2018-10-29 16:17:43.274025', '2018-10-29 16:17:43.274025', 1, 1, 1);
INSERT INTO idsmed.province VALUES (6, 1, 'Guangdong', '广东省', 'Guangdong', '广东省', 'GD', '2018-10-29 16:17:43.274025', '2018-10-29 16:17:43.274025', 1, 1, 1);
INSERT INTO idsmed.province VALUES (8, 1, 'Guizhou', '贵州省', 'Guizhou', '贵州省', 'GZ', '2018-10-29 16:17:43.274025', '2018-10-29 16:17:43.274025', 1, 1, 1);
INSERT INTO idsmed.province VALUES (17, 1, 'Jiangsu', '江苏省', 'Jiangsu', '江苏省', 'JS', '2018-10-29 16:17:43.274025', '2018-10-29 16:17:43.274025', 1, 1, 1);
INSERT INTO idsmed.province VALUES (18, 1, 'Jiangxi', '江西省', 'Jiangxi', '江西省', 'JX', '2018-10-29 16:17:43.274025', '2018-10-29 16:17:43.274025', 1, 1, 1);
INSERT INTO idsmed.province VALUES (19, 1, 'Jilin', '吉林省', 'Jilin', '吉林省', 'JL', '2018-10-29 16:17:43.274025', '2018-10-29 16:17:43.274025', 1, 1, 1);
INSERT INTO idsmed.province VALUES (20, 1, 'Liaoning', '辽宁省', 'Liaoning', '辽宁省', 'LN', '2018-10-29 16:17:43.274025', '2018-10-29 16:17:43.274025', 1, 1, 1);
INSERT INTO idsmed.province VALUES (23, 1, 'Qinghai', '青海省', 'Qinghai', '青海省', 'QH', '2018-10-29 16:17:43.274025', '2018-10-29 16:17:43.274025', 1, 1, 1);
INSERT INTO idsmed.province VALUES (25, 1, 'Shandong', '山东省', 'Shandong', '山东省', 'SD', '2018-10-29 16:17:43.274025', '2018-10-29 16:17:43.274025', 1, 1, 1);
INSERT INTO idsmed.province VALUES (26, 1, 'Shanghai', '上海市', 'Shanghai', '上海市', 'SH', '2018-10-29 16:17:43.274025', '2018-10-29 16:17:43.274025', 1, 1, 1);
INSERT INTO idsmed.province VALUES (29, 1, 'Tianjin', '天津市', 'Tianjin', '天津市', 'TJ', '2018-10-29 16:17:43.274025', '2018-10-29 16:17:43.274025', 1, 1, 1);
INSERT INTO idsmed.province VALUES (32, 1, 'Yunnan', '云南省', 'Yunnan', '云南省', 'YN', '2018-10-29 16:17:43.274025', '2018-10-29 16:17:43.274025', 1, 1, 1);
INSERT INTO idsmed.province VALUES (33, 1, 'Zhejiang', '浙江省', 'Zhejiang', '浙江省', 'ZJ', '2018-10-29 16:17:43.274025', '2018-10-29 16:17:43.274025', 1, 1, 1);
INSERT INTO idsmed.province VALUES (7, 1, 'Guangxi', '广西壮族自治区', 'Guangxi', '广西壮族', 'GX', '2018-10-29 16:17:43.274025', '2018-10-29 16:17:43.274025', 1, 1, 1);
INSERT INTO idsmed.province VALUES (9, 1, 'Hainan', '海南省', 'Hainan', '海南省', 'HI', '2018-10-29 16:17:43.274025', '2018-10-29 16:17:43.274025', 1, 1, 1);
INSERT INTO idsmed.province VALUES (12, 1, 'Henan', '河南省', 'Henan', '河南省', 'HI', '2018-10-29 16:17:43.274025', '2018-10-29 16:17:43.274025', 1, 1, 1);
INSERT INTO idsmed.province VALUES (10, 1, 'Hebei', '河北省', 'Hebei', '河北省', 'HE', '2018-10-29 16:17:43.274025', '2018-10-29 16:17:43.274025', 1, 1, 1);
INSERT INTO idsmed.province VALUES (11, 1, 'Heilongjiang', '黑龙江省', 'Heilongjiang', '黑龙江省', 'HL', '2018-10-29 16:17:43.274025', '2018-10-29 16:17:43.274025', 1, 1, 1);
INSERT INTO idsmed.province VALUES (13, 1, 'Hong Kong', '香港特别行政区', 'Hong Kong', '香港', 'HK', '2018-10-29 16:17:43.274025', '2018-10-29 16:17:43.274025', 1, 1, 1);
INSERT INTO idsmed.province VALUES (14, 1, 'Hubei', '湖北省', 'Hubei', '湖北省', 'HB', '2018-10-29 16:17:43.274025', '2018-10-29 16:17:43.274025', 1, 1, 1);
INSERT INTO idsmed.province VALUES (15, 1, 'Hunan', '湖南省', 'Hunan', '湖南省', 'HN', '2018-10-29 16:17:43.274025', '2018-10-29 16:17:43.274025', 1, 1, 1);
INSERT INTO idsmed.province VALUES (16, 1, 'Inner Mongolia', '内蒙古自治区', 'Inner Mongolia', '内蒙古', 'NM', '2018-10-29 16:17:43.274025', '2018-10-29 16:17:43.274025', 1, 1, 1);
INSERT INTO idsmed.province VALUES (21, 1, 'Macao', '澳门特别行政区', 'Macao', '澳门', 'MO', '2018-10-29 16:17:43.274025', '2018-10-29 16:17:43.274025', 1, 1, 1);
INSERT INTO idsmed.province VALUES (22, 1, 'Ningxia', '宁夏回族自治区', 'Ningxia', '宁夏回族', 'NX', '2018-10-29 16:17:43.274025', '2018-10-29 16:17:43.274025', 1, 1, 1);
INSERT INTO idsmed.province VALUES (24, 1, 'Shaanxi', '陕西省', 'Shaanxi', '陕西省', 'SN', '2018-10-29 16:17:43.274025', '2018-10-29 16:17:43.274025', 1, 1, 1);
INSERT INTO idsmed.province VALUES (27, 1, 'Shanxi', '山西省', 'Shanxi ', '山西省', 'SX', '2018-10-29 16:17:43.274025', '2018-10-29 16:17:43.274025', 1, 1, 1);
INSERT INTO idsmed.province VALUES (28, 1, 'Sichuan', '四川省', 'Sichuan', '四川省', 'SC', '2018-10-29 16:17:43.274025', '2018-10-29 16:17:43.274025', 1, 1, 1);
INSERT INTO idsmed.province VALUES (30, 1, 'Tibet', '西藏自治区', 'Tibet', '西藏', 'XZ', '2018-10-29 16:17:43.274025', '2018-10-29 16:17:43.274025', 1, 1, 1);
INSERT INTO idsmed.province VALUES (31, 1, 'Xinjiang Uyghur', '新疆维吾尔自治区', 'Xinjiang Uyghur', '新疆维吾尔', 'XJ', '2018-10-29 16:17:43.274025', '2018-10-29 16:17:43.274025', 1, 1, 1);
INSERT INTO idsmed.province VALUES (34, 1, 'Taiwan', '台湾省', 'Taiwan', '台湾省', 'TW', '2019-01-22 09:22:37.57094', '2019-01-22 09:22:37.57094', 1, 1, 3);


--
-- TOC entry 4158 (class 0 OID 0)
-- Dependencies: 328
-- Name: province_id_seq; Type: SEQUENCE SET; Schema: idsmed; Owner: dbadmin
--

SELECT pg_catalog.setval('idsmed.province_id_seq', 34, true);


--
-- TOC entry 3945 (class 0 OID 98075)
-- Dependencies: 329
-- Data for Name: schedule_setting; Type: TABLE DATA; Schema: idsmed; Owner: dbadmin
--

INSERT INTO idsmed.schedule_setting VALUES (1, 'CRJ0001', NULL, 86400, '2018-12-31 17:10:50.869135', '2018-12-31 17:10:50.869135', 1, 1, 1);
INSERT INTO idsmed.schedule_setting VALUES (2, 'CRJ0002', NULL, 86400, '2018-12-31 17:10:50.869135', '2018-12-31 17:10:50.869135', 1, 1, 1);
INSERT INTO idsmed.schedule_setting VALUES (3, 'CRJ0003', '0 0 2 * * ?', 0, '2019-01-30 17:59:01.125985', '2019-01-30 17:59:01.125985', 1, 1, 1);


--
-- TOC entry 4159 (class 0 OID 0)
-- Dependencies: 330
-- Name: schedule_setting_id_seq; Type: SEQUENCE SET; Schema: idsmed; Owner: dbadmin
--

SELECT pg_catalog.setval('idsmed.schedule_setting_id_seq', 3, true);


--
-- TOC entry 3947 (class 0 OID 98088)
-- Dependencies: 331
-- Data for Name: subscriber; Type: TABLE DATA; Schema: idsmed; Owner: dbadmin
--



--
-- TOC entry 3948 (class 0 OID 98099)
-- Dependencies: 332
-- Data for Name: subscriber_access_right; Type: TABLE DATA; Schema: idsmed; Owner: dbadmin
--



--
-- TOC entry 4160 (class 0 OID 0)
-- Dependencies: 333
-- Name: subscriber_access_right_id_seq; Type: SEQUENCE SET; Schema: idsmed; Owner: dbadmin
--

SELECT pg_catalog.setval('idsmed.subscriber_access_right_id_seq', 1, false);


--
-- TOC entry 4161 (class 0 OID 0)
-- Dependencies: 334
-- Name: subscriber_id_seq; Type: SEQUENCE SET; Schema: idsmed; Owner: dbadmin
--

SELECT pg_catalog.setval('idsmed.subscriber_id_seq', 2, false);


--
-- TOC entry 3951 (class 0 OID 98111)
-- Dependencies: 335
-- Data for Name: subscriber_ip; Type: TABLE DATA; Schema: idsmed; Owner: dbadmin
--



--
-- TOC entry 4162 (class 0 OID 0)
-- Dependencies: 336
-- Name: subscriber_ip_id_seq; Type: SEQUENCE SET; Schema: idsmed; Owner: dbadmin
--

SELECT pg_catalog.setval('idsmed.subscriber_ip_id_seq', 3, false);


--
-- TOC entry 3953 (class 0 OID 98121)
-- Dependencies: 337
-- Data for Name: tender_category; Type: TABLE DATA; Schema: idsmed; Owner: dbadmin
--



--
-- TOC entry 4163 (class 0 OID 0)
-- Dependencies: 338
-- Name: tender_category_id_seq; Type: SEQUENCE SET; Schema: idsmed; Owner: dbadmin
--

SELECT pg_catalog.setval('idsmed.tender_category_id_seq', 1, false);


--
-- TOC entry 3955 (class 0 OID 98131)
-- Dependencies: 339
-- Data for Name: token_information; Type: TABLE DATA; Schema: idsmed; Owner: dbadmin
--



--
-- TOC entry 4164 (class 0 OID 0)
-- Dependencies: 340
-- Name: token_information_id_seq; Type: SEQUENCE SET; Schema: idsmed; Owner: dbadmin
--

SELECT pg_catalog.setval('idsmed.token_information_id_seq', 1, false);


--
-- TOC entry 3957 (class 0 OID 98146)
-- Dependencies: 341
-- Data for Name: vendor; Type: TABLE DATA; Schema: idsmed; Owner: dbadmin
--



--
-- TOC entry 3958 (class 0 OID 98157)
-- Dependencies: 342
-- Data for Name: vendor_action_log; Type: TABLE DATA; Schema: idsmed; Owner: dbadmin
--



--
-- TOC entry 4165 (class 0 OID 0)
-- Dependencies: 343
-- Name: vendor_action_log_id_seq; Type: SEQUENCE SET; Schema: idsmed; Owner: dbadmin
--

SELECT pg_catalog.setval('idsmed.vendor_action_log_id_seq', 2, true);


--
-- TOC entry 3960 (class 0 OID 98166)
-- Dependencies: 344
-- Data for Name: vendor_additional_info; Type: TABLE DATA; Schema: idsmed; Owner: dbadmin
--



--
-- TOC entry 4166 (class 0 OID 0)
-- Dependencies: 345
-- Name: vendor_additional_info_id_seq; Type: SEQUENCE SET; Schema: idsmed; Owner: dbadmin
--

SELECT pg_catalog.setval('idsmed.vendor_additional_info_id_seq', 99, true);


--
-- TOC entry 3962 (class 0 OID 98179)
-- Dependencies: 346
-- Data for Name: vendor_address; Type: TABLE DATA; Schema: idsmed; Owner: dbadmin
--



--
-- TOC entry 4167 (class 0 OID 0)
-- Dependencies: 347
-- Name: vendor_address_id_seq; Type: SEQUENCE SET; Schema: idsmed; Owner: dbadmin
--

SELECT pg_catalog.setval('idsmed.vendor_address_id_seq', 269, true);


--
-- TOC entry 3964 (class 0 OID 98193)
-- Dependencies: 348
-- Data for Name: vendor_file_attachment_reminder; Type: TABLE DATA; Schema: idsmed; Owner: dbadmin
--



--
-- TOC entry 4168 (class 0 OID 0)
-- Dependencies: 349
-- Name: vendor_file_attachment_reminder_id_seq; Type: SEQUENCE SET; Schema: idsmed; Owner: dbadmin
--

SELECT pg_catalog.setval('idsmed.vendor_file_attachment_reminder_id_seq', 284, true);


--
-- TOC entry 4169 (class 0 OID 0)
-- Dependencies: 350
-- Name: vendor_id_seq; Type: SEQUENCE SET; Schema: idsmed; Owner: dbadmin
--

SELECT pg_catalog.setval('idsmed.vendor_id_seq', 148, true);


--
-- TOC entry 3967 (class 0 OID 98209)
-- Dependencies: 351
-- Data for Name: vendor_info_config; Type: TABLE DATA; Schema: idsmed; Owner: dbadmin
--



--
-- TOC entry 4170 (class 0 OID 0)
-- Dependencies: 352
-- Name: vendor_info_config_id_seq; Type: SEQUENCE SET; Schema: idsmed; Owner: dbadmin
--

SELECT pg_catalog.setval('idsmed.vendor_info_config_id_seq', 11, true);


--
-- TOC entry 3969 (class 0 OID 98219)
-- Dependencies: 353
-- Data for Name: vendor_label_config; Type: TABLE DATA; Schema: idsmed; Owner: dbadmin
--



--
-- TOC entry 4171 (class 0 OID 0)
-- Dependencies: 354
-- Name: vendor_label_config_id_seq; Type: SEQUENCE SET; Schema: idsmed; Owner: dbadmin
--

SELECT pg_catalog.setval('idsmed.vendor_label_config_id_seq', 22, true);


--
-- TOC entry 3971 (class 0 OID 98234)
-- Dependencies: 355
-- Data for Name: vendor_reject; Type: TABLE DATA; Schema: idsmed; Owner: dbadmin
--



--
-- TOC entry 4172 (class 0 OID 0)
-- Dependencies: 356
-- Name: vendor_reject_id_seq; Type: SEQUENCE SET; Schema: idsmed; Owner: dbadmin
--

SELECT pg_catalog.setval('idsmed.vendor_reject_id_seq', 43, true);


--
-- TOC entry 3973 (class 0 OID 98247)
-- Dependencies: 357
-- Data for Name: verifying_work_flow_setting; Type: TABLE DATA; Schema: idsmed; Owner: dbadmin
--

INSERT INTO idsmed.verifying_work_flow_setting VALUES (1, 'cn', 'VWFP0001', '2,3', 'Product verifying for china market', '2019-01-22 09:22:37.17827', '2019-01-22 09:22:37.17827', 1, 1, 1);
INSERT INTO idsmed.verifying_work_flow_setting VALUES (2, 'cn', 'VWV0001', '2,3', 'Vendor verifying for china market', '2019-01-22 09:22:37.17827', '2019-01-22 09:22:37.17827', 1, 1, 1);
INSERT INTO idsmed.verifying_work_flow_setting VALUES (3, 'cn', 'VWU0001', '2,3', 'User verifying for china market', '2019-01-22 09:22:37.17827', '2019-01-22 09:22:37.17827', 1, 1, 1);


--
-- TOC entry 4173 (class 0 OID 0)
-- Dependencies: 358
-- Name: verifying_work_flow_setting_id_seq; Type: SEQUENCE SET; Schema: idsmed; Owner: dbadmin
--

SELECT pg_catalog.setval('idsmed.verifying_work_flow_setting_id_seq', 3, true);


--
-- TOC entry 3975 (class 0 OID 98260)
-- Dependencies: 359
-- Data for Name: webservice_log; Type: TABLE DATA; Schema: idsmed; Owner: dbadmin
--



--
-- TOC entry 4174 (class 0 OID 0)
-- Dependencies: 360
-- Name: webservice_log_id_seq; Type: SEQUENCE SET; Schema: idsmed; Owner: dbadmin
--

SELECT pg_catalog.setval('idsmed.webservice_log_id_seq', 8633, true);


--
-- TOC entry 3977 (class 0 OID 98273)
-- Dependencies: 361
-- Data for Name: idsmed_function; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 4175 (class 0 OID 0)
-- Dependencies: 362
-- Name: idsmed_function_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.idsmed_function_id_seq', 1, false);


--
-- TOC entry 3979 (class 0 OID 98286)
-- Dependencies: 363
-- Data for Name: idsmed_permission; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 4176 (class 0 OID 0)
-- Dependencies: 364
-- Name: idsmed_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.idsmed_permission_id_seq', 1, false);


--
-- TOC entry 3981 (class 0 OID 98299)
-- Dependencies: 365
-- Data for Name: webservice_log; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 3983 (class 0 OID 0)
-- Data for Name: BLOBS; Type: BLOBS; Schema: -; Owner:
--

BEGIN;

SELECT pg_catalog.lo_open('20456', 131072);
SELECT pg_catalog.lowrite(0, '\x596f7572206c6f67696e49642069733a20746573746e6f74696669636174696f6e');
SELECT pg_catalog.lo_close(0);

COMMIT;

--
-- TOC entry 3384 (class 2606 OID 98392)
-- Name: account_notification_setting account_notification_setting_pkey; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.account_notification_setting
    ADD CONSTRAINT account_notification_setting_pkey PRIMARY KEY (id);


--
-- TOC entry 3388 (class 2606 OID 98394)
-- Name: batch_job_execution_context batch_job_execution_context_pkey; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.batch_job_execution_context
    ADD CONSTRAINT batch_job_execution_context_pkey PRIMARY KEY (job_execution_id);


--
-- TOC entry 3386 (class 2606 OID 98396)
-- Name: batch_job_execution batch_job_execution_pkey; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.batch_job_execution
    ADD CONSTRAINT batch_job_execution_pkey PRIMARY KEY (job_execution_id);


--
-- TOC entry 3390 (class 2606 OID 98398)
-- Name: batch_job_instance batch_job_instance_pkey; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.batch_job_instance
    ADD CONSTRAINT batch_job_instance_pkey PRIMARY KEY (job_instance_id);


--
-- TOC entry 3396 (class 2606 OID 98400)
-- Name: batch_step_execution_context batch_step_execution_context_pkey; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.batch_step_execution_context
    ADD CONSTRAINT batch_step_execution_context_pkey PRIMARY KEY (step_execution_id);


--
-- TOC entry 3394 (class 2606 OID 98402)
-- Name: batch_step_execution batch_step_execution_pkey; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.batch_step_execution
    ADD CONSTRAINT batch_step_execution_pkey PRIMARY KEY (step_execution_id);


--
-- TOC entry 3398 (class 2606 OID 98404)
-- Name: best_seller_product_global best_seller_product_global_pkey; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.best_seller_product_global
    ADD CONSTRAINT best_seller_product_global_pkey PRIMARY KEY (id);


--
-- TOC entry 3400 (class 2606 OID 98406)
-- Name: company_background_info company_background_info_company_code_key; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.company_background_info
    ADD CONSTRAINT company_background_info_company_code_key UNIQUE (company_code);


--
-- TOC entry 3402 (class 2606 OID 98408)
-- Name: company_background_info company_background_info_pkey; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.company_background_info
    ADD CONSTRAINT company_background_info_pkey PRIMARY KEY (id);


--
-- TOC entry 3404 (class 2606 OID 98410)
-- Name: country country_pkey; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.country
    ADD CONSTRAINT country_pkey PRIMARY KEY (id);


--
-- TOC entry 3406 (class 2606 OID 98412)
-- Name: currency_setting currency_setting_country_code_key; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.currency_setting
    ADD CONSTRAINT currency_setting_country_code_key UNIQUE (country_code);


--
-- TOC entry 3408 (class 2606 OID 98414)
-- Name: currency_setting currency_setting_pkey; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.currency_setting
    ADD CONSTRAINT currency_setting_pkey PRIMARY KEY (id);


--
-- TOC entry 3410 (class 2606 OID 98416)
-- Name: customer customer_pkey; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.customer
    ADD CONSTRAINT customer_pkey PRIMARY KEY (id);


--
-- TOC entry 3412 (class 2606 OID 98418)
-- Name: email_notification_config email_notification_config_email_type_key; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.email_notification_config
    ADD CONSTRAINT email_notification_config_email_type_key UNIQUE (email_type);


--
-- TOC entry 3414 (class 2606 OID 98420)
-- Name: email_notification_config email_notification_config_pkey; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.email_notification_config
    ADD CONSTRAINT email_notification_config_pkey PRIMARY KEY (id);


--
-- TOC entry 3416 (class 2606 OID 98422)
-- Name: email_notification_config email_notification_config_unique_template_name_key; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.email_notification_config
    ADD CONSTRAINT email_notification_config_unique_template_name_key UNIQUE (notification_name);


--
-- TOC entry 3418 (class 2606 OID 98424)
-- Name: email_template email_template_pkey; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.email_template
    ADD CONSTRAINT email_template_pkey PRIMARY KEY (id);


--
-- TOC entry 3420 (class 2606 OID 98426)
-- Name: email_template email_template_unique_template_name_key; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.email_template
    ADD CONSTRAINT email_template_unique_template_name_key UNIQUE (template_name);


--
-- TOC entry 3422 (class 2606 OID 98428)
-- Name: favourite_product_brand favourite_product_brand_pkey; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.favourite_product_brand
    ADD CONSTRAINT favourite_product_brand_pkey PRIMARY KEY (id);


--
-- TOC entry 3424 (class 2606 OID 98430)
-- Name: favourite_product_care_area favourite_product_care_area_pkey; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.favourite_product_care_area
    ADD CONSTRAINT favourite_product_care_area_pkey PRIMARY KEY (id);


--
-- TOC entry 3426 (class 2606 OID 98432)
-- Name: file_attachment file_attachment_pkey; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.file_attachment
    ADD CONSTRAINT file_attachment_pkey PRIMARY KEY (id);


--
-- TOC entry 3428 (class 2606 OID 98434)
-- Name: flyway_schema_history flyway_schema_history_pk; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

-- ALTER TABLE ONLY idsmed.flyway_schema_history
--     ADD CONSTRAINT flyway_schema_history_pk PRIMARY KEY (installed_rank);


--
-- TOC entry 3431 (class 2606 OID 98436)
-- Name: history_user_product_by_search history_user_product_by_search_pkey; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.history_user_product_by_search
    ADD CONSTRAINT history_user_product_by_search_pkey PRIMARY KEY (id);


--
-- TOC entry 3433 (class 2606 OID 98438)
-- Name: idsmed_account idsmed_account_login_id_key; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.idsmed_account
    ADD CONSTRAINT idsmed_account_login_id_key UNIQUE (login_id);


--
-- TOC entry 3435 (class 2606 OID 98440)
-- Name: idsmed_account idsmed_account_pkey; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.idsmed_account
    ADD CONSTRAINT idsmed_account_pkey PRIMARY KEY (id);


--
-- TOC entry 3437 (class 2606 OID 98442)
-- Name: idsmed_account_settings idsmed_account_settings_pkey; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.idsmed_account_settings
    ADD CONSTRAINT idsmed_account_settings_pkey PRIMARY KEY (id);


--
-- TOC entry 3439 (class 2606 OID 98444)
-- Name: idsmed_function idsmed_function_pkey; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.idsmed_function
    ADD CONSTRAINT idsmed_function_pkey PRIMARY KEY (id);


--
-- TOC entry 3441 (class 2606 OID 98446)
-- Name: idsmed_permission idsmed_permission_pkey; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.idsmed_permission
    ADD CONSTRAINT idsmed_permission_pkey PRIMARY KEY (id);


--
-- TOC entry 3443 (class 2606 OID 98448)
-- Name: idsmed_permission idsmed_permission_unique_code_key; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.idsmed_permission
    ADD CONSTRAINT idsmed_permission_unique_code_key UNIQUE (code);


--
-- TOC entry 3449 (class 2606 OID 98450)
-- Name: idsmed_role_permission idsmed_role_permission_pkey; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.idsmed_role_permission
    ADD CONSTRAINT idsmed_role_permission_pkey PRIMARY KEY (id);


--
-- TOC entry 3445 (class 2606 OID 98452)
-- Name: idsmed_role idsmed_role_pkey; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.idsmed_role
    ADD CONSTRAINT idsmed_role_pkey PRIMARY KEY (id);


--
-- TOC entry 3447 (class 2606 OID 98454)
-- Name: idsmed_role idsmed_role_unique_code_key; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.idsmed_role
    ADD CONSTRAINT idsmed_role_unique_code_key UNIQUE (code);


--
-- TOC entry 3451 (class 2606 OID 98456)
-- Name: idsmed_subscriber_role idsmed_subscriber_role_pkey; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.idsmed_subscriber_role
    ADD CONSTRAINT idsmed_subscriber_role_pkey PRIMARY KEY (id);


--
-- TOC entry 3455 (class 2606 OID 98458)
-- Name: idsmed_task_executor idsmed_task_executor_pkey; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.idsmed_task_executor
    ADD CONSTRAINT idsmed_task_executor_pkey PRIMARY KEY (id);


--
-- TOC entry 3453 (class 2606 OID 98460)
-- Name: idsmed_task idsmed_task_pkey; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.idsmed_task
    ADD CONSTRAINT idsmed_task_pkey PRIMARY KEY (id);


--
-- TOC entry 3457 (class 2606 OID 98462)
-- Name: idsmed_user idsmed_user_pkey; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.idsmed_user
    ADD CONSTRAINT idsmed_user_pkey PRIMARY KEY (id);


--
-- TOC entry 3459 (class 2606 OID 98464)
-- Name: idsmed_user_role idsmed_user_role_pkey; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.idsmed_user_role
    ADD CONSTRAINT idsmed_user_role_pkey PRIMARY KEY (id);


--
-- TOC entry 3392 (class 2606 OID 98466)
-- Name: batch_job_instance job_inst_un; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.batch_job_instance
    ADD CONSTRAINT job_inst_un UNIQUE (job_name, job_key);


--
-- TOC entry 3461 (class 2606 OID 98468)
-- Name: keyword_history keyword_history_pkey; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.keyword_history
    ADD CONSTRAINT keyword_history_pkey PRIMARY KEY (id);


--
-- TOC entry 3463 (class 2606 OID 98470)
-- Name: mail_sending_log mail_sending_log_pkey; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.mail_sending_log
    ADD CONSTRAINT mail_sending_log_pkey PRIMARY KEY (id);


--
-- TOC entry 3465 (class 2606 OID 98472)
-- Name: notification notification_pkey; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.notification
    ADD CONSTRAINT notification_pkey PRIMARY KEY (id);


--
-- TOC entry 3467 (class 2606 OID 98474)
-- Name: notification_recipient notification_recipient_pkey; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.notification_recipient
    ADD CONSTRAINT notification_recipient_pkey PRIMARY KEY (id);


--
-- TOC entry 3469 (class 2606 OID 98476)
-- Name: notification_wildcard notification_wildcard_pkey; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.notification_wildcard
    ADD CONSTRAINT notification_wildcard_pkey PRIMARY KEY (id);


--
-- TOC entry 3471 (class 2606 OID 98478)
-- Name: notification_wildcard notification_wildcard_unique_wildcard_name_key; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.notification_wildcard
    ADD CONSTRAINT notification_wildcard_unique_wildcard_name_key UNIQUE (wildcard_name);


--
-- TOC entry 3473 (class 2606 OID 98480)
-- Name: od_big_order od_big_order_pkey; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.od_big_order
    ADD CONSTRAINT od_big_order_pkey PRIMARY KEY (id);


--
-- TOC entry 3475 (class 2606 OID 98482)
-- Name: od_delivery_product od_delivery_product_pkey; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.od_delivery_product
    ADD CONSTRAINT od_delivery_product_pkey PRIMARY KEY (id);


--
-- TOC entry 3477 (class 2606 OID 98484)
-- Name: od_order_delivery od_order_delivery_pkey; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.od_order_delivery
    ADD CONSTRAINT od_order_delivery_pkey PRIMARY KEY (id);


--
-- TOC entry 3479 (class 2606 OID 98486)
-- Name: od_order_delivery_record od_order_delivery_record_pkey; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.od_order_delivery_record
    ADD CONSTRAINT od_order_delivery_record_pkey PRIMARY KEY (id);


--
-- TOC entry 3481 (class 2606 OID 98488)
-- Name: od_order_item od_order_item_pkey; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.od_order_item
    ADD CONSTRAINT od_order_item_pkey PRIMARY KEY (id);


--
-- TOC entry 3483 (class 2606 OID 98490)
-- Name: od_order_receive_address od_order_receive_address_pkey; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.od_order_receive_address
    ADD CONSTRAINT od_order_receive_address_pkey PRIMARY KEY (id);


--
-- TOC entry 3485 (class 2606 OID 98492)
-- Name: od_small_order od_small_order_pkey; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.od_small_order
    ADD CONSTRAINT od_small_order_pkey PRIMARY KEY (id);


--
-- TOC entry 3489 (class 2606 OID 98494)
-- Name: product_brand product_brand_pkey; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_brand
    ADD CONSTRAINT product_brand_pkey PRIMARY KEY (id);


--
-- TOC entry 3491 (class 2606 OID 98496)
-- Name: product_brand product_brand_unique_brand_name_key; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_brand
    ADD CONSTRAINT product_brand_unique_brand_name_key UNIQUE (brand_name);


--
-- TOC entry 3497 (class 2606 OID 98498)
-- Name: product_care_area_detail_log product_care_area_detail_log_pkey; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_care_area_detail_log
    ADD CONSTRAINT product_care_area_detail_log_pkey PRIMARY KEY (id);


--
-- TOC entry 3495 (class 2606 OID 98500)
-- Name: product_care_area_detail product_care_area_detail_pkey; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_care_area_detail
    ADD CONSTRAINT product_care_area_detail_pkey PRIMARY KEY (id);


--
-- TOC entry 3493 (class 2606 OID 98502)
-- Name: product_care_area product_care_area_pkey; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_care_area
    ADD CONSTRAINT product_care_area_pkey PRIMARY KEY (id);


--
-- TOC entry 3499 (class 2606 OID 98504)
-- Name: product_category product_category_pkey; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_category
    ADD CONSTRAINT product_category_pkey PRIMARY KEY (id);


--
-- TOC entry 3501 (class 2606 OID 98506)
-- Name: product_certificate_log product_certificate_log_pkey; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_certificate_log
    ADD CONSTRAINT product_certificate_log_pkey PRIMARY KEY (id);


--
-- TOC entry 3503 (class 2606 OID 98508)
-- Name: product_csv_log product_csv_log_pkey; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_csv_log
    ADD CONSTRAINT product_csv_log_pkey PRIMARY KEY (id);


--
-- TOC entry 3505 (class 2606 OID 98510)
-- Name: product_document_attachment product_document_attachment_pkey; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_document_attachment
    ADD CONSTRAINT product_document_attachment_pkey PRIMARY KEY (id);


--
-- TOC entry 3507 (class 2606 OID 98512)
-- Name: product_document_attachment_reminder product_document_attachment_reminder_pkey; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_document_attachment_reminder
    ADD CONSTRAINT product_document_attachment_reminder_pkey PRIMARY KEY (id);


--
-- TOC entry 3511 (class 2606 OID 98514)
-- Name: product_features_log product_features_log_pkey; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_features_log
    ADD CONSTRAINT product_features_log_pkey PRIMARY KEY (id);


--
-- TOC entry 3509 (class 2606 OID 98516)
-- Name: product_features product_features_pkey; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_features
    ADD CONSTRAINT product_features_pkey PRIMARY KEY (id);


--
-- TOC entry 3513 (class 2606 OID 98518)
-- Name: product_hierarchy product_hierarchy_pkey; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_hierarchy
    ADD CONSTRAINT product_hierarchy_pkey PRIMARY KEY (id);


--
-- TOC entry 3515 (class 2606 OID 98520)
-- Name: product_info_config product_info_config_pkey; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_info_config
    ADD CONSTRAINT product_info_config_pkey PRIMARY KEY (id);


--
-- TOC entry 3517 (class 2606 OID 98522)
-- Name: product_label_config product_label_config_pkey; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_label_config
    ADD CONSTRAINT product_label_config_pkey PRIMARY KEY (id);


--
-- TOC entry 3519 (class 2606 OID 98524)
-- Name: product_log product_log_pkey; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_log
    ADD CONSTRAINT product_log_pkey PRIMARY KEY (id);


--
-- TOC entry 3523 (class 2606 OID 98526)
-- Name: product_media_log product_media_log_pkey; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_media_log
    ADD CONSTRAINT product_media_log_pkey PRIMARY KEY (id);


--
-- TOC entry 3521 (class 2606 OID 98528)
-- Name: product_media product_media_pkey; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_media
    ADD CONSTRAINT product_media_pkey PRIMARY KEY (id);


--
-- TOC entry 3487 (class 2606 OID 98530)
-- Name: product product_pkey; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product
    ADD CONSTRAINT product_pkey PRIMARY KEY (id);


--
-- TOC entry 3527 (class 2606 OID 98532)
-- Name: product_rating_detail product_rating_detail_pkey; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_rating_detail
    ADD CONSTRAINT product_rating_detail_pkey PRIMARY KEY (id);


--
-- TOC entry 3525 (class 2606 OID 98534)
-- Name: product_rating product_rating_pkey; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_rating
    ADD CONSTRAINT product_rating_pkey PRIMARY KEY (id);


--
-- TOC entry 3531 (class 2606 OID 98536)
-- Name: product_reject_log product_reject_log_pkey; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_reject_log
    ADD CONSTRAINT product_reject_log_pkey PRIMARY KEY (id);


--
-- TOC entry 3529 (class 2606 OID 98538)
-- Name: product_reject product_reject_pkey; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_reject
    ADD CONSTRAINT product_reject_pkey PRIMARY KEY (id);


--
-- TOC entry 3533 (class 2606 OID 98540)
-- Name: product_related product_related_pkey; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_related
    ADD CONSTRAINT product_related_pkey PRIMARY KEY (id);


--
-- TOC entry 3539 (class 2606 OID 98542)
-- Name: product_second_category_detail product_second_category_detail_pkey; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_second_category_detail
    ADD CONSTRAINT product_second_category_detail_pkey PRIMARY KEY (id);


--
-- TOC entry 3535 (class 2606 OID 98544)
-- Name: product_second_category product_second_category_pkey; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_second_category
    ADD CONSTRAINT product_second_category_pkey PRIMARY KEY (id);


--
-- TOC entry 3537 (class 2606 OID 98546)
-- Name: product_second_category product_second_category_unique_category_name_key; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_second_category
    ADD CONSTRAINT product_second_category_unique_category_name_key UNIQUE (category_name);


--
-- TOC entry 3541 (class 2606 OID 98548)
-- Name: product_selling_history product_selling_history_pkey; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_selling_history
    ADD CONSTRAINT product_selling_history_pkey PRIMARY KEY (id);


--
-- TOC entry 3543 (class 2606 OID 98550)
-- Name: product_selling_rate product_selling_rate_pkey; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_selling_rate
    ADD CONSTRAINT product_selling_rate_pkey PRIMARY KEY (id);


--
-- TOC entry 3545 (class 2606 OID 98552)
-- Name: product_tech_feature product_tech_feature_pk; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_tech_feature
    ADD CONSTRAINT product_tech_feature_pk PRIMARY KEY (id);


--
-- TOC entry 3547 (class 2606 OID 98554)
-- Name: product_tech_feature product_tech_feature_unique_label_name_id_key; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_tech_feature
    ADD CONSTRAINT product_tech_feature_unique_label_name_id_key UNIQUE (label_name_id);


--
-- TOC entry 3549 (class 2606 OID 98556)
-- Name: product_tech_feature product_tech_feature_unique_label_name_key; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_tech_feature
    ADD CONSTRAINT product_tech_feature_unique_label_name_key UNIQUE (label_name);


--
-- TOC entry 3551 (class 2606 OID 98558)
-- Name: product_tech_feature product_tech_feature_unique_label_name_th_key; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_tech_feature
    ADD CONSTRAINT product_tech_feature_unique_label_name_th_key UNIQUE (label_name_th);


--
-- TOC entry 3553 (class 2606 OID 98560)
-- Name: product_tech_feature product_tech_feature_unique_label_name_vi_key; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_tech_feature
    ADD CONSTRAINT product_tech_feature_unique_label_name_vi_key UNIQUE (label_name_vi);


--
-- TOC entry 3555 (class 2606 OID 98562)
-- Name: product_tech_feature product_tech_feature_unique_label_name_zh_cn_key; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_tech_feature
    ADD CONSTRAINT product_tech_feature_unique_label_name_zh_cn_key UNIQUE (label_name_zh_cn);


--
-- TOC entry 3557 (class 2606 OID 98564)
-- Name: product_tech_feature product_tech_feature_unique_label_name_zh_tw_key; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_tech_feature
    ADD CONSTRAINT product_tech_feature_unique_label_name_zh_tw_key UNIQUE (label_name_zh_tw);


--
-- TOC entry 3561 (class 2606 OID 98566)
-- Name: product_technical_log product_technical_log_pkey; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_technical_log
    ADD CONSTRAINT product_technical_log_pkey PRIMARY KEY (id);


--
-- TOC entry 3559 (class 2606 OID 98568)
-- Name: product_technical product_technical_pkey; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_technical
    ADD CONSTRAINT product_technical_pkey PRIMARY KEY (id);


--
-- TOC entry 3565 (class 2606 OID 98570)
-- Name: product_wishlist_detail product_wishlist_detail_pkey; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_wishlist_detail
    ADD CONSTRAINT product_wishlist_detail_pkey PRIMARY KEY (id);


--
-- TOC entry 3563 (class 2606 OID 98572)
-- Name: product_wishlist product_wishlist_pkey; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_wishlist
    ADD CONSTRAINT product_wishlist_pkey PRIMARY KEY (id);


--
-- TOC entry 3567 (class 2606 OID 98574)
-- Name: province province_pkey; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.province
    ADD CONSTRAINT province_pkey PRIMARY KEY (id);


--
-- TOC entry 3569 (class 2606 OID 98576)
-- Name: schedule_setting schedule_setting_pkey; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.schedule_setting
    ADD CONSTRAINT schedule_setting_pkey PRIMARY KEY (id);


--
-- TOC entry 3571 (class 2606 OID 98578)
-- Name: schedule_setting schedule_setting_schedule_task_code_key; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.schedule_setting
    ADD CONSTRAINT schedule_setting_schedule_task_code_key UNIQUE (schedule_task_code);


--
-- TOC entry 3575 (class 2606 OID 98580)
-- Name: subscriber_access_right subscriber_access_right_pkey; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.subscriber_access_right
    ADD CONSTRAINT subscriber_access_right_pkey PRIMARY KEY (id);


--
-- TOC entry 3577 (class 2606 OID 98582)
-- Name: subscriber_ip subscriber_ip_pkey; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.subscriber_ip
    ADD CONSTRAINT subscriber_ip_pkey PRIMARY KEY (id);


--
-- TOC entry 3573 (class 2606 OID 98584)
-- Name: subscriber subscriber_pkey; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.subscriber
    ADD CONSTRAINT subscriber_pkey PRIMARY KEY (id);


--
-- TOC entry 3579 (class 2606 OID 98586)
-- Name: tender_category tender_category_pkey; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.tender_category
    ADD CONSTRAINT tender_category_pkey PRIMARY KEY (id);


--
-- TOC entry 3581 (class 2606 OID 98588)
-- Name: token_information token_information_pkey; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.token_information
    ADD CONSTRAINT token_information_pkey PRIMARY KEY (id);


--
-- TOC entry 3585 (class 2606 OID 98590)
-- Name: vendor_action_log vendor_action_log_pkey; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.vendor_action_log
    ADD CONSTRAINT vendor_action_log_pkey PRIMARY KEY (id);


--
-- TOC entry 3587 (class 2606 OID 98592)
-- Name: vendor_additional_info vendor_additional_info_pkey; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.vendor_additional_info
    ADD CONSTRAINT vendor_additional_info_pkey PRIMARY KEY (id);


--
-- TOC entry 3589 (class 2606 OID 98594)
-- Name: vendor_address vendor_address_pkey; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.vendor_address
    ADD CONSTRAINT vendor_address_pkey PRIMARY KEY (id);


--
-- TOC entry 3591 (class 2606 OID 98596)
-- Name: vendor_file_attachment_reminder vendor_file_attachment_reminder_pkey; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.vendor_file_attachment_reminder
    ADD CONSTRAINT vendor_file_attachment_reminder_pkey PRIMARY KEY (id);


--
-- TOC entry 3593 (class 2606 OID 98598)
-- Name: vendor_info_config vendor_info_config_pkey; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.vendor_info_config
    ADD CONSTRAINT vendor_info_config_pkey PRIMARY KEY (id);


--
-- TOC entry 3595 (class 2606 OID 98600)
-- Name: vendor_label_config vendor_label_config_pkey; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.vendor_label_config
    ADD CONSTRAINT vendor_label_config_pkey PRIMARY KEY (id);


--
-- TOC entry 3583 (class 2606 OID 98602)
-- Name: vendor vendor_pkey; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.vendor
    ADD CONSTRAINT vendor_pkey PRIMARY KEY (id);


--
-- TOC entry 3597 (class 2606 OID 98604)
-- Name: vendor_reject vendor_reject_pkey; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.vendor_reject
    ADD CONSTRAINT vendor_reject_pkey PRIMARY KEY (id);


--
-- TOC entry 3599 (class 2606 OID 98606)
-- Name: verifying_work_flow_setting verifying_work_flow_setting_country_code_work_flow_code_key; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.verifying_work_flow_setting
    ADD CONSTRAINT verifying_work_flow_setting_country_code_work_flow_code_key UNIQUE (country_code, work_flow_code);


--
-- TOC entry 3601 (class 2606 OID 98608)
-- Name: verifying_work_flow_setting verifying_work_flow_setting_pkey; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.verifying_work_flow_setting
    ADD CONSTRAINT verifying_work_flow_setting_pkey PRIMARY KEY (id);


--
-- TOC entry 3603 (class 2606 OID 98610)
-- Name: webservice_log webservice_log_pkey; Type: CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.webservice_log
    ADD CONSTRAINT webservice_log_pkey PRIMARY KEY (id);


--
-- TOC entry 3605 (class 2606 OID 98612)
-- Name: idsmed_function idsmed_function_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.idsmed_function
    ADD CONSTRAINT idsmed_function_pkey PRIMARY KEY (id);


--
-- TOC entry 3607 (class 2606 OID 98614)
-- Name: idsmed_permission idsmed_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.idsmed_permission
    ADD CONSTRAINT idsmed_permission_pkey PRIMARY KEY (id);


--
-- TOC entry 3609 (class 2606 OID 98616)
-- Name: webservice_log webservice_log_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.webservice_log
    ADD CONSTRAINT webservice_log_pkey PRIMARY KEY (id);


--
-- TOC entry 3429 (class 1259 OID 98617)
-- Name: flyway_schema_history_s_idx; Type: INDEX; Schema: idsmed; Owner: dbadmin
--

-- CREATE INDEX flyway_schema_history_s_idx ON idsmed.flyway_schema_history USING btree (success);


--
-- TOC entry 3610 (class 2606 OID 98618)
-- Name: account_notification_setting account_notification_setting_idsmed_account_fk; Type: FK CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.account_notification_setting
    ADD CONSTRAINT account_notification_setting_idsmed_account_fk FOREIGN KEY (idsmed_account_id) REFERENCES idsmed.idsmed_account(id);


--
-- TOC entry 3616 (class 2606 OID 98623)
-- Name: best_seller_product_global best_seller_product_global_product_fk; Type: FK CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.best_seller_product_global
    ADD CONSTRAINT best_seller_product_global_product_fk FOREIGN KEY (product_id) REFERENCES idsmed.product(id);


--
-- TOC entry 3617 (class 2606 OID 98628)
-- Name: email_notification_config email_notification_config_email_template_pk; Type: FK CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.email_notification_config
    ADD CONSTRAINT email_notification_config_email_template_pk FOREIGN KEY (template_id) REFERENCES idsmed.email_template(id);


--
-- TOC entry 3618 (class 2606 OID 98633)
-- Name: file_attachment file_attachment_vendor_label_config_fk; Type: FK CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.file_attachment
    ADD CONSTRAINT file_attachment_vendor_label_config_fk FOREIGN KEY (vendor_label_config_id) REFERENCES idsmed.vendor_label_config(id);


--
-- TOC entry 3619 (class 2606 OID 98638)
-- Name: file_attachment file_attachment_vendor_pk; Type: FK CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.file_attachment
    ADD CONSTRAINT file_attachment_vendor_pk FOREIGN KEY (vendor_id) REFERENCES idsmed.vendor(id);


--
-- TOC entry 3620 (class 2606 OID 98643)
-- Name: history_user_product_by_search history_user_product_by_search_product_fk; Type: FK CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.history_user_product_by_search
    ADD CONSTRAINT history_user_product_by_search_product_fk FOREIGN KEY (product_id) REFERENCES idsmed.product(id);


--
-- TOC entry 3621 (class 2606 OID 98648)
-- Name: idsmed_account idsmed_account_idsmed_user_pk; Type: FK CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.idsmed_account
    ADD CONSTRAINT idsmed_account_idsmed_user_pk FOREIGN KEY (idsmed_user_id) REFERENCES idsmed.idsmed_user(id);


--
-- TOC entry 3623 (class 2606 OID 98653)
-- Name: idsmed_account_settings idsmed_account_settings_idsmed_account_pk; Type: FK CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.idsmed_account_settings
    ADD CONSTRAINT idsmed_account_settings_idsmed_account_pk FOREIGN KEY (account_id) REFERENCES idsmed.idsmed_account(id);


--
-- TOC entry 3622 (class 2606 OID 98658)
-- Name: idsmed_account idsmed_account_subscriber_pk; Type: FK CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.idsmed_account
    ADD CONSTRAINT idsmed_account_subscriber_pk FOREIGN KEY (subscriber_id) REFERENCES idsmed.subscriber(id);


--
-- TOC entry 3624 (class 2606 OID 98663)
-- Name: idsmed_permission idsmed_permission_idsmed_function; Type: FK CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.idsmed_permission
    ADD CONSTRAINT idsmed_permission_idsmed_function FOREIGN KEY (function_id) REFERENCES idsmed.idsmed_function(id);


--
-- TOC entry 3625 (class 2606 OID 98668)
-- Name: idsmed_role_permission idsmed_role_permission_idsmed_permission_pk; Type: FK CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.idsmed_role_permission
    ADD CONSTRAINT idsmed_role_permission_idsmed_permission_pk FOREIGN KEY (permission_id) REFERENCES idsmed.idsmed_permission(id);


--
-- TOC entry 3626 (class 2606 OID 98673)
-- Name: idsmed_role_permission idsmed_role_permission_idsmed_role_pk; Type: FK CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.idsmed_role_permission
    ADD CONSTRAINT idsmed_role_permission_idsmed_role_pk FOREIGN KEY (role_id) REFERENCES idsmed.idsmed_role(id);


--
-- TOC entry 3627 (class 2606 OID 98678)
-- Name: idsmed_subscriber_role idsmed_subscriber_role_idsmed_role_pk; Type: FK CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.idsmed_subscriber_role
    ADD CONSTRAINT idsmed_subscriber_role_idsmed_role_pk FOREIGN KEY (role_id) REFERENCES idsmed.idsmed_role(id);


--
-- TOC entry 3628 (class 2606 OID 98683)
-- Name: idsmed_subscriber_role idsmed_subscriber_role_subscriber_pk; Type: FK CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.idsmed_subscriber_role
    ADD CONSTRAINT idsmed_subscriber_role_subscriber_pk FOREIGN KEY (subscriber_id) REFERENCES idsmed.subscriber(id);


--
-- TOC entry 3629 (class 2606 OID 98688)
-- Name: idsmed_task_executor idsmed_task_executor_idsmed_account_fk; Type: FK CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.idsmed_task_executor
    ADD CONSTRAINT idsmed_task_executor_idsmed_account_fk FOREIGN KEY (idsmed_account_id) REFERENCES idsmed.idsmed_account(id);


--
-- TOC entry 3630 (class 2606 OID 98693)
-- Name: idsmed_task_executor idsmed_task_executor_idsmed_task_fk; Type: FK CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.idsmed_task_executor
    ADD CONSTRAINT idsmed_task_executor_idsmed_task_fk FOREIGN KEY (idsmed_task_id) REFERENCES idsmed.idsmed_task(id);


--
-- TOC entry 3631 (class 2606 OID 98698)
-- Name: idsmed_user_role idsmed_user_role_idsmed_role_pk; Type: FK CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.idsmed_user_role
    ADD CONSTRAINT idsmed_user_role_idsmed_role_pk FOREIGN KEY (role_id) REFERENCES idsmed.idsmed_role(id);


--
-- TOC entry 3632 (class 2606 OID 98703)
-- Name: idsmed_user_role idsmed_user_role_idsmed_user_pk; Type: FK CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.idsmed_user_role
    ADD CONSTRAINT idsmed_user_role_idsmed_user_pk FOREIGN KEY (user_id) REFERENCES idsmed.idsmed_user(id);


--
-- TOC entry 3612 (class 2606 OID 98708)
-- Name: batch_job_execution_context job_exec_ctx_fk; Type: FK CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.batch_job_execution_context
    ADD CONSTRAINT job_exec_ctx_fk FOREIGN KEY (job_execution_id) REFERENCES idsmed.batch_job_execution(job_execution_id);


--
-- TOC entry 3613 (class 2606 OID 98713)
-- Name: batch_job_execution_params job_exec_params_fk; Type: FK CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.batch_job_execution_params
    ADD CONSTRAINT job_exec_params_fk FOREIGN KEY (job_execution_id) REFERENCES idsmed.batch_job_execution(job_execution_id);


--
-- TOC entry 3614 (class 2606 OID 98718)
-- Name: batch_step_execution job_exec_step_fk; Type: FK CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.batch_step_execution
    ADD CONSTRAINT job_exec_step_fk FOREIGN KEY (job_execution_id) REFERENCES idsmed.batch_job_execution(job_execution_id);


--
-- TOC entry 3611 (class 2606 OID 98723)
-- Name: batch_job_execution job_inst_exec_fk; Type: FK CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.batch_job_execution
    ADD CONSTRAINT job_inst_exec_fk FOREIGN KEY (job_instance_id) REFERENCES idsmed.batch_job_instance(job_instance_id);


--
-- TOC entry 3633 (class 2606 OID 98728)
-- Name: notification notification_idsmed_account_pk; Type: FK CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.notification
    ADD CONSTRAINT notification_idsmed_account_pk FOREIGN KEY (idsmed_account_id) REFERENCES idsmed.idsmed_account(id);


--
-- TOC entry 3634 (class 2606 OID 98733)
-- Name: notification_recipient notification_recipient_notification_pk; Type: FK CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.notification_recipient
    ADD CONSTRAINT notification_recipient_notification_pk FOREIGN KEY (notification_id) REFERENCES idsmed.notification(id);


--
-- TOC entry 3635 (class 2606 OID 98738)
-- Name: notification_recipient notification_recipientt_idsmed_account_pk; Type: FK CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.notification_recipient
    ADD CONSTRAINT notification_recipientt_idsmed_account_pk FOREIGN KEY (idsmed_account_id) REFERENCES idsmed.idsmed_account(id);


--
-- TOC entry 3636 (class 2606 OID 98743)
-- Name: od_delivery_product od_delivery_product_od_order_delivery_fk; Type: FK CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.od_delivery_product
    ADD CONSTRAINT od_delivery_product_od_order_delivery_fk FOREIGN KEY (od_order_delivery_id) REFERENCES idsmed.od_order_delivery(id);


--
-- TOC entry 3637 (class 2606 OID 98748)
-- Name: od_order_delivery od_order_delivery_od_small_order_fk; Type: FK CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.od_order_delivery
    ADD CONSTRAINT od_order_delivery_od_small_order_fk FOREIGN KEY (od_small_order_id) REFERENCES idsmed.od_small_order(id);


--
-- TOC entry 3638 (class 2606 OID 98753)
-- Name: od_order_delivery_record od_order_delivery_record_od_order_delivery_fk; Type: FK CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.od_order_delivery_record
    ADD CONSTRAINT od_order_delivery_record_od_order_delivery_fk FOREIGN KEY (od_order_delivery_id) REFERENCES idsmed.od_order_delivery(id);


--
-- TOC entry 3639 (class 2606 OID 98758)
-- Name: od_order_item od_order_item_od_small_order_fk; Type: FK CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.od_order_item
    ADD CONSTRAINT od_order_item_od_small_order_fk FOREIGN KEY (od_small_order_id) REFERENCES idsmed.od_small_order(id);


--
-- TOC entry 3640 (class 2606 OID 98763)
-- Name: od_order_receive_address od_order_receive_address_od_small_order_fk; Type: FK CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.od_order_receive_address
    ADD CONSTRAINT od_order_receive_address_od_small_order_fk FOREIGN KEY (od_small_order_id) REFERENCES idsmed.od_small_order(id);


--
-- TOC entry 3641 (class 2606 OID 98768)
-- Name: od_small_order od_small_order_od_big_order_fk; Type: FK CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.od_small_order
    ADD CONSTRAINT od_small_order_od_big_order_fk FOREIGN KEY (od_big_order_id) REFERENCES idsmed.od_big_order(id);


--
-- TOC entry 3646 (class 2606 OID 98773)
-- Name: product_care_area_detail product_care_area_detail_product_care_area_pk; Type: FK CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_care_area_detail
    ADD CONSTRAINT product_care_area_detail_product_care_area_pk FOREIGN KEY (product_care_area_id) REFERENCES idsmed.product_care_area(id);


--
-- TOC entry 3647 (class 2606 OID 98778)
-- Name: product_care_area_detail product_care_area_detail_product_pk; Type: FK CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_care_area_detail
    ADD CONSTRAINT product_care_area_detail_product_pk FOREIGN KEY (product_id) REFERENCES idsmed.product(id);


--
-- TOC entry 3649 (class 2606 OID 98783)
-- Name: product_document_attachment_reminder product_document_attachment_id_fk; Type: FK CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_document_attachment_reminder
    ADD CONSTRAINT product_document_attachment_id_fk FOREIGN KEY (product_document_attachment_id) REFERENCES idsmed.product_document_attachment(id);


--
-- TOC entry 3655 (class 2606 OID 98788)
-- Name: product_rating product_document_attachment_product_fk; Type: FK CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_rating
    ADD CONSTRAINT product_document_attachment_product_fk FOREIGN KEY (product_id) REFERENCES idsmed.product(id);


--
-- TOC entry 3648 (class 2606 OID 98793)
-- Name: product_document_attachment product_document_attachment_product_label_config_fk; Type: FK CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_document_attachment
    ADD CONSTRAINT product_document_attachment_product_label_config_fk FOREIGN KEY (product_label_config_id) REFERENCES idsmed.product_label_config(id);


--
-- TOC entry 3650 (class 2606 OID 98798)
-- Name: product_features product_features_product_pk; Type: FK CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_features
    ADD CONSTRAINT product_features_product_pk FOREIGN KEY (product_id) REFERENCES idsmed.product(id);


--
-- TOC entry 3651 (class 2606 OID 98803)
-- Name: product_info_config product_info_config_country_fk; Type: FK CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_info_config
    ADD CONSTRAINT product_info_config_country_fk FOREIGN KEY (country_id) REFERENCES idsmed.country(id);


--
-- TOC entry 3652 (class 2606 OID 98808)
-- Name: product_info_config product_info_config_province_fk; Type: FK CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_info_config
    ADD CONSTRAINT product_info_config_province_fk FOREIGN KEY (province_id) REFERENCES idsmed.province(id);


--
-- TOC entry 3653 (class 2606 OID 98813)
-- Name: product_label_config product_label_config_product_info_config_fk; Type: FK CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_label_config
    ADD CONSTRAINT product_label_config_product_info_config_fk FOREIGN KEY (product_info_config_id) REFERENCES idsmed.product_info_config(id);


--
-- TOC entry 3654 (class 2606 OID 98818)
-- Name: product_media product_media_product_pk; Type: FK CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_media
    ADD CONSTRAINT product_media_product_pk FOREIGN KEY (product_id) REFERENCES idsmed.product(id);


--
-- TOC entry 3642 (class 2606 OID 98823)
-- Name: product product_product_brand_pk; Type: FK CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product
    ADD CONSTRAINT product_product_brand_pk FOREIGN KEY (product_brand_id) REFERENCES idsmed.product_brand(id);


--
-- TOC entry 3643 (class 2606 OID 98828)
-- Name: product product_product_category_pk; Type: FK CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product
    ADD CONSTRAINT product_product_category_pk FOREIGN KEY (product_category_id) REFERENCES idsmed.product_category(id);


--
-- TOC entry 3644 (class 2606 OID 98833)
-- Name: product product_product_hierarchy_fk; Type: FK CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product
    ADD CONSTRAINT product_product_hierarchy_fk FOREIGN KEY (product_hierarchy_id) REFERENCES idsmed.product_hierarchy(id);


--
-- TOC entry 3657 (class 2606 OID 98838)
-- Name: product_rating_detail product_rating_detail_idsmed_account_fk; Type: FK CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_rating_detail
    ADD CONSTRAINT product_rating_detail_idsmed_account_fk FOREIGN KEY (idsmed_account_id) REFERENCES idsmed.idsmed_account(id);


--
-- TOC entry 3658 (class 2606 OID 98843)
-- Name: product_rating_detail product_rating_detail_product_fk; Type: FK CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_rating_detail
    ADD CONSTRAINT product_rating_detail_product_fk FOREIGN KEY (product_id) REFERENCES idsmed.product(id);


--
-- TOC entry 3659 (class 2606 OID 98848)
-- Name: product_rating_detail product_rating_detail_product_rating_fk; Type: FK CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_rating_detail
    ADD CONSTRAINT product_rating_detail_product_rating_fk FOREIGN KEY (product_rating_id) REFERENCES idsmed.product_rating(id);


--
-- TOC entry 3656 (class 2606 OID 98853)
-- Name: product_rating product_rating_product_fk; Type: FK CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_rating
    ADD CONSTRAINT product_rating_product_fk FOREIGN KEY (product_id) REFERENCES idsmed.product(id);


--
-- TOC entry 3660 (class 2606 OID 98858)
-- Name: product_reject product_reject_product_pk; Type: FK CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_reject
    ADD CONSTRAINT product_reject_product_pk FOREIGN KEY (product_id) REFERENCES idsmed.product(id);


--
-- TOC entry 3661 (class 2606 OID 98863)
-- Name: product_second_category_detail product_second_category_detail_product_fk; Type: FK CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_second_category_detail
    ADD CONSTRAINT product_second_category_detail_product_fk FOREIGN KEY (product_id) REFERENCES idsmed.product(id);


--
-- TOC entry 3662 (class 2606 OID 98868)
-- Name: product_second_category_detail product_second_category_detail_product_second_category_fk; Type: FK CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_second_category_detail
    ADD CONSTRAINT product_second_category_detail_product_second_category_fk FOREIGN KEY (product_second_category_id) REFERENCES idsmed.product_second_category(id);


--
-- TOC entry 3663 (class 2606 OID 98873)
-- Name: product_technical product_technical_product_pk; Type: FK CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_technical
    ADD CONSTRAINT product_technical_product_pk FOREIGN KEY (product_id) REFERENCES idsmed.product(id);


--
-- TOC entry 3645 (class 2606 OID 98878)
-- Name: product product_vendor_pk; Type: FK CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product
    ADD CONSTRAINT product_vendor_pk FOREIGN KEY (vendor_id) REFERENCES idsmed.vendor(id);


--
-- TOC entry 3665 (class 2606 OID 98883)
-- Name: product_wishlist_detail product_wishlist_detail_idsmed_account_fk; Type: FK CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_wishlist_detail
    ADD CONSTRAINT product_wishlist_detail_idsmed_account_fk FOREIGN KEY (idsmed_account_id) REFERENCES idsmed.idsmed_account(id);


--
-- TOC entry 3666 (class 2606 OID 98888)
-- Name: product_wishlist_detail product_wishlist_detail_product_fk; Type: FK CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_wishlist_detail
    ADD CONSTRAINT product_wishlist_detail_product_fk FOREIGN KEY (product_id) REFERENCES idsmed.product(id);


--
-- TOC entry 3667 (class 2606 OID 98893)
-- Name: product_wishlist_detail product_wishlist_detail_product_wishlist_fk; Type: FK CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_wishlist_detail
    ADD CONSTRAINT product_wishlist_detail_product_wishlist_fk FOREIGN KEY (product_wishlist_id) REFERENCES idsmed.product_wishlist(id);


--
-- TOC entry 3664 (class 2606 OID 98898)
-- Name: product_wishlist product_wishlist_product_id_fk; Type: FK CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.product_wishlist
    ADD CONSTRAINT product_wishlist_product_id_fk FOREIGN KEY (product_id) REFERENCES idsmed.product(id);


--
-- TOC entry 3668 (class 2606 OID 98903)
-- Name: province province_country_fk; Type: FK CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.province
    ADD CONSTRAINT province_country_fk FOREIGN KEY (country_id) REFERENCES idsmed.country(id);


--
-- TOC entry 3615 (class 2606 OID 98908)
-- Name: batch_step_execution_context step_exec_ctx_fk; Type: FK CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.batch_step_execution_context
    ADD CONSTRAINT step_exec_ctx_fk FOREIGN KEY (step_execution_id) REFERENCES idsmed.batch_step_execution(step_execution_id);


--
-- TOC entry 3670 (class 2606 OID 98913)
-- Name: subscriber_ip subbscriber_ip_subscriber_pk; Type: FK CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.subscriber_ip
    ADD CONSTRAINT subbscriber_ip_subscriber_pk FOREIGN KEY (subscriber_id) REFERENCES idsmed.subscriber(id);


--
-- TOC entry 3669 (class 2606 OID 98918)
-- Name: subscriber_access_right subscriber_access_right_subscriber_pk; Type: FK CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.subscriber_access_right
    ADD CONSTRAINT subscriber_access_right_subscriber_pk FOREIGN KEY (subscriber_id) REFERENCES idsmed.subscriber(id);


--
-- TOC entry 3671 (class 2606 OID 98923)
-- Name: tender_category tender_category_idsmed_account_fk; Type: FK CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.tender_category
    ADD CONSTRAINT tender_category_idsmed_account_fk FOREIGN KEY (idsmed_account_id) REFERENCES idsmed.idsmed_account(id);


--
-- TOC entry 3672 (class 2606 OID 98928)
-- Name: token_information token_information_subscriber_pk; Type: FK CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.token_information
    ADD CONSTRAINT token_information_subscriber_pk FOREIGN KEY (subscriber_id) REFERENCES idsmed.subscriber(id);


--
-- TOC entry 3673 (class 2606 OID 98933)
-- Name: vendor_action_log vendor_action_log_vendor_fk; Type: FK CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.vendor_action_log
    ADD CONSTRAINT vendor_action_log_vendor_fk FOREIGN KEY (vendor_id) REFERENCES idsmed.vendor(id);


--
-- TOC entry 3674 (class 2606 OID 98938)
-- Name: vendor_additional_info vendor_additional_info_vendor_fk; Type: FK CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.vendor_additional_info
    ADD CONSTRAINT vendor_additional_info_vendor_fk FOREIGN KEY (vendor_id) REFERENCES idsmed.vendor(id);


--
-- TOC entry 3675 (class 2606 OID 98943)
-- Name: vendor_additional_info vendor_additional_info_vendor_label_config_fk; Type: FK CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.vendor_additional_info
    ADD CONSTRAINT vendor_additional_info_vendor_label_config_fk FOREIGN KEY (vendor_label_config_id) REFERENCES idsmed.vendor_label_config(id);


--
-- TOC entry 3676 (class 2606 OID 98948)
-- Name: vendor_address vendor_address_country_fk; Type: FK CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.vendor_address
    ADD CONSTRAINT vendor_address_country_fk FOREIGN KEY (country_id) REFERENCES idsmed.country(id);


--
-- TOC entry 3677 (class 2606 OID 98953)
-- Name: vendor_address vendor_address_province_fk; Type: FK CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.vendor_address
    ADD CONSTRAINT vendor_address_province_fk FOREIGN KEY (province_id) REFERENCES idsmed.province(id);


--
-- TOC entry 3678 (class 2606 OID 98958)
-- Name: vendor_address vendor_address_vendor_fk; Type: FK CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.vendor_address
    ADD CONSTRAINT vendor_address_vendor_fk FOREIGN KEY (vendor_id) REFERENCES idsmed.vendor(id);


--
-- TOC entry 3679 (class 2606 OID 98963)
-- Name: vendor_file_attachment_reminder vendor_file_attachment_reminder_file_attachment_fk; Type: FK CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.vendor_file_attachment_reminder
    ADD CONSTRAINT vendor_file_attachment_reminder_file_attachment_fk FOREIGN KEY (file_attachment_id) REFERENCES idsmed.file_attachment(id);


--
-- TOC entry 3680 (class 2606 OID 98968)
-- Name: vendor_info_config vendor_info_config_country_fk; Type: FK CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.vendor_info_config
    ADD CONSTRAINT vendor_info_config_country_fk FOREIGN KEY (country_id) REFERENCES idsmed.country(id);


--
-- TOC entry 3681 (class 2606 OID 98973)
-- Name: vendor_info_config vendor_info_config_province_fk; Type: FK CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.vendor_info_config
    ADD CONSTRAINT vendor_info_config_province_fk FOREIGN KEY (province_id) REFERENCES idsmed.province(id);


--
-- TOC entry 3682 (class 2606 OID 98978)
-- Name: vendor_label_config vendor_label_config_vendor_info_config_fk; Type: FK CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.vendor_label_config
    ADD CONSTRAINT vendor_label_config_vendor_info_config_fk FOREIGN KEY (vendor_info_config_id) REFERENCES idsmed.vendor_info_config(id);


--
-- TOC entry 3683 (class 2606 OID 98983)
-- Name: vendor_reject vendor_reject_vendor_pk; Type: FK CONSTRAINT; Schema: idsmed; Owner: dbadmin
--

ALTER TABLE ONLY idsmed.vendor_reject
    ADD CONSTRAINT vendor_reject_vendor_pk FOREIGN KEY (vendor_id) REFERENCES idsmed.vendor(id);


--
-- TOC entry 3684 (class 2606 OID 98988)
-- Name: idsmed_permission idsmed_permission_idsmed_function; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.idsmed_permission
    ADD CONSTRAINT idsmed_permission_idsmed_function FOREIGN KEY (function_id) REFERENCES public.idsmed_function(id);


--
-- TOC entry 3992 (class 0 OID 0)
-- Dependencies: 368
-- Name: FUNCTION column_exists(ptable text, pcolumn text); Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON FUNCTION public.column_exists(ptable text, pcolumn text) FROM postgres;
GRANT ALL ON FUNCTION public.column_exists(ptable text, pcolumn text) TO postgres WITH GRANT OPTION;


--
-- TOC entry 3993 (class 0 OID 0)
-- Dependencies: 381
-- Name: FUNCTION rename_column(ptable text, pcolumn text, new_name text); Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON FUNCTION public.rename_column(ptable text, pcolumn text, new_name text) FROM postgres;
GRANT ALL ON FUNCTION public.rename_column(ptable text, pcolumn text, new_name text) TO postgres WITH GRANT OPTION;


--
-- TOC entry 3995 (class 0 OID 0)
-- Dependencies: 188
-- Name: TABLE batch_job_execution; Type: ACL; Schema: idsmed; Owner: dbadmin
--

REVOKE ALL ON TABLE idsmed.batch_job_execution FROM dbadmin;
GRANT ALL ON TABLE idsmed.batch_job_execution TO dbadmin WITH GRANT OPTION;
GRANT ALL ON TABLE idsmed.batch_job_execution TO PUBLIC;


--
-- TOC entry 3996 (class 0 OID 0)
-- Dependencies: 189
-- Name: TABLE batch_job_execution_context; Type: ACL; Schema: idsmed; Owner: dbadmin
--

REVOKE ALL ON TABLE idsmed.batch_job_execution_context FROM dbadmin;
GRANT ALL ON TABLE idsmed.batch_job_execution_context TO dbadmin WITH GRANT OPTION;
GRANT ALL ON TABLE idsmed.batch_job_execution_context TO PUBLIC;


--
-- TOC entry 3997 (class 0 OID 0)
-- Dependencies: 190
-- Name: TABLE batch_job_execution_params; Type: ACL; Schema: idsmed; Owner: dbadmin
--

REVOKE ALL ON TABLE idsmed.batch_job_execution_params FROM dbadmin;
GRANT ALL ON TABLE idsmed.batch_job_execution_params TO dbadmin WITH GRANT OPTION;
GRANT ALL ON TABLE idsmed.batch_job_execution_params TO PUBLIC;


--
-- TOC entry 3998 (class 0 OID 0)
-- Dependencies: 191
-- Name: TABLE batch_job_instance; Type: ACL; Schema: idsmed; Owner: dbadmin
--

REVOKE ALL ON TABLE idsmed.batch_job_instance FROM dbadmin;
GRANT ALL ON TABLE idsmed.batch_job_instance TO dbadmin WITH GRANT OPTION;
GRANT ALL ON TABLE idsmed.batch_job_instance TO PUBLIC;


--
-- TOC entry 3999 (class 0 OID 0)
-- Dependencies: 192
-- Name: TABLE batch_step_execution; Type: ACL; Schema: idsmed; Owner: dbadmin
--

REVOKE ALL ON TABLE idsmed.batch_step_execution FROM dbadmin;
GRANT ALL ON TABLE idsmed.batch_step_execution TO dbadmin WITH GRANT OPTION;
GRANT ALL ON TABLE idsmed.batch_step_execution TO PUBLIC;


--
-- TOC entry 4000 (class 0 OID 0)
-- Dependencies: 193
-- Name: TABLE batch_step_execution_context; Type: ACL; Schema: idsmed; Owner: dbadmin
--

REVOKE ALL ON TABLE idsmed.batch_step_execution_context FROM dbadmin;
GRANT ALL ON TABLE idsmed.batch_step_execution_context TO dbadmin WITH GRANT OPTION;
GRANT ALL ON TABLE idsmed.batch_step_execution_context TO PUBLIC;


--
-- TOC entry 4084 (class 0 OID 0)
-- Dependencies: 361
-- Name: TABLE idsmed_function; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE public.idsmed_function FROM postgres;
GRANT ALL ON TABLE public.idsmed_function TO postgres WITH GRANT OPTION;


--
-- TOC entry 4086 (class 0 OID 0)
-- Dependencies: 362
-- Name: SEQUENCE idsmed_function_id_seq; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON SEQUENCE public.idsmed_function_id_seq FROM postgres;
GRANT ALL ON SEQUENCE public.idsmed_function_id_seq TO postgres WITH GRANT OPTION;


--
-- TOC entry 4087 (class 0 OID 0)
-- Dependencies: 363
-- Name: TABLE idsmed_permission; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE public.idsmed_permission FROM postgres;
GRANT ALL ON TABLE public.idsmed_permission TO postgres WITH GRANT OPTION;


--
-- TOC entry 4089 (class 0 OID 0)
-- Dependencies: 364
-- Name: SEQUENCE idsmed_permission_id_seq; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON SEQUENCE public.idsmed_permission_id_seq FROM postgres;
GRANT ALL ON SEQUENCE public.idsmed_permission_id_seq TO postgres WITH GRANT OPTION;


--
-- TOC entry 4090 (class 0 OID 0)
-- Dependencies: 365
-- Name: TABLE webservice_log; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE public.webservice_log FROM postgres;
GRANT ALL ON TABLE public.webservice_log TO postgres WITH GRANT OPTION;


-- Completed on 2019-02-13 14:40:23 +08

--
-- PostgreSQL database dump complete
--


ALTER TABLE idsmed.product ADD COLUMN IF NOT EXISTS patent CHARACTER VARYING;
ALTER TABLE idsmed.product ADD COLUMN IF NOT EXISTS vendor_unit_price numeric(18,2);
ALTER TABLE idsmed.product ADD COLUMN IF NOT EXISTS vendor_price numeric(18,2);
ALTER TABLE idsmed.product ADD COLUMN IF NOT EXISTS note CHARACTER VARYING;
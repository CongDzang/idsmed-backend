ALTER TABLE idsmed.product ALTER COLUMN exp_date SET DATA TYPE timestamp without time zone;
ALTER TABLE idsmed.product_log ALTER COLUMN exp_date SET DATA TYPE timestamp without time zone;
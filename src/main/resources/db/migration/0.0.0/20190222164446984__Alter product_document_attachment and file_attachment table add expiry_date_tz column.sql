ALTER TABLE idsmed.product_document_attachment ADD COLUMN IF NOT EXISTS exp_date_tz CHARACTER VARYING;
ALTER TABLE idsmed.file_attachment ADD COLUMN IF NOT EXISTS exp_date_tz CHARACTER VARYING;
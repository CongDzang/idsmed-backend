DO $$
BEGIN
    IF NOT EXISTS (SELECT 1 FROM pg_constraint WHERE conname = 'product_hierarchy_unique_code_key') THEN
        ALTER TABLE idsmed.product_hierarchy
            ADD CONSTRAINT product_hierarchy_unique_code_key
            UNIQUE (code);
    END IF;
END;
$$;
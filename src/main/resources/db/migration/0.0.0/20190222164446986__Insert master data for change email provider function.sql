DO $$
BEGIN
    IF NOT EXISTS (SELECT 1 FROM pg_constraint WHERE conname = 'idsmed_permission_unique_code_key') THEN
        ALTER TABLE idsmed.idsmed_permission
            ADD CONSTRAINT idsmed_permission_unique_code_key
            UNIQUE (code);
    END IF;
END;
$$;

DO $$
BEGIN
    IF NOT EXISTS (SELECT 1 FROM pg_constraint WHERE conname = 'idsmed_function_unique_code_key') THEN
        ALTER TABLE idsmed.idsmed_function
            ADD CONSTRAINT idsmed_function_unique_code_key
            UNIQUE (code);
    END IF;
END;
$$;

SELECT setval('idsmed.idsmed_function_id_seq', (select max(id) from idsmed.idsmed_function));

INSERT INTO idsmed.idsmed_function(code, name, description, status, created_datetime, updated_datetime, created_by, updated_by) VALUES ('SCF', 'System configuration', 'System configuration', 3, now(), now(), 1, 1) ON CONFLICT (code) DO NOTHING;

SELECT setval('idsmed.idsmed_permission_id_seq', (select max(id) from idsmed.idsmed_permission));

INSERT INTO idsmed.idsmed_permission(function_id, code, name, description, seq, status, country_code, created_datetime, updated_datetime, created_by, updated_by) VALUES (17, 'SCF0000001', 'Change email provider', 'Change email provider', '1', 3, 'cn', now(), now(), 1, 1) ON CONFLICT (code) DO NOTHING;

DO $$
BEGIN
    IF NOT EXISTS (SELECT 1 FROM pg_constraint WHERE conname = 'idsmed_role_permission_unique_role_id_permission_id_key') THEN
        ALTER TABLE idsmed.idsmed_role_permission
            ADD CONSTRAINT idsmed_role_permission_unique_role_id_permission_id_key
            UNIQUE (role_id, permission_id);
    END IF;
END;
$$;

SELECT setval('idsmed.idsmed_role_permission_id_seq', (select max(id) from idsmed.idsmed_role_permission));
INSERT INTO idsmed.idsmed_role_permission( role_id, permission_id, status, country_code, created_datetime,
            updated_datetime, created_by, updated_by) VALUES ( 1, 69, 3, 'cn', now(), now(), 1, 1) ON CONFLICT ON CONSTRAINT idsmed_role_permission_unique_role_id_permission_id_key DO NOTHING ;

INSERT INTO idsmed.idsmed_role_permission( role_id, permission_id, status, country_code, created_datetime,
            updated_datetime, created_by, updated_by) VALUES ( 17, 69, 3, 'cn', now(), now(), 1, 1) ON CONFLICT ON CONSTRAINT idsmed_role_permission_unique_role_id_permission_id_key DO NOTHING;
UPDATE idsmed.province SET chinese_brief_name = '广西壮族自治区' WHERE province_code = 'GX';
UPDATE idsmed.province SET chinese_brief_name = '香港特别行政区' WHERE province_code = 'HK';
UPDATE idsmed.province SET chinese_brief_name = '内蒙古自治区' WHERE province_code = 'NM';
UPDATE idsmed.province SET chinese_brief_name = '澳门特别行政区' WHERE province_code = 'MO';
UPDATE idsmed.province SET chinese_brief_name = '宁夏回族自治区' WHERE province_code = 'NX';
UPDATE idsmed.province SET chinese_brief_name = '西藏自治区' WHERE province_code = 'XZ';
UPDATE idsmed.province SET chinese_brief_name = '新疆维吾尔自治区' WHERE province_code = 'XJ';
ALTER TABLE idsmed.product_hierarchy RENAME TO zh_hans_l4_product_hierarchy;
ALTER TABLE idsmed.zh_hans_l4_product_hierarchy ALTER COLUMN name SET DATA TYPE CHARACTER VARYING;
ALTER TABLE idsmed.zh_hans_l4_product_hierarchy ALTER COLUMN code SET DATA TYPE CHARACTER VARYING;
ALTER TABLE idsmed.zh_hans_l4_product_hierarchy ALTER COLUMN name_zh_cn SET DATA TYPE CHARACTER VARYING;
CREATE TABLE IF NOT EXISTS idsmed.zh_hans_l1_product_hierarchy(
    id bigserial PRIMARY  KEY  NOT NULL,
    name CHARACTER VARYING,
    code CHARACTER VARYING,
    name_zh_cn CHARACTER VARYING,
    created_datetime timestamp without time zone DEFAULT now() NOT NULL,
    updated_datetime timestamp without time zone DEFAULT now() NOT NULL,
    created_by bigint NOT NULL DEFAULT -1,
    updated_by bigint NOT NULL DEFAULT -1,
    status integer NOT NULL DEFAULT 0
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE idsmed.zh_hans_l1_product_hierarchy
    OWNER to dbadmin;

CREATE TABLE IF NOT EXISTS idsmed.zh_hans_l2_product_hierarchy(
    id bigserial PRIMARY  KEY  NOT NULL,
    zh_hans_l1_product_hierarchy_id bigint,
    name CHARACTER VARYING,
    code CHARACTER VARYING,
    name_zh_cn CHARACTER VARYING,
    created_datetime timestamp without time zone DEFAULT now() NOT NULL,
    updated_datetime timestamp without time zone DEFAULT now() NOT NULL,
    created_by bigint NOT NULL DEFAULT -1,
    updated_by bigint NOT NULL DEFAULT -1,
    status integer NOT NULL DEFAULT 0,
    CONSTRAINT zh_hans_l2_product_hierarchy_zh_hans_l1_product_hierarchy_pk FOREIGN KEY (zh_hans_l1_product_hierarchy_id)
       REFERENCES idsmed.zh_hans_l1_product_hierarchy (id) MATCH SIMPLE
       ON UPDATE NO ACTION
       ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE idsmed.zh_hans_l2_product_hierarchy
    OWNER to dbadmin;

CREATE TABLE IF NOT EXISTS idsmed.zh_hans_l3_product_hierarchy(
    id bigserial PRIMARY  KEY  NOT NULL,
    zh_hans_l2_product_hierarchy_id bigint,
    name CHARACTER VARYING,
    code CHARACTER VARYING,
    name_zh_cn CHARACTER VARYING,
    created_datetime timestamp without time zone DEFAULT now() NOT NULL,
    updated_datetime timestamp without time zone DEFAULT now() NOT NULL,
    created_by bigint NOT NULL DEFAULT -1,
    updated_by bigint NOT NULL DEFAULT -1,
    status integer NOT NULL DEFAULT 0,
    CONSTRAINT zh_hans_l3_product_hierarchy_zh_hans_l2_product_hierarchy_pk FOREIGN KEY (zh_hans_l2_product_hierarchy_id)
       REFERENCES idsmed.zh_hans_l2_product_hierarchy (id) MATCH SIMPLE
       ON UPDATE NO ACTION
       ON DELETE NO ACTION

)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE idsmed.zh_hans_l3_product_hierarchy
    OWNER to dbadmin;
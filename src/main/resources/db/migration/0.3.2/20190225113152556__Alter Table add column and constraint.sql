ALTER TABLE idsmed.zh_hans_l4_product_hierarchy ADD COLUMN IF NOT EXISTS zh_hans_l3_product_hierarchy_id bigint;
ALTER TABLE idsmed.zh_hans_l4_product_hierarchy ADD COLUMN IF NOT EXISTS zh_hans_l3_product_hierarchy_id bigint;
DO $$
BEGIN
IF NOT EXISTS (SELECT 1 FROM pg_constraint WHERE conname = 'zh_hans_l4_product_hierarchy_zh_hans_l3_product_hierarchy_fk') THEN
ALTER TABLE idsmed.zh_hans_l4_product_hierarchy
ADD CONSTRAINT zh_hans_l4_product_hierarchy_zh_hans_l3_product_hierarchy_fk FOREIGN KEY (zh_hans_l3_product_hierarchy_id)
    REFERENCES idsmed.zh_hans_l3_product_hierarchy (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION;
END IF;
END;
$$;
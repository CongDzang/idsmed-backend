ALTER TABLE idsmed.product ADD COLUMN IF NOT EXISTS zh_hans_l1_product_hierarchy_id bigint;
ALTER TABLE idsmed.product ADD COLUMN IF NOT EXISTS zh_hans_l2_product_hierarchy_id bigint;
ALTER TABLE idsmed.product ADD COLUMN IF NOT EXISTS zh_hans_l3_product_hierarchy_id bigint;

DO $$
BEGIN
IF NOT EXISTS (SELECT 1 FROM pg_constraint WHERE conname = 'product_zh_hans_l1_product_hierarchy_fk') THEN
ALTER TABLE idsmed.product
ADD CONSTRAINT product_zh_hans_l1_product_hierarchy_fk FOREIGN KEY (zh_hans_l1_product_hierarchy_id)
    REFERENCES idsmed.zh_hans_l1_product_hierarchy (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION;
END IF;
END;
$$;

DO $$
BEGIN
IF NOT EXISTS (SELECT 1 FROM pg_constraint WHERE conname = 'product_zh_hans_l2_product_hierarchy_fk') THEN
ALTER TABLE idsmed.product
ADD CONSTRAINT product_zh_hans_l2_product_hierarchy_fk FOREIGN KEY (zh_hans_l2_product_hierarchy_id)
    REFERENCES idsmed.zh_hans_l2_product_hierarchy (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION;
END IF;
END;
$$;

DO $$
BEGIN
IF NOT EXISTS (SELECT 1 FROM pg_constraint WHERE conname = 'product_zh_hans_l3_product_hierarchy_fk') THEN
ALTER TABLE idsmed.product
ADD CONSTRAINT product_zh_hans_l3_product_hierarchy_fk FOREIGN KEY (zh_hans_l3_product_hierarchy_id)
    REFERENCES idsmed.zh_hans_l3_product_hierarchy (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION;
END IF;
END;
$$;
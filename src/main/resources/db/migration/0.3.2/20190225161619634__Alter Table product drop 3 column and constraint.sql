ALTER TABLE idsmed.product DROP COLUMN IF EXISTS zh_hans_l1_product_hierarchy_id;
ALTER TABLE idsmed.product DROP COLUMN IF EXISTS zh_hans_l2_product_hierarchy_id;
ALTER TABLE idsmed.product DROP COLUMN IF EXISTS zh_hans_l3_product_hierarchy_id;
ALTER TABLE idsmed.product RENAME COLUMN product_hierarchy_id TO zh_hans_l4_product_hierarchy_id;
ALTER TABLE idsmed.product DROP CONSTRAINT IF EXISTS product_zh_hans_l1_product_hierarchy_fk;
ALTER TABLE idsmed.product DROP CONSTRAINT IF EXISTS product_zh_hans_l2_product_hierarchy_fk;
ALTER TABLE idsmed.product DROP CONSTRAINT IF EXISTS product_zh_hans_l3_product_hierarchy_fk;
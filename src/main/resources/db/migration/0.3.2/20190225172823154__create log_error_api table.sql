CREATE TABLE IF NOT EXISTS idsmed.log_error_api(
	id bigserial PRIMARY KEY NOT NULL,
	login_id character varying(100),
	error_code character varying(10),
	url character varying(500),
	description character varying(1000),
	ip character varying(20),
	browser character varying(100),
	os character varying(100),
	created_datetime timestamp without time zone NOT NULL,
    updated_datetime timestamp without time zone NOT NULL,
    created_by bigint NOT NULL DEFAULT '-1'::integer,
    updated_by bigint NOT NULL DEFAULT '-1'::integer,
    status integer NOT NULL DEFAULT 3
)WITH (OIDS = FALSE) TABLESPACE pg_default;

ALTER TABLE idsmed.log_error_api OWNER TO dbadmin;

GRANT SELECT,INSERT,UPDATE,DELETE,TRUNCATE,REFERENCES,TRIGGER ON TABLE idsmed.log_error_api TO PUBLIC;
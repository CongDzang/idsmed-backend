ALTER TABLE idsmed.product_tech_feature DROP CONSTRAINT product_tech_feature_unique_label_name_key;
ALTER TABLE idsmed.product_tech_feature DROP CONSTRAINT product_tech_feature_unique_label_name_th_key;
ALTER TABLE idsmed.product_tech_feature DROP CONSTRAINT product_tech_feature_unique_label_name_vi_key;
ALTER TABLE idsmed.product_tech_feature DROP CONSTRAINT product_tech_feature_unique_label_name_id_key;
ALTER TABLE idsmed.product_tech_feature DROP CONSTRAINT product_tech_feature_unique_label_name_zh_cn_key;
ALTER TABLE idsmed.product_tech_feature DROP CONSTRAINT product_tech_feature_unique_label_name_zh_tw_key;

DO $$
	BEGIN
		IF NOT EXISTS (SELECT 1 FROM pg_constraint WHERE conname = 'product_tech_feature_unique_label_name_key') THEN
			ALTER TABLE idsmed.product_tech_feature
				ADD CONSTRAINT product_tech_feature_unique_label_name_key
				UNIQUE (label_name, label_type);
		END IF;
	END;
$$;

DO $$
	BEGIN
		IF NOT EXISTS (SELECT 1 FROM pg_constraint WHERE conname = 'product_tech_feature_unique_label_name_th_key') THEN
			ALTER TABLE idsmed.product_tech_feature
				ADD CONSTRAINT product_tech_feature_unique_label_name_th_key
				UNIQUE (label_name_th, label_type);
		END IF;
	END;
$$;

DO $$
	BEGIN
		IF NOT EXISTS (SELECT 1 FROM pg_constraint WHERE conname = 'product_tech_feature_unique_label_name_vi_key') THEN
			ALTER TABLE idsmed.product_tech_feature
				ADD CONSTRAINT product_tech_feature_unique_label_name_vi_key
				UNIQUE (label_name_vi, label_type);
		END IF;
	END;
$$;

DO $$
	BEGIN
		IF NOT EXISTS (SELECT 1 FROM pg_constraint WHERE conname = 'product_tech_feature_unique_label_name_id_key') THEN
			ALTER TABLE idsmed.product_tech_feature
				ADD CONSTRAINT product_tech_feature_unique_label_name_id_key
				UNIQUE (label_name_id, label_type);
		END IF;
	END;
$$;

DO $$
	BEGIN
		IF NOT EXISTS (SELECT 1 FROM pg_constraint WHERE conname = 'product_tech_feature_unique_label_name_zh_cn_key') THEN
			ALTER TABLE idsmed.product_tech_feature
				ADD CONSTRAINT product_tech_feature_unique_label_name_zh_cn_key
				UNIQUE (label_name_zh_cn, label_type);
		END IF;
	END;
$$;

DO $$
	BEGIN
		IF NOT EXISTS (SELECT 1 FROM pg_constraint WHERE conname = 'product_tech_feature_unique_label_name_zh_tw_key') THEN
			ALTER TABLE idsmed.product_tech_feature
				ADD CONSTRAINT product_tech_feature_unique_label_name_zh_tw_key
				UNIQUE (label_name_zh_tw, label_type);
		END IF;
	END;
$$;
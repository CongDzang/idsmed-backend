CREATE TABLE IF NOT EXISTS idsmed.hospital (
    id bigserial PRIMARY  KEY  NOT NULL,
    name_primary CHARACTER VARYING NOT NULL,
    name_secondary CHARACTER VARYING,
    country_id bigint,
    province_id bigint,
    code CHARACTER VARYING(250) NOT NULL,
    postcode CHARACTER VARYING(200),
    first_address CHARACTER VARYING,
    second_address CHARACTER VARYING,
    created_datetime timestamp without time zone NOT NULL,
    updated_datetime timestamp without time zone NOT NULL,
    approved_datetime timestamp without time zone,
    approved_by bigint,
    created_by bigint NOT NULL DEFAULT -1,
    updated_by bigint NOT NULL DEFAULT -1,
    status integer NOT NULL DEFAULT 0,
    CONSTRAINT hospital_country_fk FOREIGN KEY (country_id)
       REFERENCES idsmed.country (id) MATCH SIMPLE
       ON UPDATE NO ACTION
       ON DELETE NO ACTION,
    CONSTRAINT hospital_province_fk FOREIGN KEY (province_id)
       REFERENCES idsmed.province (id) MATCH SIMPLE
       ON UPDATE NO ACTION
       ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE idsmed.hospital
    OWNER to dbadmin;

CREATE TABLE IF NOT EXISTS idsmed.hospital_department (
    id bigserial PRIMARY  KEY  NOT NULL,
    name_primary CHARACTER VARYING NOT NULL,
    name_secondary CHARACTER VARYING,
    hospital_id bigint,
    code CHARACTER VARYING(250) NOT NULL,
    approved_datetime timestamp without time zone,
    approved_by bigint,
    created_datetime timestamp without time zone NOT NULL,
    updated_datetime timestamp without time zone NOT NULL,
    created_by bigint NOT NULL DEFAULT -1,
    updated_by bigint NOT NULL DEFAULT -1,
    status integer NOT NULL DEFAULT 0,
    CONSTRAINT hospital_department_hospital_fk FOREIGN KEY (hospital_id)
       REFERENCES idsmed.hospital (id) MATCH SIMPLE
       ON UPDATE NO ACTION
       ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE idsmed.hospital_department
    OWNER to dbadmin;


CREATE TABLE IF NOT EXISTS idsmed.log_user_login_history (
    id bigserial PRIMARY  KEY  NOT NULL,
    ip CHARACTER VARYING,
    login_id CHARACTER VARYING(255),
    country_code CHARACTER VARYING,
    region CHARACTER VARYING ,
    time_zone CHARACTER VARYING,
    browser CHARACTER VARYING,
    os CHARACTER VARYING,
    created_datetime timestamp without time zone NOT NULL,
    login_status integer NOT NULL DEFAULT 0
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE idsmed.log_user_login_history
    OWNER to dbadmin;

-- ALTER Some Tables
ALTER TABLE idsmed.idsmed_user ADD COLUMN IF NOT EXISTS gender INTEGER DEFAULT NULL;
ALTER TABLE idsmed.idsmed_user ADD COLUMN IF NOT EXISTS birthday DATE;
ALTER TABLE idsmed.idsmed_user ADD COLUMN IF NOT EXISTS mobile_phone_number CHARACTER VARYING(50);
ALTER TABLE idsmed.idsmed_user ADD COLUMN IF NOT EXISTS id_card_number CHARACTER VARYING;
ALTER TABLE idsmed.idsmed_user ADD COLUMN IF NOT EXISTS id_card_front_pic TEXT;
ALTER TABLE idsmed.idsmed_user ADD COLUMN IF NOT EXISTS id_card_back_pic TEXT;
ALTER TABLE idsmed.idsmed_user ADD COLUMN IF NOT EXISTS hospital_id bigint;

DO $$
BEGIN
IF NOT EXISTS (SELECT 1 FROM pg_constraint WHERE conname = 'idsmed_user_hospital_fk') THEN
ALTER TABLE idsmed.idsmed_user
ADD CONSTRAINT idsmed_user_hospital_fk FOREIGN KEY (hospital_id)
    REFERENCES idsmed.hospital (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION;
END IF;
END;
$$;


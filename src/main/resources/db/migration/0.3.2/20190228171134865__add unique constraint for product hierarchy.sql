DO $$
BEGIN
    IF NOT EXISTS (SELECT 1 FROM pg_constraint WHERE conname = 'zh_hans_l1_product_hierarchy_unique_code_key') THEN
        ALTER TABLE idsmed.zh_hans_l1_product_hierarchy
            ADD CONSTRAINT zh_hans_l1_product_hierarchy_unique_code_key
            UNIQUE (code);
    END IF;
END;
$$;

DO $$
BEGIN
    IF NOT EXISTS (SELECT 1 FROM pg_constraint WHERE conname = 'zh_hans_l2_product_hierarchy_unique_code_key') THEN
        ALTER TABLE idsmed.zh_hans_l2_product_hierarchy
            ADD CONSTRAINT zh_hans_l2_product_hierarchy_unique_code_key
            UNIQUE (code);
    END IF;
END;
$$;

DO $$
BEGIN
    IF NOT EXISTS (SELECT 1 FROM pg_constraint WHERE conname = 'zh_hans_l3_product_hierarchy_unique_code_key') THEN
        ALTER TABLE idsmed.zh_hans_l3_product_hierarchy
            ADD CONSTRAINT zh_hans_l3_product_hierarchy_unique_code_key
            UNIQUE (code);
    END IF;
END;
$$;
-- Script was first done in 0.3.1 (12/Feb/2019) but was removed due to refactoring. Now redo.

DO $$
    BEGIN
        IF EXISTS
            (
                SELECT 1
                FROM idsmed.email_notification_config
                WHERE notification_name = 'User Reset Password Notification')
        THEN
            UPDATE idsmed.email_notification_config
            SET notification_name = 'CONFIGURATION.User_Reset_Password_Notification'
            WHERE notification_name = 'User Reset Password Notification';
        END IF ;
    END
$$ ;
----------------------------------------------------------------------------------------------------
DO $$
    BEGIN
        IF EXISTS
            (
                SELECT 1
                FROM idsmed.email_notification_config
                WHERE notification_name = 'SCP User Registration Approved Notification')
        THEN
            UPDATE idsmed.email_notification_config
            SET notification_name = 'CONFIGURATION.SCP_User_Registration_Approved_Notification'
            WHERE notification_name = 'SCP User Registration Approved Notification';
        END IF ;
    END
$$ ;
----------------------------------------------------------------------------------------------------
DO $$
    BEGIN
        IF EXISTS
            (
                SELECT 1
                FROM idsmed.email_notification_config
                WHERE notification_name = 'Product Approved Notification')
        THEN
            UPDATE idsmed.email_notification_config
            SET notification_name = 'CONFIGURATION.Product_Approved_Notification'
            WHERE notification_name = 'Product Approved Notification';
        END IF ;
    END
$$ ;
----------------------------------------------------------------------------------------------------
DO $$
    BEGIN
        IF EXISTS
            (
                SELECT 1
                FROM idsmed.email_notification_config
                WHERE notification_name = 'Product Approved Notification For SCP Admin')
        THEN
            UPDATE idsmed.email_notification_config
            SET notification_name = 'CONFIGURATION.Product_Approved_Notification_For_SCP_Admin'
            WHERE notification_name = 'Product Approved Notification For SCP Admin';
        END IF ;
    END
$$ ;
----------------------------------------------------------------------------------------------------
DO $$
    BEGIN
        IF EXISTS
            (
                SELECT 1
                FROM idsmed.email_notification_config
                WHERE notification_name = 'Product Batch Upload Result Notification')
        THEN
            UPDATE idsmed.email_notification_config
            SET notification_name = 'CONFIGURATION.Product_Batch_Upload_Result_Notification'
            WHERE notification_name = 'Product Batch Upload Result Notification';
        END IF ;
    END
$$ ;
----------------------------------------------------------------------------------------------------
DO $$
    BEGIN
        IF EXISTS
            (
                SELECT 1
                FROM idsmed.email_notification_config
                WHERE notification_name = 'Product Certificate Approval Notification')
        THEN
            UPDATE idsmed.email_notification_config
            SET notification_name = 'CONFIGURATION.Product_Certificate_Approval_Notification'
            WHERE notification_name = 'Product Certificate Approval Notification';
        END IF ;
    END
$$ ;
----------------------------------------------------------------------------------------------------
DO $$
    BEGIN
        IF EXISTS
            (
                SELECT 1
                FROM idsmed.email_notification_config
                WHERE notification_name = 'Product Certificate Expiry Notification')
        THEN
            UPDATE idsmed.email_notification_config
            SET notification_name = 'CONFIGURATION.Product_Certificate_Expiry_Notification'
            WHERE notification_name = 'Product Certificate Expiry Notification';
        END IF ;
    END
$$ ;
----------------------------------------------------------------------------------------------------
DO $$
    BEGIN
        IF EXISTS
            (
                SELECT 1
                FROM idsmed.email_notification_config
                WHERE notification_name = 'Product Rejected Notification')
        THEN
            UPDATE idsmed.email_notification_config
            SET notification_name = 'CONFIGURATION.Product_Rejected_Notification'
            WHERE notification_name = 'Product Rejected Notification';
        END IF ;
    END
$$ ;
----------------------------------------------------------------------------------------------------
DO $$
    BEGIN
        IF EXISTS
            (
                SELECT 1
                FROM idsmed.email_notification_config
                WHERE notification_name = 'Request to Approve Product Notification')
        THEN
            UPDATE idsmed.email_notification_config
            SET notification_name = 'CONFIGURATION.Request_to_Approve_Product_Notification'
            WHERE notification_name = 'Request to Approve Product Notification';
        END IF ;
    END
$$ ;
----------------------------------------------------------------------------------------------------
DO $$
    BEGIN
        IF EXISTS
            (
                SELECT 1
                FROM idsmed.email_notification_config
                WHERE notification_name = 'Request to Approve Product Certificate Notification')
        THEN
            UPDATE idsmed.email_notification_config
            SET notification_name = 'CONFIGURATION.Request_to_Approve_Product_Certificate_Notification'
            WHERE notification_name = 'Request to Approve Product Certificate Notification';
        END IF ;
    END
$$ ;
----------------------------------------------------------------------------------------------------
DO $$
    BEGIN
        IF EXISTS
            (
                SELECT 1
                FROM idsmed.email_notification_config
                WHERE notification_name = 'Request to Approve Vendor Registration Notification')
        THEN
            UPDATE idsmed.email_notification_config
            SET notification_name = 'CONFIGURATION.Request_to_Approve_Vendor_Registration_Notification'
            WHERE notification_name = 'Request to Approve Vendor Registration Notification';
        END IF ;
    END
$$ ;
----------------------------------------------------------------------------------------------------
DO $$
    BEGIN
        IF EXISTS
            (
                SELECT 1
                FROM idsmed.email_notification_config
                WHERE notification_name = 'Request to Review Vendor Registration Notification')
        THEN
            UPDATE idsmed.email_notification_config
            SET notification_name = 'CONFIGURATION.Request_to_Review_Vendor_Registration_Notification'
            WHERE notification_name = 'Request to Review Vendor Registration Notification';
        END IF ;
    END
$$ ;
----------------------------------------------------------------------------------------------------
DO $$
    BEGIN
        IF EXISTS
            (
                SELECT 1
                FROM idsmed.email_notification_config
                WHERE notification_name = 'Vendor Profile Certificate Approval Notification')
        THEN
            UPDATE idsmed.email_notification_config
            SET notification_name = 'CONFIGURATION.Vendor_Profile_Certificate_Approval_Notification'
            WHERE notification_name = 'Vendor Profile Certificate Approval Notification';
        END IF ;
    END
$$ ;
----------------------------------------------------------------------------------------------------
DO $$
    BEGIN
        IF EXISTS
            (
                SELECT 1
                FROM idsmed.email_notification_config
                WHERE notification_name = 'Vendor Profile Certificate Expiry Notification')
        THEN
            UPDATE idsmed.email_notification_config
            SET notification_name = 'CONFIGURATION.Vendor_Profile_Certificate_Expiry_Notification'
            WHERE notification_name = 'Vendor Profile Certificate Expiry Notification';
        END IF ;
    END
$$ ;
----------------------------------------------------------------------------------------------------
DO $$
    BEGIN
        IF EXISTS
            (
                SELECT 1
                FROM idsmed.email_notification_config
                WHERE notification_name = 'Vendor Registration Approved Notification')
        THEN
            UPDATE idsmed.email_notification_config
            SET notification_name = 'CONFIGURATION.Vendor_Registration_Approved_Notification'
            WHERE notification_name = 'Vendor Registration Approved Notification';
        END IF ;
    END
$$ ;
----------------------------------------------------------------------------------------------------
DO $$
    BEGIN
        IF EXISTS
            (
                SELECT 1
                FROM idsmed.email_notification_config
                WHERE notification_name = 'Vendor Registration Notification')
        THEN
            UPDATE idsmed.email_notification_config
            SET notification_name = 'CONFIGURATION.Vendor_Registration_Notification'
            WHERE notification_name = 'Vendor Registration Notification';
        END IF ;
    END
$$ ;
----------------------------------------------------------------------------------------------------
DO $$
    BEGIN
        IF EXISTS
            (
                SELECT 1
                FROM idsmed.email_notification_config
                WHERE notification_name = 'Vendor Registration Rejected Notification')
        THEN
            UPDATE idsmed.email_notification_config
            SET notification_name = 'CONFIGURATION.Vendor_Registration_Rejected_Notification'
            WHERE notification_name = 'Vendor Registration Rejected Notification';
        END IF ;
    END
$$ ;
----------------------------------------------------------------------------------------------------
DO $$
    BEGIN
        IF EXISTS
            (
                SELECT 1
                FROM idsmed.email_notification_config
                WHERE notification_name = 'Request to Approve Vendor Certificate Notification')
        THEN
            UPDATE idsmed.email_notification_config
            SET notification_name = 'CONFIGURATION.Request_to_Approve_Vendor_Certificate_Notification'
            WHERE notification_name = 'Request to Approve Vendor Certificate Notification';
        END IF ;
    END
$$ ;
----------------------------------------------------------------------------------------------------
DO $$
    BEGIN
        IF EXISTS
            (
                SELECT 1
                FROM idsmed.email_notification_config
                WHERE notification_name = 'Vendor User Registration Approved Notification')
        THEN
            UPDATE idsmed.email_notification_config
            SET notification_name = 'CONFIGURATION.Vendor_User_Registration_Approved_Notification'
            WHERE notification_name = 'Vendor User Registration Approved Notification';
        END IF ;
    END
$$ ;
----------------------------------------------------------------------------------------------------
DO $$
    BEGIN
        IF EXISTS
            (
                SELECT 1
                FROM idsmed.email_notification_config
                WHERE notification_name = 'User Registration Approved Notification For SCP Admin')
        THEN
            UPDATE idsmed.email_notification_config
            SET notification_name = 'CONFIGURATION.User_Registration_Approved_Notification_For_SCP_Admin'
            WHERE notification_name = 'User Registration Approved Notification For SCP Admin';
        END IF ;
    END
$$ ;
----------------------------------------------------------------------------------------------------
DO $$
    BEGIN
        IF EXISTS
            (
                SELECT 1
                FROM idsmed.email_notification_config
                WHERE notification_name = 'User Registration Rejected Notification')
        THEN
            UPDATE idsmed.email_notification_config
            SET notification_name = 'CONFIGURATION.User_Registration_Rejected_Notification'
            WHERE notification_name = 'User Registration Rejected Notification';
        END IF ;
    END
$$ ;
----------------------------------------------------------------------------------------------------
DO $$
    BEGIN
        IF EXISTS
            (
                SELECT 1
                FROM idsmed.email_notification_config
                WHERE notification_name = 'Vendor User Registration Notification')
        THEN
            UPDATE idsmed.email_notification_config
            SET notification_name = 'CONFIGURATION.Vendor_User_Registration_Notification'
            WHERE notification_name = 'Vendor User Registration Notification';
        END IF ;
    END
$$ ;
----------------------------------------------------------------------------------------------------
DO $$
    BEGIN
        IF EXISTS
            (
                SELECT 1
                FROM idsmed.email_notification_config
                WHERE notification_name = 'New Vendor Staff Registration Notification for Vendor Admin')
        THEN
            UPDATE idsmed.email_notification_config
            SET notification_name = 'CONFIGURATION.New_Vendor_Staff_Registration_Notification_for_Vendor_Admin'
            WHERE notification_name = 'New Vendor Staff Registration Notification for Vendor Admin';
        END IF ;
    END
$$ ;

ALTER TABLE idsmed.zh_hans_l1_product_hierarchy ALTER COLUMN created_datetime DROP DEFAULT;
ALTER TABLE idsmed.zh_hans_l1_product_hierarchy ALTER COLUMN updated_datetime DROP DEFAULT;
ALTER TABLE idsmed.zh_hans_l2_product_hierarchy ALTER COLUMN created_datetime DROP DEFAULT;
ALTER TABLE idsmed.zh_hans_l2_product_hierarchy ALTER COLUMN updated_datetime DROP DEFAULT;
ALTER TABLE idsmed.zh_hans_l3_product_hierarchy ALTER COLUMN created_datetime DROP DEFAULT;
ALTER TABLE idsmed.zh_hans_l3_product_hierarchy ALTER COLUMN updated_datetime DROP DEFAULT;

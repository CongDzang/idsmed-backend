
DO $$
    BEGIN
        IF EXISTS
            (
                SELECT 1
                FROM idsmed.email_template
                WHERE template_name = 'Hospital User Registration Success')
        THEN
            UPDATE idsmed.email_template
            SET content = '<p>Hi [#USER_NAME],</p><p>You have successfully activated your account in smartscp.com</p><p>You may now log into the portal to enjoy the features provided by the system</p><p><br></p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>'
            WHERE template_name = 'Hospital User Registration Success';
        END IF ;
    END
$$ ;

DO $$
    BEGIN
        IF EXISTS
            (
                SELECT 1
                FROM idsmed.email_template
                WHERE template_name = 'Hospital User Registration Activation Link')
        THEN
            UPDATE idsmed.email_template
            SET content = '<p>Hi [#USER_NAME],</p><p>Thank you for signing up for SmartSCP.com </p><p>Your user account has been created and is pending for activation</p><br><p>Please kindly verify your account by click the link below:</p><p><br></p><p>[#USER_ACTIVATION_LINK]</p><p><br></p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>'
            WHERE template_name = 'Hospital User Registration Activation Link';
        END IF ;
    END
$$ ;
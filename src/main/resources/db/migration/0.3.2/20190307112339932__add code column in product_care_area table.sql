ALTER TABLE idsmed.product_care_area ADD COLUMN IF NOT EXISTS code CHARACTER VARYING(50) UNIQUE;

-- Add code to
DO $$
    BEGIN
        IF EXISTS
            (
                SELECT 1
                FROM idsmed.product_care_area
                WHERE id = 1)
        THEN
            UPDATE idsmed.product_care_area
            SET code = 'CAR000001'
            WHERE id = 1;
        END IF ;
    END
$$ ;

DO $$
    BEGIN
        IF EXISTS
            (
                SELECT 1
                FROM idsmed.product_care_area
                WHERE id = 2)
        THEN
            UPDATE idsmed.product_care_area
            SET code = 'CAR000002'
            WHERE id = 2;
        END IF ;
    END
$$ ;

DO $$
    BEGIN
        IF EXISTS
            (
                SELECT 1
                FROM idsmed.product_care_area
                WHERE id = 3)
        THEN
            UPDATE idsmed.product_care_area
            SET code = 'CAR000003'
            WHERE id = 3;
        END IF ;
    END
$$ ;

DO $$
    BEGIN
        IF EXISTS
            (
                SELECT 1
                FROM idsmed.product_care_area
                WHERE id = 4)
        THEN
            UPDATE idsmed.product_care_area
            SET code = 'CAR000004'
            WHERE id = 4;
        END IF ;
    END
$$ ;

DO $$
    BEGIN
        IF EXISTS
            (
                SELECT 1
                FROM idsmed.product_care_area
                WHERE id = 5)
        THEN
            UPDATE idsmed.product_care_area
            SET code = 'CAR000005'
            WHERE id = 5;
        END IF ;
    END
$$ ;

DO $$
    BEGIN
        IF EXISTS
            (
                SELECT 1
                FROM idsmed.product_care_area
                WHERE id = 6)
        THEN
            UPDATE idsmed.product_care_area
            SET code = 'CAR000006'
            WHERE id = 6;
        END IF ;
    END
$$ ;

DO $$
    BEGIN
        IF EXISTS
            (
                SELECT 1
                FROM idsmed.product_care_area
                WHERE id = 7)
        THEN
            UPDATE idsmed.product_care_area
            SET code = 'CAR000007'
            WHERE id = 7;
        END IF ;
    END
$$ ;

DO $$
    BEGIN
        IF EXISTS
            (
                SELECT 1
                FROM idsmed.product_care_area
                WHERE id = 8)
        THEN
            UPDATE idsmed.product_care_area
            SET code = 'CAR000008'
            WHERE id = 8;
        END IF ;
    END
$$ ;

DO $$
    BEGIN
        IF EXISTS
            (
                SELECT 1
                FROM idsmed.product_care_area
                WHERE id = 9)
        THEN
            UPDATE idsmed.product_care_area
            SET code = 'CAR000009'
            WHERE id = 9;
        END IF ;
    END
$$ ;

DO $$
    BEGIN
        IF EXISTS
            (
                SELECT 1
                FROM idsmed.product_care_area
                WHERE id = 10)
        THEN
            UPDATE idsmed.product_care_area
            SET code = 'CAR000010'
            WHERE id = 10;
        END IF ;
    END
$$ ;

DO $$
    BEGIN
        IF EXISTS
            (
                SELECT 1
                FROM idsmed.product_care_area
                WHERE id = 11)
        THEN
            UPDATE idsmed.product_care_area
            SET code = 'CAR000011'
            WHERE id = 11;
        END IF ;
    END
$$ ;

DO $$
    BEGIN
        IF EXISTS
            (
                SELECT 1
                FROM idsmed.product_care_area
                WHERE id = 12)
        THEN
            UPDATE idsmed.product_care_area
            SET code = 'CAR000012'
            WHERE id = 12;
        END IF ;
    END
$$ ;

DO $$
    BEGIN
        IF EXISTS
            (
                SELECT 1
                FROM idsmed.product_care_area
                WHERE id = 13)
        THEN
            UPDATE idsmed.product_care_area
            SET code = 'CAR000013'
            WHERE id = 13;
        END IF ;
    END
$$ ;

DO $$
    BEGIN
        IF EXISTS
            (
                SELECT 1
                FROM idsmed.product_care_area
                WHERE id = 14)
        THEN
            UPDATE idsmed.product_care_area
            SET code = 'CAR000014'
            WHERE id = 14;
        END IF ;
    END
$$ ;

DO $$
    BEGIN
        IF EXISTS
            (
                SELECT 1
                FROM idsmed.product_care_area
                WHERE id = 15)
        THEN
            UPDATE idsmed.product_care_area
            SET code = 'CAR000015'
            WHERE id = 15;
        END IF ;
    END
$$ ;

DO $$
    BEGIN
        IF EXISTS
            (
                SELECT 1
                FROM idsmed.product_care_area
                WHERE id = 16)
        THEN
            UPDATE idsmed.product_care_area
            SET code = 'CAR000016'
            WHERE id = 16;
        END IF ;
    END
$$ ;

DO $$
    BEGIN
        IF EXISTS
            (
                SELECT 1
                FROM idsmed.product_care_area
                WHERE id = 17)
        THEN
            UPDATE idsmed.product_care_area
            SET code = 'CAR000017'
            WHERE id = 17;
        END IF ;
    END
$$ ;

DO $$
    BEGIN
        IF EXISTS
            (
                SELECT 1
                FROM idsmed.product_care_area
                WHERE id = 18)
        THEN
            UPDATE idsmed.product_care_area
            SET code = 'CAR000018'
            WHERE id = 18;
        END IF ;
    END
$$ ;

DO $$
    BEGIN
        IF EXISTS
            (
                SELECT 1
                FROM idsmed.product_care_area
                WHERE id = 19)
        THEN
            UPDATE idsmed.product_care_area
            SET code = 'CAR000019'
            WHERE id = 19;
        END IF ;
    END
$$ ;

DO $$
    BEGIN
        IF EXISTS
            (
                SELECT 1
                FROM idsmed.product_care_area
                WHERE id = 20)
        THEN
            UPDATE idsmed.product_care_area
            SET code = 'CAR000020'
            WHERE id = 20;
        END IF ;
    END
$$ ;

DO $$
    BEGIN
        IF EXISTS
            (
                SELECT 1
                FROM idsmed.product_care_area
                WHERE id = 21)
        THEN
            UPDATE idsmed.product_care_area
            SET code = 'CAR000021'
            WHERE id = 21;
        END IF ;
    END
$$ ;

DO $$
    BEGIN
        IF EXISTS
            (
                SELECT 1
                FROM idsmed.product_care_area
                WHERE id = 22)
        THEN
            UPDATE idsmed.product_care_area
            SET code = 'CAR000022'
            WHERE id = 22;
        END IF ;
    END
$$ ;

DO $$
    BEGIN
        IF EXISTS
            (
                SELECT 1
                FROM idsmed.product_care_area
                WHERE id = 23)
        THEN
            UPDATE idsmed.product_care_area
            SET code = 'CAR000023'
            WHERE id = 23;
        END IF ;
    END
$$ ;

DO $$
    BEGIN
        IF EXISTS
            (
                SELECT 1
                FROM idsmed.product_care_area
                WHERE id = 24)
        THEN
            UPDATE idsmed.product_care_area
            SET code = 'CAR000024'
            WHERE id = 24;
        END IF ;
    END
$$ ;

DO $$
    BEGIN
        IF EXISTS
            (
                SELECT 1
                FROM idsmed.product_care_area
                WHERE id = 25)
        THEN
            UPDATE idsmed.product_care_area
            SET code = 'CAR000025'
            WHERE id = 25;
        END IF ;
    END
$$ ;

DO $$
    BEGIN
        IF EXISTS
            (
                SELECT 1
                FROM idsmed.product_care_area
                WHERE id = 26)
        THEN
            UPDATE idsmed.product_care_area
            SET code = 'CAR000026'
            WHERE id = 26;
        END IF ;
    END
$$ ;

DO $$
    BEGIN
        IF EXISTS
            (
                SELECT 1
                FROM idsmed.product_care_area
                WHERE id = 27)
        THEN
            UPDATE idsmed.product_care_area
            SET code = 'CAR000027'
            WHERE id = 27;
        END IF ;
    END
$$ ;

DO $$
    BEGIN
        IF EXISTS
            (
                SELECT 1
                FROM idsmed.product_care_area
                WHERE id = 28)
        THEN
            UPDATE idsmed.product_care_area
            SET code = 'CAR000028'
            WHERE id = 28;
        END IF ;
    END
$$ ;

DO $$
    BEGIN
        IF EXISTS
            (
                SELECT 1
                FROM idsmed.product_care_area
                WHERE id = 29)
        THEN
            UPDATE idsmed.product_care_area
            SET code = 'CAR000029'
            WHERE id = 29;
        END IF ;
    END
$$ ;

DO $$
    BEGIN
        IF EXISTS
            (
                SELECT 1
                FROM idsmed.product_care_area
                WHERE id = 29)
        THEN
            UPDATE idsmed.product_care_area
            SET code = 'CAR000029'
            WHERE id = 29;
        END IF ;
    END
$$ ;

DO $$
    BEGIN
        IF EXISTS
            (
                SELECT 1
                FROM idsmed.product_care_area
                WHERE id = 30)
        THEN
            UPDATE idsmed.product_care_area
            SET code = 'CAR000030'
            WHERE id = 30;
        END IF ;
    END
$$ ;
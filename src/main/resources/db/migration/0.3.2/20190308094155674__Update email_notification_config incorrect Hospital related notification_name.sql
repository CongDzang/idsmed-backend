DO $$
    BEGIN
        IF EXISTS
            (
                SELECT 1
                FROM idsmed.email_notification_config
                WHERE notification_name = 'Hospital User Registration Activation Link')
        THEN
            UPDATE idsmed.email_notification_config
            SET notification_name = 'CONFIGURATION.Hospital_User_Registration_Activation_Link'
            WHERE notification_name = 'Hospital User Registration Activation Link';
        END IF ;
    END
$$ ;
----------------------------------------------------------------------------------------------------
DO $$
    BEGIN
        IF EXISTS
            (
                SELECT 1
                FROM idsmed.email_notification_config
                WHERE notification_name = 'Hospital User Registration Success')
        THEN
            UPDATE idsmed.email_notification_config
            SET notification_name = 'CONFIGURATION.Hospital_User_Registration_Success'
            WHERE notification_name = 'Hospital User Registration Success';
        END IF ;
    END
$$ ;
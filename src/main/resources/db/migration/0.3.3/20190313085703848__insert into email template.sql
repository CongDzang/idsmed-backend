INSERT INTO idsmed.notification_wildcard (wildcard_name, wildcard_value, status, created_by, updated_by) values('Inactive Suspend Reason', '[#INACTIVE_SUSPEND_REASON]', 3, 1, 1) ON CONFLICT(wildcard_name) do nothing;
INSERT INTO idsmed.notification_wildcard (wildcard_name, wildcard_value, status, created_by, updated_by) values('Termination Reason', '[#TERMINATION_REASON]', 3, 1, 1)ON CONFLICT(wildcard_name) do nothing;
INSERT INTO idsmed.email_template (template_name, subject, content, status, created_by, updated_by) values ('Inactive / Suspended Vendor Profile Template', 'Smart SCP-Inactive / Suspended Vendor Profile', '<p>Hi [#RECIPIENT_NAME],</p><p>Your company, [#VENDOR_NAME] has been terminated by the operation team. The company is terminated for following reasons: [#TERMINATION_REASON] <br>You will not be able to login into the system. If you have any inquiries, please do contact us at support@smartscp.com. </p><br><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 3, 1, 1) ON CONFLICT(template_name) do nothing;
INSERT INTO idsmed.email_template (template_name, subject, content, status, created_by, updated_by) values ('Terminate Vendor Profile Template', 'Smart SCP-Terminate Vendor Profile Template', '<p>Hi [#RECIPIENT_NAME],</p><p>Your company, [#VENDOR_NAME] has been inactive / suspended by the operation team. The company may have been under suspension for following reasons: [#INACTIVE_SUSPEND_REASON] <br> During this period of suspension, you will not be able to login into the system until further notice. If you have any inquiries, please do contact us at support@smartscp.com</p><br><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', 3, 1, 1) ON CONFLICT(template_name) do nothing;
INSERT INTO idsmed.email_template (template_name, subject, content, status, created_by, updated_by) values ('Inactive / Suspended Product Template', 'Smart SCP-Inactive / Suspended Product Template', '<p>Hi [#RECIPIENT_NAME],</p> <p>Your product, [#VENDOR_NAME] (Product Code: [#PRODUCT_CODE]) has been inactive / suspended by the operation team. The product may have been under suspension for following reasons: [#INACTIVE_SUSPEND_REASON] During this period of suspension, your product will not be available in eCatalog for view/purchase. If you have any inquiries, please do contact us at support@smartscp.com</p><br><p>Thanks and Best Regards,</p> <p>idsWeDoc Operation Team</p>', 3, 1, 1) ON CONFLICT(template_name) do nothing;

INSERT INTO idsmed.email_notification_config
(template_id, email_type, enabled, recipient_list, country_code, created_datetime, updated_datetime, created_by, updated_by, status, title, notification_name, notification_config_type)
VALUES
((SELECT id FROM idsmed.email_template WHERE template_name = 'Inactive / Suspended Vendor Profile Template'),
28, true, null, 'en', now(), now(), 1, 1, 3, null, 'Inactive / Suspended Vendor Profile', 2) ON CONFLICT (notification_name) DO NOTHING;

INSERT INTO idsmed.email_notification_config
(template_id, email_type, enabled, recipient_list, country_code, created_datetime, updated_datetime, created_by, updated_by, status, title, notification_name, notification_config_type)
VALUES
((SELECT id FROM idsmed.email_template WHERE template_name = 'Terminate Vendor Profile Template'),
29, true, null, 'en', now(), now(), 1, 1, 1, null, 'Terminate Vendor Profile', 2) ON CONFLICT (notification_name) DO NOTHING;

INSERT INTO idsmed.email_notification_config
(template_id, email_type, enabled, recipient_list, country_code, created_datetime, updated_datetime, created_by, updated_by, status, title, notification_name, notification_config_type)
VALUES
((SELECT id FROM idsmed.email_template WHERE template_name = 'Inactive / Suspended Product Template'),
30, true, null, 'en', now(), now(), 1, 1, 1, null, 'Inactive / Suspended Product', 1) ON CONFLICT (notification_name) DO NOTHING;
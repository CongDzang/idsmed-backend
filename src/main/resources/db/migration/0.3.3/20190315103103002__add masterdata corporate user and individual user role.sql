-- ADD Corporate user role and email template and email
INSERT INTO idsmed.idsmed_role(code, name, description, status, country_code, created_datetime, updated_datetime, created_by, updated_by, role_type) VALUES ('Corp001', 'Corporate user', 'Corporate user', 3, 'cn', '2019-03-15 16:21:09.2', '2019-03-15 16:21:09.2', -1, -1, 0) ON CONFLICT (code) DO NOTHING;

INSERT INTO idsmed.email_template
(template_name, subject, content, created_datetime, updated_datetime, created_by, updated_by, status)
VALUES
('Corporate User Registration Activation Link', 'Corporate User Registration Activation Link', '<p>Hi [#USER_NAME],</p><p>Thank you for signing up for SmartSCP.com </p><p>Your user account has been created and is pending for activation</p><br><p>Please kindly verify your account by click the link below:</p><p><br></p><p>[#USER_ACTIVATION_LINK]</p><p><br></p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', now(), now(), 1, 1, 1) ON CONFLICT (template_name) DO NOTHING;

INSERT INTO idsmed.email_template
(template_name, subject, content, created_datetime, updated_datetime, created_by, updated_by, status)
VALUES
('Corporate User Registration Success', 'Corporate User Registration Success', '<p>Hi [#USER_NAME],</p><p>You have successfully activated your account in smartscp.com</p><p>You may now log into the portal to enjoy the features provided by the system</p><p><br></p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', now(), now(), 1, 1, 1) ON CONFLICT (template_name) DO NOTHING;


------------------------------------------------------------------------------------------------

INSERT INTO idsmed.email_notification_config
(template_id, email_type, enabled, recipient_list, country_code, created_datetime, updated_datetime, created_by, updated_by, status, title, notification_name, notification_config_type)
VALUES
((SELECT id FROM idsmed.email_template WHERE template_name = 'Corporate User Registration Activation Link'),
31, true, null, 'en', now(), now(), 1, 1, 1, null, 'label.corporate.user.registration.activation.link', 3) ON CONFLICT (notification_name) DO NOTHING;

INSERT INTO idsmed.email_notification_config
(template_id, email_type, enabled, recipient_list, country_code, created_datetime, updated_datetime, created_by, updated_by, status, title, notification_name, notification_config_type)
VALUES
((SELECT id FROM idsmed.email_template WHERE template_name = 'Corporate User Registration Success'),
32, true, null, 'en', now(), now(), 1, 1, 1, null, 'label.corporate.user.registration.success', 3) ON CONFLICT (notification_name) DO NOTHING;


-- ADD Individual user role and related email template and notification
INSERT INTO idsmed.idsmed_role(code, name, description, status, country_code, created_datetime, updated_datetime, created_by, updated_by, role_type) VALUES ('Indiv001', 'Individual user', 'Individual user', 3, 'cn', '2019-03-15 16:21:09.2', '2019-03-15 16:21:09.2', -1, -1, 0) ON CONFLICT (code) DO NOTHING;

INSERT INTO idsmed.email_template
(template_name, subject, content, created_datetime, updated_datetime, created_by, updated_by, status)
VALUES
('Individual User Registration Activation Link', 'Individual User Registration Activation Link', '<p>Hi [#USER_NAME],</p><p>Thank you for signing up for SmartSCP.com </p><p>Your user account has been created and is pending for activation</p><br><p>Please kindly verify your account by click the link below:</p><p><br></p><p>[#USER_ACTIVATION_LINK]</p><p><br></p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', now(), now(), 1, 1, 1) ON CONFLICT (template_name) DO NOTHING;

INSERT INTO idsmed.email_template
(template_name, subject, content, created_datetime, updated_datetime, created_by, updated_by, status)
VALUES
('Individual User Registration Success', 'Individual User Registration Success', '<p>Hi [#USER_NAME],</p><p>You have successfully activated your account in smartscp.com</p><p>You may now log into the portal to enjoy the features provided by the system</p><p><br></p><p>Thanks and Best Regards,</p><p>idsWeDoc Operation Team</p>', now(), now(), 1, 1, 1) ON CONFLICT (template_name) DO NOTHING;


------------------------------------------------------------------------------------------------

INSERT INTO idsmed.email_notification_config
(template_id, email_type, enabled, recipient_list, country_code, created_datetime, updated_datetime, created_by, updated_by, status, title, notification_name, notification_config_type)
VALUES
((SELECT id FROM idsmed.email_template WHERE template_name = 'Individual User Registration Activation Link'),
33, true, null, 'en', now(), now(), 1, 1, 1, null, 'label.individual.user.registration.activation.link', 3) ON CONFLICT (notification_name) DO NOTHING;

INSERT INTO idsmed.email_notification_config
(template_id, email_type, enabled, recipient_list, country_code, created_datetime, updated_datetime, created_by, updated_by, status, title, notification_name, notification_config_type)
VALUES
((SELECT id FROM idsmed.email_template WHERE template_name = 'Individual User Registration Success'),
34, true, null, 'en', now(), now(), 1, 1, 1, null, 'label.individual.user.registration.success', 3) ON CONFLICT (notification_name) DO NOTHING;
CREATE TABLE IF NOT EXISTS idsmed.corporate (
    id bigserial PRIMARY  KEY  NOT NULL,
    name_primary CHARACTER VARYING NOT NULL,
    name_secondary CHARACTER VARYING,
    country_id bigint,
    province_id bigint,
    code CHARACTER VARYING(250) NOT NULL,
    postcode CHARACTER VARYING(200),
    first_address CHARACTER VARYING,
    second_address CHARACTER VARYING,
    created_datetime timestamp without time zone NOT NULL,
    updated_datetime timestamp without time zone NOT NULL,
    approved_datetime timestamp without time zone,
    approved_by bigint,
    created_by bigint NOT NULL DEFAULT -1,
    updated_by bigint NOT NULL DEFAULT -1,
    status integer NOT NULL DEFAULT 0,
    CONSTRAINT corporate_country_fk FOREIGN KEY (country_id)
       REFERENCES idsmed.country (id) MATCH SIMPLE
       ON UPDATE NO ACTION
       ON DELETE NO ACTION,
    CONSTRAINT corporate_province_fk FOREIGN KEY (province_id)
       REFERENCES idsmed.province (id) MATCH SIMPLE
       ON UPDATE NO ACTION
       ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE idsmed.corporate
    OWNER to dbadmin;

ALTER TABLE idsmed.idsmed_user ADD COLUMN IF NOT EXISTS corporate_id bigint;
DO $$
BEGIN
IF NOT EXISTS (SELECT 1 FROM pg_constraint WHERE conname = 'idsmed_user_corporate_fk') THEN
ALTER TABLE idsmed.idsmed_user
ADD CONSTRAINT idsmed_user_corporate_fk FOREIGN KEY (corporate_id)
    REFERENCES idsmed.corporate (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION;
END IF;
END;
$$;
DO $$
    BEGIN
        IF EXISTS
            (
                SELECT 1
                FROM idsmed.email_notification_config
                WHERE notification_name = 'CONFIGURATION.Product_Approved_Notification')
        THEN
            UPDATE idsmed.email_notification_config
            SET notification_name = 'label.product.approved.notification'
            WHERE notification_name = 'CONFIGURATION.Product_Approved_Notification';
        END IF ;
    END
$$ ;

DO $$
    BEGIN
        IF EXISTS
            (
                SELECT 1
                FROM idsmed.email_notification_config
                WHERE notification_name = 'CONFIGURATION.Product_Approved_Notification_For_SCP_Admin')
        THEN
            UPDATE idsmed.email_notification_config
			SET notification_name = 'label.product.approved.notification.for.scp.admin'
			WHERE notification_name = 'CONFIGURATION.Product_Approved_Notification_For_SCP_Admin';
        END IF ;
    END
$$ ;

DO $$
    BEGIN
        IF EXISTS
            (
                SELECT 1
                FROM idsmed.email_notification_config
                WHERE notification_name = 'CONFIGURATION.Product_Batch_Upload_Result_Notification')
        THEN
            UPDATE idsmed.email_notification_config
			SET notification_name = 'label.product.batch.upload.result.notification'
			WHERE notification_name = 'CONFIGURATION.Product_Batch_Upload_Result_Notification';
        END IF ;
    END
$$ ;

DO $$
    BEGIN
        IF EXISTS
            (
                SELECT 1
                FROM idsmed.email_notification_config
                WHERE notification_name = 'CONFIGURATION.Product_Certificate_Approval_Notification')
        THEN
            UPDATE idsmed.email_notification_config
			SET notification_name = 'label.product.certificate.approval.notification'
			WHERE notification_name = 'CONFIGURATION.Product_Certificate_Approval_Notification';
        END IF ;
    END
$$ ;

DO $$
    BEGIN
        IF EXISTS
            (
                SELECT 1
                FROM idsmed.email_notification_config
                WHERE notification_name = 'CONFIGURATION.Product_Certificate_Expiry_Notification')
        THEN
            UPDATE idsmed.email_notification_config
			SET notification_name = 'label.product.certificate.expiry.notification'
			WHERE notification_name = 'CONFIGURATION.Product_Certificate_Expiry_Notification';
        END IF ;
    END
$$ ;

DO $$
    BEGIN
        IF EXISTS
            (
                SELECT 1
                FROM idsmed.email_notification_config
                WHERE notification_name = 'CONFIGURATION.Product_Rejected_Notification')
        THEN
            UPDATE idsmed.email_notification_config
			SET notification_name = 'label.product.rejected.notification'
			WHERE notification_name = 'CONFIGURATION.Product_Rejected_Notification';
        END IF ;
    END
$$ ;

DO $$
    BEGIN
        IF EXISTS
            (
                SELECT 1
                FROM idsmed.email_notification_config
                WHERE notification_name = 'CONFIGURATION.Request_to_Approve_Vendor_Registration_Notification')
        THEN
            UPDATE idsmed.email_notification_config
			SET notification_name = 'label.request.to.approve.vendor.registration.notification'
			WHERE notification_name = 'CONFIGURATION.Request_to_Approve_Vendor_Registration_Notification';
        END IF ;
    END
$$ ;

DO $$
    BEGIN
        IF EXISTS
            (
                SELECT 1
                FROM idsmed.email_notification_config
                WHERE notification_name = 'CONFIGURATION.Request_to_Review_Vendor_Registration_Notification')
        THEN
            UPDATE idsmed.email_notification_config
			SET notification_name = 'label.request.to.review.vendor.registration.notification'
			WHERE notification_name = 'CONFIGURATION.Request_to_Review_Vendor_Registration_Notification';
        END IF ;
    END
$$ ;

DO $$
    BEGIN
        IF EXISTS
            (
                SELECT 1
                FROM idsmed.email_notification_config
                WHERE notification_name = 'CONFIGURATION.Vendor_User_Registration_Approved_Notification')
        THEN
            UPDATE idsmed.email_notification_config
			SET notification_name = 'label.vendor.user.registration.approved.notification'
			WHERE notification_name = 'CONFIGURATION.Vendor_User_Registration_Approved_Notification';
        END IF ;
    END
$$ ;

DO $$
    BEGIN
        IF EXISTS
            (
                SELECT 1
                FROM idsmed.email_notification_config
                WHERE notification_name = 'CONFIGURATION.User_Registration_Approved_Notification_For_SCP_Admin')
        THEN
            UPDATE idsmed.email_notification_config
			SET notification_name = 'label.user.registration.approved.notification.for.scp.admin'
			WHERE notification_name = 'CONFIGURATION.User_Registration_Approved_Notification_For_SCP_Admin';
        END IF ;
    END
$$ ;

DO $$
    BEGIN
        IF EXISTS
            (
                SELECT 1
                FROM idsmed.email_notification_config
                WHERE notification_name = 'CONFIGURATION.User_Registration_Rejected_Notification')
        THEN
            UPDATE idsmed.email_notification_config
			SET notification_name = 'label.user.registration.rejected.notification'
			WHERE notification_name = 'CONFIGURATION.User_Registration_Rejected_Notification';
        END IF ;
    END
$$ ;

DO $$
    BEGIN
        IF EXISTS
            (
                SELECT 1
                FROM idsmed.email_notification_config
                WHERE notification_name = 'CONFIGURATION.Vendor_Profile_Certificate_Approval_Notification')
        THEN
            UPDATE idsmed.email_notification_config
			SET notification_name = 'label.vendor.profile.certificate.approval.notification'
			WHERE notification_name = 'CONFIGURATION.Vendor_Profile_Certificate_Approval_Notification';
        END IF ;
    END
$$ ;

DO $$
    BEGIN
        IF EXISTS
            (
                SELECT 1
                FROM idsmed.email_notification_config
                WHERE notification_name = 'CONFIGURATION.Vendor_Profile_Certificate_Expiry_Notification')
        THEN
            UPDATE idsmed.email_notification_config
			SET notification_name = 'label.vendor.profile.certificate.expiry.notification'
			WHERE notification_name = 'CONFIGURATION.Vendor_Profile_Certificate_Expiry_Notification';
        END IF ;
    END
$$ ;

DO $$
    BEGIN
        IF EXISTS
            (
                SELECT 1
                FROM idsmed.email_notification_config
                WHERE notification_name = 'CONFIGURATION.Vendor_Registration_Approved_Notification')
        THEN
            UPDATE idsmed.email_notification_config
			SET notification_name = 'label.vendor.registration.approved.notification'
			WHERE notification_name = 'CONFIGURATION.Vendor_Registration_Approved_Notification';
        END IF ;
    END
$$ ;

DO $$
    BEGIN
        IF EXISTS
            (
                SELECT 1
                FROM idsmed.email_notification_config
                WHERE notification_name = 'CONFIGURATION.Vendor_Registration_Notification')
        THEN
            UPDATE idsmed.email_notification_config
			SET notification_name = 'label.vendor.registration.notification'
			WHERE notification_name = 'CONFIGURATION.Vendor_Registration_Notification';
        END IF ;
    END
$$ ;

DO $$
    BEGIN
        IF EXISTS
            (
                SELECT 1
                FROM idsmed.email_notification_config
                WHERE notification_name = 'CONFIGURATION.Vendor_Registration_Rejected_Notification')
        THEN
            UPDATE idsmed.email_notification_config
			SET notification_name = 'label.vendor.registration.rejected.notification'
			WHERE notification_name = 'CONFIGURATION.Vendor_Registration_Rejected_Notification';
        END IF ;
    END
$$ ;

DO $$
    BEGIN
        IF EXISTS
            (
                SELECT 1
                FROM idsmed.email_notification_config
                WHERE notification_name = 'CONFIGURATION.Vendor_User_Registration_Notification')
        THEN
            UPDATE idsmed.email_notification_config
			SET notification_name = 'label.vendor.user.registration.notification'
			WHERE notification_name = 'CONFIGURATION.Vendor_User_Registration_Notification';
        END IF ;
    END
$$ ;

DO $$
    BEGIN
        IF EXISTS
            (
                SELECT 1
                FROM idsmed.email_notification_config
                WHERE notification_name = 'CONFIGURATION.New_Vendor_Staff_Registration_Notification_for_Vendor_Admin')
        THEN
            UPDATE idsmed.email_notification_config
			SET notification_name = 'label.new.vendor.staff.registration.notification.for.vendor.admin'
			WHERE notification_name = 'CONFIGURATION.New_Vendor_Staff_Registration_Notification_for_Vendor_Admin';
        END IF ;
    END
$$ ;

DO $$
    BEGIN
        IF EXISTS
            (
                SELECT 1
                FROM idsmed.email_notification_config
                WHERE notification_name = 'CONFIGURATION.User_Reset_Password_Notification')
        THEN
            UPDATE idsmed.email_notification_config
			SET notification_name = 'label.user.reset.password.notification'
			WHERE notification_name = 'CONFIGURATION.User_Reset_Password_Notification';
        END IF ;
    END
$$ ;

DO $$
    BEGIN
        IF EXISTS
            (
                SELECT 1
                FROM idsmed.email_notification_config
                WHERE notification_name = 'CONFIGURATION.SCP_User_Registration_Approved_Notification')
        THEN
            UPDATE idsmed.email_notification_config
			SET notification_name = 'label.scp.user.registration.approved.notification'
			WHERE notification_name = 'CONFIGURATION.SCP_User_Registration_Approved_Notification';
        END IF ;
    END
$$ ;

DO $$
    BEGIN
        IF EXISTS
            (
                SELECT 1
                FROM idsmed.email_notification_config
                WHERE notification_name = 'CONFIGURATION.Request_to_Approve_Product_Notification')
        THEN
            UPDATE idsmed.email_notification_config
			SET notification_name = 'label.request.to.approve.product.notification'
			WHERE notification_name = 'CONFIGURATION.Request_to_Approve_Product_Notification';
        END IF ;
    END
$$ ;

DO $$
    BEGIN
        IF EXISTS
            (
                SELECT 1
                FROM idsmed.email_notification_config
                WHERE notification_name = 'CONFIGURATION.Request_to_Approve_Product_Certificate_Notification')
        THEN
            UPDATE idsmed.email_notification_config
			SET notification_name = 'label.request.to.approve.product.certificate.notification'
			WHERE notification_name = 'CONFIGURATION.Request_to_Approve_Product_Certificate_Notification';
        END IF ;
    END
$$ ;

DO $$
    BEGIN
        IF EXISTS
            (
                SELECT 1
                FROM idsmed.email_notification_config
                WHERE notification_name = 'CONFIGURATION.Request_to_Approve_Vendor_Certificate_Notification')
        THEN
            UPDATE idsmed.email_notification_config
			SET notification_name = 'label.request.to.approve.vendor.certificate.notification'
			WHERE notification_name = 'CONFIGURATION.Request_to_Approve_Vendor_Certificate_Notification';
        END IF ;
    END
$$ ;

DO $$
    BEGIN
        IF EXISTS
            (
                SELECT 1
                FROM idsmed.email_notification_config
                WHERE notification_name = 'CONFIGURATION.Hospital_User_Registration_Activation_Link')
        THEN
            UPDATE idsmed.email_notification_config
			SET notification_name = 'label.hospital.user.registration.activation.link'
			WHERE notification_name = 'CONFIGURATION.Hospital_User_Registration_Activation_Link';
        END IF ;
    END
$$ ;

DO $$
    BEGIN
        IF EXISTS
            (
                SELECT 1
                FROM idsmed.email_notification_config
                WHERE notification_name = 'CONFIGURATION.Hospital_User_Registration_Success')
        THEN
            UPDATE idsmed.email_notification_config
			SET notification_name = 'label.hospital.user.registration.success'
			WHERE notification_name = 'CONFIGURATION.Hospital_User_Registration_Success';
        END IF ;
    END
$$ ;

-- 3 SQL statement below is different due to a new flyway script to insert new data before this
DO $$
    BEGIN
        IF EXISTS
            (
                SELECT 1
                FROM idsmed.email_notification_config
                WHERE email_type = 28)
        THEN
            UPDATE idsmed.email_notification_config
            SET notification_name = 'label.inactive.suspended.vendor.profile',
			notification_config_type = 2
            WHERE email_type = 28;
        END IF ;
    END
$$ ;

DO $$
    BEGIN
        IF EXISTS
            (
                SELECT 1
                FROM idsmed.email_notification_config
                WHERE email_type = 29)
        THEN
            UPDATE idsmed.email_notification_config
            SET notification_name = 'label.terminate.vendor.profile',
			notification_config_type = 2
            WHERE email_type = 29;
        END IF ;
    END
$$ ;

DO $$
    BEGIN
        IF EXISTS
            (
                SELECT 1
                FROM idsmed.email_notification_config
                WHERE email_type = 30)
        THEN
            UPDATE idsmed.email_notification_config
            SET notification_name = 'label.inactive.suspended.product',
			notification_config_type = 3
            WHERE email_type = 30;
        END IF ;
    END
$$ ;


CREATE TABLE IF NOT EXISTS idsmed.corporate_department (
    id bigserial PRIMARY  KEY  NOT NULL,
    name_primary CHARACTER VARYING NOT NULL,
    name_secondary CHARACTER VARYING,
    corporate_id bigint,
    code CHARACTER VARYING(250) NOT NULL,
    approved_datetime timestamp without time zone,
    approved_by bigint,
    created_datetime timestamp without time zone NOT NULL,
    updated_datetime timestamp without time zone NOT NULL,
    created_by bigint NOT NULL DEFAULT -1,
    updated_by bigint NOT NULL DEFAULT -1,
    status integer NOT NULL DEFAULT 0,
    CONSTRAINT corporate_department_corporate_fk FOREIGN KEY (corporate_id)
       REFERENCES idsmed.corporate (id) MATCH SIMPLE
       ON UPDATE NO ACTION
       ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE idsmed.corporate_department
    OWNER to dbadmin;
CREATE TABLE idsmed.user_address
(
    id bigserial PRIMARY  KEY  NOT NULL,
    idsmed_user_id bigint,
    country_id bigint,
    province_id bigint,
    first_address character varying,
    second_address character varying,
    third_address character varying,
    postcode character varying,
    from_date timestamp without time zone,
    to_date timestamp without time zone,
    created_datetime timestamp without time zone NOT NULL,
    updated_datetime timestamp without time zone NOT NULL,
    created_by bigint NOT NULL DEFAULT -1,
    updated_by bigint NOT NULL DEFAULT -1,
    status integer DEFAULT 1,
    CONSTRAINT user_address_country_fk FOREIGN KEY (country_id)
        REFERENCES idsmed.country (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT user_address_province_fk FOREIGN KEY (province_id)
        REFERENCES idsmed.province (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT user_address_idsmed_user_fk FOREIGN KEY (idsmed_user_id)
        REFERENCES idsmed.idsmed_user (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE idsmed.user_address
    OWNER to dbadmin;
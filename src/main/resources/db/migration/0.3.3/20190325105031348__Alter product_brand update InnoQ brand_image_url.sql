DO $$
    BEGIN
        IF EXISTS
            (
                SELECT 1
                FROM idsmed.product_brand
                WHERE brand_name = 'Innoq')
        THEN
            UPDATE idsmed.product_brand
			SET brand_image_url = 'idsmedmedia/product_brand/InnoQ.svg'
			WHERE brand_name = 'Innoq';
        END IF ;
    END
$$ ;
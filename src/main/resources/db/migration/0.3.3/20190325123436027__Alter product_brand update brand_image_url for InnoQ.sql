DO $$
    BEGIN
        IF EXISTS
            (
                SELECT 1
                FROM idsmed.product_brand
                WHERE upper(brand_name) = 'INNOQ')
        THEN
            UPDATE idsmed.product_brand
			SET brand_image_url = 'idsmedmedia/product_brand/InnoQ.svg'
			WHERE upper(brand_name) = 'INNOQ';
        END IF ;
    END
$$ ;
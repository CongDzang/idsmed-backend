-- CHANGE URL IN LOCAL
UPDATE idsmed.product_media SET document_url = regexp_replace(document_url, 'http://idsmedlocal.oss-cn-hongkong.aliyuncs.com/', '') WHERE document_url like 'http://idsmedlocal.oss-cn-hongkong.aliyuncs.com/%';

UPDATE idsmed.product_document_attachment SET document_path = regexp_replace(document_path, 'http://idsmedlocal.oss-cn-hongkong.aliyuncs.com/', '') WHERE document_path like 'http://idsmedlocal.oss-cn-hongkong.aliyuncs.com/%';

UPDATE idsmed.file_attachment SET document_path = regexp_replace(document_path, 'http://idsmedlocal.oss-cn-hongkong.aliyuncs.com/', '') WHERE document_path like 'http://idsmedlocal.oss-cn-hongkong.aliyuncs.com/%';

UPDATE idsmed.idsmed_user SET profile_image_url = regexp_replace(profile_image_url, 'http://idsmedlocal.oss-cn-hongkong.aliyuncs.com/', '') WHERE profile_image_url like 'http://idsmedlocal.oss-cn-hongkong.aliyuncs.com/%';

UPDATE idsmed.idsmed_user SET id_card_front_pic = regexp_replace(id_card_front_pic, 'http://idsmedlocal.oss-cn-hongkong.aliyuncs.com/', '') WHERE id_card_front_pic like 'http://idsmedlocal.oss-cn-hongkong.aliyuncs.com/%';

UPDATE idsmed.idsmed_user SET id_card_back_pic = regexp_replace(id_card_back_pic, 'http://idsmedlocal.oss-cn-hongkong.aliyuncs.com/', '') WHERE id_card_back_pic like 'http://idsmedlocal.oss-cn-hongkong.aliyuncs.com/%';

UPDATE idsmed.vendor SET company_logo_path = regexp_replace(company_logo_path, 'http://idsmedlocal.oss-cn-hongkong.aliyuncs.com/', '') WHERE company_logo_path like 'http://idsmedlocal.oss-cn-hongkong.aliyuncs.com/%';

UPDATE idsmed.product_care_area SET document_url = regexp_replace(document_url, 'http://idsmedlocal.oss-cn-hongkong.aliyuncs.com/', '') WHERE document_url like 'http://idsmedlocal.oss-cn-hongkong.aliyuncs.com/%';


-- CHANGE URL IN DEV SERVER
UPDATE idsmed.product_media SET document_url = regexp_replace(document_url, 'https://idsmeddev.oss-cn-hongkong.aliyuncs.com/', '') WHERE document_url like 'https://idsmeddev.oss-cn-hongkong.aliyuncs.com/%';

UPDATE idsmed.product_document_attachment SET document_path = regexp_replace(document_path, 'https://idsmeddev.oss-cn-hongkong.aliyuncs.com/', '') WHERE document_path like 'https://idsmeddev.oss-cn-hongkong.aliyuncs.com/%';

UPDATE idsmed.file_attachment SET document_path = regexp_replace(document_path, 'https://idsmeddev.oss-cn-hongkong.aliyuncs.com/', '') WHERE document_path like 'https://idsmeddev.oss-cn-hongkong.aliyuncs.com/%';

UPDATE idsmed.idsmed_user SET profile_image_url = regexp_replace(profile_image_url, 'https://idsmeddev.oss-cn-hongkong.aliyuncs.com/', '') WHERE profile_image_url like 'https://idsmeddev.oss-cn-hongkong.aliyuncs.com/%';

UPDATE idsmed.idsmed_user SET id_card_front_pic = regexp_replace(id_card_front_pic, 'https://idsmeddev.oss-cn-hongkong.aliyuncs.com/', '') WHERE id_card_front_pic like 'https://idsmeddev.oss-cn-hongkong.aliyuncs.com/%';

UPDATE idsmed.idsmed_user SET id_card_back_pic = regexp_replace(id_card_back_pic, 'https://idsmeddev.oss-cn-hongkong.aliyuncs.com/', '') WHERE id_card_back_pic like 'https://idsmeddev.oss-cn-hongkong.aliyuncs.com/%';

UPDATE idsmed.vendor SET company_logo_path = regexp_replace(company_logo_path, 'https://idsmeddev.oss-cn-hongkong.aliyuncs.com/', '') WHERE company_logo_path like 'https://idsmeddev.oss-cn-hongkong.aliyuncs.com/%';

UPDATE idsmed.product_care_area SET document_url = regexp_replace(document_url, 'http://idsmeddev.oss-cn-hongkong.aliyuncs.com/', '') WHERE document_url like 'http://idsmeddev.oss-cn-hongkong.aliyuncs.com/%';

-- CHANGE URL IN DEMO SERVER
UPDATE idsmed.product_media SET document_url = regexp_replace(document_url, 'https://idsmeddemo.oss-cn-hongkong.aliyuncs.com/', '') WHERE document_url like 'https://idsmeddemo.oss-cn-hongkong.aliyuncs.com/%';

UPDATE idsmed.product_document_attachment SET document_path = regexp_replace(document_path, 'https://idsmeddemo.oss-cn-hongkong.aliyuncs.com/', '') WHERE document_path like 'https://idsmeddemo.oss-cn-hongkong.aliyuncs.com/%';

UPDATE idsmed.file_attachment SET document_path = regexp_replace(document_path, 'https://idsmeddemo.oss-cn-hongkong.aliyuncs.com/', '') WHERE document_path like 'https://idsmeddemo.oss-cn-hongkong.aliyuncs.com/%';

UPDATE idsmed.idsmed_user SET profile_image_url = regexp_replace(profile_image_url, 'https://idsmeddemo.oss-cn-hongkong.aliyuncs.com/', '') WHERE profile_image_url like 'https://idsmeddemo.oss-cn-hongkong.aliyuncs.com/%';

UPDATE idsmed.idsmed_user SET id_card_front_pic = regexp_replace(id_card_front_pic, 'https://idsmeddemo.oss-cn-hongkong.aliyuncs.com/', '') WHERE id_card_front_pic like 'https://idsmeddemo.oss-cn-hongkong.aliyuncs.com/%';

UPDATE idsmed.idsmed_user SET id_card_back_pic = regexp_replace(id_card_back_pic, 'https://idsmeddemo.oss-cn-hongkong.aliyuncs.com/', '') WHERE id_card_back_pic like 'https://idsmeddemo.oss-cn-hongkong.aliyuncs.com/%';

UPDATE idsmed.vendor SET company_logo_path = regexp_replace(company_logo_path, 'https://idsmeddemo.oss-cn-hongkong.aliyuncs.com/', '') WHERE company_logo_path like 'https://idsmeddemo.oss-cn-hongkong.aliyuncs.com/%';

UPDATE idsmed.product_care_area SET document_url = regexp_replace(document_url, 'http://idsmeddemo.oss-cn-hongkong.aliyuncs.com/', '') WHERE document_url like 'http://idsmeddemo.oss-cn-hongkong.aliyuncs.com/';

-- CHANGE URL IN TRAINING SERVER
UPDATE idsmed.product_media SET document_url = regexp_replace(document_url, 'https://idsmedtraining.oss-cn-hongkong.aliyuncs.com/', '') WHERE document_url like 'https://idsmedtraining.oss-cn-hongkong.aliyuncs.com/%';

UPDATE idsmed.product_document_attachment SET document_path = regexp_replace(document_path, 'https://idsmedtraining.oss-cn-hongkong.aliyuncs.com/', '') WHERE document_path like 'https://idsmedtraining.oss-cn-hongkong.aliyuncs.com/%';

UPDATE idsmed.file_attachment SET document_path = regexp_replace(document_path, 'https://idsmedtraining.oss-cn-hongkong.aliyuncs.com/', '') WHERE document_path like 'https://idsmedtraining.oss-cn-hongkong.aliyuncs.com/%';

UPDATE idsmed.idsmed_user SET profile_image_url = regexp_replace(profile_image_url, 'https://idsmedtraining.oss-cn-hongkong.aliyuncs.com/', '') WHERE profile_image_url like 'https://idsmedtraining.oss-cn-hongkong.aliyuncs.com/%';

UPDATE idsmed.idsmed_user SET id_card_front_pic = regexp_replace(id_card_front_pic, 'https://idsmedtraining.oss-cn-hongkong.aliyuncs.com/', '') WHERE id_card_front_pic like 'https://idsmedtraining.oss-cn-hongkong.aliyuncs.com/%';

UPDATE idsmed.idsmed_user SET id_card_back_pic = regexp_replace(id_card_back_pic, 'https://idsmedtraining.oss-cn-hongkong.aliyuncs.com/', '') WHERE id_card_back_pic like 'https://idsmedtraining.oss-cn-hongkong.aliyuncs.com/%';

UPDATE idsmed.vendor SET company_logo_path = regexp_replace(company_logo_path, 'https://idsmedtraining.oss-cn-hongkong.aliyuncs.com/', '') WHERE company_logo_path like 'https://idsmedtraining.oss-cn-hongkong.aliyuncs.com/%';

UPDATE idsmed.product_care_area SET document_url = regexp_replace(document_url, 'http://idsmedtraining.oss-cn-hongkong.aliyuncs.com/', '') WHERE document_url like 'http://idsmedtraining.oss-cn-hongkong.aliyuncs.com/';

-- CHANGE URL IN PROD SERVER
UPDATE idsmed.product_media SET document_url = regexp_replace(document_url, 'https://idsmedprod.oss-cn-hongkong.aliyuncs.com/', '') WHERE document_url like 'https://idsmedprod.oss-cn-hongkong.aliyuncs.com/%';

UPDATE idsmed.product_document_attachment SET document_path = regexp_replace(document_path, 'https://idsmedprod.oss-cn-hongkong.aliyuncs.com/', '') WHERE document_path like 'https://idsmedprod.oss-cn-hongkong.aliyuncs.com/%';

UPDATE idsmed.file_attachment SET document_path = regexp_replace(document_path, 'https://idsmedprod.oss-cn-hongkong.aliyuncs.com/', '') WHERE document_path like 'https://idsmedprod.oss-cn-hongkong.aliyuncs.com/%';

UPDATE idsmed.idsmed_user SET profile_image_url = regexp_replace(profile_image_url, 'https://idsmedprod.oss-cn-hongkong.aliyuncs.com/', '') WHERE profile_image_url like 'https://idsmedprod.oss-cn-hongkong.aliyuncs.com/%';

UPDATE idsmed.idsmed_user SET id_card_front_pic = regexp_replace(id_card_front_pic, 'https://idsmedprod.oss-cn-hongkong.aliyuncs.com/', '') WHERE id_card_front_pic like 'https://idsmedprod.oss-cn-hongkong.aliyuncs.com/%';

UPDATE idsmed.idsmed_user SET id_card_back_pic = regexp_replace(id_card_back_pic, 'https://idsmedprod.oss-cn-hongkong.aliyuncs.com/', '') WHERE id_card_back_pic like 'https://idsmedprod.oss-cn-hongkong.aliyuncs.com/%';

UPDATE idsmed.vendor SET company_logo_path = regexp_replace(company_logo_path, 'https://idsmedprod.oss-cn-hongkong.aliyuncs.com/', '') WHERE company_logo_path like 'https://idsmedprod.oss-cn-hongkong.aliyuncs.com/%';

UPDATE idsmed.product_care_area SET document_url = regexp_replace(document_url, 'http://idsmedprod.oss-cn-hongkong.aliyuncs.com/', '') WHERE document_url like 'http://idsmedprod.oss-cn-hongkong.aliyuncs.com/';






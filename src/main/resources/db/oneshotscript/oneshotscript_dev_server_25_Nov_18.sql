-- Add some roles more.
SELECT setval('idsmed.idsmed_permission_id_seq', (select max(id) from idsmed.idsmed_permission));
INSERT INTO idsmed.idsmed_permission(function_id, code, name, description, seq, status, country_code, created_datetime, updated_datetime, created_by, updated_by) VALUES (1, 'UA0000007', 'RESUME/ACTIVATE', 'Resume / Activate User Profile', '1', 3, 'cn', now(), now(), 1, 1);
INSERT INTO idsmed.idsmed_permission(function_id, code, name, description, seq, status, country_code, created_datetime, updated_datetime, created_by, updated_by) VALUES (2, 'VP0000008', 'RESUME/ACTIVATE', 'Resume / Activate Vendor Profile', '1', 3, 'cn', now(), now(), 1, 1);
INSERT INTO idsmed.idsmed_permission(function_id, code, name, description, seq, status, country_code, created_datetime, updated_datetime, created_by, updated_by) VALUES (2, 'VP0000009', 'TERMINATE', 'Terminate Vendor Profile', '1', 3, 'cn', now(), now(), 1, 1);
INSERT INTO idsmed.idsmed_permission(function_id, code, name, description, seq, status, country_code, created_datetime, updated_datetime, created_by, updated_by) VALUES (3, 'PP0000008', 'RESUME/ACTIVATE', 'Resume / Activate Product', '1', 3, 'cn', now(), now(), 1, 1);

-- Script to delete wrong data of file attachment
delete from idsmed.file_attachment where vendor_label_config_id is null;

-- Script to update vendor_info_config
update idsmed.vendor_info_config set location = 1, vendor_type = 1 where id in (select id from idsmed.vendor_info_config);

-- Init product_rating
INSERT INTO idsmed.product_rating(product_id, average_score, total_count_of_rating, created_datetime, updated_datetime, created_by, updated_by, status, total_count_of_five_star, total_count_of_four_star, total_count_of_three_star, total_count_of_two_star, total_count_of_one_star, total_count_of_zero_star, total_of_score)
    SELECT id, 0, 0, now(), now(), 1, 1, 3, 0, 0, 0, 0, 0, 0, 0
    FROM idsmed.product;

-- Init product wish list
INSERT INTO idsmed.product_wishlist(product_id, total_wishlist, created_datetime, updated_datetime, created_by, updated_by, status)
	SELECT id, 0, now(), now(), 1, 1, 3
	FROM idsmed.product;

-- Clean notifications
delete from idsmed.notification_recipient;
delete from idsmed.notification;

-- clean mail_sending_log;
delete from idsmed.mail_sending_log;
